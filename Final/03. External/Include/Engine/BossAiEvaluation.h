#pragma once
class CBossAi;



class CBossAiEvaluation
{
private:
	//	여기에 기본정보설정
	//	기본설정 값 비교 소스(추적안함)
	CBossAi* m_pBoss;

	//	추적값 즉 업데이트가 이루어져야함.
	
public:
	CBossAiEvaluation();
	virtual ~CBossAiEvaluation();

	void SetBossAi(CBossAi* _pBoss) { m_pBoss = _pBoss; }
	CBossAi* GetBossAi() { return m_pBoss; }
};

namespace nBT
{
	enum class bResult
	{
		Evaluation = -1
		, Fail = 0
		, Running = 1
		, Success = 2
	};
	namespace VTC
	{
		class bNode
		{
		private:
			static UINT		g_nId;
			UINT			m_nId;
			wstring			m_wstrName;
		protected:
			CGameObject*	m_pBossObj;
			CBossAi*		m_pBossScript;
			nBT::bResult	 m_eState;
			int				m_nEvaluateCnt;
		public:

			bNode()
				:m_wstrName(L"Node" + std::to_wstring(g_nId))
				,m_pBossObj(nullptr)
				, m_pBossScript(nullptr)
				, m_nId(g_nId)
				, m_nEvaluateCnt(0)
				, m_eState(nBT::bResult::Evaluation)

			{
				++g_nId;
			}
			virtual ~bNode()
			{
				m_pBossObj = nullptr;
				m_pBossScript = nullptr;
			};

			void SetName(const wstring& _name) { m_wstrName = _name; };
			const wstring& GetName() { return m_wstrName; }

			void SetBossScript(CBossAi* _pBoss) { m_pBossScript = _pBoss; }
			CBossAi* GetBossScript() { return m_pBossScript; m_nEvaluateCnt = 0; }

			void SetBossObj(CGameObject* _pObj) { m_pBossObj = _pObj; }
			CGameObject* GetBossObj() { return m_pBossObj; }

			virtual void ResetFunc(void* data = nullptr){}
			void ResetBagic() { ResetFunc(); m_eState = nBT::bResult::Evaluation; }

			nBT::bResult GetStatus() { return m_eState; }

			void AddEvalueCnt() { ++m_nEvaluateCnt; }
			void ResetEvalueCnt() { m_nEvaluateCnt = 0; }
			UINT GetEvaluate() { return m_nEvaluateCnt; }

			virtual nBT::bResult Evaluate(bResult _result = bResult::Evaluation) = 0;
		};

		class bCompsite :public bNode
		{
		protected:
			UINT				m_nCurIdx;
			vector< bNode*>		m_vecChild;
		public:
			bCompsite() {};
			virtual ~bCompsite() {};

			void AddChild(nBT::VTC::bNode* _child) { m_vecChild.push_back(_child); }
			nBT::VTC::bNode* GetChild(int _idx) { return m_vecChild[_idx]; }

			virtual nBT::bResult Evaluate(bResult _result = bResult::Evaluation) = 0;
		};
		//	재밌는거
	}
	namespace CMS
	{
		class bSeq : public VTC::bCompsite
		{
		public:
			bSeq();
			virtual ~bSeq();

			virtual nBT::bResult Evaluate(bResult _result = bResult::Evaluation);
		};

		class bSel : public VTC::bCompsite
		{
		public:
			bSel();
			virtual ~bSel();

			virtual nBT::bResult Evaluate(bResult _result = bResult::Evaluation);
		};
		


	}
	namespace TSK
	{
		class bCondition : public VTC::bNode
		{
		public:
			bCondition();
			virtual ~bCondition();

			virtual nBT::bResult Evaluate(bResult _result = bResult::Evaluation);
		};
		class bAction : public VTC::bNode
		{
		public:
			bAction();
			virtual ~bAction();

			virtual nBT::bResult Evaluate(bResult _result = bResult::Evaluation);
		};
		class bDecorate :public VTC::bNode
		{
		private:
			vector<nBT::TSK::bCondition*> m_vecCondition;
			vector<nBT::TSK::bCondition*> m_vecAction;
		public:
			bDecorate();
			virtual ~bDecorate();

			void AddCondition(nBT::TSK::bCondition* _cdt) { if(_cdt != nullptr)m_vecCondition.push_back(_cdt); }
			void AddAction(nBT::TSK::bCondition* _act) { if (_act != nullptr) m_vecAction.push_back(_act); }

			void SetData(nBT::TSK::bCondition* _cdt, nBT::TSK::bCondition* _act)
			{
				AddCondition(_cdt);
				AddAction(_act);
			}

			virtual nBT::bResult Evaluate(bResult _result = bResult::Evaluation);
		};
		//	동시행동
		class bComplexAct : public VTC::bNode
		{
		private:
			vector<nBT::TSK::bCondition*> m_vecAction;
		public:
			bComplexAct();
			virtual ~bComplexAct();

			void AddAction(nBT::TSK::bCondition* _act) {if(_act != nullptr)m_vecAction.push_back(_act); }

			void SetData(nBT::TSK::bCondition* _act) { AddAction(_act); }

			virtual nBT::bResult Evaluate(bResult _result = bResult::Evaluation);
		};

	}
}

