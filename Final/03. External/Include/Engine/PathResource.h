#pragma once
struct tPath {
	tPath* pParnent = nullptr;
	USHORT z, x;
	USHORT g, h;
	BYTE type;
	BYTE parentToDir;
	short _pad0;
};

class CPathResource{
private:
	UINT		m_nZCnt;
	UINT		m_nXCnt;
	tPath*		m_pData;
	int			m_nReference;

	CPathResource();
	virtual ~CPathResource();

	void AddReference() { ++m_nReference; }
	void SubReference() {--m_nReference; }
public:
	static CPathResource* CreatePathRes() {
		CPathResource* pTemp = new CPathResource;
		pTemp->AddReference();
		return pTemp;
	}
	static void ReleasePathRes(CPathResource* _dest) {
		if (_dest == nullptr)return;
		_dest->SubReference();
		if (_dest->GetReference() <= 0) { delete _dest; _dest = nullptr; }
	}
	void Init(const wstring& _pathconst,const int& _zCnt, const int& _xCnt,const bool& _bInverse = true);
	const int& GetReference() { return m_nReference; }
	tPath* GetCoord(const UINT& _z, const UINT& _x) {
		if ((_z >= m_nZCnt) || (_x >= m_nXCnt) || (_z < 0) || (_x < 0))return nullptr;
		else return &m_pData[(m_nZCnt * _z) + _x];
	}
	bool IsBlock(const UINT& _z,const UINT& _x){}
	
	void SetData(tPath* _pData) { m_pData = _pData; }
	CPathResource* GetPathResource() { AddReference(); return this; }
	
};

