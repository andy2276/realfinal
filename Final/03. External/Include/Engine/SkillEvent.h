#pragma once
#include "Script.h"


class CSkillEvent :
	public CScript
{
private:
	UINT				m_Type;			//	이벤트 정보
	//	기본정보
	Vec3				m_vStartPos;
	Vec3				m_vStartRot;
	Vec3				m_vPrePos;
	Vec3				m_vCurPos;
	//	Action Sperad
	Vec3				m_vScaleMax;
	Vec3				m_vScaleCur;
	Vec3				m_vScaleMin;
	Vec3				m_vScaleInit;
	float				m_fScaleAcc;
	//	Action Flow
	CGameObject*		m_pTarget;
	float				m_fDelayDist;		
	//	Action Rotate
	Vec3				m_vRotSpeed;
	//	만
	bool				m_bAlive;
	bool				m_bStart;
	bool				m_bEnd;
public:
	CSkillEvent();
	virtual ~CSkillEvent();
	
	void SetTypes(UINT _type);
	void SetValues(const tSkillEventValue& _value);

	void SetAlive(bool _alive) { m_bAlive = _alive; }
	void SetStart(bool _start) { m_bStart = _start; }
	void SetEnd(bool _end) { m_bEnd = _end; }

	bool GetAlive() { return m_bAlive; }
	bool GetStart() { return m_bStart; }
	bool GetEnd() { return m_bEnd; }


	virtual void update();
	CLONE(CSkillEvent);

	//	이게 이벤트 스킬을 작동시켜주는 녀석들이니깐
	//	필요한게 회전량이나 시간 그런거 정하는거가 중요함
	//	이 이벤트에서 끝나는것도 정해줘야하나?모르겠다
};

