#pragma once
#include "Script.h"
enum class EC_MAP_STATE {
	 Evaluate			=	0
	,Sponning			=	1
	,RoundClear			=	2
	,LevelClear			=	3
	,MapMove			=	4
	,Stop				=	5
	,Awake				=	6
	, Cheat
	, TowerEvent
	, BossClear
};
//		SetMapIdx
//		SetPlayer
//		SetMapRect
//		
//		SetSponnerCnt	
//		SetSponner	...
//		
//		SetMonsterMaxCnt
//		SetRoundCnt
//		SetRoundMonster .. 라운드 수만큼
//		
//		SetPortal...
//		...
//		CGameObject* g_pMap[10];		
//		CMapScript* g_pMapScript[10];
//	
namespace BT
{
	namespace Tool
	{
		class CBlackBoard;
	}
}

struct tEnvir
{
	wstring A_name;
	wstring B_type;
	wstring C_x;
	wstring D_z;
	wstring E_y;
	wstring F_scale;
};
enum class EC_PORTAL_NUM
{
	_0
	,_1
	,_2
	,_3
	,End
};

class CScene;
class CSkillStoneShower;
class CMapScript :
	public CScript
{
private:
	UINT					m_nIdx;

	vector<CGameObject*>	m_vecReady[(UINT)EC_MOSTER_TYPE::End];		//	최초에 만들 몬스터 다 여기다가
	vector<CGameObject*>	m_vecStop[(UINT)EC_MOSTER_TYPE::End];
	vector<CGameObject*>	m_vecAllObj;

	vector<CGameObject*>	m_vecEnvir;

	list< CGameObject*>		m_listReadyMonsters;

	CGameObject*			m_pBoss;

	bool					m_bBossClear;

	int						m_nMaxMonsterCnt[(UINT)EC_MOSTER_TYPE::End];
	int						m_nCurMonsterCnt[(UINT)EC_MOSTER_TYPE::End];

	vector< tResponer>		m_vecSponner;
	CSkillStoneShower*		m_pStonesSkill;
	//	다수의 몬스터들을 위한 충돌처리와 AI구현

	//		4	5	6
	//		3	9	7
	//		2	1	8
	//			0
	//	최초에는 스폰된 몬스터가 꽉차있어야하고, 현재 잡은 몬스터는 0이어야한다.

	tPortal2				m_arrPortal[10];
	UINT					m_nPossiblePortal;
	

	CGameObject*			m_pPlayer;
	vector<tRoundInfo>		m_vecRoundInfo;

	
	Vec2					m_vMapHalfSize;
	Vec2					m_vStartPos;
	Vec4					m_vMapRect;

	int						 m_nCurRound;
	int						 m_nMaxRound;

	//	CollAndHei
	Vec2					m_vMapSize;

	//	startPos +  HalfPos
	Vec3					m_v3TextureCoordMapPos;
	//	col
	XMINT2					m_vColMapSize;
	float					m_fColMapScale;
	//	hei
	XMINT2					m_vHeiMapSize;

	BYTE*					m_pOriginColliderMap;
	BYTE*					m_pColliderMap;
	float*					m_pHeightMap;


	EC_MAP_STATE			m_eState;					//	최초에는 stop

	static bool				m_bPossibleTower;
	bool					m_bPreStop;					//	최초에는 false
	bool					m_bLevelClear;
	bool					m_bStartMap;
	bool					m_bCheck;

	bool					m_bInTower;
	
	bool					m_bTowerPortalIn;
	bool					m_bStartPortalIn;

	bool					m_bStopMap;
	
	bool					m_bFloorOn;
	float					m_fFloorDelayCur;
	float					m_fFloorDelayMax;
	static CGameObject*		m_pClearUIObj;

	int						m_nTowerPortal;
	int						m_nStartPortal;				//	이거 초기화는 1	

	float					m_fMapMoveDelayMax;
	float					m_fMapMoveDelayCur;

	bool					m_bDoInit;

	int						m_nPreCnt;
	int						m_nCurCnt;

	BYTE					m_bCurColId;
	string					m_strCurState;

	Vec3					m_vDir;
	int						m_nForceActive;

	
	bool					m_bSummonsOn	[(UINT)EC_PORTAL_NUM::End];
	float					m_fSummonsCur	[(UINT)EC_PORTAL_NUM::End];
	float					m_fSummonsMax	[(UINT)EC_PORTAL_NUM::End];
	CGameObject*			m_pSummonObj	[(UINT)EC_PORTAL_NUM::End];


	static	CGameObject*			m_pFloorUI;

	vector<BT::Tool::CBlackBoard*> m_vecBlackBoard;
	

	const float Linear(float v0, float v1, float t) { return (v0 * (1.0f - t) + v1 + t); }
public:
	CMapScript();
	virtual ~CMapScript();

	static int g_nTowerIn;
public:
	CLONE(CMapScript);

public:
	virtual void update();
	//	이거부터 설정
	void SetMonsterMaxCnt(int _mimic, int _skell, int _stone) {
		if (_mimic + _skell + _stone  == 0)return;
		int max = _mimic + _skell + _stone;
		if (_mimic > 0) {
			m_vecReady[(UINT)EC_MOSTER_TYPE::Mimic].resize(_mimic,nullptr);
			m_vecStop[(UINT)EC_MOSTER_TYPE::Mimic].resize(_mimic,nullptr);
		}
		m_nMaxMonsterCnt[(UINT)EC_MOSTER_TYPE::Mimic] = _mimic;
		m_nCurMonsterCnt[(UINT)EC_MOSTER_TYPE::Mimic] = 0;

		if (_skell > 0) {
			m_vecReady[(UINT)EC_MOSTER_TYPE::Skelleton].resize(_skell, nullptr);
			m_vecStop[(UINT)EC_MOSTER_TYPE::Skelleton].resize(_skell, nullptr);
		}
		m_nMaxMonsterCnt[(UINT)EC_MOSTER_TYPE::Skelleton] = _skell;
		m_nCurMonsterCnt[(UINT)EC_MOSTER_TYPE::Skelleton] = 0;

		if (_stone > 0) {
			m_vecReady[(UINT)EC_MOSTER_TYPE::Stone].resize(_stone, nullptr);
			m_vecStop[(UINT)EC_MOSTER_TYPE::Stone].resize(_stone, nullptr);
		}
		m_nMaxMonsterCnt[(UINT)EC_MOSTER_TYPE::Stone] = _stone;
		m_nCurMonsterCnt[(UINT)EC_MOSTER_TYPE::Stone] = 0;

		m_vecAllObj.resize(max, nullptr);
	};
	//	이걸 해주면 이제 이 맵의 트리거가 작동이 된다.
	void SettingMap() { m_bDoInit = true; }
	//	맵의 인덱스를 설정해준다
	void SetMapIdx(int _idx);
	//	맵에게 플레이어를 알려준다.
	void SetPlayer(CGameObject* _pPlayer) { m_pPlayer = _pPlayer; };

	//	라운드 개수를 결정 짓는다.
	void SetRoundCnt(int _cnt) { m_vecRoundInfo.resize(_cnt); m_nMaxRound = _cnt; }
	
	//	해당 라운드 정보를 결정 짓는다.
	void SetRoundInfo(int _idx, int _nClearCnt, EC_RESPON_TYPE _sponType, int _maxSponMonster, float _fSopnDelay,bool _boss = false,int _bossNum = -1);
	
	// 해당_roundNum 라운드의 모든 몬스터수를 결정짓는다.
	void SetRoundMonster(int _roundNum, int _mimic, int _skell, int _stone,bool _start = false);

	void SetBossMonster(int _roundNum, int _BossType, int _bossCnt);

	bool SponStone(const Vec3& _pos, const Vec3& _rot);
	bool SponSkelleton(const Vec3& _pos, const Vec3& _rot);
	bool SponMimic(const Vec3& _pos, const Vec3& _rot);




	//	몬스터들을 만든다. 참고로 맥스 개수를 넘어가게는 못만듦.
	void CreateMonster(bool _bIsStop, EC_MOSTER_TYPE _type,
		const wstring& _wstrLayerName, const wstring& _wstrFirstName,
		int _nMonsterCnt, CScene* _curScene);


	//	스포너부터 설정하고, 라운드 수를 정하고, 라운드에 나올 몬스터수를 정하고
	void SetSponnerCnt(int _cnt) { if (_cnt == 0)return; m_vecSponner.resize(_cnt); }
	//	400 , 400 이라고 생각하고 포지션 정하기
	void SetSponner(int _idx, float _range,const Vec3& _pos, const Vec3& _rot);

	void SetTowerPortal() { m_bTowerPortalIn = true; m_nTowerPortal = 0; }
	void SetStartPortal() { m_nStartPortal = 0; }
	
	
	void OffStopMap() { m_bStopMap = false; }
	void OnStopMap() { m_bStopMap = true; }



	//	이거도 
	//		0,400			400,400	
	//			-------------	
	//			|			|
	//			|			|
	//			-------------	
	//		0,0				400,0
	void SetPortal(int _outMapIdx,const Vec2& _pos, float _wid, float _hei,const Vec3& _sPos, const Vec3& _sRot);

	void SetMapRect() {
	
		Vec3 mapPos = GetObj()->Transform()->GetLocalPos();
		m_vStartPos = { mapPos.x - m_vMapHalfSize.x,mapPos.z - m_vMapHalfSize.y };
		
		m_v3TextureCoordMapPos = { m_vStartPos.x,0.0f,m_vStartPos.y };
		m_vMapRect.x = mapPos.x - m_vMapHalfSize.x;//	Left
		m_vMapRect.y = mapPos.x + m_vMapHalfSize.x;//	Right
		m_vMapRect.z = mapPos.z - m_vMapHalfSize.y;//	Down
		m_vMapRect.w = mapPos.z + m_vMapHalfSize.y;//	Up
	}
	const Vec4& GetMapRect() { return m_vMapRect; }

	//	Stop -> Ready
	void ArrangeToReady(int _mopType);//	Stop -> Ready
	//	Ready -> Stop
	void ArrangeToStop(int _mopType,bool _start = false);//	Ready -> Stop


	tPortal2& GetPortal(int _curIdx) {
		return m_arrPortal[_curIdx];
	};
	
	void CreateColliderMap(const wstring& _path, const XMINT2& _colSize,float _scale);
	void CreateHeightMap(const wstring& _path, const XMINT2& _heiSize);

	//	몬스터들 들고오기!

	//몬스터 타입으로 들고온다
	vector<CGameObject*>& GetMonster(EC_MOSTER_TYPE _mopType) { return m_vecReady[(UINT)_mopType]; }
	vector<CGameObject*>& GetMonster(int _mopType) { return m_vecReady[_mopType]; }

	//	이거는 모든 몬스터 리스트를 들고오는건데 비추
	list<CGameObject*>& GetAllMonster() 
	{
		m_listReadyMonsters.clear();
		if(m_vecReady[0].size() > 0 ){m_listReadyMonsters.insert(m_listReadyMonsters.end(), m_vecReady[0].begin(), m_vecReady[0].end());}
		if (m_vecReady[1].size() > 0){m_listReadyMonsters.insert(m_listReadyMonsters.end(), m_vecReady[1].begin(), m_vecReady[1].end());}
		if (m_vecReady[2].size() > 0){m_listReadyMonsters.insert(m_listReadyMonsters.end(), m_vecReady[2].begin(), m_vecReady[2].end());}

		return m_listReadyMonsters;
	}

	//	collider
	const Vec3& UnitCollider(BYTE _colID, int _z, int _x, bool& _stop);
	void StempingColliderMap(BYTE _colID, int _z, int _x);
	void StempingOne(BYTE _colID, int _z, int _x);
	const bool ColliderEnableStemping(BYTE _self, BYTE _type) {
		if ((BYTE)EC_COL_TYPE::Blocked == _type || _self == _type) {
			return false;
		}
		else {
			return true;
		}
	}
	const string& GetMapStringState() {
		switch (m_eState)
		{
		case EC_MAP_STATE::Evaluate:m_strCurState = "Evaluate";break;
		case EC_MAP_STATE::Sponning:m_strCurState = "Sponning";break;
		case EC_MAP_STATE::RoundClear:m_strCurState = "RoundClear";break;
		case EC_MAP_STATE::LevelClear:m_strCurState = "LevelClear";break;
		case EC_MAP_STATE::MapMove:m_strCurState = "MapMove";break;
		case EC_MAP_STATE::Stop:m_strCurState = "Stop";break;
		case EC_MAP_STATE::Awake:m_strCurState = "Awake";break;
		case EC_MAP_STATE::Cheat:m_strCurState = "Cheat"; break;
		case EC_MAP_STATE::TowerEvent:m_strCurState = "TowerEvent";break;
		}
		return m_strCurState;
	}

	void CalculNowMonster(int _idx);


	bool SponSequence(EC_MOSTER_TYPE _type, const Vec3& _pos, const Vec3& _rot);
	bool SponAtOnce(EC_MOSTER_TYPE _type, const Vec3& _pos, const Vec3& _rot);

	//	이거는 ready 중에서 체력이 0.0f이하인것들을 스탑으로 정리시켜주는 역할을 할것이다!
	void ArrangeMonster();
	void AllStop();
	void AllStop(int _idx);

	tColMap GetArround(int _z, int _x);

	void Transfer(bool _false = false);
	float GetHeight(const Vec3& _pos);

	int GetTreeType(const wstring& _type)
	{
		if (L"A1" == _type)return 0;
		if (L"A2" == _type)return 1;
		if (L"B1" == _type)return 2;
		if (L"B2" == _type)return 3;
		if (L"B3" == _type)return 4;
		if (L"C" == _type)return 5;
		if (L"D" == _type)return 6;
		if (L"r" == _type)return rand() % 7;

		return rand() % 6;
	}
	int GetStoneType(const wstring& _type)
	{

		if (L"A" == _type)return 0;
		if (L"B" == _type)return 1;
		if (L"C" == _type)return 2;
		if (L"D" == _type)return 3;
		if (L"E" == _type)return 4;
		if (L"F" == _type)return 5;
		if (L"G" == _type)return 6;
		if (L"r" == _type)return rand() % 7;

		return rand() % 7;
	}

	const Vec2& GetMapStartPos() { return m_vStartPos; }
	void CreateTree(const wstring& _path, const wstring& _wstrLayerName,CScene* _curScene);
	void CreateStone(const wstring& _path, const wstring& _wstrLayerName);
	void PushBackEnviroment(CGameObject* _pObj);


	void SetStoneSkill(CScene* _pScene, const wstring& _layer);

	void ErrTest();


	void OnSummons(int _idx);
	void PlaySummons(int _idx = -1);

	void SetFloorUI(CGameObject* _pObj) { m_pFloorUI = _pObj; }
	CGameObject* GetFloorUI() { return m_pFloorUI; }

	vector<CGameObject*>& GetEnvirObj() { return m_vecEnvir; }

	void SetBlackBoard(BT::Tool::CBlackBoard* _black);

	bool SponMonster(int monsterType, const Vec3 & _pos, const Vec3 & _rot);
	void CreateMonster(int _type,const wstring& _layerName,const wstring& _firstName,int _mopCnt);
};

