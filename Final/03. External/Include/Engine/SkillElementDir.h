#pragma once
#include "Script.h"
enum class EC_SKILL_ELEMENT :UINT
{
	EndType_Pos = 0x01
	, EndType_Time = 0x02
	, EndType_Dist = 0x04
	, EndType_Col = 0x08

	, MoveType_Straight = 0x10
	, MoveType_SinCosLook = 0x20
	, MoveType_SinCosUp = 0x40

	, LifeType_Repeate = 0x80

	, EventType_StartEffect = 0x100
	, EventType_IngEffect = 0x200
	, EventType_EndEffect = 0x400
	, EventType_ColEffect = 0x800

	, ColType_Damage = 0x1000
	
	
	//	해야할것이 이미지 출력을 어떻게할건지 선택
	//	또 부딪치자마자 없앨건지 아닌지
};

enum class EC_SKILL_EVENT :UINT
{
	MeshType_Rect = 0x01
	, MeshType_Circle = 0x02
	, MeshType_Cube = 0x04
	, MeshType_Sphere = 0x08

	, ActionType_Spread = 0x10
	, ActionType_Flow = 0x20
	, ActionType_Rotate = 0x40

	, ActionType_Option_TrunRepeat = 0x80
	, ActionType_Option_BillBoard = 0x100
	
};


struct tSkillEventValue;
class CSkillElementDir :
	public CScript
{
private:
	//	옵션값
	UINT			m_nType;

	UINT			m_nStartEvent;
	UINT			m_nIngEvent;
	UINT			m_nEndEvent;

	//	위치 기록
	Vec3			m_vStartPos;
	Vec3			m_vCurPos;
	Vec3			m_vPrePos;

	Vec3			m_vOffsetPos;

	//	종료 내용
	Vec3			m_vEndPos;			//	끝나는 위치가 존재하는거임
	float			m_fEndTime;			//	끝나는 시간이 존재하는거임
	float			m_fCurTime;
	float			m_fEndDist;			//	끝나는 거리가 존재하는거임

	//	움직임 값(go)
	Vec3			m_vMoveDir;			//	이거 방향대로 갈거임
	float			m_fMoveSpeed;		//	이거는 곱해질거임

	//	콜리더 앵글
	float			m_fSinCosInitAngle;
	float			m_fSinCosCurAngle;
	float			m_fSinCosAmplitude;
	float			m_fSinCosSize;

	//	움직임 제어
	bool			m_bStart;
	bool			m_bEnd;
	bool			m_bMovePossible;
	bool			m_bAlive;
	bool			m_bCollider;
	//	시간기록
	float			m_fTic;
	float			m_fTicMax;
	float			m_fToc;
	//	이벤트 시간
	float			m_fStartEventMax;	//	이거 이후에 발사됨
	float			m_fStartEventCur;
	float			m_fEndEventMax;	//	이거 이후에 끝남
	float			m_fEndEventCur;

	//	반복자
	int				m_nRepeateMax;
	int				m_nRepeateCur;
	//	데미지
	float			m_fDamage;
	//	이벤트	이거는 생성후 파괴하자!
	CGameObject*	m_pStartEvent;
	CGameObject*	m_pIngEvent;
	CGameObject*	m_pEndEvent;

	tSkillElementValue m_tSkillElementValue;

public:
	CSkillElementDir();
	virtual ~CSkillElementDir();

	//	각 셋팅값 설정 만들기
	void InitShoot(const Vec3& _pos, const Vec3& _rot);
	void RandShootValue();

	void SetStartPos(const Vec3& _pos) {  m_vStartPos = _pos; }
	void SetEndPos(const Vec3& _pos) { m_vEndPos = _pos; }
	void SetEndTime(float _time) { m_fEndTime = _time; }
	void SetEndDist(float _dist) { m_fEndDist = _dist; }
	void SetMoveDir(const Vec3& _dir) { m_vMoveDir = _dir; }
	void SetMoveSpeed(float _speed) { m_fMoveSpeed = _speed; }

	UINT GetTypes() { return m_nType; }
	void SetTypes(UINT _type);
	void SetValues(const tSkillElementValue& _value);	//	이거쓰자
	void SetAlive(bool _alive) { m_bAlive = _alive; }

	void Option_Damage();
	void Option_Go();
	void Option_SicCos();

	void Option_Repeate();
	void Option_StartEvent(float _MaxTime) { m_fStartEventMax = _MaxTime; };
	void Option_IngEvent();
	void Option_EndEvent();


	virtual void update();
	virtual void OnCollisionEnter(CCollider2D* _pOther);
	virtual void OnCollision(CCollider2D* _pOther);
	virtual void OnCollisionExit(CCollider2D* _pOther);

	CGameObject* CreateEffect(int _timing, int _textureIdx, UINT _type,tSkillEventValue& _value);

	bool HaveStartEffect() { return(m_pStartEvent != nullptr)? true: false; }
	void OnStartEffect() { if (m_pStartEvent != nullptr)m_pStartEvent->SetActive(true); }
	void ResetStartEffect(tSkillEventValue& _value);


	CLONE(CSkillElementDir);
};

