#pragma once
#include "BehaviorTree.h"


//	action		:	act		:	ACT
//	condition	:	cdt		:	CDT
//	

class BTcdt_LessHp :public BTcls::CCondition {
private:
	float	m_fHp;
public:
	BTcdt_LessHp();
	virtual ~BTcdt_LessHp();

	void SetHp(const float& _hp) { m_fHp = _hp; }

	virtual BT::EC_RETURN Evaluation(void* _pData);
	virtual BT::EC_RETURN operator() (void* _pData) { return Evaluation(_pData); }
};
class BTcdt_OverHp :public BTcls::CCondition {
private:
	float	m_fHp;
public:
	BTcdt_OverHp();
	virtual ~BTcdt_OverHp();

	void SetHp(const float& _hp) { m_fHp = _hp; }

	virtual BT::EC_RETURN Evaluation(void* _pData);
	virtual BT::EC_RETURN operator() (void* _pData) { return Evaluation(_pData); }
};
template<typename T>
class BTcdt_LessValue : public BTcls::CCondition {
private:
	T	m_CompDestValue;
	UINT m_nCompScrIdx;
public:
	BTcdt_LessValue() = delete;
	BTcdt_LessValue(const T& _dest, const UINT& _scr)
		:m_CompDestValue(_dest), m_nCompScrIdx(_scr)
	{}
	virtual ~BTcdt_LessValue() {};

	void SetValue(const T& _value) { m_nCompScrIdx = _value; }

	virtual BT::EC_RETURN Evaluation(void* _pData) {
		
	};
	virtual BT::EC_RETURN operator() (void* _pData) { return Evaluation(_pData); }
};
template<typename T>
class BTcdt_OverValue : public BTcls::CCondition {
private:
	T	m_CompDestValue;
	UINT m_nCompScrIdx;
public:
	BTcdt_OverValue()= delete;
	BTcdt_OverValue(const T& _dest, const UINT& _scr)
		:m_CompDestValue(_dest), m_nCompScrIdx(_scr)
	{}
	virtual ~BTcdt_OverValue() {};

	void SetValue(const T& _value) { m_nCompScrIdx = _value; }
	
	virtual BT::EC_RETURN Evaluation(void* _pData) {
		
	};
	virtual BT::EC_RETURN operator() (void* _pData) { return Evaluation(_pData); }
};
template<typename T>
class BTcdt_Comparison : public BTcls::CCondition {
private:
	T	m_CompDestValue;
	UINT m_nCompScrIdx;
	std::function<const bool(const T& _dest, const T& _scr)> m_funcComparison;
public:
	BTcdt_Comparison() = delete;
	BTcdt_Comparison(const T& _dest, const UINT& _scr, std::function<const bool(const T& _dest, const T& _scr)> _comparison)
		:m_CompDestValue(_dest), m_nCompScrIdx(_scr), m_funcComparison(_comparison)
	{}
	virtual ~BTcdt_Comparison() {};

	void SetValue(const T& _value) { m_CompDestValue = _value; }

	virtual BT::EC_RETURN Evaluation(void* _pData) {
		
	};
	virtual BT::EC_RETURN operator() (void* _pData) { return Evaluation(_pData); }
};



class BTact_MoveObj : public BTcls::CAction {
private:
	Vec3		m_vTargetPos;
public:
	BTact_MoveObj();
	virtual ~BTact_MoveObj();

	void SetTargetPos(const Vec3& _targetPos) { m_vTargetPos = _targetPos; }

	virtual BT::EC_RETURN Evaluation(void* _pData);
	virtual BT::EC_RETURN operator() (void* _pData) { return Evaluation(_pData); }
};
class BTact_ExcuteAnim : public BTcls::CAction {
private:
	//	animation
public:
	BTact_ExcuteAnim();
	virtual ~BTact_ExcuteAnim();

	virtual BT::EC_RETURN Evaluation(void* _pData);
	virtual BT::EC_RETURN operator() (void* _pData) { return Evaluation(_pData); }
};
class Btact_ExcuteAttack : public BTcls::CAction {
private:

public:
	Btact_ExcuteAttack();
	virtual ~Btact_ExcuteAttack();

	virtual BT::EC_RETURN Evaluation(void* _pData);
	virtual BT::EC_RETURN operator() (void* _pData) { return Evaluation(_pData); }
};