#pragma once
#include "TriggerNode.h"
#include "TriggerSpecial.h"
class CSponSystem;
class CGameObject;
class CMapRapper;
class CTriggerMaker
{
private:
	Trigger::Base::CBasis*					m_pRoot;		//여기의 루트는 임시 루트
	CGameObject*							m_pGameObj;
	Trigger::CShareHouse*					m_pShareHouse;
	//	사실은 list
	std::list<Trigger::Base::CBasis*>*		m_pStack;
	std::vector<Trigger::Base::CBasis*>*	m_pTriggers;
	CSponSystem*							m_pSponSystem;
	CMapRapper*								m_pMapRapper;
public:
	CTriggerMaker();
	~CTriggerMaker();

	void Init(CGameObject* _pObj, Trigger::CShareHouse* _pShare, std::list<Trigger::Base::CBasis*>* _pList, std::vector<Trigger::Base::CBasis*>* _pNode, CMapRapper* _pMap);

	template<typename T>
	T* BagicSetting(const std::wstring& _name);


	Trigger::Fork::CProcedurer* Sequence(const std::wstring& _name);
	Trigger::Fork::CChooser* Selection(const std::wstring& _name);

	template<typename T>
	T* Condition(const std::wstring& _name);
	template<typename T>
	T* Action(const std::wstring& _name);

	Trigger::Base::CBasis* Trigger_Bagic();
};

template<typename T>
inline T * CTriggerMaker::BagicSetting(const std::wstring & _name)
{
	T* pTemp = new T();
	pTemp->SetName(_name);
	pTemp->SetGameObject(m_pGameObj);
	pTemp->SetShareHouse(m_pShareHouse);
	pTemp->SetStack(m_pStack);
	pTemp->SetNodesVector(m_pTriggers);
	pTemp->SetSponSystem(m_pSponSystem);
	pTemp->SetMapRapper(m_pMapRapper);
	pTemp->PushVector(pTemp);
	return pTemp;

}

template<typename T>
inline T * CTriggerMaker::Condition(const std::wstring & _name)
{
	return BagicSetting<T>(_name);
}

template<typename T>
inline T * CTriggerMaker::Action(const std::wstring & _name)
{
	return BagicSetting<T>(_name);
}
