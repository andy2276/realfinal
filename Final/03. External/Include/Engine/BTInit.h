#pragma once
#include "BT.h"



namespace BT
{
	namespace Leaf
	{
		namespace Condition
		{
			//	체력이 여기 설정된 값보다 작은가?
			class HpLess : public BT::Virtual::CCondition
			{
			private:
				float		m_fScrHp;
			public:
				HpLess();
				virtual ~HpLess();

				//	offset : 0, pData : float(ScrHp)
				virtual void SetData(int _offset, void* pData);

				virtual BT::Return Evaluate();
			};
			//	플레이어를 바라보고있는가?
			class LookPlayer :public BT::Virtual::CCondition
			{
			public:
				LookPlayer();
				virtual ~LookPlayer();

				virtual BT::Return Evaluate();
			};
			class IsDeath :public BT::Virtual::CCondition
			{
			public:
				IsDeath();
				virtual ~IsDeath();

				virtual BT::Return Evaluate();
			};
			class SetTargetPos :public BT::Virtual::CCondition
			{
			public:
				SetTargetPos();
				virtual ~SetTargetPos();

				virtual BT::Return Evaluate();
			};
			class IsDamaged:public BT::Virtual::CCondition
			{
			public:
				IsDamaged();
				virtual ~IsDamaged();

				//	offset : 0, pData : status stack
			
				virtual void SetData(int _offset, void* pData);

				virtual BT::Return Evaluate();
			};
			class FirstIn :public BT::Virtual::CCondition
			{
			public:
				bool		m_bFirst;
			public:
				FirstIn();
				virtual ~FirstIn();

				virtual void HardReset();

				//	offset : 0, pData : bool		m_bFirst;
				virtual void SetData(int _offset, void* pData);

				virtual BT::Return Evaluate();
			};
			class IsSkillOn :public BT::Virtual::CCondition
			{
			private:
				int		m_nCoolMax;
				int		m_nCoolNow;
			public:
				IsSkillOn();
				virtual ~IsSkillOn();

				//	offset : 0, pData : int m_nCoolMax
				//	offset : 1, pData : int m_nCoolNow
				virtual void SetData(int _offset, void* pData);

				virtual BT::Return Evaluate();
			};
			class IsLessRange:public BT::Virtual::CCondition
			{
			private:
				float		m_fRange;	//	100.f
			public:
				IsLessRange();
				virtual ~IsLessRange();

				//	offset : 0, pData : float m_fRange
				virtual void SetData(int _offset, void* pData);

				virtual BT::Return Evaluate();
			};

		}
		namespace Action
		{
			//	목적방향을 바라봄
			class TurnDest :public BT::Virtual::CAction
			{
				
			public:
				TurnDest();
				virtual ~TurnDest();
				 
				virtual BT::Return Evaluate();
			};

			class TurnAngle : public BT::Virtual::CAction
			{
			private:
				float m_fAngle;
				float m_fOper;
				bool  m_bRandOper;
				float m_fCurAngle;
				bool m_bStart;
			public:
				TurnAngle();
				virtual ~TurnAngle();

				//	offset : 0, pData : float(fAngle)//	절대값
				//	offset : 1, pData : float m_fOper;
				//	offset : 2, pData : bool m_bRandOper
				virtual void SetData(int _offset, void* pData);

				virtual BT::Return Evaluate();
			};

			//	지정된 방향으로 간다 그리고 그 방향으로 몸을 튼다
			class MoveDest :public BT::Virtual::CAction
			{
			private:
				float			m_fNear;
			public:
				MoveDest();
				virtual ~MoveDest();
				//	offset : 0, pData : float			m_fNear;
				virtual void SetData(int _offset, void* pData);

				virtual BT::Return Evaluate();
			};
			class GoDest:public BT::Virtual::CAction
			{
			private:
				float			m_fNear;
			public:
				GoDest();
				virtual ~GoDest();
				//	offset : 0, pData : float			m_fNear;
				virtual void SetData(int _offset, void* pData);

				virtual BT::Return Evaluate();
			};

			class MoveDirDist :public BT::Virtual::CAction
			{
			private:
				bool		m_bMoveStart;
				Vec3		m_vStartPos;
				DIR_TYPE	m_eDirType;
				float		m_fDist;
			public:
				MoveDirDist();
				virtual ~MoveDirDist();

				//	offset : 0, pData : DIR_TYPE(Dirtype)
				//	offset : 1, pData : float(m_fDist)
				virtual void SetData(int _offset, void* pData);

				virtual BT::Return Evaluate();
			};

			class ShootSkill :public BT::Virtual::CAction
			{
			private:
				int		m_nSkillNum;
				float	m_fSkillShootTime;
				bool	m_bOnShoot;
			public:
				ShootSkill();
				virtual ~ShootSkill();

				//	offset : 0, pData : int m_nSkillNum
				//	offset : 1, pData : float shootTime
				virtual void SetData(int _offset, void* pData);

				virtual BT::Return Evaluate();
			};
			class Damaged:public BT::Virtual::CAction
			{
			private:
				float			m_fStuckDamage;
				bool			m_bOnce;
			public:
				Damaged();
				virtual ~Damaged();

				//	offset : 0, pData : float			m_fStuckDamage;
				virtual void SetData(int _offset, void* pData);

				virtual BT::Return Evaluate();
			};
			class BagicAttack :public BT::Virtual::CAction
			{
			private:
				float				m_fAttackRange;
				float				m_fAttackTime;
				float				m_fDamage;
				bool				m_bOnAttack;
			public:
				BagicAttack();
				virtual ~BagicAttack();

				//	offset : 0, pData : float				m_fAttackRange;
				//	offset : 1, pData : float				m_fAttackTime;
				virtual void SetData(int _offset, void* pData);


				virtual BT::Return Evaluate();
			};
			class SetRageState: public BT::Virtual::CAction
			{
			private:
				float				m_fRageScale;
			public:
				SetRageState();
				virtual ~SetRageState();

				//	offset : 0, pData : float				m_fRageScale;
				virtual void SetData(int _offset, void* pData);


				virtual BT::Return Evaluate();
			};
			class SetDeath : public BT::Virtual::CAction
			{
			public:
				SetDeath();
				virtual ~SetDeath();

				virtual BT::Return Evaluate();
			};
			class Stay : public BT::Virtual::CAction
			{
			private:
				float		m_fStayTime;
				float		m_fStayTimeCur;
			public:
				Stay();
				virtual ~Stay();

				//	offset : 0, pData : float				m_fStayTime;
				virtual void SetData(int _offset, void* pData);

				virtual BT::Return Evaluate();
			};
			class ReValue : public BT::Virtual::CAction
			{
			private:
				BT::Return m_eReValue;
			public:
				ReValue();
				virtual ~ReValue();

				//	offset : 0, pData : BT::Return m_eReValue;
				virtual void SetData(int _offset, void* pData);

				virtual BT::Return Evaluate();
			};
		}
	}
	namespace Clt = Collect;
	namespace Cdt = Leaf::Condition;
	namespace Act = Leaf::Action;
}

