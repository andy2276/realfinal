#pragma once
#include "Script.h"
enum class EC_START_MAP_STATE {
	Evaluate,
	Close,
	Ing,
	Open,
	Move

};


class CStartMapScript :
	public CScript
{
private:
	CGameObject*			m_pPlayer;
	Vec4					m_vPortalRect;
	Vec4					m_vRoomRect;
	bool					m_IsStart;
	EC_START_MAP_STATE		m_eState;

	float					m_fMoveTimeMax;
	float					m_fMoveTimeCur;

	float					m_fOpenTimeMax;
	float					m_fOpenTimeCur;

	bool					m_bIsIng;
	bool					m_bIngDone;
	bool					m_bOnFirst;
public:
	CStartMapScript();
	virtual ~CStartMapScript();

	void SetPlayer(CGameObject* _pPlayer) { m_pPlayer = _pPlayer; }
public:
	CLONE(CStartMapScript);

	virtual void update();
	void Reset();
};

