#pragma once
#include "Script.h"

struct Icon{
	CGameObject* pObj = nullptr;
	Vec3	vPos;
	int		skillType;
	int		num;
};

class CInventoryScript :
	public CScript
{
private:
	tResolution m_res;

	//인벤토리 아이콘 //////////////////////////////////////////////////////////////////////////////
	float m_width; //인벤토리 창 가로 크기 
	float m_height;
	float m_iconColCount;// 아이콘 행 개수
	float m_iconRowCount; // 아이콘 열 개수
	float m_iconTotalCount; // 아이콘 총 개수 
	float m_haveIconCount; //가지고있는 스킬 아이콘 개수

	float m_iconWidth; //아이콘 한개의 가로 크기
	float m_iconheight; //아이콘 한개의 가로 크기

	float m_borderThickness; //인벤토리 테두리 두께 

	Vec3 m_PosCenter; // 인벤토리 중심의 위치 

	vector<CGameObject *> m_IconArr;
	//vector<Vec3> m_vecIconPos; //아이콘 중심 위치
	vector<Vec3> m_vecIconNewPos; //아이콘 새로운 위치 
	bool * isOnInventory_Icon; // 인벤토리 아이콘이 눌렸는가 

	Icon m_strInventoryIcon[20];

	//스크롤 아이콘 //////////////////////////////////////////////////////////////////////////////
	vector<CGameObject *> m_arrScroll_Icon;
//	vector<Vec3> m_vecScroll_IconPos; //아이콘 중심 위치
	vector<Vec3> m_vecScroll_IconNewPos; //아이콘 새로운 위치 
	Vec3 m_Scroll_IonScale;
	bool * isOnScroll_Icon; // 스크롤 아이콘이 눌렸는가 
	int m_ScrollCount; //스크롤 아이콘 개수
	int m_arrScroll_ownSkill[6]; // 스크롤 아이콘이 가지고있는 스킬이 무엇인지 

	Icon m_strScrollIcon[6];

	//패시브 아이콘 //////////////////////////////////////////////////////////////////////////////
	vector<CGameObject *> m_arrPassive_Icon;
	vector<Vec3> m_vecPassive_IconPos; //아이콘 중심 위치
	vector<Vec3> m_vecPassive_IconNewPos; //아이콘 새로운 위치 
	Vec3 m_Passive_IonScale;
	

	CGameObject * m_inventoryBorder;

	float m_iconColliderWidthSize;
	float m_iconColliderHeightSize;

	bool m_isSelection; // 아이콘 위치 변경될 자리르 선택했을때 
	Vec3 m_changePos;
	CGameObject * colliderIcon; //충돌한 아이콘 

	bool m_isInvenClick;  //인벤토리 클릭
	bool m_isScrollClick; //스크롤 클릭
	int ClickIconNum;
	int IventoryNum; //인벤토리에 들어갈 현재 칸의숫자 


	const int iconCount = 8;
	Ptr<CTexture> m_arrSkill_iconTex[(UINT)PLAYER_SKILL_TYPE::END];
	Ptr<CTexture> pEmptyIcon;


	bool isOnInventory;//인벤토리 키고끄기 
	int arrSkillType[20];

	//	지민추

	Ptr<CMaterial> m_arrSkill_iconMtrl[(UINT)PLAYER_SKILL_TYPE::END];
	vector<CGameObject*> m_vecItemObjs;
	bool	m_bInvenStart;
	bool	m_bOnce;
public:
	CLONE(CInventoryScript);
	virtual void awake();
	virtual void update();
	CInventoryScript();
	virtual ~CInventoryScript();

	virtual void OnCollisionEnter(CCollider2D* _pOther);

	int getSkillType(int i) { return m_strScrollIcon[i].skillType; }


	int m_OwnSkillCount;
	void setSkillOwn();
	int getOwnSkillCount() { return m_OwnSkillCount;  }
	void setInventoryOn(bool b);
};

