#pragma once
#include "Script.h"
class CItemMove :
	public CScript
{
private:
	float m_fAngle;
	float m_fHoverMax;
	float m_fHoverCur;
	float m_fHoverOfer;
	float m_fHoverMin;

	Vec3 m_vTempPos;
	Vec3 m_vTempRot;

public:
	CItemMove();
	virtual ~CItemMove();

	void Reset()
	{
		m_fAngle = (3.f);
		m_fHoverMax = (3.f);
		m_fHoverCur=(0.f);
		m_fHoverOfer=(1.f);
		m_fHoverMin=(0.f);
	}

	CLONE(CItemMove);

	virtual void update();
};

