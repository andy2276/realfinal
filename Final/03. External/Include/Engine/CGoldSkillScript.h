#pragma once
#include "Script.h"
class CGoldSkillScript :
	public CScript
{
private:
	//스킬에 필요한 변수 : 쿨타임, 현재 사용가능한지 , 스킬사용된 후 생존시간
	float m_curTime;
	float m_coolTime;
	float m_remainCoolTime;
	bool isCanShoot;

public:
	CLONE(CGoldSkillScript);
	virtual void update();
	CGoldSkillScript();
	virtual ~CGoldSkillScript();

	void setCanShoot(bool b) { isCanShoot = b; }
	bool getCanShoot() { return isCanShoot; }
};

