#pragma once
class CGameObject;
class CMapInfo
{
private:
	BYTE	cColtype;
	BYTE	cHeight;
	BYTE	cCurObj;
	BYTE	cObjIn;	//	7	6	5	4	3	2	1	0

	CGameObject*	pSplite[3][3];
	CMapInfo*		pMapInfo[3][3];
	list< CGameObject*> listObject;
public:
	CMapInfo();
	~CMapInfo();
};

