#pragma once
#include "Script.h"


class CItemScript :
	public CScript
{
private:
	UINT m_etype;

//	static CGameObject * m_ItemUI;
	static bool isUIon;
public:
	CItemScript();
	virtual ~CItemScript();

	CLONE(CItemScript);
	//virtual void awake();
	virtual void update();

	virtual void OnCollisionEnter(CCollider2D* _pOther); // 충돌체가 처음 충돌
	//virtual void OnCollision(CCollider2D* _pOther);      // 충돌 중
	//virtual void OnCollisionExit(CCollider2D* _pOther);  // 충돌체가 서로 떨어질 때


	void init(UINT e);
	UINT getItemType() { return m_etype; }

};

