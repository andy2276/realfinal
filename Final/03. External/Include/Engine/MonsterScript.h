#pragma once
#include "Script.h"

class CMonsterScript :
	public CScript
{
private:
	int		m_iDir;
	
	CGameObject * m_effect;
	bool	m_OnEffect;//이펙트 켜진상태인가 
	Vec3	m_effectScale;

	bool    m_bEffectOn;
	bool	m_bOnce;
	float	m_curTime;
	float	m_coolTime;

	CGameObject * m_hiteffect;
	bool    m_bhitOn;

public:
	virtual void update();

	virtual void OnCollisionEnter(CCollider2D* _pOther);
	virtual void OnCollisionExit(CCollider2D* _pOther);
public:
	CLONE(CMonsterScript);

public:
	CMonsterScript();
	virtual ~CMonsterScript();
};

