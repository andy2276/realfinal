#pragma once
#include "Script.h"


namespace BT {
	class CBlackBoard;
	namespace Virtual {
		class CNode;
	}
}
class CAIScript :
	public CScript
{
private:
	std::stack< BT::Virtual::CNode*>	m_pStack;		//	이거는 객체마다
	BT::CBlackBoard*					m_pBlackBoard;	//	이거는 같은 맵의 같은 오브젝트마다
	vector<BT::Virtual::CNode*>			m_vecNodes;		//	모든 노드 모음집
	BT::Virtual::CNode*					m_pRoot;
	void*								m_pData;

	bool								m_bReSet;
public:
	CAIScript();
	virtual ~CAIScript();

public:
	virtual void Init(UINT _type, BT::CBlackBoard* _pBlack);
	virtual virtual void update();
	
	std::stack< BT::Virtual::CNode*>* GetStack() { return &m_pStack; }
	vector<BT::Virtual::CNode*>* GetNodes() { return &m_vecNodes; }
	void SetRoot(BT::Virtual::CNode* _pRoot) { m_pRoot = _pRoot; }
public:
	CLONE(CAIScript);

};

