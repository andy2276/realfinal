#pragma once
class CGameObject;
namespace BT {
	enum class EC_RETURN {
		RUNNING = -1
		,FAIL = 0
		,SUCCESS = 1
	};
	
	class CBlackBoard {
	public:
		CBlackBoard() {}
		virtual ~CBlackBoard() {};
	};
	namespace VitCls {
		class CNode {
		protected:
			int		m_nCurIdx;
			CBlackBoard* m_pBlackBoard;
			CGameObject* m_pObj;
		public:
			CNode() :m_nCurIdx(0) {};
			virtual ~CNode() {};

			virtual EC_RETURN Evaluation(void* _pData) = 0;
			void SetBlackBoard(CBlackBoard* _black) { m_pBlackBoard = _black; }
			void SetObject(CGameObject* _pObj) { m_pObj = _pObj; }
			CBlackBoard* GetBlackBoard() { return m_pBlackBoard; }
		};

		class CComposite :public CNode {
		private:
			std::vector<CNode*>			m_vecChild;
		public:
			CComposite();
			virtual ~CComposite();

			void AddChild(CNode* _node) { m_vecChild.push_back(_node); }
			std::vector<CNode*> GetChildVector() { return m_vecChild; }
			CNode* GetChild(const int& _idx) { return m_vecChild[_idx]; }

			virtual EC_RETURN Evaluation(void* _pData) = 0;
		};
		class CDecorator : public CNode {
		public:
			CNode*		m_pChild;
		public:
			CDecorator();
			virtual ~CDecorator();

			void SetChild(CNode* _node) { m_pChild = _node; }
			CNode* GetChild() { return m_pChild; }

			virtual EC_RETURN Evaluation(void* _pData) = 0;
		};
	}
	namespace Cls {
		class CSelection : public VitCls::CComposite {
		public:
			CSelection();
			virtual ~CSelection();

			virtual EC_RETURN Evaluation(void* _pData);
		};

		class CSelectionRandom : public VitCls::CComposite {
		public:
			CSelectionRandom();
			virtual ~CSelectionRandom();

			void SuffleChild();
			virtual EC_RETURN Evaluation(void* _pData);
		};

		class CSequence : public VitCls::CComposite {
		public:
			CSequence();
			virtual ~CSequence();

			virtual EC_RETURN Evaluation(void* _pData);
		};

		class CCondition : public VitCls::CDecorator {
		protected:
			std::function< EC_RETURN(void*)>	m_pFunc;
		public:
			CCondition();
			virtual ~CCondition();

			void SetFunc(std::function< EC_RETURN(void*)> _func) { m_pFunc = _func; }
			std::function< EC_RETURN(void*)> GetFunc() { return m_pFunc; }

			virtual EC_RETURN Evaluation(void* _pData);
			virtual EC_RETURN operator() (void* _pData) { return m_pFunc(_pData); }
		};

		class CForwardCondition : public CCondition {
		public:
			CForwardCondition();
			~CForwardCondition();

			virtual EC_RETURN Evaluation(void* _pData);
			virtual EC_RETURN operator() (void* _pData);
		};

		class CAction : public VitCls::CNode {
		public:
			CAction();
			virtual ~CAction();

			virtual EC_RETURN Evaluation(void* _pData);
		};

		class CFuncAction :public VitCls::CNode {
		private:
			std::function< EC_RETURN(void*)>	m_pFunc;
		public:
			CFuncAction();
			~CFuncAction();

			void SetFunc(std::function< EC_RETURN(void*)> _func) { m_pFunc = _func; }
			std::function< EC_RETURN(void*)> GetFunc() { return m_pFunc; }

			virtual EC_RETURN Evaluation(void* _pData);

			EC_RETURN operator() (void* _pData) { return m_pFunc(_pData); }
		};
	}
	/*namespace Structure {
		typedef struct TInData {
			BTvit::CNode*	pNode;
			int		nIdx;
		}tInData;
		typedef struct TTest {
			std::stack<tInData> Stack;
		};
		typedef struct StackData {
			std::stack<tInData> Stack;
			bool bOnStack;
		};
	};
*/
	class CBehaviorTree {
	private:
		//	이걸 컴퍼넌트로 넣자
		//	블랙보드를 만들어서 넣자
		VitCls::CNode*		m_pRootNode;
		void*		m_pDatas;
	public:
		CBehaviorTree();
		virtual ~CBehaviorTree();

		virtual void Init();
		virtual void Update();
	};
	namespace ComparisonFunc {
		namespace Templates {
			template<typename T>
			const bool MoreThan(const T& _dest, const T& _scr) {return (_dest >= _scr);}
			template<typename T>
			const bool BelowThan(const T& _dest, const T& _scr) { return (_dest <= _scr); }
			template<typename T>
			const bool SameThing(const T& _dest, const T& _scr) { return (_dest == _scr); }
			template<typename T>
			const bool OverThan(const T& _dest, const T& _scr) { return (_dest > _scr); }
			template<typename T>
			const bool LessThan(const T& _dest, const T& _scr) { return (_dest < _scr); }
		}
	}
}

namespace BTcls = BT::Cls;
namespace BTvit = BT::VitCls;


