#pragma once
enum class E_ASTAR_DIR :BYTE {
	LeftTop = 0
	, MidTop = 1
	, RightTop = 2
	, LeftMid = 3
	, MidMid = 4
	, RightMid = 5
	, LeftBot = 6
	, MidBot = 7
	, RightBot = 8
	, End
};
class CPathResource;
struct tPath;
class CAstar{
private:
	//	BlockMap
	//	startCoord
	//	DestCoord
	CPathResource*			m_pColliderMap;

	std::list<tPath*>		m_listOpen;
	std::list<tPath*>		m_listClose;

	vector<tPathCoord>		m_vecPath;


	tPathCoord				m_tMapStart;
	bool					m_bSearch;

	const USHORT GetHuristic(const tPathIdx& _dest,const int& _y, const int& _x);
	const USHORT GetIncreaGraph(const E_ASTAR_DIR& _eDir);
	const E_ASTAR_DIR GetDir(const float& _Z, const float& _x);
	const USHORT GetDirToGraph(const float& _z, const float& _x);
	
public:
	CAstar();
	virtual ~CAstar();

	void CreateColliderMap(const wstring& _wstrPath,
		const int& _zCnt, const int& _xCnt, const bool& _bInverse = true);

	void SetColliderMap(CPathResource*);
	//	GetPath
	void SetMapStart(const float& _z, const float& _x) { m_tMapStart = { _z,_x }; }
	tPathCoord* GetPath(const tPathCoord& _start,const tPathCoord& _dest);
	void ResetPath();

};

