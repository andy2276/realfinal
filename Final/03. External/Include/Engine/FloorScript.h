#pragma once
#include "Script.h"
class FloorScript :
	public CScript
{
private:
	bool m_bClick;
	bool m_bCanShoot;
	Vec3 m_pos;

	UINT m_skillType;
	Ptr<CSound> pSound;
public:
	CLONE(FloorScript);
	virtual void update();
	FloorScript(UINT etype);
	virtual ~FloorScript();

	bool getClick() { return m_bClick; }
	void setClick(bool b) { m_bClick = b; }
	Vec3 getPos() { return m_pos; }
	void setShoot(bool b) { m_bCanShoot = b; }
	void setSound(bool b) { if (b) { pSound->Play(1, true); } else { pSound->Stop(); } }
};

