#pragma once
#include "BT.h"

namespace BT {
	namespace Leaf {
		//namespace Observer {
		//	class CObserver : public 
		//
		//}
		namespace Special {
			
			namespace Condition {
				//	this float	if(dest < m_fRadius)return true;
				class IsLessRange :public Virtual::CCondition {
				private:
					USHORT m_nCompScrIdx;
					
				public:
					IsLessRange();
					virtual ~IsLessRange();

					void SetCompScr( const USHORT& _nCompDestIdx) {
						
						m_nCompScrIdx = _nCompDestIdx;
					}

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				//	this float	if(dest > m_fRadius)return true;
				class IsOverRange: public Virtual::CCondition {
				private:
					USHORT m_nCompScrIdx;
				public:
					IsOverRange();
					virtual ~IsOverRange();

			
					void SetCompScr( const USHORT& _nCompScrIdx) {
						m_nCompScrIdx = _nCompScrIdx;
					}

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				
				//	this float if(m_pLhs < or <=  compDest < or <=  m_pRhs)
				class IsInRange : public Virtual::CCondition {
				private:
					USHORT m_nLhsSrcIdx;
					USHORT m_nRhsSrcIdx;
					
				public:
					IsInRange();
					virtual ~IsInRange();

					
					void SetRLScr(const USHORT& _nLhsSrcIdx, const USHORT& _nRhsSrcIdx) {
						m_nLhsSrcIdx = _nLhsSrcIdx;
						m_nRhsSrcIdx = _nRhsSrcIdx;
					}

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				class IsOnCoolTime :public Virtual::CCondition {
				private:
					USHORT m_nMaxCompDestIdx;
					USHORT m_nNowCompDestIdx;
				public:
					IsOnCoolTime();
					virtual ~IsOnCoolTime();

					void SetCompDest(const USHORT& _nMaxCompDestIdx, const USHORT& _nNowCompDestIdx) {
						m_nMaxCompDestIdx = _nMaxCompDestIdx;
						m_nNowCompDestIdx = _nNowCompDestIdx;
					}

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				class IsOffCoolTime :public Virtual::CCondition {
				private:
					USHORT m_nMaxCompDestIdx;
					USHORT m_nNowCompDestIdx;
				public:
					IsOffCoolTime();
					virtual ~IsOffCoolTime();

					void SetCompDest(const USHORT& _nMaxCompDestIdx, const USHORT& _nNowCompDestIdx) {
						m_nMaxCompDestIdx = _nMaxCompDestIdx;
						m_nNowCompDestIdx = _nNowCompDestIdx;
					}

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				class IsOverDot : public Virtual::CCondition {
				private:
					float m_fRadius;
					Vec3 m_vScrDot;
					USHORT m_nCompDestIdx;
				public:
					IsOverDot();
					virtual ~IsOverDot();

					void SetRadius(const float& _fRadius) { m_fRadius = _fRadius; }
					void SetPoint(const Vec3& _dot) { m_vScrDot = _dot; }
					void SetCompDestIdx(USHORT _idx) { m_nCompDestIdx = _idx; }

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				class Recorder : public Virtual::CCondition {
				private:
					BT::Return m_eReturn;
				public:
					Recorder();
					virtual ~Recorder();

					void SetReturn(BT::Return _eReturn) {m_eReturn = _eReturn;}

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				class IsLookAtPlayer : public Virtual::CCondition {
				public:
					IsLookAtPlayer();
					virtual ~IsLookAtPlayer();

					//	수정해야함
					virtual BT::Return Screening(const Return& _return);
					virtual BT::Return Evaluation(void* _pData);
				};
				class IsOverFirstArea : public Virtual::CCondition {
				public:
					IsOverFirstArea();
					virtual ~IsOverFirstArea();

					//	수정해야함
					virtual BT::Return Screening(const Return& _return);
					virtual BT::Return Evaluation(void* _pData);
				};
				
			}
			namespace Action {
				//	이거 타겟포즈에다가 설정해주는녀석임 여기 설정한 녀석으로 타겟관련된것들 설정됨
				class SettingTarget :public BT::Virtual::CSingularAction {
				public:
					SettingTarget();
					virtual ~SettingTarget();

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				

				//	정해진 방향대로 그냥 움직이는것, 목적지가 없음	단순액션(non condition)
				class MoveToDir :public BT::Virtual::CSingularAction {
				private:
					DIR_TYPE m_eDirType;
					float m_fDist;
					float m_fCurDist;
				public:
					MoveToDir();
					virtual ~MoveToDir();

					void SetDirType(const DIR_TYPE& _type) { m_eDirType = _type; }
					void SetDistance(float _dist) { m_fDist = _dist; }

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				//	정해진 위치로 그냥 움직이는것, 목적지가 존재	복잡한액션(existence condition)	
				class MoveToDest : public BT::Virtual::CComplexAction {
				private:
					Vec2 m_v2ErrRange;

					USHORT m_nValueTypeIdx;
					USHORT m_nCompDestIdx;
				public:
					MoveToDest();
					virtual ~MoveToDest();

					void SetCompDest(const USHORT& _nValueTypeIdx, const USHORT& _nCompDestIdx) {
						m_nValueTypeIdx = _nValueTypeIdx;
						m_nCompDestIdx = _nCompDestIdx;
					}
					void SetErrRange(const Vec2& _range) { m_v2ErrRange = _range; }


					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				class MoveToStep : public BT::Virtual::CComplexAction {
				private:
					float m_fStep;
				public:
					MoveToStep();
					virtual ~MoveToStep();

					void SetStep(const float& _fStep) {
						m_fStep = _fStep;
					}
					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				class MoveBackToPlayer :public BT::Virtual::CComplexAction {
				private:
					float m_fMaxStep;
					float m_fCurStep;
				public:
					MoveBackToPlayer();
					virtual ~MoveBackToPlayer();
					void SetStep(const float& _fStep) {
						m_fMaxStep = _fStep;
					}
					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				

				//	정해진 방향대로 그냥 회전, 목적지가 없음	단순액션(non condition)
				class TurnToAngle : public BT::Virtual::CSingularAction {
				private:
					float m_fAngle;
					float m_fCurAngle;
					bool m_bTurnAnglePlusMinus;
					float m_fTurnAngle;
				public:
					TurnToAngle();
					virtual ~TurnToAngle();

					void SetAngle(float _angle) { m_fAngle = _angle; }

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				//	정해진 방향대로 회전, 목적지가 존재			복잡한액션(existence condition)	
				class TurnToPlayer : public Virtual::CComplexAction {
				public:
					TurnToPlayer();
					virtual ~TurnToPlayer();

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				class TurnToDestAngle : public Virtual::CComplexAction {



				};
				class UseSkill : public Virtual::CSingularAction {
				private:
					UINT m_nSkillNum;
				public:
					UseSkill();
					virtual ~UseSkill();

					void SetSkillNum(UINT _num) { m_nSkillNum = _num; }

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				class ExcuteAnimation : public Virtual::CSingularAction {
				private:
					UINT m_nAnimIdx;

				public:
					ExcuteAnimation();
					virtual ~ExcuteAnimation();

					void SetAnimIdx(const UINT& _idx) {
						m_nAnimIdx = _idx;
					}

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				class ExcuteStay :public Virtual::CComplexAction {
				private:
					float m_fStayTime;
					float m_fCurTime;
				public:
					ExcuteStay();
					virtual ~ExcuteStay();

					void SetStayTime(const float& _fStayTime) {
						m_fStayTime = _fStayTime;
					}

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				class AnimationStop : public Virtual::CComplexAction{
				public:
					AnimationStop();
					virtual ~AnimationStop();

					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				class HpRecovery : public Virtual::CComplexAction {
				private:
					float m_fCurRecovery;
					float m_fMaxRecovery;
				public:
					HpRecovery();
					virtual ~HpRecovery();

					void SetRecovery(float _recover) {
						m_fMaxRecovery = m_fCurRecovery;
					}
					//	수정
					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
				class Escape : public Virtual::CComplexAction {
				public:
					Escape();
					virtual ~Escape();
					
					//	수정
					virtual Return Screening(const Return& _return);
					virtual Return Evaluation(void* _pData);
				};
			}
		}
	}

}