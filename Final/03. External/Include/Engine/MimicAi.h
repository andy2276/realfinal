#pragma once
#include "Script.h"
enum class EC_MIMIC_STATE {
	Evaluate					=	0
	, Turn						=	1
	, Move						=	2
	, MoveBack					=	3
	, MonsterMode				=	4
	, BoxMode					=	5
	, Idle						=	6
	, Attack					=	7
	, WaitPickItem				=	8
	, Open						=	9
	, Close						=	10
	, Closing					=	11
	, DeActive					=	12
	, Death						=	13
	, Damaged					=	14
	
};
class CMimicAi :
	public CScript
{
private:
	CGameObject*				m_pPlayer;
	EC_MIMIC_STATE				m_eState;
	bool						m_bIsMonster;
	bool						m_bIsAttackMode;
	bool						m_bIsOpened;
	bool						m_bPlayerGetItem;
	bool						m_bDoesntItem;




	//	Attack!
	bool						m_fAttackOn;
	float						m_fAttckAngle;
	float						m_fAttackTimeMax;
	float						m_fAttackTimeCur;
	float						m_fAttackPossibleTime;
	float						m_fAttackRange;
	float						m_fAttackDamage;

	//	Death
	float						m_fDeathTimeCur;
	float						m_fDeathTimeMax;


	//	Frame Culling
	float						m_fTicMax;
	float						m_fTic;
	float						m_fToc;


	bool						m_bPreAnimMove;
	bool						m_bPreIdle;

	bool						m_bDeathMode;


public:
	CMimicAi();
	virtual ~CMimicAi();

	CLONE(CMimicAi);
public:
	virtual void update();

	void SetPlayer(CGameObject* _pPlayer) { m_pPlayer = _pPlayer; }

	
	virtual void OnCollision(CCollider2D * _pOther);

	void Reset(const Vec3 & _pos, const Vec3 & _rot);
	void SetBox() { m_bIsMonster = false; }
};

