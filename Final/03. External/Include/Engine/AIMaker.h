#pragma once
#include "BT.h"
#include "BTSpecial.h"

class CAIMaker
{
private:
	BT::Virtual::CNode*					m_pRoot;
	CGameObject*						m_pGameObj;
	BT::CBlackBoard*					m_pBlackBoard;
	std::stack<BT::Virtual::CNode*>*	m_pStack;
	std::vector<BT::Virtual::CNode*>*	m_pNodes;
public:
	CAIMaker();
	~CAIMaker();

	void SetGameObject(CGameObject* _pObj) { m_pGameObj = _pObj; }
	void SetBlackBoard(BT::CBlackBoard* _pBlackBoard) { m_pBlackBoard = _pBlackBoard; }
	void SetStack(std::stack<BT::Virtual::CNode*>* _stack) { m_pStack = _stack; }
	void SetNodeVector(std::vector<BT::Virtual::CNode*>* _pNodes) { m_pNodes = _pNodes; }

	void Init(CGameObject* _pObj, BT::CBlackBoard* _pBlackBoard, std::stack<BT::Virtual::CNode*>* _stack, std::vector<BT::Virtual::CNode*>* _pNodes) {
		SetGameObject(_pObj);
		SetBlackBoard(_pBlackBoard);
		SetStack(_stack);
		SetNodeVector(_pNodes);
	}
	void __StartBTMake__(const wstring& _msg) {
		//	시작 셋팅을 해준다
	}
	template<typename T>
	T* Root(const wstring& _name);

	BT::Flow::CSequence* Sequence(const wstring& _name);
	BT::Flow::CSelection* Selection(const wstring& _name);


	//	이름,고정비교값,추적비교값(STATUS_FLOAT로 검색),비교연산자 함수(BT::Leaf::ComparisonData::TYPE 로 검색) 
	BT::Leaf::CComparison_Float* Comparison_Float(
		const wstring& _name								//	이름
		, const float& _scrValue							//	고정 비교값
		, const USHORT& _destIdx							//	게임오브젝트에서 들고올 값
		, const BT::Leaf::ComparisonData::TYPE& _compType	//	비교할 연산자 함수
	);

	//	이름,고정비교값,추적비교값(STATUS_FLOAT로 검색),비교연산자 함수(BT::Leaf::ComparisonData::TYPE 로 검색) 
	BT::Leaf::CComparison_Int* Comparison_Int(
		const wstring& _name								//	이름
		, const int& _scrValue							//	고정 비교값
		, const USHORT& _destIdx							//	게임오브젝트에서 들고올 값
		, const BT::Leaf::ComparisonData::TYPE& _compType	//	비교할 연산자 함수
	);
	BT::Flow::CDecorate* Decorate(const wstring& _name, BT::Virtual::CCondition* _condition, BT::Virtual::CSingularAction* _singleAction);


	template<typename T>
	T* Condition(const wstring& _name);
	template<typename T>
	T* Action(const wstring& _name);


	void SettingDepth(BT::Virtual::CNode* _pRoot) { int depth = 0; m_pRoot->DepthTest(depth); }
	void __EndBTMake__() {
		SettingDepth(m_pRoot);//	끝마치는 셋팅을 해준다.
	}
	BT::Virtual::CNode* AI_Skeleton();
	BT::Virtual::CNode* AI_Test();
	BT::Virtual::CNode* AI_Test2();
	BT::Virtual::CNode* AI_Bagic0();
	BT::Virtual::CNode* AI_Stone();

};

template<typename T>
inline T * CAIMaker::Root(const wstring & _name)
{
	T* pTemp = new T();
	pTemp->SetName(_name);
	pTemp->SetBlackBoard(m_pBlackBoard);
	pTemp->SetStack(m_pStack);
	pTemp->SetGameObject(m_pGameObj);
	pTemp->SetDataVoid(nullptr);
	m_pRoot = pTemp;
	pTemp->SetIndex(m_pNodes->size());
	m_pNodes->push_back(pTemp);
	return pTemp;
}

template<typename T>
inline T * CAIMaker::Condition(const wstring & _name)
{
	T* pTemp = new T();
	pTemp->SetName(_name);
	pTemp->SetBlackBoard(m_pBlackBoard);
	pTemp->SetStack(m_pStack);
	pTemp->SetGameObject(m_pGameObj);
	pTemp->SetDataVoid(nullptr);
	pTemp->SetIndex(m_pNodes->size());
	m_pNodes->push_back(pTemp);
	return pTemp;
}

template<typename T>
inline T * CAIMaker::Action(const wstring & _name)
{
	T* pTemp = new T();
	pTemp->SetName(_name);
	pTemp->SetBlackBoard(m_pBlackBoard);
	pTemp->SetStack(m_pStack);
	pTemp->SetGameObject(m_pGameObj);
	pTemp->SetDataVoid(nullptr);
	pTemp->SetIndex(m_pNodes->size());
	m_pNodes->push_back(pTemp);
	return pTemp;
}
