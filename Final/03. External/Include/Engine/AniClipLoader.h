#pragma once
class CMesh;
class CAniClipLoader{
private:
	wstring m_strName;	//	이것의 이름.
//	[0: Idle]
//	[1:Run]
//	[2:BackWalk]
//	[3:LeftWalk]
//	[4:RightWalk]
//	[5:Attack]
//	[6:Skill0]
//	[7:Skill1]
//	[8:Skill2]
//	[9:FrontLie]
//	[10:BackLie]

	tMTAnimClip*	m_arrpAniClips[(UINT)EC_ANI_CLIP::End];
	CMesh*			m_pMesh;
public:
	CAniClipLoader();
	~CAniClipLoader();

	tMTAnimClip* GetAniClip(const EC_ANI_CLIP& clip) { return m_arrpAniClips[(UINT)clip]; }
	void SetAniClip(const EC_ANI_CLIP& clip, CMesh* _pMesh, tMTAnimClip* data);
	tMTAnimClip* GetClip(const EC_ANI_CLIP& clip) { return m_arrpAniClips[(UINT)clip]; };


	//	씬에서 매쉬를 만든다.그리고 매쉬를 얻어와주고
	//	씬매니저에서 클립을 나눠준다.
	//	클립을 여러개 나눠주고 컨트롤 해준다.
};

