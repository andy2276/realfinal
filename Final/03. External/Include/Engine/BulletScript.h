#pragma once

#include "Script.h"

class CBulletScript :
	public CScript
{
	UINT m_skillType;
	UINT m_idx;

	float m_curTime; //현재 누적된 시간 
	float m_coolTime; //쿨타임 
	float m_remainCoolTime;
	bool m_isCanShoot;// 사용가능한지 
	
	float m_LifeTime;//생존 시간 
	
	//마우스클릭했을때부터 손을 떼기 전까지 게이지가 올라가고, 그만큼의 높이로 발사된다. 
	bool m_isClick;
	float m_power;
	bool m_isAway; // 총알이 발사되어 나갔는지 


	Vec3 m_Pos;
	Vec3 m_arrivePos; //도착위치 
	Vec3 m_Dir;
	Vec3 m_DirUP;
	Vec3 m_DirLook;

	Vec3 m_startPos; //메테오 시작위치
	float m_Radius;

	Ptr<CMeshData> pMeshData; //fbx  


	vector<CGameObject *> m_Bullets; //총알 벡터
	vector<CGameObject *> m_Particles; //총알 벡터


	float m_aliveCount; //생존개수
	int bulletCount; //총알 전체 개수 


	CGameObject * m_firebulletUI;
	Vec3		  m_fireBulletUI_scale;
	Vec3		  m_fireBulletUI_pos;

	float		  m_damage;
	
	bool		  m_bHasFloor;


	Ptr<CSound> pSound_Collider;
	Ptr<CSound> pSound_Shoot;

public:
	CLONE(CBulletScript);
	virtual void update();
	virtual void OnCollisionEnter(CCollider2D* _pOther);
	virtual void awake();

public:
	CBulletScript();
	CBulletScript(UINT etype);
	virtual ~CBulletScript();

	void shoot();
	void init(UINT etype);
	bool isCanShoot() { return m_isCanShoot; }
	void setCanShoot(bool b) { m_isCanShoot = b; }
	void setDir(Vec3 v) { m_Dir = v; }
	float getDamage() { return m_damage; }
	
	void setPos(Vec3 v) { m_Pos = v; }
	void setHasFloor(bool b) { m_bHasFloor = b; }

	void setIdx(UINT i) { m_idx = i; }
};

