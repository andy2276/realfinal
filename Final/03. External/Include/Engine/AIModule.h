#pragma once
//namespace BT {
//	namespace Virtual {
//		class CNode;
//	}
//	class CBlackBoard;
//}
//#include "BT.h"
//#include "BTSpecial.h"
//#include "StatusComponent.h"
//
//enum class EC_AI_TYPE {
//	MONSTER_BASIC
//};
//
//class CBTMaker {
//private:
//	BT::Virtual::CNode*					m_pRoot;
//	CGameObject*						m_pGameObj;
//	BT::CBlackBoard*					m_pBlackBoard;
//	std::stack<BT::Virtual::CNode*>*	m_pStack;
//	std::vector<BT::Virtual::CNode*>*	m_pNodes;
//public:
//	CBTMaker() {};
//	~CBTMaker() {};
//
//
//	void SetGameObject(CGameObject* _pObj) { m_pGameObj = _pObj; }
//	void SetBlackBoard(BT::CBlackBoard* _pBlackBoard) { m_pBlackBoard = _pBlackBoard; }
//	void SetStack(std::stack<BT::Virtual::CNode*>* _stack) { m_pStack = _stack; }
//	void SetNodeVector(std::vector<BT::Virtual::CNode*>* _pNodes) { m_pNodes = _pNodes; }
//
//	void Init(CGameObject* _pObj, BT::CBlackBoard* _pBlackBoard, std::stack<BT::Virtual::CNode*>* _stack, std::vector<BT::Virtual::CNode*>* _pNodes) {
//		SetGameObject(_pObj);
//		SetBlackBoard(_pBlackBoard);
//		SetStack(_stack);
//		SetNodeVector(_pNodes);
//	}
//	void __StartBTMake__(const wstring& _msg) {
//		//	시작 셋팅을 해준다
//	}
//
//	template<typename T>
//	T* Root(const wstring& _name);
//
//	BT::Flow::CSequence* Sequence(const wstring& _name);
//	BT::Flow::CSelection* Selection(const wstring& _name);
//	//BT::Flow::CSelection* SelectionRandom(const wstring& _name) {
//	//	BT::Flow::CSelection* pTemp = new BT::Flow::CSelection();
//	//	pTemp->SetName(_name);
//	//	pTemp->SetBlackBoard(m_pBlackBoard);
//	//	pTemp->SetStack(m_pStack);
//	//	pTemp->SetGameObject(m_pGameObj);
//	//	pTemp->SetDataVoid(nullptr);
//	//	return pTemp;
//	//}
//	BT::Leaf::CComparison_Float* Comparison_Float(
//		const wstring& _name								//	이름
//		, const float& _scrValue							//	고정 비교값
//		, const USHORT& _destIdx							//	게임오브젝트에서 들고올 값
//		, const BT::Leaf::ComparisonData::TYPE& _compType	//	비교할 연산자 함수
//	);
//	BT::Leaf::CComparison_Int* Comparison_Int(
//		const wstring& _name								//	이름
//		, const int& _scrValue							//	고정 비교값
//		, const USHORT& _destIdx							//	게임오브젝트에서 들고올 값
//		, const BT::Leaf::ComparisonData::TYPE& _compType	//	비교할 연산자 함수
//	);
//	template<typename T>
//	T* Condition(const wstring& _name);
//	template<typename T>
//	T* Action(const wstring& _name);
//
//	void SettingDepth(BT::Virtual::CNode* _pRoot) {int depth = 0;m_pRoot->DepthTest(depth);}
//	void __EndBTMake__() {SettingDepth(m_pRoot);//	끝마치는 셋팅을 해준다.}
//};
//
//
//
////class CAIModule{
////private:
////	BT::Virtual::CNode* m_pRootNode;
////	BT::Virtual::CNode* m_pCurNode;
////
////public:
////	CAIModule() {};
////	virtual ~CAIModule() {};
//
//	//	이거없어지면 큰일남
//	//void Update() {
//	//	BT::Virtual::CNode* pTemp = nullptr;
//	//	BT::Return revalue = BT::Return::Fail;
//	//	int maxRoof;	//	이거는 한번 init할때 전체 개수를 확인하는 용도.
//	//	//	깊이를 기록할겸 전체 개수랑 그런것들도 모두 백터에 넣어놓자.
//	//	for (int i = 0; !m_pStack->empty(); ++i) {
//	//		pTemp = m_pStack->top();
//	//		m_pStack->pop();
//	//		revalue = pTemp->Screening(revalue);
//	//		switch (revalue) {
//	//		case BT::Return::Running:
//	//			m_pStack->push(pTemp);
//	//			return;
//	//		case BT::Return::Fail: break;
//	//		case BT::Return::Success: break;
//	//		}
//	//	}
//	//}
//	//void test() {
//	//	CBTMaker m;
//	//	m.__StartBTMake__(L"makeTestStart");
//	//
//	//	BT::Flow::CSelection* root = m.Root<BT::Flow::CSelection>(L"root");
//	//	BT::Flow::CDecorate* lesshp20 = m.Condition<BT::Flow::CDecorate>(L"DecorateLessHp20");
//	//	BT::Leaf::CComparison_Float* cdtLesshp20 = m.Condition<BT::Leaf::CComparison_Float>(L"ifLessHp20");
//	//		cdtLesshp20->SetCompDest((USHORT)STATUS_TYPE::FLOAT, (USHORT)STATUS_FLOAT::HP);
//	//		cdtLesshp20->SetCompFunc(BT::Leaf::ComparisonData::TYPE::LessThan);
//	//		cdtLesshp20->SetCompScr(20.0f);
//	//	BT::Flow::CSequence* lessSequence = m.Sequence(L"LessSequenece0");
//	//	
//	//
//	//
//	//}
//
////};
//
//template<typename T>
//inline T * CBTMaker::Root(const wstring & _name)
//{
//	T* pTemp = new T();
//	pTemp->SetName(_name);
//	pTemp->SetBlackBoard(m_pBlackBoard);
//	pTemp->SetStack(m_pStack);
//	pTemp->SetGameObject(m_pGameObj);
//	pTemp->SetDataVoid(nullptr);
//	m_pRoot = pTemp;
//	pTemp->SetIndex(m_pNodes->size());
//	m_pNodes->push_back(pTemp);
//	return pTemp;
//};
//
//template<typename T>
//inline T * CBTMaker::Condition(const wstring & _name)
//{
//	T* pTemp = new T();
//	pTemp->SetName(_name);
//	pTemp->SetBlackBoard(m_pBlackBoard);
//	pTemp->SetStack(m_pStack);
//	pTemp->SetGameObject(m_pGameObj);
//	pTemp->SetDataVoid(nullptr);
//	pTemp->SetIndex(m_pNodes->size());
//	m_pNodes->push_back(pTemp);
//	return pTemp;
//};
//
//template<typename T>
//inline T * CBTMaker::Action(const wstring & _name)
//{
//	T* pTemp = new T();
//	pTemp->SetName(_name);
//	pTemp->SetBlackBoard(m_pBlackBoard);
//	pTemp->SetStack(m_pStack);
//	pTemp->SetGameObject(m_pGameObj);
//	pTemp->SetDataVoid(nullptr);
//	pTemp->SetIndex(m_pNodes->size());
//	m_pNodes->push_back(pTemp);
//	return pTemp;
//
//};
