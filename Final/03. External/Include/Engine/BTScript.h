#pragma once
#include "Script.h"
namespace BT {
	class CBlackBoard;
	namespace VitCls {
		class CNode;
	}
}
class CBTScript :public CScript{
private:
	BT::VitCls::CNode* m_pRoot;
	double m_dTicTime;
public:
	void SetBehaviorTree(const UINT _BT_Type, BT::CBlackBoard* _blackBorad);

	virtual void update();
public:
	CLONE(CBTScript);

public:
	CBTScript();
	virtual ~CBTScript();

private:
	BT::VitCls::CNode* GetMonsterBT_Level0();
	BT::VitCls::CNode* GetMonsterBT_Level1();
	BT::VitCls::CNode* GetMonsterBT_Level2();
	BT::VitCls::CNode* GetMonsterBT_Level3();
	BT::VitCls::CNode* GetMonsterBT_Level4();
	BT::VitCls::CNode* GetMonsterBT_Level5();
	BT::VitCls::CNode* GetMonsterBT_Level6();
	BT::VitCls::CNode* GetMonsterBT_Level7();
};

