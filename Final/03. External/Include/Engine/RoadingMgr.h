#pragma once

class CGameObject;
class CMeshData;
namespace BT {
	class CBlackBoard;
}

class CRoadingMgr
{
private:
	map<wstring, CGameObject*> m_mapOrignObj;
	CMeshData* m_pMonsterMeshData[(UINT)EC_MOSTER_TYPE::End];
	BT::CBlackBoard* m_pBlackBoard[(UINT)EC_MOSTER_TYPE::End];
public:
	CRoadingMgr();
	~CRoadingMgr();

	//	로딩씬에 저장되어있고 이녀석은 그냥 데이터를 쉽게 들고오게 해주는 역할을 할것이다.
	void AddLoadingObj( CGameObject* _pObj);
	CGameObject* GetOriginObj(const wstring& _name);
	void AllDeActive();

	void SetMonsterMeshData(EC_MOSTER_TYPE _type, CMeshData* _data);
	//	외부에서 블랙보드 설정해주자.
	void SetBlackBoard(EC_MOSTER_TYPE _type, BT::CBlackBoard* _pBlack);

	CGameObject* MonsterMaker(EC_MOSTER_TYPE _type, const Vec3& _initPos);
};

