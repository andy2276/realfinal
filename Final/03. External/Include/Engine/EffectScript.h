#pragma once
#include "Script.h"
class CEffectScript :
	public CScript
{
private:
	float m_fMinScale;
	float m_fCurScale;
	float m_fMaxScale;
	float m_fScaleSpeed;
	float m_fMaxLifeTime;
	float m_fCurLifeTime;
public:
	CEffectScript();
	virtual ~CEffectScript();

	CLONE(CEffectScript);
	virtual void update();

	static CGameObject* CreateFireBurn(const Vec3& _pos, const Vec3& _scale);
	static CGameObject* CreateFireBurn(const Vec3& _pos, float _minScale,float _startScale,float _maxScale,float _speed,float _lifeTime);

	void SetScales(float _minScale, float _startScale, float _maxScale, float _speed, float _lifeTime) 
	{
		m_fMinScale = _minScale;
		m_fCurScale = _startScale;
		m_fMaxScale = _maxScale;
		m_fScaleSpeed = _speed;
		m_fMaxLifeTime = _lifeTime;
		m_fCurLifeTime = 0.f;
	};
};

