#pragma once
#include "Script.h"
class CSkillElementDir;

class CSkillStoneShower :
	public CScript
{
private:
	int							m_nBulletCnt;
	vector<CGameObject*>		m_vecBullet;
	vector<CSkillElementDir*>	m_vecBulletScript;
	int							m_nCurBullet;
	wstring						m_wstrLayer;
	CScene*						m_pCurScene;
	bool						m_bInit;
public:
	CSkillStoneShower();
	virtual ~CSkillStoneShower();

	

	void Init(CScene* _pScene,const wstring& _layer);
	void Shoot();		//	이걸 쓰면 여기있는 저 불릿들이 내려옴.
	void Shoot(const Vec3& _pos,const Vec3 _rot,const Vec3& _dir);		//	

	virtual void update();

	void CreateStartEffect();


	CLONE(CSkillStoneShower);
};

