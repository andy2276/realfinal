#ifndef _FUNC
#define _FUNC

#include "value.fx"

tLightColor CalLight(int _iLightIdx, float3 _vViewNormal, float3 _vViewPos)
{
    tLightColor tCol = (tLightColor) 0.f;
    
      
    float3 vViewLightDir = (float3) 0.f;
    float fDiffPow = 0.f;
    float fSpecPow = 0.f;
    float fRatio = 1.f;
    
    // Directional Light
    if (g_Light3D[_iLightIdx].iLight3DType == 0)
    {        
        // 광원의 방향   
        vViewLightDir = normalize(mul(float4(g_Light3D[_iLightIdx].vLightDir.xyz, 0.f), g_matView).xyz);
        fDiffPow = saturate(dot(-vViewLightDir, _vViewNormal));      
    }
    // Point Light
    else if (g_Light3D[_iLightIdx].iLight3DType == 1)
    {
        float3 vViewLightPos = mul(float4(g_Light3D[_iLightIdx].vLightPos.xyz, 1.f), g_matView).xyz;        
        vViewLightDir = normalize(_vViewPos - vViewLightPos);
        
        fDiffPow = saturate(dot(-vViewLightDir, _vViewNormal));
        
        // Ratio 계산
        float fDistance = distance(_vViewPos, vViewLightPos);
        if (0.f == g_Light3D[_iLightIdx].fRange)
            fRatio = 0.f;
        else
            fRatio = saturate(1.f - fDistance / g_Light3D[_iLightIdx].fRange);        
    }
    // Spot Light
    else
    {
        
    }    
    
    // 반사 방향
    float3 vReflect = normalize(vViewLightDir + 2.f * (dot(-vViewLightDir, _vViewNormal) * _vViewNormal));    
    float3 vEye = normalize(_vViewPos);
    fSpecPow = saturate(dot(-vEye, vReflect));    
    fSpecPow = pow(fSpecPow, 10.f);
 
    tCol.vDiff = fDiffPow * g_Light3D[_iLightIdx].tCol.vDiff * fRatio;
    tCol.vSpec = fSpecPow * g_Light3D[_iLightIdx].tCol.vSpec * fRatio;
    tCol.vAmb = g_Light3D[_iLightIdx].tCol.vAmb;
    
    return tCol;
}


static float gaussian5x5[25] =
{
    0.003765, 0.015019, 0.023792, 0.015019, 0.003765,
    0.015019, 0.059912, 0.094907, 0.059912, 0.015019,
    0.023792, 0.094907, 0.150342, 0.094907, 0.023792,
    0.015019, 0.059912, 0.094907, 0.059912, 0.015019,
    0.003765, 0.015019, 0.023792, 0.015019, 0.003765,
};

static float gaussian3x3[9] =
{
    0.035f, 0.16f, 0.035f,
    0.16f, 0.22f, 0.16f,
    0.035f, 0.16f, 0.035f,
};

float4 gaussian3x3Sample(in int2 _uv, in RWTexture2D<float4> _tex)
{
    float4 fOut = (float4) 0.f;
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            fOut += _tex[_uv + int2(i - 1, j - 1)] * gaussian3x3[i * 3 + j];
        }
    }
    return fOut;
}


float gaussian5x5Sample(in int2 _uv, in Texture2D _tex)
{
    float4 fOut = (float4) 0.f;
    for (int i = 0; i < 5; ++i)
    {
        for (int j = 0; j < 5; ++j)
        {
            fOut += _tex[_uv + int2(i - 2, j - 2)] * gaussian5x5[i * 5 + j];
        }
    }
    return fOut.x;
}


struct tSkinningInfo
{
    float3 vPos;
    float3 vTangent;
    float3 vBinormal;
    float3 vNormal;
};

matrix GetBoneMat(int _iBoneIdx, int _iRowIdx)
{
    return g_arrFinalBoneMat[(g_int_1 * _iRowIdx) + _iBoneIdx];
}

void Skinning(inout float3 _vPos, inout float3 _vTangent, inout float3 _vBinormal, inout float3 _vNormal
                        , inout float4 _vWeight, inout float4 _vIndices
                        , int _iRowIdx)
{
    tSkinningInfo info = (tSkinningInfo) 0.f;

    for (int i = 0; i < 4; ++i)
    {
        if (0.f == _vWeight[i])
            continue;

        matrix matBone = GetBoneMat((int) _vIndices[i], _iRowIdx);
        
        info.vPos += (mul(float4(_vPos, 1.f), matBone) * _vWeight[i]).xyz;
        info.vTangent += (mul(float4(_vTangent, 0.f), matBone) * _vWeight[i]).xyz;
        info.vBinormal += (mul(float4(_vBinormal, 0.f), matBone) * _vWeight[i]).xyz;
        info.vNormal += (mul(float4(_vNormal, 0.f), matBone) * _vWeight[i]).xyz;
    }
    
    _vPos = info.vPos;
    _vTangent = normalize(info.vTangent);
    _vBinormal = normalize(info.vBinormal);
    _vNormal = normalize(info.vNormal);
}


void Skinning(inout float3 _vPos, inout float4 _vWeight, inout float4 _vIndices, int _iRowIdx)
{
    tSkinningInfo info = (tSkinningInfo) 0.f;

    for (int i = 0; i < 4; ++i)
    {
        if (0.f == _vWeight[i])
            continue;

        matrix matBone = GetBoneMat((int) _vIndices[i], _iRowIdx);

        info.vPos += (mul(float4(_vPos, 1.f), matBone) * _vWeight[i]).xyz;
    }
    
    _vPos = info.vPos;
}

bool FloatEqual(float _value1, float _value2, float _epsilon)
{
    float fMin = min(_value1, _value2);
    float fMax = max(_value1, _value2);
    if (abs(fMax - fMin) < _epsilon)
        return true;
    else
        return false;
    
}
float3 srgb2Lin(float3 c)
{
    return c * c;
}
float3 lin2Srgb(float3 c)
{
    return sqrt(c);
}

float Hash12n(float2 p)
{
    p = frac(p * float2(5.3987f, 5.4421f));
    p += dot(p.yx, p.xy + float2(21.5351f, 14.3137f));
    return frac(p.x * p.y * 95.4307f);
}
float RandomLine(float3 vec)
{
    float random = dot(vec, float3(12.9898, 78.233, 37.719));
    random = frac(random);
    return random;
}
float RandomNoise(float3 vec)
{
    float random = dot(vec, float3(12.9898, 78.233, 37.719));
    random = frac(random * 143758.5453);
    return random;
}
float Random3dTo1d(float3 value, float3 dotDir = float3(12.9898, 78.233, 37.719))
{
    //make value smaller to avoid artefacts
    float3 smallValue = sin(value);
    //get scalar value from 3d vector
    float Randomom = dot(smallValue, dotDir);
    //make value more Randomom by making it bigger and then taking the factional part
    Randomom = frac(sin(Randomom) * 143758.5453);
    return Randomom;
}

float Random2dTo1d(float2 value, float2 dotDir = float2(12.9898, 78.233))
{
    float2 smallValue = sin(value);
    float Randomom = dot(smallValue, dotDir);
    Randomom = frac(sin(Randomom) * 143758.5453);
    return Randomom;
}

float Random1dTo1d(float3 value, float mutator = 0.546f)
{
    float Randomom = frac(sin(value + mutator) * 143758.5453f);
    return Randomom;
}

//to 2d functions

float2 Random3dTo2d(float3 value)
{
    return float2(
        Random3dTo1d(value, float3(12.989f, 78.233, 37.719f)),
        Random3dTo1d(value, float3(39.346f, 11.135, 83.155f))
    );
}

float2 Random2dTo2d(float2 value)
{
    return float2(
        Random2dTo1d(value, float2(12.989f, 78.233f)),
        Random2dTo1d(value, float2(39.346f, 11.135f))
    );
}

float2 Random1dTo2d(float value)
{
    return float2(
        Random2dTo1d(value, 3.9812f),
        Random2dTo1d(value, 7.1536f)
    );
}

//to 3d functions

float3 Random3dTo3d(float3 value)
{
    return float3(
        Random3dTo1d(value, float3(12.989f, 78.233f, 37.719f)),
        Random3dTo1d(value, float3(39.346f, 11.135f, 83.155f)),
        Random3dTo1d(value, float3(73.156f, 52.235f, 09.151f))
    );
}

float3 Random2dTo3d(float2 value)
{
    return float3(
        Random2dTo1d(value, float2(12.989f, 78.233f)),
        Random2dTo1d(value, float2(39.346f, 11.135f)),
        Random2dTo1d(value, float2(73.156f, 52.235f))
    );
}

float3 Random1dTo3d(float value)
{
    return float3(
        Random1dTo1d(value, 3.9812f),
        Random1dTo1d(value, 7.1536f),
        Random1dTo1d(value, 5.7241f)
    );
}

#endif