#include "value.fx"
#include "func.fx"

#ifndef _GRASS
#define _GRASS

float To_Radian(float _angle)
{
    return ((0.017453292f) * (_angle));
}
float To_Degree(float _radian)
{
    return ((57.29577951f) * (_radian));

}
float4x4 Pitch(float _angle)
{
    float ccc = cos(_angle);
    float sss = sin(_angle);
    float4x4 pitch = float4x4(
    1.f, 0.f, 0.f, 0.f,
    0.f, ccc, sss, 0.f,
    0.f, -sss, ccc, 0.f,
    0.f, 0.f, 0.f, 1.f
    );
    return pitch;
}
float4x4 Yaw(float _angle)
{
    float ccc = cos(_angle);
    float sss = sin(_angle);
    float4x4 yaw = float4x4(
    ccc, 0.f, -sss, 0.f,
    0.f, 1.f, 0.f, 0.f,
    sss, 0.f, ccc, 0.f,
    0.f, 0.f, 0.f, 1.f
    );
    return yaw;
}
float4x4 Roll(float _angle)
{
    float ccc = cos(_angle);
    float sss = sin(_angle);
    float4x4 roll = float4x4(
    ccc, sss, 0.f, 0.f,
    -sss, ccc, 0.f, 0.f,
    0.f, 0.f, 1.f, 0.f,
    0.f, 0.f, 0.f, 1.f
    );
    return roll;
}
float4x4 RollPitchYaw(float3 _angle)
{
    float4x4 mat = Roll(_angle.x) * Pitch(_angle.y) * Yaw(_angle.z);
    return mat;
}

struct VS_GRASS_INPUT
{
    float3 vPos : POSITION;
    float2 vUV : TEXCOORD;
    
    float3 vTangent : TANGENT;
    float3 vNormal : NORMAL;
    float3 vBinormal : BINORMAL;
    
    row_major matrix matWorld : WORLD;
    row_major matrix matWV : WV;
    row_major matrix matWVP : WVP;
};
struct VS_GRASS_OUTPUT
{
    float4 vPosition : SV_Position;
    float4 vViewPos : POSITION;
    float3 vViewTangent : TANGENT;
    float3 vViewNormal : NORMAL;
    float3 vViewBinormal : BINORMAL;
    float2 vUV : TEXCOORD;
    float fHash : HASH;
};
float2 Grasshash(float2 p)
{
    p = float2(dot(p, float2(127.1f, 311.7f)), dot(p, float2(269.5f, 183.3f)));
    return -1.0f + 2.0f * frac(sin(p) * 43758.5453123f);
}
VS_GRASS_OUTPUT VS_GRASS(VS_GRASS_INPUT _in)
{
    VS_GRASS_OUTPUT output = (VS_GRASS_OUTPUT) 0.f;
    
    output.vPosition = float4(_in.vPos,1.f);
    output.vViewPos = mul(float4(_in.vPos, 1.f), g_matWV);
    output.vViewTangent = normalize(mul(float4(_in.vTangent, 0.f), g_matWV).xyz);
    output.vViewNormal = normalize(mul(float4(_in.vNormal, 0.f), g_matWV).xyz);
    output.vViewBinormal = normalize(mul(float4(_in.vBinormal, 0.f), g_matWV).xyz);
    output.vUV = _in.vUV;
    output.fHash = Grasshash(_in.vPos.xz);
    return output;
}


VS_GRASS_OUTPUT VS_GRASS_INST(VS_GRASS_INPUT _in)
{
    VS_GRASS_OUTPUT output = (VS_GRASS_OUTPUT) 0.f;
    
    output.vPosition = float4(_in.vPos, 1.f);
    output.vViewPos = mul(float4(_in.vPos, 1.f), _in.matWV);
    output.vViewTangent = normalize(mul(float4(_in.vTangent, 0.f), _in.matWV).xyz);
    output.vViewNormal = normalize(mul(float4(_in.vNormal, 0.f), _in.matWV).xyz);
    output.vViewBinormal = normalize(mul(float4(_in.vBinormal, 0.f), _in.matWV).xyz);
    output.vUV = _in.vUV;
    
    return output;
}
// ===============
//  Billboard Grass Shader
//  
// ===============
//  g_float_0   :   scale
//  g_vec2_0    :   windDir

struct GS_GRASS_OUTPUT
{
    float4 vPosition : SV_Position;
    float3 vNormal : NORMAL;
    float4 vViewPos : VIEWPOS;
    float2 vUV : TEXCOORD;
};
struct GrassPatch
{
    float fEdge[4] : SV_TessFactor;
    float fInside[2] : SV_InsideTessFactor;
};
[maxvertexcount(18)]
void GS_GRASS(point VS_GRASS_OUTPUT _in[1], inout TriangleStream<GS_GRASS_OUTPUT> OutputStream)
{
    GS_GRASS_OUTPUT output[12] =
    {
        (GS_GRASS_OUTPUT) 0.f, (GS_GRASS_OUTPUT) 0.f, (GS_GRASS_OUTPUT) 0.f,
        (GS_GRASS_OUTPUT) 0.f, (GS_GRASS_OUTPUT) 0.f, (GS_GRASS_OUTPUT) 0.f, 
        (GS_GRASS_OUTPUT) 0.f, (GS_GRASS_OUTPUT) 0.f, (GS_GRASS_OUTPUT) 0.f,
        (GS_GRASS_OUTPUT) 0.f, (GS_GRASS_OUTPUT) 0.f, (GS_GRASS_OUTPUT) 0.f
    };
    float fCurScale = 7.f;
    //  main rect           -   
    //  [0]--[1]
    //  |  \   |
    //  |   \  |
    //  [3]--[2]
    
    //  Set Postion
   
    float xMove = 0.939692616f * fCurScale; //  cos
    float zMove = 0.342020124f * fCurScale; //  sin
    
    xMove = cos(_in[0].fHash) * fCurScale;
    zMove = sin(_in[0].fHash) * fCurScale;
    
    output[0].vPosition = _in[0].vViewPos + float4(-g_float_0 + cos(g_fAccTime), g_float_0 + _in[0].fHash * 5.f + sin(g_fAccTime), 0.f, 0.f);
    output[1].vPosition = _in[0].vViewPos + float4(g_float_0 + cos(g_fAccTime), g_float_0 + _in[0].fHash * 5.f + -sin(g_fAccTime), 0.f, 0.f);
    output[2].vPosition = _in[0].vViewPos + float4(g_float_0, 0.f, 0.f, 0.f);
    output[3].vPosition = _in[0].vViewPos + float4(-g_float_0, 0.f, 0.f, 0.f);
    
    output[4].vPosition = _in[0].vViewPos + float4(-xMove + cos(g_fAccTime), g_float_0 + _in[0].fHash * 5.f + sin(g_fAccTime), zMove, 0.f);
    output[5].vPosition = _in[0].vViewPos + float4(xMove + cos(g_fAccTime), g_float_0 + _in[0].fHash * 5.f - sin(g_fAccTime), -zMove, 0.f);
    output[6].vPosition = _in[0].vViewPos + float4(xMove, 0.f, -zMove, 0.f);
    output[7].vPosition = _in[0].vViewPos + float4(-xMove, 0.f, zMove, 0.f);
    
    output[8].vPosition = _in[0].vViewPos + float4(-xMove + cos(g_fAccTime), g_float_0 + _in[0].fHash * 5.f + sin(g_fAccTime), -zMove, 0.f);
    output[9].vPosition = _in[0].vViewPos + float4(xMove + cos(g_fAccTime), g_float_0 + _in[0].fHash * 5.f - sin(g_fAccTime), zMove, 0.f);
    output[10].vPosition =  _in[0].vViewPos + float4(xMove, 0.f, zMove, 0.f);
    output[11].vPosition = _in[0].vViewPos + float4(-xMove, 0.f, -zMove, 0.f);
    
    //  rotate Origion Pos
  //  output[4].vPosition = mul(output[4].vPosition, g_mat_0);
  //  output[5].vPosition = mul(output[5].vPosition, g_mat_0);
  //  output[6].vPosition = mul(output[6].vPosition, g_mat_0);
  //  output[7].vPosition = mul(output[7].vPosition, g_mat_0);
  //  
  //  output[8].vPosition =   mul(output[8].vPosition, g_mat_1);
  //  output[9].vPosition =   mul(output[9].vPosition, g_mat_1);
  //  output[10].vPosition =  mul(output[10].vPosition, g_mat_1);
  //  output[11].vPosition = mul(output[11].vPosition, g_mat_1);
    
    //  billboardlize
    // viewpos
    output[0].vViewPos =   output[0].vPosition;
    output[1].vViewPos = output[1].vPosition;
    output[2].vViewPos = output[2].vPosition;
    output[3].vViewPos = output[3].vPosition;
    //       left Tex                           
    output[4].vViewPos = output[4].vPosition;
    output[5].vViewPos =   output[5].vPosition;
    output[6].vViewPos =   output[6].vPosition;
    output[7].vViewPos = output[7].vPosition;
    //      right Tex
    output[8].vViewPos =   output[8].vPosition;
    output[9].vViewPos = output[9].vPosition;
    output[10].vViewPos =  output[10].vPosition;
    output[11].vViewPos = output[11].vPosition;
    
    //      mainTex
    output[0].vPosition = mul(output[0].vPosition, g_matProj);
    output[1].vPosition = mul(output[1].vPosition, g_matProj);
    output[2].vPosition = mul(output[2].vPosition, g_matProj);
    output[3].vPosition = mul(output[3].vPosition, g_matProj);
    //       left Tex
    output[4].vPosition = mul(output[4].vPosition, g_matProj);
    output[5].vPosition = mul(output[5].vPosition, g_matProj);
    output[6].vPosition = mul(output[6].vPosition, g_matProj);
    output[7].vPosition = mul(output[7].vPosition, g_matProj);
    //      right Tex
    output[8].vPosition = mul(output[8].vPosition, g_matProj);
    output[9].vPosition = mul(output[9].vPosition, g_matProj);
    output[10].vPosition = mul(output[10].vPosition, g_matProj);
    output[11].vPosition = mul(output[11].vPosition, g_matProj);
    
    
    //  Set Normal
   // output[0].vNormal = _in[0].vViewNormal;
   // output[1].vNormal = _in[0].vViewNormal;
   // output[2].vNormal = _in[0].vViewNormal;
   // output[3].vNormal = _in[0].vViewNormal;
    
    //  Set UV
    float3 cc = mul(float4(0.f, 0.f, 1.f, 0.0f), -g_matWV).xyz;
    output[0].vNormal = cc;
    output[1].vNormal = cc;
    output[2].vNormal = cc;
    output[3].vNormal = cc;
    
    output[4].vNormal = output[0].vNormal;
    output[5].vNormal = output[1].vNormal;
    output[6].vNormal = output[2].vNormal;
    output[7].vNormal = output[3].vNormal;
    
    output[8].vNormal = output[0].vNormal;
    output[9].vNormal = output[1].vNormal;
    output[10].vNormal = output[2].vNormal;
    output[11].vNormal = output[3].vNormal;
    
    //  setNormal
    
    output[0].vUV = float2(0.f, 0.f);
    output[1].vUV = float2(1.f, 0.f);
    output[2].vUV = float2(1.f, 1.f);
    output[3].vUV = float2(0.f, 1.f);
    
    output[4].vUV = output[0].vUV;
    output[5].vUV = output[1].vUV;
    output[6].vUV = output[2].vUV;
    output[7].vUV = output[3].vUV;
    
    output[8].vUV = output[0].vUV;
    output[9].vUV = output[1].vUV;
    output[10].vUV = output[2].vUV;
    output[11].vUV = output[3].vUV;
    
    
    //  Append Vertex
    //      mainTex
   OutputStream.Append(output[0]);
   OutputStream.Append(output[1]);
   OutputStream.Append(output[2]);
   OutputStream.RestartStrip();
   
   OutputStream.Append(output[0]);
   OutputStream.Append(output[2]);
   OutputStream.Append(output[3]);
   OutputStream.RestartStrip();
   
   //       left Tex
   OutputStream.Append(output[4]);
   OutputStream.Append(output[5]);
   OutputStream.Append(output[6]);
   OutputStream.RestartStrip();
   
   OutputStream.Append(output[4]);
   OutputStream.Append(output[6]);
   OutputStream.Append(output[7]);
   OutputStream.RestartStrip();
    
    //      right Tex
    OutputStream.Append(output[8]);
    OutputStream.Append(output[9]);
    OutputStream.Append(output[10]);
    OutputStream.RestartStrip();
    
    OutputStream.Append(output[8]);
    OutputStream.Append(output[10]);
    OutputStream.Append(output[11]);
    OutputStream.RestartStrip();
    
}
struct PS_GRASS_OUTPUT
{
    float4 vTarget0 : SV_Target0; // Diffuse
    float4 vTarget1 : SV_Target1; // Normal
    float4 vTarget2 : SV_Target2; // Position
};
PS_GRASS_OUTPUT PS_GRASS(GS_GRASS_OUTPUT _input)
{
    PS_GRASS_OUTPUT output = (PS_GRASS_OUTPUT) 0.f;
    float4 vColor = (float4) 0.f;
    if(tex_0)
    {
        float4 sten = g_tex_3.Sample(g_sam_1, _input.vUV);
        vColor = g_tex_0.Sample(g_sam_1, _input.vUV);
       
        if (sten.r <= 0.f && sten.g <= 0.f && sten.b <= 0.f)
        {
            clip(-1);
        }
    }
    else
    {
        vColor = float4(0.f, 1.0f, 0.f, 1.f);
    }
    output.vTarget0 = vColor;
    output.vTarget1.xyz = _input.vNormal;
    output.vTarget2 = _input.vViewPos;
    
    //vColor = float4(0.f,0.5f,0.0f,0.1f);
   
    return output;
}
// ===============
//  Triangle Grass Shader
//  
// ===============
//  :VEC4_0, &camPos);
//  :VEC4_1, &size);
//  
//  :VEC2_0, &widHei);
//  :VEC2_1, &vWindDir);
//  
//  :FLOAT_0, &curScale)
//  :FLOAT_1, &tessMin);
//  :FLOAT_2, &tessMax);
struct VS_GRASS_TRI_INPUT
{
    float3 vPos : POSITION;
    float2 vUV : TEXCOORD;
    
    float3 vTangent : TANGENT;
    float3 vNormal : NORMAL;
    float3 vBinormal : BINORMAL;
    
    row_major matrix matWorld : WORLD;
    row_major matrix matWV : WV;
    row_major matrix matWVP : WVP;
};
struct VS_GRASS_TRI_OUTPUT
{
    float4 vPosition : SV_Position;
    float2 vUV : TEXCOORD;
    float4 vViewPos : VIEWPOS;
    
    float3 Tangent : TANGENT;
    float3 Normal : NORMAL;
    float3 Binormal : BINORMAL;
    
    row_major matrix matWorld : WORLD;
    row_major matrix matWV : WV;
    row_major matrix matWVP : WVP;
};

VS_GRASS_TRI_OUTPUT VS_GRASS_TRI(VS_GRASS_TRI_INPUT _in)
{
    VS_GRASS_TRI_OUTPUT output = (VS_GRASS_TRI_OUTPUT) 0.f;
    output.vPosition = float4(_in.vPos, 1.f);
    output.vUV = _in.vUV;
    output.Tangent = _in.vTangent;
    output.Normal = _in.vNormal;
    output.Binormal = _in.vBinormal;
    return output;
}

VS_GRASS_TRI_OUTPUT VS_GRASS_TRI_INST(VS_GRASS_TRI_INPUT _in)
{
    VS_GRASS_TRI_OUTPUT output = (VS_GRASS_TRI_OUTPUT) 0.f;
    
    output.vPosition = float4(_in.vPos, 1.f);
    
    output.vUV = _in.vUV;
    output.Tangent = _in.vTangent;
    output.Normal = _in.vNormal;
    output.Binormal = _in.vBinormal;
    
    output.matWorld = _in.matWorld;
    output.matWV = _in.matWV;
    output.matWVP = _in.matWVP;
    
    return output;
}
GrassPatch HS_GrassTriConstant(InputPatch<VS_GRASS_TRI_OUTPUT, 4> _patch, uint patchID : SV_PrimitiveID)
{
    GrassPatch output = (GrassPatch) 0.f;
    
    float3 centerL = 0.25f * (_patch[0].vPosition.xyz + _patch[1].vPosition.xyz + _patch[2].vPosition.xyz + _patch[3].vPosition.xyz);
    float3 centerW = (float3) 0.f;
    
    centerW = mul(float4(centerL, 1.0f), g_matWorld).xyz;
    
   //   metarial 에다가 카메라 포지션 이거 넣어놓자
    float d = distance(centerW, g_vec4_0.xyz);
    float tess = 64.f * saturate((g_float_2 - d) / (g_float_2 - g_float_1));
    
    output.fEdge[0] = tess;
    output.fEdge[1] = tess;
    output.fEdge[2] = tess;
    output.fEdge[3] = tess;
    output.fInside[0] = tess;
    output.fInside[1] = tess;
    
    return output;
}
struct HS_GRASS_TRI_OUTPUT
{
    float4 vPosL : POSITION;
    float2 vUV : TEXCOORD;
    
    float3 vTangent : TANGENT;
    float3 vNormal : NORMAL;
    float3 vBinormal : BINORMAL;
};
[domain("quad")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(4)]
[patchconstantfunc("HS_GrassTriConstant")]
[maxtessfactor(8.f)]
HS_GRASS_TRI_OUTPUT HS_GRASS_TRI(InputPatch<VS_GRASS_TRI_OUTPUT, 4> _patch, uint i : SV_OutputControlPointID, uint patchID : SV_PrimitiveID)
{
    HS_GRASS_TRI_OUTPUT output = (HS_GRASS_TRI_OUTPUT) 0.f;
    output.vPosL = _patch[i].vPosition;
    output.vUV = _patch[i].vUV;
    output.vNormal = _patch[i].Normal;
    output.vBinormal = _patch[i].Binormal;
    output.vTangent = _patch[i].Tangent;
    return output;
}
struct DS_GRASS_TRI_OUTPUT
{
    float4 vPosition : SV_POSITION;
    float4 vViewPos : VIEWPOS;
    float2 vUV : TEXCOORD;
    
    float3 vTangent : TANGENT;
    float3 vNormal : NORMAL;
    float3 vBinormal : BINORMAL;
};
[domain("quad")]
DS_GRASS_TRI_OUTPUT DS_GRASS_TRI(GrassPatch _patch, float2 uv : SV_DomainLocation, const OutputPatch<HS_GRASS_TRI_OUTPUT,4> quad)
{
    DS_GRASS_TRI_OUTPUT output = (DS_GRASS_TRI_OUTPUT) 0.f;
    
    float4 v1 = lerp(quad[0].vPosL, quad[1].vPosL, uv.x);
    float4 v2 = lerp(quad[2].vPosL, quad[3].vPosL, uv.x);
    float4 p = lerp(v1, v2, uv.y);
    
    float2 uv1 = p.xz / 400.f;
    
    output.vPosition = p;
    output.vViewPos = mul(p, g_matWV);
    output.vUV = uv1;
    output.vNormal = quad[0].vNormal;
    output.vBinormal = quad[0].vBinormal;
    output.vTangent = quad[0].vTangent;
    return output;
}



GS_GRASS_OUTPUT VertexOutput(float4 _in,float3 _normal,float2 _uv)
{
    GS_GRASS_OUTPUT output = (GS_GRASS_OUTPUT) 0.f;
    output.vPosition = mul(_in, g_matWVP);
    output.vNormal = mul(_in, g_matWV);
    output.vViewPos = mul(_in, g_matWV);
    output.vNormal = _normal;
    output.vUV = _uv;
    return output;
}


[maxvertexcount(18)]
void GS_GRASS_TRI(triangle DS_GRASS_TRI_OUTPUT _in[3], inout TriangleStream<GS_GRASS_OUTPUT> OutputStream)
{
    GS_GRASS_OUTPUT output[3] =
    {
        (GS_GRASS_OUTPUT) 0.f, (GS_GRASS_OUTPUT) 0.f, (GS_GRASS_OUTPUT) 0.f
    };
    
    
    //  -----------------------------------------------
     //  이전방식
    
    float2 widHei = g_vec2_0;
    float2 hash = float2(RandomLine(_in[0].vPosition.xyz), RandomNoise(_in[1].vPosition.xyz));
    widHei += hash * 0.5f;
    widHei.x = clamp(widHei.x, 0.15f, 1.5f);
    widHei.y = clamp(widHei.y, 0.0f, 10.f);
   
    
    float angle = To_Degree(hash.x);
    float4x4 mat = Yaw(angle);
    float4 left = mul(float4(-widHei.x, 0.f, 0.f, 0.f), mat);
    float4 up = mul(float4(0.f, widHei.y, 0.f, 0.f), mat);
    float4 right = mul(_in[0].vPosition, g_matWV) + mul(float4(widHei.x, 0.f, 0.f, 0.f), mat);
    
    output[0].vPosition = _in[0].vPosition + left;
    output[1].vPosition = _in[0].vPosition + up;
    output[2].vPosition = _in[0].vPosition + right;
    
    float4 u = normalize((output[0].vPosition - output[1].vPosition));
    float4 v = normalize((output[2].vPosition - output[1].vPosition));
    float4 crs = float4(cross(u.xyz, v.xyz),1.f);
    
   // output[0].vViewPos = _in[0].vPosition + left;
   // output[1].vViewPos = _in[0].vPosition + up + float4(cos(g_fAccTime), 0.0f, sin(g_fAccTime), 0.f);
   // output[2].vViewPos = _in[0].vPosition + right;
    
   // output[0].vViewPos = _in[0].vViewPos + left;
   // output[1].vViewPos = _in[0].vViewPos + up + float4(cos(g_fAccTime), 0.0f, sin(g_fAccTime), 0.f);
   // output[2].vViewPos = _in[0].vViewPos + right;
    
    
    output[0].vViewPos = mul(_in[0].vPosition, g_matWV) + left;
    
    float2 windUV = sin(((_in[0].vPosition.xz * g_vec2_1) * g_fAccTime) * 0.06f) * 0.3f;
    float3 rand = RandomLine((_in[0].vPosition.xyz) * g_fAccTime);
    
    output[1].vViewPos = mul(_in[0].vPosition, g_matWV) + up +float4(windUV.x, 0.0f, windUV.y, 0.f)* g_float_3;
    output[2].vViewPos = mul(_in[0].vPosition, g_matWV) + right;
        
    output[0].vPosition = mul(output[0].vViewPos, g_matProj);
    output[1].vPosition = mul(output[1].vViewPos, g_matProj);
    output[2].vPosition = mul(output[2].vViewPos, g_matProj);
    
    float3 normal = mul(crs, mat).xyz;
    
    output[0].vNormal = normal;
    output[1].vNormal = normal;
    output[2].vNormal = normal;
    
    output[0].vUV = float2(0.f,0.f);
    output[1].vUV = float2(0.f,1.f);             
    output[2].vUV = float2(1.f, 0.f);
    
    OutputStream.Append(output[0]);
    OutputStream.Append(output[1]);
    OutputStream.Append(output[2]);
    OutputStream.RestartStrip();
}


struct PS_GRASS_TRI_OUTPUT
{
    float4 vTarget0 : SV_Target0; // Diffuse
    float4 vTarget1 : SV_Target1; // Normal
    float4 vTarget2 : SV_Target2; // Position
};
PS_GRASS_TRI_OUTPUT PS_GRASS_TRI(GS_GRASS_OUTPUT _input)
{
    PS_GRASS_TRI_OUTPUT output = (PS_GRASS_TRI_OUTPUT) 0.f;
   
    float4 vColor = (float4) 0.f;
    vColor = float4(0.f,0.5f - _input.vUV.x, 0.f, 1.f);
    
    output.vTarget0 = vColor;
    output.vTarget1.xyz = _input.vNormal;
    output.vTarget2 = _input.vViewPos;
    
    return output;
}
// ===============
//  TESS Grass Shader
//  
// ===============
  

struct VS_GRASS_TESS_INPUT
{
    float3 vPos : POSITION;
    float2 vUV : TEXCOORD;
    
    float3 vTangent : TANGENT;
    float3 vNormal : NORMAL;
    float3 vBinormal : BINORMAL;
    
    row_major matrix matWorld : WORLD;
    row_major matrix matWV : WV;
    row_major matrix matWVP : WVP;
    
    int nInst;
};
struct VS_GRASS_TESS_OUTPUT
{
    float3 vPosition : POSITION;
    float2 vUV : TEXCOORD;
    
    float3 Tangent : TANGENT;
    float3 Normal : NORMAL;
    float3 Binormal : BINORMAL;
    
    row_major matrix matWorld : WORLD;
    row_major matrix matWV : WV;
    row_major matrix matWVP : WVP;
    
    int nInst : INST;
};

VS_GRASS_TESS_OUTPUT VS_GRASS_TESS(VS_GRASS_TESS_INPUT _in)
{
    VS_GRASS_TESS_OUTPUT output = (VS_GRASS_TESS_OUTPUT) 0.f;

    output.vPosition = _in.vPos;
    output.vUV      =    _in.vUV;
    output.Tangent  =   _in.vTangent;
    output.Normal = _in.vNormal;
    output.Binormal = _in.vBinormal;
  
    output.nInst = 0;
    return output;
}
VS_GRASS_TESS_OUTPUT VS_GRASS_TESS_INST(VS_GRASS_TESS_INPUT _in)
{
    VS_GRASS_TESS_OUTPUT output = (VS_GRASS_TESS_OUTPUT) 0.f;

    output.vPosition = _in.vPos;
    output.vUV = _in.vUV;
    output.Tangent = _in.vTangent;
    output.Normal = _in.vNormal;
    output.Binormal = _in.vBinormal;
    output.matWorld = _in.matWorld;
    output.matWV = _in.matWV;
    output.matWVP = _in.matWVP;

    output.nInst = 1;
    return output;
}

//  g_float_0 : min dist
//  g_float_1 : max dist
//  g_vec4_0 : cameraPos
//  
//  
GrassPatch HS_GrassConstant(InputPatch<VS_GRASS_TESS_OUTPUT,4> _patch,uint patchID : SV_PrimitiveID)
{
    GrassPatch output = (GrassPatch) 0.f;
    
    float3 centerL = 0.25f * (_patch[0].vPosition.xyz + _patch[1].vPosition.xyz + _patch[2].vPosition.xyz + _patch[3].vPosition.xyz);
    float3 centerW = (float3) 0.f;
    if (_patch[0].nInst == 0)
    {
        centerW = mul(float4(centerL, 1.0f), g_matWorld).xyz;
    }
    else
    {
        centerW = mul(float4(centerL, 1.0f), _patch[0].matWorld).xyz;
    }
   //   metarial 에다가 카메라 포지션 이거 넣어놓자
    float d = distance(centerW, g_vec4_0.xyz);
    float tess = 64.f * saturate((g_float_1 - d) / (g_float_1 - g_float_0));
    
    output.fEdge[0] = tess; output.fEdge[1] = tess; output.fEdge[2] = tess; output.fEdge[3] = tess;
    output.fInside[0] = tess; output.fInside[1] = tess;
    
    return output;
}
struct HS_GRASS_TESS_OUTPUT
{
    float3 vPosL : POSITION;
    float2 vUV : TEXCOORD;
    
    float3 Tangent : TANGENT;
    float3 Normal : NORMAL;
    float3 Binormal : BINORMAL;
    
    row_major matrix matWorld : WORLD;
    row_major matrix matWV : WV;
    row_major matrix matWVP : WVP;
    
    int nInst : INST;
};

[domain("quad")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoint(4)]
[patchconstantfunc("HS_GrassConstant")]
[maxtessfactor(64.f)]
HS_GRASS_TESS_OUTPUT HS_GRASS_TESS(InputPatch<VS_GRASS_TESS_OUTPUT,4> _patch, uint i : SV_OutputControlPointID,uint patchID : SV_PrimitiveID)
{
    HS_GRASS_TESS_OUTPUT output = (HS_GRASS_TESS_OUTPUT) 0.f;
    output.vPosL = _patch[i].vPosition;
    output.vUV = _patch[i].vUV;
    output.Tangent = _patch[i].Tangent;
    output.Normal = _patch[i].Normal;
    output.Binormal = _patch[i].Binormal;
    output.nInst = _patch[i].nInst;
    if (_patch[i].nInst == 1)
    {
        output.matWorld = _patch[i].matWorld;
        output.matWV = _patch[i].matWV;
        output.matWVP = _patch[i].matWVP;
    }
    return output;
}
struct DS_GRASS_OUTPUT
{
    float4 PosH : SV_POSITION;
    
};

#endif