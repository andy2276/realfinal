#include "value.fx"
#include "func.fx"

#ifndef _EFFECT
#define _EFFECT

// ===============
//  FireBurn Shader
//  
// ===============
//	g_tex_0 noise

struct VS_FIREBURN_INPUT
{
    float3 vPos : POSITION;
    float2 vUV : TEXCOORD;
    
    row_major matrix matWorld : WORLD;
    row_major matrix matWV : WV;
    row_major matrix matWVP : WVP;
    
    uint nInstanceID : SV_InstanceID;
};


struct VS_FIREBURN_OUTPUT
{
    float4 vPosition : SV_Position;
    float4 vViewPos : VIEWPOS;
    float2 vUV : TEXCOORD;
    uint nInstID : INSTID;
};

VS_FIREBURN_OUTPUT VS_FIREBURN(VS_FIREBURN_INPUT _input)
{
    VS_FIREBURN_OUTPUT output = (VS_FIREBURN_OUTPUT) 0;
    
    // 투영좌표계를 반환할 때에는 float4 4번째 w 요소에 1.f 을 넣어준다.
    float4 vWorldPos = mul(float4(_input.vPos, 1.f), g_matWorld);
    float4 vViewPos = mul(vWorldPos, g_matView);
    float4 vProjPos = mul(vViewPos, g_matProj);

    output.vPosition = vProjPos;
    output.vViewPos = vViewPos;
    output.vUV = _input.vUV;
    output.nInstID = 0;
    return output;
}

VS_FIREBURN_OUTPUT VS_FIREBURN_Inst(VS_FIREBURN_INPUT _input)
{
    VS_FIREBURN_OUTPUT output = (VS_FIREBURN_OUTPUT) 0;
    
    // 투영좌표계를 반환할 때에는 float4 4번째 w 요소에 1.f 을 넣어준다.
    float4 vProjPos = mul(float4(_input.vPos, 1.f), _input.matWVP);
    float4 vViewPos = mul(float4(_input.vPos, 1.f), _input.matWV);

    output.vPosition = vProjPos;
    output.vViewPos = vViewPos;
    output.vUV = _input.vUV;
    output.nInstID = _input.nInstanceID;
    return output;
}

float4 PS_FIREBURN(VS_FIREBURN_OUTPUT _input) : SV_Target
{
    float2 colorCord = _input.vUV;
    float2 noiseCord = _input.vUV;
    float2 stenCord = _input.vUV;
    
    float2 vDir = normalize(float2(0.5f, 0.5f) - _input.vUV);
    float fDist = distance(float2(0.5f, 0.5f), _input.vUV);
    float fRatio = (fDist / 0.5f);
  //  fRatio = fRatio * (0.2f + (fRatio) * 0.4f);
    
    colorCord.x += sin(colorCord.y * 10.f + g_fAccTime) * 0.1f;
    colorCord.y += cos(colorCord.x * -10.f + g_fAccTime) * 0.034f;
    
    noiseCord.x += cos(noiseCord.y * -5.f + g_fAccTime) * 0.4f;
    noiseCord.y += sin(noiseCord.y * 5.f + g_fAccTime) * 0.4f;
    
    float4 vColor = (float4)0.f;
    float4 vNoise = (float4) 0.f;
    float4 vSten = (float4) 0.f;
    if (fRatio < 1.1f)
    {
        fRatio = 1.3f;
    }
 
    vColor = g_tex_2.Sample(g_sam_0, colorCord) * fRatio * 2.5f ;
    if (fRatio < 2.3f)
    {
        vColor.r *= fRatio;
    }
    vNoise = g_tex_0.Sample(g_sam_0, noiseCord);
    vSten = g_tex_1.Sample(g_sam_0, stenCord);
    vSten.rgba = vSten.rrrr;
    vNoise.rgba = vNoise.rrrr;
    vColor = vColor * vNoise * vSten;
    return vColor;

}



#endif