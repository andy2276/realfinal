#pragma once
#include "TriggerNode.h"

namespace Trigger {
	namespace Event {
		namespace Special {
			namespace Queries {
				//	레벨을 재방문 했었나?
				class CIfLevelRevisit : public Trigger::Base::CQuery {
				public:
					CIfLevelRevisit();
					virtual ~CIfLevelRevisit();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				//	플레이어가 지금 맵에 들어와있는가?
				class CIfPlayerInMap : public Trigger::Base::CQuery {
				public:
					CIfPlayerInMap();
					virtual ~CIfPlayerInMap();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				//	내가 설정한 몬스터 수보다 지금 몬스터가 적은가?(아니면 계속 wait 맞으면 success)
				class CIfCurMonsterLess : public Trigger::Base::CQuery {
				public:
					CIfCurMonsterLess();
					virtual ~CIfCurMonsterLess();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				//	라운드가 끝인가? 아니면 다른걸로감
				class CIfRoundEnd : public Trigger::Base::CQuery {
				public:
					CIfRoundEnd();
					virtual ~CIfRoundEnd();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				//	내가 설정한 라운드가 맞는가
				//	그니깐 예를들어서 만약 처음 그 레벨 들어갔다면 이 맵에 설정된 이 트리거 목록을 실행함
				//	
				//	지금 이라운드가 맞는가?	지금 현재 라운드를 찾는거임
				class CIfSetRound : public Trigger::Base::CQuery {
				public:
					CIfSetRound();
					virtual ~CIfSetRound();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				//	지금 스폰타입에 따라 나눔
				class CIfSponTypeSame : public Trigger::Base::CQuery {
				private:
					EC_RESPON_TYPE m_eSponType;
				public:
					CIfSponTypeSame();
					virtual ~CIfSponTypeSame();

					void SetSponType(EC_RESPON_TYPE _type) { m_eSponType = _type; }


					virtual Trigger::Result Update(Trigger::Result _return);
				};
				//	라운드가 클리어되었나?
				class CIfRoundClear : public Trigger::Base::CQuery {
				public:
					CIfRoundClear();
					virtual ~CIfRoundClear();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				class CIfLevelEnd : public Trigger::Base::CQuery {
				public:
					CIfLevelEnd();
					virtual ~CIfLevelEnd();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				class CIfPossibleInTower: public Trigger::Base::CQuery {
				public:
					CIfPossibleInTower();
					virtual ~CIfPossibleInTower();

					virtual Trigger::Result Update(Trigger::Result _return);
				};

				//	라운드 설정조건들
				//		지금 맵에서 액티브 되는 몬스터들의 수가 내가 설정한 수인가
				//	

			}
			namespace Works {
				//	몬스터를 설정한 수만큼 바로 생성한다.
				class CDoSponAtOnce : public Trigger::Base::CWork {
				public:
					CDoSponAtOnce();
					virtual ~CDoSponAtOnce();

					virtual Trigger::Result Update(Trigger::Result _return);

				};
				class CDoSponSequence : public Trigger::Base::CWork {
				public:
					CDoSponSequence();
					virtual ~CDoSponSequence();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				//	지금 레벨의 몬스터들을 끈다
				class CDoOffMonsters : public Trigger::Base::CWork {
				public:
					CDoOffMonsters();
					virtual ~CDoOffMonsters();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				//	지금 레벨의 몬스터들을 킨다
				class CDoOnMonsters : public Trigger::Base::CWork {
				public:
					CDoOnMonsters();
					virtual ~CDoOnMonsters();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				//	맵이동을 가능하게 해주는것 이게 켜져야 이동이 가능함.
				class CDoMoveMapOn : public Trigger::Base::CWork {
				public:
					CDoMoveMapOn();
					virtual ~CDoMoveMapOn();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				class CDoTowerEventStart : public Trigger::Base::CWork {
				public:
					CDoTowerEventStart();
					virtual ~CDoTowerEventStart();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				class CDoMapMove : public Trigger::Base::CWork {
				public:
					CDoMapMove();
					virtual ~CDoMapMove();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				class CDoMonsterSponSequence : public Trigger::Base::CWork {
				public:
					CDoMonsterSponSequence();
					virtual ~CDoMonsterSponSequence();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				class CDoMonsterSponAtOnce : public Trigger::Base::CWork {
				public:
					CDoMonsterSponAtOnce();
					virtual ~CDoMonsterSponAtOnce();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				class CDoRoundEnd : public Trigger::Base::CWork {
				public:
					CDoRoundEnd();
					virtual ~CDoRoundEnd();

					virtual Trigger::Result Update(Trigger::Result _return);
				};
				//	설정한 사운드를 출력한다 이거는 다음에
	
				//	뜨문뜨문 출력
				
			}
		}
	}
}