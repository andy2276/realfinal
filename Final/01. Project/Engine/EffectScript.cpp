#include "stdafx.h"
#include "EffectScript.h"
#include "MapMgr.h"

CEffectScript::CEffectScript()
	:CScript((UINT)SCRIPT_TYPE::OTHERS)
	, m_fMinScale(0.f)
	, m_fCurScale(0.0f)
, m_fMaxScale(0.f)
, m_fScaleSpeed(0.f)
, m_fMaxLifeTime(0.f)
, m_fCurLifeTime(0.f)
{
}


CEffectScript::~CEffectScript()
{
}

void CEffectScript::update()
{
	static CMapMgr* mgr = CMapMgr::GetInst();
	static Vec3 camPos, mePos, lookPos,meRot,scale;
	float time = DT;
	m_fCurLifeTime += time;
	if (m_fCurLifeTime < m_fMaxLifeTime)
	{
		if (m_fMinScale < m_fCurScale && m_fCurScale < m_fMaxScale)
		{
			m_fCurScale += m_fScaleSpeed * time;
			scale.x = m_fCurScale; scale.y = m_fCurScale; scale.z = 1.f;
			Transform()->SetLocalScale(scale);
		}
		else
		{
			m_fMinScale = 0.f;
			m_fCurScale = 0.f;
			m_fMaxScale = 0.f;
			m_fScaleSpeed = 0.f;
			m_fMaxLifeTime = 0.f;
			m_fCurLifeTime = 0.f;
			GetObj()->SetActive(false);
		}
	}
	else
	{
		m_fMinScale = 0.f;
		m_fCurScale = 0.f;
		m_fMaxScale = 0.f;
		m_fScaleSpeed = 0.f;
		m_fMaxLifeTime = 0.f;
		m_fCurLifeTime = 0.f;
		GetObj()->SetActive(false);
	}
	camPos = CMapMgr::GetInst()->GetMainCam()->Transform()->GetWorldPos();
	mePos = Transform()->GetLocalPos();
	lookPos = mePos - camPos;
	float fAngle = atan2(lookPos.x, lookPos.z);
	meRot = Transform()->GetLocalRot();
	meRot.y = fAngle;
	Transform()->SetLocalRot(meRot);
}

CGameObject* CEffectScript::CreateFireBurn(const Vec3 & _pos, const Vec3 & _scale)
{
	CGameObject* pObj = nullptr;
	pObj = new CGameObject;
	pObj->AddComponent(new CTransform);
	pObj->AddComponent(new CMeshRender);
	pObj->AddComponent(new CEffectScript);
	pObj->SetName(L"fire");
	pObj->FrustumCheck(false);
	pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	pObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"EffectFireBurnMtrl"));

	pObj->Transform()->SetLocalPos(_pos);
	pObj->Transform()->SetLocalScale(_scale);
	CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"Default")->AddGameObject(pObj);
	return pObj;
}

CGameObject* CEffectScript::CreateFireBurn(const Vec3 & _pos, float _minScale, float _startScale, float _maxScale, float _speed, float _lifeTime)
{
	CGameObject* pObj = nullptr;
	CEffectScript* ef = new CEffectScript;
	pObj = new CGameObject;
	pObj->AddComponent(new CTransform);
	pObj->AddComponent(new CMeshRender);
	pObj->AddComponent(ef);
	ef->SetScales(_minScale, _startScale, _maxScale, _speed, _lifeTime);
	pObj->SetName(L"fire");
	pObj->FrustumCheck(false);
	pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	pObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"EffectFireBurnMtrl"));

	pObj->Transform()->SetLocalPos(_pos);
	pObj->Transform()->SetLocalScale(Vec3(_startScale, _startScale,1.f));
	CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"Default")->AddGameObject(pObj);
	return pObj;
}
