#include "stdafx.h"
#include "MirrorBossSkillScript.h"

#include"ParticleSystem.h"	
MirrorBossSkillScript::MirrorBossSkillScript():CScript((UINT)SCRIPT_TYPE::INVENTORYSCRIPT)
{
	m_Pos= Vec3(0,0,0);

	//스킬 오브젝트 
	// 1. 파티클 생성 
	// 2. 텍스쳐 원 밑에 생성 
	// 3. 충돌 박스 연결해주기 
	
	// 4. 플레이어와의 거리 * 2 지점까지 반지름으로 잡고 그중에서 랜덤위치를 잡고, 그 위치에 3개 생성하기 . 
	
	Vec3 vPlayerpos;




	Vec3 vectemp = vPlayerpos - m_Pos; //최적화 필요할때 수정 
	Vec2 temp;
	temp.x = vPlayerpos.x - m_Pos.x;
	temp.y = vPlayerpos.z - m_Pos.z; 
	
	m_Attackrange = (temp.Length()) * 2;

//	(x - a)2 + (y - b)2 = r2(알제곱)

	float x, y;

	//random x를 찾으면 y를 찾을 수 있다. 

	//(x - vPlayerpos.x)* (x - vPlayerpos.x) + (y - vPlayerpos.y) * (y - vPlayerpos.y) = m_Attackrange * m_Attackrange;

	//이 x와 y가 랜덤 위치가 되는것이고 

	//m_Attackrange = temp;

	//0721 아림추
	//파티클 생성 
	{
		// ====================
		// Particle Object 생성
		// ====================
		CGameObject * pObject = new CGameObject;
		pObject->SetName(L"Particle");
		pObject->AddComponent(new CTransform);
	//	pObject->AddComponent(new CParticleSystem);

		pObject->FrustumCheck(false);
		pObject->Transform()->SetLocalPos(Vec3(m_Pos));
		CreateObject(pObject, L"Player");

	}

	//=========================
	// 암흑 장판 오브젝트 
	//=========================

	{

		CGameObject * pObject = new CGameObject;
		pObject->SetName(L"Mirror floor");
		pObject->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
		pObject->AddComponent(new CTransform);
		pObject->AddComponent(new CMeshRender);

		
		Vec3 vScale(850, 900.f, 1.f);
//		pObject->Transform()->SetLocalPos(Vec3(m_Pos));
		pObject->Transform()->SetLocalPos(Vec3(0,200,500));

		pObject->Transform()->setPhysics(false);
		pObject->Transform()->SetLocalScale(vScale);
		pObject->Transform()->SetLocalRot(Vec3(XM_ANGLE * 90, 0, 0)); //x축 90도 회전 


		pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"CircleMesh"));
		Ptr<CMaterial> pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
		pObject->MeshRender()->SetMaterial(pMtrl->Clone());
		Ptr<CTexture> pTexture = CResMgr::GetInst()->Load<CTexture>(L"purpleFloor", L"Texture\\purpleFloor.png");
		pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTexture.GetPointer());


		CreateObject(pObject, L"Bullet");//여기수정해주세요 
		
	

	}




}


MirrorBossSkillScript::~MirrorBossSkillScript()
{
}
