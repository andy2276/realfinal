#pragma once
#include "BossAiEvaluation.h"

namespace nBT
{
	namespace TSK
	{
		namespace EnumValue
		{
			enum class Cmp
			{
				Less = 0,
				below = 1,
				more = 2,
				above = 3,
				same = 4,
				End

			};
		}

		namespace CDT
		{
			//	scr	:	고정	dest	:	업데이트
			//	scr		<		dest	:	Less	:	0
			//	scr		<=		dest	:	below	:	1
			//	scr		>		dest	:	more	:	2
			//	scr		>=		dest	:	above	:	3
			//	scr		==		dest	:	same	:	4

			class bFloatScrCmpDest : public bCondition
			{
			private:
				float	m_fScrValue;
				float*	m_pDestValue;
				EnumValue::Cmp		m_eCmp;

			public:
				bFloatScrCmpDest();
				virtual ~bFloatScrCmpDest();

				void SetScrValue(float _scr) { m_fScrValue = _scr; };
				void SetDestValue(float* _dest) { m_pDestValue = _dest; }
				void SetCmpValue(EnumValue::Cmp _cmp) { m_eCmp = _cmp; }

				void SetData(float _scr, float* _dest, EnumValue::Cmp _cmp)
				{
					SetScrValue(_scr);
					SetDestValue(_dest);
					SetCmpValue(_cmp);
				}

				virtual nBT::bResult Evaluate(bResult _result = bResult::Evaluation);
			};
		}

		namespace ACT
		{
			class bMovePos : public bAction
			{
			private:
				float		m_fNear;
				Vec3		m_vDestPos;
			public:
				bMovePos();
				virtual ~bMovePos();

				void SetNear(float _fNear) { m_fNear = _fNear; }
				void SetDestPos(const Vec3& _destPos) { m_vDestPos = _destPos; }

				void SetData(float _fNear, const Vec3& _destPos)
				{
					SetNear(_fNear);
					SetDestPos(_destPos);
				}

				virtual nBT::bResult Evaluate(bResult _result = bResult::Evaluation);
			};

			class bTurnAngle : public bAction
			{
			private:
				float		m_fOper;
				float		m_fCurAngle;
				float		m_fOrderAngle;
				float		m_fAngle;
			public:
				bTurnAngle();
				virtual ~bTurnAngle();

				void SetOper(float _oper) { m_fOper = _oper; }
				void SetDestAngle(float _destAngle) { m_fAngle = _destAngle; }

				void SetData(float _oper, float _destAngle)
				{
					SetOper(_oper);
					SetDestAngle(_destAngle);
				}

				virtual nBT::bResult Evaluate(bResult _result = bResult::Evaluation);
			};
			class bTurnDest : public bAction
			{
			private:
				Vec3		m_vDestPos;
			public:
				bTurnDest();
				virtual ~bTurnDest();

				void SetDestPos(const Vec3& _destPos) { m_vDestPos = _destPos; }

				void SetData(const Vec3& _destPos)
				{
					SetDestPos(_destPos);
				}

				virtual nBT::bResult Evaluate(bResult _result = bResult::Evaluation);
			};
			class bShootSkill : public bAction
			{
			private:



			public:
				bShootSkill();
				virtual ~bShootSkill();

				void SetData(const Vec3& _destPos)
				{
					
				}

				virtual nBT::bResult Evaluate(bResult _result = bResult::Evaluation);

			};
			
		}
		

	}
}