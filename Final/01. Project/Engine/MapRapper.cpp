#include "stdafx.h"
#include "MapRapper.h"
#include <fstream>
#include "Transform.h"
#include "Collider2D.h"
#include "StatusComponent.h"
#include "GameObject.h"
#include "PathMgr.h"
#include "TimeMgr.h"
#include <bitset>

CMapRapper::CMapRapper() 
	:m_pMap(nullptr)
	, m_pHeightMap(nullptr)
	, m_pColliderMap(nullptr)
	, m_nCurMopStopCnt{0,0,0}
	, m_bMovePassible(false)
	, m_bColID(BYTE(EC_COL_TYPE::Player)+1)
{

}


CMapRapper::~CMapRapper()
{
	if(m_pHeightMap != nullptr)delete[] m_pHeightMap;
	if (m_pColliderMap != nullptr)delete[] m_pColliderMap;
	if (m_pOriginColliderMap != nullptr)delete[] m_pOriginColliderMap;

}

void CMapRapper::SetMapObj(UINT _type, CGameObject * _pObj)
{
	UINT id = _pObj->GetID();
	for (int i = 0; i < (int)EC_MOSTER_TYPE::End; ++i) {
		if (m_vecMonsterObjStop[i].end() != std::find(m_vecMonsterObjStop[i].begin(), m_vecMonsterObjStop[i].end(), _pObj)) {
			return;
		}
	}
	if (m_vecItemObjStop.end() != std::find(m_vecItemObjStop.begin(), m_vecItemObjStop.end(), _pObj)) {
		return;
	}
	for (int i = 0; i < (int)EC_MAP_OBJ_TYPE::End; ++i) {
		if (m_mapObjReady[i].end() != m_mapObjReady[i].find(id))return;
	}
	_pObj->StatusComponent()->SetColNumId(m_bColID++);
	if (m_bColID == (BYTE)EC_COL_TYPE::Path)return;
	m_mapObjReady[_type].insert(make_pair(id, _pObj));
}

void CMapRapper::SetMapObj(const EC_MAP_OBJ_TYPE & _type, CGameObject * _pObj)
{
	UINT id = _pObj->GetID();
	for (int i = 0; i < (int)EC_MOSTER_TYPE::End; ++i) {
		if (m_vecMonsterObjStop[i].end() != std::find(m_vecMonsterObjStop[i].begin(), m_vecMonsterObjStop[i].end(), _pObj)) {
			return;
		}
	}
	if (m_vecItemObjStop.end() != std::find(m_vecItemObjStop.begin(), m_vecItemObjStop.end(), _pObj)) {
		return;
	}
	for (int i = 0; i < (int)EC_MAP_OBJ_TYPE::End; ++i) {
		if (m_mapObjReady[i].end() != m_mapObjReady[i].find(id))return;
	}
	_pObj->StatusComponent()->SetColNumId(m_bColID++);
	if (m_bColID == (BYTE)EC_COL_TYPE::Path)return;
	m_mapObjReady[(UINT)_type].insert(make_pair(id, _pObj));

}

CGameObject * CMapRapper::GetMapObj(UINT _type, UINT _id)
{
	map<UINT, CGameObject*>::iterator iter = m_mapObjReady[_type].find(_id);
	return ((*iter).second);
}

CGameObject * CMapRapper::GetMapObj(const EC_MAP_OBJ_TYPE & _type, UINT _id)
{
	map<UINT, CGameObject*>::iterator iter = m_mapObjReady[(UINT)_type].find(_id);

	return ((*iter).second);
}

void CMapRapper::ReleaseAnyObj(UINT _type, UINT _id)
{
	m_mapObjReady[_type].erase(_id);
}

void CMapRapper::ReleaseAnyObj(const EC_MAP_OBJ_TYPE & _type, UINT _id)
{
	m_mapObjReady[(UINT)_type].erase(_id);
}

void CMapRapper::ArrangeToDead()
{

}

void CMapRapper::ArrangeToStop()
{
	EC_MOSTER_TYPE mopType;
	int stopSearch[(UINT)EC_MOSTER_TYPE::End] = {};
	int stopSearchItem = 0;
	for (int i = 0; i < (UINT)EC_MAP_OBJ_TYPE::End; ++i) {
		map<UINT, CGameObject*>::iterator target;
		for (map<UINT, CGameObject*>::iterator itor = m_mapObjReady[i].begin(); itor != m_mapObjReady[i].end();) {
			//	몬스터들의 시작을 들고오고
			CGameObject* pObj = (*itor).second;
			//	이 몬스터가 널이 아니고, 액티브가 false면
			if (pObj != nullptr && pObj->IsActive() == false) {
				//	타겟에다가 넣어놓고
				target = itor;
				//	그 i가 몬스터면
				if (i == (UINT)EC_MAP_OBJ_TYPE::Monster) {
					//	몬스터 종류를 들고오고
					mopType = (*target).second->StatusComponent()->GetMonsterType();
					//	몬스터 종류들이 들어가있는 스탑에서
					for (int j = stopSearch[UINT(mopType)]; j < m_vecMonsterObjStop[UINT(mopType)].size(); ++j) {
						//	몬스터 종류가 들어간 스탑자리가 비어있다면
						if (m_vecMonsterObjStop[UINT(mopType)][j] == nullptr) {
							//	전체 목록을 전진시키고
							++itor;
							//	몬스터 스탑에다가 지금 타겟을 넣어놓고
							m_vecMonsterObjStop[UINT(mopType)][j] = (*target).second;
							//	몬스터 스탑 개수를 늘려주고
							++m_nCurMopStopCnt[UINT(mopType)];
							//	몬스터를 전체목록에서 지워주고
							m_mapObjReady[i].erase(target);
							//	몬스터 스탑서치목록을 전진
							stopSearch[UINT(mopType)] = j+1;
							//	그리고 몬스터를 넣었으니 break;
							break;
						}
					}
				}
				else {
					//	Item
					for (int j = stopSearchItem; j < m_vecItemObjStop.size(); ++j) {
						//	아이템이 비어있다면
						if (m_vecItemObjStop[j] == nullptr) {
							//	전체목록을 전진
							++itor;
							//	비어있는 타겟목록에 아이템을 넣어주고
							m_vecItemObjStop[j] = (*target).second;
							//	아이템레디 목록을 지워주며
							m_mapObjReady[i].erase(target);
							//	아이템의 서치목록을 증가시켜준다.
							stopSearchItem = j + 1;
							//	아이템의 현재 스탑개수를 늘려준다.
							++m_nCurItemStopCnt;
							break;
						}
					}
				}
			}
			else {
				//	만약 멈춘게 아니라면 그냥 전진
				++itor;
			}
		}
	}
}

void CMapRapper::ArrangeToReady()
{
	int mopType = 0;
	CGameObject* pObj = nullptr;
	
	for (int i = 0; i < (UINT)EC_MAP_OBJ_TYPE::End;  )
	{
		//	만약 i가 몬스터라면
		if (i == (UINT)EC_MAP_OBJ_TYPE::Monster) 
		{
			if (mopType < (UINT)EC_MOSTER_TYPE::End) {
				for (int j = 0; j < m_vecMonsterObjStop[mopType].size(); ++j)
				{
					//	만약에 몬스터 스탑목록중 지금 cnt가 늘어나지 않았다면? 끝내준다
					if (0 == m_nCurMopStopCnt[(mopType)]) { break; }
					//	스탑 몬스터를 일단 들고오고
					pObj = m_vecMonsterObjStop[(mopType)][j];
					//	몬스터중에서 만약 이게 널이 아니고 살아있다면
					if (pObj != nullptr && pObj->IsActive()) {
						//	레디 목록으로 회귀시키고
						m_mapObjReady[i].insert(make_pair(pObj->GetID(), pObj));
						//	스탑목록을 비워주고
						m_vecMonsterObjStop[(mopType)][j] = nullptr;
						//	스탑 개수를 줄여준다
						--m_nCurMopStopCnt[(mopType)];
					}
				}
				++mopType;
			}
			else {
				++i;
			}
			//	몬스터 스탑 목록을 들고온다
		}
		else {//	item
			for (int j = 0; j < m_vecItemObjStop.size();) {
				if (m_nCurItemStopCnt == 0)break;
				pObj = m_vecItemObjStop[j];
				if (pObj != nullptr && pObj->IsActive()) {
					m_mapObjReady[i].insert(make_pair(pObj->GetID(), pObj));
					m_vecItemObjStop[j] = nullptr;
					--m_nCurItemStopCnt;
				}
			}
		}
	}
}
void CMapRapper::PauseReadyObj()
{
	for (int i = 0; i < (UINT)EC_MAP_OBJ_TYPE::End; ++i) {
		for (map<UINT, CGameObject*>::iterator itor = m_mapObjReady[i].begin(); itor != m_mapObjReady[i].end(); ++itor) {
			if (itor->second != nullptr && itor->second->IsActive() == true) {
				itor->second->SetActive(false);
			}
		}
	}
}
void CMapRapper::PlayReadyObj()
{
	for (int i = 0; i < (UINT)EC_MAP_OBJ_TYPE::End; ++i) {
		for (map<UINT, CGameObject*>::iterator itor = m_mapObjReady[i].begin(); itor != m_mapObjReady[i].end(); ++itor) {
			if (itor->second != nullptr && itor->second->IsActive() == false) {
				itor->second->SetActive(true);
			}
		}
	}
}
void CMapRapper::PauseMap()
{
	if(m_pMap != nullptr)m_pMap->SetActive(false);
}
void CMapRapper::PlayMap()
{
	if (m_pMap != nullptr)m_pMap->SetActive(true);
}

void CMapRapper::ReleaseAllObjs()
{
	for (int i = 0; i < (UINT)EC_MAP_OBJ_TYPE::End; ++i) {
		m_mapObjReady[i].clear();
	}
	for (int i = 0; i < (UINT)EC_MOSTER_TYPE::End; ++i) {
		m_vecMonsterObjStop[i].clear();
	}
	m_vecItemObjStop.clear();
}
void CMapRapper::ResetAllObjs()
{
	//	전부 레디오브젝트에 올려놓기
	for (int i = 0; i < (UINT)EC_MOSTER_TYPE::End; ++i) {
		for (int j = 0; j < m_vecMonsterObjStop[i].size(); ++j) {
			if (m_nCurMopStopCnt[i] == 0)break;
			else if (m_vecMonsterObjStop[i][j] != nullptr) {
				m_vecMonsterObjStop[i][j]->SetActive(true);
				m_mapObjReady[i].insert(make_pair(m_vecMonsterObjStop[i][j]->GetID(), m_vecMonsterObjStop[i][j]));
				m_vecMonsterObjStop[i][j] = nullptr;
				--m_nCurMopStopCnt[i];
			}
		}
	}
	for (int i = 0; i < m_vecItemObjStop.size(); ++i) {
		if (m_nCurItemStopCnt == 0)break;
		else if (m_vecItemObjStop[i] != nullptr) {
			m_vecItemObjStop[i]->SetActive(true);
			m_mapObjReady[(UINT)EC_MAP_OBJ_TYPE::Item].insert(make_pair(m_vecItemObjStop[i]->GetID(), m_vecItemObjStop[i]));
			m_vecItemObjStop[i] = nullptr;
			--m_nCurItemStopCnt;
		}
	}
}
void CMapRapper::SetHeightMap(wstring _filePath, int wid, int hei)
{
	m_vHeightSize = { wid,hei };
	wstring filePath = CPathMgr::GetResPath() + _filePath;
	std::ifstream fin(filePath.c_str());
	int size = wid * hei;
	m_pHeightMap = new float[size];
	string f;
	for (int i = 0; i < size; ++i) {
		fin >>f;
		m_pHeightMap[i] = std::stof(f);
	}
	fin.close();
}
void CMapRapper::SetColliderMap(wstring _filePath, int wid, int hei,float _scale)
{

	m_vColSize = { wid ,hei  };//	1200,1200
	m_fColMapScale = _scale;
	wstring filePath = CPathMgr::GetResPath() + _filePath;
	std::ifstream fin(filePath.c_str(), std::ios::binary);
	
	int size = m_vColSize.x * m_vColSize.y;
	m_pColliderMap = new BYTE[size];
	m_pOriginColliderMap = new BYTE[size];
	for (int i = 0; i < size; ++i) {
		m_pColliderMap[i] = BYTE(fin.get());
		m_pOriginColliderMap[i] = m_pColliderMap[i];
	}
	
}
const float CMapRapper::GetCurHeight(float _z, float _x)
{
	if((m_pMapStart.x < _x&&_x < m_pMapEnd.x) && (m_pMapStart.y < _z&&_z < m_pMapEnd.y))return 0.0f;
	else return m_pHeightMap[(int(_z) * m_vHeightSize.x) + int(_x)];
}

const Vec3 & CMapRapper::GetMapPos()
{
	return m_pMap->Transform()->GetLocalPos();
}
const Vec2 & CMapRapper::GetMapPos2()
{
	return m_vMapPos;
}
void CMapRapper::UpdateHeight()
{
	static Vec2 mapSize = { 400.f,400.f };
	static const float fOffset = 0.19f;
	if (m_pHeightMap == nullptr)return;

	Vec3 pos;
	Vec2 fIdx, fDeci;
	float A = 0.0f, B = 0.0f, C = 0.0f, D = 0.0f;
	int x, z;

	map<UINT, CGameObject*>::iterator itor = m_mapObjReady->begin();
	for (; itor != m_mapObjReady->end(); ++itor) {
		if (itor->second != nullptr) {
			pos = itor->second->Transform()->GetLocalPos();
			fIdx = ((Vec2(pos.x, pos.z) - m_vMapPos)) + (mapSize * 0.5f);
			x = fIdx.x; z = fIdx.y;
			fDeci = { fIdx.x - float(x),fIdx.y - float(z) };
			
			A = m_pHeightMap[(z * 400) + x];
			B = m_pHeightMap[(z * 400) + x + 1];
			C = m_pHeightMap[((z + 1) * 400) + x];
			D = m_pHeightMap[((z + 1) * 400) + x + 1];

			pos.y =
				(A * (1.f - fDeci.x)*(1.f - fDeci.y) + B * fDeci.x*(1.f - fDeci.y) + C * (1.f - fDeci.x)*fDeci.y + D * fDeci.x*fDeci.y) * fOffset;
			if (A < pos.y&&B < pos.y&&C < pos.y&&D < pos.y) {
				//	있을수 없는일임 그냥 제일 큰값을 넣어줌
				pos.y = A;
				pos.y = (pos.y < B) ? B : pos.y;
				pos.y = (pos.y < C) ? C : pos.y;
				pos.y = (pos.y < D) ? D : pos.y;
			}


			pos.y *= fOffset;
			itor->second->Transform()->SetLocalPos(pos);
		}
	}
}

void CMapRapper::SetPortal(POINT _lu, POINT _rd, const UINT & _outMapIdx, const Vec3& _startPos,const Vec3& _startRot)
{
	m_arrPortal_Out[_outMapIdx].IsOn = true;
	m_arrPortal_Out[_outMapIdx].StartPos = _startPos + Vec3(m_pMapStart.x,0.0f, m_pMapStart.y);
	m_arrPortal_Out[_outMapIdx].StartRot = _startRot;
	m_arrPortal_Out[_outMapIdx].lu = {float(_lu.x),float(_lu.y)};
	m_arrPortal_Out[_outMapIdx].rd = {float(_rd.x),float(_rd.y)};
}

Vec2 CMapRapper::SearchAround(CGameObject * _pObj, int _z, int _x)
{
	int x = _x - 1, z = _z + 1;
	if (IsOver(m_vColSize,z, x)) {
		if (EC_COL_TYPE::Path == GetColType(x, z)) {
			return Vec2(-1.f,1.f );
		}
	}
	x = _x , z = _z + 1;
	if (IsOver(m_vColSize, z, x)) {
		if (EC_COL_TYPE::Path == GetColType(x, z)) {
			return Vec2( 0.f,1.f );
		}
	}
	x = _x + 1 , z = _z + 1;
	if (IsOver(m_vColSize, z, x)) {
		if (EC_COL_TYPE::Path == GetColType(x, z)) {
			return Vec2( 1.f,1.f );
		}
	}

	x = _x -1, z = _z;
	if (IsOver(m_vColSize, z, x)) {
		if (EC_COL_TYPE::Path == GetColType(x, z)) {
			return Vec2( -1.f,0.f );
		}
	}
	x = _x +1, z = _z ;
	if (IsOver(m_vColSize, z, x)) {
		if (EC_COL_TYPE::Path == GetColType(x, z)) {
			return Vec2( 1.f,0.f );
		}
	}

	x = _x-1, z = _z-1 ;
	if (IsOver(m_vColSize, z, x)) {
		if (EC_COL_TYPE::Path == GetColType(x, z)) {
			return Vec2( -1.f,-1.f );
		}
	}
	x = _x, z = _z - 1;
	if (IsOver(m_vColSize, z, x)) {
		if (EC_COL_TYPE::Path == GetColType(x, z)) {
			return Vec2( 0.f,-1.f );
		}
	}
	x = _x +1, z = _z - 1;
	if (IsOver(m_vColSize, z, x)) {
		if (EC_COL_TYPE::Path == GetColType(x, z)) {
			return Vec2( 1.f,-1.f );
		}
	}
	return Vec2( -2.f,-2.f );
}
const Vec3& CMapRapper::UnitCollider(CGameObject* _pObj, int _z, int _x,bool& _stop)
{
	static const int nBitCnt = 8;
	//	bits				:		0000'0001	0000'0010	0000'0100	0000'1000	0001'0000	0010'0000	0100'0000	1000'0000
	//	index				:		[	0	]	[	1	]	[	2	]	[	3	]	[	4	]	[	5	]	[	6	]	[	7	]
	//	order				:		<	1	>	<	2	>	<	3	>	<	4	>	<	5	>	<	6	>	<	7	>	<	8	>
	static const BYTE bits[nBitCnt] =		{0x01,		0x02,		0x04,		0x08,		0x10,		0x20,		0x40,		0x80 };
	//	order			idx					angle					plusDir
	//	6	7	8		5	6	7		135		90		45			(-1,1)		(0,1)		(1,1)
	//	4		5		3		4		180				0			(-1,0)		(0,0)		(1,0)
	//	3	2	1		2	1	0		225		270		315			(-1,-1)		(0,-1)		(1,-1)
	static const Vec3 around[nBitCnt] = {
			XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(315.f * XM_ANGLE))
		,	XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(270.f * XM_ANGLE))
		,	XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(225.f * XM_ANGLE))
		,	XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(0.f * XM_ANGLE))
		,	XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(180.f * XM_ANGLE))
		,	XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(135.f * XM_ANGLE))
		,	XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(90.f * XM_ANGLE))
		,	XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(45.f * XM_ANGLE))
	};
	static const XMINT2 dir[nBitCnt] = {
			{1,-1}
		,	{0,-1}
		,	{-1,-1}
		,	{-1,0}
		,	{1,0}
		,	{-1,1}
		,	{0,1}
		,	{1,1}
	};
	//	제자리
	static Vec3 tPos;
	tPos = { 0.f,0.f,0.f };

	BYTE cul = 0xff;

	if (_z - 1 < 0) {
		cul &= ~(bits[0] | bits[1] | bits[2]);
	}
	else if (_z + 1 > m_vColSize.y) {
		cul &= ~(bits[5] | bits[6] | bits[7]);
	}
	if (_x - 1 < 0) {
		cul &= ~(bits[2] | bits[3] | bits[5]);
	}
	else if (_x + 1 > m_vColSize.y) {
		cul &= ~(bits[0] | bits[4] | bits[7]);
	}
	int nIdx = 0;
	BYTE type = 0;
	for (int i = 0; i < nBitCnt; ++i) {
		nIdx = 0;
		type = 0;
		if (cul & bits[i])
		{
			nIdx = ((_z + dir[i].y) * m_vColSize.x) + (_x + dir[i].x);
			type = m_pColliderMap[nIdx];
			if (type == (BYTE)EC_COL_TYPE::Path)continue;
			else if (type == _pObj->StatusComponent()->GetColNumId())continue;
			cul &= ~bits[i];
		}
	}
	cul = ~cul;
	
	if (cul & bits[0]) { return around[0]; }
	if (cul & bits[1]) { return around[1]; }
	if (cul & bits[2]) { return around[2]; }
	if (cul & bits[3]) { return around[3]; }
	if (cul & bits[4]) { return around[4]; }
	if (cul & bits[5]) { return around[5]; }
	if (cul & bits[6]) { return around[6]; }
	if (cul & bits[7]) { return around[7]; }
	if (cul & bits[8]) { return around[8]; }
	
	_stop = true;
	return tPos;
}
void CMapRapper::StempingColliderMap(BYTE _colID, int _z, int _x)
{
	//	00
	int nIdx = (_z * m_vColSize.x) + _x;
	int x = 0, z = 0;
	if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
		m_pColliderMap[nIdx] = _colID;
	}
	//	1,+1
	x = 1; z = -1;
	if (-1 < x + _x && x + _x < m_vColSize.x && -1 < z + _z && z + _z < m_vColSize.y) {
		nIdx += (m_vColSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
	x = 0; z = -1;
	if (-1 < x + _x && x + _x < m_vColSize.x && -1 < z + _z && z + _z < m_vColSize.y) {
		//	-1,+1
		nIdx += (m_vColSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
	x = -1; z = -1;
	if (-1 < x + _x && x + _x < m_vColSize.x && -1 < z + _z && z + _z < m_vColSize.y) {
		//	-1,+1
		nIdx += (m_vColSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
	x = 1; z = 0;
	if (-1 < x + _x && x + _x < m_vColSize.x && -1 < z + _z && z + _z < m_vColSize.y) {
		//	-1,+1
		nIdx += (m_vColSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
	x = -1; z = 0;
	if (-1 < x + _x && x + _x < m_vColSize.x && -1 < z + _z && z + _z < m_vColSize.y) {
		//	-1,+1
		nIdx += (m_vColSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
	x = 1; z = 1;
	if (-1 < x + _x && x + _x < m_vColSize.x && -1 < z + _z && z + _z < m_vColSize.y) {
		//	-1,+1
		nIdx += (m_vColSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
	x = 0; z = 1;
	if (-1 < x + _x && x + _x < m_vColSize.x && -1 < z + _z && z + _z < m_vColSize.y) {
		//	-1,+1
		nIdx += (m_vColSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
	x = -1; z = 1;
	if (-1 < x + _x && x + _x < m_vColSize.x && -1 < z + _z && z + _z < m_vColSize.y) {
		//	-1,+1
		nIdx += (m_vColSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
}
void CMapRapper::UpdateCollider()
{
	memcpy(m_pColliderMap, m_pOriginColliderMap, m_nFullSize);
	static Vec2 mapSize = { 400.f,400.f };
	static const Vec2 errPoint = { -2.f,-2.f };
	Vec2 temp;
	if (m_pColliderMap == nullptr)return;
	Vec3 pos;
	Vec2 idx;
	Vec2 fIdx;
	int x, z;
	map<UINT, CGameObject*>::iterator itor = m_mapObjReady->begin();
	UINT nIdx = 0;
	for (; itor != m_mapObjReady->end();++itor) {
		pos = (*itor).second->Transform()->GetLocalPos();
		idx = (Vec2(pos.x,pos.z) - m_vMapPos)+ (mapSize * 0.5f);
		x = idx.x; z = idx.y;
		nIdx = (z*m_vColSize.y) + x;
		switch ((UINT)m_pColliderMap[nIdx])
		{
		case 0://Blocked
		case 2:
			//temp = SearchAround((*itor).second, int(idx.y), int(idx.x));
			//if (errPoint.x != temp.x && errPoint.y != temp.y) {
			//	pos.x += temp.x; pos.y; pos.z += temp.y;
			//	(*itor).second->Transform()->SetLocalPos(pos);
			//}
			(*itor).second->Transform()->SwapPreToPos();
			break;
		case 255:
			m_pColliderMap[nIdx] = 2;
			break;
		}
	}
}
void CMapRapper::Update()
{
	static const float fOffset = 1.0f;
	static Vec2 mapHalfSize = { 400.f * 0.5f,400.f * 0.5f };
	memcpy(m_pColliderMap, m_pOriginColliderMap, m_nFullSize);

	bool bWhatCalcul = false;
	BYTE* pColMap = nullptr;	
	float* pHeiMap = nullptr;
	pColMap = m_pColliderMap;	//	1200 * 1200
	pHeiMap = m_pHeightMap;		//	400	* 400

	if (pColMap == nullptr && pHeiMap == nullptr)return;	//	아예 시작도 하지 않는다.
	//	따로 존재할때는 작동을 시켜준다.
	map<UINT, CGameObject*>::iterator itor = m_mapObjReady[(UINT)EC_MAP_OBJ_TYPE::Monster].begin();	//	시작의 컨테이너
	//	매번 다시만들기 보다는 이런 클래스는 미리 공용을 만들기									
	int nIdxX = 0, nIdxZ = 0,nIdx = 0,nColX = 0,nColZ = 0;		
	bool bStopIt = false;
	//	C -- D
	//	|  / |
	//	A -- B
	//	pos : 오브젝트 위치 ,movPos : 움직인 위치	
	static	Vec3 pos, movPos,vA, vB, vC, vD,vUh,vVh,vNormal,vRot;
	static Vec2 fIdx, fDeci,fColIdx;		
				
	vA = { 0.0f, 0.0f, 0.0f };vB = { 1.0f, 0.0f, 0.0f };
	vC = { 0.0f, 0.0f, 1.0f }; vD = { 1.0f, 0.0f, 1.0f };
	vUh = { 0.f,0.f,0.f }; vVh = { 0.f,0.f,0.f }; 
	vNormal = { 0.f,0.f,0.f }; vRot = { 0.f,0.f,0.f };

	fIdx = { 0.f,0.f }; fDeci = { 0.f,0.f }; fColIdx = { 0.f,0.f };

	int againMax = 3,againCnt = 3;

	for (; itor != m_mapObjReady[(UINT)EC_MAP_OBJ_TYPE::Monster].end(); ) {
		pos = (*itor).second->Transform()->GetLocalPos();	//	객체의 중점좌표를 얻어온다
		fIdx = (Vec2(pos.x, pos.z) - m_vMapPos) + mapHalfSize;	//	좌표계를 맵좌표계로 이동(float)
		//	콜리더 맵확인
		if (pColMap != nullptr) {
			fColIdx = fIdx * m_fColMapScale;	//	콜리더 맵은 여기다가 스케일을 곱해준다
			nIdx = (m_vColSize.x * int(fColIdx.y)) + int(fColIdx.x);
			BYTE meColType = (*itor).second->StatusComponent()->GetColNumId();
			BYTE type = pColMap[nIdx];
			
			if ((BYTE)EC_COL_TYPE::Blocked == type ||
				(BYTE)EC_COL_TYPE::Path != type ||
				meColType != type) {
				movPos = UnitCollider((*itor).second, int(fColIdx.y), int(fColIdx.x), bStopIt);

			//	(*itor).second->Transform()->SwapPreToPos();
				(*itor).second->Transform()->AddForce(movPos*500.f, DT);
				//(*itor).second->Transform()->SetLocalPos(pos + (movPos * 100.f * DT));
				//if (bStopIt) {
				//	(*itor).second->Transform()->SwapPreToPos();
				//	bStopIt = false;
				//	if (againCnt < 0) {
				//		++itor;
				//		againCnt = againMax;
				//		break;
				//	}
				//	--againCnt;
				//}
				//else {
				//	//(*itor).second->Transform()->SwapPreToPos();
				//	//(*itor).second->Transform()->SetLocalPos(pos + (movPos * 100.f * DT));
				//	
				//}
			}
			else {
				againCnt = againMax;
				StempingColliderMap(meColType,fColIdx.y, fColIdx.x);
			}
		}
		//	높이맵 확인
		if (pHeiMap != nullptr) {
			nIdxZ = fIdx.y; nIdxX = fIdx.x;
			fDeci = { fIdx.x - float(nIdxX) ,fIdx.y - float(nIdxZ) };

			vA.y = m_pHeightMap[(nIdxZ * 400) + nIdxX];
			vB.y = m_pHeightMap[(nIdxZ * 400) + nIdxX + 1];
			vC.y = m_pHeightMap[((nIdxZ + 1) * 400) + nIdxX];
			vD.y = m_pHeightMap[((nIdxZ + 1) * 400) + nIdxX + 1];
			
			if (bWhatCalcul) {
				pos.y =
					(vA.y * (1.f - fDeci.x)*(1.f - fDeci.y) + vB.y * fDeci.x*(1.f - fDeci.y) + vC.y * (1.f - fDeci.x)*fDeci.y + vD.y * fDeci.x*fDeci.y) * fOffset;
				if (vA.y < pos.y&&vB.y < pos.y&&vC.y < pos.y&&vD.y < pos.y) {
					//	있을수 없는일임 그냥 제일 큰값을 넣어줌
					pos.y = vA.y;
					pos.y = (pos.y < vB.y) ? vB.y : pos.y;
					pos.y = (pos.y < vC.y) ? vC.y : pos.y;
					pos.y = (pos.y < vD.y) ? vD.y : pos.y;
				}
				pos.y *= fOffset;
				itor->second->Transform()->SetLocalPos(pos);
			}
			else {
				if (fIdx.x >= fIdx.y) {
				//	아래쪽 삼각형
					vUh = (vA - vB);
					vVh = (vD - vB);

					pos.y = vA.y + Linear(0.0f, vUh.y, 1.0f - fDeci.x) + Linear(0.0f, vVh.y, fDeci.y);
				}
				else {
				//	위쪽 삼각형
					vUh = (vD - vC);
					vVh = (vA - vC);

					pos.y = vC.y + Linear(0.0f, vUh.y, fDeci.x) + Linear(0.0f, vVh.y,1.0f - fDeci.y);
				}
				vNormal = (vUh.Cross(vVh)).Normalize();
				if (vNormal.y > 0.f) {
					float xAngle = vNormal.Dot(Vec3::Front);
					float zAngle = vNormal.Dot(Vec3::Right);
					vRot = (*itor).second->Transform()->GetLocalRot();
					vRot.x = xAngle; vRot.z = zAngle;
					(*itor).second->Transform()->SetLocalRot(vRot);
				}
				itor->second->Transform()->SetLocalPos(pos);
			}
		}
	}
}

void CMapRapper::SetMap(CGameObject * _pMap)
{
	m_vMapPos = { _pMap->Transform()->Transform()->GetLocalPos().x, _pMap->Transform()->Transform()->GetLocalPos().z };
	 m_pMap = _pMap; 
}
void CMapRapper::SetAwakeIdx(int * _idxArr, int cnt)
{
	m_vecAwakeIdx.resize(cnt);
	for (int i = 0; i < cnt; ++i) {
		m_vecAwakeIdx[i] = _idxArr[i];
	}
}

bool CMapRapper::IsInPortar(const Vec2 & _playerPos)
{
	for (int i = 0; i < 10; ++i) {
		if (!m_arrPortal_Out[i].IsOn)continue;
		else if (m_arrPortal_Out[i].lu.x < _playerPos.x &&_playerPos.x < m_arrPortal_Out[i].rd.x &&
			m_arrPortal_Out[i].rd.y < _playerPos.y &&_playerPos.y < m_arrPortal_Out[i].lu.y) {
			m_nNextMap = i;

			return true;
		}
	}
	return false;
}

void CMapRapper::CheckCurMonster()
{
	m_nCurMopCnt = m_mapObjReady[(UINT)EC_MAP_OBJ_TYPE::Monster].size();
}

CGameObject* CMapRapper::OnMonster(UINT _type)
{
	for (int i = 0; i < m_vecMonsterObjStop[_type].size(); ++i) {
		if (nullptr != m_vecMonsterObjStop[_type][i] && !m_vecMonsterObjStop[_type][i]->IsActive()) {
			return m_vecMonsterObjStop[_type][i];
		}
	}
}

void CMapRapper::Level0()
{
}

void CMapRapper::Level1()
{





}

void CMapRapper::Level2()
{
}

void CMapRapper::Level3()
{
}

void CMapRapper::Level4()
{
}

void CMapRapper::Level5()
{
}

void CMapRapper::Level6()
{
}

void CMapRapper::Level7()
{
}

void CMapRapper::Level8()
{
}

void CMapRapper::LevelTower()
{
}
