#include "Script.h"

class CTestScript :
	public CScript
{
private:
	Vec3	m_vInitPos;
	Vec3	m_vTargetPos;
	Vec3	m_nDir;
	float	m_fSpeed;
	float	m_fMaxSpeed;
public:	
	virtual void update();
	virtual void OnCollisionEnter(CCollider2D* _pOther);
	virtual void OnCollisionExit(CCollider2D* _pOther);
public:
	CLONE(CTestScript);

public:
	CTestScript();
	virtual ~CTestScript();

	void SetInitPos(const Vec3& _initPos) { m_vInitPos = _initPos; }
	void SetTargetPos(const Vec3& _targetPos);
	void SetSpeed(float _speed) { m_fMaxSpeed = _speed; }
};

