#pragma once
#include "Script.h"
class CEnviromentObj :
	public CScript
{
public:
	CEnviromentObj();
	virtual ~CEnviromentObj();


	virtual void update();
	CLONE(CEnviromentObj);

	virtual void OnCollisionEnter(CCollider2D* _pOther);
	virtual void OnCollision(CCollider2D* _pOther);
	virtual void OnCollisionExit(CCollider2D* _pOther);
};

