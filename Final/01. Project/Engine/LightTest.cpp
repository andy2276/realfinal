#include "stdafx.h"
#include "LightTest.h"
#include "Light3D.h"


CLightTest::CLightTest()
	:CScript((UINT)SCRIPT_TYPE::CAM)
{
}


CLightTest::~CLightTest()
{
}

void CLightTest::update()
{
	bool bMove = false;
	Vec3 vPos = Transform()->GetLocalPos();
	static Vec3 look = Vec3(0.f, 0.f, 600.f);
	
	if (KEY_HOLD(KEY_TYPE::KEY_UP))
	{
		vPos.z += 0.25f;
		bMove = true;
	}
	else if (KEY_HOLD(KEY_TYPE::KEY_DOWN))
	{
		vPos.z -= 0.25f;
		bMove = true;
	}
	if (KEY_HOLD(KEY_TYPE::KEY_LEFT))
	{
		vPos.x -= 0.25f;
		bMove = true;
	}
	else if (KEY_HOLD(KEY_TYPE::KEY_RIGHT))
	{
		vPos.x += 0.25f;
		bMove = true;
	}
	if (KEY_HOLD(KEY_TYPE::KEY_Q))
	{
		vPos.y -= 0.25f;
		bMove = true;
	}
	else if (KEY_HOLD(KEY_TYPE::KEY_E))
	{
		vPos.y += 0.25f;
		bMove = true;
	}



	if (bMove)
	{
		Vec3 lightDir = (look - vPos);
		Light3D()->SetLightDir(lightDir);

		Light3D()->SetLightPos(vPos);
		bMove = false;
	}
	
}
