#include "stdafx.h"
#include "CBoundingBox.h"


CBoundingBox::CBoundingBox()
{
	center[0] = center[1] = center[2] = 0.f;
	extent[0] = extent[1] = extent[2] = 0.f;



	// identity world coordinate axis
	axis[0][0] = 1.0F; axis[0][1] = 0.0F; axis[0][2] = 0.0F;
	axis[1][0] = 0.0F; axis[1][1] = 1.0F; axis[1][2] = 0.0F;
	axis[2][0] = 0.0F; axis[2][1] = 0.0F; axis[2][2] = 1.0F;

}


CBoundingBox::~CBoundingBox()
{
}



void CBoundingBox::CalcOBBBox(const Vec3 _vLocalMin, const Vec3 _vLocalMax, const Vec3 _vScale, Matrix  _pmatRT)
{
	Vec3 vC = (_vLocalMax + _vLocalMin) * 0.5f;



	//local extent
	extent[0] = _vScale.x/2.f;
	extent[1] = _vScale.y/2.f;
	extent[2] = _vScale.z/2.f;



	//world pos
	center[0] = _pmatRT._41;
	center[1] = _pmatRT._42;
	center[2] = _pmatRT._43;



	//world axis
	//분리축
	//회전만이 적용된 매트릭스값을 축으로 사용한다
	//매트릭스에 포지션값이 들어있어도 41,42,43 값은 사용되지 않는다.
	//스케일값이 매트릭스에 적용되어 있으면, 분리축에 대한 충돌검사가 제대로 되지 않는다
	//따라서 RT 매트릭스를 사용한다.

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			axis[i][j] = (_pmatRT)(i, j);
		}
	}

}

//Vec3 CBoundingBox::getHeightVector(SHAPE a) //높이 계산
//{
//	Vec3 ret;
//	ret.x = a.scale.y*sinf(a.rot) / 2;
//	ret.y = -a.scale.y*cosf(a.rot) / 2;
//	return ret;
//}
//
//Vec3 CBoundingBox::getWidthVector(SHAPE a) //너비 계산
//{
//	Vec3 ret;
//	ret.x = a.scale.x*cosf(a.rot) / 2;
//	ret.y = -a.scale.x*sinf(a.rot) / 2;
//	return ret;
//}
//
//Vec3 CBoundingBox::getUnitVector(Vec3 a) //내적 
//{
//
//	Vec3 ret;
//	float size;
//	size = sqrt(a.x*a.x + a.y*a.y);
//	ret.x = a.x / size;
//	ret.y = a.y / size;
//	return ret;
//}
//
//bool CBoundingBox::OBB(SHAPE a, SHAPE b) //충돌 여부 확인 
//{
//	Vec3 dist = a.position - b.position; //a 에서 b 거리  
//	Vec3 vec[4] = { getHeightVector(a),getHeightVector(b),getWidthVector(a),getWidthVector(b) };
//	Vec3 unit;
//	for (int i = 0; i < 4; i++) {
//		double sum = 0;
//		unit = getUnitVector(vec[i]);
//		for (int j = 0; j < 4; j++) {
//			sum += abs(vec[j].x*unit.x + vec[j].y*unit.y);
//		}
//		if (abs(dist.x*unit.x + dist.y*unit.y) > sum) {
//			return false;
//		}
//	}
//	return true;
//}