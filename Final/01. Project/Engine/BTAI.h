#pragma once
#include "Script.h"
namespace BT
{
	namespace Virtual
	{
		class CNode;
	}
	namespace Tool
	{
		class CBlackBoard;
		class CAIProperty;
	}
}
struct AIDesc
{
	struct HP
	{
		float _0;
		float _1;
		float _2;
	};
	HP hp;
	struct Bagic
	{
		float _range;
		float _time;
		float _near;
		float _stay;
	};
	struct Skill
	{
		float _range;
		float _shootTime;
		int _SkillNum;
		float _normalStay;
		float _rageStay;
	};
	struct Attack
	{
		Bagic _bagic;
		Skill _0;	//	for normal
		Skill _1;	//	for rage
	};
	Attack attack;
	struct Status
	{
		float _hp;
		float _movSpeed;
		float _rotSpeed;
		float _near;
	};
	Status status;
};

class CBTAI :
	public CScript
{
private:
	BT::Tool::CBlackBoard*	m_pBlackBoard;
	BT::Tool::CAIProperty*	m_pAiProperty;
	BT::Virtual::CNode*		m_pRoot;

	bool					m_bAIUpdate;
	float					m_fTic;
	float					m_fToc;
	float					m_fTicMax;
public:
	CBTAI();
	virtual ~CBTAI();
public:
	void Init(
		BT::Tool::CBlackBoard* _black,
		CGameObject* _pObj,
		UINT _option = 0);

	void CreateAi(
		const AIDesc& _desc
	);

	void Test(BT::Tool::CBlackBoard* _black);
	void Test2(BT::Tool::CBlackBoard* _black);
	virtual void OnCollision(CCollider2D* _pOther);

	CLONE(CBTAI);
	virtual void update();
};

