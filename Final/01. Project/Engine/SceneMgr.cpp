#include "stdafx.h"
#include "SceneMgr.h"
#include "RenderMgr.h"

#include "Scene.h"
#include "Layer.h"
#include "GameObject.h"

#include "ResMgr.h"
#include "Shader.h"
#include "Mesh.h"
#include "Texture.h"
#include "Transform.h"
#include "MeshRender.h"
#include "Animator2D.h"
#include "Animation2D.h"
#include "Animator3D.h"
#include "Collider2D.h"
#include "Light2D.h"
#include "Light3D.h"
#include "ParticleSystem.h"

#include "TimeMgr.h"
#include "KeyMgr.h"
#include "Camera.h"

#include "CollisionMgr.h"
#include "EventMgr.h"
#include "RenderMgr.h"
#include "Device.h"
#include "Core.h"

#include "PlayerScript.h"
#include "MonsterScript.h"
#include "ToolCamScript.h"
#include "GridScript.h"
#include "BulletScript.h"
#include "StartMapScript.h"

#include "meshdata.h"
#include "TestScript.h"

#include "SponSystem.h"
#include "MapRapper.h"
#include "MapMgr.h"

#include "StatusComponent.h"
#include "MapScript.h"

#include "BulletBossSkillScript.h"

#include "StoneAI.h"
#include "UIscript.h"
#include "CInventoryScript.h"

#include"BulletBossSkillScript.h"
#include"UIscript.h"
#include"CInventoryScript.h"
#include"CMETEOR.h"
#include"CPlayerSkillScript.h"
#include "PixCamera.h"
#include "PathMgr.h"
#include <fstream>
#include"CGoldSkillScript.h"
#include "LightTest.h"
#include "GrassScript.h"

#include "BT.h"
#include "BTInit.h"
#include "BTAI.h"
#include "EffectScript.h"

#include"Sound.h"

CScene* CSceneMgr::GetCurScene()
{
		return m_pCurScene;
}

void CSceneMgr::ChangeScene(CScene * _pNextScene)
{
	SAFE_DELETE(m_pCurScene);
	m_pCurScene = _pNextScene;
}

CSceneMgr::CSceneMgr()
	: m_pCurScene(nullptr)
{
}

CSceneMgr::~CSceneMgr()
{
	SAFE_DELETE(m_pCurScene);
}


void CSceneMgr::CreateTargetUI()
{
	//Vec3 vScale(150.f, 150.f, 1.f);

	//Ptr<CTexture> arrTex[5] = { CResMgr::GetInst()->FindRes<CTexture>(L"DiffuseTargetTex")
	//	, CResMgr::GetInst()->FindRes<CTexture>(L"NormalTargetTex")
	//	, CResMgr::GetInst()->FindRes<CTexture>(L"PositionTargetTex")
	//	, CResMgr::GetInst()->FindRes<CTexture>(L"DiffuseLightTargetTex")
	//	, CResMgr::GetInst()->FindRes<CTexture>(L"SpecularLightTargetTex") };

	//for (UINT i = 0; i < 5; ++i)
	//{
	//	CGameObject* pObject = new CGameObject;
	//	pObject->SetName(L"UI Object");
	//	pObject->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	//	pObject->AddComponent(new CTransform);
	//	pObject->AddComponent(new CMeshRender);

	//	// Transform 설정
//		tResolution res = CRenderMgr::GetInst()->GetResolution();

	//	pObject->Transform()->SetLocalPos(Vec3(-(res.fWidth / 2.f) + (vScale.x / 2.f) + (i * vScale.x)
	//									, -(res.fHeight / 2.f) + (vScale.y / 2.f)
	//									, 1.f));

	//	pObject->Transform()->SetLocalScale(vScale);
	//	
	//	// MeshRender 설정
	//	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	//	Ptr<CMaterial> pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
	//	pObject->MeshRender()->SetMaterial(pMtrl->Clone());
	//	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, arrTex[i].GetPointer());
	//
	//	// AddGameObject
	//	m_pCurScene->FindLayer(L"UI")->AddGameObject(pObject);
	//}	



	tResolution res = CRenderMgr::GetInst()->GetResolution();


	Vec3 vScale2 = Vec3(85, 70.f, 1.f);



	//============================================================================================================
	// 0731 UI 작업
	//============================================================================================================


	CGameObject * pObject = new CGameObject;
	pObject->SetName(L"UI Border");
	pObject->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);

	// Transform 설정
	//tResolution res = CRenderMgr::GetInst()->GetResolution();

	// -( 화면가로 크기/2 ) + (크기/2 ) + ( i * 크기) ,  ( 화면높이/2 ) - ( 크기 / 2) , 1.f

	// k : 앞에 띄울 공간 크기 비율
	//x 좌표 : -(화면가로크기 / 2) + ( 화면크기  / k )
	//y 좌표 :  -(화면세로크기 / 2) + ( 유아이 크기 ) 

	float frontRatio = 4;
	Vec3 vScale(700, 150.f, 1.f);
	//여기
	pObject->Transform()->SetLocalPos(Vec3(0, -(res.fHeight / 2.f) + (vScale.y / 2.f), 1.2f));
	//	pObject->Transform()->SetLocalPos(Vec3(0, 0, 1.002f));

	pObject->Transform()->setPhysics(false);


	pObject->Transform()->SetLocalScale(vScale);

	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	Ptr<CMaterial> pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
	pObject->MeshRender()->SetMaterial(pMtrl->Clone());
	Ptr<CTexture> pBorder = CResMgr::GetInst()->Load<CTexture>(L"scrollui11", L"Texture\\scrollui11.png");
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pBorder.GetPointer());

	m_pCurScene->FindLayer(L"UI")->AddGameObject(pObject);




	//============================================================================================================
	// 미니 맵
	//============================================================================================================
	//
	//	pObject = new CGameObject;
	//	pObject->SetName(L"UI Border");
	//	pObject->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	//	pObject->AddComponent(new CTransform);
	//	pObject->AddComponent(new CMeshRender);
	//
	//
	//	vScale = Vec3(200, 200.f, 1.f);
	//
	//	
	//	pObject->Transform()->setPhysics(false);
	//	pObject->Transform()->SetLocalScale(vScale);
	////	pObject->Transform()->SetLocalPos(Vec3(+(res.fWidth / 2.f) - 30 - (vScale.x /2.f), +(res.fHeight ) - 15 - (vScale.y) , 1.f));
	//	pObject->Transform()->SetLocalPos(Vec3(+(res.fWidth / 2.f) - 20 - (vScale.x / 2.f), +(res.fHeight/2.f) - 20 - (vScale.y/2.f), 1.f));
	//
	//	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	//	pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
	//	pObject->MeshRender()->SetMaterial(pMtrl->Clone());
	//
	//	pBorder = CResMgr::GetInst()->Load<CTexture>(L"minimap", L"Texture\\minimap_1f.png");
	//	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pBorder.GetPointer());
	//
	//	pObject->AddComponent(new CTestScript);
	//
	//	//	virtual void OnCollisionExit(CCollider2D* _pOther);
	//	//	pObject->AddComponent(new CMonsterScript);
	//
	////	pObject->AddComponent(new CCollider2D);
	////	pObject->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::UIRECT);
	////	pObject->Collider2D()->setUIsize(Vec2(vScale.x / 3, vScale.y / 3));
	//
	//	m_pCurScene->FindLayer(L"UI")->AddGameObject(pObject);
	//
	//


	
		

}

void CSceneMgr::init()
{

	//---------------------------------------------------------------------------------------------
	//	설정
	bool	bIsToolCam = false;
	bool bFBXOn = false;
	bool bNeoFBX = false;
	bool bLightMove = true;
	bool bLightCam = false;

	//---------------------------------------------------------------------------------------------
	//	필요 포인터 생성
	CGameObject* pMap[10];
	CGameObject* pObj = nullptr;
	CGameObject* pMainCam = nullptr;
	CGameObject* pPlayer = nullptr;
	CGameObject* pTemp = nullptr;
	CGameObject* pTower = nullptr;

	CGameObject* pGrass = nullptr;

	CMesh*		pMesh = nullptr;
	CMapScript* cScript = nullptr;
	CMapScript* cTestMapScript = nullptr;
	CStartMapScript* startMap = nullptr;
	CMapScript*	pMapScript[10];

	int mapIdx = 0;

	//---------------------------------------------------------------------------------------------
	//	최초 씬 생성
	{
		m_pCurScene = new CScene;
		m_pCurScene->SetName(L"InitScene");
	}
	//	레이어 생성
	m_pCurScene->GetLayer(0)->SetName(L"Default");
	m_pCurScene->GetLayer(1)->SetName(L"Player");
	m_pCurScene->GetLayer(2)->SetName(L"Monster");

	m_pCurScene->GetLayer(3)->SetName(L"Bullet");
	m_pCurScene->GetLayer(4)->SetName(L"Map");
	m_pCurScene->GetLayer(5)->SetName(L"Enviroment");

	m_pCurScene->GetLayer(6)->SetName(L"Sky");
	m_pCurScene->GetLayer(7)->SetName(L"Particle");
	m_pCurScene->GetLayer(8)->SetName(L"Item");

	m_pCurScene->GetLayer(9)->SetName(L"MonsterBullet");
	m_pCurScene->GetLayer(10)->SetName(L"notInfluenceAnything");


	m_pCurScene->GetLayer(28)->SetName(L"ReadyMonster");
	m_pCurScene->GetLayer(29)->SetName(L"PostEffect");
	m_pCurScene->GetLayer(30)->SetName(L"UI");
	m_pCurScene->GetLayer(31)->SetName(L"Tool");

	//---------------------------------------------------------------------------------------------
	//	리소스 생성
	Ptr<CMeshData> mapMeshData[10];
	Ptr<CMeshData> playerData;
	Ptr<CMeshData> pMonster[3];
	Ptr<CMeshData> pTrees[7];

	Ptr<CMeshData> pStone[7];

	Ptr<CMeshData> pMonsterSkill[10];
	Ptr< CMeshData> pMidFar[4];
	Ptr<CMeshData> pScroll;

	Ptr<CMeshData> pStoneBossLow;
	Ptr<CMeshData> pMirrorBoss;
	//cheeseballx34D.FBX
	//타워
	Ptr<CMeshData> towerData;
	Ptr<CMeshData> tempData;
	//	텍스쳐
	Ptr<CTexture>	pSky;
	Ptr<CTexture>	pTexForest;
	Ptr<CTexture> pPositionTargetTex;
	Ptr<CTexture> pPostEffectTex;
	Ptr<CTexture> pTestGlass1;
	Ptr<CTexture> pTestGlass2;
	Ptr<CTexture> pTestGlass3;
	Ptr<CTexture> pTestGlass4;
	Ptr<CTexture> pTestGlass5;
	Ptr<CTexture> pTestGlass6;
	Ptr<CTexture> pTestGlass7;
	Ptr<CTexture> pNeoTex;
	Ptr<CTexture> pNeoTowerTex;
	//	텍스쳐 - 몬스터 이펙트
	Ptr< CTexture> pMopYellowBlack;
	Ptr<CTexture> pSummon1;

	//	Mtrl
	Ptr<CMaterial> SkyMtrl;
	Ptr<CMaterial> TexMtrl;
	Ptr<CMaterial> ForestMtrl;

	//	setValue
	Vec3 lightPos = Vec3(0.f, 4000.f, 800.f);
	Vec3 lookPos = Vec3(0.f, 0.f, 800.f);

	//	newTex.png
	//	TestGlass.jpg
	//	Texture
	{
		//forest1.png
		pSky = CResMgr::GetInst()->Load<CTexture>(L"Sky01", L"Texture\\Skybox\\skybox.jpg");
		pTexForest = CResMgr::GetInst()->Load<CTexture>(L"MidForest", L"Texture\\forest1.png");
		
		SkyMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"SkyboxMtrl");
		TexMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
		ForestMtrl = TexMtrl->Clone();
		

		pMopYellowBlack = CResMgr::GetInst()->Load<CTexture>(L"YellowBlcak", L"Texture\\MagicCircle\\YellowBlack.png");
		pSummon1 = CResMgr::GetInst()->Load<CTexture>(L"summ0n1", L"Texture\\MagicCircle\\Summon1.png");
		pNeoTex = CResMgr::GetInst()->Load<CTexture>(L"neoTex", L"Texture\\newTex.png");
		pNeoTowerTex = CResMgr::GetInst()->Load<CTexture>(L"neoTowerTex", L"Texture\\top3.jpg");
	}
	

	//		SetMapResource

	//pMeshData1->Save(pMeshData1->GetPath());
	//
	//Ptr<CMeshData> pMeshData1 = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Lv1map.mdat", L"MeshData\\Lv1map.mdat");
	CPixCamera* pixCamera = new CPixCamera;
	Vec3 initPos = Vec3(5.f, 18.f, -50.0f);
	Vec3 initRot = Vec3(XM_ANGLE * 8.f, 0.0f, 0.0f);
	//	MainCam
	{
		pMainCam = new CGameObject;
		pMainCam->SetName(L"MainCam");
		pMainCam->AddComponent(new CTransform);
		pMainCam->AddComponent(new CCamera);
		if (bIsToolCam)pMainCam->AddComponent(new CToolCamScript);

		pMainCam->AddComponent(pixCamera);

		pMainCam->Camera()->SetProjType(PROJ_TYPE::PERSPECTIVE);
		pMainCam->Camera()->SetFar(100000.f);

		pMainCam->Camera()->SetLayerAllCheck();
		pMainCam->Camera()->SetLayerCheck(28, false);
		pMainCam->Camera()->SetLayerCheck(30, false);
		//기존 카메라뷰
//		pMainCam->Transform()->SetLocalPos(Vec3(5.f, 15.f, -26.0f));
//		pMainCam->Transform()->SetLocalRot(Vec3(XM_ANGLE * 10.f, 0.0f, 0.0f));


		pMainCam->Transform()->SetLocalPos(initPos);
		pMainCam->Transform()->SetLocalRot(initRot);
		if (bLightCam)
		{
			pMainCam->Transform()->SetLocalPos(lightPos);
			pMainCam->Transform()->SetLocalRot(initRot);
		}

		pixCamera->SetOriginPos(initPos);
		pixCamera->SetOriginRot(initRot);

		m_pCurScene->FindLayer(L"Default")->AddGameObject(pMainCam);


		CGameObject* pUICam = new CGameObject;
		pUICam->SetName(L"UIcam");
		pUICam->AddComponent(new CTransform);
		pUICam->AddComponent(new CCamera);


		pUICam->Camera()->SetProjType(PROJ_TYPE::ORTHOGRAPHIC);
		pUICam->Camera()->SetFar(100.f);
		pUICam->Camera()->SetNear(1.f);
		pUICam->Camera()->SetLayerCheck(30, true);
		pUICam->Camera()->SetWidth(CRenderMgr::GetInst()->GetResolution().fWidth);
		pUICam->Camera()->SetHeight(CRenderMgr::GetInst()->GetResolution().fHeight);
		pUICam->Transform()->setPhysics(false);

		m_pCurScene->FindLayer(L"Default")->AddGameObject(pUICam);

		CreateTargetUI();

	}
	//---------------------------------------------------------------------------------------------
	//	Dir Light
	{
		//lightPos = Vec3(1000.f, 4000.f, -1000.f);
		lookPos = Vec3(-300.f, -100.f, 1500.f); 
		lightPos = Vec3(0.f, 600.f, -1600.f);

		pObj = new CGameObject;
		pObj->AddComponent(new CTransform);
		pObj->AddComponent(new CLight3D);

	//	pObj->Transform()->SetLocalPos(lightPos);
		pObj->Light3D()->SetLightType(LIGHT_TYPE::DIR);

		Vec3 lightDir = (lookPos - lightPos).Normalize();
		//pObj->Light3D()->SetLightDir(lightDir);
		//pObj->Light3D()->SetLightPos(lightPos);

		pObj->Light3D()->SetLightPos(lightPos);
		pObj->Transform()->SetLocalPos(lightPos);
		pObj->Light3D()->SetLightDir(lightDir);

		pObj->Light3D()->SetLightRange(100000.f);
		pObj->Light3D()->SetDiffuseColor(Vec3(1.f, 1.f, 1.f));
		pObj->Light3D()->SetSpecular(Vec3(0.2f, 0.2f, 0.2f));
		pObj->Light3D()->SetAmbient(Vec3(0.5f, 0.5f, 0.5f));
		
		if(bLightMove)pObj->AddComponent(new CLightTest);

	//	pObj->Light3D()->ChangeCam(pMainCam);
		pObj->Light3D()->SetMainCam(pMainCam);
	//	pObj->Transform()->SetLocalPos(lightPos);
		m_pCurScene->FindLayer(L"Default")->AddGameObject(pObj);
	}
	//{
	//	pObj = new CGameObject;
	//	pObj->AddComponent(new CTransform);
	//	pObj->AddComponent(new CLight3D);
	//
	//	Vec3 lightPos = Vec3(0.f, 1500.f, 1500.f);
	//	Vec3 lookPos = Vec3(0.f, 0.f, 600.f);
	//
	//	//	pObj->Transform()->SetLocalPos(lightPos);
	//	pObj->Light3D()->SetLightType(LIGHT_TYPE::DIR);
	//	pObj->Light3D()->SetLightPos(lightPos);
	//
	//	Vec3 lightDir = (lookPos - lightPos);
	//	pObj->Light3D()->SetLightDir(lightDir);
	//
	//	pObj->Light3D()->SetDiffuseColor(Vec3(1.f, 1.f, 1.f));
	//	pObj->Light3D()->SetSpecular(Vec3(0.2f, 0.2f, 0.2f));
	//	pObj->Light3D()->SetAmbient(Vec3(0.2f, 0.2f, 0.2f));
	//
	//	pObj->Light3D()->SetLightRange(100000.f);
	////	if (bLightMove)pObj->AddComponent(new CLightTest);
	//
	//	m_pCurScene->FindLayer(L"Default")->AddGameObject(pObj);
	//}
	//{
	//	pObj = new CGameObject;
	//	pObj->AddComponent(new CTransform);
	//	pObj->AddComponent(new CLight3D);
	//
	//	Vec3 lightPos = Vec3(-100.f, 1500.f, 1500.f);
	//	Vec3 lookPos = Vec3(0.f, 0.f, 600.f);
	//
	//	//	pObj->Transform()->SetLocalPos(lightPos);
	//	pObj->Light3D()->SetLightType(LIGHT_TYPE::DIR);
	//	pObj->Light3D()->SetLightPos(lightPos);
	//
	//	Vec3 lightDir = (lookPos - lightPos);
	//	pObj->Light3D()->SetLightDir(lightDir);
	//
	//	pObj->Light3D()->SetDiffuseColor(Vec3(1.f, 1.f, 1.f));
	//	pObj->Light3D()->SetSpecular(Vec3(0.2f, 0.2f, 0.2f));
	//	pObj->Light3D()->SetAmbient(Vec3(0.2f, 0.2f, 0.2f));
	//
	//	pObj->Light3D()->SetLightRange(100000.f);
	//	//	if (bLightMove)pObj->AddComponent(new CLightTest);
	//
	//	m_pCurScene->FindLayer(L"Default")->AddGameObject(pObj);
	//}

	//---------------------------------------------------------------------------------------------
	//	SetFBX Resource
	if (bFBXOn)
	{
		//butterfly.FBX
		mapMeshData[0] = CResMgr::GetInst()->LoadFBX(L"FBX\\Lv0_02.FBX");
		{
			pMesh = mapMeshData[0]->GetMesh();
			pMesh->ClipedAnimationReset();
			pMesh->ClipedAnimationResize(7);
			pMesh->ClipedAnimation(L"Close", 0, 0, 0);
			pMesh->ClipedAnimation(L"Opening", 1, 1, 89);
			pMesh->ClipedAnimation(L"Open", 2, 90, 90);
		}

		mapMeshData[1] = CResMgr::GetInst()->LoadFBX(L"FBX\\Maps\\Lv01Nobj.FBX");
		mapMeshData[2] = CResMgr::GetInst()->LoadFBX(L"FBX\\Maps\\Lv02Nobj.FBX");
		mapMeshData[3] = CResMgr::GetInst()->LoadFBX(L"FBX\\Maps\\Lv03Nobj.FBX");
		mapMeshData[4] = CResMgr::GetInst()->LoadFBX(L"FBX\\Maps\\Lv04Nobj.FBX");
		mapMeshData[5] = CResMgr::GetInst()->LoadFBX(L"FBX\\Maps\\Lv05Nobj.FBX");
		mapMeshData[6] = CResMgr::GetInst()->LoadFBX(L"FBX\\Maps\\Lv06Nobj.FBX");
		mapMeshData[7] = CResMgr::GetInst()->LoadFBX(L"FBX\\Maps\\Lv07Nobj.FBX");
		mapMeshData[8] = CResMgr::GetInst()->LoadFBX(L"FBX\\Maps\\Lv08Nobj.FBX");
		mapMeshData[9] = CResMgr::GetInst()->LoadFBX(L"FBX\\test05.FBX");

		pMidFar[0] = CResMgr::GetInst()->LoadFBX(L"FBX\\backG218.FBX");	//	down
		pMidFar[1] = CResMgr::GetInst()->LoadFBX(L"FBX\\backG234.FBX");	//	left
		pMidFar[2] = CResMgr::GetInst()->LoadFBX(L"FBX\\backG456.FBX");	//	up
		pMidFar[3] = CResMgr::GetInst()->LoadFBX(L"FBX\\backG678.FBX");	//	right

		towerData = CResMgr::GetInst()->LoadFBX(L"FBX\\tower06.FBX");
		playerData = CResMgr::GetInst()->LoadFBX(L"FBX\\cheeseballx29.FBX");
		{
			pMesh = playerData->GetMesh();
			pMesh->ClipedAnimationReset();
			pMesh->ClipedAnimationResize(7);
			pMesh->ClipedAnimation(L"Idle", 0, 0, 30);
			pMesh->ClipedAnimation(L"Run", 1, 31, 61);
			pMesh->ClipedAnimation(L"Damage", 2, 62, 90);
			pMesh->ClipedAnimation(L"Death", 3, 91, 131);
			pMesh->ClipedAnimation(L"Attack", 4, 132, 158);
			pMesh->ClipedAnimation(L"Skill", 5, 159, 219);
			pMesh->ClipedAnimation(L"Stun", 6, 220, 250);
		}
		pMonster[(UINT)EC_MOSTER_TYPE::Stone] = CResMgr::GetInst()->LoadFBX(L"FBX\\stone_soul.FBX");
		{
			pMesh = pMonster[(UINT)EC_MOSTER_TYPE::Stone]->GetMesh();
			pMesh->ClipedAnimationReset();
			pMesh->ClipedAnimationResize(5);
			pMesh->ClipedAnimation(L"IdleOrRun", 0, 0, 59);
			pMesh->ClipedAnimation(L"Attack", 1, 60, 89);
			pMesh->ClipedAnimation(L"Damage", 2, 90, 119);
			pMesh->ClipedAnimation(L"Death", 3, 120, 159);
			pMesh->ClipedAnimation(L"Skill1", 4, 160, 270);
		}
		pMonster[(UINT)EC_MOSTER_TYPE::Skelleton] = CResMgr::GetInst()->LoadFBX(L"FBX\\skell01.FBX");
		{
			pMesh = pMonster[(UINT)EC_MOSTER_TYPE::Skelleton]->GetMesh();
			pMesh->ClipedAnimationReset();
			pMesh->ClipedAnimationResize(6);
			pMesh->ClipedAnimation(L"Idle", 0, 0, 59);
			pMesh->ClipedAnimation(L"Attack", 1, 60, 100);
			pMesh->ClipedAnimation(L"Damage", 2, 101, 119);
			pMesh->ClipedAnimation(L"Death", 3, 120, 140);
			pMesh->ClipedAnimation(L"Run", 4, 141, 180);
			pMesh->ClipedAnimation(L"FastRun", 5, 181, 200);
		}
		pMonster[(UINT)EC_MOSTER_TYPE::Mimic] = CResMgr::GetInst()->LoadFBX(L"FBX\\mimic.FBX");
		{
			pMesh = pMonster[(UINT)EC_MOSTER_TYPE::Mimic]->GetMesh();
			pMesh->ClipedAnimationReset();
			pMesh->ClipedAnimationResize(4);
			pMesh->ClipedAnimation(L"Open", 0, 0, 29);
			pMesh->ClipedAnimation(L"Close", 1, 30, 59);
			pMesh->ClipedAnimation(L"Jump", 2, 60, 89);
			pMesh->ClipedAnimation(L"Idle", 3, 90, 149);
		}
	
		//	stoneA;
		pTrees[0] = CResMgr::GetInst()->LoadFBX(L"FBX\\Trees\\treeA1.FBX");
		pTrees[1] = CResMgr::GetInst()->LoadFBX(L"FBX\\Trees\\treeA2.FBX");
		pTrees[2] = CResMgr::GetInst()->LoadFBX(L"FBX\\Trees\\treeB1.FBX");
		pTrees[3] = CResMgr::GetInst()->LoadFBX(L"FBX\\Trees\\treeB2.FBX");
		pTrees[4] = CResMgr::GetInst()->LoadFBX(L"FBX\\Trees\\treeB3.FBX");
		pTrees[5] = CResMgr::GetInst()->LoadFBX(L"FBX\\Trees\\treeC.FBX");
		pTrees[6] = CResMgr::GetInst()->LoadFBX(L"FBX\\Trees\\treeD.FBX");

		pStone[0] = CResMgr::GetInst()->LoadFBX(L"FBX\\stoneA.FBX");
		pStone[1] = CResMgr::GetInst()->LoadFBX(L"FBX\\stoneB.FBX");
		pStone[2] = CResMgr::GetInst()->LoadFBX(L"FBX\\stoneC.FBX");
		pStone[3] = CResMgr::GetInst()->LoadFBX(L"FBX\\stoneD.FBX");
		pStone[4] = CResMgr::GetInst()->LoadFBX(L"FBX\\stoneE.FBX");
		pStone[5] = CResMgr::GetInst()->LoadFBX(L"FBX\\stoneF.FBX");
		pStone[6] = CResMgr::GetInst()->LoadFBX(L"FBX\\stoneG.FBX");

		pMonsterSkill[0] = CResMgr::GetInst()->LoadFBX(L"FBX\\stone_s.FBX");
		pMonsterSkill[1] = CResMgr::GetInst()->LoadFBX(L"FBX\\meteo05.FBX");

		pScroll = CResMgr::GetInst()->LoadFBX(L"FBX\\scroll02.fbx");

		pStoneBossLow = CResMgr::GetInst()->LoadFBX(L"FBX\\boss_stone02.FBX");
		{
			pMesh = pStoneBossLow->GetMesh();
			pMesh->ClipedAnimationReset();
			pMesh->ClipedAnimationResize(5);
			pMesh->ClipedAnimation(L"Idle", 0, 0, 59);
			pMesh->ClipedAnimation(L"Attack", 1, 60, 90);
			pMesh->ClipedAnimation(L"Damage", 2, 90, 120);
			pMesh->ClipedAnimation(L"Death", 3, 120, 190);
			pMesh->ClipedAnimation(L"Skill", 4, 190, 299);
		}
		pMirrorBoss = CResMgr::GetInst()->LoadFBX(L"FBX\\cheeseballx34D.FBX");
		{
			pMesh = pMirrorBoss->GetMesh();
			pMesh->ClipedAnimationReset();
			pMesh->ClipedAnimationResize(7);
			pMesh->ClipedAnimation(L"Idle", 0, 0, 30);
			pMesh->ClipedAnimation(L"Run", 1, 31, 61);
			pMesh->ClipedAnimation(L"Damage", 2, 62, 90);
			pMesh->ClipedAnimation(L"Death", 3, 91, 131);
			pMesh->ClipedAnimation(L"Attack", 4, 132, 158);
			pMesh->ClipedAnimation(L"Skill", 5, 159, 219);
			pMesh->ClipedAnimation(L"Stun", 6, 220, 250);
		}
		//	save
		if(false)
		{

			for (int i = 0; i < 10; ++i)
			{
				mapMeshData[i]->Save(mapMeshData[i]->GetPath());
			}
			for (int i = 0; i < 4; ++i)
			{
				pMidFar[i]->Save(pMidFar[i]->GetPath());
			}
			for (int i = 0; i < (UINT)EC_MOSTER_TYPE::End; ++i)
			{
				pMonster[i]->Save(pMonster[i]->GetPath());
			}
			for (int i = 0; i < 7; ++i)
			{
				pTrees[i]->Save(pTrees[i]->GetPath());
				pStone[i]->Save(pStone[i]->GetPath());
			}

			pMonsterSkill[0]->Save(pMonsterSkill[0]->GetPath());
			pMonsterSkill[1]->Save(pMonsterSkill[1]->GetPath());
			pScroll->Save(pScroll->GetPath());
			pStoneBossLow->Save(pStoneBossLow->GetPath());
			playerData->Save(playerData->GetPath());
			towerData->Save(towerData->GetPath());
			pMirrorBoss->Save(pMirrorBoss->GetPath());
		}
	}
	else //	mdat
	{
		mapMeshData[0] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Lv0_02.mdat", L"MeshData\\Lv0_02.mdat");
		{
			pMesh = mapMeshData[0]->GetMesh();
			pMesh->ClipedAnimationReset();
			pMesh->ClipedAnimationResize(7);
			pMesh->ClipedAnimation(L"Close", 0, 0, 0);
			pMesh->ClipedAnimation(L"Opening", 1, 1, 89);
			pMesh->ClipedAnimation(L"Open", 2, 90, 90);
		}

		mapMeshData[1] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Lv01Nobj.mdat", L"MeshData\\Lv01Nobj.mdat");
		mapMeshData[2] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Lv02Nobj.mdat", L"MeshData\\Lv02Nobj.mdat");
		mapMeshData[3] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Lv03Nobj.mdat", L"MeshData\\Lv03Nobj.mdat");
		mapMeshData[4] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Lv04Nobj.mdat", L"MeshData\\Lv04Nobj.mdat");
		mapMeshData[5] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Lv05Nobj.mdat", L"MeshData\\Lv05Nobj.mdat");
		mapMeshData[6] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Lv06Nobj.mdat", L"MeshData\\Lv06Nobj.mdat");
		mapMeshData[7] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Lv07Nobj.mdat", L"MeshData\\Lv07Nobj.mdat");
		mapMeshData[8] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Lv08Nobj.mdat", L"MeshData\\Lv08Nobj.mdat");
		mapMeshData[9] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\test05.mdat", L"MeshData\\test05.mdat");
		playerData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\cheeseballx29.mdat", L"MeshData\\cheeseballx29.mdat");
		{
			pMesh = playerData->GetMesh();
			pMesh->ClipedAnimationReset();
			pMesh->ClipedAnimationResize(7);
			pMesh->ClipedAnimation(L"Idle", 0, 0, 30);
			pMesh->ClipedAnimation(L"Run", 1, 31, 61);
			pMesh->ClipedAnimation(L"Damage", 2, 62, 90);
			pMesh->ClipedAnimation(L"Death", 3, 91, 131);
			pMesh->ClipedAnimation(L"Attack", 4, 132, 158);
			pMesh->ClipedAnimation(L"Skill", 5, 159, 219);
			pMesh->ClipedAnimation(L"Stun", 6, 220, 250);
		}
		pMonster[(UINT)EC_MOSTER_TYPE::Stone] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\stone_soul.mdat", L"MeshData\\stone_soul.mdat");
		{
			pMesh = pMonster[(UINT)EC_MOSTER_TYPE::Stone]->GetMesh();
			pMesh->ClipedAnimationReset();
			pMesh->ClipedAnimationResize(5);
			pMesh->ClipedAnimation(L"IdleOrRun", 0, 0, 59);
			pMesh->ClipedAnimation(L"Attack", 1, 60, 89);
			pMesh->ClipedAnimation(L"Damage", 2, 90, 119);
			pMesh->ClipedAnimation(L"Death", 3, 120, 159);
			pMesh->ClipedAnimation(L"Skill1", 4, 160, 270);
		}
		pMonster[(UINT)EC_MOSTER_TYPE::Skelleton] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\skell01.mdat", L"MeshData\\skell01.mdat");
		{
			pMesh = pMonster[(UINT)EC_MOSTER_TYPE::Skelleton]->GetMesh();
			pMesh->ClipedAnimationReset();
			pMesh->ClipedAnimationResize(6);
			pMesh->ClipedAnimation(L"Idle", 0, 0, 59);
			pMesh->ClipedAnimation(L"Attack", 1, 60, 100);
			pMesh->ClipedAnimation(L"Damage", 2, 101, 119);
			pMesh->ClipedAnimation(L"Death", 3, 120, 140);
			pMesh->ClipedAnimation(L"Run", 4, 141, 180);
			pMesh->ClipedAnimation(L"FastRun", 5, 181, 199);
		}
		pMonster[(UINT)EC_MOSTER_TYPE::Mimic] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\mimic.mdat", L"MeshData\\mimic.mdat");
		{
			pMesh = pMonster[(UINT)EC_MOSTER_TYPE::Mimic]->GetMesh();
			pMesh->ClipedAnimationReset();
			pMesh->ClipedAnimationResize(4);
			pMesh->ClipedAnimation(L"Open", 0, 0, 29);
			pMesh->ClipedAnimation(L"Close", 1, 30, 59);
			pMesh->ClipedAnimation(L"Jump", 2, 60, 89);
			pMesh->ClipedAnimation(L"Idle", 3, 90, 149);
		}
		towerData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\tower06.mdat", L"MeshData\\tower06.mdat");

		pMidFar[0] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\backG218r.mdat", L"MeshData\\backG218r.mdat");
		pMidFar[1] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\backG234r.mdat", L"MeshData\\backG234r.mdat");
		pMidFar[2] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\backG456r.mdat", L"MeshData\\backG456r.mdat");
		pMidFar[3] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\backG678r.mdat", L"MeshData\\backG678r.mdat");

		pTrees[0] =  CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\treeA1.mdat", L"MeshData\\treeA1.mdat");
		pTrees[1] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\treeA2.mdat", L"MeshData\\treeA2.mdat");
		pTrees[2] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\treeB1.mdat", L"MeshData\\treeB1.mdat");
		pTrees[3] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\treeB2.mdat", L"MeshData\\treeB2.mdat");
		pTrees[4] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\treeB3.mdat", L"MeshData\\treeB3.mdat");
		pTrees[5] =  CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\treeC.mdat", L"MeshData\\treeC.mdat");
		pTrees[6] =  CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\treeD.mdat", L"MeshData\\treeD.mdat");

		pStone[0] =  CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\stoneA.mdat", L"MeshData\\stoneA.mdat");
		pStone[1] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\stoneB.mdat", L"MeshData\\stoneB.mdat");
		pStone[2] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\stoneC.mdat", L"MeshData\\stoneC.mdat");
		pStone[3] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\stoneD.mdat", L"MeshData\\stoneD.mdat");
		pStone[4] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\stoneE.mdat", L"MeshData\\stoneE.mdat");
		pStone[5] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\stoneF.mdat", L"MeshData\\stoneF.mdat");
		pStone[6] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\stoneG.mdat", L"MeshData\\stoneG.mdat");


		pMonsterSkill[0] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\stone_s.mdat", L"MeshData\\stone_s.mdat");
		pMonsterSkill[1] = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\meteo05.mdat", L"MeshData\\meteo05.mdat");
		pScroll = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\scroll02.mdat", L"MeshData\\scroll02.mdat");

		pStoneBossLow =CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\boss_stone02.mdat", L"MeshData\\boss_stone02.mdat");
		{
			pMesh = pStoneBossLow->GetMesh();
			pMesh->ClipedAnimationReset();
			pMesh->ClipedAnimationResize(5);
			pMesh->ClipedAnimation(L"Idle", 0, 0, 59);
			pMesh->ClipedAnimation(L"Attack", 1, 60, 90);
			pMesh->ClipedAnimation(L"Damage", 2, 90, 120);
			pMesh->ClipedAnimation(L"Death", 3, 120, 190);
			pMesh->ClipedAnimation(L"Skill", 4, 190, 299);
		}
		pMirrorBoss = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\cheeseballx34D.mdat", L"MeshData\\cheeseballx34D.mdat");
		{
			pMesh = pMirrorBoss->GetMesh();
			pMesh->ClipedAnimationReset();
			pMesh->ClipedAnimationResize(7);
			pMesh->ClipedAnimation(L"Idle", 0, 0, 30);
			pMesh->ClipedAnimation(L"Run", 1, 31, 61);
			pMesh->ClipedAnimation(L"Damage", 2, 62, 90);
			pMesh->ClipedAnimation(L"Death", 3, 91, 131);
			pMesh->ClipedAnimation(L"Attack", 4, 132, 158);
			pMesh->ClipedAnimation(L"Skill", 5, 159, 219);
			pMesh->ClipedAnimation(L"Stun", 6, 220, 250);
		}
	}
	//---------------------------------------------------------------------------------------------
	//	Flow Test
	CGameObject* pTestFlow = nullptr;
	//{
	//pObj = nullptr;
	//tResolution tRes = CRenderMgr::GetInst()->GetResolution();
	////pPositionTargetTex = CResMgr::GetInst()->FindRes<CTexture>(L"PositionTargetTex");
	////	pPostEffectTex = CResMgr::GetInst()->FindRes<CTexture>(L"PosteffectTargetTex");
	//
	//pObj = new CGameObject;
	//pObj->SetName(L"testDistortion");
	//pObj->AddComponent(new CTransform);
	//pObj->AddComponent(new CMeshRender);
	//pObj->Transform()->SetLocalPos(Vec3(0.f, 20.f, 400.f));
	//pObj->Transform()->SetLocalScale(Vec3(10.f, 10.f, 10.f));
	//pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
//	//pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	//pObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"FlameEffectEz4Mtrl"));
	//pTestFlow = pObj;
	//
	////pObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pPostEffectTex.GetPointer());
	////pObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pPositionTargetTex.GetPointer());
	//m_pCurScene->FindLayer(L"PostEffect")->AddGameObject(pObj);
	//
	//
	//}

	//	플레이어 생성
	{
		CPlayerScript* playerScript = nullptr;
		pPlayer = playerData->Instantiate();

		pPlayer->SetName(L"Player");
		pPlayer->FrustumCheck(false);
		pPlayer->MeshRender()->SetDynamicShadow(true);
		pPlayer->Transform()->setPhysics(true); //true값이 움직일수 있게 하는것. 
		pPlayer->Transform()->SetLocalPos(Vec3(0.f, 0.f, 0.f));
		pPlayer->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
		pPlayer->Transform()->SetLocalRot(Vec3(0.0f, 0.f, 0.f));
		pPlayer->Transform()->SetHasOffset(BYTE(EC_TRANS_OFFSET::ROTATE) | BYTE(EC_TRANS_OFFSET::NONOFFMAT));
		pPlayer->Transform()->SetLocalOffset(Vec3(0.f, XM_ANGLE * 180.f, 0.f), Vec3::Zero);
		if (!bIsToolCam)
		{
			playerScript = new CPlayerScript;
			pPlayer->AddComponent(playerScript);
		}



		//	pPlayer->AddComponent(new CBulletScript);
		//	pPlayer->GetScript<CBulletScript>()->init();

		pPlayer->AddComponent(new CStatusComponent);
		pPlayer->StatusComponent()->SetHpUI((UINT)MONSTER_TYPE::Player);
		pPlayer->StatusComponent()->setIsPlayer(true);
		pPlayer->AddComponent(new CInventoryScript);
		pPlayer->AddComponent(new CPlayerSkillScript);
		if (!bIsToolCam)pPlayer->GetScript<CPlayerScript>()->awake();
		if (!bIsToolCam)playerScript->SetPixCamera(pixCamera);
		pPlayer->GetScript<CPlayerScript>()->init();

		pPlayer->AddComponent(new CCollider2D);
		pPlayer->Collider2D()->SetOffsetPos(Vec3(0.0f, 5.0f, 0.f));
		pPlayer->Collider2D()->SetOffsetScale(Vec3(5.f, 5.f, 5.f));

		pPlayer->AddComponent(new CGoldSkillScript);


		if (!bIsToolCam)pMainCam->SetParent(pPlayer);
	//	pTestFlow->SetParent(pPlayer);
		m_pCurScene->FindLayer(L"Player")->AddGameObject(pPlayer);
	}
	//	MapMgr	-	first
	{
		CMapMgr::GetInst()->SetPlayerObj(pPlayer);
		CMapMgr::GetInst()->SetMainCam(pMainCam);
		//	몬스터
		CMapMgr::GetInst()->SetMonsterMeshData(EC_MOSTER_TYPE::Stone, pMonster[(UINT)EC_MOSTER_TYPE::Stone].GetPointer());
		CMapMgr::GetInst()->SetMonsterMeshData(EC_MOSTER_TYPE::Skelleton, pMonster[(UINT)EC_MOSTER_TYPE::Skelleton].GetPointer());
		CMapMgr::GetInst()->SetMonsterMeshData(EC_MOSTER_TYPE::Mimic, pMonster[(UINT)EC_MOSTER_TYPE::Mimic].GetPointer());
		//	나무
		CMapMgr::GetInst()->SetTreeData(0, pTrees[0].GetPointer());
		CMapMgr::GetInst()->SetTreeData(1, pTrees[1].GetPointer());
		CMapMgr::GetInst()->SetTreeData(2, pTrees[2].GetPointer());
		CMapMgr::GetInst()->SetTreeData(3, pTrees[3].GetPointer());
		CMapMgr::GetInst()->SetTreeData(4, pTrees[4].GetPointer());
		CMapMgr::GetInst()->SetTreeData(5, pTrees[5].GetPointer());
		CMapMgr::GetInst()->SetTreeData(6, pTrees[6].GetPointer());

		CMapMgr::GetInst()->SetStoneData(0, pStone[0].GetPointer());
		CMapMgr::GetInst()->SetStoneData(1, pStone[1].GetPointer());
		CMapMgr::GetInst()->SetStoneData(2, pStone[2].GetPointer());
		CMapMgr::GetInst()->SetStoneData(3, pStone[3].GetPointer());
		CMapMgr::GetInst()->SetStoneData(4, pStone[4].GetPointer());
		CMapMgr::GetInst()->SetStoneData(5, pStone[5].GetPointer());
		CMapMgr::GetInst()->SetStoneData(6, pStone[6].GetPointer());

		CMapMgr::GetInst()->SetScene(m_pCurScene);
		CMapMgr::GetInst()->setBulletData(pMonsterSkill[0]);
		CMapMgr::GetInst()->setBulletData(pMonsterSkill[1]);

		CMapMgr::GetInst()->PushMonsterSkillEffectTexture(pMopYellowBlack);
		CMapMgr::GetInst()->PushMonsterSkillEffectTexture(pSummon1);
		CMapMgr::GetInst()->PushItemScrollData(pScroll);

		CMapMgr::GetInst()->PushBossMeshData(pStoneBossLow);
		CMapMgr::GetInst()->PushBossMeshData(pMirrorBoss);

		CMapMgr::GetInst()->InitFloorTex();
	}
	//	맵 생성
	float fSponDelayTime = 2.1f;
	{
		//	0 : start
		{
			mapIdx = 0;
			
			pMap[mapIdx] = mapMeshData[mapIdx]->Instantiate();
			pMap[mapIdx]->SetName(L"StartMap");
			pMap[mapIdx]->FrustumCheck(false);
			pMap[mapIdx]->MeshRender()->SetDynamicShadow(true);
			pMap[mapIdx]->Transform()->SetLocalPos(Vec3(0.f, 0.f, 0.f));
			pMap[mapIdx]->Transform()->SetLocalScale(Vec3(0.10f, 0.10f, 0.10f));
			

			cScript = new CMapScript;
			pMap[mapIdx]->AddComponent(cScript);
			{
				cScript->SetMapIdx(mapIdx);
				cScript->SetPlayer(pPlayer);
				cScript->SetMapRect();
				cScript->SetSponnerCnt(0);
				cScript->SetSponner(-1, 0.0f, Vec3::Zero, Vec3::Zero);
				cScript->SetPortal(1, Vec2(0.f, 180.f), 0.f, 0.f, Vec3::Zero, Vec3::Zero);
				cScript->SetMonsterMaxCnt(0, 0, 0);
				cScript->SetRoundCnt(0);
			}

			startMap = new CStartMapScript;
			pMap[mapIdx]->AddComponent(startMap);
			{
				startMap->SetPlayer(pPlayer);
			}
			CMapMgr::GetInst()->SetMap(mapIdx, pMap[mapIdx]);
			m_pCurScene->FindLayer(L"Default")->AddGameObject(pMap[0]);
		}
		//	1 
		{
			mapIdx = 1;
			pMap[1] = mapMeshData[1]->Instantiate();
			pMap[1]->SetName(L"Map1");
			pMap[1]->FrustumCheck(false);
			pMap[mapIdx]->MeshRender()->SetDynamicShadow(true);
			pMap[1]->Transform()->SetLocalPos(Vec3(0.f, 0.f, 400.f));
			pMap[1]->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
		//		pMap[1]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTestGlass1.GetPointer());
			pMap[mapIdx]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pNeoTex.GetPointer());
			cScript = new CMapScript;
			pMap[1]->AddComponent(cScript);
			pMapScript[mapIdx] = cScript;
			CMapMgr::GetInst()->SetMap(mapIdx, pMap[mapIdx]);
			{
				cTestMapScript = cScript;
				cScript->SetStoneSkill(m_pCurScene, L"MonsterBullet");
				//	높이맵	
				cScript->CreateHeightMap(L"Texture\\Height\\level1hei.txt", XMINT2(400, 400));

				//	충돌맵
			//	cScript->CreateColliderMap(L"Texture\\Map2\\col1.dat", XMINT2(1200, 1200), 3.f);

				cScript->SetMapIdx(1);
				cScript->SetPlayer(pPlayer);
				cScript->SetMapRect();
				cScript->SetTowerPortal();
				//	스포너
				cScript->SetSponnerCnt(2);
				cScript->SetSponner(0, 15.0f, Vec3(100.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.f, 0.0f)); //세번째인자 : 포지션 , 네번째 인자 : 생성시 각도
				cScript->SetSponner(1, 15.0f, Vec3(225.f, 0.f, 325.f), Vec3(0.0f, XM_ANGLE * 135.f, 0.0f));
		//		cScript->SetSponner(2, 15.0f, Vec3(275.f, 0.f, 75.f), Vec3(0.0f, XM_ANGLE * 45.f, 0.0f));
				//	포탈
				cScript->SetPortal(0, Vec2(200.f, 20.f), 100.f, 40.f, Vec3(200.f, 0.f, 20.f), Vec3(0.0f, 0.0f, 0.0f));						//	down
				cScript->SetPortal(8, Vec2(380, 200.f), 40.f, 100.f, Vec3(380.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * -90.0f, 0.0f));	//	right
				cScript->SetPortal(2, Vec2(20.f, 200.f), 40.f, 100.f, Vec3(20.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.0f, 0.0f));	//	left
				cScript->SetPortal(9, Vec2(200.f, 380.f), 100.f, 40.f, Vec3(200.f, 0.f, 380.f), Vec3(0.0f, XM_ANGLE * 180.0f, 0.0f));	//	up
				//	라운드

				cScript->SetRoundCnt(1);
				cScript->SetRoundInfo(0, 4, EC_RESPON_TYPE::SQUENCE, 4, fSponDelayTime); //두번째 인자: 지금라운드에서 죽여야하는 숫자 , 네번째 인자 : 전체 몬스터 수 ( 3-4인자 수 똑같게 해줘야함)
				cScript->SetRoundMonster(0, 0, 0, 4); // 그라운드에 나와야 하는 몬스터 , 첫번째 : 인덱스 , 두번째 : 미믹, 세번째 : 스켈레톤, 네번째 : 스톤 , 다섯번째 : 신경안써도됨 

			//	cScript->SetRoundInfo(1, 2, EC_RESPON_TYPE::SQUENCE, 2, fSponDelayTime);
			//	cScript->SetRoundMonster(1, 0, 0, 2);

			//	cScript->SetRoundInfo(2, 7, EC_RESPON_TYPE::SQUENCE, 7, fSponDelayTime); // 8이 숫자가 
			//	cScript->SetRoundMonster(2, 0, 0, 7); //이숫자의 총합과 일치해야함 

				//cScript->SetRoundMonster(2, 0, 0, 6);
				//	몬스터

				cScript->SetMonsterMaxCnt(0, 0, 4); // 전체 몬스터수 //여기숫자가 최대숫자여야함 위에 setroundmonster는 이것보다 작아야함 
			//	cScript->CreateMonster(true, EC_MOSTER_TYPE::Mimic, L"Monster", L"Mimic",0, m_pCurScene);
		//		cScript->CreateMonster(true, EC_MOSTER_TYPE::Skelleton, L"Monster", L"Skelleton",0, m_pCurScene);

				cScript->CreateMonster(true, EC_MOSTER_TYPE::Stone, L"Monster", L"Stone", 4, m_pCurScene);
				cScript->SettingMap();
			}
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pMap[1]);

		}
		//	2 
		{
			mapIdx = 2;
			pMap[2] = mapMeshData[2]->Instantiate();
			pMap[2]->SetName(L"Map2");
			pMap[2]->FrustumCheck(false);
			pMap[mapIdx]->MeshRender()->SetDynamicShadow(true);
			pMap[2]->Transform()->SetLocalPos(Vec3(-400.f, 0.f, 400.f));
			pMap[2]->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			//	pMap[2]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTestGlass2.GetPointer());
			pMap[mapIdx]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pNeoTex.GetPointer());
			cScript = new CMapScript;
			pMapScript[mapIdx] = cScript;
			pMap[2]->AddComponent(cScript);
			{
				cScript->SetStoneSkill(m_pCurScene, L"MonsterBullet");
				//	높이맵	
				cScript->CreateHeightMap(L"Texture\\Height\\level2hei.txt", XMINT2(400, 400));

				//	충돌맵
			//	cScript->CreateColliderMap(L"Texture\\Map2\\col2.dat", XMINT2(1200, 1200), 3.f);

				cScript->SetMapIdx(2);
				cScript->SetPlayer(pPlayer);
				cScript->SetMapRect();
				//	스포너
				cScript->SetSponnerCnt(2);
				cScript->SetSponner(0, 15.0f, Vec3(100.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.f, 0.0f));
				cScript->SetSponner(1, 15.0f, Vec3(225.f, 0.f, 325.f), Vec3(0.0f, XM_ANGLE * 135.f, 0.0f));
			//	cScript->SetSponner(2, 15.0f, Vec3(275.f, 0.f, 75.f), Vec3(0.0f, XM_ANGLE * 45.f, 0.0f));
				//	포탈
			//	cScript->SetPortal(0, Vec2(200.f, 20.f), 100.f, 40.f, Vec3(200.f, 0.f, 20.f), Vec3(0.0f, 0.0f, 0.0f));						//	down
				cScript->SetPortal(1, Vec2(380, 200.f), 40.f, 100.f, Vec3(380.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.0f, 0.0f));	//	right
			//	cScript->SetPortal(2, Vec2(20.f, 200.f), 40.f, 100.f, Vec3(20.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * -90.0f, 0.0f));	//	left
				cScript->SetPortal(3, Vec2(200.f, 380.f), 100.f, 40.f, Vec3(200.f, 0.f, 380.f), Vec3(0.0f, XM_ANGLE * 180.0f, 0.0f));	//	up
				//	라운드
				cScript->SetRoundCnt(1);
				cScript->SetRoundInfo(0, 4, EC_RESPON_TYPE::SQUENCE, 4, fSponDelayTime);
				cScript->SetRoundMonster(0, 0, 4, 0);

			//	cScript->SetRoundInfo(1, 2, EC_RESPON_TYPE::SQUENCE, 2, fSponDelayTime);
			//	cScript->SetRoundMonster(1, 0, 2, 0);

			//	cScript->SetRoundInfo(2, 13, EC_RESPON_TYPE::SQUENCE, 13, fSponDelayTime);
			//	cScript->SetRoundMonster(2, 4, 4, 5);
				//	몬스터
				cScript->SetMonsterMaxCnt(0, 4, 0);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Mimic, L"Monster", L"Mimic", 0, m_pCurScene);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Skelleton, L"Monster", L"Skelleton", 4, m_pCurScene);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Stone, L"Monster", L"Stone",0, m_pCurScene);
				cScript->SettingMap();

			}
			CMapMgr::GetInst()->SetMap(mapIdx, pMap[mapIdx]);
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pMap[2]);
		}
		//	3 
		{
			mapIdx = 3;
			pMap[3] = mapMeshData[3]->Instantiate();
			pMap[3]->SetName(L"Map3");
			pMap[3]->FrustumCheck(false);
			pMap[mapIdx]->MeshRender()->SetDynamicShadow(true);
			pMap[3]->Transform()->SetLocalPos(Vec3(-400.f, 0.f, 800.f));
			pMap[3]->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			//	pMap[3]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTestGlass3.GetPointer());
			pMap[mapIdx]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pNeoTex.GetPointer());
			cScript = new CMapScript;
			pMapScript[mapIdx] = cScript;
			pMap[3]->AddComponent(cScript);
			{
				cScript->SetStoneSkill(m_pCurScene, L"MonsterBullet");
				//	높이맵	
				cScript->CreateHeightMap(L"Texture\\Height\\level3hei.txt", XMINT2(400, 400));

				//	충돌맵
			//	cScript->CreateColliderMap(L"Texture\\Map2\\col3.dat", XMINT2(1200, 1200), 3.f);

				cScript->SetMapIdx(3);
				cScript->SetPlayer(pPlayer);
				cScript->SetMapRect();
				//	스포너
				cScript->SetSponnerCnt(3);
				cScript->SetSponner(0, 15.0f, Vec3(100.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.f, 0.0f));
				cScript->SetSponner(1, 15.0f, Vec3(225.f, 0.f, 325.f), Vec3(0.0f, XM_ANGLE * 135.f, 0.0f));
				cScript->SetSponner(2, 15.0f, Vec3(275.f, 0.f, 75.f), Vec3(0.0f, XM_ANGLE * 45.f, 0.0f));
				//	포탈
				cScript->SetPortal(2, Vec2(200.f, 20.f), 100.f, 40.f, Vec3(200.f, 0.f, 20.f), Vec3(0.0f, 0.0f, 0.0f));						//	down
			//	cScript->SetPortal(8, Vec2(380, 200.f), 40.f, 100.f, Vec3(380.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.0f, 0.0f));	//	right
			//	cScript->SetPortal(2, Vec2(20.f, 200.f), 40.f, 100.f, Vec3(20.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * -90.0f, 0.0f));	//	left
				cScript->SetPortal(4, Vec2(200.f, 380.f), 100.f, 40.f, Vec3(200.f, 0.f, 380.f), Vec3(0.0f, XM_ANGLE * 180.0f, 0.0f));	//	up
				//	라운드
				cScript->SetRoundCnt(1);
				cScript->SetRoundInfo(0, 5, EC_RESPON_TYPE::SQUENCE, 5, fSponDelayTime);
				cScript->SetRoundMonster(0, 0, 2, 3);

			//	cScript->SetRoundInfo(1, 8, EC_RESPON_TYPE::SQUENCE, 8, fSponDelayTime);
			//	cScript->SetRoundMonster(1, 2, 3, 3);

			//	cScript->SetRoundInfo(2, 13, EC_RESPON_TYPE::SQUENCE, 13, fSponDelayTime);
			//	cScript->SetRoundMonster(2, 4, 4, 5);

			//	cScript->SetRoundInfo(3, 14, EC_RESPON_TYPE::SQUENCE, 14, fSponDelayTime);
			//	cScript->SetRoundMonster(3, 4, 5, 5);
				//	몬스터
				cScript->SetMonsterMaxCnt(0, 2, 3);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Mimic, L"Monster", L"Mimic", 0, m_pCurScene);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Skelleton, L"Monster", L"Skelleton", 2, m_pCurScene);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Stone, L"Monster", L"Stone", 3, m_pCurScene);
				cScript->SettingMap();

			}
			CMapMgr::GetInst()->SetMap(mapIdx, pMap[mapIdx]);
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pMap[3]);
		}
		//	4 
		{
			mapIdx = 4;
			pMap[4] = mapMeshData[4]->Instantiate();
			pMap[4]->SetName(L"Map4");
			pMap[4]->FrustumCheck(false);
			pMap[mapIdx]->MeshRender()->SetDynamicShadow(true);
			pMap[4]->Transform()->SetLocalPos(Vec3(-400.f, 0.f, 1200.f));
			pMap[4]->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			//	pMap[4]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTestGlass4.GetPointer());
			pMap[mapIdx]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pNeoTex.GetPointer());
			cScript = new CMapScript;
			pMapScript[mapIdx] = cScript;
			pMap[4]->AddComponent(cScript);
			{
				cScript->SetStoneSkill(m_pCurScene, L"MonsterBullet");
				//	높이맵	
				cScript->CreateHeightMap(L"Texture\\Height\\level4hei.txt", XMINT2(400, 400));

				//	충돌맵
			//	cScript->CreateColliderMap(L"Texture\\Map2\\col4.dat", XMINT2(1200, 1200), 3.f);

				cScript->SetMapIdx(4);
				cScript->SetPlayer(pPlayer);
				cScript->SetMapRect();
				//	스포너
				cScript->SetSponnerCnt(3);
				cScript->SetSponner(0, 15.0f, Vec3(100.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.f, 0.0f));
				cScript->SetSponner(1, 15.0f, Vec3(225.f, 0.f, 325.f), Vec3(0.0f, XM_ANGLE * 135.f, 0.0f));
				cScript->SetSponner(2, 15.0f, Vec3(275.f, 0.f, 75.f), Vec3(0.0f, XM_ANGLE * 45.f, 0.0f));
				//	포탈
				cScript->SetPortal(3, Vec2(200.f, 20.f), 100.f, 40.f, Vec3(200.f, 0.f, 20.f), Vec3(0.0f, 0.0f, 0.0f));				//	down
				cScript->SetPortal(5, Vec2(380, 200.f), 40.f, 100.f, Vec3(380.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.0f, 0.0f));	//	right
			//	cScript->SetPortal(2, Vec2(20.f, 200.f), 40.f, 100.f, Vec3(20.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * -90.0f, 0.0f));	//	left
			//	cScript->SetPortal(9, Vec2(200.f, 380.f), 100.f, 40.f, Vec3(200.f, 0.f, 380.f), Vec3(0.0f, XM_ANGLE * 180.0f, 0.0f));	//	up
				//	라운드
				cScript->SetRoundCnt(1);
				cScript->SetRoundInfo(0, 4, EC_RESPON_TYPE::SQUENCE, 4, fSponDelayTime);
				cScript->SetRoundMonster(0, 1, 3, 0);

			//	cScript->SetRoundInfo(1, 8, EC_RESPON_TYPE::SQUENCE, 8, fSponDelayTime);
			//	cScript->SetRoundMonster(1, 2, 3, 3);

			//	cScript->SetRoundInfo(2, 13, EC_RESPON_TYPE::SQUENCE, 13, fSponDelayTime);
			//	cScript->SetRoundMonster(2, 4, 4, 5);

			//	cScript->SetRoundInfo(3, 14, EC_RESPON_TYPE::SQUENCE, 14, fSponDelayTime);
			//	cScript->SetRoundMonster(3, 4, 5, 5);
				//	몬스터
				cScript->SetMonsterMaxCnt(1, 3, 0);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Mimic, L"Monster", L"Mimic", 1, m_pCurScene);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Skelleton, L"Monster", L"Skelleton", 3, m_pCurScene);
				//cScript->CreateMonster(true, EC_MOSTER_TYPE::Stone, L"Monster", L"Stone", 0, m_pCurScene);
				cScript->SettingMap();

			}
			CMapMgr::GetInst()->SetMap(mapIdx, pMap[mapIdx]);
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pMap[4]);
		}
		//	5 
		{
			mapIdx = 5;
			pMap[5] = mapMeshData[5]->Instantiate();
			pMap[5]->SetName(L"Map5");
			pMap[5]->FrustumCheck(false);
			pMap[mapIdx]->MeshRender()->SetDynamicShadow(true);
			pMap[5]->Transform()->SetLocalPos(Vec3(0.f, 0.f, 1200.f));
			pMap[5]->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			//	pMap[5]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTestGlass5.GetPointer());
			pMap[mapIdx]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pNeoTex.GetPointer());
			cScript = new CMapScript;
			pMapScript[mapIdx] = cScript;
			pMap[5]->AddComponent(cScript);
			{
				cScript->SetStoneSkill(m_pCurScene, L"MonsterBullet");
				//	높이맵	
				cScript->CreateHeightMap(L"Texture\\Height\\level5hei.txt", XMINT2(400, 400));

				//	충돌맵
			//	cScript->CreateColliderMap(L"Texture\\Map2\\col5.dat", XMINT2(1200, 1200), 3.f);

				cScript->SetMapIdx(5);
				cScript->SetPlayer(pPlayer);
				cScript->SetMapRect();
				//	스포너
				cScript->SetSponnerCnt(3);
				cScript->SetSponner(0, 15.0f, Vec3(100.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.f, 0.0f));
				cScript->SetSponner(1, 15.0f, Vec3(225.f, 0.f, 325.f), Vec3(0.0f, XM_ANGLE * 135.f, 0.0f));
				cScript->SetSponner(2, 15.0f, Vec3(275.f, 0.f, 75.f), Vec3(0.0f, XM_ANGLE * 45.f, 0.0f));
				//	포탈
			//	cScript->SetPortal(0, Vec2(200.f, 20.f), 100.f, 40.f, Vec3(200.f, 0.f, 20.f), Vec3(0.0f, 0.0f, 0.0f));						//	down
				cScript->SetPortal(6, Vec2(380, 200.f), 40.f, 100.f, Vec3(380.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.0f, 0.0f));	//	right
				cScript->SetPortal(4, Vec2(20.f, 200.f), 40.f, 100.f, Vec3(20.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * -90.0f, 0.0f));	//	left
			//	cScript->SetPortal(9, Vec2(200.f, 380.f), 100.f, 40.f, Vec3(200.f, 0.f, 380.f), Vec3(0.0f, XM_ANGLE * 180.0f, 0.0f));	//	up
				//	라운드
				cScript->SetRoundCnt(1);
				cScript->SetRoundInfo(0, 3, EC_RESPON_TYPE::SQUENCE, 3, fSponDelayTime);
				cScript->SetRoundMonster(0, 3, 0, 0);

			//	cScript->SetRoundInfo(1, 5, EC_RESPON_TYPE::SQUENCE, 5, fSponDelayTime);
			//	cScript->SetRoundMonster(1, 2, 0, 3);

			//	cScript->SetRoundInfo(2, 9, EC_RESPON_TYPE::SQUENCE, 9, fSponDelayTime);
			//	cScript->SetRoundMonster(2, 4, 0, 5);

			//	cScript->SetRoundInfo(3, 9, EC_RESPON_TYPE::SQUENCE, 9, fSponDelayTime);
			//	cScript->SetRoundMonster(3, 4, 0, 5);
				//	몬스터13 19
				cScript->SetMonsterMaxCnt(3, 0, 0);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Mimic, L"Monster", L"Mimic",3, m_pCurScene);
			//	cScript->CreateMonster(true, EC_MOSTER_TYPE::Mimic, L"Monster", L"Mimic", 4, m_pCurScene);
			//	cScript->CreateMonster(true, EC_MOSTER_TYPE::Stone, L"Monster", L"Stone", 5, m_pCurScene);
				cScript->SettingMap();

			}
			CMapMgr::GetInst()->SetMap(mapIdx, pMap[mapIdx]);
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pMap[5]);
		}
		//	6 
		{
			mapIdx = 6;
			pMap[6] = mapMeshData[6]->Instantiate();
			pMap[6]->SetName(L"Map6");
			pMap[6]->FrustumCheck(false);
			pMap[mapIdx]->MeshRender()->SetDynamicShadow(true);
			pMap[6]->Transform()->SetLocalPos(Vec3(400.f, 0.f, 1200));
			pMap[6]->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			//	pMap[6]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTestGlass6.GetPointer());
			pMap[mapIdx]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pNeoTex.GetPointer());
			cScript = new CMapScript;
			pMapScript[mapIdx] = cScript;
			pMap[6]->AddComponent(cScript);
			{
				cScript->SetStoneSkill(m_pCurScene, L"MonsterBullet");
				//	높이맵	
				cScript->CreateHeightMap(L"Texture\\Height\\level6hei.txt", XMINT2(400, 400));

				//	충돌맵
			//	cScript->CreateColliderMap(L"Texture\\Map2\\col6.dat", XMINT2(1200, 1200), 3.f);

				cScript->SetMapIdx(6);
				cScript->SetPlayer(pPlayer);
				cScript->SetMapRect();
				//	스포너
				cScript->SetSponnerCnt(3);
				cScript->SetSponner(0, 15.0f, Vec3(100.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.f, 0.0f));
				cScript->SetSponner(1, 15.0f, Vec3(225.f, 0.f, 325.f), Vec3(0.0f, XM_ANGLE * 135.f, 0.0f));
				cScript->SetSponner(2, 15.0f, Vec3(275.f, 0.f, 75.f), Vec3(0.0f, XM_ANGLE * 45.f, 0.0f));
				//	포탈
				cScript->SetPortal(7, Vec2(200.f, 20.f), 100.f, 40.f, Vec3(200.f, 0.f, 20.f), Vec3(0.0f, 0.0f, 0.0f));						//	down
			//	cScript->SetPortal(8, Vec2(380, 200.f), 40.f, 100.f, Vec3(380.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.0f, 0.0f));	//	right
				cScript->SetPortal(5, Vec2(20.f, 200.f), 40.f, 100.f, Vec3(20.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * -90.0f, 0.0f));	//	left
			//	cScript->SetPortal(9, Vec2(200.f, 380.f), 100.f, 40.f, Vec3(200.f, 0.f, 380.f), Vec3(0.0f, XM_ANGLE * 180.0f, 0.0f));	//	up
				//	라운드
				cScript->SetRoundCnt(1);
				cScript->SetRoundInfo(0, 5, EC_RESPON_TYPE::SQUENCE, 5, fSponDelayTime);
				cScript->SetRoundMonster(0, 3, 0, 2);

				//cScript->SetRoundInfo(1, 5, EC_RESPON_TYPE::SQUENCE, 5, fSponDelayTime);
				//cScript->SetRoundMonster(1, 2, 0, 3);

				//cScript->SetRoundInfo(2, 9, EC_RESPON_TYPE::SQUENCE, 9, fSponDelayTime);
				//cScript->SetRoundMonster(2, 4, 0, 5);

				//cScript->SetRoundInfo(3, 9, EC_RESPON_TYPE::SQUENCE, 9, fSponDelayTime);
				//cScript->SetRoundMonster(3, 4, 0, 5);
				//	몬스터 21 29
				cScript->SetMonsterMaxCnt(3, 0, 2);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Mimic, L"Monster", L"Mimic", 3, m_pCurScene);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Stone, L"Monster", L"Stone", 2, m_pCurScene);
				cScript->SettingMap();

			}
			CMapMgr::GetInst()->SetMap(mapIdx, pMap[mapIdx]);
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pMap[6]);
		}
		//	7 
		{
			mapIdx = 7;
			pMap[7] = mapMeshData[7]->Instantiate();
			pMap[7]->SetName(L"Map7");
			pMap[7]->FrustumCheck(false);
			pMap[mapIdx]->MeshRender()->SetDynamicShadow(true);
			pMap[7]->Transform()->SetLocalPos(Vec3(400.f, 0.f, 800.f));
			pMap[7]->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			//	pMap[7]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTestGlass7.GetPointer());
			pMap[mapIdx]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pNeoTex.GetPointer());
			cScript = new CMapScript;
			pMapScript[mapIdx] = cScript;
			pMap[7]->AddComponent(cScript);
			{
				cScript->SetStoneSkill(m_pCurScene, L"MonsterBullet");
				//	높이맵	
				cScript->CreateHeightMap(L"Texture\\Height\\level7hei.txt", XMINT2(400, 400));

				//	충돌맵
			//	cScript->CreateColliderMap(L"Texture\\Map2\\col7.dat", XMINT2(1200, 1200), 3.f);

				cScript->SetMapIdx(7);
				cScript->SetPlayer(pPlayer);
				cScript->SetMapRect();
				//	스포너
				cScript->SetSponnerCnt(3);
				cScript->SetSponner(0, 15.0f, Vec3(100.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.f, 0.0f));
				cScript->SetSponner(1, 15.0f, Vec3(225.f, 0.f, 325.f), Vec3(0.0f, XM_ANGLE * 135.f, 0.0f));
				cScript->SetSponner(2, 15.0f, Vec3(275.f, 0.f, 75.f), Vec3(0.0f, XM_ANGLE * 45.f, 0.0f));
				//	포탈
				cScript->SetPortal(8, Vec2(200.f, 20.f), 100.f, 40.f, Vec3(200.f, 0.f, 20.f), Vec3(0.0f, 0.0f, 0.0f));						//	down
				//cScript->SetPortal(8, Vec2(380, 200.f), 40.f, 100.f, Vec3(380.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.0f, 0.0f));	//	right
				//cScript->SetPortal(2, Vec2(20.f, 200.f), 40.f, 100.f, Vec3(20.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * -90.0f, 0.0f));	//	left
				cScript->SetPortal(6, Vec2(200.f, 380.f), 100.f, 40.f, Vec3(200.f, 0.f, 380.f), Vec3(0.0f, XM_ANGLE * 180.0f, 0.0f));	//	up
				//	라운드
				cScript->SetRoundCnt(2);
				cScript->SetRoundInfo(0, 3, EC_RESPON_TYPE::SQUENCE, 3, fSponDelayTime);
				cScript->SetRoundMonster(0,0 , 2, 1);

				cScript->SetRoundInfo(1, 3, EC_RESPON_TYPE::SQUENCE, 3, fSponDelayTime);
				cScript->SetRoundMonster(1, 0, 1, 2);

			//	cScript->SetRoundInfo(2, 9, EC_RESPON_TYPE::SQUENCE, 9, fSponDelayTime);
			//	cScript->SetRoundMonster(2, 4, 0, 5);

			//	cScript->SetRoundInfo(3, 9, EC_RESPON_TYPE::SQUENCE, 9, fSponDelayTime);
			//	cScript->SetRoundMonster(3, 4, 0, 5);
				//	몬스터 25 34
				cScript->SetMonsterMaxCnt(0, 3, 3);
				//cScript->CreateMonster(true, EC_MOSTER_TYPE::Mimic, L"Monster", L"Mimic", 0, m_pCurScene);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Skelleton, L"Monster", L"Skelleton", 3, m_pCurScene);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Stone, L"Monster", L"Stone", 3, m_pCurScene);
				cScript->SettingMap();

			}
			CMapMgr::GetInst()->SetMap(mapIdx, pMap[mapIdx]);
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pMap[7]);
		}
		//	8 
		{
			mapIdx = 8;
			pMap[8] = mapMeshData[8]->Instantiate();
			pMap[8]->SetName(L"Map8");
			pMap[8]->FrustumCheck(false);
			pMap[mapIdx]->MeshRender()->SetDynamicShadow(true);
			pMap[8]->Transform()->SetLocalPos(Vec3(400.f, 0.f, 400.f));
			pMap[8]->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			pMap[mapIdx]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pNeoTex.GetPointer());
			cScript = new CMapScript;
			pMapScript[mapIdx] = cScript;
			pMap[8]->AddComponent(cScript);
			{
				cScript->SetStoneSkill(m_pCurScene, L"MonsterBullet");
				//	높이맵	
				cScript->CreateHeightMap(L"Texture\\Height\\level8hei.txt", XMINT2(400, 400));

				//	충돌맵
			//	cScript->CreateColliderMap(L"Texture\\Map2\\col8.dat", XMINT2(1200, 1200), 3.f);

				cScript->SetMapIdx(8);
				cScript->SetPlayer(pPlayer);
				cScript->SetMapRect();
				//	스포너
				cScript->SetSponnerCnt(3);
				cScript->SetSponner(0, 15.0f, Vec3(100.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.f, 0.0f));
				cScript->SetSponner(1, 15.0f, Vec3(225.f, 0.f, 325.f), Vec3(0.0f, XM_ANGLE * 135.f, 0.0f));
				cScript->SetSponner(2, 15.0f, Vec3(275.f, 0.f, 75.f), Vec3(0.0f, XM_ANGLE * 45.f, 0.0f));
				//	포탈
				//cScript->SetPortal(0, Vec2(200.f, 20.f), 100.f, 40.f, Vec3(200.f, 0.f, 20.f), Vec3(0.0f, 0.0f, 0.0f));				//	down
				//cScript->SetPortal(8, Vec2(380, 200.f), 40.f, 100.f, Vec3(380.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.0f, 0.0f));	//	right
				cScript->SetPortal(1, Vec2(20.f, 200.f), 40.f, 100.f, Vec3(20.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * -90.0f, 0.0f));	//	left
				cScript->SetPortal(7, Vec2(200.f, 380.f), 100.f, 40.f, Vec3(200.f, 0.f, 380.f), Vec3(0.0f, XM_ANGLE * 180.0f, 0.0f));//	up
				//	라운드
				cScript->SetRoundCnt(2);
				cScript->SetRoundInfo(0, 4, EC_RESPON_TYPE::SQUENCE, 4, fSponDelayTime);
				cScript->SetRoundMonster(0, 1, 1, 2);

				cScript->SetRoundInfo(1, 4, EC_RESPON_TYPE::SQUENCE, 4, fSponDelayTime);
				cScript->SetRoundMonster(1, 1, 2, 1);

				//cScript->SetRoundInfo(2, 9, EC_RESPON_TYPE::SQUENCE, 9, fSponDelayTime);
				//cScript->SetRoundMonster(2, 4, 0, 5);

				//cScript->SetRoundInfo(3, 9, EC_RESPON_TYPE::SQUENCE, 9, fSponDelayTime);
				//cScript->SetRoundMonster(3, 4, 0, 5);
				//	몬스터29 39
				cScript->SetMonsterMaxCnt(1, 2 ,2);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Mimic, L"Monster", L"Mimic", 1, m_pCurScene);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Skelleton, L"Monster", L"Skelleton", 2, m_pCurScene);
				cScript->CreateMonster(true, EC_MOSTER_TYPE::Stone, L"Monster", L"Stone", 2, m_pCurScene);
				cScript->SettingMap();

			}

			CMapMgr::GetInst()->SetMap(mapIdx, pMap[mapIdx]);
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pMap[8]);
		}

		{//
			mapIdx = 9;
			pMap[9] = mapMeshData[9]->Instantiate();
			pMap[9]->SetName(L"OutTower");
			pMap[9]->FrustumCheck(false);
			pMap[mapIdx]->MeshRender()->SetDynamicShadow(true);
			pMap[9]->Transform()->SetLocalPos(Vec3(0.f, -2.3f, 800.f));
			pMap[9]->Transform()->SetLocalScale(Vec3(1.f, 2.5f, 1.f));
		//	pMap[mapIdx]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pNeoTex.GetPointer());
			cScript = new CMapScript;
			pMapScript[mapIdx] = cScript;
			pMap[9]->AddComponent(cScript);
			{
				//	이거 외부 타워도 꺼야함!
				cScript->SetStoneSkill(m_pCurScene, L"MonsterBullet");

				cScript->SetMapIdx(9);
				cScript->SetPlayer(pPlayer);
				cScript->SetMapRect();
				//	스포너
				cScript->SetSponnerCnt(4);
				cScript->SetSponner(0, 15.0f, Vec3(200.f, 0.f, 350.f), Vec3(0.0f, XM_ANGLE * 90.f, 0.0f));
				cScript->SetSponner(1, 15.0f, Vec3(50.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 135.f, 0.0f));
				cScript->SetSponner(2, 15.0f, Vec3(350.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 45.f, 0.0f));
				cScript->SetSponner(3, 15.0f, Vec3(200.f, 0.f, 50.f), Vec3(0.0f, XM_ANGLE * 45.f, 0.0f));
				//	포탈
			//	cScript->SetPortal(0, Vec2(200.f, 20.f), 100.f, 40.f, Vec3(200.f, 0.f, 20.f), Vec3(0.0f, 0.0f, 0.0f));				//	down
				//cScript->SetPortal(8, Vec2(380, 200.f), 40.f, 100.f, Vec3(380.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * 90.0f, 0.0f));	//	right
				//cScript->SetPortal(1, Vec2(20.f, 200.f), 40.f, 100.f, Vec3(20.f, 0.f, 200.f), Vec3(0.0f, XM_ANGLE * -90.0f, 0.0f));	//	left
				//cScript->SetPortal(7, Vec2(200.f, 380.f), 100.f, 40.f, Vec3(200.f, 0.f, 380.f), Vec3(0.0f, XM_ANGLE * 180.0f, 0.0f));//	up
				//	라운드29 39
				int idx = 0, maxMop = 8;
				cScript->SetRoundCnt(10);


				cScript->SetRoundInfo(idx, 5, EC_RESPON_TYPE::SQUENCE, 5, fSponDelayTime);
				cScript->SetRoundMonster(idx++, 5, 0, 0);


				cScript->SetRoundInfo(idx, 5, EC_RESPON_TYPE::SQUENCE, 5, fSponDelayTime);
				cScript->SetRoundMonster(idx++, 0, 5, 0);


				cScript->SetRoundInfo(idx, 5, EC_RESPON_TYPE::SQUENCE, 5, fSponDelayTime,true,(UINT)EC_BOSS_TYPE::MIRROR);
				cScript->SetRoundMonster(idx++, 0, 0, 5);

				cScript->SetRoundInfo(idx, 10, EC_RESPON_TYPE::SQUENCE, 10, fSponDelayTime);
				cScript->SetRoundMonster(idx++, 5, 0, 5);


				cScript->SetRoundInfo(idx, 10, EC_RESPON_TYPE::SQUENCE, 10, fSponDelayTime);
				cScript->SetRoundMonster(idx++, 0, 5, 5);


				cScript->SetRoundInfo(idx, 10, EC_RESPON_TYPE::SQUENCE, 10, fSponDelayTime, true, (UINT)EC_BOSS_TYPE::STONE);
				cScript->SetRoundMonster(idx++, 5, 5, 0);


				cScript->SetRoundInfo(idx, 15, EC_RESPON_TYPE::SQUENCE, 15, fSponDelayTime);
				cScript->SetRoundMonster(idx++, 0, 10, 5);

				cScript->SetRoundInfo(idx, 15, EC_RESPON_TYPE::SQUENCE, 15, fSponDelayTime);
				cScript->SetRoundMonster(idx++, 5, 5, 5);


				cScript->SetRoundInfo(idx, 20, EC_RESPON_TYPE::SQUENCE, 20, fSponDelayTime, true, (UINT)EC_BOSS_TYPE::MIRROR);
				cScript->SetRoundMonster(idx++, 5, 10, 5);

				//	몬스터
				cScript->SetMonsterMaxCnt(25, 40, 30);
				cScript->Transfer();

				cScript->SettingMap();
			}

			CMapMgr::GetInst()->SetMap(9, pMap[9]);
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pMap[9]);
		}
		{
			//	midFar

			//	down
			pObj = pMidFar[0]->Instantiate();
			pObj->SetName(L"Mid218");
			pObj->FrustumCheck(false);
			pObj->MeshRender()->SetDynamicShadow(true);
			pObj->Transform()->SetLocalPos(Vec3(0.f, 0.f, 200.f));
			pObj->Transform()->SetLocalScale(Vec3(1.f, 1.3f, 1.f));
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pObj);

			//	left
			pObj = pMidFar[1]->Instantiate();
			pObj->SetName(L"Mid234");
			pObj->FrustumCheck(false);
			pObj->MeshRender()->SetDynamicShadow(true);
			pObj->Transform()->SetLocalPos(Vec3(-600.f, 0.f, 800.f));
			pObj->Transform()->SetLocalScale(Vec3(1.f, 1.3f, 1.f));
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pObj);
			//	up
			pObj = pMidFar[2]->Instantiate();
			pObj->SetName(L"Mid456");
			pObj->FrustumCheck(false);
			pObj->MeshRender()->SetDynamicShadow(true);
			pObj->Transform()->SetLocalPos(Vec3(0.f, 0.f, 1400.f));
			pObj->Transform()->SetLocalScale(Vec3(1.f, 1.3f, 1.f));
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pObj);
			//	right
			pObj = pMidFar[3]->Instantiate();
			pObj->SetName(L"Mid678");
			pObj->FrustumCheck(false);
			pObj->MeshRender()->SetDynamicShadow(true);
			pObj->Transform()->SetLocalPos(Vec3(600.f, 0.f, 800.f));
			pObj->Transform()->SetLocalScale(Vec3(1.f, 1.3f, 1.f));
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pObj);

		}
		//	Particle test
		//{
		//	pObj = new CGameObject;
		//	pObj->AddComponent(new CTransform);
		//	pObj->AddComponent(new CTransform);
		//
		//}

		Ptr<CMaterial> pForestMtrl;
		{
			pForestMtrl = new CMaterial;
			pForestMtrl->DisableFileSave();
			pForestMtrl->SetShader(CResMgr::GetInst()->FindRes<CShader>(L"TexShader"));
			CResMgr::GetInst()->AddRes(L"ForestMtrl", pForestMtrl);
			pForestMtrl->SetData(SHADER_PARAM::TEX_0, pTexForest.GetPointer());
		}
		//	BT Test : Stone
		if(false)
		{
			pObj =  pMonster[0]->Instantiate();
			pObj->Transform()->SetLocalScale(Vec3(10.f, 10.f, 10.f));
			pObj->Transform()->SetHasOffset((BYTE)EC_TRANS_OFFSET::POSITION );
			pObj->Transform()->SetLocalOffset(Vec3(0.f, XM_ANGLE * 180.f, 0.f), Vec3(0.f, 10.f, 0.f));
			CBTAI* btAi = new CBTAI;
			CStatusComponent* stat = new CStatusComponent;
			pObj->AddComponent(stat);
			pObj->AddComponent(btAi);

			pObj->Transform()->SetLocalPos(Vec3(0.f, 0.f, 250.f));

			BT::Tool::CBlackBoard* blackBoard = new BT::Tool::CBlackBoard;
			blackBoard->pPlayer = pPlayer;
			stat->SetMapNum(1);
			
			btAi->Test2(blackBoard);
			//	이거 바꿔주고
			blackBoard->SetMappingAnimIdx(EC_MAPPING_ANIM::Idle, (UINT)EC_STONE_ANIM::Idle);
			blackBoard->SetMappingAnimIdx(EC_MAPPING_ANIM::Run, (UINT)EC_STONE_ANIM::Idle);
			blackBoard->SetMappingAnimIdx(EC_MAPPING_ANIM::Damage, (UINT)EC_STONE_ANIM::Damage);
			blackBoard->SetMappingAnimIdx(EC_MAPPING_ANIM::Death, (UINT)EC_STONE_ANIM::Death);
			blackBoard->SetMappingAnimIdx(EC_MAPPING_ANIM::Skill1, (UINT)EC_STONE_ANIM::Skill);
			blackBoard->SetMappingAnimIdx(EC_MAPPING_ANIM::Skill2, (UINT)EC_STONE_ANIM::Skill);
			blackBoard->SetMappingAnimIdx(EC_MAPPING_ANIM::Skill3, (UINT)EC_STONE_ANIM::Skill);
			blackBoard->SetMappingAnimIdx(EC_MAPPING_ANIM::Attack, (UINT)EC_STONE_ANIM::Attack);
			blackBoard->SetMappingAnimIdx(EC_MAPPING_ANIM::Stun, (UINT)EC_STONE_ANIM::Idle);

			//	여기에서 블랙보드를 잠시 추가해야한다
			cTestMapScript->SetBlackBoard(blackBoard);
			m_pCurScene->FindLayer(L"Monster")->AddGameObject(pObj);
		}

		////	testEffect
		//{
		//	pObj = new CGameObject;
		//	pObj->FrustumCheck(false);
		//	pObj->AddComponent(new CMeshRender);
		//	pObj->AddComponent(new CTransform);
		//	pObj->Transform()->SetLocalPos(Vec3(0.f, 0.f, 30.f));
		//	pObj->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
		//	pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		//	pObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"SkyboxMtrl"));
		//	pObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pSky.GetPointer());
		//}
		////	leftMid
		//{
		//	//	L"TexMtrl" L"RectMesh"
		//	pObj = new CGameObject;
		//	pObj->FrustumCheck(false);
		//	pObj->AddComponent(new CMeshRender);
		//	pObj->AddComponent(new CTransform);
		//	pObj->Transform()->SetLocalPos(Vec3(-800.f, 50.f, 800.f));
		//	pObj->Transform()->SetLocalScale(Vec3(1400.f, 300.f, 1.f));
		//	pObj->Transform()->SetLocalRot(Vec3(0.f, XM_ANGLE * -90.f, 0.f));
		//	pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		//
		//	pObj->MeshRender()->SetMaterial(pForestMtrl);
		//	//forest1.png
		//
		//	m_pCurScene->FindLayer(L"Map")->AddGameObject(pObj);
		//}
		//////	UpMid
		//{
		//	//	L"TexMtrl" L"RectMesh"
		//	pObj = new CGameObject;
		//	pObj->FrustumCheck(false);
		//	pObj->AddComponent(new CMeshRender);
		//	pObj->AddComponent(new CTransform);
		//	pObj->Transform()->SetLocalPos(Vec3(0.f, 50.f, 1600.f));
		//	pObj->Transform()->SetLocalScale(Vec3(1400.f, 300.f, 1.f));
		//	pObj->Transform()->SetLocalRot(Vec3(0.f, 0.f, 0.f));
		//	pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		//	pObj->MeshRender()->SetMaterial(pForestMtrl);
		//	//forest1.png
		//
		//	m_pCurScene->FindLayer(L"Map")->AddGameObject(pObj);
		//}
		//////	rightMid
		//{
		//	//	L"TexMtrl" L"RectMesh"
		//	pObj = new CGameObject;
		//	pObj->FrustumCheck(false);
		//	pObj->AddComponent(new CMeshRender);
		//	pObj->AddComponent(new CTransform);
		//	pObj->Transform()->SetLocalPos(Vec3(800.f, 50.f, 800.f));
		//	pObj->Transform()->SetLocalScale(Vec3(1400.f, 300.f, 1.f));
		//	pObj->Transform()->SetLocalRot(Vec3(0.f, XM_ANGLE * 90.f, 0.f));
		//	pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		//	pObj->MeshRender()->SetMaterial(pForestMtrl);
		//	//forest1.png
		//
		//	m_pCurScene->FindLayer(L"Map")->AddGameObject(pObj);
		//}
		////	downMid
		//{
		//	//	L"TexMtrl" L"RectMesh"
		//	pObj = new CGameObject;
		//	pObj->FrustumCheck(false);
		//	pObj->AddComponent(new CMeshRender);
		//	pObj->AddComponent(new CTransform);
		//	pObj->Transform()->SetLocalPos(Vec3(0.f, 50.f, 0.f));
		//	pObj->Transform()->SetLocalScale(Vec3(1400.f, 300.f, 1.f));
		//	pObj->Transform()->SetLocalRot(Vec3(0.f, XM_ANGLE * 180.f, 0.f));
		//	pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		//	pObj->MeshRender()->SetMaterial(pForestMtrl);
		//	//forest1.png
		//
		//	m_pCurScene->FindLayer(L"Map")->AddGameObject(pObj);
		//}
		{
			pTower = towerData->Instantiate();
			pTower->SetName(L"tower");
			pTower->FrustumCheck(false);
			pTower->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pNeoTowerTex.GetPointer());
			pTower->MeshRender()->SetDynamicShadow(true);
			pTower->Transform()->SetLocalPos(Vec3(0.f, 0.f, 800.f));
			pTower->Transform()->SetLocalScale(Vec3(1.f, 2.5f, 1.f));
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pTower);
		}
		{
			// MeshRender 설정
			pObj = new CGameObject;
			pObj->FrustumCheck(false);
			pObj->AddComponent(new CMeshRender);
			pObj->AddComponent(new CTransform);
			pObj->Transform()->SetLocalPos(Vec3(0.f, 0.f, 990.f));
			pObj->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
			pObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"SkyboxMtrl"));
			pObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pSky.GetPointer());

			m_pCurScene->FindLayer(L"Sky")->AddGameObject(pObj);
			CMapMgr::GetInst()->SetOutTowerObj(pTower);
		}


		//{
		//	// MeshRender 설정
		//	pObj = new CGameObject;
		//	pObj->FrustumCheck(false);
		//	pObj->AddComponent(new CMeshRender);
		//	pObj->AddComponent(new CTransform);
		//	pObj->Transform()->SetLocalPos(Vec3(0.f, 0.f, 0.f));
		//	pObj->Transform()->SetLocalScale(Vec3(10.f, 10.f, 1.f));
		//	pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
		//	pObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"SkyboxMtrl"));
		//	pObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pSky.GetPointer());
		//
		//	m_pCurScene->FindLayer(L"Sky")->AddGameObject(pObj);
		//}
		//{
			//	나무테스트

		//{
		//	pObj = pTrees[0]->Instantiate();
		//	pObj->Transform()->SetLocalPos(Vec3((10 * i), 0.f, 400.f));
		//	pObj->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
		//	pObj->AddComponent(new CCollider2D);
		//	pObj->Collider2D()->SetOffsetScale(Vec3(3.f, 3.f, 3.f));
		//	pObj->SetName(L"arrrrrrrrararararararararararararar");
		//	pObj->FrustumCheck(false);
		//	m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObj);
		//}
		//	{
		//		pObj = CMapMgr::GetInst()->GetTreeData(1)->Instantiate();
		//		pObj->Transform()->SetLocalPos(Vec3(-70.f, 0.f, 400.f));
		//		pObj->Transform()->SetLocalScale(Vec3(5.f, 5.f, 5.f));
		//		pObj->SetName(L"tree2");
		//		pObj->FrustumCheck(false);
		//		m_pCurScene->FindLayer(L"Default")->AddGameObject(pObj);
		//	}
		//	{
		//		pObj = CMapMgr::GetInst()->GetTreeData(2)->Instantiate();
		//		pObj->Transform()->SetLocalPos(Vec3(-40.f, 0.f, 400.f));
		//		pObj->Transform()->SetLocalScale(Vec3(5.f, 5.f, 5.f));
		//		pObj->SetName(L"tree3");
		//		pObj->FrustumCheck(false);
		//		m_pCurScene->FindLayer(L"Default")->AddGameObject(pObj);
		//	}
		//	{
		//		pObj = CMapMgr::GetInst()->GetTreeData(3)->Instantiate();
		//		pObj->Transform()->SetLocalPos(Vec3(0.f, 0.f, 400.f));
		//		pObj->Transform()->SetLocalScale(Vec3(5.f, 5.f, 5.f));
		//		pObj->SetName(L"tree4");
		//		pObj->FrustumCheck(false);
		//		m_pCurScene->FindLayer(L"Default")->AddGameObject(pObj);
		//	}
		//	{
		//		pObj = CMapMgr::GetInst()->GetTreeData(4)->Instantiate();
		//		pObj->Transform()->SetLocalPos(Vec3(40.f, 0.f, 400.f));
		//		pObj->Transform()->SetLocalScale(Vec3(5.f, 5.f, 5.f));
		//		pObj->SetName(L"tree5");
		//		pObj->FrustumCheck(false);
		//		m_pCurScene->FindLayer(L"Default")->AddGameObject(pObj);
		//	}

		//}

		//{
		//	//	TestScript
		//	pObj = new CGameObject;
		//	pObj->FrustumCheck(false);
		//	pObj->AddComponent(new CMeshRender);
		//	pObj->AddComponent(new CTransform);
		//	pObj->AddComponent(new CTestScript);
		//	pObj->Transform()->SetLocalPos(Vec3(0.f, 10.f, 0.f));
		//	pObj->Transform()->SetLocalScale(Vec3(10.f, 10.f, 1.f));
		//	pObj->Transform()->SetLocalRot(Vec3(0.f, 0.f, 0.f));
		//	pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		//	pObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));
		//
		//	//forest1.png
		//	pObj->MeshRender()->GetCloneMaterial()->SetData(SHADER_PARAM::TEX_0, pSky.GetPointer());
		//	m_pCurScene->FindLayer(L"Default")->AddGameObject(pObj);
		//}
		
		{
			//	Blur
			pObj = nullptr;
			tResolution tRes = CRenderMgr::GetInst()->GetResolution();
			//pPositionTargetTex = CResMgr::GetInst()->FindRes<CTexture>(L"PositionTargetTex");
			//	pPostEffectTex = CResMgr::GetInst()->FindRes<CTexture>(L"PosteffectTargetTex");

			pObj = new CGameObject;
			pObj->SetName(L"Blur");
			pObj->AddComponent(new CTransform);
			pObj->AddComponent(new CMeshRender);
			pObj->Transform()->SetLocalPos(Vec3(0.f, 0.f, -1.f));
			pObj->Transform()->SetLocalScale(Vec3(tRes.fWidth, tRes.fHeight, 1.f));
			pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
			pObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"BlurMtrl"));

			//pObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pPostEffectTex.GetPointer());
			//pObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pPositionTargetTex.GetPointer());
			m_pCurScene->FindLayer(L"PostEffect")->AddGameObject(pObj);
		}
	
		{
			//	Forge
			pObj = nullptr;
			tResolution tRes = CRenderMgr::GetInst()->GetResolution();
			//pPositionTargetTex = CResMgr::GetInst()->FindRes<CTexture>(L"PositionTargetTex");
			//	pPostEffectTex = CResMgr::GetInst()->FindRes<CTexture>(L"PosteffectTargetTex");

			pObj = new CGameObject;
			pObj->SetName(L"Forge");
			pObj->AddComponent(new CTransform);
			pObj->AddComponent(new CMeshRender);
			pObj->Transform()->SetLocalPos(Vec3(0.f, 0.f, 0.f));
			pObj->Transform()->SetLocalScale(Vec3(tRes.fWidth, tRes.fHeight, 1.f));
			pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
			pObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"ForgeMtrl"));

			//pObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pPostEffectTex.GetPointer());
			//pObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pPositionTargetTex.GetPointer());
			m_pCurScene->FindLayer(L"PostEffect")->AddGameObject(pObj);
		}
		//{
		//	//	Distortion
		//	pObj = nullptr;
		//	tResolution tRes = CRenderMgr::GetInst()->GetResolution();
		//	//pPositionTargetTex = CResMgr::GetInst()->FindRes<CTexture>(L"PositionTargetTex");
		//	//	pPostEffectTex = CResMgr::GetInst()->FindRes<CTexture>(L"PosteffectTargetTex");
		//
		//	pObj = new CGameObject;
		//	pObj->SetName(L"testDistortion");
		//	pObj->AddComponent(new CTransform);
		//	pObj->AddComponent(new CMeshRender);
		//	pObj->Transform()->SetLocalPos(Vec3(0.f, 0.f, 400.f));
		//	pObj->Transform()->SetLocalScale(Vec3(10.f, 10.f, 10.f));
		//	pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
		//	pObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"DistortionMtrl"));
		//
		//	//pObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pPostEffectTex.GetPointer());
		//	//pObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pPositionTargetTex.GetPointer());
		//	m_pCurScene->FindLayer(L"PostEffect")->AddGameObject(pObj);
		//}
		{

			pMapScript[1]->CreateTree(L"FBX\\Maps\\Lv01Tree.txt", L"Enviroment", m_pCurScene);
			pMapScript[2]->CreateTree(L"FBX\\Maps\\Lv02Tree.txt", L"Enviroment", m_pCurScene);
			pMapScript[3]->CreateTree(L"FBX\\Maps\\Lv03Tree.txt", L"Enviroment", m_pCurScene);
			pMapScript[4]->CreateTree(L"FBX\\Maps\\Lv04Tree.txt", L"Enviroment", m_pCurScene);
			pMapScript[5]->CreateTree(L"FBX\\Maps\\Lv05Tree.txt", L"Enviroment", m_pCurScene);
			pMapScript[6]->CreateTree(L"FBX\\Maps\\Lv06Tree.txt", L"Enviroment", m_pCurScene);
			pMapScript[7]->CreateTree(L"FBX\\Maps\\Lv07Tree.txt", L"Enviroment", m_pCurScene);
			pMapScript[8]->CreateTree(L"FBX\\Maps\\Lv08Tree.txt", L"Enviroment", m_pCurScene);

			pMapScript[1]->CreateStone(L"FBX\\Maps\\Lv01Stone.txt", L"Enviroment");
			pMapScript[2]->CreateStone(L"FBX\\Maps\\Lv02Stone.txt", L"Enviroment");
			pMapScript[3]->CreateStone(L"FBX\\Maps\\Lv03Stone.txt", L"Enviroment");
			pMapScript[4]->CreateStone(L"FBX\\Maps\\Lv04Stone.txt", L"Enviroment");
			pMapScript[5]->CreateStone(L"FBX\\Maps\\Lv05Stone.txt", L"Enviroment");
			pMapScript[6]->CreateStone(L"FBX\\Maps\\Lv06Stone.txt", L"Enviroment");
			pMapScript[7]->CreateStone(L"FBX\\Maps\\Lv07Stone.txt", L"Enviroment");
			pMapScript[8]->CreateStone(L"FBX\\Maps\\Lv08Stone.txt", L"Enviroment");
			//{
			//	pObj = CMapMgr::GetInst()->GetStoneData(5)->Instantiate();
			//	pObj->Transform()->SetLocalPos(Vec3(0.f, 0.f, 400.f));
			//	pObj->Transform()->SetLocalScale(Vec3(5.f, 5.f, 5.f));
			//	pObj->SetName(L"tree6");
			//	pObj->FrustumCheck(false);
			//	m_pCurScene->FindLayer(L"Default")->AddGameObject(pObj);
			//}
		}
	}
	//for (int j = 0; j < 6; ++j)
	//{
	//	for (UINT i = 0; i < 20; ++i)
	//	{
	//		pObj = pTrees[j]->Instantiate();
	//		pObj->SetName(L"tree");
	//		pObj->FrustumCheck(false);
	//		pObj->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
	//		pObj->Transform()->SetLocalPos(Vec3(i * 50.f, 0.f, 200.f *j));
	//		// pObject->MeshRender()->SetDynamicShadow(true);		
	//		m_pCurScene->AddGameObject(L"Default", pObj, false);
	//	}
	//}
//	CreateTargetUI();

	//	--------------------------------------------------------------------
	//	noiseTest
	if(false)
	{
		pObj = new CGameObject;
		pObj->AddComponent(new CTransform);
		pObj->AddComponent(new CMeshRender);
		pObj->AddComponent(new CEffectScript);
		pObj->SetName(L"TestFire");
		pObj->FrustumCheck(false);
		pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		pObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"EffectFireBurnMtrl"));

		pObj->Transform()->SetLocalPos(Vec3(0.f, 20.f, 20.f));
		pObj->Transform()->SetLocalScale(Vec3(10.f, 10.f, 1.f));
		m_pCurScene->FindLayer(L"Default")->AddGameObject(pObj);
	}
	


	wstring grassName = L"MapGrass";
	if(false)
	{
		mapIdx = 1;
		//pGrass = mapMeshData[1]->Instantiate();
		pGrass = new CGameObject;
		pGrass->AddComponent(new CTransform);
		pGrass->AddComponent(new CMeshRender);
		pGrass->AddComponent(new CGrassScript);

		pGrass->MeshRender()->SetMesh(mapMeshData[mapIdx]->GetMesh());
		wstring numName = std::to_wstring(mapIdx);
		pGrass->SetName(grassName + numName);
		pGrass->FrustumCheck(false);
		pGrass->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"GrassMtrl"));

		pGrass->Transform()->SetLocalPos(Vec3(0.f, 0.f, 400.f));
		pGrass->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
		m_pCurScene->FindLayer(L"Map")->AddGameObject(pGrass);
	}

	{
		mapIdx = 1;
		//pGrass = mapMeshData[1]->Instantiate();
		pGrass = new CGameObject;
		pGrass->AddComponent(new CTransform);
		pGrass->AddComponent(new CMeshRender);
		pGrass->AddComponent(new CGrassScript);

		pGrass->MeshRender()->SetMesh(mapMeshData[mapIdx]->GetMesh());
		wstring numName = std::to_wstring(mapIdx);
		pGrass->SetName(grassName + numName);
		pGrass->FrustumCheck(false);
		pGrass->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"GrassTriMtrl"));

		pGrass->Transform()->SetLocalPos(Vec3(0.f, 0.f, 400.f));
		pGrass->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
		m_pCurScene->FindLayer(L"Map")->AddGameObject(pGrass);
	}

	if(true)
	{
		{
			mapIdx = 2;
			//pGrass = mapMeshData[1]->Instantiate();
			pGrass = new CGameObject;
			pGrass->AddComponent(new CTransform);
			pGrass->AddComponent(new CMeshRender);
			pGrass->AddComponent(new CGrassScript);

			pGrass->MeshRender()->SetMesh(mapMeshData[mapIdx]->GetMesh());
			wstring numName = std::to_wstring(mapIdx);
			pGrass->SetName(grassName + numName);
			pGrass->FrustumCheck(false);
			pGrass->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"GrassTriMtrl"));

			pGrass->Transform()->SetLocalPos(Vec3(-400.f, 0.f, 400.f));
			pGrass->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pGrass);
		}
		{
			mapIdx = 3;
			//pGrass = mapMeshData[1]->Instantiate();
			pGrass = new CGameObject;
			pGrass->AddComponent(new CTransform);
			pGrass->AddComponent(new CMeshRender);
			pGrass->AddComponent(new CGrassScript);

			pGrass->MeshRender()->SetMesh(mapMeshData[mapIdx]->GetMesh());
			wstring numName = std::to_wstring(mapIdx);
			pGrass->SetName(grassName + numName);
			pGrass->FrustumCheck(false);
			pGrass->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"GrassTriMtrl"));

			pGrass->Transform()->SetLocalPos(Vec3(-400.f, 0.f, 800.f));
			pGrass->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pGrass);
		}
		{
			mapIdx = 4;
			//pGrass = mapMeshData[1]->Instantiate();
			pGrass = new CGameObject;
			pGrass->AddComponent(new CTransform);
			pGrass->AddComponent(new CMeshRender);
			pGrass->AddComponent(new CGrassScript);

			pGrass->MeshRender()->SetMesh(mapMeshData[mapIdx]->GetMesh());
			wstring numName = std::to_wstring(mapIdx);
			pGrass->SetName(grassName + numName);
			pGrass->FrustumCheck(false);
			pGrass->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"GrassTriMtrl"));

			pGrass->Transform()->SetLocalPos(Vec3(-400.f, 0.f, 1200.f));
			pGrass->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pGrass);
		}
		{
			mapIdx = 5;
			//pGrass = mapMeshData[1]->Instantiate();
			pGrass = new CGameObject;
			pGrass->AddComponent(new CTransform);
			pGrass->AddComponent(new CMeshRender);
			pGrass->AddComponent(new CGrassScript);

			pGrass->MeshRender()->SetMesh(mapMeshData[mapIdx]->GetMesh());
			wstring numName = std::to_wstring(mapIdx);
			pGrass->SetName(grassName + numName);
			pGrass->FrustumCheck(false);
			pGrass->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"GrassTriMtrl"));

			pGrass->Transform()->SetLocalPos(Vec3(0.f, 0.f, 1200.f));
			pGrass->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pGrass);
		}
		{
			mapIdx = 6;
			//pGrass = mapMeshData[1]->Instantiate();
			pGrass = new CGameObject;
			pGrass->AddComponent(new CTransform);
			pGrass->AddComponent(new CMeshRender);
			pGrass->AddComponent(new CGrassScript);

			pGrass->MeshRender()->SetMesh(mapMeshData[mapIdx]->GetMesh());
			wstring numName = std::to_wstring(mapIdx);
			pGrass->SetName(grassName + numName);
			pGrass->FrustumCheck(false);
			pGrass->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"GrassTriMtrl"));

			pGrass->Transform()->SetLocalPos(Vec3(400.f, 0.f, 1200));
			pGrass->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pGrass);
		}
		{
			mapIdx = 7;
			//pGrass = mapMeshData[1]->Instantiate();
			pGrass = new CGameObject;
			pGrass->AddComponent(new CTransform);
			pGrass->AddComponent(new CMeshRender);
			pGrass->AddComponent(new CGrassScript);

			pGrass->MeshRender()->SetMesh(mapMeshData[mapIdx]->GetMesh());
			wstring numName = std::to_wstring(mapIdx);
			pGrass->SetName(grassName + numName);
			pGrass->FrustumCheck(false);
			pGrass->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"GrassTriMtrl"));

			pGrass->Transform()->SetLocalPos(Vec3(400.f, 0.f, 800.f));
			pGrass->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pGrass);
		}
		{
			mapIdx = 8;
			//pGrass = mapMeshData[1]->Instantiate();
			pGrass = new CGameObject;
			pGrass->AddComponent(new CTransform);
			pGrass->AddComponent(new CMeshRender);
			pGrass->AddComponent(new CGrassScript);

			pGrass->MeshRender()->SetMesh(mapMeshData[mapIdx]->GetMesh());
			wstring numName = std::to_wstring(mapIdx);
			pGrass->SetName(grassName + numName);
			pGrass->FrustumCheck(false);
			pGrass->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"GrassTriMtrl"));

			pGrass->Transform()->SetLocalPos(Vec3(400.f, 0.f, 400.f));
			pGrass->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->FindLayer(L"Map")->AddGameObject(pGrass);
		}
	}
	

	CCollisionMgr::GetInst()->CheckCollisionLayer(L"Monster", L"Monster");
	CCollisionMgr::GetInst()->CheckCollisionLayer(L"Monster", L"Enviroment");
	CCollisionMgr::GetInst()->CheckCollisionLayer(L"Bullet", L"Monster");
	CCollisionMgr::GetInst()->CheckCollisionLayer(L"Player", L"Monster");

	CCollisionMgr::GetInst()->CheckCollisionLayer(L"Player", L"Enviroment");
	CCollisionMgr::GetInst()->CheckCollisionLayer(L"Player", L"MonsterBullet");

	CCollisionMgr::GetInst()->CheckCollisionLayer(L"Player", L"Item");

	CMapMgr::GetInst()->InitSkillArch();

	m_pCurScene->awake();
	m_pCurScene->start();

//	CSound::g_pFMOD.
//	CSound::g_pFMOD->getChannel();
	

//	g_sound.Play(3);
}

void CSceneMgr::update()
{
	m_pCurScene->update();
	m_pCurScene->lateupdate();

	//	CMapMgr::GetInst()->Update();
		// rendermgr 카메라 초기화
	CRenderMgr::GetInst()->ClearCamera();

	m_pCurScene->finalupdate();

	// 충돌 처리
	CCollisionMgr::GetInst()->update();
}

void CSceneMgr::update_tool()
{
	// rendermgr 카메라 초기화
	CRenderMgr::GetInst()->ClearCamera();
	m_pCurScene->finalupdate();
}

void CSceneMgr::FindGameObjectByTag(const wstring& _strTag, vector<CGameObject*>& _vecFindObj)
{
	for (int i = 0; i < MAX_LAYER; ++i)
	{
		const vector<CGameObject*>& vecObject = m_pCurScene->GetLayer(i)->GetObjects();
		for (size_t j = 0; j < vecObject.size(); ++j)
		{
			if (_strTag == vecObject[j]->GetName())
			{
				_vecFindObj.push_back(vecObject[j]);
			}
		}
	}
}



bool Compare(CGameObject* _pLeft, CGameObject* _pRight)
{
	return (_pLeft->Transform()->GetWorldPos().z < _pRight->Transform()->GetWorldPos().z);
}

//void CSceneMgr::FindGameObjectByPoint(POINT _point, vector<CGameObject*>& _vecFindObj, CCamera* _pToolCam)
//{
//	CCamera* pCam = _pToolCam;
//	if (CCore::GetInst()->GetSceneMod() == SCENE_MOD::SCENE_PLAY)
//	{
//		pCam = CRenderMgr::GetInst()->GetCamera(0);
//	}
//
//	tResolution tRes = CRenderMgr::GetInst()->GetResolution();
//	Vec3 vPickPos = Vec3((float)_point.x - (tRes.fWidth / 2.f), (tRes.fHeight / 2.f) - (float)_point.y, 0.f);
//	vPickPos *= pCam->GetScale(); 
//	vPickPos += pCam->Transform()->GetWorldPos();
//
//	for (int i = 0; i < MAX_LAYER; ++i)
//	{
//		const vector<CGameObject*>& vecObject = m_pCurScene->GetLayer(i)->GetObjects();
//		for (size_t j = 0; j < vecObject.size(); ++j)
//		{
//			if (vecObject[j]->Transform()->IsCasting(vPickPos))
//			{
//				_vecFindObj.push_back(vecObject[j]);
//			}
//		}
//	}
//
//	sort(_vecFindObj.begin(), _vecFindObj.end(), Compare);
//}

