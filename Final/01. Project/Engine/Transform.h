#pragma once
#include "Component.h"

enum class EC_TRANS_OFFSET :BYTE {
	  NONE = 0x00
	, POSITION = 0x01		//	오프셋이 적용된 값을 최종매트릭스에 넣을거면
	, ROTATE = 0x02			//	오프셋이 적용된 값을 최종매트릭스에 넣을거면
	, NONOFFMAT = 0x04	//	얻어오는쪽에서 오프셋이 적용안된 걸 얻고싶으면 넣으면 됨.
};


enum class PHYSICS_TYPE
{
	INT,
	FLOAT,
	VEC3,
	End,
	BLACKBOARD_FLOAT,
	BLACKBOARD_INT,
	BLACKBOARD_VEC3,
	BLACKBOARD_COOPERDATA
};
enum class PHYSICS_FLOAT {
	//HP,
	//MP,
	MASS,
	SEARCHRANGE,
	ATTACKRANGE,
	ROTSPEED,
	SKILL_1_COOL_MAX,
	SKILL_1_COOL_NOW,
	SKILL_2_COOL_MAX,
	SKILL_2_COOL_NOW,
	End
};
enum class PHYSICS_VEC3 {
	VELOCITY,
	ACCELERATION,
	FRICTION,
	LOCAL_POS,
	LOCAL_POS_OFFSET,
	LOCAL_ROT,
	LOCAL_SCALE,
	TARGET_POS,
	TARGET_DIR,
	TARGET_VECTOR,
	End
};




class CTransform :
	public CComponent
{
private:
	Vec3		m_vLocalPos;			// GameObject 좌표
	Vec3		m_vLocalPrePos;
	Vec3		m_vLocalScale;			// GameObject 크기
	Vec3		m_vLocalRot;			// GameObject 회전량
	Vec3		m_vLocalPosOffset;
	//	추<
	Vec3		m_vLocalRotOffset;
	Vec3		m_vObjectDir;

	Vec3		m_vLocalDir[(UINT)DIR_TYPE::END]; // GameObject 의 Front, Up, Right 방향
	Vec3		m_vWorldDir[(UINT)DIR_TYPE::END]; // GameObject 의 Front, Up, Right 방향

	Matrix		m_matWorld;		//	최종 파이프라인에 들어가는것
	//	추<
	Matrix		m_matNonOffWorld;	//	
	Matrix		m_matWorldInv;
	Matrix		m_matWorldRT;
	Matrix		m_matRot;


	BYTE		m_bHasOffset;

	bool		m_bMoveStop;
	
	int			m_nFrameMax;
	int			m_nFrameCur;

	//20200729 아림추 스테이터스 리팩토링

	bool m_bAIUpdate;
	float m_fValue[(UINT)PHYSICS_FLOAT::End];
	Vec3 m_vec3Value[(UINT)PHYSICS_VEC3::End];

	bool m_bPhysics; //기본값은  false . (true일때 물리계산이 적용됨)
	bool m_bSkillPhysics;// 스킬위해서 만든 물리 

	float m_fn; //마찰계수 
	float m_gravity; //중력값 
	float m_mass;
public:
	const Vec3& GetLocalPos() { return m_vLocalPos; }
	Vec3 GetWorldPos() { return m_matWorld.Translation(); }

	//	이거는 파이널 업데이트 이전까지만 유효
	const Vec3& GetLocalPrePos() { return m_vLocalPrePos; }
	
	const Vec3& GetLocalScale() { return m_vLocalScale; }
	Vec3 GetWorldScale();

	const Vec3& GetLocalRot() { return m_vLocalRot; }
	const Matrix& GetWorldMat() {
		if ((BYTE)EC_TRANS_OFFSET::NONOFFMAT & m_bHasOffset)return m_matNonOffWorld;
		else return m_matWorld;
	}
	const Vec3& GetLocalDir(DIR_TYPE _eType) { return m_vLocalDir[(UINT)_eType]; }
	const Vec3& GetWorldDir(DIR_TYPE _eType) { return m_vWorldDir[(UINT)_eType]; }

	const Matrix& GetWorldMatRT() { return m_matWorldRT; }
	const Matrix& GetWorldMatR() { return m_matRot; }

	const Vec3& GetMoveDir() { return m_vObjectDir; }
	void SetLocalPos(const Vec3& _vPos) { if (m_vLocalPos != _vPos) { m_vLocalPos = _vPos; Changed(); } }
	//	추<
	void SetLocalPrePos(const Vec3& _vPrePos) { m_vLocalPrePos = _vPrePos; }

	void SetLocalScale(const Vec3& _vScale) { if (m_vLocalScale != _vScale) { m_vLocalScale = _vScale; Changed(); } }
	void SetLocalRot(const Vec3& _vRot) { if (m_vLocalRot != _vRot) { m_vLocalRot = _vRot; Changed(); } }

	void SetLocalPosOffset(const Vec3& _vOffset) { m_vLocalPosOffset = _vOffset; }
	void SetHasOffset(const BYTE& _bool) { m_bHasOffset |= _bool; }
	const Vec3& GetOffsetPos() { return m_vLocalPosOffset; }
	const BYTE& GetHasOffset() { return m_bHasOffset; }
	bool IsCasting(const Vec3& _vPos);
	float GetMaxScale();
	void SwapPreToPos() { m_vLocalPos = m_vLocalPrePos; }
	//	추
	void StopMove(){ m_bMoveStop = true; }
	void PlayMove() { m_bMoveStop = false; }
	void LookAt(const Vec3 & _vLook);

	void SetLocalOffset(const Vec3& _vOffRot, const Vec3& _vOffPos) {
		m_vLocalPosOffset = _vOffPos;
		m_vLocalRotOffset = _vOffRot;
	}

	// Transform 정보를 상수데이터 및 레지스터로 전달한다.
	void UpdateData();

public:

	virtual void finalupdate();

	virtual void SaveToScene(FILE* _pFile);
	virtual void LoadFromScene(FILE* _pFile);


public:
	virtual CTransform* Clone() { return new CTransform(*this); }

public:
	CTransform();
	virtual ~CTransform();


	//0729 아림추
	void AddForce(float x, float y, float z, float time);
	void AddForce(const Vec3& _dir, const float& _time);
	void setVelocity(float x, float y, float z);

	void setPhysics(bool b) {  m_bPhysics = b; }
	void setSkillPhysics(bool b) { m_bSkillPhysics = b; }
	void setPhysicsValue(float fn, float g, float m) { m_gravity = g; m_fn = fn; m_mass = m; }
};

