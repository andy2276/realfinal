#include "stdafx.h"
#include "CollisionMgr.h"

#include "SceneMgr.h"
#include "Scene.h"
#include "Layer.h"
#include "GameObject.h"
#include "Collider2D.h"
#include"CBoundingBox.h"

#include"Transform.h"
CCollisionMgr::CCollisionMgr()
	: m_LayerCheck{}

{
}

CCollisionMgr::~CCollisionMgr()
{
}

void CCollisionMgr::init()
{
}

void CCollisionMgr::update()
{
	CScene* pCurScene = CSceneMgr::GetInst()->GetCurScene();

	for (int i = 0; i < MAX_LAYER; ++i)
	{
		for (int j = 0; j < MAX_LAYER; ++j)
		{			
			if (m_LayerCheck[i] & (1 << j))
			{
				// i <= j
				CollisionLayer(pCurScene->GetLayer(i), pCurScene->GetLayer(j));				
			}
		}
	}
}

void CCollisionMgr::CheckCollisionLayer(const wstring & _strLayerName1, const wstring & _strLayerName2)
{
	CLayer* pLayer1 = CSceneMgr::GetInst()->GetCurScene()->FindLayer(_strLayerName1);
	CLayer* pLayer2 = CSceneMgr::GetInst()->GetCurScene()->FindLayer(_strLayerName2);

	assert(nullptr != pLayer1 && nullptr != pLayer2);

	int iIdx1 = pLayer1->GetLayerIdx();
	int iIdx2 = pLayer2->GetLayerIdx();

	int iMinIdx = min(iIdx1, iIdx2);
	int iMaxIdx = max(iIdx1, iIdx2);

	m_LayerCheck[iMinIdx] |= (1 << iMaxIdx);
}

void CCollisionMgr::CheckCollisionLayer(int _iLayerIdx1, int _iLyaerIdx2)
{
	CScene* pScene = CSceneMgr::GetInst()->GetCurScene();

	assert(pScene->GetLayer(_iLayerIdx1) && pScene->GetLayer(_iLyaerIdx2));

	int iMinIdx = min(_iLayerIdx1, _iLyaerIdx2);
	int iMaxIdx = max(_iLayerIdx1, _iLyaerIdx2);

	m_LayerCheck[iMinIdx] |= (1 << iMaxIdx);
}

void CCollisionMgr::CollisionLayer(const CLayer * _pLayer1, const CLayer * _pLayer2)
{
	const vector<CGameObject*>& vecObj1 = _pLayer1->GetObjects();
	const vector<CGameObject*>& vecObj2 = _pLayer2->GetObjects();

	map<DWORD_PTR, bool>::iterator iter;

	for (size_t i = 0; i < vecObj1.size(); ++i)
	{
		CCollider2D* pCollider1 = vecObj1[i]->Collider2D();

		if (nullptr == pCollider1)
			continue;

		size_t j = 0;		
		if (_pLayer1 == _pLayer2) // 동일한 레이어 간의 충돌을 검사하는 경우
			j = i + 1;

		for (; j < vecObj2.size(); ++j)
		{			
			CCollider2D* pCollider2 = vecObj2[j]->Collider2D();

			if (nullptr == pCollider2)
				continue;

			tColID id;
			id.iColID1 = pCollider1->GetColID();
			id.iColID2 = pCollider2->GetColID();
			iter = m_mapCol.find(id.ID);

			bool IsDead = false;
			if (pCollider1->GetObj()->IsDead() || pCollider2->GetObj()->IsDead())
				IsDead = true;

			// 충돌했다.
			if (IsCollision(pCollider1, pCollider2))
			{
				// 충돌 중이다
				if (m_mapCol.end() != iter && iter->second == true)
				{
					if (IsDead)
					{
						pCollider1->OnCollisionExit(pCollider2);
						pCollider2->OnCollisionExit(pCollider1);
						iter->second = false;
					}
					else
					{
						pCollider1->OnCollision(pCollider2);
						pCollider2->OnCollision(pCollider1);
					}					
				}
				// 처음 충돌했다
				else
				{
					if (IsDead)
						continue;

					pCollider1->OnCollisionEnter(pCollider2);
					pCollider2->OnCollisionEnter(pCollider1);

					if (m_mapCol.end() == iter)
					{
						m_mapCol.insert(make_pair(id.ID, true));
					}
					else
					{
						iter->second = true;
					}
				}
			}
			else // 충돌하지 않는다.
			{
				if (m_mapCol.end() != iter && true == iter->second)
				{
					pCollider1->OnCollisionExit(pCollider2);
					pCollider2->OnCollisionExit(pCollider1);

					iter->second = false;
				}
			}
		}
	}
}

bool CCollisionMgr::IsCollision(CCollider2D * _pCollider1, CCollider2D * _pCollider2)
{
	if (!_pCollider1->IsActive() || !_pCollider1->GetObj()->IsActive() || !_pCollider2->IsActive() || !_pCollider2->GetObj()->IsActive())
		return false;

	if (COLLIDER2D_TYPE::RECT == _pCollider1->GetColliderType() && COLLIDER2D_TYPE::RECT == _pCollider2->GetColliderType())
	{
		return CollisionRect(_pCollider1, _pCollider2);
	}
	else if (COLLIDER2D_TYPE::CIRCLE == _pCollider1->GetColliderType() && COLLIDER2D_TYPE::CIRCLE == _pCollider2->GetColliderType())
	{
		return CollisionCircle(_pCollider1, _pCollider2);
	}
	else if (COLLIDER2D_TYPE::SPHERE == _pCollider1->GetColliderType() && COLLIDER2D_TYPE::SPHERE == _pCollider2->GetColliderType())
	{
		return CollisionSphere(_pCollider1, _pCollider2);
	}
	else if (COLLIDER2D_TYPE::OBB == _pCollider1->GetColliderType() && COLLIDER2D_TYPE::OBB == _pCollider2->GetColliderType())
	{
		return CollisionOBB(_pCollider1, _pCollider2);
	}
	else if (COLLIDER2D_TYPE::UIRECT == _pCollider1->GetColliderType() && COLLIDER2D_TYPE::UIRECT == _pCollider2->GetColliderType())
	{

		return CollisionUIrect(_pCollider1, _pCollider2);
	}
	else
	{
		return CollisionRectCircle(_pCollider1, _pCollider2);
	}	

	return false;
}

bool CCollisionMgr::CollisionRect(CCollider2D * _pCollider1, CCollider2D * _pCollider2)
{
	static Vec3 arrLocal[4] = {					// 0 -- 1
		  Vec3(-0.5f, 0.5f, 0.f)				// |	|
		, Vec3(0.5f, 0.5f, 0.f)					// 3 -- 2
		, Vec3(0.5f, -0.5f, 0.f)
		, Vec3(-0.5f, -0.5f, 0.f)};	
	

	const Matrix& matCol1 = _pCollider1->GetColliderWorldMat();
	const Matrix& matCol2 = _pCollider2->GetColliderWorldMat();

	Vec3 arrCol1[4] = {};
	Vec3 arrCol2[4] = {};
	Vec3 arrCenter[2] = {};
	
	for (UINT i = 0; i < 4; ++i)
	{
		arrCol1[i] = XMVector3TransformCoord(arrLocal[i], matCol1);
		arrCol2[i] = XMVector3TransformCoord(arrLocal[i], matCol2);

		// 2D 충돌이기 때문에 같은 Z 좌표상에서 충돌을 계산한다.
		arrCol1[i].z = 0.f;
		arrCol2[i].z = 0.f;
	}

	arrCenter[0] = XMVector3TransformCoord(Vec3(0.f, 0.f, 0.f), matCol1);
	arrCenter[1] = XMVector3TransformCoord(Vec3(0.f, 0.f, 0.f), matCol2);
	arrCenter[0].z = 0.f;
	arrCenter[1].z = 0.f;

	Vec3 vCenter = arrCenter[1] - arrCenter[0];
	
	Vec3 arrOriginVec[4] = { arrCol1[3] - arrCol1[0]
		, arrCol1[1] - arrCol1[0]
		, arrCol2[3] - arrCol2[0]
		, arrCol2[1] - arrCol2[0]
	};

	Vec3 arrProjVec[4] = {};
	for (UINT i = 0; i < 4; ++i)
	{
		arrOriginVec[i].Normalize(arrProjVec[i]);
	}


	// 투영을 통해서 분리축 테스트
	// vCenter		 두 사각형의 중심을 잇는 벡터
	// arrOriginVec  각 사각형의 표면 벡터
	// arrProjVec    사각형의 표면과 평행한 투영축 벡터(단위벡터)

	for (UINT i = 0; i < 4; ++i)
	{
		float fCenter = abs(vCenter.Dot(arrProjVec[i])); // 중심 거리 벡터를 해당 투영축으로 투영시킨 길이
		
		float fAcc = 0.f;
		for (UINT j = 0; j < 4; ++j)
			fAcc += abs(arrOriginVec[j].Dot(arrProjVec[i]));

		fAcc /= 2.f;

		if (fCenter > fAcc)
			return false;
	}

	return true;
}

bool CCollisionMgr::CollisionCircle(CCollider2D * _pCollider1, CCollider2D * _pCollider2)
{
	return false;
}
bool CCollisionMgr::CollisionRectCircle(CCollider2D * _pCollider1, CCollider2D * _pCollider2)
{
	return false;
}
bool CCollisionMgr::CollisionSphere(CCollider2D * _pCollider1, CCollider2D * _pCollider2)
{
	Vec3 a_center = _pCollider1->getLocalPos() + _pCollider1->GetOffsetPos();
	Vec3 b_center = _pCollider2->getLocalPos() + _pCollider2->GetOffsetPos();;

	Vec3 distance_vector = a_center - b_center;
	float distance = distance_vector.Length();

	Vec3 a_temp_radius = _pCollider1->GetOffsetScale() + _pCollider1->getLocalScale();
	Vec3 b_temp_radius = _pCollider2->GetOffsetScale() + _pCollider2->getLocalScale();

	float a_radius = a_temp_radius.x;
	float b_radius = b_temp_radius.x;

	if (distance <= a_radius + b_radius)
		//if ((a_radius + b_radius)*(a_radius + b_radius) >= distance)
	{
		return true;//충돌
	}
	else {

		return false;
	}
}

float FDotProduct(const float v0[3], const float v1[3])
{
	return v0[0] * v1[0] + v0[1] * v1[1] + v0[2] * v1[2];
}


bool CCollisionMgr::CollisionOBB(CCollider2D * _pCollider1, CCollider2D * _pCollider2)
{
	CBoundingBox box1;
	Vec3 vPos = _pCollider1->getLocalPos();
	//	Vec3 vSacle = _pCollider1->getLocalScale();
	//	Vec3 vSacle = _pCollider1->getLocalScale() + _pCollider1->GetOffsetScale();
	Vec3 vSacle = _pCollider1->GetOffsetScale();
	Vec3 vSacle2 = _pCollider1->GetOffsetScale();


	vSacle.x /= 2.f;
	vSacle.y /= 2.f;
	vSacle.z /= 2.f;
	Vec3 vMin(vPos.x - vSacle.x, vPos.y - vSacle.y, vPos.z - vSacle.z);
	Vec3 vMax(vPos.x + vSacle.x, vPos.y + vSacle.y, vPos.z + vSacle.z);


	box1.CalcOBBBox(vMin, vMax, vSacle2, _pCollider1->GetMatrixRT());

	CBoundingBox box0;
	vPos = _pCollider2->getLocalPos();
	//	 vSacle = _pCollider2->getLocalScale();
		// vSacle = _pCollider2->getLocalScale() +_pCollider2->GetOffsetScale();
	vSacle = _pCollider2->GetOffsetScale();
	vSacle2 = _pCollider2->GetOffsetScale();

	vSacle.x /= 2;
	vSacle.y /= 2;
	vSacle.z /= 2;

	Vec3 vMin2(vPos.x - vSacle.x, vPos.y - vSacle.y, vPos.z - vSacle.z);
	Vec3 vMax2(vPos.x + vSacle.x, vPos.y + vSacle.y, vPos.z + vSacle.z);


	box0.CalcOBBBox(vMin2, vMax2, vSacle2, _pCollider2->GetMatrixRT());



	float D[3] =
	{
		 box1.center[0] - box0.center[0],
		 box1.center[1] - box0.center[1],
		 box1.center[2] - box0.center[2]
	};

	float C[3][3];    //matrix C=A^T B,c_{ij}=Dot(A_i,B_j)				float3x3 matrix
	float absC[3][3]; //|c_{ij}|										float3x3 matrix
	float AD[3];      //Dot(A_i,D)										float3 
	float R0, R1, R;    //interval radii and distance between centers
	float R01;        //=R0+R1

	//A0
	C[0][0] = FDotProduct(box0.axis[0], box1.axis[0]);
	C[0][1] = FDotProduct(box0.axis[0], box1.axis[1]);
	C[0][2] = FDotProduct(box0.axis[0], box1.axis[2]);
	AD[0] = FDotProduct(box0.axis[0], D);
	absC[0][0] = (float)fabsf(C[0][0]);
	absC[0][1] = (float)fabsf(C[0][1]);
	absC[0][2] = (float)fabsf(C[0][2]);
	R = (float)fabsf(AD[0]);
	R1 = box1.extent[0] * absC[0][0] + box1.extent[1] * absC[0][1] + box1.extent[2] * absC[0][2];
	R01 = box0.extent[0] + R1;
	if (R > R01)
		return 0;

	//A1
	C[1][0] = FDotProduct(box0.axis[1], box1.axis[0]);
	C[1][1] = FDotProduct(box0.axis[1], box1.axis[1]);
	C[1][2] = FDotProduct(box0.axis[1], box1.axis[2]);
	AD[1] = FDotProduct(box0.axis[1], D);
	absC[1][0] = (float)fabsf(C[1][0]);
	absC[1][1] = (float)fabsf(C[1][1]);
	absC[1][2] = (float)fabsf(C[1][2]);
	R = (float)fabsf(AD[1]);
	R1 = box1.extent[0] * absC[1][0] + box1.extent[1] * absC[1][1] + box1.extent[2] * absC[1][2];
	R01 = box0.extent[1] + R1;
	if (R > R01)
		return 0;

	//A2
	C[2][0] = FDotProduct(box0.axis[2], box1.axis[0]);
	C[2][1] = FDotProduct(box0.axis[2], box1.axis[1]);
	C[2][2] = FDotProduct(box0.axis[2], box1.axis[2]);
	AD[2] = FDotProduct(box0.axis[2], D);
	absC[2][0] = (float)fabsf(C[2][0]);
	absC[2][1] = (float)fabsf(C[2][1]);
	absC[2][2] = (float)fabsf(C[2][2]);
	R = (float)fabsf(AD[2]);
	R1 = box1.extent[0] * absC[2][0] + box1.extent[1] * absC[2][1] + box1.extent[2] * absC[2][2];
	R01 = box0.extent[2] + R1;
	if (R > R01)
		return 0;



	//B0
	R = (float)fabsf(FDotProduct(box1.axis[0], D));
	R0 = box0.extent[0] * absC[0][0] + box0.extent[1] * absC[1][0] + box0.extent[2] * absC[2][0];
	R01 = R0 + box1.extent[0];
	if (R > R01)
		return 0;

	//B1
	R = (float)fabsf(FDotProduct(box1.axis[1], D));
	R0 = box0.extent[0] * absC[0][1] + box0.extent[1] * absC[1][1] + box0.extent[2] * absC[2][1];
	R01 = R0 + box1.extent[1];
	if (R > R01)
		return 0;

	//B2
	R = (float)fabsf(FDotProduct(box1.axis[2], D));
	R0 = box0.extent[0] * absC[0][2] + box0.extent[1] * absC[1][2] + box0.extent[2] * absC[2][2];
	R01 = R0 + box1.extent[2];
	if (R > R01)
		return 0;

	//A0xB0
	R = (float)fabsf(AD[2] * C[1][0] - AD[1] * C[2][0]);
	R0 = box0.extent[1] * absC[2][0] + box0.extent[2] * absC[1][0];
	R1 = box1.extent[1] * absC[0][2] + box1.extent[2] * absC[0][1];
	R01 = R0 + R1;
	if (R > R01)
		return 0;

	//A0xB1
	R = (float)fabsf(AD[2] * C[1][1] - AD[1] * C[2][1]);
	R0 = box0.extent[1] * absC[2][1] + box0.extent[2] * absC[1][1];
	R1 = box1.extent[0] * absC[0][2] + box1.extent[2] * absC[0][0];
	R01 = R0 + R1;
	if (R > R01)
		return 0;

	//A0xB2
	R = (float)fabsf(AD[2] * C[1][2] - AD[1] * C[2][2]);
	R0 = box0.extent[1] * absC[2][2] + box0.extent[2] * absC[1][2];
	R1 = box1.extent[0] * absC[0][1] + box1.extent[1] * absC[0][0];
	R01 = R0 + R1;
	if (R > R01)
		return 0;

	//A1xB0
	R = (float)fabsf(AD[0] * C[2][0] - AD[2] * C[0][0]);
	R0 = box0.extent[0] * absC[2][0] + box0.extent[2] * absC[0][0];
	R1 = box1.extent[1] * absC[1][2] + box1.extent[2] * absC[1][1];
	R01 = R0 + R1;
	if (R > R01)
		return 0;

	//A1xB1
	R = (float)fabsf(AD[0] * C[2][1] - AD[2] * C[0][1]);
	R0 = box0.extent[0] * absC[2][1] + box0.extent[2] * absC[0][1];
	R1 = box1.extent[0] * absC[1][2] + box1.extent[2] * absC[1][0];
	R01 = R0 + R1;
	if (R > R01)
		return 0;

	//A1xB2
	R = (float)fabsf(AD[0] * C[2][2] - AD[2] * C[0][2]);
	R0 = box0.extent[0] * absC[2][2] + box0.extent[2] * absC[0][2];
	R1 = box1.extent[0] * absC[1][1] + box1.extent[1] * absC[1][0];
	R01 = R0 + R1;
	if (R > R01)
		return 0;

	//A2xB0
	R = (float)fabsf(AD[1] * C[0][0] - AD[0] * C[1][0]);
	R0 = box0.extent[0] * absC[1][0] + box0.extent[1] * absC[0][0];
	R1 = box1.extent[1] * absC[2][2] + box1.extent[2] * absC[2][1];
	R01 = R0 + R1;
	if (R > R01)
		return 0;

	//A2xB1
	R = (float)fabsf(AD[1] * C[0][1] - AD[0] * C[1][1]);
	R0 = box0.extent[0] * absC[1][1] + box0.extent[1] * absC[0][1];
	R1 = box1.extent[0] * absC[2][2] + box1.extent[2] * absC[2][0];
	R01 = R0 + R1;
	if (R > R01)
		return 0;

	//A2xB2
	R = (float)fabsf(AD[1] * C[0][2] - AD[0] * C[1][2]);
	R0 = box0.extent[0] * absC[1][2] + box0.extent[1] * absC[0][2];
	R1 = box1.extent[0] * absC[2][1] + box1.extent[1] * absC[2][0];
	R01 = R0 + R1;
	if (R > R01)
		return 0;

	return 1;



}

bool CCollisionMgr::CollisionUIrect(CCollider2D * _pCollider1, CCollider2D * _pCollider2)
{



	Vec3 pos1 =	_pCollider1->Transform()->GetLocalPos();
	Vec3 pos2 = _pCollider2->Transform()->GetLocalPos();

	Vec2 size1 = _pCollider1->Collider2D()->getUIsize();
	Vec2 size2 = _pCollider2->Collider2D()->getUIsize();

	size1.x /= 2;
	size1.y /= 2;

	size2.x /= 2;
	size2.y /= 2;

//size1.x *= 400;
//size1.y *= 400;
//
//size2.x *= 400;
//size2.y *= 400;


	//std::wcout << "1 :  "<<pos1.x << " . " << pos1.y<< " / " << size1.y << std::endl;
	//std::wcout << "2 :   " << pos2.x << " . " << pos2.y << " / " << size2.y << std::endl;
	//std::wcout << "================================" << std::endl;


	RECT rect1;
	rect1.left = pos1.x - size1.x;
	rect1.right = pos1.x + size1.x;
	rect1.top = pos1.y + size1.y;
	rect1.bottom = pos1.y - size1.y;

	RECT rect2;
	rect2.left = pos2.x - size2.x;
	rect2.right = pos2.x + size2.x;
	rect2.top = pos2.y + size2.y;
	rect2.bottom = pos2.y - size2.y;
	

	if (rect1.right < rect2.left)return false;
	if (rect1.left > rect2.right)return false;
	if (rect1.top < rect2.bottom)return false;
	if (rect1.bottom > rect2.top)return false;


	else return true;
//
//	if (rect1.left < rect2.right &&
//		rect1.top < rect2.bottom &&
//		rect1.right > rect2.left &&
//		rect1.bottom > rect2.top)
//	{
//
//	//	std::wcout << "true"<<std::endl;
////		return true;
//		return false;
//	}
//	else {
//
////		std::wcout << "false"<<std::endl;
////		return false;
//		return true;
//	}



}
