#pragma once
#include"Script.h"

enum  class UI_TYPE {
	MP,
	HP,
	z,
};


class UIscript : public CScript
{
private:
	CGameObject * m_pPlayer;
	float m_hp;
	float m_mp;

	UINT eType;
public:
	UIscript();
	virtual ~UIscript();
	CLONE(UIscript);

	virtual void update();

	void setPlayer(CGameObject * obj) { m_pPlayer = obj; }

	void setUItype(UINT type) { eType = type; };
	void init();
};

