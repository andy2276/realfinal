#include "stdafx.h"
#include "BT.h"
#include "BTInit.h"

#include "Transform.h"
#include "Animator3D.h"
#include "StatusComponent.h"
#include "GameObject.h"

#include "SkillPlatform.h"

#include "TimeMgr.h"
#include "MapMgr.h"


BT::Leaf::Condition::HpLess::HpLess()
{
}

BT::Leaf::Condition::HpLess::~HpLess()
{
}
//	offset : 0, pData : float(ScrHp)
void BT::Leaf::Condition::HpLess::SetData(int _offset, void * pData)
{
	m_fScrHp = *static_cast<float*>(pData);
}

BT::Return BT::Leaf::Condition::HpLess::Evaluate()
{
	if (BT::Return::Evaluation != m_eState)return m_eState;

	if (m_fScrHp >= Status()->GetFloat(STATUS_FLOAT::HP)) 
	{ 
		if (m_bRecordType)m_eState = BT::Return::Succeess;
		return BT::Return::Succeess;
	}
	else
	{
		if (m_bRecordType)m_eState = BT::Return::Failure;
		return BT::Return::Failure;
	}
}

BT::Leaf::Condition::LookPlayer::LookPlayer()
{
}

BT::Leaf::Condition::LookPlayer::~LookPlayer()
{
}

BT::Return BT::Leaf::Condition::LookPlayer::Evaluate()
{
	if (BT::Return::Evaluation != m_eState)return m_eState;
	static Vec3 mePos,meLook;
	mePos = Transform()->GetLocalPos();
	//	1차로 플레이어의 위치와 자신을 계산해준다.
	if (Status()->GetFloat(STATUS_FLOAT::SEARCHRANGE) > m_pBlackBoard->PlayerToDist(mePos))
	{
		meLook = Transform()->GetLocalDir(DIR_TYPE::FRONT);
		if (m_pBlackBoard->PlayerToAngle(mePos, meLook) < RADIAN(45.f))
		{
			if (m_bRecordType)m_eState = BT::Return::Succeess;
			return BT::Return::Succeess;
		}
	}
	if (m_bRecordType)m_eState = BT::Return::Failure;
	return BT::Return::Failure;
}

BT::Leaf::Condition::IsDeath::IsDeath()
{
}

BT::Leaf::Condition::IsDeath::~IsDeath()
{
}

BT::Return BT::Leaf::Condition::IsDeath::Evaluate()
{
	if (m_eState != BT::Return::Evaluation)return m_eState;

	if (Status()->GetLowHp())
	{
		if (m_bRecordType)m_eState = BT::Return::Succeess;
		return BT::Return::Succeess;
	}
	else
	{
		if (m_bRecordType)m_eState = BT::Return::Failure;
		return BT::Return::Failure;
	}
}

BT::Leaf::Action::TurnDest::TurnDest()
{
}

BT::Leaf::Action::TurnDest::~TurnDest()
{
}

BT::Return BT::Leaf::Action::TurnDest::Evaluate()
{
	//	현재 각도를 가져온다. 
	//	그각도로 계속 더한다
	//	그걸 다시 셋한다.
	if (m_eState != BT::Return::Evaluation)return m_eState;
	static Vec3 targetPos, mePos,meRot, meLook,lookPlayer;
	targetPos = Status()->GetVec3(STATUS_VEC3::TARGET_POS);
	mePos = Transform()->GetLocalPos();
	meLook = Transform()->GetLocalDir(DIR_TYPE::FRONT);
	lookPlayer = (targetPos - mePos).Normalize();
	float oper = ((meLook.Cross(lookPlayer)).y  < 0.f )? -1.f:1.f;
	float angle = meLook.Dot(lookPlayer);

	if (!FloatCmp(angle, 1.0f, 0.01f))
	{
		meRot = Transform()->GetLocalRot();
		meRot.y += Status()->GetFloat(STATUS_FLOAT::ROTSPEED) * Toc() * oper;
		Transform()->SetLocalRot(meRot);
		return BT::Return::Running;
	}
	else
	{

		if (m_bRecordType)m_eState = BT::Return::Succeess;
		return BT::Return::Succeess;
	}
}

BT::Leaf::Action::TurnAngle::TurnAngle()
	:m_bStart(true)
	, m_bRandOper(false)
	, m_fAngle(10.f)
	, m_fOper(1.f)
	, m_fCurAngle(0.f)
{
}

BT::Leaf::Action::TurnAngle::~TurnAngle()
{
}

void BT::Leaf::Action::TurnAngle::SetData(int _offset, void * pData)
{
	switch (_offset)
	{
	case 0:
		m_fAngle = *static_cast<float*>(pData);
		break;
	case 1:
		m_fOper = *static_cast<float*>(pData);
		break;
	case 2:
		m_bRandOper = *static_cast<bool*>(pData);
		break;
	default:
		break;
	}
	
}

BT::Return BT::Leaf::Action::TurnAngle::Evaluate()
{
	if (m_eState != BT::Return::Evaluation)return m_eState;

	static Vec3 rot;
	
	if (m_bStart)
	{
		m_bStart = false;
		
		if (m_bRandOper)
		{
			if (((rand() % 7) / 2) == 0)
			{
				m_fOper = -1.f;
			}
			else
			{
				m_fOper = 1.f;
			}
		}
		if (m_fOper < 0.f)
		{
			m_fCurAngle = m_fAngle;
		}
		else
		{
			m_fCurAngle = 0.f;
		}
		
	}
	float calculAngle = Status()->GetFloat(STATUS_FLOAT::ROTSPEED) * Toc() * m_fOper;
	rot = Transform()->GetLocalRot();
	m_fCurAngle += calculAngle;
	rot.y += m_fCurAngle;

	if (m_fOper < 0.f)
	{
		if (m_fCurAngle > 0.f)
		{
			Transform()->SetLocalRot(rot);
			return BT::Return::Running;
		}
		else
		{
			m_bStart = true;
			if (m_bRecordType)m_eState = BT::Return::Succeess;
			return BT::Return::Succeess;
		}
	}
	else
	{
		if (m_fCurAngle < m_fAngle)
		{
			Transform()->SetLocalRot(rot);
			return BT::Return::Running;
		}
		else
		{
			m_bStart = true;
			if (m_bRecordType)m_eState = BT::Return::Succeess;
			return BT::Return::Succeess;
		}
	}
}

BT::Leaf::Action::MoveDest::MoveDest()
{
	m_fNear = 5.0f;
}

BT::Leaf::Action::MoveDest::~MoveDest()
{
}

void BT::Leaf::Action::MoveDest::SetData(int _offset, void * pData)
{
	m_fNear = *static_cast<float*>(pData);
}

BT::Return BT::Leaf::Action::MoveDest::Evaluate()
{
	if (BT::Return::Evaluation != m_eState)return m_eState;
	static Vec3 targetPos,mePos,lookDest,meLook,turnDest,vAngle;
	targetPos = Status()->GetVec3(STATUS_VEC3::TARGET_POS);
	targetPos.y = 0.f;
	mePos = Transform()->GetLocalPos();
	mePos.y = 0.f;
	//	회전	:	회전은 무한
	lookDest = (targetPos - mePos).Normalize();
	meLook = Transform()->GetLocalDir(DIR_TYPE::FRONT);
	float angle = meLook.Dot(lookDest);
	//		135		90	45
	//		-0.707	0	0.707	
	//			\	|	/
	//	------------|------------->
	//				0
	if (angle > RADIAN(5.f) || angle < 0.f)
	{
		turnDest = meLook.Cross(lookDest);
		float oper = (turnDest.y < 0.f)? 1.f : -1.f;
		vAngle = Transform()->GetLocalRot();
		vAngle.y += Status()->GetFloat(STATUS_FLOAT::ROTSPEED) * oper * m_pProperty->GetToc();
		Transform()->SetLocalRot(vAngle);
	}

	//	이동
	float dist = Vec3::Distance(targetPos, mePos);
	if (m_fNear < dist)
	{
		mePos += lookDest * Status()->GetFloat(STATUS_FLOAT::MOVESPEED) * m_pProperty->GetToc();
		mePos.y = CMapMgr::GetInst()->GetHeight(Status()->GetMapNum(),mePos);
		Transform()->SetLocalPos(mePos);
		m_eState = BT::Return::Evaluation;
		return BT::Return::Running;
	}
	else
	{
		if (m_bRecordType)m_eState = BT::Return::Succeess;
		return BT::Return::Succeess;
	}
	return m_eState;
}

BT::Leaf::Action::MoveDirDist::MoveDirDist()
	:m_bMoveStart(true)
{
}

BT::Leaf::Action::MoveDirDist::~MoveDirDist()
{
}

void BT::Leaf::Action::MoveDirDist::SetData(int _offset, void * pData)
{
	switch (_offset)
	{
	case 0://	Dirtype
		m_eDirType = *static_cast<DIR_TYPE*>(pData);
		break;
	case 1://	fdist
		m_fDist = *static_cast<float*>(pData);
		break;
	default:
		break;
	}
}

BT::Return BT::Leaf::Action::MoveDirDist::Evaluate()
{
	if (BT::Return::Evaluation != m_eState)return m_eState;
	static Vec3 curPos;
	if (m_bMoveStart)
	{
		m_vStartPos = Transform()->GetLocalPos();
		m_vStartPos.y = 0.f;
		m_bMoveStart = false;
	}

	curPos = Transform()->GetLocalPos();
	curPos.y = 0.f;

	float len = (curPos - m_vStartPos).Length();

	if (len < m_fDist)
	{
		curPos += Transform()->GetLocalDir(m_eDirType) * Status()->GetFloat(STATUS_FLOAT::MOVESPEED) * m_pProperty->GetToc();
		curPos.y = CMapMgr::GetInst()->GetHeight(Status()->GetMapNum(),curPos);
		Transform()->SetLocalPos(curPos);
		if (m_pBlackBoard->IsPossibleAnim((int)EC_MAPPING_ANIM::Run))
		{
			Animator3D()->ChangeAnimation(m_pBlackBoard->GetMappingAnimIdx((int)EC_MAPPING_ANIM::Run), true);
		}

		return BT::Return::Running;
	}
	else
	{
		m_bMoveStart = true;
		if (m_bRecordType)m_eState = BT::Return::Succeess;
		return BT::Return::Succeess;
	}
	return m_eState;
}

BT::Leaf::Action::ShootSkill::ShootSkill()
	:m_bOnShoot(true)
{
}

BT::Leaf::Action::ShootSkill::~ShootSkill()
{
}

void BT::Leaf::Action::ShootSkill::SetData(int _offset, void * pData)
{
	switch (_offset)
	{
	case 0://	skillnum
		m_nSkillNum = *static_cast<int*>(pData);
		break;
	case 1://	shootTime
		m_fSkillShootTime = *static_cast<float*>(pData);
		break;
	default:
		break;
	}
}

BT::Return BT::Leaf::Action::ShootSkill::Evaluate()
{
	if (BT::Return::Evaluation != m_eState)return m_eState;

	if (m_pBlackBoard->IsPossibleAnim(m_nSkillNum) && Skills() != nullptr)
	{
		if (Animator3D()->IsAnimIdx(m_pBlackBoard->GetMappingAnimIdx(m_nSkillNum)))
		{
			if (m_bOnShoot && Animator3D()->GetCurAnimLeftTime() < m_fSkillShootTime)
			{
				Skills()->Shoot(Transform()->GetLocalPos(), Transform()->GetLocalRot(), Vec3::Zero);
				m_bOnShoot = false;
			}

			if (Animator3D()->IsAnimEnd())
			{
				m_bOnShoot = true;
				Status()->SetFloat(m_nSkillNum, 0.f);
				if (m_pBlackBoard->IsPossibleAnim((int)EC_MAPPING_ANIM::Idle))
				{
					Animator3D()->ChangeAnimation(m_pBlackBoard->GetMappingAnimIdx((int)EC_MAPPING_ANIM::Idle), true);
				}

				if (m_bRecordType)m_eState = BT::Return::Succeess;
				return  BT::Return::Succeess;
			}
			else
			{
				m_eState = BT::Return::Evaluation;
				return BT::Return::Running;
			}
		}
		else
		{
			Animator3D()->ChangeAnimation(m_pBlackBoard->GetMappingAnimIdx(m_nSkillNum), true);
			m_eState = BT::Return::Evaluation;
			return BT::Return::Running;
		}
	}
	else
	{
		//	아예사용 불가능
		if (m_bRecordType)m_eState = BT::Return::Failure;
		return BT::Return::Failure;
	}

	return m_eState;
}

BT::Leaf::Condition::IsDamaged::IsDamaged()
{
}

BT::Leaf::Condition::IsDamaged::~IsDamaged()
{ 
}

void BT::Leaf::Condition::IsDamaged::SetData(int _offset, void * _data)
{
	switch (_offset)
	{
	case 0:
		m_pProperty->StackDmg = static_cast<std::list<tDamageStack>*>(_data);
		break;
	default:
		break;
	}
	
}

BT::Return BT::Leaf::Condition::IsDamaged::Evaluate()
{
	if (m_pProperty->StackDmg != nullptr)
	{
		float tempDmg = 0.f;
		while (!m_pProperty->StackDmg->empty())
		{
			tempDmg += m_pProperty->StackDmg->back().fAttackDamage;
			//	dir은 나중에
			m_pProperty->StackDmg->pop_back();
		}
		m_pProperty->SetDamaged(tempDmg);
		if (tempDmg >0.f)
		{
			m_eState = BT::Return::Succeess;
		}
		else
		{
			m_eState = BT::Return::Failure;
		}
		
		return m_eState;
	}
	else
	{
		m_eState = BT::Return::Failure;
		return m_eState;
	}
	

}

BT::Leaf::Action::Damaged::Damaged()
	:m_fStuckDamage(19.f)
	, m_bOnce(true)
{
}

BT::Leaf::Action::Damaged::~Damaged()
{
}

void BT::Leaf::Action::Damaged::SetData(int _offset, void * pData)
{
	m_fStuckDamage = *static_cast<float*>(pData);
}

BT::Return BT::Leaf::Action::Damaged::Evaluate()
{
	if (m_bOnce)
	{
		float fDmg = m_pProperty->GetDamaged();
		Status()->setHpDown(fDmg);
		m_pProperty->SetDamaged(0.0f);

		if (fDmg > m_fStuckDamage)
		{
			if (m_pBlackBoard->IsPossibleAnim((int)EC_MAPPING_ANIM::Damage))
			{
				if (!Animator3D()->IsAnimIdx(m_pBlackBoard->GetMappingAnimIdx((int)EC_MAPPING_ANIM::Damage)))
				{
					m_bOnce = false;
					Animator3D()->ChangeAnimation(m_pBlackBoard->GetMappingAnimIdx((int)EC_MAPPING_ANIM::Damage), true);
					return  BT::Return::Running;
				}
			}
		}

		if (m_bRecordType)m_eState = BT::Return::Succeess;
		m_bOnce = true;
		return BT::Return::Succeess;

	}
	else
	{
		//	러닝상태에 오더라도 데미지는 계속는 계속 준다.
		float fDmg = m_pProperty->GetDamaged();
		if (fDmg > 0.1f)
		{
			Status()->setHpDown(fDmg);
			m_pProperty->SetDamaged(0.0f);
		}

		if (Animator3D()->IsAnimIdx(m_pBlackBoard->GetMappingAnimIdx((int)EC_MAPPING_ANIM::Damage)))
		{
			if (!Animator3D()->IsAnimEnd())
			{
				m_bOnce = true;
				if (m_bRecordType)m_eState = BT::Return::Succeess;
				return BT::Return::Succeess;
			}
			else
			{
				
				return  BT::Return::Running;
			}
		}
	}
}

BT::Leaf::Condition::IsSkillOn::IsSkillOn()
{
}

BT::Leaf::Condition::IsSkillOn::~IsSkillOn()
{
}

void BT::Leaf::Condition::IsSkillOn::SetData(int _offset, void * pData)
{
	switch (_offset)
	{
	case 0:
		m_nCoolMax = *static_cast<int*>(pData);
		break;
	case 1:
		m_nCoolNow = *static_cast<int*>(pData);
		break;
	default:
		break;
	}
}

BT::Return BT::Leaf::Condition::IsSkillOn::Evaluate()
{
	if (BT::Return::Evaluation != m_eState)return m_eState;
	if (Status()->GetFloat(m_nCoolMax) < Status()->GetFloat(m_nCoolNow))
	{
		if (m_bRecordType)m_eState = BT::Return::Succeess;
		return BT::Return::Succeess;
	}
	else
	{
		if (m_bRecordType)m_eState = BT::Return::Failure;
		return BT::Return::Failure;
	}
}

BT::Leaf::Condition::IsLessRange::IsLessRange()
	:m_fRange(100.f)
{
}

BT::Leaf::Condition::IsLessRange::~IsLessRange()
{
}

void BT::Leaf::Condition::IsLessRange::SetData(int _offset, void * pData)
{
	m_fRange = *static_cast<float*>(pData);
}

BT::Return BT::Leaf::Condition::IsLessRange::Evaluate()
{
	static Vec3 targetPos, mePos;

	if (m_eState != BT::Return::Evaluation)return m_eState;

	targetPos = Status()->GetVec3(STATUS_VEC3::TARGET_POS);
	targetPos.y = 0.f;
	mePos = Transform()->GetLocalPos();
	mePos.y = 0.f;
	if (Vec3::Distance(targetPos, mePos) < m_fRange)
	{
		if (m_bRecordType)m_eState = BT::Return::Succeess;
		return BT::Return::Succeess;

	}
	else
	{
		if (m_bRecordType)m_eState = BT::Return::Failure;
		return BT::Return::Failure;
	}
}

BT::Leaf::Action::BagicAttack::BagicAttack()
	:m_bOnAttack(true)
	, m_fAttackRange(15.f)
	, m_fAttackTime(ANIMFRAME(7))
	, m_fDamage(20.f)
{
}

BT::Leaf::Action::BagicAttack::~BagicAttack()
{
}

void BT::Leaf::Action::BagicAttack::SetData(int _offset, void * pData)
{
}

BT::Return BT::Leaf::Action::BagicAttack::Evaluate()
{
	if (m_eState != BT::Return::Evaluation)return m_eState;
	if (m_pBlackBoard->IsPossibleAnim((int)EC_MAPPING_ANIM::Attack))
	{
		if (Animator3D()->IsAnimIdx(m_pBlackBoard->GetMappingAnimIdx((int)EC_MAPPING_ANIM::Attack)))
		{
			if (!Animator3D()->IsAnimEnd())
			{
				if (m_bOnAttack)
				{
					if (Animator3D()->GetCurAnimLeftTime() < m_fAttackTime)
					{
						if (m_pBlackBoard->PlayerToDist(Transform()->GetLocalPos()) < m_fAttackRange)
						{
							m_bOnAttack = false;
						}
					}
				}
				return BT::Return::Running;
			}
			else
			{
				Status()->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW, 0.f);
				if (m_pBlackBoard->IsPossibleAnim((int)EC_MAPPING_ANIM::Idle))
				{
					Animator3D()->ChangeAnimation(m_pBlackBoard->GetMappingAnimIdx((int)EC_MAPPING_ANIM::Idle), true);
				}
				if (!m_bOnAttack)
				{
					m_pBlackBoard->pPlayer->StatusComponent()->DamageStack(m_pObj->GetID(), Transform()->GetLocalDir(DIR_TYPE::FRONT), m_pProperty->GetBagicAttackDmg());
					m_bOnAttack = true;
					if (m_bRecordType)m_eState = BT::Return::Succeess;
					return BT::Return::Succeess;
				}
				else
				{
					if (m_bRecordType)m_eState = BT::Return::Failure;
					return BT::Return::Failure;
				}
			}
		}
		else
		{
			Animator3D()->ChangeAnimation(m_pBlackBoard->GetMappingAnimIdx((int)EC_MAPPING_ANIM::Attack), true);
			return BT::Return::Running;
		}
	}
	else
	{
		if (m_bRecordType)m_eState = BT::Return::Failure;
		return BT::Return::Failure;
	}
}

BT::Leaf::Condition::FirstIn::FirstIn()
	:m_bFirst(true)
{
}

BT::Leaf::Condition::FirstIn::~FirstIn()
{
}

void BT::Leaf::Condition::FirstIn::HardReset()
{
	m_bFirst = true;
}

void BT::Leaf::Condition::FirstIn::SetData(int _offset, void * pData)
{
	m_bFirst = *static_cast<bool*>(pData);
}

BT::Return BT::Leaf::Condition::FirstIn::Evaluate()
{
	if (m_bFirst)
	{
		m_bFirst = false;
		return BT::Return::Succeess;
	}
	else
	{
		return BT::Return::Failure;
	}
}

BT::Leaf::Action::SetRageState::SetRageState()
	:m_fRageScale(1.4f)
{
}

BT::Leaf::Action::SetRageState::~SetRageState()
{
}

void BT::Leaf::Action::SetRageState::SetData(int _offset, void * pData)
{
	m_fRageScale = *static_cast<float*>(pData);
}

BT::Return BT::Leaf::Action::SetRageState::Evaluate()
{
	if (m_eState != BT::Return::Evaluation)return m_eState;

	//	moveSpeed,rotSpeed
	//	all cool
	//	BagicAttackDmg
	float dmg = m_pProperty->GetBagicAttackDmg();
	float movSpeed = Status()->GetFloat(STATUS_FLOAT::MOVESPEED);
	float rotSpeed = Status()->GetFloat(STATUS_FLOAT::ROTSPEED);
	float coolMax[3] = 
	{ 
		Status()->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX)
		,Status()->GetFloat(STATUS_FLOAT::SKILL_1_COOL_MAX)
		,Status()->GetFloat(STATUS_FLOAT::SKILL_2_COOL_MAX)
	};


	dmg *= m_fRageScale;
	movSpeed *= m_fRageScale;
	rotSpeed *= m_fRageScale;

	coolMax[0] /= m_fRageScale;
	coolMax[1] /= m_fRageScale;
	coolMax[2] /= m_fRageScale;

	m_pProperty->SetBagicAttackDmg(dmg);
	Status()->SetFloat(STATUS_FLOAT::MOVESPEED, movSpeed);
	Status()->SetFloat(STATUS_FLOAT::ROTSPEED, rotSpeed);

	Status()->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX, rotSpeed);
	Status()->SetFloat(STATUS_FLOAT::SKILL_1_COOL_MAX, coolMax[1]);
	Status()->SetFloat(STATUS_FLOAT::SKILL_2_COOL_MAX, coolMax[2]);

	
	if (m_bRecordType)m_eState = BT::Return::Succeess;
	return BT::Return::Succeess;
}

BT::Leaf::Action::SetDeath::SetDeath()
{
}

BT::Leaf::Action::SetDeath::~SetDeath()
{
}

BT::Return BT::Leaf::Action::SetDeath::Evaluate()
{
	if (m_pBlackBoard->IsPossibleAnim((int)EC_MAPPING_ANIM::Death))
	{
		if (Animator3D()->IsAnimIdx(m_pBlackBoard->GetMappingAnimIdx((int)EC_MAPPING_ANIM::Death)))
		{
			if (Animator3D()->IsAnimEnd())
			{
				if (m_pObj->IsActive())
				{
					m_pObj->SetActive(false);
					return BT::Return::Succeess;
				}
			}
			return BT::Return::Running;
		}
		else
		{
			Animator3D()->ChangeAnimation(m_pBlackBoard->GetMappingAnimIdx((int)EC_MAPPING_ANIM::Death), true);
			return BT::Return::Running;
		}
	}
	else
	{
		if (m_pObj->IsActive())
		{
			m_pObj->SetActive(false);
			return BT::Return::Succeess;
		}
		return BT::Return::Succeess;
	}
}

BT::Leaf::Action::Stay::Stay()
	:m_fStayTimeCur(0.f)
	, m_fStayTime(2.f)
{
}

BT::Leaf::Action::Stay::~Stay()
{
}

void BT::Leaf::Action::Stay::SetData(int _offset, void * pData)
{
	m_fStayTime = *static_cast<float*>(pData);
}

BT::Return BT::Leaf::Action::Stay::Evaluate() 
{
	if (m_eState != BT::Return::Evaluation)return m_eState;

	if (m_pBlackBoard->IsPossibleAnim((int)EC_MAPPING_ANIM::Idle))
	{
		if (Animator3D()->IsAnimIdx(m_pBlackBoard->GetMappingAnimIdx((int)EC_MAPPING_ANIM::Idle)))
		{
			if (m_fStayTimeCur < m_fStayTime)
			{
				m_fStayTimeCur += Toc();
				return BT::Return::Running;
			}
			else
			{
				m_fStayTimeCur = 0.f;
				if (m_bRecordType)m_eState = BT::Return::Succeess;
				return m_eState;
			}
			
		}
		else
		{
			m_fStayTimeCur += Toc();
			Animator3D()->ChangeAnimation(m_pBlackBoard->GetMappingAnimIdx((int)EC_MAPPING_ANIM::Idle), true);
			return BT::Return::Running;
		}
	}
	else
	{
		if (m_fStayTimeCur < m_fStayTime)
		{
			m_fStayTimeCur += Toc();
			return BT::Return::Running;
		}
		else
		{
			m_fStayTimeCur = 0.f;
			if (m_bRecordType)m_eState = BT::Return::Succeess;
			return BT::Return::Succeess;
		}
	}
}

BT::Leaf::Action::GoDest::GoDest()
	:m_fNear(3.f)
{
}

BT::Leaf::Action::GoDest::~GoDest()
{
}

void BT::Leaf::Action::GoDest::SetData(int _offset, void * pData)
{
}

BT::Return BT::Leaf::Action::GoDest::Evaluate()
{
	if (BT::Return::Evaluation != m_eState)return m_eState;
	static Vec3 targetPos, mePos, lookDest, meLook, turnDest, vAngle;

	targetPos = Status()->GetVec3(STATUS_VEC3::TARGET_POS);
	targetPos.y = 0.f;
	mePos = Transform()->GetLocalPos();
	mePos.y = 0.f;

	lookDest = (targetPos - mePos).Normalize();

	float dist = Vec3::Distance(targetPos, mePos);

	if (m_fNear < dist)
	{
		mePos += lookDest * Status()->GetFloat(STATUS_FLOAT::MOVESPEED) * m_pProperty->GetToc();
		mePos.y = CMapMgr::GetInst()->GetHeight(Status()->GetMapNum(), mePos);
		Transform()->SetLocalPos(mePos);
		m_eState = BT::Return::Evaluation;
		return BT::Return::Running;
	}
	else
	{
		if (m_bRecordType)m_eState = BT::Return::Succeess;
		return BT::Return::Succeess;
	}
}

BT::Leaf::Action::ReValue::ReValue()
	:m_eReValue(BT::Return::Succeess)
{
}

BT::Leaf::Action::ReValue::~ReValue()
{
}

void BT::Leaf::Action::ReValue::SetData(int _offset, void * pData)
{
	m_eReValue = *static_cast<BT::Return*>(pData);
}

BT::Return BT::Leaf::Action::ReValue::Evaluate()
{
	return m_eReValue;
}
