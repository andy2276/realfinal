#pragma once
#include "Script.h"
class CGrassScript :
	public CScript
{
private:
	Vec3 m_vGrassScale;

public:
	CGrassScript();
	virtual ~CGrassScript();

	CLONE(CGrassScript);
	virtual void update();

	Vec2 GrassHash(Vec2& _vec);
};

