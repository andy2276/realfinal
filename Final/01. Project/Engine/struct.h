#pragma once

#include "define.h"

struct tResolution
{
	float fWidth;
	float fHeight;
};

// 정점 구조체
struct VTX
{
	Vec3 vPos;
	Vec4 vColor;
	Vec2 vUV;

	Vec3 vTangent;
	Vec3 vNormal;	
	Vec3 vBinormal;

	Vec4 vWeights;
	Vec4 vIndices;
};

struct tDBG
{
	DBG_TYPE eType;
	wstring  strDBG;
};


struct tLightColor
{
	Vec4 vDiff;
	Vec4 vSpec;
	Vec4 vAmb;
};

struct tParticle
{
	Vec3 vWorldPos;		// 위치	
	Vec3 vWorldDir;		// 이동 방향

	float m_fCurTime;	// 현재 시간
	float m_fLifeTime;	// 최대 생명주기

	int  iAlive;
	int  arrPading[3];
};

struct tParticleShared
{
	int iAddCount;
	int arrPading[3];
};

// ===========
// Instancing
// ===========
union uInstID
{
	struct {
		UINT iMesh;
		WORD iMtrl;
		WORD iMtrlIdx;
	};
	ULONG64 llID;
};

union uInstBufferID
{
	struct {
		UINT iMesh;
		UINT iMtrl;
	};
	ULONG64 llID;
};

class CGameObject;

struct tInstObj
{
	CGameObject*	pObj;
	UINT			iMtrlIdx;
};

// ==============
// 상수버퍼 메모리
// ==============
struct tTransform
{
	Matrix matWorld;	//64 byte
	Matrix matView;
	Matrix matProj;
	Matrix matWV;
	Matrix matWVP;
	Matrix matWorldInv;
	Matrix matViewInv;
	Matrix matProjInv;
//	추<	UINT objId;
//	추<	XMINT3 _padding0;

	UINT objID; //12 Btye
	XMINT3 _padding0;
	XMINT4 _padding1;
 
};

extern tTransform g_transform;

//struct tTransform
//{
//	Matrix matWorld;
//	Matrix matView;
//	Matrix matProj;
//
//	Matrix matWV;
//	Matrix matWVP;
//};


struct tMtrlParam
{
	Vec4			m_vDiff; // 색상 계수
	Vec4			m_vSpec; // 반사광 계수
	Vec4			m_vEmv;  // 발광 계수

	int				m_arrInt[(UINT)SHADER_PARAM::INT_END - (UINT)SHADER_PARAM::INT_0];
	float			m_arrFloat[(UINT)SHADER_PARAM::FLOAT_END - (UINT)SHADER_PARAM::FLOAT_0];
	Vec2			m_arrVec2[(UINT)SHADER_PARAM::VEC2_END - (UINT)SHADER_PARAM::VEC2_0];
	Vec4			m_arrVec4[(UINT)SHADER_PARAM::VEC4_END - (UINT)SHADER_PARAM::VEC4_0];
	Matrix			m_arrMat[(UINT)SHADER_PARAM::MATRIX_END - (UINT)SHADER_PARAM::MATRIX_0];

	int				m_iArrTex[(UINT)SHADER_PARAM::TEX_END - (UINT)SHADER_PARAM::TEX_0];
};


struct tAnim2D
{
	Vec2 vLT;			// UV 좌상단
	Vec2 vLen;			// UV 길이
	Vec2 vOffset;		// Offset Pos
	int  IsAnim2D;		// Animation2D 컴포넌트 사용여부
	int  iPadding;
};

struct tLight2D
{
	Vec4 vLightPos;
	Vec4 vLightColor;
	Vec4 vLightDir;
	int   iLightType;
	float fRange;
	float fAngle;
	int   ipadding;
};

struct tLight2DInfo
{
	tLight2D	arrLight2D[100];
	UINT		iCount;
};

struct tLight3D
{
	tLightColor tColor;
	Vec4		vLightPos;
	Vec4		vLightDir;
	int			iLightType;
	float		fRange;
	float		fAngle;
	int			iPadding;
};

struct tLight3DInfo
{
	tLight3D arrLight3D[100];
	UINT	 iCurCount;
	Vec3     vPadding;
};


struct tGlobalValue
{
	tResolution vResolution;
	float fDT;
	float fAccTime;

	//	추<
	Vec2 vMousePos;
	int nLButtonDown;
	int nRButtonDown;
};
struct tReadBackInfo {
	UINT objId;
};

//================
// Struct of FBX 
//=================
typedef struct _tagMtrlInfo
{
	Vec4	vMtrlDiff;
	Vec4	vMtrlSpec;
	Vec4	vMtrlAmb;
	Vec4	vMtrlEmiv;
	_tagMtrlInfo()
		: vMtrlDiff(1.f, 1.f, 1.f, 1.f)
		, vMtrlSpec(1.f, 1.f, 1.f, 1.f)
		, vMtrlAmb(1.f, 1.f, 1.f, 1.f)
		, vMtrlEmiv(1.f, 1.f, 1.f, 1.f)
	{}

}tMtrlInfo;

typedef struct _tagFbxMat
{
	tMtrlInfo	tMtrl;
	wstring     strMtrlName;
	wstring     strDiff;
	wstring		strNormal;
	wstring		strSpec;
}tFbxMaterial;

typedef struct _tagWeightsAndIndices
{
	int		iBoneIdx;
	double	dWeight;
}tWeightsAndIndices;

typedef struct _tagContainer
{
	wstring								strName;
	vector<Vec3>						vecPos;
	vector<Vec3>						vecTangent;
	vector<Vec3>						vecBinormal;
	vector<Vec3>						vecNormal;
	vector<Vec2>						vecUV;

	vector<Vec4>						vecIndices;
	vector<Vec4>						vecWeights;

	vector<vector<UINT>>				vecIdx;
	vector<tFbxMaterial>				vecMtrl;

	// Animation 관련 정보
	bool								bAnimation;
	vector<vector<tWeightsAndIndices>>	vecWI;

	void Resize(UINT _iSize)
	{
		vecPos.resize(_iSize);
		vecTangent.resize(_iSize);
		vecBinormal.resize(_iSize);
		vecNormal.resize(_iSize);
		vecUV.resize(_iSize);
		vecIndices.resize(_iSize);
		vecWeights.resize(_iSize);
		vecWI.resize(_iSize);
	}

}tContainer;

// Animation
struct tFrameTrans
{
	Vec4	vTranslate;
	Vec4	vScale;
	Vec4	qRot;
};

struct tMTKeyFrame
{
	double	dTime;
	int		iFrame;
	Vec3	vTranslate;
	Vec3	vScale;
	Vec4	qRot;
};

struct tMTBone
{
	wstring				strBoneName;
	int					iDepth;
	int					iParentIndx;
	Matrix				matOffset;	// Offset 행렬(뼈 -> 루트 까지의 행렬)
	Matrix				matBone;   // 이거 안씀
	vector<tMTKeyFrame>	vecKeyFrame;
};

struct tMTAnimClip
{
	wstring		strAnimName;

	int			iStartFrame;
	int			iEndFrame;
	int			iFrameLength;

	double		dStartTime;
	double		dEndTime;
	double		dTimeLength;

	float		fUpdateTime; // 이거 안씀

	FbxTime::EMode eMode;
};
//	이거 안씀
enum class EC_ANI_CLIP {
	Idle,
	BackWalk,
	LeftWalk,
	RightWalk,
	Attack,
	Skill0,
	Skill1,
	Skill2,
	FrontLie,
	BackLie,
	Assaulted,
	End
};
enum class EC_PLAYER_ANIM
{
	Idle,
	Run,
	Damage,
	Death,
	Attack,
	Skill,
	Stun,
	End
};
enum class EC_STONE_ANIM {
	Idle
	,Attack
	,Damage
	,Death
	,Skill
	,End
};
enum class EC_SKELL_ANIM {
	Idle
	, Attack
	, Damage
	, Death
	, Run
	, Fast_run
	, End
};
enum class EC_MIMIC_ANIM {
	Idle
	, Open
	, Close
	, Jump
	, End
};
enum class EC_STONE_BOSS_ANIM
{
	Idle
	,Attack
	,Damaged
	,Death
	,Skill
	,End
};

enum class EC_MOSTER_TYPE {
	Stone
	,Skelleton
	,Mimic
	,End
};
//57.29577951729296
const float XM_ANGLE =  0.017453292f;
const float XM_DEGREE = 57.29577951f;
enum class EC_PATH_TYPE :BYTE {
	 Path = 0
	,Block = 1
	,Unit = 2
	,Err = 5

};

struct tPathCoord {
	float x, z;
};
struct tPathIdx {
	int x, z;
};
enum class EC_COL_MAP_TYPE:BYTE {
	 Block = 0
	,Unit = 1
	,Path = 2
	,End

};
struct tMTAnimClipEasy {
	wstring strAnimName;
	int			iEndFrame;
};
struct tMonsterInfo {
	bool bIsRespon;
	int nMonsterType;
	int nResponerIdx;
};
struct tRoundEvent {
	int nEventType;
};
struct tResponer {
	Vec3 vPos;
	Vec3 vRot;
	float Range;
	int nCurIdx;
};

struct tRoundInfo {
	//	Respon
	int nMapIdx;
	int nMaxMonsterCnt = 0;		//	이거는 사용안함
	int nCurMonsterCnt = 0;		//	이거는 플레이어가 잡은 몬스터수	0에서 시작
	//	개수임 (0 이면 아무것도 없다 최소 1마리!)
	int nClearMonsterCnt = 0;	//	만약 잡은 몬스터가 이것과 같거나 크다면 라운드 클리어

	int nResponType = 0;
	int nNowMaxMonster = 0;		//	스폰해야할 몬스터수
	int nNowCurMonster = 0;		//	스폰한 몬스터 수
	float fSponDelay = 3.f;
	float fCurSponDelay = 0.f;

	std::vector< tMonsterInfo> monsterInfo;
	std::vector< tMonsterInfo>::iterator itorMonster;
	tRoundEvent* tStartEvent = nullptr;
	tRoundEvent* tIngEvent = nullptr;
	tRoundEvent* tEndEvent = nullptr;
	bool bIsIng = false;
	bool bHaveBoss = false;
	CGameObject* pBoss= nullptr;
	int nBossType;
};
enum class EC_RESPON_TYPE {
	SQUENCE = 0		//	init 후에 잡히면 하나씩 최대까지 리스폰
	, ATONCE = 1		//	init 후에 한번에 다시 최대치가 나옴
	, RANDOM = 2		
};
struct tPortal {
	//	그 맵의 인덱스
	bool IsOn = false;
	Vec3 StartPos;
	Vec3 StartRot;
	Vec2 lu;
	Vec2 rd;
};
struct tPortal2 {
	//	그 맵의 인덱스
	bool IsOn = false;
	Vec3 StartPos;
	Vec3 StartRot;
	Vec4 PortalRect;	//	[ x: left] [ y : right ] [ z : down ] [ w : up ]
};
enum class EC_COL_TYPE :BYTE {
	Blocked = 0
	, Player = 1
	, _MonsterStart = 2
	, _MonsterEnd = 252
	, Path = 255
};
struct tColMap {
	BYTE _0lu, _1mu, _2ru;
	BYTE _3lm, _4mm, _5rm;
	BYTE _6ld, _7md, _8rd;

};
struct tDamageStack {
	UINT nBulletID;
	Vec3 vAttackDir;
	float fAttackDamage;
	UINT State;
};

enum class PLAYER_SKILL_TYPE
{
	SKILL_FIREBALL
	, SKILL_METEOR
	, SKILL_ICEARROW
	, SKILL_SNOWSTORM
	, SKILL_GOLD
	, END
};

struct tSkillElementValue
{
	//	위치 기록
	struct tPosValues
	{
		Vec3				vStartPos;
		Vec3				vOffsetPos;
		bool				bRandom = false;
		int					nRandomSeed = 0;

		bool				bSequence = false;
		Vec3				vSeqOffsetPos;
	};
	tPosValues PosValues;
	//	종료 내용
	struct tEndtypes
	{
		Vec3				vEndPos;			//	끝나는 위치가 존재하는거임
		float				fEndTime = 0.f;			//	끝나는 시간이 존재하는거임
		float				fCurTime = 0.f;
		float				fEndDist = 0.f;			//	끝나는 거리가 존재하는거임
	};
	tEndtypes Endtypes;
	//	움직임 값(go)
	struct tMoveValues
	{	
		Vec3				vMoveDir;				//	이거 방향대로 갈거임
		bool				bDonInitMove = false;	//	이거는 바라보는 방향을 내가 바라보는 방향으로 변경할것인가 하는 것이다
		bool				bTargetPlayer = false;	//	이거는 플레이어 위치에 쏠것인가하는것이다.
		DIR_TYPE			eDirType;				//	이거는 어떤방향을 가져올것인가 하는것이다.
		float				fMoveSpeed = 0.f;		//	이거는 곱해질거임

		bool				bRandom = false;
		int					nRandomSeed = 0;

		bool				bSequence = false;
		float				bSeqSpeed = 0.f;
	};
	tMoveValues MoveValues;
	//	콜리더 앵글
	struct tSinCosAngles
	{
		float				fSinCosInitAngle = 0.f;
		float				fSinCosCurAngle = 0.f;
		float				fSinCosAmplitude = 0.f;
		float				fSinCosSize = 0.f;
	};
	tSinCosAngles SinCosAngles;
	//	이벤트 시간
	struct tEventTimes
	{
		float				fStartEventMax = 0.f;	//	이거 이후에 발사됨
		float				fStartEventCur = 0.f;
		float				fEndEventMax = 0.f;	//	이거 이후에 끝남
		float				fEndEventCur = 0.f;
	};
	tEventTimes EventTimes;
	//	반복자
	struct tRepeateValues
	{
		int					nRepeateMax = 0;
	};
	tRepeateValues RepeateValues;

	//	데미지
	struct tOthers
	{
		float				fDamage = 0.f;
		UINT				nBulletType = 0;

	};
	tOthers Others;

};

struct tSkillEventValue
{

	Vec3				a_vStartPos;
	Vec3				a_vStratRot;


	Vec3				b_vScaleMax;
	Vec3				b_vScaleCur;
	Vec3				b_vScaleMin;
	Vec3				b_vScaleInit;
	float				b_fScaleAcc;

	CGameObject*		c_pTarget;
	float				c_fDelayDist;

	Vec3				d_vRotSpeed;

	int					eA_nTexIdx;
	UINT				eB_nType;
	Vec3				eC_vInitPos;
	Vec3				eD_vInitRot;
	Vec3				eE_vInitRotSpeed;

};
struct tSkillEventInfo
{
	int nTexIdx = 0;

};

struct tSkillArch
{

	int A_nBulletObjCnt;						//	
	int B_nShootBulletCnt;						//	한번에 발사되는수

	bool C_bBulletMesh;							//	직접 만들건가?
	wstring Ca_wstrMeshPath;					//	그럼 그녀석의 path

	bool D_bMapMgrMesh;							//	맵매니저에서 가져올건가
	bool D_bMapMgrFireBall = false;
	int Da_nMapMgrMeshIdx;						//	그럼 그것의 인덱스

	tSkillElementValue E_tSkillElementValue;	//	스킬 정보설정

	UINT F_nTypes;								//	스킬 정보	EC_SKILL_ELEMENT

	Vec3 G_vOffsetScale;						//	스케일

	wstring H_wstrLayer;						//	설정레이어
};

enum class EC_BOSS_STATE : UINT
{
	Done = 0x00
	, Evaluate = 0x01
	, Idle = 0x02
	, DestMove = 0x04
	, Turn = 0x08
	, Damaged = 0x10
	, Death = 0x20
	, BagicAttack = 0x40
	, Skill1 = 0x80
	, Skill2 = 0x100
	, Skill3 = 0x200
	, Collison = 0x400
	, Phase = 0x800
	, TargetPlayer = 0x1000
	, BackMove = 0x2000
	, SideMove = 0x4000
	, Search = 0x8000
	, Stay = 0x10000
	, End
};
enum class EC_MAPPING_ANIM
{
	Idle
	, Run
	, Damage
	, Death
	, Attack
	, Skill1
	, Skill2
	, Skill3
	, Stun
	, End
};