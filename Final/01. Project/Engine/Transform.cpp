#include "stdafx.h"
#include "Transform.h"

#include "ConstantBuffer.h"
#include "Device.h"


#include "TimeMgr.h"


tTransform	g_transform;
Vec3 vAsis[3] = { Vec3::Right , Vec3::Up , Vec3::Front };

CTransform::CTransform()
	: CComponent(COMPONENT_TYPE::TRANSFORM)
	, m_vLocalPos(Vec3(0.f, 0.f, 0.f))
	, m_vLocalRot(Vec3(0.f, 0.f, 0.f))
	, m_vLocalScale(Vec3(1.f, 1.f, 1.f))
	, m_vLocalPrePos(Vec3(0.f, 0.f, 0.f))
	, m_bHasOffset((BYTE)EC_TRANS_OFFSET::NONE)
	,m_bPhysics(false)
	, m_bMoveStop(false)
	, m_nFrameMax(180)
	, m_nFrameCur(0)
	, m_bSkillPhysics(false)
{
	m_bAIUpdate = true;

	m_fValue[(UINT)PHYSICS_FLOAT::MASS] = 10.0f;
	m_fValue[(UINT)PHYSICS_FLOAT::SEARCHRANGE] = 150.0f;		//	(??)여기수정
	m_fValue[(UINT)PHYSICS_FLOAT::ATTACKRANGE] = 50.0f;
	m_fValue[(UINT)PHYSICS_FLOAT::ROTSPEED] = 25.0f * XM_ANGLE;

	m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)PHYSICS_VEC3::FRICTION] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)PHYSICS_VEC3::LOCAL_POS] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)PHYSICS_VEC3::LOCAL_SCALE] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)PHYSICS_VEC3::LOCAL_ROT] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)PHYSICS_VEC3::LOCAL_POS_OFFSET] = Vec3(0.f, 0.f, 0.f);

	m_fn = NULL;
	m_gravity = NULL;
	m_mass = NULL;
}

CTransform::~CTransform()
{
}

void CTransform::AddForce(float x, float y, float z, float time)
{
	float accX = x / m_fValue[(UINT)PHYSICS_FLOAT::MASS];
	float accY = y / m_fValue[(UINT)PHYSICS_FLOAT::MASS];
	float accZ = z / m_fValue[(UINT)PHYSICS_FLOAT::MASS];

	m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x = m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x + accX * DT;
	m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y = m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y + accY * DT;
	m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z = m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z + accZ * DT;

	//가속도  = 힘/ 질량

}

void CTransform::AddForce(const Vec3 & _dir, const float & _time)
{

//	Vec3 acc = _dir / m_vec3Value[(UINT)PHYSICS_FLOAT::MASS];
	m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY] = m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY] + (_dir * DT);
}

void CTransform::setVelocity(float x, float y, float z)
{

	m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x = x;
	m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y = y;
	m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z = z;
}

void CTransform::finalupdate()
{
	
	static Vec3 vOff;
	 
	if (m_bSkillPhysics)
	{
		float dirY;
		float velSize = sqrt(m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y * m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y);
		//단위벡터를 만들기위해 현재 벡터의 크기를 알아냄
//		if (velSize > 0.00001)// 0.001보다 작을때는 멈춰있다고 가정하자 , 그러니 이보다 크면 마찰력이 적용되게 하자  
//		{
			dirY = m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y / velSize;
			float gravity = -9.8f;
			m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION].y += gravity;//y 축은 항상 바닥에 붙어있도록 
			m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y += m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION].y * DT;
	//		std::cout << " physics down" << std::endl;

		//}
		//else {
		//	m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y = 0.f;

		//}

		m_vLocalPos.y += m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y * DT;
		

		if (m_vLocalPos.y <= 0)
		{
	//		std::cout << " physics fix - 2" << std::endl;

			m_vLocalPos.y = 0.0f;
			m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y = 0.0f;
		}

		Transform()->SetLocalPos(m_vLocalPos);

	}
	else if (m_bPhysics && !m_bSkillPhysics)
	{
		//스테이터스 추가 시작 
		//=======================================================================
		float dirX, dirY, dirZ;//현재 오브젝트의 진행방향을 알아내는것 이때, 단위벡터로 만들어야함. 
		float velSize = sqrt(m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x*m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x
			+ m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y * m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y
			+ m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z * m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z);//단위벡터를 만들기위해 현재 벡터의 크기를 알아냄
	
		if (velSize > 0.00001)// 0.001보다 작을때는 멈춰있다고 가정하자 , 그러니 이보다 크면 마찰력이 적용되게 하자  
		{

			using namespace std;
			//	cout << "now";
			dirX = m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x / velSize;
			dirY = m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y / velSize;
			dirZ = m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z / velSize;

			float m_dir;
			float fn = 0.1f;//마찰계수
			if (m_fn != NULL) fn = m_fn;
			float gravity = -9.8f;

			if (m_gravity != NULL) gravity = m_gravity;
			float mass = 10;
			if (m_mass != NULL) mass = m_mass;
			m_fValue[(UINT)PHYSICS_FLOAT::MASS] = mass;
			//마찰력 = -방향 * 힘 ( 질량 * 계수 ) ;
			m_vec3Value[(UINT)PHYSICS_VEC3::FRICTION].x = -dirX * (m_fValue[(UINT)PHYSICS_FLOAT::MASS] * 9.8f * fn);
			m_vec3Value[(UINT)PHYSICS_VEC3::FRICTION].z = -dirZ * (m_fValue[(UINT)PHYSICS_FLOAT::MASS] * 9.8f * fn);
			//	m_friction.x = -1.f* (Transform()->GetWorldDir(DIR_TYPE::RIGHT)) * (m_mass * 9.8f * m_fn);
			//	m_friction.z = -1.f *(Transform()->GetWorldDir(DIR_TYPE::RIGHT)) * (m_mass * 9.8f * m_fn);

				//가속도 = 마찰력 / 질량 
			m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION].x = m_vec3Value[(UINT)PHYSICS_VEC3::FRICTION].x / m_fValue[(UINT)PHYSICS_FLOAT::MASS];
			//	m_acceleration.y = m_friction.y / m_mass;

			m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION].y += gravity;//y 축은 항상 바닥에 붙어있도록 


			m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION].z = m_vec3Value[(UINT)PHYSICS_VEC3::FRICTION].z / m_fValue[(UINT)PHYSICS_FLOAT::MASS];

			//속도 += 가속도 * 시간 
		//	m_velocity += m_acceleration * DT;
			m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x += m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION].x * DT;
			m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y += m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION].y * DT;
			m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z += m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION].z * DT;
		}
		else
		{
			m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x = 0.f;
			m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y = 0.f;
			m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z = 0.f;
		}

		//위치 += 속도 * 시간 ; 
		//Vec3 vPos = Transform()->GetLocalPos();
		m_vLocalPos.x += m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x * DT;
		m_vLocalPos.y += m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y * DT;
		m_vLocalPos.z += m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z * DT;


		if (m_vLocalPos.y <= 0)
		{
	
			m_vLocalPos.y = 0.0f;
		}
		if (m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y <= 0)
		{

			m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y = 0.0f;
		//	m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y = 0;
		//	m_vLocalPos.y = 0.0f;
		}

		Transform()->SetLocalPos(m_vLocalPos);

		//	m_bPhysics = GetObj()->getPhysics();

				//스테이터스 추가 시작 
				//=======================================================================
			//	dirX, dirY, dirZ;//현재 오브젝트의 진행방향을 알아내는것 이때, 단위벡터로 만들어야함. 
			//	float velSize = sqrt(m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x*m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x
			//		+ m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y * m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y
			//		+ m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z * m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z);//단위벡터를 만들기위해 현재 벡터의 크기를 알아냄
			//
			//	if (velSize > 0.00001f)// 0.001보다 작을때는 멈춰있다고 가정하자 , 그러니 이보다 크면 마찰력이 적용되게 하자  
			//	{
			//		using namespace std;
			//		//	cout << "now";
			//		dirX = m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x / velSize;
			//		dirY = m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y / velSize;
			//		dirZ = m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z / velSize;
			//
			//
			//		float m_dir;
			//		float m_fn = 0.3f;//마찰계수
			//		//마찰력 = -방향 * 힘 ( 질량 * 계수 ) ;
			//		m_vec3Value[(UINT)PHYSICS_VEC3::FRICTION].x = -dirX * (m_fValue[(UINT)PHYSICS_FLOAT::MASS] * 9.8f * m_fn);
			//		m_vec3Value[(UINT)PHYSICS_VEC3::FRICTION].z = -dirZ * (m_fValue[(UINT)PHYSICS_FLOAT::MASS] * 9.8f * m_fn);
			//		//	m_friction.x = -1.f* (Transform()->GetWorldDir(DIR_TYPE::RIGHT)) * (m_mass * 9.8f * m_fn);
			//		//	m_friction.z = -1.f *(Transform()->GetWorldDir(DIR_TYPE::RIGHT)) * (m_mass * 9.8f * m_fn);
			//
			//
			//
			//			//가속도 = 마찰력 / 질량 
			//		m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION].x = m_vec3Value[(UINT)PHYSICS_VEC3::FRICTION].x / m_fValue[(UINT)PHYSICS_FLOAT::MASS];
			//		//	m_acceleration.y = m_friction.y / m_mass;
			//		m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION].y += -9.8f;//y 축은 항상 바닥에 붙어있도록 
			//
			//		m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION].z = m_vec3Value[(UINT)PHYSICS_VEC3::FRICTION].z / m_fValue[(UINT)PHYSICS_FLOAT::MASS];
			//
			//		//속도 += 가속도 * 시간 
			//	//	m_velocity += m_acceleration * DT;
			//		m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x += m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION].x * DT;
			//		m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y += m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION].y * DT;
			//		m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z += m_vec3Value[(UINT)PHYSICS_VEC3::ACCELERATION].z * DT;
			//
			//
			//	}
			//	else
			//	{
			//		m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x = 0.f;
			//		m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y = 0.f;
			//		m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z = 0.f;
			//	}
			//
			//
			//	//위치 += 속도 * 시간 ; 
			//	//Vec3 vPos = Transform()->GetLocalPos();
			//	m_vLocalPos.x += m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].x * DT;
			//	m_vLocalPos.y += m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y * DT;
			//	m_vLocalPos.z += m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].z * DT;
			//
			//
			//	if (m_vLocalPos.y <= 0)
			//	{
			//		m_vLocalPos.y = 0.0f;
			//		m_vec3Value[(UINT)PHYSICS_VEC3::VELOCITY].y = 0.0f;
			//	}
			//
			//}
			//=======================================================================
			//스테이터스 추가 끝
	}
	
	
	++m_nFrameCur;
	Matrix matTranslation = XMMatrixTranslation(m_vLocalPos.x, m_vLocalPos.y, m_vLocalPos.z);
	Matrix matScale = XMMatrixScaling(m_vLocalScale.x, m_vLocalScale.y, m_vLocalScale.z);

	Matrix matRot = XMMatrixRotationX(m_vLocalRot.x);
	matRot *= XMMatrixRotationY(m_vLocalRot.y);
	matRot *= XMMatrixRotationZ(m_vLocalRot.z);

	static Vec3 arrDefault[(UINT)DIR_TYPE::END] = { Vec3::Right, Vec3::Up, Vec3::Front };

	for (UINT i = 0; i < (UINT)DIR_TYPE::END; ++i)
	{
		m_vLocalDir[i] = XMVector3TransformNormal(arrDefault[i], matRot);
	}

	// 로컬 x (크기 x 회전 x 이동)(월드행렬)
	//	추<
	m_matNonOffWorld = matScale * matRot * matTranslation;
	if ((BYTE)EC_TRANS_OFFSET::POSITION & m_bHasOffset) {
		vOff = m_vLocalPos + m_vLocalPosOffset;
		matTranslation = XMMatrixTranslation(vOff.x, vOff.y, vOff.z);
	}
	if ((BYTE)EC_TRANS_OFFSET::ROTATE & m_bHasOffset) {
		matRot = Matrix::Identity;
		vOff = m_vLocalRot + m_vLocalRotOffset;
		matRot = XMMatrixRotationX(vOff.x);
		matRot *= XMMatrixRotationY(vOff.y);
		matRot *= XMMatrixRotationZ(vOff.z);
	}

	// 로컬 x (크기 x 회전 x 이동)(월드행렬)
	m_matWorld = matScale * matRot * matTranslation;

	if (GetObj()->GetParent())
	{
		m_matWorld *= GetObj()->GetParent()->Transform()->GetWorldMat();

		for (UINT i = 0; i < (UINT)DIR_TYPE::END; ++i)
		{
			m_vWorldDir[i] = XMVector3TransformNormal(arrDefault[i], m_matWorld);
			m_vWorldDir[i].Normalize();
			
		}
	}
	else
	{
		memcpy(m_vWorldDir, m_vLocalDir, sizeof(Vec3) * 3);
	}

	// 역행렬 계산
	m_matWorldInv = XMMatrixInverse(nullptr, m_matWorld);
	if (Vector3Cmp(m_vLocalPos, m_vLocalPrePos))return;
	m_vObjectDir = (m_vLocalPos - m_vLocalPrePos).Normalize();
	m_vLocalPrePos = m_vLocalPos;
}

void CTransform::UpdateData()
{
	static CConstantBuffer* pCB = CDevice::GetInst()->GetCB(CONST_REGISTER::b0);

	g_transform.matWorld = m_matWorld;
	g_transform.matWorldInv = m_matWorldInv;
	g_transform.matWV = g_transform.matWorld * g_transform.matView;
	g_transform.matWVP = g_transform.matWV * g_transform.matProj;
	g_transform.objID = GetObj()->GetID();

	UINT iOffsetPos = pCB->AddData(&g_transform);
	CDevice::GetInst()->SetConstBufferToRegister(pCB, iOffsetPos);
}
Vec3 CTransform::GetWorldScale()
{
	CGameObject* pParent = GetObj()->GetParent();
	Vec3 vWorldScale = m_vLocalScale;
	while (pParent)
	{
		vWorldScale *= pParent->Transform()->GetLocalScale();

		pParent = pParent->GetParent();
	}
	return vWorldScale;
}
bool CTransform::IsCasting(const Vec3 & _vPos)
{
	Vec3 vWorldPos = GetWorldPos();
	Vec3 vWorldScale = GetWorldScale();
	if (vWorldPos.x - vWorldScale.x / 2.f < _vPos.x && _vPos.x < vWorldPos.x + vWorldScale.x / 2.f
		&& vWorldPos.y - vWorldScale.y / 2.f < _vPos.y && _vPos.y < vWorldPos.y + vWorldScale.y / 2.f)
	{
		return true;
	}
	return false;
}
float CTransform::GetMaxScale()
{
	Vec3 vWorldScale = GetWorldScale();
	float fMax = max(vWorldScale.x, vWorldScale.y);
	fMax = max(fMax, vWorldScale.z);
	return fMax;
}
void CTransform::LookAt(const Vec3 & _vLook)
{
	Vec3 vFront = _vLook;
	vFront.Normalize();

	Vec3 vRight = Vec3::Up.Cross(_vLook);
	vRight.Normalize();

	Vec3 vUp = vFront.Cross(vRight);
	vUp.Normalize();

	Matrix matRot = XMMatrixIdentity();

	matRot.Right(vRight);
	matRot.Up(vUp);
	matRot.Front(vFront);

	m_vLocalRot = DecomposeRotMat(matRot);

	// 방향벡터(우, 상, 전) 갱신하기	
	Matrix matRotate = XMMatrixRotationX(m_vLocalRot.x);
	matRotate *= XMMatrixRotationY(m_vLocalRot.y);
	matRotate *= XMMatrixRotationZ(m_vLocalRot.z);

	for (UINT i = 0; i < (UINT)DIR_TYPE::END; ++i)
	{
		m_vLocalDir[i] = XMVector3TransformNormal(vAsis[i], matRotate);
		m_vLocalDir[i].Normalize();
		m_vWorldDir[i] = m_vLocalDir[i];
	}
}
void CTransform::SaveToScene(FILE * _pFile)
{
	UINT iType = (UINT)GetComponentType();
	fwrite(&iType, sizeof(UINT), 1, _pFile);

	fwrite(&m_vLocalPos, sizeof(Vec3), 1, _pFile);
	fwrite(&m_vLocalScale, sizeof(Vec3), 1, _pFile);
	fwrite(&m_vLocalRot, sizeof(Vec3), 1, _pFile);
}
void CTransform::LoadFromScene(FILE * _pFile)
{
	fread(&m_vLocalPos, sizeof(Vec3), 1, _pFile);
	fread(&m_vLocalScale, sizeof(Vec3), 1, _pFile);
	fread(&m_vLocalRot, sizeof(Vec3), 1, _pFile);
}

