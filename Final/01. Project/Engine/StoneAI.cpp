#include "stdafx.h"
#include "StoneAI.h"
#include "Animator3D.h"
#include "StatusComponent.h"
#include "BulletBossSkillScript.h"
#include "SkillStoneShower.h"
#include "MapMgr.h"
#include "EffectScript.h"

//
CStoneAI::CStoneAI()
	: CScript((UINT)SCRIPT_TYPE::AI)
	, m_pPlayer(nullptr)
	, m_pInitPos()

	, m_nHealCnt(2)
	, m_state(EC_STONE_STATE::Evaluate)

	, m_fMaxDeathTime(0.03333f * 6.0f)
	, m_fCurDeathTime(0.0f)
	, m_fMaxSkillTime(0.03333f* 110.f)
	, m_fCurSkillTime(0.03333f* 110.f)
	, m_fBagicAttackMaxTime(0.0333f*26.f)
	, m_fBagicAttackHitTime(0.0333f* 4.f)
	, m_fBagicAttackRange(12.f)
	, m_fBagicAttackDamage(10.f)
	, m_fBagicAttackAngle(60.f * XM_ANGLE)
	, m_fPatrolDistNow(15.f)
	, m_fPatrolDistMax(15.f)
	, m_fTic(0.f)
	, m_fToc(0.0f)
	, m_fTicMax(0.016f)
	, m_fSkill1TimeCur(0.f)
	, m_fSkill1TimeShoot(0.f)
	, m_fSkill1TimeMax(0.f)	//	여기 필히 수정
	, m_bBagicAttackOn(false)
	, m_bDamaged(false)

{
	m_pInitPos = { 0.f,0.f,400.f };
	m_fBagicAttackTiming = m_fBagicAttackMaxTime + 0.0000001f;
	m_fBagicAttackHitTime = m_fBagicAttackMaxTime * 0.2f;
	m_fFindDestTimeMax = 5.f;
	m_fFindDestTimeCur = 0.0f;
	m_pEffect = nullptr;
}


CStoneAI::~CStoneAI()
{
}

void CStoneAI::update()
{

	if (!GetObj()->IsActive())return;

	if (m_fTic < m_fTicMax)
	{
		m_fTic += DT;
		m_fToc = m_fTic;
		return;
	}
	else
	{
		m_fToc = m_fTic;
		m_fTic = 0.0f;
	}
	//	-----------------------------------------------------------------------
	//	자료부
	static Vec3 mePos, meLook, playerPos, lookPlayer, turnOper, rot, dest;
	static CStatusComponent* state = nullptr;
	static CTransform* trans = nullptr;
	static CAnimator3D* animate = nullptr;
	static float dist, angle, oper, idleDist, idleDest, fHeight;

	//	-----------------------------------------------------------------------
	//	계산부
	state = GetObj()->StatusComponent();
	trans = GetObj()->Transform();
	animate = GetObj()->Animator3D();

	int nonAttack = 0;

	playerPos = m_pPlayer->Transform()->GetLocalPos();
	mePos = trans->GetLocalPos();
	meLook = trans->GetWorldDir(DIR_TYPE::FRONT);
	int a = state->GetMapNum();
	fHeight = CMapMgr::GetInst()->GetHeight(state->GetMapNum(), mePos);
	playerPos.y = 0.0f;
	mePos.y = 0.0f;
	meLook.y = 0.0f;

	lookPlayer = (playerPos - mePos).Normalize();
	turnOper = meLook.Cross(lookPlayer);

	rot = trans->GetLocalRot();

	dist = mePos.Distance(playerPos, mePos);
	angle = meLook.Dot(lookPlayer);

	oper = (turnOper.y < 0.f) ? -1.0f : 1.0f;

	idleDist = 0.0f;
	idleDest = 10.f;

	//	데미지 계산
	float fDamaged = 0.f;
	std::list<tDamageStack>& stack = state->GetDamageStack();
	while (!stack.empty())
	{
		fDamaged += stack.back().fAttackDamage;
		stack.pop_back();
	}
	if (fDamaged > 10.f)	//	순간 데미지
	{
		m_bDamaged = true;
	}
	if (!FloatCmp(fDamaged, 0.0f, 0.000001f))state->setHpDown(fDamaged);
	if (m_bDamaged)
	{
		m_state = EC_STONE_STATE::Damaged;
	}


	if (state->GetLowHp())
	{
		m_state = EC_STONE_STATE::Death;
	}


	//	반복부
	switch (m_state)
	{
	case EC_STONE_STATE::Evaluate:
		if (!animate->IsAnimIdx((UINT)EC_STONE_ANIM::Idle))
		{
			GetObj()->Animator3D()->ChangeAnimation((UINT)EC_STONE_ANIM::Idle);
		}
		break;
	case EC_STONE_STATE::BackHeal:
		if (dist <= state->GetFloat(STATUS_FLOAT::ATTACKRANGE))
		{
			trans->SetLocalPos(lookPlayer * (state->GetFloat(STATUS_FLOAT::MOVESPEED) * m_fToc * -1.f));
		}
		else
		{
			state->SetFloat(STATUS_FLOAT::ATTACKRANGE, state->GetFloat(STATUS_FLOAT::HP) + 20.0f);
			m_state = EC_STONE_STATE::Evaluate;
		}
		break;
	case EC_STONE_STATE::Move:
		GetObj()->Animator3D()->ChangeAnimation((UINT)EC_STONE_ANIM::Idle, true);
		mePos.y = fHeight;
		trans->SetLocalPos(mePos + (lookPlayer * m_fToc * state->GetFloat(STATUS_FLOAT::MOVESPEED)));
		m_state = EC_STONE_STATE::Evaluate;
		break;
	case EC_STONE_STATE::Death:
		animate->ChangeAnimation((UINT)EC_STONE_ANIM::Death, true);
		if (animate->IsAnimIdx((UINT)EC_STONE_ANIM::Death) )
		{
			if (animate->IsAnimEnd())
			{
				if (m_pEffect == nullptr)
				{
					mePos.y +=10.f;
					m_pEffect = CEffectScript::CreateFireBurn(mePos, 5.f, 6.f, 20.f, 25.f, 15.f);
				}
				else
				{
					if (!m_pEffect->IsActive())
					{
						mePos.y += 10.f;
						m_pEffect->GetScript<CEffectScript>()->SetScales(5.f, 6.f, 20.f, 25.f, 15.f);
						m_pEffect->SetActive(true);
					}
				}

				state->OffHpUies();
				CMapMgr::GetInst()->DropItem(mePos);
				GetObj()->SetActive(false);
				m_state = EC_STONE_STATE::Evaluate;
			}
		}
		//	스킬을 막사용하는 문제가 있다 이거 해결해야함
		break;
	case EC_STONE_STATE::Escape:
		m_state = EC_STONE_STATE::Evaluate;
		break;
	case EC_STONE_STATE::SKill1:
		//	shot Skill
		//	시전시간도 넣어야함.
		//	0810

		//if (m_fSkill1TimeCur > 0.0f) 
		//{
		//	GetObj()->Animator3D()->ChangeAnimation((UINT)EC_STONE_ANIM::Skill,true);
		//	m_fSkill1TimeCur -= DT;
		//	if (m_fSkill1TimeCur < m_fSkill1TimeShoot)
		//	{
		//		
		//		GetObj()->GetScript<CBulletBossSkillScript>()->shoot((UINT)BULLET_TYPE::CIRCLE);
		//		state->SetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW, 0.0f);
		//		m_fSkill1TimeCur = m_fSkill1TimeMax;
		//		m_state = EC_STONE_STATE::Evaluate;
		//	}
		//}
		//else
		//{
		//	state->SetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW, 0.0f);
		//	m_fSkill1TimeCur = m_fSkill1TimeMax;
		//	m_state = EC_STONE_STATE::Evaluate;
		//}
		if (animate->IsAnimIdx((UINT)EC_STONE_ANIM::Skill))
		{
			if (animate->IsAnimEnd())
			{
				m_state = EC_STONE_STATE::Evaluate;
				m_bSkillShootOn = false;
			}
			else
			{
				if (animate->GetCurAnimLeftTime() < 1.f)
				{
					if (!m_bSkillShootOn)
					{
						m_bSkillShootOn = true;
						Vec3 skillPos = mePos + (meLook * 20.f);
						skillPos.y = fHeight + 50.f;
						m_pSkill->Shoot(skillPos, Vec3::Zero, Vec3::Down);
						state->SetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW, 0.f);
					}
				}
			}
		}
		else
		{
			animate->ChangeAnimation((UINT)EC_STONE_ANIM::Skill, true);
		}
		break;
	case EC_STONE_STATE::Skill2:
		state->SetFloat(STATUS_FLOAT::SKILL_2_COOL_NOW, 0.0f);
		m_state = EC_STONE_STATE::Evaluate;
		break;
	case EC_STONE_STATE::BagicAttack:
		if (animate->IsAnimIdx((UINT)EC_STONE_ANIM::Attack))
		{

			if (animate->IsAnimEnd())
			{
				if (m_bBagicAttackOn)
				{

					m_pPlayer->StatusComponent()->DamageStack(GetObj()->GetID(),meLook, m_fBagicAttackDamage);
					m_bBagicAttackOn = false;
				}
				state->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW, 0.f);
				m_state = EC_STONE_STATE::Evaluate;
			}
			else
			{
				if (animate->GetCurAnimLeftTime() < 1.f)
				{
					if (dist < m_fBagicAttackRange && abs(angle) < m_fBagicAttackAngle)
					{
						if (!m_bBagicAttackOn)m_bBagicAttackOn = true;
					}
				}
			}
		}
		else
		{
			animate->ChangeAnimation((UINT)EC_STONE_ANIM::Attack);
		}
		break;
	case EC_STONE_STATE::Turn:
		if (FloatCmp(angle, 1.0f, 0.01f))
		{
			m_state = EC_STONE_STATE::Evaluate;
		}
		else
		{
			GetObj()->Animator3D()->ChangeAnimation((UINT)EC_STONE_ANIM::Idle, true);
			rot.y += state->GetFloat(STATUS_FLOAT::ROTSPEED)*m_fToc * oper;
			trans->SetLocalRot(rot);
		}
		break;
	case EC_STONE_STATE::Patrol:
		if (m_fPatrolDistNow > 0.0f)
		{

			m_fPatrolDistNow -= m_fToc * state->GetFloat(STATUS_FLOAT::MOVESPEED);
			mePos.y = fHeight;
			trans->SetLocalPos(mePos + (lookPlayer * m_fToc * state->GetFloat(STATUS_FLOAT::MOVESPEED)));
		}
		else
		{
			m_fPatrolDistNow = m_fPatrolDistMax;
			m_state = EC_STONE_STATE::Evaluate;
		}
		break;
	case EC_STONE_STATE::DestGo:
		if (Vec3::Distance(dest, mePos) > 0.5f)
		{
			mePos.y = fHeight;
			trans->SetLocalPos(mePos + (Vec3(dest - mePos).Normalize() * m_fToc * state->GetFloat(STATUS_FLOAT::MOVESPEED)));
			if (m_fFindDestTimeMax > m_fFindDestTimeCur)
			{
				m_fFindDestTimeCur += m_fToc;
			}
			else
			{
				m_fFindDestTimeCur = 0.0f;
				m_state = EC_STONE_STATE::Evaluate;
			}
			//	푸쉬용
		}
		else
		{
			m_fFindDestTimeCur = 0.0f;
			m_state = EC_STONE_STATE::Evaluate;
		}
		break;
	case EC_STONE_STATE::MoveBack:
		mePos.y = fHeight;
		trans->SetLocalPos(mePos + (lookPlayer * (state->GetFloat(STATUS_FLOAT::MOVESPEED) * m_fToc * -1.f)));
		m_state = EC_STONE_STATE::Evaluate;
		break;
	case EC_STONE_STATE::Damaged:
		if (animate->IsAnimIdx((UINT)EC_SKELL_ANIM::Damage))
		{
			if (animate->IsAnimEnd())
			{
				m_bDamaged = false;
				m_state = EC_STONE_STATE::Evaluate;
			}
		}
		else
		{
			GetObj()->Animator3D()->ChangeAnimation((UINT)EC_SKELL_ANIM::Damage, true);
		}
		break;
	default:
		m_state = EC_STONE_STATE::Evaluate;
		break;
	}
	if (m_state != EC_STONE_STATE::Evaluate)return;


	//	--------------------------------------------------------------------------------------------
	//		새로운 평가부
	if (dist < state->GetFloat(STATUS_FLOAT::MINRANGE))
	{
		//	뒤로 물러나기
		m_state = EC_STONE_STATE::MoveBack;
	}
	else if (dist < state->GetFloat(STATUS_FLOAT::ATTACKRANGE))
	{
		//	공격
		if (state->GetFloat(STATUS_FLOAT::SKILL_1_COOL_MAX) < state->GetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW))
		{
			if (FloatCmp(angle, 1.f, 0.01f))
			{
				m_state = EC_STONE_STATE::SKill1;
				return;
			}
			else
			{
				m_state = EC_STONE_STATE::Turn;
			}
		}
		else
		{
			//	현재 쿨에 이걸 더하기
			state->SetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW, state->GetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW) + m_fToc);
		}

		if (state->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX) < state->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW))
		{
			if (FloatCmp(angle, 1.f, 0.01f))
			{
				m_state = EC_STONE_STATE::BagicAttack;
				return;
			}
			else
			{
				m_state = EC_STONE_STATE::Turn;
			}
		}
		else
		{
			state->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW, state->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW) + m_fToc);
		}
	}
	else if (state->GetFloat(STATUS_FLOAT::SEARCHRANGE))
	{
		//	이동
		if (FloatCmp(angle, 1.f, 0.01f))
		{
			m_state = EC_STONE_STATE::Move;
		}
		else
		{
			m_state = EC_STONE_STATE::Turn;
		}
	}
	else
	{
		//	idle
		idleDist = m_pInitPos.Distance(m_pInitPos, mePos);
		if (idleDist > 20.0f)
		{
			rot.y += XM_PI;
			trans->SetLocalRot(rot);
			dest = m_pInitPos;
			m_state = EC_STONE_STATE::DestGo;
			return;
		}
		else
		{
			rot.y += XM_ANGLE * 30.f * oper;
			idleDest = 10.0f;
			m_state = EC_STONE_STATE::Patrol;
			return;
		}
	}

}

void CStoneAI::Reset(const Vec3 & _pos, const Vec3 & _rot)
{
	m_pInitPos = _pos;
	GetObj()->Transform()->SetLocalPos(_pos);
	GetObj()->Transform()->SetLocalRot(_rot);


	m_fCurDeathTime = 0.0f;
	m_fCurSkillTime = m_fMaxSkillTime;	//	바로사용가능
	m_fBagicAttackTiming = m_fBagicAttackMaxTime;
	m_fPatrolDistNow = m_fPatrolDistMax;
	m_fDamagedNow = m_fDamagedMax;
	m_state = EC_STONE_STATE::Evaluate;
	
	CStatusComponent* s = GetObj()->StatusComponent();
	s->SetStone(_pos, _rot);
	//s->SetFloat(STATUS_FLOAT::HP, 100.f);
	//s->SetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW, s->GetFloat(STATUS_FLOAT::SKILL_1_COOL_MAX));
	//s->SetFloat(STATUS_FLOAT::SKILL_2_COOL_NOW, s->GetFloat(STATUS_FLOAT::SKILL_2_COOL_MAX));
	//s->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW, s->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX));
}

void CStoneAI::OnCollisionEnter(CCollider2D * _pOther)
{
}

void CStoneAI::OnCollision(CCollider2D * _pOther)
{
	if (CMapMgr::GetInst()->GetCalculMapIdx() != GetObj()->StatusComponent()->GetMapNum())return;
	Vec3 otherPos = _pOther->Transform()->GetLocalPos();
	Vec3 mePos = GetObj()->Transform()->GetLocalPos();

	Vec3 dir = (otherPos - mePos).Normalize();
	dir.y = 0.f;
	GetObj()->Transform()->SetLocalPos(mePos + (dir * -3.f));
}