#include "stdafx.h"
#include "MapScript.h"
#include "StoneAI.h"
#include "StatusComponent.h"
#include "MapMgr.h"
#include "MimicAi.h"
#include "SkelletonAI.h"
#include "BulletBossSkillScript.h"
#include "Collider2D.h"
#include "MonsterScript.h"
#include "EnviromentObj.h"
#include "MeshData.h"
#include "ResMgr.h"
#include "SkillStoneShower.h"
#include "SkillElementDir.h"
#include "SkillEvent.h"
#include "RenderMgr.h"
#include "Material.h"

#include "BT.h"
#include "BTInit.h"
#include "BTAI.h"

#include <fstream>
int CMapScript::g_nTowerIn = 8;
CGameObject* CMapScript::m_pFloorUI = nullptr;
CGameObject* CMapScript::m_pClearUIObj = nullptr;
bool CMapScript::m_bPossibleTower = false;

CMapScript::CMapScript() 
	: CScript((UINT)SCRIPT_TYPE::RESPON)
, m_nPossiblePortal(0)
, m_pPlayer(nullptr)
, m_vMapHalfSize{200.0f,200.0f}
, m_vMapRect{0.f,0.f ,0.f ,0.f }
, m_nCurRound(0)
, m_nMaxRound(0)
, m_vMapSize{400.f,400.f}
, m_pOriginColliderMap(nullptr)
, m_pColliderMap(nullptr)
, m_pHeightMap(nullptr)
, m_eState(EC_MAP_STATE::Stop)
, m_bPreStop(false)
, m_bLevelClear(false)
, m_bTowerPortalIn(false)
, m_bStartPortalIn(false)
, m_bBossClear(false)

, m_bStopMap(false)
, m_nTowerPortal(0)
, m_nStartPortal(0)
, m_fMapMoveDelayMax(1.f)
, m_fMapMoveDelayCur(0.f)
, m_bCurColId((BYTE)EC_COL_TYPE::_MonsterStart)
, m_nForceActive(2)
, m_pBoss(nullptr)
{
	m_fFloorDelayMax = 3.f;
	m_vMapHalfSize = { 200.f,200.0f };
	
	for (int i = 0; i < (UINT)EC_PORTAL_NUM::End; ++i)
	{
		m_bSummonsOn[i] = false;
		m_fSummonsCur[i] = 0.0f;
		m_fSummonsMax[i] = 1.5f;
		m_pSummonObj[i] = nullptr;
	}
	
}


CMapScript::~CMapScript()
{
	if (m_pOriginColliderMap != nullptr) { delete[] m_pOriginColliderMap; m_pOriginColliderMap = nullptr; }
	if (m_pColliderMap != nullptr) { delete[] m_pColliderMap; m_pColliderMap = nullptr; }
	if (m_pHeightMap != nullptr) { delete[] m_pHeightMap; m_pHeightMap = nullptr; }
	for (int i = 0; i < (UINT)EC_MOSTER_TYPE::End; ++i) 
	{
		m_vecReady[i].clear();
		m_vecStop[i].clear();
	}
	m_pPlayer = nullptr;
}

void CMapScript::update()
{

	if (!m_bDoInit)return;
	if (m_bStopMap)return;

	//	여기서 높이적용 및 충돌맵 처리하자
	//	그리고 컬링도 여기서 적용하자
	//	먼저 플레이어가 여기 안에 들어와있는지 부터보자.	

	Vec3 playerPos = m_pPlayer->Transform()->GetLocalPos();	//	플레이어 위치
	
	Vec3 v3destPos = playerPos;
	Vec3 playerRot = m_pPlayer->Transform()->GetLocalRot();
	Vec3 v3destPosText,v3colIdx,moveDir, v3Deci,v3Test;
	Vec2 v2fIdx,v2Deci,v2ColIdx;
	XMINT2 vi2Idx;
	int nIdx = 0;
	float fHight,fA,fB,fC,fD,uH,uV;
	BYTE bValue = 0;
	int op = 1;
	BYTE dsetColNum = (BYTE)EC_COL_TYPE::Player;
	//	캐릭터 포지션 - m_v3TextureCoordMapPos
	//	이거를 인덱스화
	//	m_v3TextureCoordMapPos
	//	높이맵 적용
	//	충돌맵 적용
	CGameObject* destObj = m_pPlayer;
	int curIdx = CMapMgr::GetInst()->GetCalculMapIdx();
	int mopType = 0;
	int objCnt = -1;
	bool bStopIt = false;
	int againCnt = 0;
	int againMax = 2;

	for (int i = 0; i < m_vecBlackBoard.size(); ++i)
	{
		m_vecBlackBoard[i]->UpdateBlackBoard();
	}

	if (KEY_TAB(KEY_TYPE::KEY_LCTRL))
	{
		for (int j = 0; j < (UINT)EC_MOSTER_TYPE::End; ++j)
		{
			for (int i = 0; i < m_vecReady[j].size(); ++i)
			{
				if (m_vecReady[j][i] != nullptr)
				{
					m_vecReady[j][i]->StatusComponent()->SetFloat(STATUS_FLOAT::HP, -1.0f);
				}
			}
		}
		if (m_pBoss != nullptr)
		{
			m_pBoss->StatusComponent()->SetFloat(STATUS_FLOAT::HP, -1.0f);
		}
	}
	if (KEY_TAB(KEY_TYPE::KEY_9))
	{
		m_eState = EC_MAP_STATE::Cheat;
	}
	v3destPos = playerPos;
	ArrangeMonster();
	PlaySummons();
	if (g_nTowerIn <= 0)
	{
		//TowerIn!!
		m_bPossibleTower = true;
	}

	switch (m_eState)
	{
	case EC_MAP_STATE::Evaluate:
		if (m_vMapRect.x < playerPos.x && playerPos.x < m_vMapRect.y
		&&m_vMapRect.z < playerPos.z && playerPos.z < m_vMapRect.w) {
			m_eState = EC_MAP_STATE::Evaluate;
		}
		else {
			m_eState = EC_MAP_STATE::Stop;
		}
		break;
	case EC_MAP_STATE::Sponning:
		//	여기서 감시할 필요는 없는데 그냥 정확도를 위해서 하자 최적화를 시킨다면 여기를 없애면됨
		if (m_vecRoundInfo[m_nCurRound].bHaveBoss && m_vecRoundInfo[m_nCurRound].pBoss == nullptr)
		{
			Vec3 bossRot = playerRot;
			playerRot.y += XM_PI;
			Vec3 mapPos = Transform()->GetLocalPos();
			m_vecRoundInfo[m_nCurRound].pBoss = CMapMgr::GetInst()->ResponBoss(m_nIdx, m_vecRoundInfo[m_nCurRound].nBossType, mapPos, bossRot);
			m_pBoss = m_vecRoundInfo[m_nCurRound].pBoss;
		}
		if (m_vecRoundInfo[m_nCurRound].nNowCurMonster >= m_vecRoundInfo[m_nCurRound].nNowMaxMonster)
		{
			m_eState = EC_MAP_STATE::Evaluate;
		}
		else {
			switch (m_vecRoundInfo[m_nCurRound].nResponType)
			{
			case (UINT)EC_RESPON_TYPE::SQUENCE:
				if (m_vecRoundInfo[m_nCurRound].fCurSponDelay < m_vecRoundInfo[m_nCurRound].fSponDelay)
				{
					m_vecRoundInfo[m_nCurRound].fCurSponDelay += DT;
					return;
				}
				else 
				{
					m_vecRoundInfo[m_nCurRound].fCurSponDelay = 0.0f;
					if (m_vecRoundInfo[m_nCurRound].itorMonster != m_vecRoundInfo[m_nCurRound].monsterInfo.end())
					{
						EC_MOSTER_TYPE mType;
						tMonsterInfo& m = (*m_vecRoundInfo[m_nCurRound].itorMonster);
						switch ((*m_vecRoundInfo[m_nCurRound].itorMonster).nMonsterType)
						{
						case (UINT)EC_MOSTER_TYPE::Stone:mType = EC_MOSTER_TYPE::Stone; break;
						case (UINT)EC_MOSTER_TYPE::Skelleton: mType = EC_MOSTER_TYPE::Skelleton; break;
						case (UINT)EC_MOSTER_TYPE::Mimic: mType = EC_MOSTER_TYPE::Mimic; break;
						}
						OnSummons(m.nResponerIdx);
						if (SponSequence(mType, m_vecSponner[m.nResponerIdx].vPos, m_vecSponner[m.nResponerIdx].vRot))
						{
							++m_vecRoundInfo[m_nCurRound].itorMonster;
							++m_vecRoundInfo[m_nCurRound].nNowCurMonster;

						}
					}
					else 
					{
						m_eState = EC_MAP_STATE::Evaluate;
					}
				}
				break;
			case (UINT)EC_RESPON_TYPE::ATONCE:
				for(;m_vecRoundInfo[m_nCurRound].itorMonster != m_vecRoundInfo[m_nCurRound].monsterInfo.end();)
					{
						if (m_vecRoundInfo[m_nCurRound].itorMonster != m_vecRoundInfo[m_nCurRound].monsterInfo.end())
						{
							EC_MOSTER_TYPE mType;
							tMonsterInfo& m = (*m_vecRoundInfo[m_nCurRound].itorMonster);
							switch ((*m_vecRoundInfo[m_nCurRound].itorMonster).nMonsterType)
							{
							case (UINT)EC_MOSTER_TYPE::Stone:mType = EC_MOSTER_TYPE::Stone; break;
							case (UINT)EC_MOSTER_TYPE::Skelleton: mType = EC_MOSTER_TYPE::Skelleton; break;
							case (UINT)EC_MOSTER_TYPE::Mimic: mType = EC_MOSTER_TYPE::Mimic; break;
							}
							OnSummons(m.nResponerIdx);
							if (SponAtOnce(mType, m_vecSponner[m.nResponerIdx].vPos, m_vecSponner[m.nResponerIdx].vRot))
							{
								++m_vecRoundInfo[m_nCurRound].itorMonster;
								++m_vecRoundInfo[m_nCurRound].nNowCurMonster;
							}
						}
						else
						{
							m_eState = EC_MAP_STATE::Evaluate;
						}
					}
				break;
			case (UINT)EC_RESPON_TYPE::RANDOM:
				break;
			default:
				break;
			}
		}
		break;
	case EC_MAP_STATE::RoundClear:
		if (m_nIdx != 9)
		{
			++m_nCurRound;
			if (m_nCurRound < m_nMaxRound)
			{
				m_eState = EC_MAP_STATE::Evaluate;
			}
			else
			{
				m_eState = EC_MAP_STATE::LevelClear;
			}
		}
		else
		{
			if (m_fFloorDelayCur < m_fFloorDelayMax)
			{
				m_fFloorDelayCur += DT;
				if (m_pClearUIObj != nullptr && m_pClearUIObj->IsActive() == false)
				{
					m_pClearUIObj->SetActive(true);
				}
			}
			else
			{
				m_fFloorDelayCur = 0.0f;
				m_bFloorOn = true;
				m_pClearUIObj->SetActive(false);
				++m_nCurRound;
			}
		}
		if (m_bFloorOn)
		{
			m_bFloorOn = false;
			if (m_nCurRound < 9)
			{
				if (m_pFloorUI != nullptr)
				{
					Ptr<CTexture> tex = CMapMgr::GetInst()->GetFloorTex(m_nCurRound);
					m_pFloorUI->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, tex.GetPointer());
					m_eState = EC_MAP_STATE::Evaluate;
				}
			}
			else
			{
				if (!m_pClearUIObj->IsActive())
				{
					m_pClearUIObj->SetActive(true);
					Ptr<CTexture> vitory = CResMgr::GetInst()->Load<CTexture>(L"ClearMtrl", L"Texture\\Victory.png");
					m_pClearUIObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, vitory.GetPointer());
				}
			}

		}
		break;
	case EC_MAP_STATE::LevelClear:
		if (g_nTowerIn <= 0) 
		{
			//TowerIn!!
			m_bPossibleTower = true;
			m_eState = EC_MAP_STATE::MapMove;
		}
		else
		{
			--g_nTowerIn;
			m_eState = EC_MAP_STATE::MapMove;
			m_bLevelClear = true;
			//	여기 있는게 outPortal
		}
		break;
	case EC_MAP_STATE::MapMove:
		for (int i = m_nStartPortal; i < 10 - m_nTowerPortal; ++i) {
			if (m_arrPortal[i].IsOn)
			{
				if (m_arrPortal[i].PortalRect.x < playerPos.x && playerPos.x < m_arrPortal[i].PortalRect.y &&
					m_arrPortal[i].PortalRect.z < playerPos.z && playerPos.z < m_arrPortal[i].PortalRect.w)
				{
					//	내가 갈맵의 인덱스에 접근
					
					if (i == 9 && m_arrPortal[9].IsOn)
					{
						if (m_bPossibleTower)
						{
							AllStop();
							m_pPlayer->Transform()->SetLocalPos(CMapMgr::GetInst()->GetInTowerPos());
							m_pPlayer->Transform()->SetLocalRot(CMapMgr::GetInst()->GetInTowerRot());
							m_eState = EC_MAP_STATE::TowerEvent;
							break;
						}
					}
					else if (m_fMapMoveDelayCur > m_fMapMoveDelayMax) {
						tPortal2& portal = CMapMgr::GetInst()->GetMapScript(i)->GetPortal(m_nIdx);
						if (i != 9)
						{
							m_pPlayer->Transform()->SetLocalPos(portal.StartPos);
							m_pPlayer->Transform()->SetLocalRot(portal.StartRot);
							m_eState = EC_MAP_STATE::Stop;
							m_fMapMoveDelayCur = 0.0f;
						}
					}
					else {
						m_fMapMoveDelayCur += DT;
					}
				}
			}
		}
		break;
	case EC_MAP_STATE::Stop:
		if (m_bLevelClear) {
			m_eState = EC_MAP_STATE::MapMove;
		}
		else if (m_vMapRect.x < playerPos.x && playerPos.x < m_vMapRect.y
			&&m_vMapRect.z < playerPos.z && playerPos.z < m_vMapRect.w) {
			m_bStartMap = false;
			m_eState = EC_MAP_STATE::Awake;
		}
		else {
			if (m_bPreStop) { return; }
			else
			{
				m_bPreStop = true;
				for (int i = 0; i < (UINT)EC_MOSTER_TYPE::End; ++i)
				{

					AllStop(i);
				}
				if (m_nForceActive > 0)
				{
					for (int i = 0; i < m_vecEnvir.size(); ++i)
					{
						m_vecEnvir[i]->SetActive(true);
					}
					--m_nForceActive;
				}
			}
		}
		break;
	case EC_MAP_STATE::Awake:
		m_bPreStop = false;
		for (int i = 0; i < (UINT)EC_MOSTER_TYPE::End; ++i) {
			for (vector<CGameObject*>::iterator itor = m_vecReady[i].begin(); itor != m_vecReady[i].end(); ++itor) {
				if ((*itor) == nullptr)continue;
				else if (!(*itor)->IsActive()) {
					(*itor)->SetActive(true);
				}
			}
		}
		m_eState = EC_MAP_STATE::Evaluate;
		break;
	case EC_MAP_STATE::Cheat:
		AllStop();
		m_pPlayer->Transform()->SetLocalPos(CMapMgr::GetInst()->GetInTowerPos());
		m_pPlayer->Transform()->SetLocalRot(CMapMgr::GetInst()->GetInTowerRot());

		m_eState = EC_MAP_STATE::TowerEvent;
		break;
	case EC_MAP_STATE::TowerEvent:
			CMapMgr::GetInst()->StopFiledMap();
			CMapMgr::GetInst()->PlayTowerMap();
			CMapMgr::GetInst()->OffOutTowerObj();
			{
				m_bInTower = true;
				if (m_pFloorUI == nullptr)
				{
					m_bInTower = false;
					tResolution res = CRenderMgr::GetInst()->GetResolution();
					m_pFloorUI = new CGameObject;
					m_pFloorUI->AddComponent(new CTransform);
					m_pFloorUI->AddComponent(new CMeshRender);
					Vec3 uiPos = Vec3((res.fWidth / 2.f) - 20.f - (100.f), (res.fHeight / 2.f) - 20.f - (100.f), 1.f);

					m_pFloorUI->Transform()->SetLocalScale(Vec3(200.f, 200.f, 1.f));
					m_pFloorUI->Transform()->SetLocalPos(uiPos);

					m_pFloorUI->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
					Ptr<CMaterial> mtrl = new CMaterial;
					mtrl->SetName(L"FloorMtrl");
					mtrl->SetShader(CResMgr::GetInst()->FindRes<CShader>(L"TexShader"));
					CResMgr::GetInst()->AddRes(L"FloorMtrl", mtrl);
					mtrl->SetData(SHADER_PARAM::TEX_0, CMapMgr::GetInst()->GetFloorTex(0).GetPointer());

					m_pFloorUI->MeshRender()->SetMaterial(mtrl);

					CMapMgr::GetInst()->GetMapScript(9)->SetFloorUI(m_pFloorUI);
					CSceneMgr::GetInst()->GetCurScene()->AddGameObject(L"UI", m_pFloorUI, false);
				}
				if (m_pClearUIObj == nullptr)
				{
					tResolution res = CRenderMgr::GetInst()->GetResolution();
					m_pClearUIObj = new CGameObject;
					m_pClearUIObj->AddComponent(new CTransform);
					m_pClearUIObj->AddComponent(new CMeshRender);
					Vec3 uiPos = Vec3((res.fWidth / 2.f)-600.f, (res.fHeight / 2.f) - 400.f, 1.f);

					m_pClearUIObj->Transform()->SetLocalScale(Vec3(600.f, 200.f, 1.f));
					m_pClearUIObj->Transform()->SetLocalPos(uiPos);

					m_pClearUIObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
					Ptr<CMaterial> mtrl = new CMaterial;
					mtrl->SetName(L"ClearMtrl");
					mtrl->SetShader(CResMgr::GetInst()->FindRes<CShader>(L"TexShader"));
					CResMgr::GetInst()->AddRes(L"ClearMtrl", mtrl);
					Ptr<CTexture> tex = CResMgr::GetInst()->Load<CTexture>(L"ClearMtrl", L"Texture\\ClearUI.png");
					
					mtrl->SetData(SHADER_PARAM::TEX_0, tex.GetPointer());

					m_pClearUIObj->MeshRender()->SetMaterial(mtrl);
					m_pClearUIObj->SetActive(false);
					CSceneMgr::GetInst()->GetCurScene()->AddGameObject(L"UI", m_pClearUIObj, false);


				}
				{
					Ptr<CSound> filedSound = CResMgr::GetInst()->FindRes<CSound>(L"land02");
					if (filedSound.GetPointer() != nullptr)
					{
						filedSound->Stop();
					}
					Ptr<CSound> towerSound = CResMgr::GetInst()->Load<CSound>(L"InTowerMain", L"Sound\\BGM-2floor-02.mp3");
					towerSound->Play(0, 0.5f, true);

				}
			}
			m_eState = EC_MAP_STATE::Evaluate;
			break;
		case EC_MAP_STATE::BossClear:
			//	다죽이고 레벨클리어
			for (int j = 0; j < (UINT)EC_MOSTER_TYPE::End; ++j)
			{
				for (int i = 0; i < m_vecReady[j].size(); ++i)
				{
					if (m_vecReady[j][i] != nullptr)
					{
						m_vecReady[j][i]->StatusComponent()->SetFloat(STATUS_FLOAT::HP, -1.0f);
					}
				}
			}
			//	바로 레벨클리어?
			if (m_nIdx != 9)
			{
				m_eState = EC_MAP_STATE::LevelClear;

			}
			else
			{
				Ptr<CSound> bossSound = CResMgr::GetInst()->FindRes<CSound>(L"InTowerBoss");
				if (bossSound.GetPointer() == nullptr)
				{
					bossSound = CResMgr::GetInst()->Load<CSound>(L"InTowerBoss", L"Sound\\BGM-3floor-01.mp3");
				}
				bossSound->Play(0, 0.5f, true);
				Ptr<CSound> inTower = CResMgr::GetInst()->FindRes<CSound>(L"InTowerMain");
				inTower->Play(0, 0.5f, true);
				Ptr<CSound> clearSound = CResMgr::GetInst()->FindRes<CSound>(L"clearSound");
				//32 fdf.wav
				if (clearSound.GetPointer() == nullptr)
				{
					clearSound = CResMgr::GetInst()->Load<CSound>(L"clearSound", L"Sound\\32 fdf.wav");
				}
				clearSound->Play(1, 1.f, true);
				m_eState = EC_MAP_STATE::RoundClear;
			}
			break;
	}

	//	-----------------------------------------------------------------------------------------
	//		심사부
	ErrTest();
	if (m_eState != EC_MAP_STATE::Evaluate|| m_bLevelClear)return;
	


	if (m_vecRoundInfo[m_nCurRound].nClearMonsterCnt <= m_vecRoundInfo[m_nCurRound].nCurMonsterCnt) 
	{
		m_eState = EC_MAP_STATE::RoundClear;
		return;
	}
	else
	{
		if (m_vecRoundInfo[m_nCurRound].nNowCurMonster < m_vecRoundInfo[m_nCurRound].nNowMaxMonster)
		{
			m_eState = EC_MAP_STATE::Sponning;
			return;
		}
		else 
		{
			//	스폰 종료
			m_eState = EC_MAP_STATE::Evaluate;
			return;
		}
	}
}


void CMapScript::SetMapIdx(int _idx)
{
	m_nIdx = _idx;
}


void CMapScript::SetRoundInfo(int _idx, int _nClearCnt, EC_RESPON_TYPE _sponType, int _maxSponMonster, float _fSopnDelay,bool _boss, int _bossNum)
{
	m_vecRoundInfo[_idx].nClearMonsterCnt = _nClearCnt;
	m_vecRoundInfo[_idx].nResponType = (UINT)_sponType;
	m_vecRoundInfo[_idx].nNowMaxMonster = _maxSponMonster;
	m_vecRoundInfo[_idx].fSponDelay = _fSopnDelay;
	m_vecRoundInfo[_idx].bHaveBoss = _boss;
	m_vecRoundInfo[_idx].nBossType = _bossNum;
}

void CMapScript::SetRoundMonster(int _roundNum, int _mimic, int _skell, int _stone, bool _start)
{
	int max = _mimic + _skell + _stone;
	m_vecRoundInfo[_roundNum].monsterInfo.resize(max);
	int reNum = m_vecSponner.size();

	int r = 0; 
	for (int i = 0; i < _mimic; ++i) {
		m_vecRoundInfo[_roundNum].monsterInfo[r].bIsRespon = false;
		m_vecRoundInfo[_roundNum].monsterInfo[r].nMonsterType = (UINT)EC_MOSTER_TYPE::Mimic;
		m_vecRoundInfo[_roundNum].monsterInfo[r].nResponerIdx = r % reNum;
		r++;
	}
	for (int i = 0; i < _skell; ++i) {
		m_vecRoundInfo[_roundNum].monsterInfo[r].bIsRespon = false;
		m_vecRoundInfo[_roundNum].monsterInfo[r].nMonsterType = (UINT)EC_MOSTER_TYPE::Skelleton;
		m_vecRoundInfo[_roundNum].monsterInfo[r].nResponerIdx = r % reNum;
		r++;
	}
	for (int i = 0; i < _stone; ++i) {
		m_vecRoundInfo[_roundNum].monsterInfo[r].bIsRespon = false;
		m_vecRoundInfo[_roundNum].monsterInfo[r].nMonsterType = (UINT)EC_MOSTER_TYPE::Stone;
		m_vecRoundInfo[_roundNum].monsterInfo[r].nResponerIdx = r % reNum;
		r++;
	}
//	std::random_shuffle(m_vecRoundInfo[_roundNum].monsterInfo.begin(), m_vecRoundInfo[_roundNum].monsterInfo.end());
	m_vecRoundInfo[_roundNum].itorMonster = m_vecRoundInfo[_roundNum].monsterInfo.begin();
}

void CMapScript::SetBossMonster(int _roundNum, int _BossType, int _bossCnt)
{
	//	끼워넣을 라운드
	//	라운드 정보
}

bool CMapScript::SponStone(const Vec3 & _pos, const Vec3 & _rot)
{
	for (int i = 0; i < m_vecStop[(UINT)EC_MOSTER_TYPE::Stone].size(); ++i) {
		if (m_vecStop[(UINT)EC_MOSTER_TYPE::Stone][i] != nullptr) {
			m_vecStop[(UINT)EC_MOSTER_TYPE::Stone][i]->SetActive(true);
			m_vecStop[(UINT)EC_MOSTER_TYPE::Stone][i]->GetScript<CStoneAI>()->Reset(_pos,_rot);
			return true;
		}
	}
	return false;
}
bool CMapScript::SponSkelleton(const Vec3 & _pos, const Vec3 & _rot)
{
	return false;
}
bool CMapScript::SponMimic(const Vec3 & _pos, const Vec3 & _rot)
{
	return false;
}
void CMapScript::CreateMonster(bool _bIsStop, EC_MOSTER_TYPE _type, const wstring & _wstrLayerName, const wstring & _wstrFirstName, int _nMonsterCnt, CScene * _curScene)
{
	//	
	CGameObject* pObj = nullptr;
	CStatusComponent* pState = nullptr;
	
	CStoneAI* pStone = nullptr;
	CMimicAi* pMimic = nullptr;
	CSkelletonAI* pSkell = nullptr;
	CBulletBossSkillScript* skill1 = nullptr;
	//	0810

	int j = 0;
	int colId = m_bCurColId;
	
	Vec3 vscale(2, 2, 2);
	Vec3 vscale_skell(1.2, 1.2, 1.2);


	Ptr<CMeshData> mData = CMapMgr::GetInst()->GetMonsterMeshData(_type);
	wstring name = _wstrFirstName;
	for (int i = 0; i < _nMonsterCnt; ++i)
	{
		name = _wstrFirstName + std::to_wstring(colId);
		pObj = mData->Instantiate();
		pObj->MeshRender()->SetDynamicShadow(true);
		pObj->AddComponent(new CMonsterScript);
		pObj->SetName(name);
		pObj->FrustumCheck(false);
		pState = new CStatusComponent;
		pObj->AddComponent(pState);
		pState->SetColNumId(colId);
		pState->SetMapNum(m_nIdx);
		pState->SetMonsterType(_type);
		pState->SetHpUI((UINT)MONSTER_TYPE::Mimic);
		pObj->SetOnReady(true);
		pObj->AddComponent(new CCollider2D);
		pObj->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::SPHERE);
		
		switch (_type)
		{
		case EC_MOSTER_TYPE::Stone:
			pObj->Transform()->setPhysics(false);
			pObj->Transform()->SetHasOffset((BYTE)EC_TRANS_OFFSET::POSITION | (BYTE)EC_TRANS_OFFSET::ROTATE);
			pObj->Transform()->SetLocalOffset(Vec3(0.f, XM_ANGLE * 180.f, 0.f), Vec3(0.f, 10.f, 0.f));
			pObj->Transform()->SetLocalScale(Vec3(vscale));
			pObj->Collider2D()->SetOffsetScale(vscale * 2);

			pObj->Collider2D()->SetOffsetPos(Vec3(0.f, 10.f, 0.f));
		//	pObj->Collider2D()->SetOffsetScale(Vec3(2.5f, 2.5f, 2.5f));
			pStone = new CStoneAI;
			pStone->SetPlayer(m_pPlayer);
			pObj->AddComponent(pStone);
			pStone->SetSkill(m_pStonesSkill);
			//	에러나면 여길볼것----------------------------------------------------------------------------------------
			//skill1 = new CBulletBossSkillScript;
			//pStone->SetSkill1(skill1);
			break;
		case EC_MOSTER_TYPE::Skelleton:
			pObj->Transform()->setPhysics(false);
			pObj->Transform()->SetHasOffset((BYTE)EC_TRANS_OFFSET::ROTATE);
			pObj->Transform()->SetLocalOffset(Vec3(0.f, XM_ANGLE * 180.f, 0.f), Vec3::Zero);
			pObj->Transform()->SetLocalScale(Vec3(vscale_skell));
			pObj->Collider2D()->SetOffsetPos(Vec3(0.f, 2.5f, 0.f));
			pObj->Collider2D()->SetOffsetScale(vscale_skell *3.3);
		//	pObj->Collider2D()->SetOffsetScale(Vec3(3.0f, 3.0f, 3.f));
			pSkell = new CSkelletonAI;
			pSkell->SetPlayer(m_pPlayer);
			pObj->AddComponent(pSkell);
			break;
		case EC_MOSTER_TYPE::Mimic:
			//	이거도 바꿔줘야함.
			pObj->Transform()->setPhysics(false);
			pObj->Transform()->SetHasOffset((BYTE)EC_TRANS_OFFSET::ROTATE);
			pObj->Transform()->SetLocalOffset(Vec3(0.f, XM_ANGLE * 180.f, 0.f), Vec3::Zero);

			pObj->Collider2D()->SetOffsetPos(Vec3(0.f, 2.5f, 0.f));
			pObj->Collider2D()->SetOffsetScale(Vec3(5.0f, 5.0f, 5.f));
			pMimic = new CMimicAi;
			pMimic->SetPlayer(m_pPlayer);
			pObj->AddComponent(pMimic);
			break;
		}
		++colId;
		if (_bIsStop)
		{
			for (; j < m_vecStop[(UINT)_type].size(); ++j)
			{
				if (m_vecStop[(UINT)_type][j] == nullptr)
				{
					m_vecStop[(UINT)_type][j] = pObj;
					break;
				}
			}
		}
		else
		{
			for (; j < m_vecReady[(UINT)_type].size(); ++j)
			{
				if (m_vecReady[(UINT)_type][j] == nullptr)
				{
					m_vecReady[(UINT)_type][j] = pObj;
					break;
				}
			}
		}
		CMapMgr::GetInst()->PushObj(_type, pObj);
		_curScene->FindLayer(_wstrLayerName)->AddGameObject(pObj);
	}
	m_bCurColId = colId;
	
}
void CMapScript::SetSponner(int _idx, float _range, const Vec3& _pos, const Vec3& _rot)
{
	if (_idx < 0)return;
	m_vecSponner[_idx].nCurIdx = _idx;
	m_vecSponner[_idx].Range = _range;
	m_vecSponner[_idx].vPos = _pos + Vec3(m_vStartPos.x,0.0f, m_vStartPos.y);
	m_vecSponner[_idx].vRot = _rot;
}
void CMapScript::SetPortal(int _outMapIdx, const Vec2 & _pos, float _wid, float _hei, const Vec3& _sPos, const Vec3& _sRot)
{
	Vec2 pos =  m_vStartPos + _pos;
	m_arrPortal[_outMapIdx].IsOn = true;
	m_arrPortal[_outMapIdx].PortalRect = { pos.x - (_wid*0.5f),pos.x + (_wid*0.5f),pos.y - (_hei*0.5f),pos.y + (_hei*0.5f) };
	m_arrPortal[_outMapIdx].StartPos = _sPos + Vec3(m_vStartPos.x,0.f, m_vStartPos.y) ;
	m_arrPortal[_outMapIdx].StartRot = _sRot;
}
//	Stop -> Ready
void CMapScript::ArrangeToReady(int _mopType)
{
	int r = 0;
	for (int j = 0; j < m_vecStop[_mopType].size(); ++j) 
	{
		if (m_vecStop[_mopType][j] == nullptr)continue;
		if (m_vecStop[_mopType][j]->IsActive()) 
		{
			for (; r < m_vecReady[_mopType].size(); ++r) 
			{
				if (m_vecReady[_mopType][r] == nullptr) 
				{
					m_vecReady[_mopType][r] = m_vecStop[_mopType][j];
					m_vecStop[_mopType][j] = nullptr;
					++r;
					break;
				}
			}
		}
	}
}
//	Ready -> Stop
void CMapScript::ArrangeToStop(int _mopType,bool _start)
{
	int k = 0;
	for (int j = 0; j < m_vecReady[_mopType].size(); ++j)
	{
		if (m_vecReady[_mopType][j] == nullptr)continue;
		if (!m_vecReady[_mopType][j]->IsActive())
		{
			for (; k < m_vecStop[_mopType].size(); ++k)
			{
				if (m_vecStop[_mopType][k] == nullptr)
				{
					m_vecStop[_mopType][k] = m_vecReady[_mopType][j];
					m_vecReady[_mopType][j] = nullptr;
					++k;
					++m_vecRoundInfo[m_nCurRound].nClearMonsterCnt;
					break;
				}
			}
		}
	}
}

void CMapScript::CreateColliderMap(const wstring & _path, const XMINT2 & _colSize, float _scale)
{
	m_vColMapSize = _colSize;//	1200,1200
	m_fColMapScale = _scale;
	wstring filePath = CPathMgr::GetResPath() + _path;
	std::ifstream fin(filePath.c_str(), std::ios::binary);

	int size = m_vColMapSize.x * m_vColMapSize.y;
	m_pColliderMap = new BYTE[size];
	m_pOriginColliderMap = new BYTE[size];
	for (int i = 0; i < size; ++i) {
		m_pColliderMap[i] = BYTE(fin.get());
		m_pOriginColliderMap[i] = m_pColliderMap[i];
	}
	fin.close();
}

void CMapScript::CreateHeightMap(const wstring & _path, const XMINT2 & _heiSize)
{
	m_vHeiMapSize = _heiSize;
	wstring filePath = CPathMgr::GetResPath() + _path;
	std::ifstream fin(filePath.c_str());

	int size = m_vHeiMapSize.x * m_vHeiMapSize.y;
	m_pHeightMap = new float[size];
	for (int i = 0; i < size; ++i)
	{
		fin >> m_pHeightMap[i];
	}
	fin.close();
}

const Vec3 & CMapScript::UnitCollider(BYTE _colID, int _z, int _x, bool & _stop)
{
	static const int nBitCnt = 8;
	//	bits				:		0000'0001	0000'0010	0000'0100	0000'1000	0001'0000	0010'0000	0100'0000	1000'0000
	//	index				:		[	0	]	[	1	]	[	2	]	[	3	]	[	4	]	[	5	]	[	6	]	[	7	]
	//	order				:		<	1	>	<	2	>	<	3	>	<	4	>	<	5	>	<	6	>	<	7	>	<	8	>
	static const BYTE bits[nBitCnt] = { 0x01,		0x02,		0x04,		0x08,		0x10,		0x20,		0x40,		0x80 };
	//	order			idx					angle					plusDir
	//	6	7	8		5	6	7		135		90		45			(-1,1)		(0,1)		(1,1)
	//	4		5		3		4		180				0			(-1,0)		(0,0)		(1,0)
	//	3	2	1		2	1	0		225		270		315			(-1,-1)		(0,-1)		(1,-1)
	static const Vec3 around[nBitCnt] = {
			XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(-45.f * XM_ANGLE))
		,	XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(-90.f * XM_ANGLE))
		,	XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(-135.f * XM_ANGLE))
		,	XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(180.f * XM_ANGLE))
		,	XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(0.f * XM_ANGLE))
		,	XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(135.f * XM_ANGLE))
		,	XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(90.f * XM_ANGLE))
		,	XMVector3TransformNormal(Vec3::Right,XMMatrixRotationY(45.f * XM_ANGLE))
	};
	static const int v = 3;
	static const XMINT2 dir[nBitCnt] = {
			{2,-2}
		,	{0,-3}
		,	{-2,-2}
		,	{-3,0}
		,	{3,0}
		,	{-2,2}
		,	{0,3}
		,	{2,2}
	};
	//	제자리
	static Vec3 tPos;
	static int max = 1200 * 1200;
	tPos = { 0.f,0.f,0.f };


	BYTE cul = 0xff;


	if (_z - 3 < 0) {
		cul &= ~(bits[0] | bits[1] | bits[2]);
	}
	else if (m_vColMapSize.y < _z + 3) {
		cul &= ~(bits[5] | bits[6] | bits[7]);
	}
	if (_x - 3 < 0) {
		cul &= ~(bits[2] | bits[3] | bits[5]);
	}
	else if (_x + 3 > m_vColMapSize.y) {
		cul &= ~(bits[0] | bits[4] | bits[7]);
	}
	int idx = 0;
	BYTE type = 0;
	for (int i = 0; i < nBitCnt; ++i)
	{
		idx = 0;
		if (cul & bits[i]) 
		{
			idx = (((dir[i].y + _z) * m_vColMapSize.x) + (dir[i].x + _x));
			if (max > idx)
			{
				type = m_pColliderMap[idx];
				if (type != (BYTE)EC_COL_TYPE::Path && _colID != type)
				{
					cul &= ~bits[i];
				}
			}
		}
	}

	if (cul == 0xff)
	{
		_stop = true;
		return tPos;
	}

	if (cul & bits[0]) { return around[0]; }
	if (cul & bits[1]) { return around[1]; }
	if (cul & bits[2]) { return around[2]; }
	if (cul & bits[3]) { return around[3]; }
	if (cul & bits[4]) { return around[4]; }
	if (cul & bits[5]) { return around[5]; }
	if (cul & bits[6]) { return around[6]; }
	if (cul & bits[7]) { return around[7]; }
	if (cul & bits[8]) { return around[8]; }

	_stop = false;
	return tPos;
}

void CMapScript::StempingColliderMap(BYTE _colID, int _z, int _x)
{
	int nIdx = (_z * m_vColMapSize.x) + _x;
	int x = 0, z = 0;
	if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
		m_pColliderMap[nIdx] = _colID;
	}
	//	1,+1
	x = 1; z = -1;
	if (-1 < x + _x && x + _x < m_vColMapSize.x && -1 < z + _z && z + _z < m_vColMapSize.y) {
		nIdx += (m_vColMapSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
	x = 0; z = -1;
	if (-1 < x + _x && x + _x < m_vColMapSize.x && -1 < z + _z && z + _z < m_vColMapSize.y) {
		//	-1,+1
		nIdx += (m_vColMapSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
	x = -1; z = -1;
	if (-1 < x + _x && x + _x < m_vColMapSize.x && -1 < z + _z && z + _z < m_vColMapSize.y) {
		//	-1,+1
		nIdx += (m_vColMapSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
	x = 1; z = 0;
	if (-1 < x + _x && x + _x < m_vColMapSize.x && -1 < z + _z && z + _z < m_vColMapSize.y) {
		//	-1,+1
		nIdx += (m_vColMapSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
	x = -1; z = 0;
	if (-1 < x + _x && x + _x < m_vColMapSize.x && -1 < z + _z && z + _z < m_vColMapSize.y) {
		//	-1,+1
		nIdx += (m_vColMapSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
	x = 1; z = 1;
	if (-1 < x + _x && x + _x < m_vColMapSize.x && -1 < z + _z && z + _z < m_vColMapSize.y) {
		//	-1,+1
		nIdx += (m_vColMapSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
	x = 0; z = 1;
	if (-1 < x + _x && x + _x < m_vColMapSize.x && -1 < z + _z && z + _z < m_vColMapSize.y) {
		//	-1,+1
		nIdx += (m_vColMapSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
	x = -1; z = 1;
	if (-1 < x + _x && x + _x < m_vColMapSize.x && -1 < z + _z && z + _z < m_vColMapSize.y) {
		//	-1,+1
		nIdx += (m_vColMapSize.x * z) + x;
		if (m_pColliderMap[nIdx] != (BYTE)EC_COL_TYPE::Blocked && m_pColliderMap[nIdx] != _colID) {
			m_pColliderMap[nIdx] = _colID;
		}
	}
}

void CMapScript::CalculNowMonster(int _idx)
{
	int j = 0;
	for (int i = 0; i < m_vecReady[_idx].size(); ++i)
	{
		if (m_vecReady[_idx][i] != nullptr)
		{
			if (m_vecReady[_idx][i]->StatusComponent()->GetFloat(STATUS_FLOAT::HP) < 0.0001f)
			{
				for (; j < m_vecStop[_idx].size(); ++j)
				{
					if (m_vecStop[_idx][j] == nullptr)
					{
						m_vecReady[_idx][i]->SetActive(false);
						m_vecStop[_idx][j] = m_vecReady[_idx][i];
						m_vecReady[_idx][i] = nullptr;
						++j;
						++m_vecRoundInfo[m_nCurRound].nClearMonsterCnt;
						break;
					}
				}
			}
		}
	}
}
bool CMapScript::SponSequence(EC_MOSTER_TYPE _type, const Vec3 & _pos, const Vec3 & _rot)
{
	for (int i = 0; i < m_vecStop[(UINT)_type].size(); ++i)
	{
		if (m_vecStop[(UINT)_type][i] != nullptr)
		{
			for (int j = 0; j < m_vecReady[(UINT)_type].size(); ++j)
			{
				if (m_vecReady[(UINT)_type][j] == nullptr)
				{
					m_vecStop[(UINT)_type][i]->StatusComponent()->SetMapNum(CMapMgr::GetInst()->GetCurMapIdx());
					m_vecStop[(UINT)_type][i]->setPhysics(false);
					switch (_type)
					{
					case EC_MOSTER_TYPE::Stone:
						m_vecStop[(UINT)_type][i]->GetScript<CStoneAI>()->Reset(_pos, _rot);
						break;
					case EC_MOSTER_TYPE::Skelleton:
						m_vecStop[(UINT)_type][i]->GetScript<CSkelletonAI>()->Reset(_pos, _rot);
						break;
					case EC_MOSTER_TYPE::Mimic:
						m_vecStop[(UINT)_type][i]->GetScript<CMimicAi>()->Reset(_pos, _rot);
						break;
					}
					m_vecReady[(UINT)_type][j] = m_vecStop[(UINT)_type][i];
					m_vecReady[(UINT)_type][j]->SetActive(true);
					m_vecStop[(UINT)_type][i] = nullptr;
					return true;
				}
			}
		}
	}
	return false;
}

bool CMapScript::SponAtOnce(EC_MOSTER_TYPE _type, const Vec3 & _pos, const Vec3 & _rot)
{
	for (int i = 0; i < m_vecStop[(UINT)_type].size(); ++i)
	{
		if (m_vecStop[(UINT)_type][i] != nullptr)
		{
			for (int j = 0; j < m_vecReady[(UINT)_type].size(); ++j)
			{
				if (m_vecReady[(UINT)_type][j] == nullptr)
				{
					m_vecStop[(UINT)_type][i]->StatusComponent()->SetMapNum(CMapMgr::GetInst()->GetCurMapIdx());
					m_vecStop[(UINT)_type][i]->setPhysics(false);
					switch (_type)
					{
					case EC_MOSTER_TYPE::Stone:
						m_vecStop[(UINT)_type][i]->GetScript<CStoneAI>()->Reset(_pos, _rot);
						break;
					case EC_MOSTER_TYPE::Skelleton:
						m_vecStop[(UINT)_type][i]->GetScript<CSkelletonAI>()->Reset(_pos, _rot);
						break;
					case EC_MOSTER_TYPE::Mimic:
						m_vecStop[(UINT)_type][i]->GetScript<CMimicAi>()->Reset(_pos, _rot);
						break;
					}
					m_vecReady[(UINT)_type][j] = m_vecStop[(UINT)_type][i];
					m_vecReady[(UINT)_type][j]->SetActive(true);
					m_vecStop[(UINT)_type][i] = nullptr;
					return true;
				}
			}
		}
	}
	return false;
}

void CMapScript::ArrangeMonster()
{
	for (int k = 0; k < (UINT)EC_MOSTER_TYPE::End; ++k)
	{
		int j = 0;
		for (int i = 0; i < m_vecReady[k].size(); ++i)
		{
			if (m_vecReady[k][i] != nullptr)
			{
				if (m_vecReady[k][i]->StatusComponent()->GetFloat(STATUS_FLOAT::HP) < 0.0001f)
				{
					for (; j < m_vecStop[k].size(); ++j)
					{
						if (m_vecStop[k][j] == nullptr)
						{
							m_vecReady[k][i]->StatusComponent()->LowHp();
							//m_vecReady[k][i]->SetActive(false);
							m_vecStop[k][j] = m_vecReady[k][i];
							m_vecReady[k][i] = nullptr;
							++j;
							++m_vecRoundInfo[m_nCurRound].nCurMonsterCnt;
							break;
						}
					}
				}
			}
		}
	}
	if (m_nCurRound < m_vecRoundInfo.size() && m_vecRoundInfo[m_nCurRound].bHaveBoss && m_vecRoundInfo[m_nCurRound].pBoss != nullptr)
	{
		if (m_vecRoundInfo[m_nCurRound].pBoss->StatusComponent()->GetFloat(STATUS_FLOAT::HP) < 0.0001f)
		{
			m_vecRoundInfo[m_nCurRound].pBoss->StatusComponent()->LowHp();
			m_vecRoundInfo[m_nCurRound].pBoss = nullptr;
			m_bBossClear = true;
			m_eState = EC_MAP_STATE::BossClear;
		}
	}
	
}

void CMapScript::AllStop()
{
	for (int _idx = 0; _idx < (UINT)EC_MOSTER_TYPE::End; ++_idx)
	{
		for (int i = 0; i < m_vecReady[_idx].size(); ++i)
		{
			if (m_vecReady[_idx][i] != nullptr)
			{
				m_vecReady[_idx][i]->SetActive(false);
			}
		}
		for (int i = 0; i < m_vecStop[_idx].size(); ++i)
		{
			if (m_vecStop[_idx][i] != nullptr)
			{
				m_vecStop[_idx][i]->SetActive(false);
			}
		}
	}
}

void CMapScript::AllStop(int _idx)
{
	for (int i = 0; i < m_vecReady[_idx].size(); ++i)
	{
		if (m_vecReady[_idx][i] != nullptr)
		{
			m_vecReady[_idx][i]->SetActive(false);
		}
	}
	for (int i = 0; i < m_vecStop[_idx].size(); ++i)
	{
		if (m_vecStop[_idx][i] != nullptr)
		{
			m_vecStop[_idx][i]->SetActive(false);
		}
	}
}

void CMapScript::Transfer(bool _false)
{
	vector<CGameObject*>::iterator itor;
	vector<CGameObject*>::iterator end;
	vector<CGameObject*>* vec;
	for (int i = 0; i < (UINT)EC_MOSTER_TYPE::End; ++i)
	{
		vec = CMapMgr::GetInst()->GetTransferMonster(i);
		itor = vec->begin();
		end = vec->end();
		for (int j = 0; j < m_vecStop[i].size(); ++j)
		{
			if (itor == end)break;
			m_vecStop[i][j] = (*itor);
			m_vecStop[i][j]->SetActive(false);
			++itor;
		}
	}
}

float CMapScript::GetHeight( const Vec3 & _pos)
{
	const int MaxIdx = 400 * 400;
	if (m_pHeightMap == nullptr)return 0.f;
	static Vec3 v3destPosText,v3Deci;
	static int nIdx;
	static float fA, fB, fC, fD,uH,uV,fHeight;
	
	v3destPosText = _pos - Vec3(m_vStartPos.x, 0.0f, m_vStartPos.y);
	v3Deci = { v3destPosText.x - int(v3destPosText.x),0.0f,v3destPosText.z - int(v3destPosText.z) };
	nIdx = (m_vHeiMapSize.x * int(v3destPosText.z)) + int(v3destPosText.x);

	if (int(v3destPosText.x) + 1 >= m_vHeiMapSize.x || int(v3destPosText.z) + 1 >= m_vHeiMapSize.y)return 0.0f;
	if (nIdx >= MaxIdx || nIdx < 0)return 0.0f;


	fA = m_pHeightMap[nIdx];
	fB = m_pHeightMap[nIdx + 1];
	fC = m_pHeightMap[nIdx + m_vHeiMapSize.x];
	fD = m_pHeightMap[nIdx + m_vHeiMapSize.x + 1];


	//if (v3destPosText.x >= v3destPosText.z)
	//{
	//	//	아래쪽 삼각형
	//	uH = (fA - fB);
	//	uV = (fD - fB);
	//	fHeight = fA + Linear(0.f, uH, 1.f - v3Deci.x) + Linear(0.f, uV, v3Deci.z);
	//}
	//else
	//{
	//	//	위쪽 삼각형
	//	uH = (fD - fC);
	//	uV = (fA - fC);
	//	fHeight = fA + Linear(0.f, uH, v3Deci.x) + Linear(0.f, uV, 1.f - v3Deci.z);
	//}
	
	fHeight = (fA * (1.f - v3Deci.x)*(1.f - v3Deci.z) + fB * v3Deci.x*(1.f - v3Deci.z) + fC * (1.f - v3Deci.x)*v3Deci.z + fD * v3Deci.x*v3Deci.z);
	return fHeight;
}

void CMapScript::CreateTree(const wstring & _path, const wstring & _wstrLayerName, CScene * _curScene)
{
	static Ptr<CMeshData> pTree[6];
	static float angle = 0.0f;
	wstring path = CPathMgr::GetResPath() + _path;
	std::wifstream in(path);
	int cnt = 0;
	
	Ptr<CMeshData> mData;
	int typeIdx = 0;
	Vec3 pos;
	Vec3 rot;
	Vec3 scale = Vec3(3.5f,4.f,3.5f);
	float colScale = 0.0f;
	CGameObject* pObj = nullptr;
	vector< tEnvir> vec;
	tEnvir t;
	Ptr<CMeshData> pData;
	float oper = 1.0f;
	static bool bOper = true;

	while (in >> t.A_name >> t.B_type >> t.C_x >> t.D_z >> t.E_y >> t.F_scale)
	{
		vec.push_back(t);
	//	std::wcout << t.A_name << " " << t.B_type << " " << t.C_x << " " << t.D_z << " " << t.E_y << " " << t.F_scale << std::endl;
	}
	for (int i = 0; i < vec.size(); ++i)
	{
		scale = Vec3(3.f, 3.5f, 3.f);
		typeIdx = GetTreeType(vec[i].B_type);
		
		//	CalculPosRotScale
		pos.x = std::stof(vec[i].C_x) + m_vStartPos.x;
		pos.y = 0.0f;
		pos.z = std::stof(vec[i].D_z) + m_vStartPos.y;
		pos.y = CMapMgr::GetInst()->GetHeight(m_nIdx, pos);
		rot.y = float(std::rand() % 360) * XM_ANGLE;
		colScale = std::stof(t.F_scale);

		if (bOper) 
		{ 
			oper = 1.0f; 
			bOper = false;
		}
		else 
		{ 
			oper = -1.f; 
			bOper = true;
		}

		scale.y += float(rand() % 5) * oper * 0.4287792f;

	//	std::wcout << vec[i].A_name << " " << typeIdx << " " << pos.x << " " << pos.y << " " << pos.z << " " << vec[i].F_scale << std::endl;
		pData = CMapMgr::GetInst()->GetTreeData(typeIdx);
		pObj = pData->Instantiate();
		colScale = 3.4f;
		pObj->SetName(vec[i].A_name);
		pObj->FrustumCheck(false);
		pObj->Transform()->SetLocalPos(pos);
		pObj->Transform()->SetLocalRot(rot);
		pObj->MeshRender()->SetDynamicShadow(true);
		
		pObj->Transform()->SetLocalScale(scale);
		//pObj->AddComponent(new CStatusComponent);
		//pObj->StatusComponent()->SetMapNum(m_nIdx);
		pObj->AddComponent(new CCollider2D);
		pObj->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::SPHERE);
		CEnviromentObj* e = new CEnviromentObj;
		pObj->AddComponent(e);
		pObj->Collider2D()->SetOffsetScale(Vec3(colScale, colScale, colScale));
		m_vecEnvir.push_back(pObj);

		_curScene->AddGameObject(_wstrLayerName, pObj, false);
		pObj = nullptr;
	}
	in.close();
}

void CMapScript::CreateStone(const wstring & _path, const wstring & _wstrLayerName)
{

	static Ptr<CMeshData> pStone[6];
	static float angle = 0.0f;
	wstring path = CPathMgr::GetResPath() + _path;
	std::wifstream in(path);
	int cnt = 0;

	Ptr<CMeshData> mData;
	int typeIdx = 0;
	Vec3 pos;
	Vec3 rot;
	Vec3 scale = Vec3(1.5f, 1.5f, 1.5f);
	float colScale = 0.0f;
	CGameObject* pObj = nullptr;
	vector< tEnvir> vec;
	tEnvir t;
	Ptr<CMeshData> pData;


	while (in >> t.A_name >> t.B_type >> t.C_x >> t.D_z >> t.E_y >> t.F_scale)
	{
		vec.push_back(t);
		//	std::wcout << t.A_name << " " << t.B_type << " " << t.C_x << " " << t.D_z << " " << t.E_y << " " << t.F_scale << std::endl;
	}
	for (int i = 0; i < vec.size(); ++i)
	{
		typeIdx = GetStoneType(vec[i].B_type);

		//	CalculPosRotScale
		pos.x = std::stof(vec[i].C_x) + m_vStartPos.x;
		pos.y = 0.0f;
		pos.z = std::stof(vec[i].D_z) + m_vStartPos.y;
		pos.y = CMapMgr::GetInst()->GetHeight(m_nIdx, pos);

		rot.y = float(std::rand() % 360) * XM_ANGLE;
		colScale = std::stof(t.F_scale);

		scale = Vec3(1.5f, 1.5f, 1.5f);
		scale.x += float(rand() % 5) * 0.4233017f;
		scale.y += float(rand() % 5) * 0.4233017f;
		scale.z += float(rand() % 5) * 0.4233017f;


		//	std::wcout << vec[i].A_name << " " << typeIdx << " " << pos.x << " " << pos.y << " " << pos.z << " " << vec[i].F_scale << std::endl;
		pData = CMapMgr::GetInst()->GetStoneData(typeIdx);
		pObj = pData->Instantiate();
		colScale = 5.f;
		//if (typeIdx == 5)
		//{
		//	pObj->Transform()->SetHasOffset(BYTE(EC_TRANS_OFFSET::POSITION));
		//	pObj->Transform()->SetLocalOffset(Vec3::Zero, Vec3(0.f, 70.f, 0.f));
		//}
	
		pObj->SetName(vec[i].A_name);
		pObj->FrustumCheck(false);
		pObj->Transform()->SetLocalPos(pos);
		pObj->Transform()->SetLocalRot(rot);
		pObj->Transform()->SetLocalScale(scale);
		//pObj->AddComponent(new CStatusComponent);
		//pObj->StatusComponent()->SetMapNum(m_nIdx);
		pObj->MeshRender()->SetDynamicShadow(true);
		pObj->AddComponent(new CCollider2D);
		pObj->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::SPHERE);
		CEnviromentObj* e = new CEnviromentObj;
		pObj->AddComponent(e);
		pObj->Collider2D()->SetOffsetScale(scale);
		m_vecEnvir.push_back(pObj);
		
		CSceneMgr::GetInst()->GetCurScene()->AddGameObject(_wstrLayerName, pObj, false);
		pObj = nullptr;
	}
	in.close();

}

void CMapScript::PushBackEnviroment(CGameObject * _pObj)
{
	Vec3 dest = _pObj->Transform()->GetLocalPos();
	Vec3 me;
	 for (int i = 0; i < m_vecEnvir.size(); ++i)
	 {
		 me = m_vecEnvir[i]->Transform()->GetLocalPos();
		 if (Vec3::Distance(me, dest) <= 3.0f)
		 {
			 _pObj->PushNear(m_vecEnvir[i]);
			 m_vecEnvir[i]->PushNear(_pObj);
		 }
	 }
	 m_vecEnvir.push_back(_pObj); 
}

void CMapScript::SetStoneSkill(CScene * _pScene, const wstring & _layer)
{
	m_pStonesSkill = new CSkillStoneShower;
	m_pStonesSkill->Init(_pScene, _layer);
}

void CMapScript::ErrTest()
{
	for (int i = 0; i < (UINT)EC_MOSTER_TYPE::End; ++i)
	{
		for (int j = 0; j < m_vecReady[i].size(); ++j)
		{
			if (m_vecReady[i][j] != nullptr)
			{
				if (m_vecReady[i][j]->StatusComponent()->GetFloat(STATUS_FLOAT::HP) > 0.0001f)
				{
					if (!m_vecReady[i][j]->IsActive())
					{
						
						m_vecReady[i][j]->SetActive(true);
					}
				}
			}
		}
	}
}

void CMapScript::OnSummons(int _idx)
{
	if (m_bSummonsOn[_idx])
	{
		m_fSummonsCur[_idx] = 0.0f;
	}
	else
	{
		m_bSummonsOn[_idx] = true;
		m_fSummonsCur[_idx] = 0.0f;
	}
}

void CMapScript::PlaySummons(int _idx)
{
	float fDeltaTime = DT;
	for (int i = 0; i < m_vecSponner.size(); ++i)
	{
		if (m_bSummonsOn[i])
		{
			if (m_pSummonObj[i] != nullptr )
			{
				if (m_pSummonObj[i]->IsActive())
				{
					if (m_fSummonsMax[i] > m_fSummonsCur[i])
					{
						m_fSummonsCur[i] += fDeltaTime;
					}
					else
					{
						m_fSummonsCur[i] = 0.0f;
						m_bSummonsOn[i] = false;
						m_pSummonObj[i]->GetScript< CSkillEvent>()->SetAlive(false);
						m_pSummonObj[i]->SetActive(false);
					}
				}
				else
				{
					m_pSummonObj[i]->GetScript< CSkillEvent>()->SetAlive(true);
					m_pSummonObj[i]->GetScript< CSkillEvent>()->SetStart(true);
					m_pSummonObj[i]->SetActive(true);
				}
			}
			else
			{
				Ptr<CMaterial> pMtr = CResMgr::GetInst()->FindRes<CMaterial>(L"PortalMtrl");
				Ptr<CTexture> pTex = CMapMgr::GetInst()->GetMonsterSkillEffectTexture(1);
				if (pMtr.GetPointer() == nullptr)
				{
					pMtr = new CMaterial;
					pMtr->DisableFileSave();
					pMtr->SetShader(CResMgr::GetInst()->FindRes<CShader>(L"TexShader"));
					CResMgr::GetInst()->AddRes(L"PortalMtrl", pMtr);
					pMtr->SetData(SHADER_PARAM::TEX_0, pTex.GetPointer());
				
				}
				//	포탈 이펙트 생성
				CGameObject* pObj = new CGameObject;
				pObj->FrustumCheck(false);
				pObj->AddComponent(new CMeshRender);
				pObj->AddComponent(new CTransform);
				Vec3 pos = m_vecSponner[i].vPos;
				pos.y = 10.f;
				pObj->Transform()->SetLocalPos(pos);
				//Vec3 rot = m_vecSponner[i].vRot;
				Vec3 scale = Vec3(2.0f, 2.0f, 2.0f);
				Vec3 rot = m_vecSponner[i].vRot;
				pObj->Transform()->SetLocalRot(rot);
				pObj->Transform()->SetLocalScale(scale);
				pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
				pObj->MeshRender()->SetMaterial(pMtr);

				m_pSummonObj[i] = pObj;
				CSkillEvent* skill = new CSkillEvent;
				pObj->AddComponent(skill);
				tSkillEventValue t = {};
				t.a_vStartPos = pos;
				t.a_vStratRot = rot;
				t.b_fScaleAcc = 0.0625f;
				t.b_vScaleMax = Vec3(20.f, 20.f, 20.f);
				t.b_vScaleCur = scale;
				t.b_vScaleMin = scale;
				t.b_vScaleInit = Vec3(1.f, 1.f, 1.f);
				t.d_vRotSpeed = Vec3(0.f, 0.f, 1.5f);

				skill->SetTypes
				(
					(UINT)EC_SKILL_EVENT::ActionType_Spread
					| (UINT)EC_SKILL_EVENT::ActionType_Rotate
					| (UINT)EC_SKILL_EVENT::ActionType_Option_BillBoard
				);
				skill->SetValues(t);
				skill->SetAlive(true);
				skill->SetStart(true);

				CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"Default")->AddGameObject(pObj);
			}
		}
	}
}

void CMapScript::SetBlackBoard(BT::Tool::CBlackBoard * _black)
{
	//	0 : mimic
	//	1 : skell
	//	2 : Stone

	m_vecBlackBoard.push_back(_black);
}

bool CMapScript::SponMonster(int monsterType, const Vec3 & _pos, const Vec3 & _rot)
{
	


	return false;
}

void CMapScript::CreateMonster(int _type, const wstring & _layerName, const wstring & _firstName, int _mopCnt)
{
	CGameObject* pObj = nullptr;
	CStatusComponent* pState = nullptr;





}
