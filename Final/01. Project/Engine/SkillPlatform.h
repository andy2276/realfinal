#pragma once
#include "Script.h"



class CSkillElementDir;
class CSkillPlatform :
	public CScript
{
private:
	vector<CGameObject*>	m_vecSkillObj;
	vector<CSkillElementDir*>	m_vecBulletScript;
	tSkillArch				m_tSkillInfo;
	tSkillEventValue		m_tSkillEventInfo[5];

public:
	CSkillPlatform();
	virtual ~CSkillPlatform();

	void InitSkillBullet(const tSkillArch& _arch);
	void InitSkillBullet(
		int A_nBulletObjCnt,
	int B_nShootBulletCnt,
	bool C_bBulletMesh,
	wstring Ca_wstrMeshPath,
	bool D_bMapMgrMesh,
	int Da_nMapMgrMeshIdx,
	tSkillElementValue::tPosValues _posValue,
	tSkillElementValue::tEndtypes _endType,
	tSkillElementValue::tMoveValues _movValue,
	tSkillElementValue::tSinCosAngles _sinCos,
	tSkillElementValue::tEventTimes _eventTime,
	tSkillElementValue::tRepeateValues _respon,
	tSkillElementValue::tOthers _other,
	UINT F_nTypes,
	Vec3 G_vOffsetScale,
	wstring H_wstrLayer
	);

	void InitSkillEvent(int _idx, tSkillEventValue& _event);
	void InitSkillEvent
	(
				int					_idx,
		const	Vec3&				a_vStartPos,
		const	Vec3&				a_vStratRot,
		const	Vec3&				b_vScaleMax,
		const	Vec3&				b_vScaleCur,
		const	Vec3&				b_vScaleMin,
		const	Vec3&				b_vScaleInit,
				float				b_fScaleAcc,
				CGameObject*		c_pTarget,
		const	float				c_fDelayDist,
		const	Vec3&				d_vRotSpeed,
				int					eA_nTexIdx,
				UINT				eB_nType,
		const	Vec3&				eC_vInitPos,
		const	Vec3&				eD_vInitRot,
		const	Vec3&				eE_vInitRotSpeed
	);

	void CreateSkillBullet(int _cnt = -1);
	void CreateSkillEvent(int _timing, tSkillEventValue& _tValue); 

	void Shoot(const Vec3& _pos, const Vec3 _rot, const Vec3& _dir,int _option = -1);

	virtual void update();

	
	
	CLONE(CSkillPlatform);


};

