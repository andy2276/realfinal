#include "stdafx.h"
#include "StartMapScript.h"
#include "Animator3D.h"
#include "MapMgr.h"

CStartMapScript::CStartMapScript()
	: CScript((UINT)SCRIPT_TYPE::RESPON)
	, m_pPlayer(nullptr)
	, m_vPortalRect(-24.f,24.f,20.f,100.f)
	, m_vRoomRect(-100.f,100.f,-45.f,45.f)
	, m_IsStart(false)
	, m_bIsIng(false)
	, m_bIngDone(false)
	, m_eState(EC_START_MAP_STATE::Evaluate)
	, m_fMoveTimeMax(0.02f)
	, m_fMoveTimeCur(0.02f)
	, m_fOpenTimeCur(88.f * 0.0333f)
	, m_fOpenTimeMax(88.f * 0.0333f)
	, m_bOnFirst(true)
{
	
}


CStartMapScript::~CStartMapScript()
{
}

void CStartMapScript::update()
{
	if (m_bOnFirst) 
	{
		CMapMgr::GetInst()->StopTowerMap();
		m_bOnFirst = false;
	}
	if (m_IsStart)return;
	Vec3 playerPos = m_pPlayer->Transform()->GetLocalPos();
	
	if (m_vRoomRect.x < playerPos.x && playerPos.x < m_vRoomRect.y &&
		m_vRoomRect.z < playerPos.z && playerPos.z < m_vRoomRect.w)
	{
		if (m_vPortalRect.x < playerPos.x && playerPos.x < m_vPortalRect.y)
		{
			if (m_bIngDone) {
				if (m_vPortalRect.z < playerPos.z) {
					
					if (m_fMoveTimeCur > 0.0f)
					{
						m_fMoveTimeCur -= DT;
					}
					else {
						//	move	여기를 수정해야함!
						m_pPlayer->Transform()->SetLocalPos(Vec3(0.f, 0.f, 280.f));
						CMapMgr::GetInst()->ReUpdate();
						m_IsStart = true;
						GetObj()->SetActive(false);
						m_fMoveTimeCur = m_fMoveTimeMax;
					}
				}
			}
			else if (m_bIsIng)
			{
				if (m_fOpenTimeCur > 0.0f)
				{
					m_fOpenTimeCur -= DT;
				}
				else {
					m_fOpenTimeCur = m_fOpenTimeMax;
					m_bIngDone = true;
					Animator3D()->ChangeAnimation(2);
				}
			}
			else 
			{
				if (m_vPortalRect.z < playerPos.z) {
					Animator3D()->ChangeAnimation(1);
					m_bIsIng = true;
				}
				
			}
		}
	}
	else 
	{
		Vec3 mePos = GetObj()->Transform()->GetLocalPos();
		Vec3 pos = m_pPlayer->Transform()->GetLocalPos();
		Vec3 movDir = (mePos - pos).Normalize();
		
		m_pPlayer->Transform()->SetLocalPos(pos +(movDir * 10.f));
	}


}

void CStartMapScript::Reset()
{
}
