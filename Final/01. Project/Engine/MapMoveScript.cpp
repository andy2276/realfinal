#include "stdafx.h"
#include "MapMoveScript.h"
#include "MapMgr.h"


CMapMoveScript::CMapMoveScript() :CScript((UINT)SCRIPT_TYPE::MAPMOVE), m_bEventEnd(false)
{
}


CMapMoveScript::~CMapMoveScript()
{
}

void CMapMoveScript::update()
{
	return;
}

void CMapMoveScript::OnCollisionEnter(CCollider2D * _pOther)
{
	if (m_bEventEnd) {
		CMapMgr::GetInst()->MoveMap();
	}
}

void CMapMoveScript::OnCollisionExit(CCollider2D * _pOther)
{
}

