#include "stdafx.h"
#include "ToolCamScript.h"

#include "Camera.h"

CToolCamScript::CToolCamScript()
	: CScript(0)
	, m_fSpeed(100.f)
	, m_fScaleSpeed(1.f)
{
}

CToolCamScript::~CToolCamScript()
{
}

void CToolCamScript::update()
{
	Vec3 vPos = Transform()->GetLocalPos();
	float fScale = Camera()->GetScale();
	float fSpeed = m_fSpeed;

	if (KEY_HOLD(KEY_TYPE::KEY_LSHIFT))
	{
		fSpeed *= 5.f;
	}

	if (KEY_HOLD(KEY_TYPE::KEY_W))
	{
		Vec3 vFront = Transform()->GetWorldDir(DIR_TYPE::FRONT);
		vPos += vFront * m_fSpeed * DT;
	}

	if (KEY_HOLD(KEY_TYPE::KEY_S))
	{
		Vec3 vBack = -Transform()->GetWorldDir(DIR_TYPE::FRONT);
		vPos += vBack * m_fSpeed* DT;
	}

	if (KEY_HOLD(KEY_TYPE::KEY_A))
	{
		Vec3 vLeft = -Transform()->GetWorldDir(DIR_TYPE::RIGHT);
		vPos += vLeft * m_fSpeed* DT;
	}

	if (KEY_HOLD(KEY_TYPE::KEY_D))
	{
		Vec3 vRight = Transform()->GetWorldDir(DIR_TYPE::RIGHT);
		vPos += vRight * m_fSpeed* DT;
	}

	if (KEY_TAB(KEY_TYPE::KEY_Q))
	{
		m_fSpeed -= 10.f*DT;
		if (m_fSpeed < 0.f)
		{
			m_fSpeed = 0.25f;
		}
	}

	if (KEY_TAB(KEY_TYPE::KEY_E))
	{
		m_fSpeed += 10.f*DT;
		if (m_fSpeed > 20.f)
		{
			m_fSpeed = 19.75f;
		}
	}

	if (KEY_HOLD(KEY_TYPE::KEY_RBTN))
	{
		Vec2 vDrag = CKeyMgr::GetInst()->GetDragDir();
		Vec3 vRot = Transform()->GetLocalRot();

		vRot.x -= vDrag.y * DT;
		vRot.y += vDrag.x * DT;

		Transform()->SetLocalRot(vRot);
	}
	Vec3 vRot = Transform()->GetLocalRot();
	vRot;
	Vec3 vLookDirz = Transform()->GetLocalDir(DIR_TYPE::FRONT);
	vLookDirz;
	vLookDirz += Vec3::Zero;
	Transform()->SetLocalPos(vPos);
}
