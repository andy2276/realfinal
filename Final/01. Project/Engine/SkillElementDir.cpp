#include "stdafx.h"
#include "SkillElementDir.h"
#include "StatusComponent.h"
#include "ResMgr.h"
#include "MapMgr.h"
#include "SkillEvent.h"
CSkillElementDir::CSkillElementDir()
	:CScript((UINT)SCRIPT_TYPE::MONSTERSKILL)
	, m_bMovePossible(true)
	, m_bAlive(false)
	, m_fTic(0.0f)
	, m_fTicMax(0.016f)
	, m_fToc(0.0f)
	, m_nRepeateMax(0)
	, m_nRepeateCur(0)
	, m_bCollider(false)
	, m_fSinCosInitAngle(0.f)
	, m_fSinCosCurAngle(0.f)
	, m_fSinCosAmplitude(1.f)
	, m_fSinCosSize(1.f)
	, m_bStart(true)
	, m_pStartEvent(nullptr)
	, m_pIngEvent(nullptr)
	, m_pEndEvent(nullptr)
{
}


CSkillElementDir::~CSkillElementDir()
{
}

void CSkillElementDir::InitShoot(const Vec3 & _pos, const Vec3 & _rot)
{

	Transform()->SetLocalPos(_pos + m_vOffsetPos);
	Transform()->SetLocalRot(_rot);
	SetStartPos(_pos + m_vOffsetPos);
	RandShootValue();
}

void CSkillElementDir::RandShootValue()
{
	static bool randOper = true;
	static int randPosOrder = 0;
	static int randAddOper = 0;
	static int randHeight = 0;
	static Vec3 dir;
	float xOper = 0.f;
	
	float zOper = 0.f;

	randPosOrder = rand() % 360;
	randAddOper = rand() % 32;
	randHeight = rand() % 78;
	if (m_tSkillElementValue.PosValues.bRandom)
	{
		if (randOper)
		{
			randOper = false;
			xOper = float(randAddOper);
			zOper = float(randPosOrder) * 0.12112356f;
			dir.x = cos(float(randPosOrder)) * xOper;
			dir.z = sin(float(randPosOrder)) * zOper;
			dir.y = sin(float(randHeight)) * xOper * zOper * 0.0412877f;
			m_vStartPos += dir;
		}
		else
		{
			randOper = true;
			xOper = float(randAddOper);
			zOper = float(randPosOrder) * 0.12112356f;
			dir.x = cos(float(randPosOrder)) * zOper;
			dir.z = sin(float(randPosOrder)) * xOper;
			dir.y = cos(float(randHeight)) * xOper * zOper * 0.0412877f;
			m_vStartPos += dir;
		}
		
	}
	else
	{
		if (m_tSkillElementValue.PosValues.bSequence)
		{

		}
	}


	if (m_tSkillElementValue.MoveValues.bRandom)
	{
		if (randOper)
		{
			randOper = true;
			m_fMoveSpeed += float(randHeight) * 0.42783f;

		}
		else
		{
			randOper = false;
			m_fMoveSpeed -= float(randHeight) * 0.02783f;
		}
		
		
	}
	else
	{
		if (m_tSkillElementValue.MoveValues.bSequence)
		{

		}
	}




}

void CSkillElementDir::SetTypes(UINT _type)
{
	m_nType = _type;
	if ((UINT)EC_SKILL_ELEMENT::EndType_Pos & m_nType)
	{
		
	}
	if ((UINT)EC_SKILL_ELEMENT::EndType_Time & m_nType)
	{
		m_fCurTime = 0.0f;
	}
	if ((UINT)EC_SKILL_ELEMENT::EndType_Dist & m_nType)
	{

	}
	if ((UINT)EC_SKILL_ELEMENT::EndType_Col & m_nType)
	{

	}
	if ((UINT)EC_SKILL_ELEMENT::MoveType_Straight & m_nType)
	{

	}
	if ((UINT)EC_SKILL_ELEMENT::MoveType_SinCosLook & m_nType)
	{

	}
	if ((UINT)EC_SKILL_ELEMENT::MoveType_SinCosUp & m_nType)
	{

	}
	if ((UINT)EC_SKILL_ELEMENT::LifeType_Repeate & m_nType)
	{

	}
	if ((UINT)EC_SKILL_ELEMENT::EventType_StartEffect & m_nType)
	{
		m_fStartEventCur = 0.0f;
		
	}
	if ((UINT)EC_SKILL_ELEMENT::EventType_IngEffect & m_nType)
	{

	}
	if ((UINT)EC_SKILL_ELEMENT::EventType_EndEffect & m_nType)
	{
		m_fEndEventCur = 0.0f;
	}
	if ((UINT)EC_SKILL_ELEMENT::EventType_ColEffect & m_nType)
	{

	}
	if ((UINT)EC_SKILL_ELEMENT::ColType_Damage & m_nType)
	{

	}

}

void CSkillElementDir::SetValues(const tSkillElementValue & _value)
{
	static bool randOper = true;
	static int randPosOrder = 0;
	m_tSkillElementValue = _value;
	float oper = 1.0f;

	m_vOffsetPos = _value.PosValues.vOffsetPos;
	m_vStartPos = _value.PosValues.vStartPos;
	if(_value.PosValues.bRandom)
	{
		if (randPosOrder == 0 || randPosOrder == 2|| randPosOrder == 11)
		{
			if (randOper)
			{
				oper = 1.0f;
				randOper = false;
			}
			else
			{
				oper = -1.0f;
				randOper = true;
			}
			m_vStartPos.y += float(rand() % _value.PosValues.nRandomSeed) * oper;
		}
		else if (randPosOrder == 1 || randPosOrder == 5 || randPosOrder == 9)
		{
			if (randOper)
			{
				oper = 1.0f;
				randOper = false;
			}
			else
			{
				oper = -1.0f;
				randOper = true;
			}
			m_vStartPos.z += float(rand() % _value.PosValues.nRandomSeed) * oper;
		}
		else if(randPosOrder == 3 || randPosOrder == 7 || randPosOrder == 8)
		{
			if (randOper)
			{
				oper = 1.0f;
				randOper = false;
			}
			else
			{
				oper = -1.0f;
				randOper = true;
			}
			m_vStartPos.x += float(rand() % _value.PosValues.nRandomSeed) * oper;
		}
		else if (randPosOrder == 4 || randPosOrder == 6 || randPosOrder == 10)
		{
			m_vStartPos.x += float(rand() % _value.PosValues.nRandomSeed) * oper;
			if (randOper)
			{
				oper = 1.0f;
				randOper = false;
			}
			else
			{
				oper = -1.0f;
				randOper = true;
			}
			m_vStartPos.z += float(rand() % _value.PosValues.nRandomSeed) * oper;
		}

		randPosOrder += rand() % 13;
	}
	else
	{
		if (_value.PosValues.bSequence)
		{
			
		}
	}
	
	if ((UINT)EC_SKILL_ELEMENT::EndType_Pos & m_nType)
	{
		m_vEndPos = _value.Endtypes.vEndPos;
	}
	if ((UINT)EC_SKILL_ELEMENT::EndType_Time & m_nType)
	{
		m_fEndTime = _value.Endtypes.fEndTime;
	}
	if ((UINT)EC_SKILL_ELEMENT::EndType_Dist & m_nType)
	{
		m_fEndDist = _value.Endtypes.fEndDist;
	}
	if ((UINT)EC_SKILL_ELEMENT::EndType_Col & m_nType)
	{
		m_bCollider = false;
	}
	if ((UINT)EC_SKILL_ELEMENT::MoveType_Straight & m_nType)
	{
		m_fMoveSpeed = _value.MoveValues.fMoveSpeed;
		m_vMoveDir = _value.MoveValues.vMoveDir;
		if (_value.MoveValues.bRandom)
		{

			if (randOper)
			{
				oper = 1.0f;
				randOper = false;
			}
			else
			{
				oper = -1.0f;
				randOper = true;
			}
			m_fMoveSpeed += (float(rand() % _value.MoveValues.nRandomSeed) * oper) + (float(randPosOrder) * 0.232875f);
		}
		else
		{
			if (_value.MoveValues.bSequence)
			{
				
			}
		}
	}
	if ((UINT)EC_SKILL_ELEMENT::MoveType_SinCosLook & m_nType)
	{
		m_fSinCosInitAngle = _value.SinCosAngles.fSinCosInitAngle;
		m_fSinCosCurAngle = _value.SinCosAngles.fSinCosCurAngle;
		m_fSinCosAmplitude = _value.SinCosAngles.fSinCosAmplitude;
		m_fSinCosSize = _value.SinCosAngles.fSinCosSize;
	}
	if ((UINT)EC_SKILL_ELEMENT::MoveType_SinCosUp & m_nType)
	{
		m_fSinCosInitAngle = _value.SinCosAngles.fSinCosInitAngle;
		m_fSinCosCurAngle = _value.SinCosAngles.fSinCosCurAngle;
		m_fSinCosAmplitude = _value.SinCosAngles.fSinCosAmplitude;
		m_fSinCosSize = _value.SinCosAngles.fSinCosSize;
	}
	if ((UINT)EC_SKILL_ELEMENT::LifeType_Repeate & m_nType)
	{
		m_nRepeateMax = _value.RepeateValues.nRepeateMax;
		m_nRepeateCur = 0;
	}
	if ((UINT)EC_SKILL_ELEMENT::EventType_StartEffect & m_nType)
	{
		

	}
	if ((UINT)EC_SKILL_ELEMENT::EventType_IngEffect & m_nType)
	{

	}
	if ((UINT)EC_SKILL_ELEMENT::EventType_EndEffect & m_nType)
	{
		
	}
	if ((UINT)EC_SKILL_ELEMENT::EventType_ColEffect & m_nType)
	{

	}
	if ((UINT)EC_SKILL_ELEMENT::ColType_Damage & m_nType)
	{
		m_fDamage = _value.Others.fDamage;
	}



}

void CSkillElementDir::update()
{
	if (m_fTicMax > m_fTic)
	{
		m_fTic += DT;
		return;
	}
	else
	{
		m_fToc = m_fTic;
		m_fTic = 0.0f;
	}


	if (m_bAlive)//	살아있니?
	{
		if (!m_bStart)//	시작이벤트가 아니니?
		{
			//	이거 setTypes에서 설정해주기
			//	끝나는 이벤트 설정
			m_vPrePos = m_vCurPos;
			m_vCurPos = Transform()->GetLocalPos();
			if ((UINT)EC_SKILL_ELEMENT::EndType_Pos & m_nType)//	목적지에 도착했니?
			{
				if (Vector3Cmp(m_vCurPos, m_vEndPos, 0.01f))
				{
					m_bMovePossible = false;
				}
			}
			if ((UINT)EC_SKILL_ELEMENT::EndType_Time & m_nType)//	시간이 다되었니?
			{
				if (m_fCurTime < m_fEndTime)
				{
					m_fCurTime += m_fToc;
				}
				else
				{
					m_fCurTime = 0.0f;
					m_bMovePossible = false;
				}
			}
			if ((UINT)EC_SKILL_ELEMENT::EndType_Dist & m_nType)//	거리에 도달했니?
			{
				float dist = Vec3::Distance(m_vStartPos, m_vCurPos);
				if (dist > m_fEndDist)
				{
					m_bMovePossible = false;
				}
			}
			if ((UINT)EC_SKILL_ELEMENT::EndType_Col & m_nType)//	충돌을 했니?
			{
				if (m_bCollider)
				{
					m_bMovePossible = false;
				}
			}
			//	그 이벤트에 따라서 움직임 설정
			if (m_bMovePossible)//	움직이는게 가능하니?
			{
				if ((UINT)EC_SKILL_ELEMENT::EventType_IngEffect & m_nType)
				{
					//	ingEvent
				}
				if ((UINT)EC_SKILL_ELEMENT::MoveType_Straight & m_nType)//	앞으록 가야하니?
				{
					m_vCurPos += m_vMoveDir * m_fMoveSpeed * m_fToc;
				}
				if ((UINT)EC_SKILL_ELEMENT::MoveType_SinCosLook & m_nType)//	바라보는 방향으로 회전하니?
				{
					Vec3 dir = XMVector3TransformNormal(Vec3::Up, XMMatrixRotationZ(m_fSinCosCurAngle));
					dir *= m_fSinCosSize;
					m_fSinCosCurAngle += m_fSinCosAmplitude;

					m_vCurPos += dir;
				}
				if ((UINT)EC_SKILL_ELEMENT::MoveType_SinCosUp & m_nType)//	위아래로 회전하니?
				{
					Vec3 dir = XMVector3TransformNormal(Vec3::Front, XMMatrixRotationY(m_fSinCosCurAngle));
					dir *= m_fSinCosSize;
					m_fSinCosCurAngle += m_fSinCosAmplitude;

					m_vCurPos += dir;
				}

				Transform()->SetLocalPos(m_vCurPos);
			}
			else//	움직이지 않으면 끝나는거란다
			{
				//	끝내기
				if ((UINT)EC_SKILL_ELEMENT::LifeType_Repeate & m_nType)//	반복해야하는 친구니?
				{
					if (m_nRepeateCur > 0)//	반복 횟수가 남았니?
					{
						--m_nRepeateCur;
						m_fCurTime = 0.0f;
						m_fSinCosCurAngle = 0.0f;
						m_bMovePossible = true;
						Transform()->SetLocalPos(m_vStartPos);
					}
					else
					{
						m_bAlive = false;
						m_nRepeateCur = m_nRepeateMax;
					}
				}
				else//	AtOnce
				{
					m_bAlive = false;
				}
			}
		}
		else
		{
			//	스타트에서 해줘야하나?
			//	시작 이벤트
			//	이벤트 오브젝트 만들기
			if ((UINT)EC_SKILL_ELEMENT::EventType_StartEffect & m_nType)
			{
				if (m_fStartEventCur < m_fStartEventMax)
				{
					if (!m_pStartEvent->IsActive())
					{
						m_pStartEvent->SetActive(true);
						CSkillEvent* pStart = m_pStartEvent->GetScript<CSkillEvent>();
						pStart->SetAlive(true);
						pStart->SetStart(true);
					}
					m_fStartEventCur += m_fToc;
				}
				else
				{
					m_pStartEvent->SetActive(false);
					CSkillEvent* pStart = m_pStartEvent->GetScript<CSkillEvent>();
					pStart->SetAlive(true);
					pStart->SetStart(true);

					Transform()->SetLocalPos(m_vStartPos);
					m_vCurPos = m_vStartPos;
					m_bStart = false;
					m_fStartEventCur = 0.0f;
				}
			}
			else
			{
				Transform()->SetLocalPos(m_vStartPos);
				m_vCurPos = m_vStartPos;
				m_bStart = false;
			}
		}
	}
	else
	{
		if (m_bEnd)
		{
			if ((UINT)EC_SKILL_ELEMENT::EventType_EndEffect & m_nType)
			{
				if (m_fEndEventCur < m_fEndEventMax)
				{
					//	엔드 이벤트
					m_fStartEventCur += m_fToc;
				}
				else
				{
					m_bEnd = false;
				}
			}
			else
			{
				m_bEnd = false;
			}
			
		}
		else
		{
			GetObj()->SetActive(false);
			m_fCurTime = 0.0f;
			m_fSinCosCurAngle = 0.0f;
			m_bMovePossible = true;
			m_bStart = true;
			m_bEnd = false;
			m_bCollider = false;
			Transform()->SetLocalPos(m_vStartPos);
			return;
		}
	}
}

void CSkillElementDir::OnCollisionEnter(CCollider2D * _pOther)
{
	if (m_bAlive)
	{
		if ((UINT)EC_SKILL_ELEMENT::ColType_Damage)
		{
			_pOther->GetObj()->StatusComponent()->DamageStack(GetObj()->GetID(),m_vCurPos, m_fDamage);
		}
		if ((UINT)EC_SKILL_ELEMENT::EventType_ColEffect)
		{
			//	충돌시 이벤트
		}

		m_bCollider = true;
	}
}

void CSkillElementDir::OnCollision(CCollider2D * _pOther)
{
}

void CSkillElementDir::OnCollisionExit(CCollider2D * _pOther)
{
}

CGameObject * CSkillElementDir::CreateEffect(int _timing, int _textureIdx, UINT _type, tSkillEventValue& _value)
{
	CGameObject* pObj = new CGameObject;
	pObj->AddComponent(new CTransform);
	pObj->AddComponent(new CMeshRender);

	if ((UINT)EC_SKILL_EVENT::MeshType_Rect & _type)
	{
		pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		pObj->Transform()->SetLocalScale(Vec3(10.f, 10.f, 1.f));
	}
	else if ((UINT)EC_SKILL_EVENT::MeshType_Circle & _type)
	{
		//CircleMesh
		pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"CircleMesh"));
		pObj->Transform()->SetLocalScale(Vec3(3.f, 3.f, 3.f));
	}
	else if ((UINT)EC_SKILL_EVENT::MeshType_Cube & _type)
	{
		pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"CubeMesh"));
		pObj->Transform()->SetLocalScale(Vec3(3.f, 3.f, 3.f));

	}
	else if ((UINT)EC_SKILL_EVENT::MeshType_Sphere & _type)
	{
		pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
		pObj->Transform()->SetLocalScale(Vec3(3.f, 3.f, 3.f));
	}
	else
	{
		SAFE_DELETE(pObj);
		return nullptr;
	}
	//pObj->MeshRender()->GetSharedMaterial()
	Ptr<CMaterial> TexMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
	Ptr<CTexture> tex = CMapMgr::GetInst()->GetMonsterSkillEffectTexture(0);

	pObj->MeshRender()->SetMaterial(TexMtrl->Clone());
	pObj->MeshRender()->GetCloneMaterial()->SetData(SHADER_PARAM::TEX_0, tex.GetPointer());

	CSkillEvent* skill = nullptr;
	skill = new CSkillEvent;
	pObj->AddComponent(skill);
	skill->SetTypes(_type);
	skill->SetAlive(true);
	skill->SetStart(true);
	skill->SetEnd(false);
	if ((UINT)EC_SKILL_EVENT::ActionType_Spread & _type)
	{
		
	}
	if ((UINT)EC_SKILL_EVENT::ActionType_Flow & _type)
	{
		_value.c_pTarget = GetObj();

	}
	if ((UINT)EC_SKILL_EVENT::ActionType_Rotate & _type)
	{

	}
	skill->SetValues(_value);

	if (0 == _timing)
	{
		m_pStartEvent = pObj;
	}
	else if (1 == _timing)
	{
		m_pIngEvent = pObj;
	}
	else if (2 == _timing)
	{
		m_pEndEvent = pObj;
	}
	return pObj;
}

void CSkillElementDir::ResetStartEffect(tSkillEventValue& _value)
{
	CSkillEvent* skill = m_pStartEvent->GetScript<CSkillEvent>();
	skill->SetValues(_value);
}
