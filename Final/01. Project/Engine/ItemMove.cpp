#include "stdafx.h"
#include "ItemMove.h"


CItemMove::CItemMove()
	:CScript((UINT)SCRIPT_TYPE::ITEM)
	, m_fAngle(3.f)
	, m_fHoverMax(3.f)
	, m_fHoverCur(0.f)
	, m_fHoverOfer(1.f)
	, m_fHoverMin(0.f)
{
}


CItemMove::~CItemMove()
{
}

void CItemMove::update()
{
	float delta = DT;
	float dist = 0.f;
	m_vTempPos = Transform()->GetLocalPos();
	m_vTempRot = Transform()->GetLocalRot();

	dist = m_fHoverOfer * delta;
	m_fHoverCur += dist;

	if (m_fHoverMin < m_fHoverCur && m_fHoverCur < m_fHoverMax)
	{
		m_vTempPos.y += dist;
		Transform()->SetLocalPos(m_vTempPos);
	}
	else
	{
		m_fHoverOfer *= -1.f;
	}

	if (m_vTempRot.y > 360.f)m_vTempRot.y -= 360.f;
	m_vTempRot.y += m_fAngle * delta;

	Transform()->SetLocalRot(m_vTempRot);
}
