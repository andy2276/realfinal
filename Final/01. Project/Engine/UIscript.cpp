#include "stdafx.h"
#include "UIscript.h"

#include "StatusComponent.h"
#include"TimeMgr.h"

void UIscript::update()
{

	int scale = 10; 

	if (m_pPlayer != nullptr)
	{
		float player_hp = m_pPlayer->StatusComponent()->getHp();
		float player_mp = m_pPlayer->StatusComponent()->getMp();

	switch (eType)
	{
	case (UINT)UI_TYPE::HP:

		//플레이어의 new hp와 old hp

		//HP를 받아와서 크기를 줄여주는것. 
		if (m_hp != player_hp)
		{
			

			Vec3 vScale = Transform()->GetLocalScale();
			vScale.x -= scale;

			Transform()->SetLocalScale(vScale);




			Vec3 vPos = Transform()->GetLocalPos();
			vPos.x -= scale/2;
			Transform()->SetLocalPos(vPos);

			
			m_hp -= 1;
		}
		else {
			m_hp = player_hp;
		}
		break;

	case (UINT)UI_TYPE::MP:


		if (m_mp != player_mp)
		{
			m_mp = player_mp;

			Vec3 vScale = Transform()->GetLocalScale();
			vScale.x -= 10;

			Transform()->SetLocalScale(vScale);

			Vec3 vPos = Transform()->GetLocalPos();
			vPos.x -= 5;
			Transform()->SetLocalPos(vPos);
		}
		break;

	default:
		break;
	}

		


	}

}

void UIscript::init()
{
	if (m_pPlayer != NULL)
	{
		m_hp = m_pPlayer->StatusComponent()->getHp();
		m_mp = m_pPlayer->StatusComponent()->getMp();
	}
}

UIscript::UIscript() : CScript((UINT)SCRIPT_TYPE::UISCRIPT),eType((UINT)UI_TYPE::HP)
,m_pPlayer(nullptr)
{
	
}


UIscript::~UIscript()
{
}
