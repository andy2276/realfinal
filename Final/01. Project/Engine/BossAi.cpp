#include "stdafx.h"
#include "BossAi.h"
#include "Animator3D.h"
#include "StatusComponent.h"
#include "SkillPlatform.h"
#include "MapScript.h"
#include "MapMgr.h"

CBossAi::CBossAi()
	:CScript((UINT)SCRIPT_TYPE::AI)
	, m_fTic(0.0f)
	, m_fToc(0.0f)
	, m_fTicMax(0.016f)
{
	m_bShootSkillOn[0] = m_bShootSkillOn[1] = m_bShootSkillOn[2] = false;
	m_bBagicAttackDamage = 20.f;
	m_bBaggicAttackDist = 20.f;
	m_bBaggicAttackMinAngle = -45.0f * XM_ANGLE;
	m_bBaggicAttackMaxAngle = 45.0f * XM_ANGLE;
	m_OnActOrder = false;
	m_OnActOrderStart = true;
	m_nCurAction = (UINT)EC_BOSS_STATE::Evaluate;
}


CBossAi::~CBossAi()
{

}

void CBossAi::SetBossSkill(int _idx, int _type)
{
	m_arrSkillPlatform[_idx] = new CSkillPlatform;
	GetObj()->AddComponent(m_arrSkillPlatform[_idx]);
	m_arrSkillPlatform[_idx]->InitSkillBullet(CMapMgr::GetInst()->GetSkillArch(_type));

}

CStatusComponent * CBossAi::GetStatus()
{
	return GetObj()->StatusComponent();
}

void CBossAi::Reset(int _mapNum, const Vec3 & _pos, const Vec3 & _rot)
{
	const int skill1 = 0, skill2 = 1, skill3 = 2;
	const int phase1 = 0, phase2 = 1, phase3 = 2;


	CStatusComponent* state = GetObj()->StatusComponent();

	state->SetFloat(STATUS_FLOAT::HP, m_tResetInfo.fMax);
	state->SetFloat(STATUS_FLOAT::MAX_HP, m_tResetInfo.fMax);
	state->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX, m_tResetInfo.bagicCool);
	state->SetFloat(STATUS_FLOAT::SKILL_1_COOL_MAX, m_tResetInfo.skill1Cool);
	state->SetFloat(STATUS_FLOAT::SKILL_2_COOL_MAX, m_tResetInfo.skill1Cool);
	state->SetFloat(STATUS_FLOAT::MOVESPEED, m_tResetInfo.moveSpeed);
	state->SetFloat(STATUS_FLOAT::ROTSPEED, m_tResetInfo.rotSpeed);
	state->SetFloat(STATUS_FLOAT::TIME_ACCEL, 1.f);
	state->SetFloat(STATUS_FLOAT::ATTACKRANGE, 1.f);
	state->SetMapNum(_mapNum);

	SetSkillShootDist(skill1, m_tResetInfo.skill1Dist);
	SetSkillShootTime(skill1, m_tResetInfo.skill1ShootTime);

	SetSkillShootDist(skill2, m_tResetInfo.skill2Dist);
	SetSkillShootTime(skill2, m_tResetInfo.skill2ShootTime);

	SetBagicAttackDist(m_tResetInfo.bagicDist);
	SetBagicAttackAngle(-m_tResetInfo.bagicAngle, m_tResetInfo.bagicAngle);
	SetBagicAttackTime(m_tResetInfo.bagicAttackTime);

	SetPhaseHp(phase1, m_tResetInfo.phase1hp);
	SetPhaseHp(phase2, m_tResetInfo.phase2hp);
	SetPhaseHp(phase3, m_tResetInfo.phase3hp);

	Transform()->SetLocalPos(_pos);
	Transform()->SetLocalRot(_rot);
}

void CBossAi::PushStack(UINT _nValue)
{
	static const int nPrecision = 4;
	list<UINT>::reverse_iterator itor = m_eStatStack.rbegin();
	list<UINT>::reverse_iterator end = m_eStatStack.rend();
	for (int i = 0; itor != end; ++itor, ++i)
	{
		if ((*itor) == _nValue)return;
		if (i > nPrecision)
		{
			break;
		}
	}
	m_eStatStack.push_back(_nValue);
}

void CBossAi::update()
{
	static Vec3 mePos, meLook, playerPos, lookPlayer, turnOper, meRot, destPos, initDir, lookDest;
	static float playerTodist, destToDist, angle, oper, idleDist, idleDest, fHeight;
	static float fPhase2Hp(50.f), fStuckDamage(20.f), fPattern(75.f), fNearDist(19.f);
	static UINT nStateSkills = ((UINT)EC_BOSS_STATE::Skill1 | (UINT)EC_BOSS_STATE::Skill2 | (UINT)EC_BOSS_STATE::Skill3);
	static UINT nStateAttack = (nStateSkills | (UINT)EC_BOSS_STATE::BagicAttack);
	CGameObject* pTarget = nullptr;
	float searchGoDist = 15.f;

	if (m_fTicMax > m_fTic)
	{
		m_fTic += DT;
		return;
	}
	else
	{
		m_fToc = m_fTic;
		m_fTic = 0.0f;
	}

	CStatusComponent* state = GetObj()->StatusComponent();
	CTransform* trans = GetObj()->Transform();
	CAnimator3D* animate = GetObj()->Animator3D();

	//---------------------------------------------------------------------------------------------------------------
	//	계속 확인하는곳
	//	스킬1 확인
	if ((UINT)EC_BOSS_STATE::Skill1 & m_nInitCheckList)//	초기에 확인해야하는 리스트에 있다
	{
		if ((UINT)EC_BOSS_STATE::Skill1 & ~m_nCurCheckList)
		{
			if (state->GetFloat(STATUS_FLOAT::SKILL_1_COOL_MAX) > state->GetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW))
			{
				state->SetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW, state->GetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW) + m_fToc);
			}
			else
			{
				m_nCurCheckList |= (UINT)EC_BOSS_STATE::Skill1;
				m_bShootSkillOn[0] = false;
			}
		}

	}
	//	스킬2 확인
	if ((UINT)EC_BOSS_STATE::Skill2 & m_nInitCheckList)
	{
		if ((UINT)EC_BOSS_STATE::Skill2 & ~m_nCurCheckList)
		{
			if (state->GetFloat(STATUS_FLOAT::SKILL_2_COOL_MAX) > state->GetFloat(STATUS_FLOAT::SKILL_2_COOL_NOW))
			{
				state->SetFloat(STATUS_FLOAT::SKILL_2_COOL_NOW, state->GetFloat(STATUS_FLOAT::SKILL_2_COOL_NOW) + m_fToc);
			}
			else
			{
				m_nCurCheckList |= (UINT)EC_BOSS_STATE::Skill2;
				m_bShootSkillOn[1] = false;
			}
		}
	}
	//	스킬3 확인
	if ((UINT)EC_BOSS_STATE::Skill3 & m_nInitCheckList)
	{
		if ((UINT)EC_BOSS_STATE::Skill3 & ~m_nCurCheckList)
		{
			if (state->GetFloat(STATUS_FLOAT::SKILL_HEAL_COOL_MAX) > state->GetFloat(STATUS_FLOAT::SKILL_HEAL_COOL_NOW))
			{
				state->SetFloat(STATUS_FLOAT::SKILL_HEAL_COOL_NOW, state->GetFloat(STATUS_FLOAT::SKILL_HEAL_COOL_NOW) + m_fToc);
			}
			else
			{
				m_nCurCheckList |= (UINT)EC_BOSS_STATE::Skill3;
				m_bShootSkillOn[2] = false;
			}
		}
	}
	//	기본공격 확인
	if ((UINT)EC_BOSS_STATE::BagicAttack & ~m_nCurCheckList)
	{
		if (state->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX) > state->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW))
		{
			state->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW, state->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW) + m_fToc);
		}
		else
		{
			m_nCurCheckList |= (UINT)EC_BOSS_STATE::BagicAttack;
		}
	}


	//	가장 높은 우선순위를 가진것
	//	데미지 확인
	float fDamaged = 0.f;
	{
		std::list<tDamageStack>& stack = state->GetDamageStack();
		while (!stack.empty())
		{
			fDamaged += stack.back().fAttackDamage;
			stack.pop_back();
		}
		if (fDamaged > fStuckDamage)	//	순간 데미지
		{
			//	m_eState = EC_BOSS_STATE::Damaged;	//	바로 데미지로 상태를 변경
			m_nAlwaysCheckList = (UINT)EC_BOSS_STATE::Damaged;
			state->setHpDown(fDamaged);
		}
	}
	//	죽어야하는지 확인
	{
		if (state->GetLowHp())
		{
			m_eState = EC_BOSS_STATE::Death;
			m_nAlwaysCheckList = (UINT)EC_BOSS_STATE::Death;
		}
	}
	//---------------------------------------------------------------------------------------------------------------
	//	계산부	이건 없앨수도 있음
	playerPos = m_pPlayer->Transform()->GetLocalPos();
	mePos = trans->GetLocalPos();
	meLook = trans->GetLocalDir(DIR_TYPE::FRONT);


	playerTodist = Vec3::Distance(playerPos, mePos);
	if ((UINT)EC_BOSS_STATE::Collison & m_nCurCheckList)
	{
		//	Astar
		//	and SetDest
		vector<CGameObject*>& vecE = CMapMgr::GetInst()->GetMapScript(state->GetMapNum())->GetEnvirObj();
		list<CGameObject*> listE(vecE.begin(), vecE.end());
		list<CGameObject*> listTemp;
		map<float, CGameObject*> mapTemp;

		list<CGameObject*>::iterator begin = listE.begin();
		list<CGameObject*>::iterator itor = begin;
		list<CGameObject*>::iterator end = listE.end();

		//	firstSort
		Vec3 vFinDest = playerPos;
		Vec3 vCurDest = playerPos;
		Vec3 vCurPoint = mePos;

		Vec3 vLine = vCurDest - vCurPoint;
		Vec3 vLineLook = vLine.Normalize();

		float fFintoDist = Vec3::Distance(vFinDest, vCurPoint);

		Vec3 vTemp;
		Vec3 vDist;
		Vec3 vCirclePos;
		Vec3 vInCircle;
		float fBotLength;
		float fDist;
		float fLen;
		float fLowDist = 200.0f;
		CGameObject* pLow = nullptr;
		float d;
		for (int i = 0; i < vecE.size(); ++i)
		{
			vCirclePos = vecE[i]->Transform()->GetLocalPos();
			vTemp = vCirclePos - mePos;
			fLen = vTemp.Length();
			fBotLength = vTemp.Dot(vLineLook);

			d = 3.f * 3.f - (fLen * fLen - fBotLength * fBotLength);
			if (d > 0.f)
			{
				mapTemp.insert(make_pair(d, vecE[i]));
			}
		}
		map<float, CGameObject*>::iterator mItor = mapTemp.begin();
		map<float, CGameObject*>::iterator mEnd = mapTemp.end();


		while (true)
		{
			if (itor == end)break;
			else if (Vec3::Distance(vCurPoint, vFinDest) < 10.0f)break;

		}



	}

	if ((UINT)EC_BOSS_STATE::TargetPlayer & m_nCurCheckList)
	{
		destPos = playerPos;
	}
	else
	{



	}
	destPos = playerPos;


	//	trans

	int a = state->GetMapNum();
	//	2차원으로 변경
	mePos.y = 0.0f;
	meLook.y = 0.0f;
	playerPos.y = 0.0f;
	destPos.y = 0.0f;
	//	look
	lookPlayer = (playerPos - mePos).Normalize();
	lookDest = (destPos - mePos).Normalize();
	//	distance
	playerTodist = Vec3::Distance(playerPos, mePos);
	destToDist = Vec3::Distance(destPos, mePos);
	//	rot
	turnOper = meLook.Cross(lookDest);
	meRot = trans->GetLocalRot();

	angle = meLook.Dot(lookDest);

	oper = (turnOper.y < 0.f) ? -1.0f : 1.0f;
	//---------------------------------------------------------------------------------------------------------------
	//	행동부
	if (m_nAlwaysCheckList != (UINT)EC_BOSS_STATE::Done)
	{
		if ((UINT)EC_BOSS_STATE::Death & m_nAlwaysCheckList)
		{
			//	죽는다! 죽는 애니메이션과 죽는거를 출력!
			if (m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Death] >= 0)//	매핑이 되어있는가
			{
				if (animate->IsAnimIdx(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Death]))
				{
					if (animate->IsAnimEnd())
					{
						CMapMgr::GetInst()->DropItem(mePos);
						m_nAlwaysCheckList = (UINT)EC_BOSS_STATE::Done;
						GetObj()->SetActive(false);
						//	dropItem
						//	DeathEvent
						//	SetActive(false)
						//	초기화
						//	m_nAlwaysCheckList = Done;
					}
				}
				else
				{
					animate->ChangeAnimation(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Death]);
				}
			}
			else
			{
				//	dropItem
				CMapMgr::GetInst()->DropItem(mePos);
				//	DeathEvent
				GetObj()->SetActive(false);
				//	초기화
				//	일반리셋도 만들자
				m_nAlwaysCheckList = (UINT)EC_BOSS_STATE::Done;
			}

		}
		else
		{
			//	데미지
			if (m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Damage] >= 0)
			{
				if (animate->IsAnimIdx(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Damage]))
				{
					if (animate->IsAnimEnd())
					{
						//	초기화
						//	m_nAlwaysCheckList = Done;
						m_nAlwaysCheckList &= ~(UINT)EC_BOSS_STATE::Damaged;
					}
				}
				else
				{
					animate->ChangeAnimation(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Damage]);
				}
			}
			else
			{
				//	데미지 매핑이 안되어있다.
			}
		}
	}
	else
	{
		//	액션
		if (m_OnActOrder)
		{
			if (!m_eStatStack.empty())
			{
				if (m_OnActOrderStart)
				{
					m_nCurAction = m_eStatStack.front();
					m_eStatStack.pop_front();
					m_OnActOrderStart = false;
				}
			}
			else
			{
				m_OnActOrder = false;
			}
		}

		//	지금해야하는 액션을 가져온다

			//	회전
		if ((UINT)EC_BOSS_STATE::Turn & m_nCurAction)
		{
			if (!FloatCmp(angle, 1.0f, 0.01f))
			{
				meRot.y += state->GetFloat(STATUS_FLOAT::ROTSPEED)*m_fToc * oper;
				trans->SetLocalRot(meRot);
			}
			else
			{
				m_nCurAction &= ~(UINT)EC_BOSS_STATE::Turn;
			}
		}

		//	이동
		if ((UINT)EC_BOSS_STATE::DestMove & m_nCurAction)
		{
			if (destToDist > m_fCurNearDist)
			{
				if (m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Run] >= 0)
				{
					if (!animate->IsAnimIdx(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Run]))
					{
						if (m_nCurAction & ~nStateAttack)animate->ChangeAnimation(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Run], true);
					}
				}
				else
				{
					//	매핑안됨
				}
				mePos += lookDest * state->GetFloat(STATUS_FLOAT::MOVESPEED) * m_fToc * state->GetFloat(STATUS_FLOAT::TIME_ACCEL);
				mePos.y = CMapMgr::GetInst()->GetHeight(state->GetMapNum(), mePos);
				trans->SetLocalPos(mePos);
			}
			else
			{
				m_nCurAction &= ~(UINT)EC_BOSS_STATE::DestMove;
			}
		}
		else if ((UINT)EC_BOSS_STATE::BackMove & m_nCurAction)
		{
		}
		else if ((UINT)EC_BOSS_STATE::SideMove & m_nCurAction)
		{
		}

		//	스킬 사용
		if ((UINT)EC_BOSS_STATE::Skill1 & m_nCurAction)
		{
			if ((UINT)EC_BOSS_STATE::Skill1& m_nCurCheckList)
			{
				if (m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Skill1] >= 0)
				{
					if (animate->IsAnimIdx(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Skill1]))
					{
						if (!animate->IsAnimEnd())
						{
							if (animate->GetCurAnimLeftTime() < m_fShootSkillTime[0])
							{
								if (!m_bShootSkillOn[0])
								{
									m_bShootSkillOn[0] = true;	//	발사성공
									//	여기에 스킬 슛코드를 쓰자
									m_arrSkillPlatform[0]->Shoot(mePos + (meLook) * 5.f, meRot, meLook);
								}
							}
						}
						else
						{
							m_nCurAction &= ~(UINT)EC_BOSS_STATE::Skill1;
							m_nCurCheckList &= ~(UINT)EC_BOSS_STATE::Skill1;
							state->SetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW, 0.0f);
							m_bShootSkillOn[0] = false;
						}
					}
					else
					{
						animate->ChangeAnimation(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Skill1], true);
					}
				}
				else
				{
					//	매핑안됨
				}
			}
			else
			{
				m_nCurAction &= ~(UINT)EC_BOSS_STATE::Skill1;
			}
		}
		if ((UINT)EC_BOSS_STATE::Skill2 & m_nCurAction)
		{
			if ((UINT)EC_BOSS_STATE::Skill2& m_nCurCheckList)
			{
				if (m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Skill2] >= 0)
				{
					if (animate->IsAnimIdx(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Skill2]))
					{
						if (!animate->IsAnimEnd())
						{
							if (animate->GetCurAnimLeftTime() < m_fShootSkillTime[1])
							{
								if (!m_bShootSkillOn[1])
								{
									m_bShootSkillOn[1] = true;	//	발사성공
									//	여기에 스킬 슛코드를 쓰자
								}
							}
						}
						else
						{
							m_nCurAction &= ~(UINT)EC_BOSS_STATE::Skill2;
							m_nCurCheckList &= ~(UINT)EC_BOSS_STATE::Skill2;
							state->SetFloat(STATUS_FLOAT::SKILL_2_COOL_NOW, 0.0f);
							m_bShootSkillOn[1] = false;
							animate->ChangeAnimation(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Idle], true);
						}
					}
					else
					{
						animate->ChangeAnimation(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Skill2], true);
					}
				}
				else
				{
					//	매핑안됨
				}
			}
			else
			{
				m_nCurAction &= ~(UINT)EC_BOSS_STATE::Skill2;
			}
		}
		if ((UINT)EC_BOSS_STATE::Skill3 & m_nCurAction)
		{
			if ((UINT)EC_BOSS_STATE::Skill3& m_nCurCheckList)
			{
				if (m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Skill3] >= 0)
				{
					if (animate->IsAnimIdx(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Skill3]))
					{
						if (!animate->IsAnimEnd())
						{
							if (animate->GetCurAnimLeftTime() < m_fShootSkillTime[2])
							{
								if (!m_bShootSkillOn[2])
								{
									m_bShootSkillOn[2] = true;	//	발사성공
									//	여기에 스킬 슛코드를 쓰자
								}
							}
						}
						else
						{
							m_nCurAction &= ~(UINT)EC_BOSS_STATE::Skill3;
							m_nCurCheckList &= ~(UINT)EC_BOSS_STATE::Skill3;
							state->SetFloat(STATUS_FLOAT::SKILL_HEAL_COOL_NOW, 0.0f);
							m_bShootSkillOn[2] = false;
							animate->ChangeAnimation(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Idle], true);
						}
					}
					else
					{
						animate->ChangeAnimation(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Skill3], true);
					}
				}
				else
				{
					//	매핑안됨
				}
			}
			else
			{
				m_nCurAction &= ~(UINT)EC_BOSS_STATE::Skill3;
			}
		}
		if ((UINT)EC_BOSS_STATE::BagicAttack & m_nCurAction)
		{
			if ((UINT)EC_BOSS_STATE::BagicAttack & m_nCurCheckList)
			{
				if (m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Attack] >= 0)
				{
					if (animate->IsAnimIdx(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Attack]))
					{
						if (!animate->IsAnimEnd())
						{
							if (animate->GetCurAnimLeftTime() < m_fBagicAttackTime)
							{
								if (angle > 0.0f && abs(angle) < 75.f * XM_ANGLE)
								{
									if (playerTodist < m_bBaggicAttackDist)
									{
										if (!m_bBagicAttackOn)
										{
											m_bBagicAttackOn = true;
											state->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW, 0.0f);
											m_pPlayer->StatusComponent()->DamageStack(GetObj()->GetID(), meLook, m_bBagicAttackDamage);
										}
									}
								}
							}
						}
						else
						{
							m_nCurAction &= ~(UINT)EC_BOSS_STATE::BagicAttack;
							m_nCurCheckList &= ~(UINT)EC_BOSS_STATE::BagicAttack;
							state->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW, 0.0f);
							m_bBagicAttackOn = false;
							animate->ChangeAnimation(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Idle], true);
						}
					}
					else
					{
						if (m_nCurAction & ~nStateSkills)animate->ChangeAnimation(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Attack], true);
					}
				}
				else
				{
					m_nCurAction &= ~(UINT)EC_BOSS_STATE::BagicAttack;
					m_bBagicAttackOn = false;
				}
			}
		}
		if ((UINT)EC_BOSS_STATE::Search & m_nCurAction)
		{

		}

		if (m_nCurAction == (UINT)EC_BOSS_STATE::Done)
		{
			if (m_OnActOrder)
			{
				m_OnActOrderStart = true;
			}
			else
			{
				m_nCurAction |= (UINT)EC_BOSS_STATE::Evaluate;
				if (nStateAttack & ~m_nCurAction)animate->ChangeAnimation(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Idle], true);
			}
		}
	}
	//	실행부
	//	평가부
	//	(is turn)
	//	평가를 해야하거나 그렇다면 이걸 진행한다
	UINT nTempAction = (UINT)EC_BOSS_STATE::Done;
	//if (abs(angle) < 85.f * XM_ANGLE)
	if ((UINT)EC_BOSS_STATE::Evaluate & m_nCurAction)
	{
		if (state->GetFloat(STATUS_FLOAT::HP) <= m_fPhaseHp[2])
		{
			if (state->GetFloat(STATUS_FLOAT::HP) < 100.f)
			{
				if (m_OnAngryStart)
				{
					m_OnAngry = true;
					m_OnAngryStart = false;
				}
			}

			if (playerTodist < m_bBaggicAttackDist)
			{
				m_OnActOrder = false;
				if (abs(angle) < 85.f * XM_ANGLE)
				{
					if ((UINT)EC_BOSS_STATE::BagicAttack & m_nCurCheckList)
					{
						m_fCurNearDist = m_bBaggicAttackDist;
						m_nCurAction |= (UINT)EC_BOSS_STATE::BagicAttack | (UINT)EC_BOSS_STATE::DestMove | (UINT)EC_BOSS_STATE::Turn;
						m_fMoveDelayCur = 0.0f;
					}
				}
				else
				{
					m_fCurNearDist = m_bBaggicAttackDist;
					m_nCurAction |= (UINT)EC_BOSS_STATE::Turn | (UINT)EC_BOSS_STATE::DestMove;
				}
				

			}
			else//	가까운거리가 아니다
			{
				m_OnActOrder = false;
				if (m_fMoveDelayCur > m_fMoveDelayMax)//	가까워지지 않는다.
				{
					if (playerTodist < m_fShootSkillDist[0])
					{
						if ((UINT)EC_BOSS_STATE::Skill1 & m_nCurCheckList)
						{
							m_fCurNearDist = m_fShootSkillDist[0];
							m_fMoveDelayCur = 0.0f;
							m_nCurAction |= (UINT)EC_BOSS_STATE::Skill1| (UINT)EC_BOSS_STATE::DestMove| (UINT)EC_BOSS_STATE::Turn;
						}
					}
					else if (playerTodist < m_fShootSkillDist[1])
					{
						if ((UINT)EC_BOSS_STATE::Skill2 & m_nCurCheckList)
						{
							m_fCurNearDist = m_fShootSkillDist[1];

							m_fMoveDelayCur = 0.0f;
							m_nCurAction |= (UINT)EC_BOSS_STATE::Skill2 | (UINT)EC_BOSS_STATE::DestMove | (UINT)EC_BOSS_STATE::Turn;
						}
					}
					else
					{
						m_nCurAction |= ((UINT)EC_BOSS_STATE::Turn | (UINT)EC_BOSS_STATE::DestMove);
						m_OnActOrder = false;
					}
				}
				else
				{
					m_fCurNearDist = m_fShootSkillDist[0];
					m_fMoveDelayCur += m_fToc;
					m_nCurAction |= ((UINT)EC_BOSS_STATE::Turn | (UINT)EC_BOSS_STATE::DestMove);
					m_OnActOrder = false;
				}
			}


		}
		else if (state->GetFloat(STATUS_FLOAT::HP) <= m_fPhaseHp[1])
		{

		}
		else if (state->GetFloat(STATUS_FLOAT::HP) <= m_fPhaseHp[0])
		{
			if ((UINT)EC_BOSS_STATE::Skill2 & m_nCurCheckList)
			{
				if (!FloatCmp(angle, 1.0f, 0.1f))
				{
					m_nCurAction |= (UINT)EC_BOSS_STATE::Turn;
				}

				if (m_fShootSkillDist[1] > playerTodist)
				{
					m_nCurAction |= (UINT)EC_BOSS_STATE::Skill2;
				}
				else
				{
					m_fCurNearDist = m_fShootSkillDist[1];
					m_nCurAction |= (UINT)EC_BOSS_STATE::DestMove;
				}
			}
			else if ((UINT)EC_BOSS_STATE::Skill1 & m_nCurCheckList)
			{
				if (!FloatCmp(angle, 1.0f, 0.1f))
				{
					m_nCurAction |= (UINT)EC_BOSS_STATE::Turn;
				}

				if (m_fShootSkillDist[0] > playerTodist)
				{
					m_nCurAction |= (UINT)EC_BOSS_STATE::Skill1;
				}
				else
				{
					m_fCurNearDist = m_fShootSkillDist[0];
					m_nCurAction |= (UINT)EC_BOSS_STATE::DestMove;
				}
			}
			else if ((UINT)EC_BOSS_STATE::BagicAttack & m_nCurCheckList)
			{
				if (!FloatCmp(angle, 1.0f, 0.1f))
				{
					m_nCurAction |= (UINT)EC_BOSS_STATE::Turn;
				}
				if (m_bBaggicAttackDist > playerTodist)
				{
					m_nCurAction |= (UINT)EC_BOSS_STATE::BagicAttack;	
				}
				else
				{
					m_fCurNearDist = m_bBaggicAttackDist;
					m_nCurAction |= (UINT)EC_BOSS_STATE::DestMove;
				}
			}
			else
			{
				if (playerTodist < state->GetFloat(STATUS_FLOAT::SEARCHRANGE))
				{
					if (abs(angle) > m_bBaggicAttackMaxAngle)
					{
						m_nCurAction |= (UINT)EC_BOSS_STATE::Turn;
					}
					m_fCurNearDist = m_fShootSkillDist[0];
					m_nCurAction |= (UINT)EC_BOSS_STATE::DestMove;
				}
				else
				{
					animate->ChangeAnimation(m_arrBossAnim[(UINT)EC_MAPPING_ANIM::Idle], true);
				}
			}
			m_eState = EC_BOSS_STATE::End;
			if (m_nCurAction & (UINT)EC_BOSS_STATE::Evaluate)
			{
				m_nCurAction &= ~(UINT)EC_BOSS_STATE::Evaluate;
			}
		}
	}
	else
	{
		//	평가를 하지 않고 종료
	}
}

void CBossAi::ChangeMappingAnim(int _idx, bool _bTrue)
{
	if (!GetObj()->Animator3D()->IsAnimIdx(m_arrBossAnim[_idx]))
	{
		GetObj()->Animator3D()->ChangeAnimation(m_arrBossAnim[_idx], _bTrue);
	}
}

