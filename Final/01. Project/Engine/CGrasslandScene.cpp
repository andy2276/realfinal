#include "stdafx.h"
#include "CGrasslandScene.h"

#include"Layer.h"
#include"Ptr.h"
#include"MeshData.h"
#include"ResMgr.h"
#include"GameObject.h"
#include"Transform.h"
#include"StatusComponent.h"
#include"MeshRender.h"
#include"Mesh.h"
#include"Animator3D.h"
#include"PlayerScript.h"
#include"Collider2D.h"
#include"CPlayerSkillScript.h"
#include"Camera.h"
#include"ToolCamScript.h"
#include"RenderMgr.h"
#include"Light3D.h"
#include"CInventoryScript.h"
#include"UIscript.h"

#include"StatusComponent.h"
#include"MonsterScript.h"
void CGrasslandScene::CreateDefault()
{


	CGameObject* pObject = nullptr;

	// =============================================================================================
	// Camera Object 생성
	// =============================================================================================
	// Main Camera
	CGameObject* pMainCam = new CGameObject;
	pMainCam->SetName(L"MainCam");
	pMainCam->AddComponent(new CTransform);
	pMainCam->AddComponent(new CCamera);
	pMainCam->AddComponent(new CToolCamScript);

	pMainCam->Camera()->SetProjType(PROJ_TYPE::PERSPECTIVE);
	pMainCam->Camera()->SetFar(100000.f);

	pMainCam->Camera()->SetLayerAllCheck();
	pMainCam->Camera()->SetLayerCheck(30, false);
	pMainCam->Transform()->setPhysics(false);
	pMainCam->Transform()->SetLocalPos(Vec3(10.f, 10.f, -20.0f));
	//	pMainCam->Transform()->SetLocalPos(Vec3(0.f, 0.0f, 0.0f));

	pMainCam->Transform()->SetLocalRot(Vec3(XM_ANGLE * 15.f, 0.0f, 0.0f));

	FindLayer(L"Default")->AddGameObject(pMainCam);


	// =============================================================================================
	// UI Camera Object 생성
	// =============================================================================================

	CGameObject* pUICam = new CGameObject;
	pUICam->SetName(L"UIcam");
	pUICam->AddComponent(new CTransform);
	pUICam->AddComponent(new CCamera);

	pUICam->Camera()->SetProjType(PROJ_TYPE::ORTHOGRAPHIC);
	pUICam->Camera()->SetFar(100.f);
	pUICam->Camera()->SetNear(1.f);
	pUICam->Camera()->SetLayerCheck(30, true);
	pUICam->Camera()->SetWidth(CRenderMgr::GetInst()->GetResolution().fWidth);
	pUICam->Camera()->SetHeight(CRenderMgr::GetInst()->GetResolution().fHeight);
	pUICam->Transform()->setPhysics(false);

	FindLayer(L"Default")->AddGameObject(pUICam);

	CreateTargetUI();

	//------------------------------------------------------------------------------------------------------------------
	// 3D Light Object 추가
	//------------------------------------------------------------------------------------------------------------------
	pObject = new CGameObject;
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CLight3D);

	pObject->Light3D()->SetLightPos(Vec3(500.f, 500.f, 0.f));
	pObject->Light3D()->SetLightType(LIGHT_TYPE::DIR);
	pObject->Light3D()->SetDiffuseColor(Vec3(1.5f, 1.5f, 1.5f));
	pObject->Light3D()->SetSpecular(Vec3(0.5f, 0.5f, 0.5f));
	pObject->Light3D()->SetAmbient(Vec3(0.53f, 0.53f, 0.53f));
	pObject->Light3D()->SetLightDir(Vec3(1.f, -1.f, 1.f));
	pObject->Light3D()->SetLightRange(1000.f);


	pObject->Transform()->SetLocalPos(Vec3(-1000.f, 1000.f, -1000.f));
	FindLayer(L"Default")->AddGameObject(pObject);



}

void CGrasslandScene::CreateTargetUI()
{

	//============================================================================================================
	// 0731 UI 아이콘 
	//============================================================================================================

	//CGameObject * pObject2 = new CGameObject;
	//pObject2->SetName(L"UI Border");
	//pObject2->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	//pObject2->AddComponent(new CTransform);
	//pObject2->AddComponent(new CMeshRender);

	tResolution res = CRenderMgr::GetInst()->GetResolution();


	Vec3 vScale2 = Vec3(85, 70.f, 1.f);
	//pObject2->Transform()->SetLocalPos(Vec3(-300, -(res.fHeight / 2.f) + (vScale2.y / 2.f) + 10, 1.001f));

	//pObject2->Transform()->setPhysics(false);



	////	pObject->Transform()->SetLocalPos(Vec3(0,0, 1.f));

	//pObject2->Transform()->SetLocalScale(vScale2);

	//pObject2->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	//Ptr<CMaterial> pMtrl2 = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
	//pObject2->MeshRender()->SetMaterial(pMtrl2->Clone());

	//Ptr<CTexture> pUIsnowArrow = CResMgr::GetInst()->Load<CTexture>(L"UIsnowArrow", L"Texture\\UIsnowArrow.png");
	//pObject2->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pUIsnowArrow.GetPointer());

	//m_pCurScene->FindLayer(L"UI")->AddGameObject(pObject2);








	//-------------------------------------------------------------------------------------------------------------
	// skill UI / 두루마리 스킬 창 
	//-------------------------------------------------------------------------------------------------------------

	CGameObject * pObject = new CGameObject;
	pObject->SetName(L"UI Border");
	pObject->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);

	// Transform 설정
	//tResolution res = CRenderMgr::GetInst()->GetResolution();

	// -( 화면가로 크기/2 ) + (크기/2 ) + ( i * 크기) ,  ( 화면높이/2 ) - ( 크기 / 2) , 1.f

	// k : 앞에 띄울 공간 크기 비율
	//x 좌표 : -(화면가로크기 / 2) + ( 화면크기  / k )
	//y 좌표 :  -(화면세로크기 / 2) + ( 유아이 크기 ) 

	float frontRatio = 4;
	Vec3 vScale(700, 150.f, 1.f);
	//여기
	pObject->Transform()->SetLocalPos(Vec3(0, -(res.fHeight/2.f) + (vScale.y/2.f), 1.2f));
//	pObject->Transform()->SetLocalPos(Vec3(0, 0, 1.002f));

	pObject->Transform()->setPhysics(false);



	pObject->Transform()->SetLocalScale(vScale);

	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	Ptr<CMaterial> pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
	pObject->MeshRender()->SetMaterial(pMtrl->Clone());
	Ptr<CTexture> pBorder = CResMgr::GetInst()->Load<CTexture>(L"scrollui10", L"Texture\\scrollui10.png");
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pBorder.GetPointer());

	FindLayer(L"UI")->AddGameObject(pObject);


	//============================================================================================================
	// 0731 UI - 인벤토리
	//============================================================================================================

	pObject = new CGameObject;
	pObject->SetName(L"UI Border");
	pObject->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);

	// Transform 설정
	//tResolution res = CRenderMgr::GetInst()->GetResolution();

	// -( 화면가로 크기/2 ) + (크기/2 ) + ( i * 크기) ,  ( 화면높이/2 ) - ( 크기 / 2) , 1.f

	// k : 앞에 띄울 공간 크기 비율
	//x 좌표 : -(화면가로크기 / 2) + ( 화면크기  / k )
	//y 좌표 :  -(화면세로크기 / 2) + ( 유아이 크기 ) 

//	vScale = Vec3(320, 460.f, 1.f);
	vScale = Vec3(0, 0.f, 0.f);

//	pObject->Transform()->SetLocalPos(Vec3(0, +13, 1.1f));
	pObject->Transform()->SetLocalPos(Vec3(0, 0, 0));

	pObject->Transform()->setPhysics(false);
	pObject->Transform()->SetLocalScale(vScale);

	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
	pObject->MeshRender()->SetMaterial(pMtrl->Clone());

	pBorder = CResMgr::GetInst()->Load<CTexture>(L"inventory", L"Texture\\inventoryui04.png");
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pBorder.GetPointer());

//	pObject->AddComponent(new CInventoryScript);
	//	virtual void OnCollisionExit(CCollider2D* _pOther);
	//	pObject->AddComponent(new CMonsterScript);

		//pObject->AddComponent(new CCollider2D);
		//pObject->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::UIRECT);
		//pObject->Collider2D()->setUIsize(Vec2(vScale.x / 3, vScale.y / 3));

	FindLayer(L"UI")->AddGameObject(pObject);



	//============================================================================================================
	// 미니 맵
	//============================================================================================================

	pObject = new CGameObject;
	pObject->SetName(L"UI Border");
	pObject->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);


	vScale = Vec3(200, 200.f, 1.f);


	pObject->Transform()->setPhysics(false);
	pObject->Transform()->SetLocalScale(vScale);
	//	pObject->Transform()->SetLocalPos(Vec3(+(res.fWidth / 2.f) - 30 - (vScale.x /2.f), +(res.fHeight ) - 15 - (vScale.y) , 1.f));
	pObject->Transform()->SetLocalPos(Vec3(+(res.fWidth / 2.f) - 20 - (vScale.x / 2.f), +(res.fHeight / 2.f) - 20 - (vScale.y / 2.f), 1.f));

	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
	pObject->MeshRender()->SetMaterial(pMtrl->Clone());

	pBorder = CResMgr::GetInst()->Load<CTexture>(L"minimap", L"Texture\\minimap_1f.png");
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pBorder.GetPointer());

	//	virtual void OnCollisionExit(CCollider2D* _pOther);
	//	pObject->AddComponent(new CMonsterScript);

//	pObject->AddComponent(new CCollider2D);
//	pObject->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::UIRECT);
//	pObject->Collider2D()->setUIsize(Vec2(vScale.x / 3, vScale.y / 3));

	FindLayer(L"UI")->AddGameObject(pObject);




	//============================================================================================================
	// 스킬 아이콘 
	//============================================================================================================

	/*pBorder = CResMgr::GetInst()->Load<CTexture>(L"minimap", L"Texture\\icon_fire01.png");
	pBorder = CResMgr::GetInst()->Load<CTexture>(L"minimap", L"Texture\\icon_fire02.png");
	pBorder = CResMgr::GetInst()->Load<CTexture>(L"minimap", L"Texture\\icon_fire03.png");
	pBorder = CResMgr::GetInst()->Load<CTexture>(L"minimap", L"Texture\\icon_ice01.png");
	pBorder = CResMgr::GetInst()->Load<CTexture>(L"minimap", L"Texture\\icon_ice02.png");
	pBorder = CResMgr::GetInst()->Load<CTexture>(L"minimap", L"Texture\\icon_energy.png");*/


//	const int iconCount = 6;
//	Ptr<CTexture> arrTex[iconCount] = {
//		CResMgr::GetInst()->Load<CTexture>(L"iconfire01", L"Texture\\icon_fire01.png")
//		, CResMgr::GetInst()->Load<CTexture>(L"iconfire02", L"Texture\\icon_fire02.png")
//		, CResMgr::GetInst()->Load<CTexture>(L"iconfire03", L"Texture\\icon_fire03.png")
//		, CResMgr::GetInst()->Load<CTexture>(L"iconice01", L"Texture\\icon_ice01.png")
//		, CResMgr::GetInst()->Load<CTexture>(L"iconice02", L"Texture\\icon_ice02.png")
//		, CResMgr::GetInst()->Load<CTexture>(L"iconenergy", L"Texture\\icon_energy.png")
//	};
//
//
//	for (int i = 0; i < iconCount; ++i)
//	{
//		pObject = new CGameObject;
//		pObject->SetName(L"UI ICON");
//		pObject->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
//		pObject->AddComponent(new CTransform);
//		pObject->AddComponent(new CMeshRender);
//
//
//		vScale = Vec3(80, 80.f, 1.f);
//
//		pObject->Transform()->setPhysics(false);
//		pObject->Transform()->SetLocalScale(vScale);
//		//pObject->Transform()->SetLocalPos(Vec3(-350 + 50 + (vScale.x ) * i + ( i * 10), -(res.fHeight / 2.f) + 30 - (vScale.y), 1.f));
//		pObject->Transform()->SetLocalPos(Vec3(-260 + 50 + (vScale.x) * i + (i * 10), -(res.fHeight / 2.f) + 10 + (vScale.y / 2.f), 1.f));
//
//		pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
//		pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
//		pObject->MeshRender()->SetMaterial(pMtrl->Clone());
//
//		pBorder = arrTex[i];
//		pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pBorder.GetPointer());
//
//		//	virtual void OnCollisionExit(CCollider2D* _pOther);
//		//	pObject->AddComponent(new CMonsterScript);
//
//	//	pObject->AddComponent(new CCollider2D);
//	//	pObject->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::UIRECT);
//	//	pObject->Collider2D()->setUIsize(Vec2(vScale.x / 3, vScale.y / 3));
//
//		FindLayer(L"UI")->AddGameObject(pObject);
//
//	}
//
//
//	//============================================================================================================
//	// 패시브 아이콘 
//	//============================================================================================================
//
//	const int iconPassiveCount = 2;
//	Ptr<CTexture> arrTex2[iconPassiveCount] = {
//		CResMgr::GetInst()->Load<CTexture>(L"iconbook", L"Texture\\icon_book.png")
//		, CResMgr::GetInst()->Load<CTexture>(L"iconshield", L"Texture\\icon_shield.png")
//		//		, CResMgr::GetInst()->Load<CTexture>(L"iconfire03", L"Texture\\icon_fire03.png")
//		//		, CResMgr::GetInst()->Load<CTexture>(L"iconice01", L"Texture\\icon_ice01.png")
//	};
//
//
//	for (int i = 0; i < iconPassiveCount; ++i)
//	{
//		pObject = new CGameObject;
//		pObject->SetName(L"UI ICON");
//		pObject->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
//		pObject->AddComponent(new CTransform);
//		pObject->AddComponent(new CMeshRender);
//
//
//		vScale = Vec3(35, 35.f, 1.f);
//
//		pObject->Transform()->setPhysics(false);
//		pObject->Transform()->SetLocalScale(vScale);
//
//
//		pObject->Transform()->SetLocalPos(Vec3((-res.fWidth / 2.f) + (vScale.x + 10)*(i + 1) + 110, +(res.fHeight / 2.f) - 110, 1.01f));
//
//		pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
//		pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
//		pObject->MeshRender()->SetMaterial(pMtrl->Clone());
//
//		pBorder = arrTex2[i];
//		pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pBorder.GetPointer());
//
//		//	virtual void OnCollisionExit(CCollider2D* _pOther);
//		//	pObject->AddComponent(new CMonsterScript);
//
//	//	pObject->AddComponent(new CCollider2D);
//	//	pObject->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::UIRECT);
//	//	pObject->Collider2D()->setUIsize(Vec2(vScale.x / 3, vScale.y / 3));
////
//		FindLayer(L"UI")->AddGameObject(pObject);
//
//	}


	//============================================================================================================
	// 캐릭터 UI
	//============================================================================================================

	//해줘야할것 -> HP 연동해서 숫자 키우고 줄이기 
	// 위치 선정하기 
//
//
//	pObject = new CGameObject;
//	pObject->SetName(L"HP UI BAR");
//	pObject->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
//	pObject->AddComponent(new CTransform);
//	pObject->AddComponent(new CMeshRender);
//	pObject->AddComponent(new UIscript);
//
//	//pObject->GetScript<UIscript>()->setPlayer(pPlayer);
//	//pObject->GetScript<UIscript>()->init();
//
//	Vec3 vScale3_character(300, 128.f, 1.f);
//	res = CRenderMgr::GetInst()->GetResolution();
//
//
////	pObject->Transform()->SetLocalPos(Vec3((-res.fWidth / 2.f) + 20 + (vScale.x / 2.f), +(res.fHeight / 2.f) - 20 - (vScale.y ), 1.1f));
//	pObject->Transform()->SetLocalPos(Vec3((-res.fWidth / 2.f) + 20 + (vScale3_character.x / 2.f), +(res.fHeight / 2.f) - 20 - (vScale.y), 1.1f));
//
//	pObject->Transform()->setPhysics(false);
//
//
//	pObject->Transform()->SetLocalScale(vScale3_character);
//
//	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
//	pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
//	pObject->MeshRender()->SetMaterial(pMtrl->Clone());
//
//	pBorder = CResMgr::GetInst()->Load<CTexture>(L"character", L"Texture\\character04.png");
//	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pBorder.GetPointer());
//
//	FindLayer(L"UI")->AddGameObject(pObject);


}

void CGrasslandScene::awake()
{


}

CGrasslandScene::CGrasslandScene()
{
	
	


}


CGrasslandScene::~CGrasslandScene()
{
}

void CGrasslandScene::init()
{

	//씬 레이어 생성하기 
	GetLayer(0)->SetName(L"Default");
	GetLayer(1)->SetName(L"Player");
	GetLayer(2)->SetName(L"Monster");
	GetLayer(3)->SetName(L"Bullet");
	GetLayer(4)->SetName(L"Map");
	GetLayer(5)->SetName(L"ITEM");
	GetLayer(6)->SetName(L"Particle");


	GetLayer(30)->SetName(L"UI");
	GetLayer(31)->SetName(L"Tool");

		

	// 1. 땅 임의로 하나만 출력하기-> 그냥 RECT 출력해서 땅처럼 쓰던가
	// 2. 땅 위에 몬스터 세우기 

	// 3. 스킬 발사하기 -> 캐릭터 스킬은 shoot으로 통일하기. type은 따로 주고. 
	// 4. 몬스터와 원충돌 일으키기 
	// 5. 아이템 박스에 가까이 가면 Z 유아이 생성되게 하기 .
	// 6. 아이템 습득하면 아이템박스 사라지게 하기. 
	// 7. 아이템 습득하면 인벤토리로 자동으로 들어오게 하기. 
	// 8. 몬스터와 충돌하면 HP감소하게 하기 . 

	//9.HP 프로그래스 바 와, 스테이터스 설정하기 



	CGameObject* pPlayer;


	CGameObject* pObject = new CGameObject;

	//---------------------------------------------------------------------------------------
	// 바닥 오브젝트 
	//---------------------------------------------------------------------------------------
	{
		pObject = new CGameObject;
		pObject->SetName(L"Mirror floor");
		pObject->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
		pObject->AddComponent(new CTransform);
		pObject->AddComponent(new CMeshRender);


		Vec3 vScale(900, 900.f, 1.f);
		Vec3 vPos(0, 0, 0);
		//		pObject->Transform()->SetLocalPos(Vec3(m_Pos));
		pObject->Transform()->SetLocalPos(vPos);
		pObject->Transform()->setPhysics(false);
		pObject->Transform()->SetLocalScale(vScale);
		pObject->Transform()->SetLocalRot(Vec3(XM_ANGLE * 90, 0, 0)); //x축 90도 회전 


		pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		Ptr<CMaterial> pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
		pObject->MeshRender()->SetMaterial(pMtrl->Clone());
		Ptr<CTexture> pTexture = CResMgr::GetInst()->Load<CTexture>(L"purpleFloor3", L"Texture\\forest3.png");
		pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTexture.GetPointer());

		FindLayer(L"Player")->AddGameObject(pObject);
		//------------------------------------------------------------------------------------

	}


	//------------------------------------------------------------------------------------------------------------------
	//  캐릭터 character
	//------------------------------------------------------------------------------------------------------------------	
	{
		Ptr<CMeshData> pMeshData10 = CResMgr::GetInst()->LoadFBX(L"FBX\\cheeseballx29.FBX");
		//	Ptr<CMeshData> pMeshData10 = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\cheeseballx09.mdat", L"MeshData\\cheeseballx09.mdat");
		pMeshData10->Save(pMeshData10->GetPath());

		CMesh* pMesh = pMeshData10->GetMesh();
		//	pMesh->ClipedInfo();
		pMesh->ClipedAnimationReset();
		pMesh->ClipedAnimationResize(7);
		pMesh->ClipedAnimation(L"Idle", 0, 0, 30);
		pMesh->ClipedAnimation(L"Run", 1, 31, 61);
		pMesh->ClipedAnimation(L"Damage", 2, 62, 90);
		pMesh->ClipedAnimation(L"Death", 3, 91, 131);
		pMesh->ClipedAnimation(L"Attack", 4, 132, 158);
		pMesh->ClipedAnimation(L"Skill", 5, 159, 219);
		pMesh->ClipedAnimation(L"Stun", 6, 220, 250);
		//	pMesh->ClipedInfo();
		pObject = pMeshData10->Instantiate();
		pObject->Animator3D()->SetAnimClip(pMesh->GetAnimClip());
		//pObject->Animator3D()->AnimClipeReset();
		//pMeshData10->Save(pMeshData10->GetPath());

		//카메라 고정
		//pMainCam->SetParent(pObject);
		
		pObject->SetName(L"character");
		pObject->FrustumCheck(false);

		// Transform 설정
		Vec3 vScale_character(1, 1, 1);
		pObject->Transform()->setPhysics(true); //true값이 움직일수 있게 하는것. 
		pObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 250.f));
		pObject->Transform()->SetLocalScale(vScale_character);
		pObject->Transform()->SetLocalRot(Vec3(0.0f, 0.f, 0.f));
		pObject->Transform()->SetLocalPosOffset(Vec3(0.f, XM_ANGLE * 180, 0.f));

		//추후 수정 요망 
		pObject->AddComponent(new CStatusComponent());
		pObject->StatusComponent()->SetHpUI((UINT)MONSTER_TYPE::Player);
		pObject->AddComponent(new CInventoryScript);

		pObject->AddComponent(new CPlayerScript);
	//	pObject->GetScript<CPlayerScript>()->init();
		pObject->GetScript<CPlayerScript>()->awake();
		//	pObject->AddComponent(new CBulletScript);
		
		pPlayer = pObject;

	

		//	pObject->AddComponent(new CMeteorScript);
		FindLayer(L"Player")->AddGameObject(pObject);
		pObject->AddComponent(new CPlayerSkillScript);


		pObject->AddComponent(new CCollider2D);
		pObject->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::SPHERE);
		pObject->Collider2D()->SetOffsetScale(vScale_character * 10);


	}

	//---------------------------------------------------------------------------------------
	// 스켈레톤 생성 
	//---------------------------------------------------------------------------------------
	{
		Ptr<CMeshData> pMeshData10 = CResMgr::GetInst()->LoadFBX(L"FBX\\skell01.FBX");
		//	Ptr<CMeshData> pMeshData10 = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\cheeseballx09.mdat", L"MeshData\\cheeseballx09.mdat");
		pMeshData10->Save(pMeshData10->GetPath());

		CMesh* pMesh = pMeshData10->GetMesh();

		pObject = pMeshData10->Instantiate();

		pObject->SetName(L"Monster");
		pObject->FrustumCheck(false);

		// Transform 설정
		pObject->Transform()->setPhysics(true); //true값이 움직일수 있게 하는것. 
		pObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 250.f));
		Vec3 vScale_monster(1.f, 1.f, 1.f);
		pObject->Transform()->SetLocalScale(vScale_monster);
		pObject->Transform()->SetLocalRot(Vec3(0.0f, 0.f, 0.f));
		pObject->Transform()->SetLocalPosOffset(Vec3(0.f, XM_ANGLE * 180, 0.f));

		pObject->AddComponent(new CStatusComponent);
		pObject->StatusComponent()->SetHpUI((UINT)MONSTER_TYPE::Mimic);
		pObject->StatusComponent()->setPlayer(pPlayer);

		pObject->AddComponent(new CMonsterScript);

		pObject->AddComponent(new CCollider2D);
		pObject->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::SPHERE);
		pObject->Collider2D()->SetOffsetScale(vScale_monster * 2);
		//	pObject->Collider2D()->SetOffsetScale(Vec3(vScale_monster.x * 10, vScale_monster.y * 10, vScale_monster.z * 10));

		FindLayer(L"Monster")->AddGameObject(pObject);
	}
	//---------------------------------------------------------------------------------------
	// 맵 생성 
	//---------------------------------------------------------------------------------------
	{
		Ptr<CMeshData> pMeshData10 = CResMgr::GetInst()->LoadFBX(L"FBX\\test03.FBX");
		//	Ptr<CMeshData> pMeshData10 = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\cheeseballx09.mdat", L"MeshData\\cheeseballx09.mdat");
		pMeshData10->Save(pMeshData10->GetPath());

		CMesh* pMesh = pMeshData10->GetMesh();

		pObject = pMeshData10->Instantiate();

		pObject->SetName(L"Monster");
		pObject->FrustumCheck(false);

		// Transform 설정
		pObject->Transform()->setPhysics(false); //true값이 움직일수 있게 하는것. 
		pObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 250.f));
		Vec3 vScale_monster(1.f, 1.f, 1.f);
		pObject->Transform()->SetLocalScale(vScale_monster);
		pObject->Transform()->SetLocalRot(Vec3(0.0f, 0.f, 0.f));
		pObject->Transform()->SetLocalPosOffset(Vec3(0.f, XM_ANGLE * 180, 0.f));

		FindLayer(L"Monster")->AddGameObject(pObject);
	}


	
	CreateDefault();
}
