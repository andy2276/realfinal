#include "stdafx.h"
#include "MimicAi.h"
#include "StatusComponent.h"
#include "Animator3D.h"
#include "MapMgr.h"

CMimicAi::CMimicAi()
	: CScript((UINT)SCRIPT_TYPE::AI)
	, m_pPlayer(nullptr)
	, m_eState(EC_MIMIC_STATE::Evaluate)
	, m_bIsMonster(true)
	, m_bIsAttackMode(false)
	, m_bIsOpened(false)
	, m_bPlayerGetItem(false)
	, m_bDoesntItem(false)
	, m_fAttckAngle(1.f)
	, m_fAttackTimeMax(0.03333f*50.f)
	, m_fAttackTimeCur(0.f)
	, m_fAttackPossibleTime(0.03333f*24.f)
	, m_fAttackRange(5.f)
	, m_fAttackDamage(5.f)
	, m_fDeathTimeCur(0.f)
	, m_fDeathTimeMax(0.03333f * 30.f)
	, m_fTicMax(0.016f)
	, m_fTic(0.f)
	, m_fToc(0.f)
	, m_bPreAnimMove(false)
	, m_bPreIdle(false)
	, m_bDeathMode(false)

{
}


CMimicAi::~CMimicAi()
{
}

void CMimicAi::update()
{
	if (m_fTic < m_fTicMax)
	{
		m_fTic += DT;
		m_fToc = m_fTic;
		return;
	}
	else
	{
		m_fToc = m_fTic;
		m_fTic = 0.0f;
	}
	static CStatusComponent* pStatus;
	static CTransform* pTrans;
	static CAnimator3D* animation;

	static Vec3 vPlayerPos, vMePos, vLookDir, vForward, vRot, vTurnOfer;
	static float fPlayerToDist, fAngle, fOnOper,height;


	pStatus = GetObj()->StatusComponent();
	pTrans = GetObj()->Transform();
	animation = GetObj()->Animator3D();

	vRot = pTrans->GetLocalRot();
	vPlayerPos = m_pPlayer->Transform()->GetLocalPos();
	vMePos = pTrans->GetLocalPos();
	height = CMapMgr::GetInst()->GetHeight(pStatus->GetMapNum(), vMePos);
	vLookDir = pTrans->GetWorldDir(DIR_TYPE::FRONT);

	vPlayerPos.y = 0.0f;
	vMePos.y = 0.0f;
	vLookDir.y = 0.0f;

	vForward = (vPlayerPos - vMePos).Normalize();
	vTurnOfer = vLookDir.Cross(vForward);

	fPlayerToDist = Vec3::Distance(vPlayerPos, vMePos);
	fAngle = vLookDir.Dot(vForward);

	fOnOper = (vTurnOfer.y < 0.f) ? -1.0f : 1.0f;

	if (pStatus->GetLowHp())
	{
		m_eState = EC_MIMIC_STATE::Death;
		m_bDeathMode = true;
	}

	std::list<tDamageStack>& stack = GetObj()->StatusComponent()->GetDamageStack();
	Vec3 vDamageDir;
	float fDamage = 0.0f;
	while (!stack.empty())
	{
		fDamage += stack.back().fAttackDamage;
		vDamageDir = stack.back().vAttackDir;
		stack.pop_back();
	}
	if (fDamage > 0.1f)
	{
		pStatus->setHpDown(fDamage);
	//	m_eState = EC_MIMIC_STATE::Damaged;
	}
	//	작동부
	switch (m_eState)
	{
	case EC_MIMIC_STATE::Evaluate:
		if (!animation->IsAnimIdx((UINT)EC_MIMIC_ANIM::Idle))
		{
			GetObj()->Animator3D()->ChangeAnimation((UINT)EC_MIMIC_ANIM::Idle,true);
		}
		break;
	case EC_MIMIC_STATE::Turn:
		if (Vector3Cmp(vLookDir, vForward, 0.1f))
		{
			m_eState = EC_MIMIC_STATE::Evaluate;
		}
		else
		{
			GetObj()->Animator3D()->ChangeAnimation((UINT)EC_MIMIC_ANIM::Close,true);
			vRot.y += pStatus->GetFloat(STATUS_FLOAT::ROTSPEED) * m_fToc * fOnOper;
			pTrans->SetLocalRot(vRot);
		}
		break;
	case EC_MIMIC_STATE::Move:
		GetObj()->Animator3D()->ChangeAnimation((UINT)EC_MIMIC_ANIM::Idle,true);
		vMePos.y = height;
		pTrans->SetLocalPos(vMePos + (vLookDir * m_fToc * pStatus->GetFloat(STATUS_FLOAT::MOVESPEED)));
		m_eState = EC_MIMIC_STATE::Evaluate;
		break;
	case EC_MIMIC_STATE::MoveBack:
		if (fPlayerToDist > pStatus->GetFloat(STATUS_FLOAT::MINRANGE))
		{
			m_eState = EC_MIMIC_STATE::Evaluate;
		}
		else
		{
			GetObj()->Animator3D()->ChangeAnimation((UINT)EC_MIMIC_ANIM::Open, true);
			vMePos.y = height;
			pTrans->SetLocalPos(vMePos + (vLookDir * m_fToc * pStatus->GetFloat(STATUS_FLOAT::MOVESPEED) * -1.f));
		}
		break;
	case EC_MIMIC_STATE::MonsterMode:
		break;
	case EC_MIMIC_STATE::BoxMode:
		break;
	case EC_MIMIC_STATE::Idle:
		GetObj()->Animator3D()->ChangeAnimation((UINT)EC_MIMIC_ANIM::Close, true);
		break;
	case EC_MIMIC_STATE::Attack:
		if (animation->IsAnimIdx((UINT)EC_MIMIC_ANIM::Jump))
		{
			if (animation->IsAnimEnd())
			{
				if (m_fAttackOn)
				{
					m_pPlayer->StatusComponent()->DamageStack(GetObj()->GetID(),vLookDir, m_fAttackDamage);
					m_fAttackOn = false;
				}
				pStatus->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW, 0.f);
				m_eState = EC_MIMIC_STATE::Evaluate;
			}
			else
			{
				if (animation->GetCurAnimLeftTime() < 1.0f)
				{
					if (fPlayerToDist < m_fAttackRange && fAngle < m_fAttckAngle)
					{
						if (!m_fAttackOn)m_fAttackOn = true;
					}
				}
			}
		}
		else
		{
			GetObj()->Animator3D()->ChangeAnimation((UINT)EC_MIMIC_ANIM::Jump, true);
		}
		break;
	case EC_MIMIC_STATE::WaitPickItem:
		break;
	case EC_MIMIC_STATE::Open:
		break;
	case EC_MIMIC_STATE::Close:
		break;
	case EC_MIMIC_STATE::Closing:
		break;
	case EC_MIMIC_STATE::DeActive:
		break;
	case EC_MIMIC_STATE::Death:
		if (m_bDeathMode)
		{
			if (animation->IsAnimIdx((UINT)EC_MIMIC_ANIM::Open))
			{
				if (animation->IsAnimEnd())
				{
					pStatus->OffHpUies();
					CMapMgr::GetInst()->DropItem(vMePos);
					m_bDeathMode = false;
					GetObj()->SetActive(false); 
					m_eState = EC_MIMIC_STATE::Evaluate;
				}
			}
			else
			{
				animation->ChangeAnimation((UINT)EC_MIMIC_ANIM::Open, true);
			}
		}
		else
		{
			m_eState = EC_MIMIC_STATE::Evaluate;
		}
		break;
	case EC_MIMIC_STATE::Damaged:
		vDamageDir.y = 0.0f;
		vMePos += (vDamageDir* fDamage * 0.1f);
		vMePos.y = CMapMgr::GetInst()->GetHeight(vMePos);
		Transform()->SetLocalPos(vMePos);
		m_eState = EC_MIMIC_STATE::Evaluate;
		break;
	default:
		break;
	}


	if (EC_MIMIC_STATE::Evaluate != m_eState)return;

	//----------------------------------------------------------------------------------
	//	명령부
	if (m_bIsMonster)
	{
		//	몬스터
		if (pStatus->GetLowHp())
		{
			m_eState = EC_MIMIC_STATE::Death;
		}
		else
		{
			if (fPlayerToDist < pStatus->GetFloat(STATUS_FLOAT::MINRANGE))
			{
				m_eState = EC_MIMIC_STATE::MoveBack;
			}
			else if (fPlayerToDist < pStatus->GetFloat(STATUS_FLOAT::ATTACKRANGE))
			{
				if (pStatus->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW) > pStatus->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX))
				{
					if (FloatCmp(fAngle, m_fAttckAngle, 0.1f))
					{
						m_eState = EC_MIMIC_STATE::Attack;
					}
					else
					{
						m_eState = EC_MIMIC_STATE::Turn;
					}
				}
				else
				{
					pStatus->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW, pStatus->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW) + m_fToc);
				}
			}
			else if (fPlayerToDist < pStatus->GetFloat(STATUS_FLOAT::SEARCHRANGE))
			{
				//	move
				if (FloatCmp(fAngle, m_fAttckAngle, 0.1f))
				{
					m_eState = EC_MIMIC_STATE::Move;
				}
				else
				{
					m_eState = EC_MIMIC_STATE::Turn;
				}
			}
			else
			{
				//	idle
				m_eState = EC_MIMIC_STATE::Idle;
			}
		}
	}
	else
	{
		//	그냥 박스



	}

}

void CMimicAi::Reset(const Vec3 & _pos, const Vec3 & _rot)
{
	GetObj()->Transform()->SetLocalPos(_pos);
	GetObj()->Transform()->SetLocalRot(_rot);

	//	 m_fAttckAngle(2.f)
	//	, m_fAttackTimeMax(0.03333f*50.f)
	//	, m_fAttackTimeCur(0.f)
	//	, m_fAttackPossibleTime(0.03333f*24.f)
	//	, m_fAttackRange(5.f)
	//	, m_fAttackDamage(5.f)
	//	, m_fDeathTimeCur(0.f)
	//	, m_fDeathTimeMax(0.03333f * 30.f)
	//	, m_fTicMax(0.016f)
	//	, m_fTic(0.f)
	//	, m_fToc(0.f)
	

	m_bDeathMode = false;
	m_fAttckAngle = 1.f;
	m_fAttackTimeMax = 2.999;
	m_fAttackTimeCur = m_fAttackTimeMax;
	m_fAttackPossibleTime = 0.03333f*38.f;
	m_fAttackRange = 15.f;
	m_fAttackDamage = 5.f;

	m_fDeathTimeCur = 0.f;
	m_fDeathTimeMax = 0.03333f * 30.f;

	m_fTicMax = 0.016f;
	m_fTic = 0.f;
	m_fToc = 0.f;


	//	HP,	
	//	MAX_HP,
	//	MP,
	//	MAX_MP,
	//	MASS,
	//	SEARCHRANGE,
	//	ATTACKRANGE,
	//	MINRANGE,
	//	ROTSPEED,
	//	MOVESPEED,
	//	SKILL_1_COOL_MAX,
	//	SKILL_1_COOL_NOW,
	//	SKILL_2_COOL_MAX,
	//	SKILL_2_COOL_NOW,
	//	SKILL_HEAL_COOL_MAX,
	//	SKILL_HEAL_COOL_NOW,
	//	ATTACK_BAGIC_COOL_NOW,
	//	ATTACK_BAGIC_COOL_MAX,
	//	
	//	
	CStatusComponent* s = GetObj()->StatusComponent();
	s->SetMimic(_pos,_rot);
	//s->SetFloat(STATUS_FLOAT::HP, 100.f);
	//s->SetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW, s->GetFloat(STATUS_FLOAT::SKILL_1_COOL_MAX));
	//s->SetFloat(STATUS_FLOAT::SKILL_2_COOL_NOW, s->GetFloat(STATUS_FLOAT::SKILL_2_COOL_MAX));
	//s->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW, s->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX));
}
void CMimicAi::OnCollision(CCollider2D * _pOther)
{
	Vec3 otherPos = _pOther->Transform()->GetLocalPos();
	Vec3 mePos = GetObj()->Transform()->GetLocalPos();

	Vec3 dir = (otherPos - mePos).Normalize();
	dir.y = 0.f;
	GetObj()->Transform()->SetLocalPos(mePos + (dir * -3.f * DT));
}