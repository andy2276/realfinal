#pragma once
class CGameObject;
class CSponSystem;
class CMapRapper;
namespace Trigger {
		class CShareHouse;
	//	결과 BT == Return
	enum class Result {
		 Wait = -1
		,Fail = 0
		,Success = 1
		,Evaluate = 2
	};
	//	기반 가상함수 BT == Virtual
	namespace Base {
		//	오브젝트 공용공간 BT == BlackBoard
		class CBasis{
		private:
			static UINT g_nId;
			UINT m_nId;
			UINT m_nIdx;
			wstring m_wstrName;
			std::list<CBasis*>* m_pStack;
			CShareHouse* m_pShareHouse;
			CGameObject* m_pGameObj;
			CSponSystem* m_pSponSystem;
			std::vector<CBasis*>* m_pVecNodes;
			CMapRapper* m_pMapRapper;

		public:
			CBasis();
			virtual ~CBasis();

			virtual Result Update(Trigger::Result _return) = 0;
			virtual void ResetIdx() { }
			virtual void AddIdx() { }
			virtual UINT GetIdx() { return m_nIdx; }
			virtual void AddChild(Base::CBasis* _node) {};

			void SetName(const wstring& _name) { m_wstrName = _name; }
			void SetIdx() { m_nIdx = m_pVecNodes->size(); }	//	자동으로 push하면 호출됨
			void SetStack(std::list< CBasis*>* _stack) { m_pStack = _stack; }
			void SetShareHouse(CShareHouse* _pShare) { m_pShareHouse = _pShare; }
			void SetGameObject(CGameObject* _pObj) { m_pGameObj = _pObj; }
			void SetSponSystem(CSponSystem* _pSpon) { m_pSponSystem = _pSpon; }
			void SetNodesVector(std::vector<CBasis*>* _pVector) { m_pVecNodes = _pVector; }
			void SetMapRapper(CMapRapper* _pMapRapper) { m_pMapRapper = _pMapRapper; }

			const wstring& GetName() { return m_wstrName; }
			std::list< CBasis*>* GetStack() { return m_pStack; }
			CShareHouse* GetShareHouse() { return m_pShareHouse; }
			CGameObject* GetGameObject() { return m_pGameObj; }
			CSponSystem* GetSponSystem() { return m_pSponSystem; }
			CMapRapper* GetMapRapper() { return m_pMapRapper; }

			//	주어진 인덱스에 해당하는 라운드의 리스폰타입을 찾아온다
			EC_RESPON_TYPE GetResponType(UINT _idx);
			//	현재 라운드의 리스폰 타입을 가져온다.
			EC_RESPON_TYPE GetResponType();

			//	스택기능
			void PushStack(Base::CBasis* _node) { m_pStack->push_back(_node); }
			void PushFrontStack(Base::CBasis* _node){ m_pStack->push_front(_node); }
			Base::CBasis* GetTopStack() { return m_pStack->back(); }
			Base::CBasis* GetFirstStack() { return m_pStack->front(); }
			void PopStack() { m_pStack->pop_back(); }

			//	자동으로 push하면 호출됨
			void PushVector(Trigger::Base::CBasis* _pTrigger) { SetIdx(); m_pVecNodes->push_back(_pTrigger); }
			std::vector<CBasis*>* GetTriggers() { return m_pVecNodes; }


			UINT GetId() { return m_nId; }
		};
		//	모음 BT == Composite
		class CControler : public CBasis {
		private:
			UINT	m_nCurChildIdx;
			vector<Trigger::Base::CBasis*> m_vecChild;
		public:
			CControler();
			virtual ~CControler();

			virtual Result Update(Trigger::Result _return) = 0;

			void ResetIdx() {
				m_nCurChildIdx = 0; 
				for (int i = 0; i < m_vecChild.size(); ++i) {
					if (m_vecChild[i] != nullptr)m_vecChild[i]->ResetIdx();
				}
			}
			void AddIdx() { ++m_nCurChildIdx; if (m_nCurChildIdx <= m_vecChild.size())m_nCurChildIdx = 0; }
			UINT GetCurIdx() { return m_nCurChildIdx; }
			void SetCurIdx(int _idx) { m_nCurChildIdx = _idx; }

			void AddChild(Base::CBasis* _node) { m_vecChild.push_back(_node); }
			Trigger::Base::CBasis* GetChild(int _idx) { return m_vecChild[_idx]; }
			vector<Trigger::Base::CBasis*>* GetChildren() { return &m_vecChild; }
		};
		class CQuery :public Base::CBasis {
		public:
			CQuery();
			virtual ~CQuery();
			virtual Result Update(Trigger::Result _return);
		};
		class CWork :public Base::CBasis {
		public:
			CWork();
			virtual ~CWork();

			virtual Result Update(Trigger::Result _return);
		};
	}
	//	분기점 BT == flow
	namespace Fork {
		//	연속 BT == Sequence
		class CProcedurer : public Base::CControler {
		public:
			CProcedurer();
			virtual ~CProcedurer();

			virtual Result Update(Trigger::Result _return);
		};
		//	선택 BT == Selection
		class CChooser : public Base::CControler {
		public:
			CChooser();
			virtual ~CChooser();

			virtual Result Update(Trigger::Result _return);
		};
	}
	//	행동 BT == Action
	namespace Event {
		
	}
	class CShareHouse {
	private:

	public:
		CShareHouse();
		~CShareHouse();

	};
}

