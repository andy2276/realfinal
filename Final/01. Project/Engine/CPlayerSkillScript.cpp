#include "stdafx.h"
#include "CPlayerSkillScript.h"
#include "MeshRender.h"
#include "ParticleSystem.h"
#include"StatusComponent.h"
#include"CSkillScript.h"
#include"BulletScript.h"
#include"MapMgr.h"
#include"FloorScript.h"
#include"PlayerScript.h"
CPlayerSkillScript::CPlayerSkillScript() : CScript((UINT)SCRIPT_TYPE::INVENTORYSCRIPT)
{
	snowOn = false;
	meteorOn = false;

	Bulletcount = 20; 

	//meteo04
	m_FireBalls.assign(Bulletcount, nullptr); //meteo04
	Ptr<CMeshData> pMeteo04 = CResMgr::GetInst()->LoadFBX(L"FBX\\meteo04.FBX");
	for (int i = 0; i < Bulletcount; ++i)
	{
		//m_FireBalls[i] = new CGameObject;
		//pMeteo04->Save(pMeteo04->GetPath());
		m_FireBalls[i] = pMeteo04->Instantiate();
		m_FireBalls[i]->SetName(L"Bullet");

		m_FireBalls[i]->Transform()->setPhysics(true); //true값이 움직일수 있게 하는것. 
		//m_FireBalls[i]->Transform()->setSkillPhysics(true); //true값이 움직일수 있게 하는것. 

		m_FireBalls[i]->Transform()->SetLocalPos(Vec3(0,0,0));
		Vec3 vScale_monster(1.f/7, 1.f/7, 1.f/7);
		m_FireBalls[i]->Transform()->SetLocalScale(vScale_monster);

		//m_FireBalls[i]->AddComponent(new CSkillScript((int)PLAYER_SKILL_TYPE::SKILL_FIREBALL));

		//충돌체 달아주기 
		m_FireBalls[i]->AddComponent(new CCollider2D);
		m_FireBalls[i]->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::SPHERE);
		m_FireBalls[i]->Collider2D()->SetOffsetScale(Vec3( 4.5f,4.5f,4.5f));

		m_FireBalls[i]->AddComponent(new CBulletScript((int)PLAYER_SKILL_TYPE::SKILL_FIREBALL));

		CreateObject(m_FireBalls[i], L"Bullet");
	}



	m_iceArrow.assign(Bulletcount, nullptr);
	Ptr<CMeshData> pIce = CResMgr::GetInst()->LoadFBX(L"FBX\\ice_errow13.FBX");
	for (int i = 0; i < Bulletcount; ++i)
	{
		//m_FireBalls[i] = new CGameObject;
	//	pMeteo04->Save(pMeteo04->GetPath());
		m_iceArrow[i] = pIce->Instantiate();
		m_iceArrow[i]->SetName(L"Bullet");

		m_iceArrow[i]->Transform()->setPhysics(false); //true값이 움직일수 있게 하는것. 
		m_iceArrow[i]->Transform()->SetLocalPos(Vec3(0, 0, 0));
	//	m_iceArrow[i]->Transform()->SetHasOffset((BYTE)EC_TRANS_OFFSET::ROTATE);
	//	m_iceArrow[i]->Transform()->SetLocalOffset(Vec3(XM_ANGLE * 90.f, 0.f, 0.f), Vec3(0.f, 0.f, 0.f));
		Vec3 vScale_monster(5.f, 5.f, 10.f);
		m_iceArrow[i]->Transform()->SetLocalScale(vScale_monster);
		
		//충돌체 달아주기 

		m_iceArrow[i]->AddComponent(new CCollider2D);
		m_iceArrow[i]->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::SPHERE);
		m_iceArrow[i]->Collider2D()->SetOffsetScale(Vec3(3.f,3.f,3.f));

		m_iceArrow[i]->AddComponent(new CBulletScript((int)PLAYER_SKILL_TYPE::SKILL_ICEARROW));

		CreateObject(m_iceArrow[i], L"Bullet");



	}

	//메테오
	//-----------------------------------------------------------------------------------------

	m_Meteor.assign(Bulletcount, nullptr);
	m_meteor_particle.assign(Bulletcount, nullptr);
//	Ptr<CMeshData> pMeteo04 = CResMgr::GetInst()->LoadFBX(L"FBX\\meteo04.FBX");
	for (int i = 0; i < Bulletcount; ++i)
	{
		//m_FireBalls[i] = new CGameObject;
	//	pMeteo04->Save(pMeteo04->GetPath());
		m_Meteor[i] = pMeteo04->Instantiate();
		m_Meteor[i]->SetName(L"Bullet");

		m_Meteor[i]->Transform()->setPhysics(true); //true값이 움직일수 있게 하는것. 
	
		m_Meteor[i]->Transform()->SetLocalPos(Vec3(0, 0, 0));
		
		Vec3 vScale_monster(0.15f, 0.15f, 0.15f);
		m_Meteor[i]->Transform()->SetLocalScale(vScale_monster);
		m_Meteor[i]->AddComponent(new CBulletScript((int)PLAYER_SKILL_TYPE::SKILL_METEOR));

		//충돌체 달아주기 
		m_Meteor[i]->AddComponent(new CCollider2D);
		m_Meteor[i]->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::SPHERE);
		m_Meteor[i]->Collider2D()->SetOffsetScale(vScale_monster * 20);

	//	m_Meteor[i]->AddComponent(new CBulletScript);

		//================================
		// 불 파티클 
		//================================
		{
			m_meteor_particle[i] = new CGameObject;
			m_meteor_particle[i]->SetName(L"Particle");
			m_meteor_particle[i]->AddComponent(new CTransform);
			m_meteor_particle[i]->AddComponent(new CParticleSystem((UINT)PARTICLE_TYPE::FIRE));

			m_meteor_particle[i]->FrustumCheck(false);
		//	m_meteor_particle->Transform()->SetLocalPos(Vec3(0.f, 50.f, 0.f));
			m_meteor_particle[i]->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_meteor_particle[i]->Particlesystem()->setParent(m_Meteor[i]);

		}


	}

	m_floorMeteor = new CGameObject;
	m_floorMeteor->SetName(L"Snow floor");
	m_floorMeteor->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	m_floorMeteor->AddComponent(new CTransform);
	m_floorMeteor->AddComponent(new CMeshRender);
	m_floorMeteor->AddComponent(new CBulletScript((int)PLAYER_SKILL_TYPE::SKILL_METEOR));
	m_floorMeteor->AddComponent(new FloorScript((int)PLAYER_SKILL_TYPE::SKILL_METEOR));

	m_floorMeteor->GetScript<CBulletScript>()->setHasFloor(true);
	m_attackRadius = 50;
	Vec3 vScale(m_attackRadius * 2, m_attackRadius * 2, 1.f);

	m_floorMeteor->Transform()->setPhysics(false);
	m_floorMeteor->Transform()->SetLocalScale(vScale);
	m_floorMeteor->Transform()->SetLocalRot(Vec3(XM_ANGLE * 270, 0, 0)); //x축 90도 회전 
	
	m_floorMeteor->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"CircleMesh"));
	Ptr<CMaterial> pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
	m_floorMeteor->MeshRender()->SetMaterial(pMtrl->Clone());
	Ptr<CTexture> pTexture = CResMgr::GetInst()->Load<CTexture>(L"meteorfloor", L"Texture\\meteorfloor.png");
	m_floorMeteor->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTexture.GetPointer());




	//눈보라
	//-----------------------------------------------------------------------------------------
	m_SnowStorm.assign(Bulletcount, nullptr);
	m_snow_particles.assign(Bulletcount, nullptr);
	m_snowblurEffect.assign(Bulletcount, nullptr);

	Ptr<CMeshData> pIceNani = CResMgr::GetInst()->LoadFBX(L"FBX\\ice_errow13.FBX");
	for (int i = 0; i < Bulletcount; ++i)
	{
		//m_FireBalls[i] = new CGameObject;
	//	pMeteo04->Save(pMeteo04->GetPath());
		m_SnowStorm[i] = pIceNani->Instantiate();
		m_SnowStorm[i]->SetName(L"Bullet");

		m_SnowStorm[i]->Transform()->setPhysics(false); //true값이 움직일수 있게 하는것. 
		m_SnowStorm[i]->Transform()->SetLocalPos(Vec3(0,0,0));
		m_SnowStorm[i]->Transform()->SetHasOffset((BYTE)EC_TRANS_OFFSET::ROTATE);
//		m_SnowStorm[i]->Transform()->SetLocalOffset(Vec3(XM_ANGLE *  130.f, 0.f, 0.f), Vec3(0.f, 0.f, XM_ANGLE * 90.f));

		m_SnowStorm[i]->AddComponent(new CBulletScript((int)PLAYER_SKILL_TYPE::SKILL_SNOWSTORM));

		

		Vec3 vScale_monster(2.f, 1.f, 1.f);
		m_SnowStorm[i]->Transform()->SetLocalScale(vScale_monster);
		//충돌체 달아주기 
		m_SnowStorm[i]->AddComponent(new CCollider2D);
		m_SnowStorm[i]->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::SPHERE);
		m_SnowStorm[i]->Collider2D()->SetOffsetScale(vScale_monster * 20);

		//CreateObject(m_SnowStorm[i], L"Bullet");
		//================================
		// 눈꽃 파티클 
		//================================
		{
			//눈보라 생성 지점을 넘겨주고 , 생성될 크기도 넘겨준다. 
			//그 크기 안에서 눈보라들이 흩날리는것을 보여주기 위함 



			m_snow_particles[i] = new CGameObject;
			m_snow_particles[i]->SetName(L"Particle");
			m_snow_particles[i]->AddComponent(new CTransform);
			m_snow_particles[i]->AddComponent(new CParticleSystem((UINT)PARTICLE_TYPE::SNOW));


		
			m_snow_particles[i]->FrustumCheck(false);
			m_snow_particles[i]->Transform()->SetLocalPos(Vec3(0.f, 50.f, 0.f));
			m_snow_particles[i]->Transform()->SetLocalScale(Vec3(100.f, 300.f, 1.f));
			m_snow_particles[i]->Particlesystem()->setParent(m_SnowStorm[i]);

		}

		//snow blur ----------------------------------------------------------------
	
	
			m_snowblurEffect[i] = new CGameObject;
			m_snowblurEffect[i]->SetName(L"Snow blur effect");

			m_snowblurEffect[i]->AddComponent(new CTransform);
			m_snowblurEffect[i]->AddComponent(new CParticleSystem((UINT)PARTICLE_TYPE::BLUR));



			m_snowblurEffect[i]->FrustumCheck(false);
			m_snowblurEffect[i]->Transform()->SetLocalPos(Vec3(0.f, 50.f, 0.f));
			m_snowblurEffect[i]->Transform()->SetLocalScale(Vec3(100.f, 300.f, 1.f));
			m_snowblurEffect[i]->Particlesystem()->setParent(m_SnowStorm[i]);
	

	}
	m_floorSnow = new CGameObject;
	m_floorSnow->SetName(L"Snow floor");
	m_floorSnow->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	m_floorSnow->AddComponent(new CTransform);
	m_floorSnow->AddComponent(new CMeshRender);
	m_floorSnow->AddComponent(new CBulletScript((int)PLAYER_SKILL_TYPE::SKILL_SNOWSTORM));
	m_floorSnow->AddComponent(new FloorScript((int)PLAYER_SKILL_TYPE::SKILL_SNOWSTORM));

	m_floorSnow->GetScript<CBulletScript>()->setHasFloor(true);
	m_attackRadius = 50;
	Vec3 vfloorSnowScale(m_attackRadius * 2, m_attackRadius * 2, 1.f);

	m_floorSnow->Transform()->setPhysics(false);
	m_floorSnow->Transform()->SetLocalScale(vfloorSnowScale);
	m_floorSnow->Transform()->SetLocalRot(Vec3(XM_ANGLE * 90, 0, 0)); //x축 90도 회전 

	m_floorSnow->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"CircleMesh"));
	pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
	m_floorSnow->MeshRender()->SetMaterial(pMtrl->Clone());
	pTexture = CResMgr::GetInst()->Load<CTexture>(L"snowFloor", L"Texture\\ice0.png");
	m_floorSnow->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTexture.GetPointer());

	//-----------------------------------------------------------------------
	// gold skill object
	//-----------------------------------------------------------------------

	m_GoldSkill = new CGameObject;
	m_GoldSkill->SetName(L"Gold Skill");

	m_GoldSkill->AddComponent(new CTransform);
	m_GoldSkill->AddComponent(new CBulletScript((int)PLAYER_SKILL_TYPE::SKILL_GOLD));

	//-----------------------------------------------------------------------
	//-----------------------------------------------------------------------

	Snowcount = 0;
	Firecount = 0;
	Meteorcount = 0; 
	icecount = 0;

	m_bOnce = false;

	m_bOnGold = false;
}


CPlayerSkillScript::~CPlayerSkillScript()
{
}

void CPlayerSkillScript::awake()
{
}

void CPlayerSkillScript::shoot(UINT u )
{
	//키를 입력하면 총알이 바라보고 있는 방향으로 날아간다. 

	Vec3 vpos = GetObj()->Transform()->GetLocalPos();
	Vec3 vdirFront = GetObj()->Transform()->GetLocalDir(DIR_TYPE::FRONT);
	Vec3 vdirRight = GetObj()->Transform()->GetLocalDir(DIR_TYPE::RIGHT);
	float famount = 200;

	switch (u)
	{
	case (UINT)PLAYER_SKILL_TYPE::SKILL_FIREBALL:

		//	불렛들 포물선 그리면서 날아가게 하기 

		//이미 날아간 파이어볼이 삭제되었거나 발사되었을경우에만 새롭게 발사가 가능하도록 한다 
		if (Firecount == 0)
		{
			vpos.y += 7;
			m_FireBalls[Firecount]->Transform()->SetLocalPos(vpos);



			m_FireBalls[Firecount]->SetActive(true);
			CreateObject(m_FireBalls[Firecount], L"Bullet");

			Firecount++;
			if (Firecount >= Bulletcount - 1)
			{
				Firecount = 1;
			}
		}
		else if (m_FireBalls[Firecount-1]->GetScript<CBulletScript>()->isCanShoot() == true || m_FireBalls[Firecount-1]->IsActive() == false)
		{
			vpos.y += 7;
			m_FireBalls[Firecount]->Transform()->SetLocalPos(vpos);


			m_FireBalls[Firecount]->SetActive(true);
			CreateObject(m_FireBalls[Firecount], L"Bullet");

			Firecount++;
			if (Firecount >= Bulletcount - 1)
			{
				Firecount = 1;
			}
		}

		m_FireBalls[Firecount]->GetScript<CBulletScript>()->shoot();
		break;
	case (UINT)PLAYER_SKILL_TYPE::SKILL_ICEARROW:
	{
		vpos.y += 10;

		m_icePrePos[icecount] = vpos;
		m_iceArrow[icecount]->Transform()->SetLocalPos(vpos);
		
		//플레이어의 회전각도받아와서 넣어주기
		Vec3 vRot =	Transform()->GetLocalRot();
		m_iceArrow[icecount]->Transform()->SetLocalRot(vRot);


		m_iceArrow[icecount]->GetScript<CBulletScript>()->shoot();
		m_iceArrow[icecount]->SetActive(true);
		CreateObject(m_iceArrow[icecount], L"Bullet");

		icecount++;
		if (icecount >= Bulletcount - 1)
		{
			icecount = 1;
		}
		break;


	}
	case (UINT)PLAYER_SKILL_TYPE::SKILL_METEOR:

		if (Meteorcount >= 8)
		{
			Meteorcount = 0;
		}
		if (!m_floorMeteor->IsActive())
		{

				m_floorMeteor->SetActive(true);
				m_floorMeteor->GetScript<FloorScript>()->setShoot(true);

		}

		Meteorcount+=8;

		
		break;
	case (UINT)PLAYER_SKILL_TYPE::SKILL_SNOWSTORM:
	{	m_attackRadius = 25;



		if (!m_floorSnow->IsActive())
		{
			m_floorSnow->SetActive(true);
			m_floorSnow->GetScript<FloorScript>()->setShoot(true);

		}
	}
	break;

	case (UINT)PLAYER_SKILL_TYPE::SKILL_GOLD:

		m_GoldSkill->GetScript<CBulletScript>()->shoot();
		GetObj()->GetScript<CPlayerScript>()->setGoldMaterial();
		m_bOnGold = true;
		m_GoldSkill->SetActive(true);
			break;
	default:
		break;
	}
}

void CPlayerSkillScript::update()
{
	//업데이트 될때마다 파이어볼의 위치값을 더해주기. 
	//어디까지 날아갈것인지? -> 그냥 y가 0이 되면 사라지게 하기. 

	if (!m_bOnce)
	{
		CreateObject(m_floorMeteor, L"Bullet");
		CreateObject(m_floorSnow, L"Bullet");
		CreateObject(m_GoldSkill, L"notInfluenceAnything");


		m_floorSnow->SetActive(false);
		m_floorMeteor->SetActive(false);

		for (int i = 0; i < 20; i++)
		{
			CreateObject(m_Meteor[i], L"Bullet");
			CreateObject(m_meteor_particle[i], L"Particle");

			CreateObject(m_snowblurEffect[i], L"notInfluenceAnything");
			CreateObject(m_SnowStorm[i], L"Bullet");
			CreateObject(m_snow_particles[i], L"Particle");



			m_SnowStorm[i]->SetActive(false);
			m_snow_particles[i]->SetActive(false);
			m_Meteor[i]->SetActive(false);
			m_meteor_particle[i]->SetActive(false);
			m_snowblurEffect[i]->SetActive(false);
			//CreateObject(m_Meteor[i], L"Bullet");
			//CreateObject(m_meteor_particle[i], L"Particle");

			//CreateObject(m_SnowStorm[i], L"Bullet");
			//CreateObject(m_snow_particles[i], L"Particle");

			//m_SnowStorm[i]->SetActive(false);
			//m_snow_particles[i]->SetActive(false);
			//m_Meteor[i]->SetActive(false);
			//m_meteor_particle[i]->SetActive(false);

			m_Meteor[i]->GetScript<CBulletScript>()->setIdx(i);
		}

		m_PlayerMtrl = GetObj()->MeshRender()->GetSharedMaterial();
		m_bOnce = true;
	}

	Vec3 vpos = Transform()->GetLocalPos();
	Vec3 vdirFront = GetObj()->Transform()->GetLocalDir(DIR_TYPE::FRONT);
	Vec3 vdirRight = GetObj()->Transform()->GetLocalDir(DIR_TYPE::RIGHT);
	float famount = 200;

	if (m_floorMeteor->IsActive())
	{
		if (m_floorMeteor->GetScript<FloorScript>()->getClick())
		{
			Vec3 tempos = m_floorMeteor->GetScript<FloorScript>()->getPos();

			for (int i = Meteorcount; i < Meteorcount+8; ++i)
			{

				m_Meteor[i]->SetActive(true);

				m_Meteor[i]->GetScript<CBulletScript>()->setPos(tempos);
				m_Meteor[i]->GetScript<CBulletScript>()->shoot();			

				m_meteor_particle[i]->SetActive(true);
				Vec3 vec = CMapMgr::GetInst()->GetPlayerObj()->Transform()->GetLocalPos();
				vec.y += 25;
			//	tempos.y -= 50;
			//	meteorOn(vec);
				meteorOn = true;
			}
			m_floorMeteor->GetScript<FloorScript>()->setClick(false);

		}
	}
	else {
		if (meteorOn)
		{
			for (int i = Meteorcount; i < Meteorcount + 8; ++i)
			{
				m_meteor_particle[i]->SetActive(false);
			}
			meteorOn = false;
		}
	}

	if (m_floorSnow->IsActive())
	{
		if (m_floorSnow->GetScript<FloorScript>()->getClick())
		{//위치 설정이 끝나고 난 후부터 눈보라 스킬 이동시작 

			Vec3 tempos = m_floorSnow->GetScript<FloorScript>()->getPos();
			
			for (int i = 0; i < 20; ++i)
			{

				m_snowblurEffect[i]->SetActive(true);
				m_SnowStorm[i]->SetActive(true);
				m_snow_particles[i]->SetActive(true);
				m_SnowStorm[i]->GetScript<CBulletScript>()->setPos(tempos);
				Vec3 vec = CMapMgr::GetInst()->GetPlayerObj()->Transform()->GetLocalPos();
				vec.y += 15;
			//	tempos.y += 100;
			//	snowOn(vec);
				m_SnowStorm[i]->GetScript<CBulletScript>()->shoot();

				snowOn = true;
			}
			m_floorSnow->GetScript<FloorScript>()->setClick(false);
			
			//Ptr<CSound> pSound = CResMgr::GetInst()->Load<CSound>(L"29_arrowDamage", L"Sound\\29_arrowDamage.mp3");
			//pSound->Play(3, true);
		}
	}
	else {
		if (snowOn)
		{
			for (int i = 0; i < 20; ++i)
			{
				m_snow_particles[i]->SetActive(false);
				m_snowblurEffect[i]->SetActive(false);
			}
			snowOn = false;
		}
	}

	if (m_bOnGold)
	{
		if (!m_GoldSkill->IsActive())
		{
			m_bOnGold = false;
			GetObj()->MeshRender()->SetMaterial(m_PlayerMtrl);
		}
	}
}

void CPlayerSkillScript::OnCollisionEnter(CCollider2D * _pOther)
{
	if (_pOther->GetObj()->GetName() == L"Monster")
	{

	}
}

