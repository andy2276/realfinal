#include "stdafx.h"
#include "BTAI.h"

#include "BT.h"
#include "BTInit.h"


#include "StatusComponent.h"

#include "SkillPlatform.h"

CBTAI::CBTAI()
	:CScript((UINT)SCRIPT_TYPE::AI)
	, m_bAIUpdate(false)
{
}


CBTAI::~CBTAI()
{
}

void CBTAI::Init(BT::Tool::CBlackBoard* _black,CGameObject* _pObj, UINT _option)
{
	//	blackBoard는 외부에서 작동
	//	blackBoard만 작동시키는 오브젝트를 만들어야함

	m_pBlackBoard = _black;
}
//	맵스크립트에서 UpdateBlackBoard 이거를 매번불러줘야하고 여기 AI에서 시간을 업데이트 해줘야한다!


void CBTAI::Test(BT::Tool::CBlackBoard* _black)
{
	using namespace BT;

	Tool::CBTMaker m;
	CSkillPlatform* skill = new CSkillPlatform;
	GetObj()->AddComponent(skill);
	m.Init(GetObj(), _black);
	_black->UpdateBlackBoard();
	m_pBlackBoard = _black;
	int sel = 0;
	int seq = 1;

	float hp[3] = { 50.f,100.f,300.f };
	float rageScale = 1.5f;

	float bagicAttackRange = 15.f;
	int bagicCoolMax = (int)STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX;
	int bagicCoolNow = (int)STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW;
	float bagicAttackTime = ANIMFRAME(5);
	float bagicNear = bagicAttackRange;
	float bagicStay = 2.5f;

	float skill2Range = 100.f;
	int skill2CoolMax = (int)STATUS_FLOAT::SKILL_2_COOL_MAX;
	int skill2CoolNow = (int)STATUS_FLOAT::SKILL_2_COOL_NOW;
	int skill2Num = 0;
	float skill2ShootTime = ANIMFRAME(7);
	float skill2Stay = 4.f;

	float Skill1Range = 200.f;
	int skill1CoolMax = (int)STATUS_FLOAT::SKILL_1_COOL_MAX;
	int skill1CoolNow = (int)STATUS_FLOAT::SKILL_1_COOL_NOW;
	int skill1Num = 2;
	float skill1ShootTime = ANIMFRAME(7);
	float skill1Stay = 3.f;
	float skill1rageStay = 1.5f;
	

	DIR_TYPE rageGo = DIR_TYPE::FRONT;
	float rageGoDist = 10.f;

	m.NeoRoot<Clt::CSelection>(L"selRoot")
		//	죽었는가 확인(만들어야함)
		->Parent(L"selRoot")->NeoChild<Clt::CReDecorate>(L"dcrIsDeath")->SetData(0, &seq)
		->a____()->Parent(L"dcrIsDeath")->NeoChild<Cdt::IsDeath>(L"cdtIsDeath")
		->a____()->Parent(L"dcrIsDeath")->NeoChild<Act::SetDeath>(L"actDeath")
		//	데미지 확인 및 계산
		->Parent(L"selRoot")->NeoChild<Clt::CReDecorate>(L"dcrIsDamage")->SetData(0, &seq)
		->a____()->Parent(L"dcrIsDamage")->NeoChild<Cdt::IsDamaged>(L"cdtDamaged")
		->a____()->Parent(L"dcrIsDamage")->NeoChild<Act::Damaged>(L"actDamaged")
		//	체력확인
		->Parent(L"selRoot")->NeoChild<Clt::CSelection>(L"selWhatHp")
		//		체력이 50이하면
		->a____()->Parent(L"selWhatHp")->NeoChild<Clt::CSequence>(L"seqLessHp50")
		->a____()->a____()->Parent(L"seqLessHp50")->NeoChild<Cdt::HpLess>(L"cdtHpLess50")->SetData(0, &hp[0])
		->a____()->a____()->Parent(L"seqLessHp50")->NeoChild<Clt::CSelection>(L"selRageAttack")
		//							처음 들어왔다면 분노
		->a____()->a____()->a____()->Parent(L"selRageAttack")->NeoChild<Clt::CSequence>(L"seqIsFirst")
		->a____()->a____()->a____()->a____()->Parent(L"seqIsFirst")->NeoChild<Cdt::FirstIn>(L"cdtIsFirstIn")
		->a____()->a____()->a____()->a____()->Parent(L"seqIsFirst")->NeoChild<Act::SetRageState>(L"actRageState")->SetData(0, &rageScale)
		//							근접할땐 근접어택
		->a____()->a____()->a____()->Parent(L"selRageAttack")->NeoChild<Clt::CSequence>(L"seqRageBagicAttack")
		->a____()->a____()->a____()->a____()->Parent(L"seqRageBagicAttack")->NeoChild<Cdt::IsLessRange>(L"cdtRageBagicAttackRange")->SetData(0, &bagicAttackRange)
		->a____()->a____()->a____()->a____()->Parent(L"seqRageBagicAttack")->NeoChild<Cdt::IsSkillOn>(L"cltIsRageBagicAttack")->SetData(0, &bagicCoolMax)->SetData(1, &bagicCoolNow)
		->a____()->a____()->a____()->a____()->Parent(L"seqRageBagicAttack")->NeoChild<Act::BagicAttack>(L"actRageBagicAttack")->SetData(0, &bagicAttackRange)->SetData(1, &bagicAttackTime)
		//							스킬사용
		->a____()->a____()->a____()->Parent(L"selRageAttack")->NeoChild<Clt::CSequence>(L"seqRageSkil2Range")
		->a____()->a____()->a____()->a____()->Parent(L"seqRageSkil2Range")->NeoChild<Cdt::IsLessRange>(L"cdtSkill2Rage")->SetData(0, &skill2Range)
		->a____()->a____()->a____()->a____()->Parent(L"seqRageSkil2Range")->NeoChild<Clt::CSelection>(L"selSkillPossible")
		//											가장 근접 스킬2사용
		->a____()->a____()->a____()->a____()->a____()->Parent(L"selSkillPossible")->NeoChild<Clt::CSequence>(L"seqSkill2Possible")
		->a____()->a____()->a____()->a____()->a____()->a____()->Parent(L"seqSkill2Possible")->NeoChild<Cdt::IsSkillOn>(L"cdtRegeIsSkill2On")->SetData(0, &skill2CoolMax)->SetData(1, &skill2CoolNow)
		->a____()->a____()->a____()->a____()->a____()->a____()->Parent(L"seqSkill2Possible")->NeoChild<Act::ShootSkill>(L"actRageShootSkill2")->SetData(0, &skill2Num)->SetData(1, &skill2ShootTime)
		->a____()->a____()->a____()->a____()->a____()->a____()->Parent(L"seqSkill2Possible")->NeoChild<Act::Stay>(L"actRageShootSkill2Stay")->SetData(0, &skill2Num)->SetData(1, &skill2ShootTime)
		//											스킬 사용1
		->a____()->a____()->a____()->a____()->a____()->Parent(L"selSkillPossible")->NeoChild<Clt::CSequence>(L"seqRageSkill1Possible")
		->a____()->a____()->a____()->a____()->a____()->a____()->Parent(L"seqRageSkill1Possible")->NeoChild<Cdt::IsSkillOn>(L"cdtRegeIsSkill1On")->SetData(0, &skill1CoolMax)->SetData(1, &skill1CoolNow)
		->a____()->a____()->a____()->a____()->a____()->a____()->Parent(L"seqRageSkill1Possible")->NeoChild<Act::ShootSkill>(L"actRageShootSkill1")->SetData(0, &skill1Num)->SetData(1, &skill1ShootTime)
		->a____()->a____()->a____()->a____()->a____()->a____()->Parent(L"seqRageSkill1Possible")->NeoChild<Act::Stay>(L"actRageShootSkill1Stay")->SetData(0, &skill1rageStay)
		//											아무것도 못할땐 이동
		->a____()->a____()->a____()->a____()->a____()->Parent(L"selSkillPossible")->NeoChild<Clt::CSequence>(L"seqRageBagicPossible")
		->a____()->a____()->a____()->a____()->a____()->a____()->Parent(L"seqRageBagicPossible")->NeoChild<Act::MoveDest>(L"actRageMoveDest")->SetData(0, &bagicNear)
		//							스킬 1사용 가능?
		->a____()->a____()->a____()->Parent(L"selRageAttack")->NeoChild<Clt::CSequence>(L"seqRageSkil1Range")
		->a____()->a____()->a____()->a____()->Parent(L"seqRageSkil1Range")->NeoChild<Cdt::IsLessRange>(L"cdtSkill1Rage")->SetData(0, &Skill1Range)
		->a____()->a____()->a____()->a____()->Parent(L"seqRageSkil1Range")->NeoChild<Cdt::IsSkillOn>(L"cdtSkill1RageOn")->SetData(0, &skill1CoolMax)->SetData(1, &skill1CoolNow)
		->a____()->a____()->a____()->a____()->Parent(L"seqRageSkil1Range")->NeoChild<Act::ShootSkill>(L"cdtSkill1RageShoot")->SetData(0, &skill1Num)->SetData(1, &skill1ShootTime)
		->a____()->a____()->a____()->Parent(L"selRageAttack")->NeoChild<Act::TurnDest>(L"actRageTurnRageDest")
		->a____()->a____()->a____()->Parent(L"selRageAttack")->NeoChild<Act::MoveDirDist>(L"actRageMoveDest")->SetData(0, &rageGo)->SetData(1, &rageGoDist)

		->a____()->Parent(L"selWhatHp")->NeoChild<Clt::CSequence>(L"seqLessHp100")
		->a____()->a____()->Parent(L"seqLessHp100")->NeoChild<Clt::CSelection>(L"selHp100Attack")
		->a____()->a____()->a____()->Parent(L"selHp100Attack")->NeoChild<Clt::CSequence>(L"seqBagicAttack")
		->a____()->a____()->a____()->a____()->Parent(L"seqBagicAttack")->NeoChild<Cdt::IsLessRange>(L"cdtBagicAttackRange")->SetData(0, &bagicAttackRange)
		->a____()->a____()->a____()->a____()->Parent(L"seqBagicAttack")->NeoChild<Cdt::IsSkillOn>(L"cdtBagicAttackOn")->SetData(0, &bagicCoolMax)->SetData(1, &bagicCoolNow)
		->a____()->a____()->a____()->a____()->Parent(L"seqBagicAttack")->NeoChild<Act::BagicAttack>(L"actBagicAttackGo")->SetData(0, &bagicAttackRange)->SetData(1, &bagicAttackTime)
		->a____()->a____()->a____()->a____()->Parent(L"seqBagicAttack")->NeoChild<Act::Stay>(L"actBagicAttackGoStay")->SetData(0, &bagicStay)
		->a____()->a____()->a____()->Parent(L"selHp100Attack")->NeoChild<Clt::CSequence>(L"seqSkill1")
		->a____()->a____()->a____()->a____()->Parent(L"seqSkill1")->NeoChild<Cdt::IsLessRange>(L"cdtRangeSkill1")->SetData(0, &Skill1Range)
		->a____()->a____()->a____()->a____()->Parent(L"seqSkill1")->NeoChild<Cdt::IsSkillOn>(L"cdtSkill1On")->SetData(0, &skill1CoolMax)->SetData(1, &skill1CoolNow)
		->a____()->a____()->a____()->a____()->Parent(L"seqSkill1")->NeoChild<Act::BagicAttack>(L"actSkill1Go")->SetData(0, &skill1Num)->SetData(1, &skill1ShootTime)
		->a____()->a____()->a____()->a____()->Parent(L"seqSkill1")->NeoChild<Act::Stay>(L"actSkill1GoStay")->SetData(0, &skill1Stay)

		->a____()->a____()->a____()->Parent(L"selHp100Attack")->NeoChild<Clt::CSequence>(L"seqMovePlayer")
		->a____()->a____()->a____()->a____()->Parent(L"seqMovePlayer")->NeoChild<Act::MoveDest>(L"actToPlayer")->SetData(0, &bagicNear)
		//->a____()->a____()->a____()->a____()->Parent(L"seqMovePlayer")->NeoChild<Act::TurnDest>(L"actTurnPlayer")
		//->a____()->a____()->a____()->a____()->Parent(L"seqMovePlayer")->NeoChild<Act::MoveDirDist>(L"actMoveDest")->SetData(0, &rageGo)->SetData(1, &rageGoDist)
		->a____()->a____()->a____()->a____()->Parent(L"seqMovePlayer")->NeoChild<Act::Stay>(L"actMoveDestStay")
		->Final();

	m_pRoot = m.GetRoot();
	m_pAiProperty = m.GetProperty();
}

void CBTAI::Test2(BT::Tool::CBlackBoard * _black)
{
	using namespace BT;

	Tool::CBTMaker m;
	CSkillPlatform* skill = new CSkillPlatform;
	GetObj()->AddComponent(skill);
	m.Init(GetObj(), _black);
	_black->UpdateBlackBoard();
	m_pBlackBoard = _black;
	int sel = 0;
	int seq = 1;

	float hp[3] = { 50.f,100.f,300.f };
	float range[3] = { 100.f,200.f,300.f };
	float rageScale = 1.5f;

	float bagicAttackRange = 15.f;
	int bagicCoolMax = (int)STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX;
	int bagicCoolNow = (int)STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW;
	float bagicAttackTime = ANIMFRAME(5);
	float bagicNear = bagicAttackRange;
	float bagicStay = 2.5f;
	float bagicRageStay = 1.2f;

	float skill2Range = 100.f;
	int skill2CoolMax = (int)STATUS_FLOAT::SKILL_2_COOL_MAX;
	int skill2CoolNow = (int)STATUS_FLOAT::SKILL_2_COOL_NOW;
	int skill2Num = 0;
	float skill2ShootTime = ANIMFRAME(7);
	float skill2Stay = 4.f;

	float Skill1Range = 200.f;
	int skill1CoolMax = (int)STATUS_FLOAT::SKILL_1_COOL_MAX;
	int skill1CoolNow = (int)STATUS_FLOAT::SKILL_1_COOL_NOW;
	int skill1Num = 2;
	float skill1ShootTime = ANIMFRAME(7);
	float skill1Stay = 3.f;
	float skill1rageStay = 1.5f;


	DIR_TYPE rageGo = DIR_TYPE::FRONT;
	float rageGoDist = 10.f;

	//	중요한게 타겟을 고정시키면 안된다는 것이다! 물론여기서는 타켓은 무조건 플레이어
	m.NeoRoot<Clt::CSelection>(L"selRoot")
		->Parent(L"selRoot")
		->NeoChild<Clt::CSequence>(L"dcrIsDeath")
		->Parent(L"dcrIsDeath")
		->a____()->NeoChild<Cdt::IsDeath>(L"cdtIsDeath")
		->a____()->NeoChild<Act::SetDeath>(L"actDeath")
		->Parent(L"selRoot")
		->NeoChild<Clt::CSequence>(L"dcrIsDamage")
		->Parent(L"dcrIsDamage")
		->a____()->NeoChild<Cdt::IsDamaged>(L"cdtIsDamage")
		->a____()->NeoChild<Act::Damaged>(L"actDamage")
		->Parent(L"selRoot")
		->NeoChild<Clt::CSelection>(L"selHpTest")
		->a____()->Parent(L"selHpTest")
		->a____()->NeoChild<Clt::CSequence>(L"seqLess50")
		->a____()->a____()->Parent(L"seqLess50")
		->a____()->a____()->NeoChild<Cdt::HpLess>(L"cdtLess50")
		->a____()->a____()->NeoChild<Clt::CSelection>(L"selIsFirst")
		->a____()->a____()->a____()->Parent(L"selIsFirst")
		->a____()->a____()->a____()->NeoChild<Clt::CSequence>(L"seqIsFirstCheck")
		->a____()->a____()->a____()->a____()->Parent(L"seqIsFirstCheck")
		->a____()->a____()->a____()->a____()->NeoChild<Cdt::FirstIn>(L"cdtIsFirst")
		->a____()->a____()->a____()->a____()->NeoChild<Act::SetRageState>(L"actRageState")->SetData(0, &rageScale)
		->a____()->a____()->a____()->Parent(L"selIsFirst")
		->a____()->a____()->a____()->NeoChild<Act::ReValue>(L"actFirstAlwaysTrue")
		->a____()->a____()->Parent(L"seqLess50")
		->a____()->a____()->NeoChild<Clt::CSequence>(L"seqRageBagicAttack")
		->a____()->a____()->a____()->Parent(L"seqRageBagicAttack")
		->a____()->a____()->a____()->NeoChild<Cdt::IsLessRange>(L"cdtRageIsInPlayer")->SetData(0, &range[0])
		->a____()->a____()->a____()->NeoChild<Act::TurnDest>(L"actRageTurnToDest")
		->a____()->a____()->a____()->NeoChild<Act::GoDest>(L"actRageGoDest")
		->a____()->a____()->a____()->NeoChild<Cdt::IsSkillOn>(L"cdtRageBagicAttackOn")->SetData(0, &bagicCoolMax)->SetData(1, &bagicCoolNow)
		->a____()->a____()->a____()->NeoChild<Act::BagicAttack>(L"actRageBagicAttack")->SetData(0, &bagicAttackRange)->SetData(1, &bagicAttackTime)
		->a____()->a____()->a____()->NeoChild<Act::Stay>(L"actRageBagicStay")->SetData(0, &bagicRageStay)
		->a____()->a____()->Parent(L"seqLess50")
		->a____()->a____()->NeoChild<Clt::CSequence>(L"seqRageSkill1")
		->a____()->a____()->a____()->Parent(L"seqRageSkill1")
		->a____()->a____()->a____()->NeoChild<Cdt::IsLessRange>(L"cdtRageIsInPlayer")->SetData(0, &range[1])
		->a____()->a____()->a____()->NeoChild<Act::TurnDest>(L"actRageTurnToDest")
		->a____()->a____()->a____()->NeoChild<Act::GoDest>(L"actRageGoDest")
		->a____()->a____()->a____()->NeoChild<Cdt::IsSkillOn>(L"cdtBagicAttackOn")->SetData(0, &bagicCoolMax)->SetData(1, &bagicCoolNow)
		->a____()->a____()->a____()->NeoChild<Act::BagicAttack>(L"actRageBagicAttack")->SetData(0, &bagicAttackRange)->SetData(1, &bagicAttackTime)
		->a____()->a____()->a____()->NeoChild<Act::Stay>(L"actRageBagicStay")->SetData(0, &bagicRageStay)
		->a____()->Parent(L"selHpTest")
		->a____()->NeoChild<Clt::CSequence>(L"seqLess100")
		->a____()->a____()->Parent(L"seqLess100")
		->a____()->a____()->NeoChild<Cdt::IsLessRange>(L"cdtIsInPlayer")->SetData(0, &range[0])
		->a____()->a____()->NeoChild<Act::TurnDest>(L"actTurnToDest")
		->a____()->a____()->NeoChild<Act::GoDest>(L"actGoDest")
		->a____()->a____()->NeoChild<Cdt::IsSkillOn>(L"cdtBagicAttackOn")->SetData(0, &bagicCoolMax)->SetData(1, &bagicCoolNow)
		->a____()->a____()->NeoChild<Act::BagicAttack>(L"actBagicAttack")->SetData(0, &bagicAttackRange)->SetData(1, &bagicAttackTime)
		->a____()->a____()->NeoChild<Act::Stay>(L"actBagicStay")->SetData(0, &bagicRageStay)
		->Final();

	m_pRoot = m.GetRoot();
	m_pAiProperty = m.GetProperty();
}

void CBTAI::OnCollision(CCollider2D * _pOther)
{
	m_bAIUpdate = true;
}

void CBTAI::update()
{
	if (!m_bAIUpdate && m_fTic < m_fTicMax)
	{
		m_fTic += DT;
		m_fToc = m_fTic;
		return;
	}
	else
	{
		m_fToc = m_fTic;
		m_fTic = 0.0f;
	}

	if (m_bAIUpdate)m_fToc = DT;

	m_bAIUpdate = false;
	m_pAiProperty->SetToc(m_fToc);
	BT::Return re = BT::Return::Evaluation;
	//	시간 업데이트
	
	Status()->UpdateCoolTime(m_fToc);
	Status()->SetVec3(STATUS_VEC3::TARGET_POS, m_pBlackBoard->pPlayer->Transform()->GetLocalPos());

	re = m_pRoot->Evaluate();
	switch (re)
	{
	case BT::Return::Evaluation:
	case BT::Return::Failure:
	case BT::Return::Succeess:
		m_pAiProperty->StatusReset();
		break;
	case BT::Return::Running:
		m_bAIUpdate = true;
		break;
	default:
		break;
	}


}
