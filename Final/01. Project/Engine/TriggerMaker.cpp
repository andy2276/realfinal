#include "stdafx.h"
#include "TriggerMaker.h"
#include "SponSystem.h"
#include "GameObject.h"


CTriggerMaker::CTriggerMaker()
	:	m_pRoot(nullptr)
	,	m_pGameObj(nullptr)
	,	m_pShareHouse(nullptr)
	,	m_pStack(nullptr)
	,	m_pTriggers(nullptr)
	,	m_pSponSystem(nullptr)
{
}
CTriggerMaker::~CTriggerMaker()
{
	m_pRoot = nullptr;
	m_pGameObj = nullptr;
	m_pShareHouse = nullptr;
	m_pStack = nullptr;
	m_pTriggers = nullptr;
	m_pSponSystem = nullptr;
}

void CTriggerMaker::Init(CGameObject * _pObj, Trigger::CShareHouse * _pShare, std::list<Trigger::Base::CBasis*>* _pList, std::vector<Trigger::Base::CBasis*>* _pNode, CMapRapper* _pMap)
{
	m_pGameObj = _pObj;
	m_pShareHouse = _pShare;
	m_pStack = _pList;
	m_pTriggers = _pNode;
	m_pSponSystem = _pObj->GetScript<CSponSystem>();
	m_pMapRapper = _pMap;
}

Trigger::Fork::CProcedurer * CTriggerMaker::Sequence(const std::wstring & _name)
{
	Trigger::Fork::CProcedurer * pTemp = BagicSetting<Trigger::Fork::CProcedurer>(_name);
	return pTemp;
}

Trigger::Fork::CChooser * CTriggerMaker::Selection(const std::wstring & _name)
{
	Trigger::Fork::CChooser * pTemp = BagicSetting<Trigger::Fork::CChooser>(_name);
	return pTemp;
}

Trigger::Base::CBasis * CTriggerMaker::Trigger_Bagic()
{
	Trigger::Base::CBasis* p[15] = {};
	m_pRoot = Selection(L"SelRoot"); 
	{
		p[0] = Sequence(L"SeqIsLevelEnd?");
		{
			m_pRoot->AddChild(p[0]);

			p[1] = Condition<Trigger::Event::Special::Queries::CIfLevelEnd>(L"CdtIsLevelEnd?"); 
			{
				p[0]->AddChild(p[1]);
			}

			p[2] = Selection(L"SelIsPossibleTorwerIn?"); 
			{
				p[0]->AddChild(p[2]);

				p[3] = Sequence(L"SeqIsPossibleTowerIn?"); 
				{
					//	이거 수정해야함.
					p[4] = Condition<Trigger::Event::Special::Queries::CIfPossibleInTower>(L"CdtTowerIn?"); 
					{
						p[3]->AddChild(p[4]);
					}
					//	여기에 타워이동 이벤트 추가(사운드 출력이나 뭐....씬이동?)
				}

				p[3] = Sequence(L"SeqMapMoveOn"); 
				{
					p[2]->AddChild(p[3]);

					p[4] = Action<Trigger::Event::Special::Works::CDoMoveMapOn>(L"ActPossibleMoveMap"); 
					{
						p[3]->AddChild(p[4]);
					}
					//	 여기에 맵이동 이벤트들 넣기
				}

			}
		}
		p[0] = Selection(L"SelCheckResponType"); 
		{
			m_pRoot->AddChild(p[0]);
			p[1] = Sequence(L"CdtResTypeSequence"); 
			{
				p[0]->AddChild(p[1]);

				p[2] = Condition<Trigger::Event::Special::Queries::CIfSponTypeSame>(L"CdtResponSequence"); 
				{
					p[1]->AddChild(p[2]);
					dynamic_cast<Trigger::Event::Special::Queries::CIfSponTypeSame*>(p[1])->SetSponType(EC_RESPON_TYPE::SQUENCE);
				}

				p[3] = Selection(L"SelRoundclear?"); 
				{
					p[1]->AddChild(p[3]);

					p[4] = Sequence(L"SeqIsRoundClear?"); 
					{
						p[3]->AddChild(p[4]);

						p[5] = Condition<Trigger::Event::Special::Queries::CIfRoundClear>(L"CdtIsRoundClear?"); 
						{
							p[4]->AddChild(p[5]);
						}

						p[6] = Action<Trigger::Event::Special::Works::CDoRoundEnd>(L"ActRoundEnd");
						{
							p[4]->AddChild(p[6]);
						}
						//	여기다가 라운드 클리어시 이벤트 출력
					}

					p[4] = Sequence(L"SeqSponMonster");
					{
						p[3]->AddChild(p[4]);

						p[5] = Action<Trigger::Event::Special::Works::CDoMonsterSponSequence>(L"ActMonsterSponSequence");
						{
							p[4]->AddChild(p[5]);
						}

						//	여기다가 스폰시 이벤트를 넣는다. 참고로 몬스터가 하나씩나오는 wait이므로 좀 다르게 해야할거다.
					}

				}
			}
			p[1] = Sequence(L"CdtResTypeAtOnce"); 
			{
				p[0]->AddChild(p[1]);

				p[2] = Condition<Trigger::Event::Special::Queries::CIfSponTypeSame>(L"CdtResponAtOnce");
				{
					p[1]->AddChild(p[2]);
					dynamic_cast<Trigger::Event::Special::Queries::CIfSponTypeSame*>(p[1])->SetSponType(EC_RESPON_TYPE::ATONCE);
				}

				p[3] = Selection(L"SelRoundclear?");
				{
					p[1]->AddChild(p[3]);

					p[4] = Sequence(L"SeqIsRoundClear?"); 
					{
						p[3]->AddChild(p[4]);

						p[5] = Condition<Trigger::Event::Special::Queries::CIfRoundClear>(L"CdtIsRoundClear?"); 
						{
							p[4]->AddChild(p[5]);
						}

						p[6] = Action<Trigger::Event::Special::Works::CDoRoundEnd>(L"ActRoundEnd");
						{
							p[4]->AddChild(p[6]);
						}

						//	여기다가 라운드 클리어시 이벤트 출력
					}
		
					p[4] = Sequence(L"SeqSponMonster");
					{
						p[3]->AddChild(p[4]);

						p[5] = Action<Trigger::Event::Special::Works::CDoMonsterSponAtOnce>(L"ActMonsterSponAtOnce");
						{
							p[4]->AddChild(p[5]);
						}
						//	여기다가 스폰시 이벤트를 넣는다. 참고로 몬스터가 하나씩나오는 wait이므로 좀 다르게 해야할거다.
					}
				}
			}
			//	보스를 추가?!
		}
	}
	return m_pRoot;
}
