#pragma once
class CBoundingBox
{

public:
	CBoundingBox();
	~CBoundingBox();



	void CalcOBBBox(const Vec3 _vLocalMin, const Vec3 _vLocalMax,const Vec3 _vScale, Matrix _pmatRT);


	/*float FDotProduct(const float v0[3], const float v1[3]);

	int OBB2OBBIntersection(const CBoundingBox& box0, const CBoundingBox& box1);
*/
public:
	float center[3]; //월드중심 포지션
	float extent[3]; //세축의 길이
	float axis[3][3]; //세축의 x,y,z



};

