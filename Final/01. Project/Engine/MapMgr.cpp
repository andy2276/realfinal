#include "stdafx.h"
#include "MapMgr.h"
#include "KeyMgr.h"
#include "RenderMgr.h"
#include "GameObject.h"
#include "MapRapper.h"
#include "TimeMgr.h"
#include "Transform.h"
#include "MapScript.h"
#include "ItemMove.h"
#include "BossAi.h"
#include "StatusComponent.h"
#include "SkillPlatform.h"
#include "SkillElementDir.h"


CMapMgr::CMapMgr(): m_bMoveMap(false)
{
	m_arrMapIdx[2][0] = 4;	m_arrMapIdx[2][1] = 5;	 m_arrMapIdx[2][2] = 6;
	m_arrMapIdx[1][0] = 3;	m_arrMapIdx[1][1] = 9;	 m_arrMapIdx[1][2] = 7;
	m_arrMapIdx[0][0] = 2;	m_arrMapIdx[0][1] = 1;	 m_arrMapIdx[0][2] = 8;
	m_nForcedActive = 5;
}


CMapMgr::~CMapMgr()
{
}

const int CMapMgr::CalculMapIdxByPlayerPos()
{
	int xIdx = 0,zIdx = 0; 
	Vec3 pos = m_pPlayer->Transform()->GetLocalPos();
	if (600.f < pos.z) {
		++zIdx;
		if (1000.f < pos.z) {
			++zIdx;
		}
	}
	if (-200.f < pos.x) {
		++xIdx;
		if (200.f < pos.x) {
			++xIdx;
		}
	}
	return m_arrMapIdx[zIdx][xIdx];
}

void CMapMgr::Init()
{
	m_vMapSize = { 400.f,400.f };
	Vec2 half = m_vMapSize * 0.5;

	m_arrMapIdx[2][0] = 4;	m_arrMapIdx[2][1] = 5;	 m_arrMapIdx[2][2] = 6;
	m_arrMapIdx[1][0] = 3;	m_arrMapIdx[1][1] = 9;	 m_arrMapIdx[1][2] = 7;
	m_arrMapIdx[0][0] = 2;	m_arrMapIdx[0][1] = 1;	 m_arrMapIdx[0][2] = 8;

	//	추<
	//	여기 설정해야함.
	m_vecMaplappers[0]->SetStartPos(-half.x, -half.y);	//	startPoint
	m_vecMaplappers[1]->SetStartPos(-half.x, half.y);
	m_vecMaplappers[2]->SetStartPos(-half.y*3, half.y);
	m_vecMaplappers[3]->SetStartPos(-half.y*3, (half.y + m_vMapSize.y));
	m_vecMaplappers[4]->SetStartPos(-(half.x + m_vMapSize.y), (half.x + m_vMapSize.y * 2));
	m_vecMaplappers[5]->SetStartPos(-half.x, (half.y + m_vMapSize.y * 2));
	m_vecMaplappers[6]->SetStartPos(half.x, (half.y + m_vMapSize.y * 2));
	m_vecMaplappers[7]->SetStartPos(half.x, (half.y + m_vMapSize.y));
	m_vecMaplappers[8]->SetStartPos(half.x, half.y);
	m_vecMaplappers[9]->SetStartPos(-half.x, m_vMapSize.y);		//	중앙

	int arr[9] = {};
	int cnt = 0;
	arr[0] = 1; arr[1] = 9; arr[2] = 0;
	arr[3] = 0; arr[4] = 0; arr[5] = 0;
	arr[6] = 0; arr[7] = 0; arr[8] = 0;
	cnt = 0;
	m_vecMaplappers[0]->SetAwakeIdx(arr, cnt);
//---------------------------------------------------------------
	arr[0] = 2; arr[1] = 3; arr[2] = 9;
	arr[3] = 7; arr[4] = 8; arr[5] = 0;
	arr[6] = 0; arr[7] = 0; arr[8] = 0;
	cnt = 5;
	m_vecMaplappers[1]->SetAwakeIdx(arr, cnt);
//---------------------------------------------------------------
	arr[0] = 3; arr[1] = 9; arr[2] = 1;
	arr[3] = 0; arr[4] = 0; arr[5] = 0;
	arr[6] = 0; arr[7] = 0; arr[8] = 0;
	cnt = 3;
	m_vecMaplappers[2]->SetAwakeIdx(arr, cnt);
//---------------------------------------------------------------
	arr[0] = 4; arr[1] = 5; arr[2] = 9;
	arr[3] = 1; arr[4] = 2; arr[5] = 0;
	arr[6] = 0; arr[7] = 0; arr[8] = 0;
	cnt = 5;
	m_vecMaplappers[3]->SetAwakeIdx(arr, cnt);
//---------------------------------------------------------------
	arr[0] = 5; arr[1] = 9; arr[2] = 3;
	arr[3] = 0; arr[4] = 0; arr[5] = 0;
	arr[6] = 0; arr[7] = 0; arr[8] = 0;
	cnt = 3;
	m_vecMaplappers[4]->SetAwakeIdx(arr, cnt);
//---------------------------------------------------------------
	arr[0] = 6; arr[1] = 7; arr[2] = 9;
	arr[3] = 3; arr[4] = 4; arr[5] = 0;
	arr[6] = 0; arr[7] = 0; arr[8] = 0;
	cnt = 5;
	m_vecMaplappers[5]->SetAwakeIdx(arr, cnt);
//---------------------------------------------------------------
	arr[0] = 7; arr[1] = 9; arr[2] = 5;
	arr[3] = 0; arr[4] = 0; arr[5] = 0;
	arr[6] = 0; arr[7] = 0; arr[8] = 0;
	cnt = 3;
	m_vecMaplappers[6]->SetAwakeIdx(arr, cnt);
//---------------------------------------------------------------
	arr[0] = 8; arr[1] = 1; arr[2] = 9;
	arr[3] = 5; arr[4] = 6; arr[5] = 0;
	arr[6] = 0; arr[7] = 0; arr[8] = 0;
	cnt = 5;
	m_vecMaplappers[7]->SetAwakeIdx(arr, cnt);
//---------------------------------------------------------------
	arr[0] = 1; arr[1] = 9; arr[2] = 7;
	arr[3] = 0; arr[4] = 0; arr[5] = 0;
	arr[6] = 0; arr[7] = 0; arr[8] = 0;
	cnt = 3;
	m_vecMaplappers[8]->SetAwakeIdx(arr, cnt);
//---------------------------------------------------------------
	arr[0] = 0; arr[1] = 0; arr[2] = 0;
	arr[3] = 0; arr[4] = 0; arr[5] = 0;
	arr[6] = 0; arr[7] = 0; arr[8] = 0;
	cnt = 0;
	m_vecMaplappers[9]->SetAwakeIdx(arr, cnt);
//---------------------------------------------------------------


}

void CMapMgr::Resize(UINT _cnt)
{
	m_vecMaplappers.resize(_cnt);
	for (int i = 0; i < m_vecMaplappers.size(); ++i) {
		m_vecMaplappers[i] = new CMapRapper;
	}
}

void CMapMgr::RegistMap(UINT _mapNum,CGameObject * _pMap)
{
	m_vecMaplappers[_mapNum]->SetMap(_pMap);
}

void CMapMgr::RegistHeightMap(UINT _mapNum, const wstring & _path, float wid, float hei)
{
	if (m_vecMaplappers[_mapNum] != nullptr) {
		m_vecMaplappers[_mapNum]->SetHeightMap(_path, wid, hei);
	}
}

void CMapMgr::RegistColliderMap(UINT _mapNum, const wstring & _path, float wid, float hei, float _scale)
{
	if (m_vecMaplappers[_mapNum] != nullptr) {
		m_vecMaplappers[_mapNum]->SetColliderMap(_path, wid, hei, _scale);
	}
}

void CMapMgr::SettingAwakeMap(UINT _mapIdx, int * _mapArr, int _mapCnt)
{
	m_vecMaplappers[_mapIdx]->SetAwakeIdx(_mapArr, _mapCnt);
}

void CMapMgr::SetPortal(UINT _Mapidx, POINT _lu, POINT _rd, UINT _outMapIdx, const Vec3 & _startPos, const Vec3 & _startRot)
{
	m_vecMaplappers[_Mapidx]->SetPortal(_lu, _rd, _outMapIdx, _startPos, _startRot);
}


void CMapMgr::Update()
{
//	if (m_vecMaplappers[m_nCurMapIdx] == nullptr)return;
	//	문제는 해결했으니 천천히 해보자
	//	이동하는 이벤트시에만 작동을 시킨다.
	 if (m_bMoveMap) {
		m_bMoveMap = false;
		int preIdx = m_nCurMapIdx;
		m_nCurMapIdx = CalculMapIdxByPlayerPos();
		if (m_nCurMapIdx > 0) {
			m_vecMaplappers[preIdx]->ArrangeToStop();
			//m_vecMaplappers[preIdx]->PauseReadyObj();
			m_vecMaplappers[m_nCurMapIdx]->PlayMap();
			m_vecMaplappers[m_nCurMapIdx]->PlayReadyObj();
			vector<UINT>& awake = m_vecMaplappers[m_nCurMapIdx]->GetAwakeIdx();
			for (int i = 0; i < m_vecMaplappers.size(); ++i) {
				if (m_nCurMapIdx == i) { continue; }
				else if (awake.end() != std::find(awake.begin(), awake.end(), i)) {
					m_vecMaplappers[i]->PlayMap();
				}
				else {
 					m_vecMaplappers[i]->PauseMap();
				}
				m_vecMaplappers[preIdx]->PauseReadyObj();
			}
		}
		else {	//	중앙에 갈때 전부를 끄고간다.
			for (int i = 0; i < m_vecMaplappers.size(); ++i) {
				m_vecMaplappers[i]->PauseMap();
				m_vecMaplappers[i]->ResetAllObjs();
			}
		}
	}
	else {
		//	아직 맵안에 있다는 거니깐
		 UpdatePlayer();
		//ClearCurColliderMap();
		//CalculCollider(m_pPlayer);
		//CalculHeight(m_pPlayer);
		//m_vecMaplappers[m_nCurMapIdx]->ArrangeToReady();	//	되살아난 녀석이 있는지 없는지는 모르지만 어쨌든 다시 재등록
		//m_vecMaplappers[m_nCurMapIdx]->Update();
		//m_vecMaplappers[m_nCurMapIdx]->UpdateCollider();
		//m_vecMaplappers[m_nCurMapIdx]->UpdateHeight();

	}
}

void CMapMgr::ClearCurColliderMap()
{
	m_vecMaplappers[m_nCurMapIdx]->ClearColliderMap();
}



void CMapMgr::SetPlayerObj( CGameObject * _pPlayer)
{
	m_pPlayer = _pPlayer;
}

void CMapMgr::SetMoster(UINT _mapNum, CGameObject * _pMonster)
{
	m_vecMaplappers[_mapNum]->SetMapObj(EC_MAP_OBJ_TYPE::Monster, _pMonster);
}

void CMapMgr::SetItem(UINT _mapNum, CGameObject * _pItem)
{
	m_vecMaplappers[_mapNum]->SetMapObj(EC_MAP_OBJ_TYPE::Item, _pItem);
}

void CMapMgr::CalculHeight(CGameObject * _obj)
{
	BYTE* heiMap = nullptr;
	if (heiMap == nullptr)return;
	Vec3 pos = _obj->Transform()->GetLocalPos();
	//다시생각해보니깐 이렇게 필요는 없음.
	//bool turn = ((pos.x * pos.z) < 0.f) ? true : false; //	if (x*z < 0) 2,4 else 1,3
	////	좌표계 이동 (가운데 0,0 에서 좌하 0.0 으로)
	Vec2 fIdx = ((Vec2(pos.x, pos.z) - m_vecMaplappers[m_nCurMapIdx]->GetMapPos2())) + (m_vMapSize * 0.5f);
	int x = fIdx.x, z = fIdx.y;
	Vec2 fDeci = { fIdx.x - float(x),fIdx.y - float(z) };
	
	float A = heiMap[(z * 400) + x];
	float B = heiMap[(z * 400) + x+1];
	float C = heiMap[((z+1) * 400) + x];
	float D = heiMap[((z+1) * 400) + x+1];
	pos.y =
		A * (1.f - fDeci.x)*(1.f - fDeci.y) + B * fDeci.x*(1.f - fDeci.y) + C * (1.f - fDeci.x)*fDeci.y + D * fDeci.x*fDeci.y;

	pos.y = pos.y* 0.15f;
	if (A < pos.y&&B < pos.y&&C < pos.y&&D < pos.y) {
		//	있을수 없는일임 그냥 제일 큰값을 넣어줌
		pos.y = A;
		pos.y = (pos.y < B)? B: pos.y;
		pos.y = (pos.y < C) ? C : pos.y;
		pos.y = (pos.y < D) ? D : pos.y;
		pos.y *= 0.25f;
	}
	_obj->Transform()->SetLocalPos(pos);

}

void CMapMgr::CalculCollider(CGameObject * _obj)
{
	static XMINT2 size = { 400,400 };
	Vec2 temp;
	BYTE* colMap = m_vecMaplappers[m_nCurMapIdx]->GetColliderMap();
	if (colMap == nullptr)return;
	tPortal tPo = {};
	Vec3 pos = _obj->Transform()->GetLocalPos();
	Vec2 fIdx = ((Vec2(pos.x, pos.z) - m_vecMaplappers[m_nCurMapIdx]->GetMapPos2())) + (m_vMapSize * 0.5f);
	int x = fIdx.x, z = fIdx.y;
	

	int idx = (z * size.y) + x;
	int data = int(colMap[idx]);

	Vec3 dir;
	switch (data)
	{
	case 0://Blocked
	case 2:
		dir = _obj->Transform()->GetMoveDir();
		_obj->Transform()->SetLocalPos(pos + (dir * -1.f * 5.f));
		//_obj->Transform()->SwapPreToPos();
		//temp = m_vecMaplappers[m_nCurMapIdx]->SearchAround(_obj, z, x);
		//if (errPoint.x < temp.x && errPoint.y < temp.y) {
		//	pos.x += temp.x; pos.y; pos.z += temp.y;
		//	_obj->Transform()->SetLocalPos(pos);
		//}
		//else {
		//
		//}
		break;
	
		//	만약 이동하라라는 명령이 떨어졌다면
		
		break;
	case 255:
		colMap[idx] = 2;
		break;
	}
}

void CMapMgr::UpdatePlayer()
{
	m_vecMaplappers[m_nCurMapIdx]->ClearColliderMap();
	static const float fOffset = 1.f;
	static Vec2 mapHalfSize = { 400.f * 0.5f,400.f * 0.5f };

	BYTE* colMap = m_vecMaplappers[m_nCurMapIdx]->GetColliderMap();
	float* heiMap = m_vecMaplappers[m_nCurMapIdx]->GetHeightMap();

	int nIdxX = 0, nIdxZ = 0, nIdx = 0, nColX = 0, nColZ = 0;

	static	Vec3 pos, movPos, vA, vB, vC, vD, vUh, vVh, vNormal, vRot;
	static Vec2 fIdx, fDeci, fColIdx;
	bool bWhatCalcul = true;
	bool bStopIt = false;
	bool bNomalOn = false;

	vA = { 0.0f, 0.0f, 0.0f }; vB = { 1.0f, 0.0f, 0.0f };
	vC = { 0.0f, 0.0f, 1.0f }; vD = { 1.0f, 0.0f, 1.0f };
	vUh = { 0.f,0.f,0.f }; vVh = { 0.f,0.f,0.f };
	vNormal = { 0.f,0.f,0.f }; vRot = { 0.f,0.f,0.f };

	fIdx = { 0.f,0.f }; fDeci = { 0.f,0.f }; fColIdx = { 0.f,0.f };
	int againMax = 3, againCnt = 3;

	pos = m_pPlayer->Transform()->GetLocalPos();
	fIdx = (Vec2(pos.x, pos.z) - m_vecMaplappers[m_nCurMapIdx]->GetMapPos2()) + mapHalfSize;

	if (m_vecMaplappers[m_nCurMapIdx]->IsMapMovePossible()) {
		if (m_vecMaplappers[m_nCurMapIdx]->IsInPortar(fIdx)) {
			int nextPortal = m_vecMaplappers[m_nCurMapIdx]->GetNextMap();
			tPortal tPo = {};
			tPo = m_vecMaplappers[nextPortal]->GetPortal(m_nCurMapIdx);
			m_pPlayer->Transform()->SetLocalPos(tPo.StartPos);
			m_pPlayer->Transform()->SetLocalRot(tPo.StartRot);
			m_bMoveMap = true;
			
			return;
		}
	}
	if (colMap != nullptr) {
		Vec3 movD = m_pPlayer->Transform()->GetMoveDir();
		const XMINT2& colSize = m_vecMaplappers[m_nCurMapIdx]->GetColMapSize();
		fColIdx = fIdx * m_vecMaplappers[m_nCurMapIdx]->GetColMapScale();
		nIdx = (colSize.x * int(fColIdx.y)) + int(fColIdx.x);
		BYTE type = colMap[nIdx];
		
		if (type != (BYTE)EC_COL_TYPE::Player && type != (BYTE)EC_COL_TYPE::Path) {
			//	충돌
			movPos = m_vecMaplappers[m_nCurMapIdx]->UnitCollider(m_pPlayer, fColIdx.y, fColIdx.x, bStopIt);

			if (bStopIt) {

			}
			else {
			//	m_pPlayer->Transform()->SetLocalPos(pos + (movPos * 100.f * DT));
			//	m_pPlayer->Transform()->AddForce(movPos, DT);
				m_pPlayer->Transform()->SwapPreToPos();
				//m_pPlayer->Transform()->SetLocalPos(pos + (m_pPlayer->Transform()->GetMoveDir() * 500.f * DT * -1.f));
			}
		}
		else {
			m_vecMaplappers[m_nCurMapIdx]->StempingColliderMap((BYTE)EC_COL_TYPE::Player, fColIdx.y, fColIdx.x);
		}
		
	}
	if (heiMap != nullptr) {
		nIdxZ = fIdx.y; nIdxX = fIdx.x;
		fDeci = { fIdx.x - float(nIdxX) ,fIdx.y - float(nIdxZ) };

		vA.y = heiMap[(nIdxZ * 400) + nIdxX];
		vB.y = heiMap[(nIdxZ * 400) + nIdxX + 1];
		vC.y = heiMap[((nIdxZ + 1) * 400) + nIdxX];
		vD.y = heiMap[((nIdxZ + 1) * 400) + nIdxX + 1];

		if (fIdx.x >= fIdx.y) {
			//	아래쪽 삼각형
			vUh = (vA - vB);
			vVh = (vD - vB);
		}
		else {
			//	위쪽 삼각형
			vUh = (vD - vC);
			vVh = (vA - vC);
		}

		if (bWhatCalcul) {
			pos.y =
				(vA.y * (1.f - fDeci.x)*(1.f - fDeci.y) + 
					vB.y * fDeci.x*(1.f - fDeci.y) + 
					vC.y * (1.f - fDeci.x)*fDeci.y + 
					vD.y * fDeci.x*fDeci.y) * fOffset;
			if (vA.y < pos.y&&vB.y < pos.y&&vC.y < pos.y&&vD.y < pos.y) {
				//	있을수 없는일임 그냥 제일 큰값을 넣어줌
				pos.y = vA.y;
				pos.y = (pos.y < vB.y) ? vB.y : pos.y;
				pos.y = (pos.y < vC.y) ? vC.y : pos.y;
				pos.y = (pos.y < vD.y) ? vD.y : pos.y;
			}
			pos.y *= fOffset;
		}
		else {
			if (fIdx.x >= fIdx.y) {
				//	아래쪽 삼각형
				pos.y = (vA.y + Linear(0.0f, vUh.y, 1.0f - fDeci.x) + Linear(0.0f, vVh.y, fDeci.y)) * fOffset;
			}
			else {
				//	위쪽 삼각형
				pos.y = (vC.y + Linear(0.0f, vUh.y, fDeci.x) + Linear(0.0f, vVh.y, 1.0f - fDeci.y)) * fOffset;
			}
		}
		if (bNomalOn) {
			vNormal = ((vUh.Cross(vVh)).Normalize()) ;
			if (vNormal.y > 0.f) {
				float xAngle = vNormal.Dot(Vec3::Front);
				float zAngle = vNormal.Dot(Vec3::Right);

				vRot = m_pPlayer->Transform()->GetLocalRot();
				vRot.x = xAngle; vRot.z = zAngle;
				m_pPlayer->Transform()->SetLocalRot(vRot);
			}
		}
		pos.y = vA.y;

		m_pPlayer->Transform()->SetLocalPos(pos);
	}
	
}

UINT CMapMgr::GetReadyObj(UINT _idx)
{
	return m_vecMaplappers[_idx]->GetReadyObjCnt();
}

UINT CMapMgr::GetCurReadyObj()
{
	return m_vecMaplappers[m_nCurMapIdx]->GetReadyObjCnt();
}

int CMapMgr::GetCurMapIdx()
{
	return m_nCurMap;
}

int CMapMgr::GetMapIdx(const Vec3 & _pos)
{
	int z = 0, x = 0;

	if (200.f > _pos.z)
	{
		m_nCurMap = 0;
		return 0;
	}
	else if (_pos.z > 600.f)
	{
		++z;
		if (_pos.z > 1000.f)
		{
			++z;
		}
	}

	if (_pos.x > -200.f)
	{
		++x;
		if (_pos.x > 200.f)
		{
			++x;
		}
	}
	m_nCurMap = m_arrMapIdx[z][x];
	return m_nCurMap;
}

void CMapMgr::SetMap(int _mapNum, CGameObject * _pMap)
{
	m_arrMaps[_mapNum] = _pMap;
	m_arrMapScripts[_mapNum] = m_arrMaps[_mapNum]->GetScript<CMapScript>();
}

void CMapMgr::ReUpdate()
{
	int z = 0, x = 0;
	Vec3 vPlayerPos = m_pPlayer->Transform()->GetLocalPos();
	if (200.f > vPlayerPos.z)
	{
		m_nCurMap = 0;
		return;
	}
	else if (vPlayerPos.z > 600.f)
	{
		++z;
		if (vPlayerPos.z > 1000.f)
		{
			++z;
		}
	}

	if (vPlayerPos.x > -200.f)
	{
		++x;
		if (vPlayerPos.x > 200.f)
		{
			++x;
		}
	}
	m_nCurMap = m_arrMapIdx[z][x];
	return;
}

int CMapMgr::GetCalculMapIdx()
{
	Vec3 vPlayerPos = m_pPlayer->Transform()->GetLocalPos();
	int z = 0, x = 0;

	if (200.f > vPlayerPos.z)
	{ 
		m_nCurMap = 0;
		return 0; 
	}

	else if (vPlayerPos.z > 600.f) 
	{
		++z;
		if (vPlayerPos.z > 1000.f)
		{
			++z;
		}
	}

	if (vPlayerPos.x > -200.f)
	{
		++x;
		if (vPlayerPos.x > 200.f)
		{
			++x;
		}
	}
	m_nCurMap = m_arrMapIdx[z][x];
	return m_nCurMap;


}

vector<CGameObject*>& CMapMgr::GetMonster(int _mapNum, int _mopType)
{
	return m_arrMapScripts[_mapNum]->GetMonster(_mopType);
}

vector<CGameObject*>& CMapMgr::GetMonster(int _mopType)
{
	return m_arrMapScripts[m_nCurMap]->GetMonster(_mopType);
}

list<CGameObject*>& CMapMgr::GetMonster()
{
	return m_arrMapScripts[m_nCurMap]->GetAllMonster();
}

void CMapMgr::PushObj(EC_MOSTER_TYPE _type, CGameObject* _pObj)
{
	m_vecAllMonster[(UINT)_type].push_back(_pObj);
}

void CMapMgr::PushStone(CGameObject* _pObj)
{
	m_vecAllMonster[(UINT)EC_MOSTER_TYPE::Stone].push_back(_pObj);
}

void CMapMgr::PushMimic(CGameObject* _pObj)
{
	m_vecAllMonster[(UINT)EC_MOSTER_TYPE::Mimic].push_back(_pObj);
}

void CMapMgr::PushSkell(CGameObject* _pObj)
{
	m_vecAllMonster[(UINT)EC_MOSTER_TYPE::Skelleton].push_back(_pObj);
}

//	스킬 레인지를 추가할까? 이거는 나중에
const Vec3 & CMapMgr::GetMouseWorld(float _range)
{
	//	마우스를 들고온다
	//	마우스 포즈를 -1 ~ 1 로변환한다.
	//		변환시에는 -화면 절반
	//	그리고 그 높이에 따라서...음 그냥 퍼센테이지로 할까?
	//	오류 -> 창의 위치를 변경시키면 오차가 발생함.
	//	그리고 그 만들어진 좌표를 다시 맵사이즈로 확장시킨다. 확장은 그냥 곱하기 하면 알아서 나올거임. 
	//	100 * 100 으로 변환후(범위일거임) 플레이어 위치를 더해주면 끝남.
	static POINT mousePos,dPos;
	static tResolution resolution = CRenderMgr::GetInst()->GetResolution();
	static Vec2 halfResolution = { (resolution.fWidth*0.5f),  (resolution.fHeight*0.5f) };
	float range = 100.f;
	mousePos = KEY_MOUSEPOS;
//	dPos = { (mousePos.x - halfResolution.x)/ halfResolution.x,((mousePos.y - halfResolution.y)/ halfResolution.y ) * -1};	//	-1 ~ 1변환
//	m_vMouseWorld = { ((float(mousePos.x) - halfResolution.x) / halfResolution.x)*range ,0.f, ((float(mousePos.y) - halfResolution.y) / halfResolution.y) * -range };
	m_vMouseWorld = { ((float(mousePos.x) / halfResolution.x) -1.f)*_range ,0.f, (1.f - float(mousePos.y) /resolution.fHeight)*_range };
	m_vMouseWorld += m_pPlayer->Transform()->GetLocalPos();
	

	return m_vMouseWorld;
}

void CMapMgr::StopFiledMap()
{
	for (int i = 0; i < 9; ++i)
	{
	//	if(m_arrMaps[i] != nullptr)m_arrMaps[i]->SetActive(false);
		if (m_arrMapScripts[i] != nullptr)m_arrMapScripts[i]->OnStopMap();
	}


}

void CMapMgr::PlayFiledMap()
{
	m_arrMaps[9]->SetActive(false);
	for (int i = 0; i < 9; ++i)
	{
		m_arrMaps[i]->SetActive(true);
	}

}

void CMapMgr::StopTowerMap()
{
	m_arrMaps[9]->SetActive(false);
}

void CMapMgr::PlayTowerMap()
{
	m_arrMaps[9]->SetActive(true);
}

float CMapMgr::GetHeight(int _mapNum, const Vec3& _pos)
{
	return m_arrMapScripts[_mapNum]->GetHeight(_pos);
}

float CMapMgr::GetHeight(const Vec3 & _pos)
{
	return m_arrMapScripts[m_nCurMap]->GetHeight(_pos);
}


const tMapRect & CMapMgr::GetMapRect(int _idx)
{
	static tMapRect t = {};
	t.v4Rect = m_arrMapScripts[_idx]->GetMapRect();
	t.v3Pos = m_arrMaps[_idx]->Transform()->GetLocalPos();
	return t;
}

void CMapMgr::DropItem(const Vec3 & _pos,int _offset)
{
	Vec3 newPos = _pos;
	for (int i = 0; i < m_vecItems.size(); ++i)
	{
		
		if (m_vecItems[i] != nullptr && !m_vecItems[i]->IsActive())
		{
			newPos.y += 4.f;
			m_vecItems[i]->Transform()->SetLocalPos(newPos);
			m_vecItems[i]->GetScript<CItemMove>()->Reset();
			m_vecItems[i]->SetActive(true);
			return;
		}
	}
	//	여기는 새로만들어야함
	int dataIdx = 0;
	if (_offset > 0)
	{
		dataIdx = rand() % m_vecScrolls.size();
	}
	CGameObject* pObj = m_vecScrolls[dataIdx]->Instantiate();
	pObj->SetName(L"Item");
	pObj->FrustumCheck(false);

	newPos.y = GetHeight(m_nCurMap, newPos);
	newPos.y += 4.0f;
	pObj->Transform()->setPhysics(false);
	pObj->Transform()->SetLocalPos(newPos);
	pObj->AddComponent(new CCollider2D);
	pObj->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::SPHERE);
	pObj->Collider2D()->SetOffsetScale(Vec3(3.f, 3.f, 3.f));
	pObj->AddComponent(new CItemMove);
	m_vecItems.push_back(pObj);
	CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"Item")->AddGameObject(pObj);
	//	등록시킨 아이템이 사라지는지 확인!
}

const Vec3 & CMapMgr::GetPlayerAttackDir()
{
	return m_vPlayerAttackDir;
}

CGameObject* CMapMgr::CreateBossMonster(int _bossType, int _offset)
{
	const int skill1 = 0, skill2 = 1, skill3 = 2;
	const int phase1 = 0, phase2 = 1, phase3 = 2;

	tBossReset t = {}; 
	//	Defualt

	t.fMax = 500.f;
	t.bagicDist = 20.f;
	t.bagicCool = 5.f;
	t.bagicAngle = 45.f * XM_ANGLE;
	t.bagicAttackTime = ANIMFRAME(5);
	t.skill1Dist = 40.f;
	t.skill1ShootTime = 1.f;
	t.skill1Cool = 15.f;
	t.skill2Dist = 60.f;
	t.skill2ShootTime = 1.f;
	t.skill2Cool = 15.f;
	t.skill3Dist = 100.f;
	t.skill3ShootTime = 1.f;
	t.skill3Cool = 15.f;
	t.minRange = 15.f;
	t.attackRange = 60.f;
	t.SearchRange = 300.f;
	t.phase1hp = t.fMax;
	t.phase2hp = 0.f;
	t.phase3hp = 0.f;
	t.moveSpeed = 65.f;
	t.rotSpeed = 120.0f * XM_ANGLE;


	CGameObject* pObj = nullptr;
	CBossAi* pScript = nullptr;
	CStatusComponent* state = nullptr;

	pObj = m_vecBossData[_bossType]->Instantiate();
	pObj->AddComponent(new CStatusComponent);
	pObj->AddComponent(new CCollider2D);
	pObj->FrustumCheck(false);
	state = pObj->StatusComponent();
	pScript = new CBossAi;
	pObj->AddComponent(pScript);
	pScript->SetPlayer(m_pPlayer);

	

	UINT skillCheck = 0;
	if ((UINT)EC_BOSS_TYPE::STONE == _bossType)
	{
		skillCheck = ((UINT)EC_BOSS_STATE::Skill1 | (UINT)EC_BOSS_STATE::BagicAttack);
		t.fMax = 500.f;
		t.bagicDist = 20.f;
		t.bagicCool = 5.f;
		t.bagicAngle = 45.f * XM_ANGLE;
		t.bagicAttackTime = ANIMFRAME(5);
		t.skill1Dist = 40.f;
		t.skill1ShootTime = 10.f;
		t.skill1Cool = 15.f;
		t.skill2Dist = 60.f;
		t.skill2ShootTime = 1.f;
		t.skill2Cool = 15.f;
		t.skill3Dist = 100.f;
		t.skill3ShootTime = 1.f;
		t.skill3Cool = 15.f;
		t.minRange = 15.f;
		t.attackRange = 60.f;
		t.SearchRange = 300.f;
		t.phase1hp = t.fMax;
		t.phase2hp = 0.f;
		t.phase3hp = 0.f;
		t.moveSpeed = 65.f;
		t.rotSpeed = 120.0f * XM_ANGLE;

		pObj->Transform()->SetHasOffset((BYTE)EC_TRANS_OFFSET::ROTATE);
		pObj->Transform()->SetLocalOffset(Vec3(0.0f, XM_PI, 0.0f), Vec3::Zero);
		pObj->Transform()->SetLocalScale(Vec3(1.5f, 1.5f, 1.5f));
		if (_offset > 0)
		{
			skillCheck |= (UINT)EC_BOSS_STATE::Skill3;

			t.skill3Cool = 50.f;

			pScript->SetSkillShootDist(skill3, t.skill3Dist);
			pScript->SetSkillShootTime(skill3, t.skill3ShootTime);
		}

		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Idle, (UINT)EC_STONE_BOSS_ANIM::Idle);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Run, (UINT)EC_STONE_BOSS_ANIM::Idle);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Damage, (UINT)EC_STONE_BOSS_ANIM::Damaged);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Death, (UINT)EC_STONE_BOSS_ANIM::Death);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Skill1, (UINT)EC_STONE_BOSS_ANIM::Skill);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Skill2, (UINT)EC_STONE_BOSS_ANIM::Skill);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Skill3, (UINT)EC_STONE_BOSS_ANIM::Skill);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Attack, (UINT)EC_STONE_BOSS_ANIM::Attack);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Stun, (UINT)EC_STONE_BOSS_ANIM::Idle);
		
		pScript->SetBossSkill(0, 0);


	}
	else if ((UINT)EC_BOSS_TYPE::MIRROR == _bossType)
	{
		skillCheck = ((UINT)EC_BOSS_STATE::Skill1 | (UINT)EC_BOSS_STATE::BagicAttack);
		t.fMax = 500.f;
		t.bagicDist = 20.f;
		t.bagicCool = 5.f;
		t.bagicAngle = 45.f * XM_ANGLE;
		t.bagicAttackTime = ANIMFRAME(5);
		t.skill1Dist = 40.f;
		t.skill1ShootTime = 10.f;
		t.skill1Cool = 10.f;
		t.skill2Dist = 60.f;
		t.skill2ShootTime = 1.f;
		t.skill2Cool = 15.f;
		t.skill3Dist = 100.f;
		t.skill3ShootTime = 1.f;
		t.skill3Cool = 100.f;
		t.minRange = 15.f;
		t.attackRange = 45.f;
		t.SearchRange = 500.f;
		t.phase1hp = t.fMax;
		t.phase2hp = 200.f;
		t.phase3hp = 0.f;
		t.moveSpeed = 75.f;
		t.rotSpeed = 110.0f * XM_ANGLE;

		pObj->Transform()->SetHasOffset((BYTE)EC_TRANS_OFFSET::ROTATE);
		pObj->Transform()->SetLocalOffset(Vec3(0.0f, XM_PI, 0.0f), Vec3::Zero);
		pObj->Transform()->SetLocalScale(Vec3(1.3f, 1.3f, 1.3f));
		if (_offset > 0)
		{
			skillCheck |= (UINT)EC_BOSS_STATE::Skill3;

			t.skill3Cool = 50.f;

			pScript->SetSkillShootDist(skill3, t.skill3Dist);
			pScript->SetSkillShootTime(skill3, t.skill3ShootTime);
		}

		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Idle, (UINT)EC_PLAYER_ANIM::Idle);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Run, (UINT)EC_PLAYER_ANIM::Run);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Damage, (UINT)EC_PLAYER_ANIM::Damage);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Death, (UINT)EC_PLAYER_ANIM::Death);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Skill1, (UINT)EC_PLAYER_ANIM::Attack);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Skill2, (UINT)EC_PLAYER_ANIM::Attack);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Skill3, (UINT)EC_PLAYER_ANIM::Attack);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Attack, (UINT)EC_PLAYER_ANIM::Skill);
		pScript->SetMappingAnimIdx(EC_MAPPING_ANIM::Stun, (UINT)EC_PLAYER_ANIM::Stun);

		pScript->SetBossSkill(0, 1);
	}
	state->SetFloat(STATUS_FLOAT::HP, t.fMax);
	state->SetFloat(STATUS_FLOAT::MAX_HP, t.fMax);
	state->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX, t.bagicCool);
	state->SetFloat(STATUS_FLOAT::SKILL_1_COOL_MAX, t.skill1Cool);
	state->SetFloat(STATUS_FLOAT::SKILL_2_COOL_MAX, t.skill2Cool);
	state->SetFloat(STATUS_FLOAT::SKILL_HEAL_COOL_MAX, t.skill3Cool);
	state->SetFloat(STATUS_FLOAT::MOVESPEED, t.moveSpeed);
	state->SetFloat(STATUS_FLOAT::ROTSPEED, t.rotSpeed);
	state->SetFloat(STATUS_FLOAT::TIME_ACCEL, 1.f);
	state->SetFloat(STATUS_FLOAT::ATTACKRANGE, t.attackRange);

	pScript->SetSkillCheck(skillCheck);

	pScript->SetSkillShootDist(skill1, t.skill1Dist);
	pScript->SetSkillShootTime(skill1, t.skill1ShootTime);

	pScript->SetSkillShootDist(skill2, t.skill2Dist);
	pScript->SetSkillShootTime(skill2, t.skill2ShootTime);

	pScript->SetBagicAttackDist(t.bagicDist);
	pScript->SetBagicAttackAngle(-t.bagicAngle, t.bagicAngle);
	pScript->SetBagicAttackTime(t.bagicAttackTime);

	pScript->SetPhaseHp(phase1, t.phase1hp);
	pScript->SetPhaseHp(phase2, t.phase2hp);
	pScript->SetPhaseHp(phase3, t.phase3hp);
								
	pScript->SetResetInfo(t);
	pScript->SetBossType(_bossType);

	m_vecBossMonster.push_back(pObj);
	m_vecBossAiScript.push_back(pScript);

	CSceneMgr::GetInst()->GetCurScene()->AddGameObject(L"Monster", pObj, false);
	return pObj;
}

CGameObject * CMapMgr::ResponBoss(int _mapNum, int _bossType, const Vec3 & _pos, const Vec3 & _rot, int _offset)
{
	CGameObject* pObj = nullptr;
	for (int i = 0; i < m_vecBossMonster.size(); ++i)
	{
		if (m_vecBossMonster[i] != nullptr && !m_vecBossMonster[i]->IsActive() && m_vecBossAiScript[i]->GetBossType() == _bossType)
		{
			pObj = m_vecBossMonster[i];
			m_vecBossAiScript[i]->Reset(_mapNum, _pos, _rot);
			pObj->SetActive(true);

			return pObj;
		}
	}
	pObj = CreateBossMonster(_bossType, _offset);
	pObj->StatusComponent()->SetMapNum(_mapNum);
	pObj->Transform()->SetLocalPos(_pos);
	pObj->Transform()->SetLocalRot(_rot);
	return pObj;
}

void CMapMgr::InitSkillArch()
{
	tSkillArch t = {};
	tSkillElementValue v = {};

	//	============================
	//	realStonShower
	//	============================
	{
		t.A_nBulletObjCnt = 50;
		t.B_nShootBulletCnt = 10;

		t.C_bBulletMesh = false;

		t.D_bMapMgrMesh = true;

		t.Da_nMapMgrMeshIdx = (UINT)EC_MONSTER_BULLET::Stone;
		{
			v.PosValues.vOffsetPos = Vec3(0.0f, 30.0f, 0.0f);
			v.PosValues.bRandom = true;
			v.PosValues.nRandomSeed = 50;

			v.Endtypes.fEndDist = 50.f;

			v.MoveValues.vMoveDir = Vec3::Down;
			v.MoveValues.fMoveSpeed = 50.f;
			v.MoveValues.bRandom = true;
			v.MoveValues.nRandomSeed = 10;

			v.SinCosAngles.fSinCosInitAngle = 0.f;
			v.SinCosAngles.fSinCosCurAngle = 0.f;
			v.SinCosAngles.fSinCosAmplitude = 5.0f * XM_ANGLE;
			v.SinCosAngles.fSinCosSize = 0.5f;

			v.EventTimes.fStartEventMax = 1.0f;

			v.Others.fDamage = 22.f;
			v.Others.nBulletType = (UINT)EC_MONSTER_BULLET::Stone;
		}
		t.E_tSkillElementValue = v;

		t.F_nTypes = 
			(
				(UINT)EC_SKILL_ELEMENT::EndType_Col
				| (UINT)EC_SKILL_ELEMENT::EndType_Dist
				| (UINT)EC_SKILL_ELEMENT::ColType_Damage
				| (UINT)EC_SKILL_ELEMENT::MoveType_Straight
				| (UINT)EC_SKILL_ELEMENT::MoveType_SinCosLook
			);
		t.G_vOffsetScale = Vec3(3.f, 3.f, 3.f);
		t.H_wstrLayer = L"MonsterBullet";
	}
	
	m_vecSkillArchInfo.push_back(t);

	t = {};
	v = {};

	//	============================
	//	FireBoom
	//	============================
	{
		t.A_nBulletObjCnt = 50;
		t.B_nShootBulletCnt = 1;

		t.C_bBulletMesh = false;

		t.D_bMapMgrMesh = true;

		t.Da_nMapMgrMeshIdx = (UINT)EC_MONSTER_BULLET::DarkBall;
		{
			v.PosValues.vOffsetPos = Vec3(0.0f, 5.0f, 0.0f);
			v.PosValues.bRandom = false;
			v.PosValues.nRandomSeed = 0;

			v.Endtypes.fEndDist = 45.f;

			v.MoveValues.vMoveDir = Vec3::Front;
			v.MoveValues.bDonInitMove = true;
			v.MoveValues.eDirType = DIR_TYPE::FRONT;
			v.MoveValues.fMoveSpeed = 120.f;
			v.MoveValues.bRandom = false;
			v.MoveValues.nRandomSeed = 10;

			v.SinCosAngles.fSinCosInitAngle = 0.f;
			v.SinCosAngles.fSinCosCurAngle = 0.f;
			v.SinCosAngles.fSinCosAmplitude = 1.0f * XM_ANGLE;
			v.SinCosAngles.fSinCosSize = 0.25f;

			v.EventTimes.fStartEventMax = 1.0f;

			v.Others.fDamage = 35.f;
			v.Others.nBulletType = (UINT)EC_MONSTER_BULLET::DarkBall;
		}
		t.E_tSkillElementValue = v;

		t.F_nTypes =
			(
			(UINT)EC_SKILL_ELEMENT::EndType_Col
				| (UINT)EC_SKILL_ELEMENT::EndType_Dist
				| (UINT)EC_SKILL_ELEMENT::ColType_Damage
				| (UINT)EC_SKILL_ELEMENT::MoveType_Straight
				| (UINT)EC_SKILL_ELEMENT::MoveType_SinCosLook
				);
		t.G_vOffsetScale = Vec3(2.f, 2.f, 2.f);
		t.H_wstrLayer = L"MonsterBullet";
	}
	//	============================
	//	DropStone
	//	============================
	{
		t.A_nBulletObjCnt = 2;
		t.B_nShootBulletCnt = 1;

		t.C_bBulletMesh = false;

		t.D_bMapMgrMesh = true;

		t.Da_nMapMgrMeshIdx = (UINT)EC_MONSTER_BULLET::Stone;
		{
			v.PosValues.vOffsetPos = Vec3(0.0f, 35.0f, 0.0f);
			v.PosValues.bRandom = false;
			v.PosValues.nRandomSeed = 0;

			v.Endtypes.fEndDist = 50.f;

			v.MoveValues.vMoveDir = Vec3::Down;
			v.MoveValues.bDonInitMove = false;
			v.MoveValues.eDirType = DIR_TYPE::FRONT;
			v.MoveValues.bTargetPlayer = true;
			v.MoveValues.fMoveSpeed = 180.f;
			v.MoveValues.bRandom = false;
			v.MoveValues.nRandomSeed = 10;

			v.SinCosAngles.fSinCosInitAngle = 0.f;
			v.SinCosAngles.fSinCosCurAngle = 0.f;
			v.SinCosAngles.fSinCosAmplitude = 1.0f * XM_ANGLE;
			v.SinCosAngles.fSinCosSize = 0.25f;

			v.EventTimes.fStartEventMax = 1.0f;

			v.Others.fDamage = 25.f;
			v.Others.nBulletType = (UINT)EC_MONSTER_BULLET::Stone;
		}
		t.E_tSkillElementValue = v;

		t.F_nTypes =
			(
			(UINT)EC_SKILL_ELEMENT::EndType_Col
				| (UINT)EC_SKILL_ELEMENT::EndType_Dist
				| (UINT)EC_SKILL_ELEMENT::ColType_Damage
				| (UINT)EC_SKILL_ELEMENT::MoveType_Straight
				);
		t.G_vOffsetScale = Vec3(2.f, 2.f, 2.f);
		t.H_wstrLayer = L"MonsterBullet";
	}

	m_vecSkillArchInfo.push_back(t);
}

void CMapMgr::OnOutTowerObj()
{
	if (m_pOutTowerObj != nullptr)m_pOutTowerObj->SetActive(true); 
	
}

void CMapMgr::OffOutTowerObj()
{
	if (m_pOutTowerObj != nullptr)m_pOutTowerObj->SetActive(false);
}

void CMapMgr::InitFloorTex()
{
	
	
	Ptr<CTexture> ptr1 = CResMgr::GetInst()->Load<CTexture>(L"FloorTex1", L"Texture\\minimap_1F.png");
	ptr1->SetName(L"FloorTex1");
	m_vecFloorTex.push_back(ptr1);
	Ptr<CTexture> ptr2 = CResMgr::GetInst()->Load<CTexture>(L"FloorTex2", L"Texture\\minimap_2F.png");
	ptr2->SetName(L"FloorTex2");
	m_vecFloorTex.push_back(ptr2);
	Ptr<CTexture> ptr3 = CResMgr::GetInst()->Load<CTexture>(L"FloorTex3", L"Texture\\minimap_3F.png");
	ptr3->SetName(L"FloorTex3");
	m_vecFloorTex.push_back(ptr3);
	Ptr<CTexture> ptr4 = CResMgr::GetInst()->Load<CTexture>(L"FloorTex4", L"Texture\\minimap_4F.png");
	ptr4->SetName(L"FloorTex4");
	m_vecFloorTex.push_back(ptr4);
	Ptr<CTexture> ptr5 = CResMgr::GetInst()->Load<CTexture>(L"FloorTex5", L"Texture\\minimap_5F.png");
	ptr5->SetName(L"FloorTex5");
	m_vecFloorTex.push_back(ptr5);
	Ptr<CTexture> ptr6 = CResMgr::GetInst()->Load<CTexture>(L"FloorTex6", L"Texture\\minimap_6F.png");
	ptr6->SetName(L"FloorTex6");
	m_vecFloorTex.push_back(ptr6);
	Ptr<CTexture> ptr7 = CResMgr::GetInst()->Load<CTexture>(L"FloorTex7", L"Texture\\minimap_7F.png");
	ptr7->SetName(L"FloorTex7");
	m_vecFloorTex.push_back(ptr7);
	Ptr<CTexture> ptr8 = CResMgr::GetInst()->Load<CTexture>(L"FloorTex8", L"Texture\\minimap_8F.png");
	ptr8->SetName(L"FloorTex8");
	m_vecFloorTex.push_back(ptr8);
	Ptr<CTexture> ptr9 = CResMgr::GetInst()->Load<CTexture>(L"FloorTex9", L"Texture\\minimap_9F.png");
	ptr9->SetName(L"FloorTex9");
	m_vecFloorTex.push_back(ptr9);

}


