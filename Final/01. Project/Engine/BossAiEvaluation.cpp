#include "stdafx.h"
#include "BossAiEvaluation.h"


CBossAiEvaluation::CBossAiEvaluation()
{
}


CBossAiEvaluation::~CBossAiEvaluation()
{
}

nBT::CMS::bSeq::bSeq()
{
}

nBT::CMS::bSeq::~bSeq()
{
}

nBT::bResult nBT::CMS::bSeq::Evaluate(bResult _result)
{
	if (m_eState == nBT::bResult::Evaluation)
	{
		AddEvalueCnt();
		bResult curResult;
		for (int i = 0; i < m_vecChild.size(); ++i)
		{
			curResult = m_vecChild[i]->Evaluate();
			switch (curResult)
			{
			case nBT::bResult::Evaluation:
				break;
			case nBT::bResult::Fail:
				m_eState = nBT::bResult::Fail;
				return nBT::bResult::Fail;
			case nBT::bResult::Running:
				m_eState = nBT::bResult::Evaluation;
				return  nBT::bResult::Running;

			case nBT::bResult::Success:
				m_eState = nBT::bResult::Success;
				break;
			}
		}
		m_eState = nBT::bResult::Success;
		return nBT::bResult::Success;
	}
	else
	{
		return m_eState;
	}
}

nBT::CMS::bSel::bSel()
{
}

nBT::CMS::bSel::~bSel()
{
}

nBT::bResult nBT::CMS::bSel::Evaluate(bResult _result)
{
	if (m_eState == nBT::bResult::Evaluation)
	{
		AddEvalueCnt();
		bResult curResult;
		for (int i = 0; i < m_vecChild.size(); ++i)
		{
			curResult = m_vecChild[i]->Evaluate();
			switch (curResult)
			{
			case nBT::bResult::Evaluation:
				break;
			case nBT::bResult::Fail:
				m_eState = nBT::bResult::Fail;
				break;
			case nBT::bResult::Running:
				m_eState = nBT::bResult::Evaluation;
				return  nBT::bResult::Running;

			case nBT::bResult::Success:
				m_eState = nBT::bResult::Success;
				return nBT::bResult::Success;
			}
		}
		m_eState = nBT::bResult::Fail;
		return nBT::bResult::Fail;
	}
	else
	{
		return m_eState;
	}
}

nBT::TSK::bCondition::bCondition()
{
}

nBT::TSK::bCondition::~bCondition()
{
}

nBT::bResult nBT::TSK::bCondition::Evaluate(bResult _result)
{
	return m_eState = nBT::bResult::Fail;
}

nBT::TSK::bAction::bAction()
{
}

nBT::TSK::bAction::~bAction()
{
}

nBT::bResult nBT::TSK::bAction::Evaluate(bResult _result)
{
	return m_eState = nBT::bResult::Fail;
}

nBT::TSK::bDecorate::bDecorate()
{
}

nBT::TSK::bDecorate::~bDecorate()
{
}

nBT::bResult nBT::TSK::bDecorate::Evaluate(bResult _result)
{
	if (m_eState == bResult::Evaluation)
	{
		AddEvalueCnt();
		bResult curResult;
		//	if (&&)
		for (int i = 0; i < m_vecCondition.size(); ++i)
		{
			curResult = m_vecCondition[i]->Evaluate();
			switch (curResult)
			{
			case nBT::bResult::Evaluation:
				break;
			case nBT::bResult::Fail:
				m_eState = nBT::bResult::Fail;
				return  nBT::bResult::Fail;
			case nBT::bResult::Running:
				m_eState = nBT::bResult::Evaluation;
				return nBT::bResult::Running;
	
			case nBT::bResult::Success:
				m_eState = nBT::bResult::Success;
				break;
			}
		}
		for (int i = 0; i < m_vecAction.size(); ++i)
		{
			curResult = m_vecAction[i]->Evaluate();
			switch (curResult)
			{
			case nBT::bResult::Evaluation:
				break;
			case nBT::bResult::Fail:
				m_eState = nBT::bResult::Fail;
				return  nBT::bResult::Fail;
			case nBT::bResult::Running:
				m_eState = nBT::bResult::Evaluation;
				return nBT::bResult::Running;
			case nBT::bResult::Success:
				m_eState = nBT::bResult::Success;
				break;
			}
		}
	}
	else
	{
		return m_eState;

	}

	return m_eState;
}

nBT::TSK::bComplexAct::bComplexAct()
{
}

nBT::TSK::bComplexAct::~bComplexAct()
{
}
nBT::bResult nBT::TSK::bComplexAct::Evaluate(bResult _result)
{
	if (m_eState == bResult::Evaluation)
	{
		int okCnt = m_vecAction.size();
		AddEvalueCnt();
		bResult curResult;
		for (int i = 0; i < m_vecAction.size(); ++i)
		{
			curResult = m_vecAction[i]->Evaluate();
			switch (curResult)
			{
			case nBT::bResult::Evaluation:
				break;
			case nBT::bResult::Fail:
				m_eState = nBT::bResult::Fail;
				return nBT::bResult::Fail;
			case nBT::bResult::Running:
				break;
			case nBT::bResult::Success:
				--okCnt;
				break;
			}
		}
		if (okCnt <= 0)
		{
			m_eState = nBT::bResult::Success;
			return nBT::bResult::Success;
		}
		else
		{
			m_eState = nBT::bResult::Evaluation;
			return nBT::bResult::Running;
		}
	}
	else
	{
		return m_eState;
	}
}
