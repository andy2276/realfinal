#pragma once
#include "Script.h"

enum class BULLET_TYPE {
	STRAIGHT,
	CIRCLE,
	LAYERCIRCLE,
	RANDOM,
	
};


class CBulletBossSkillScript :
	public CScript
{
private:
	vector<CGameObject *> m_Bullets;
public:
	CBulletBossSkillScript();
	virtual ~CBulletBossSkillScript();
	CLONE(CBulletBossSkillScript);


	virtual void awake();
	virtual void update();

	void shoot(UINT eType);

};

