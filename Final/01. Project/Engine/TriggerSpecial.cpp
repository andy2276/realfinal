#include "stdafx.h"
#include "TriggerSpecial.h"
#include "MapMgr.h"
#include "MapRapper.h"
#include "SponSystem.h"
#include "StatusComponent.h"
#include "GameObject.h"

Trigger::Event::Special::Queries::CIfLevelEnd::CIfLevelEnd()
{
}

Trigger::Event::Special::Queries::CIfLevelEnd::~CIfLevelEnd()
{
}

Trigger::Result Trigger::Event::Special::Queries::CIfLevelEnd::Update(Trigger::Result _return)
{
	UINT max = GetSponSystem()->GetMaxRound();
	UINT cur = GetSponSystem()->GetCurRound();
	if (CSponSystem::g_nPossibleInTower > 0)--CSponSystem::g_nPossibleInTower;
	if (cur < max)return Trigger::Result::Fail;
	else return Trigger::Result::Success;
}

Trigger::Event::Special::Queries::CIfSponTypeSame::CIfSponTypeSame()
{
}

Trigger::Event::Special::Queries::CIfSponTypeSame::~CIfSponTypeSame()
{
}

Trigger::Result Trigger::Event::Special::Queries::CIfSponTypeSame::Update(Trigger::Result _return)
{
	if (m_eSponType == GetResponType())return Trigger::Result::Success;
	else return Trigger::Result::Fail;
}

Trigger::Event::Special::Queries::CIfRoundClear::CIfRoundClear()
{
}

Trigger::Event::Special::Queries::CIfRoundClear::~CIfRoundClear()
{
}

Trigger::Result Trigger::Event::Special::Queries::CIfRoundClear::Update(Trigger::Result _return)
{
	if (GetSponSystem()->GetCurCurObjCnt() <= GetSponSystem()->GetCurClearObjCnt()) {
		return Trigger::Result::Success;
	}
	else return Trigger::Result::Fail;
}

Trigger::Event::Special::Works::CDoMonsterSponSequence::CDoMonsterSponSequence()
{
}

Trigger::Event::Special::Works::CDoMonsterSponSequence::~CDoMonsterSponSequence()
{
}

Trigger::Result Trigger::Event::Special::Works::CDoMonsterSponSequence::Update(Trigger::Result _return)
{
	tRoundInfo round = GetSponSystem()->GetCurRoundInfo();
	vector<tMonsterInfo>::iterator itor = round.itorMonster;
	vector<tMonsterInfo>::iterator end = round.monsterInfo.end();
	if (itor == end)return Trigger::Result::Success;
	else if (round.fCurSponDelay < round.fSponDelay) {
		round.fCurSponDelay += DT;
		return Trigger::Result::Wait;
	}
	const tResponer& respon = GetSponSystem()->GetResponer()[(*itor).nResponerIdx];
	GetSponSystem()->SponMonster((*itor).nMonsterType,respon.vPos);
//	--round.nCurMonsterCnt;
	++round.nNowCurMonster;
	++itor;
	round.fCurSponDelay = 0.0f;
	return Trigger::Result::Wait;
}

Trigger::Event::Special::Queries::CIfPossibleInTower::CIfPossibleInTower()
{
}

Trigger::Event::Special::Queries::CIfPossibleInTower::~CIfPossibleInTower()
{
}

Trigger::Result Trigger::Event::Special::Queries::CIfPossibleInTower::Update(Trigger::Result _return)
{
	if (CSponSystem::g_nPossibleInTower <= 0) {
		return Trigger::Result::Success;
	}
	return Trigger::Result::Fail;
}

Trigger::Event::Special::Works::CDoMoveMapOn::CDoMoveMapOn()
{
}

Trigger::Event::Special::Works::CDoMoveMapOn::~CDoMoveMapOn()
{
}

Trigger::Result Trigger::Event::Special::Works::CDoMoveMapOn::Update(Trigger::Result _return)
{
	GetMapRapper()->MapMoveOn();
	return Trigger::Result::Success;
}

Trigger::Event::Special::Works::CDoRoundEnd::CDoRoundEnd()
{
}

Trigger::Event::Special::Works::CDoRoundEnd::~CDoRoundEnd()
{
}

Trigger::Result Trigger::Event::Special::Works::CDoRoundEnd::Update(Trigger::Result _return)
{
	GetSponSystem()->SetCurRound( GetSponSystem()->GetCurRound() + 1);
	return Trigger::Result::Success;
}

Trigger::Event::Special::Works::CDoMonsterSponAtOnce::CDoMonsterSponAtOnce()
{
}

Trigger::Event::Special::Works::CDoMonsterSponAtOnce::~CDoMonsterSponAtOnce()
{
}

Trigger::Result Trigger::Event::Special::Works::CDoMonsterSponAtOnce::Update(Trigger::Result _return)
{
	std::vector< tMonsterInfo>& mopInfo = GetSponSystem()->GetCurRoundInfo().monsterInfo;
	std::vector< tMonsterInfo>::iterator itor = GetSponSystem()->GetCurRoundInfo().itorMonster;
	std::vector< tResponer>& responer = GetSponSystem()->GetResponer();
	for (; itor != mopInfo.end(); ++itor) {
		GetSponSystem()->SponMonster((*itor).nMonsterType, responer[(*itor).nResponerIdx].vPos);
	}
	return Trigger::Result::Success;
}
