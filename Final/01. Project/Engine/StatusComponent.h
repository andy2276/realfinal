#pragma once
#include"Component.h"
#include"Ptr.h"
#include"Sound.h"
enum class STATUS_TYPE
{
	INT,
	FLOAT,
	VEC3,
	End,
};
enum class STATUS_FLOAT {
	HP,
	MAX_HP,
	MP,
	MAX_MP,
	MASS,
	SEARCHRANGE,
	ATTACKRANGE,
	MINRANGE,
	ROTSPEED,
	MOVESPEED,
	SKILL_1_COOL_MAX,
	SKILL_1_COOL_NOW,
	SKILL_2_COOL_MAX,
	SKILL_2_COOL_NOW,
	SKILL_HEAL_COOL_MAX,
	SKILL_HEAL_COOL_NOW,
	ATTACK_BAGIC_COOL_NOW,
	ATTACK_BAGIC_COOL_MAX,
	TIME_ACCEL,				//	지민추 이거한이유는 속도 증가사용하려고
	End
};
enum class STATUS_VEC3 {
	VELOCITY,
	ACCELERATION,
	FRICTION,
	INIT_POS,
	INIT_ROT,
	LOCAL_POS,
	LOCAL_POS_OFFSET,
	LOCAL_ROT,
	LOCAL_SCALE,
	TARGET_POS,
	TARGET_DIR,
	TARGET_VECTOR,
	IINT_POS,
	BORDER_POS,
	BORDER_SCALE,
	UI_HP_SCALE,
	UI_BLACK_HP_SCALE,
	End
};
enum class MONSTER_TYPE {
	 Player = 0
	,Stone = 1
	,Skell = 2
	,Mimic = 3
};

struct tCollision {
	CGameObject* pObj;		//	충돌한 오브젝트
	UINT		nObjID;
	Vec3		vColDir;
	int			nColType;
};


class CStatusComponent : public CComponent
{
private : 
	bool m_bAIUpdate;
	bool temp;
	EC_MOSTER_TYPE m_eMosterType;
	float m_fValue[(UINT)STATUS_FLOAT::End];
	Vec3 m_vec3Value[(UINT)STATUS_VEC3::End];

	//	지민추
	BYTE m_bColNumId;
	int m_nMapNum;

	MONSTER_TYPE m_eMonsterType;
	std::list<tDamageStack> m_tDamageStack;
//	float m_acceleration;
//	float m_friction;
	//float m_velocity;

	bool boN;

	bool	m_bPlayerDeath;

	//Vec3 m_velocity;
	//Vec3 m_acceleration;
	//Vec3 m_friction;
	//
	//
	//Vec3		m_vLocalPos;
	//Vec3		m_vLocalScale;
	//Vec3		m_vLocalRot;
	//Vec3		m_vLocalPosOffset;
	//
	//Vec3		m_vLocalDir[(UINT)DIR_TYPE::END];
	//Vec3		m_vWorldDir[(UINT)DIR_TYPE::END];

	CGameObject * m_characterUI;
	CGameObject * m_HpUI;
	CGameObject * m_BlackHpUI;
	//	지민추
	bool			m_bInit;
	Vec3 m_HpPos;
	Vec3 m_hpScale;

	CGameObject * pPlayer;
	bool isOwnSkill[(UINT)PLAYER_SKILL_TYPE::END];//true이면 그 스킬을 가지고 있다는 뜻임. 
	//	지민추
	bool m_bLowHp;

	int m_totalSkillCount = 20; //아이콘 총 개수 
	int m_curOwnSkillCount = 0; // 가지고있는 스킬의 개수 
	int m_ArrOwnSkill[20];
	
	CGameObject *		m_gameoverUI;
	bool				m_bGOVERcontrol;
	bool				m_bIsPlayer;

	Ptr<CSound> pSound;

public:
	CStatusComponent();
	virtual ~CStatusComponent();

	virtual void update();
	virtual void finalupdate() {};

	virtual void SaveToScene(FILE* _pFile) {};
	virtual void LoadFromScene(FILE* _pFile) {};
	virtual CStatusComponent* Clone() { return new CStatusComponent(*this); }

	void setHp(float hp) { m_fValue[(UINT)STATUS_FLOAT::HP] = hp; }
	float getHp() { return  m_fValue[(UINT)STATUS_FLOAT::HP]; }

	void setMp(float hp) { m_fValue[(UINT)STATUS_FLOAT::MP] = hp; }
	float getMp() { return  m_fValue[(UINT)STATUS_FLOAT::MP]; }

	const float& GetFloat(const STATUS_FLOAT& _var) {return m_fValue[(UINT)_var];}
	const float& GetFloat(const USHORT& _var) { return m_fValue[_var]; }
	void SetFloat(const STATUS_FLOAT& _var, const float& _value) { m_fValue[(UINT)_var] = _value; }
	void SetFloat(const UINT& _var, const float& _value) { m_fValue[_var] = _value; }

	const Vec3& GetVec3(const UINT& _var) { return m_vec3Value[_var]; }
	const Vec3& GetVec3(const STATUS_VEC3& _var) { return m_vec3Value[(UINT)_var]; }
	void SetVec3(const STATUS_VEC3& _var, const Vec3& _value) { m_vec3Value[(UINT)_var] = _value; }
	void SetVec3(const UINT& _var, const Vec3& _value) { m_vec3Value[_var] = _value; }

	void SetAIUpdate(const bool& _update) { m_bAIUpdate = _update; }
	const bool& IsAIUpdate() { return m_bAIUpdate; }

	const EC_MOSTER_TYPE& GetMonsterType() { return m_eMosterType; }
	void SetMonsterType(EC_MOSTER_TYPE _type) { m_eMosterType = _type; }
	void SetColNumId(BYTE _id) { m_bColNumId = _id; }
	BYTE GetColNumId() { return m_bColNumId; }
	void SetMapNum(int _mapNum) { m_nMapNum = _mapNum; }
	int GetMapNum() { return m_nMapNum; }

	//	충돌타입, 충돌한 객체(스킬)	,충돌한 방향(스킬의 진행방향 아마 월드 Dir혹은 가지고 있는 방향),이거 데미지
	void Collision(int _colType, CGameObject* _pCollisionObj, const Vec3& _vCollisionDir, float _pDamage);
public:

	void AddForce(float x,float y , float z, float time);
	void AddForce(const Vec3& _dir, const float& _time);
	
	void setVelocity(float x, float y, float z);

	void setHpDown(float x,bool reset=false); 

	void SetStone(const Vec3 & _pos, const Vec3 & _rot);
	void SetSkelleton(const Vec3 & _pos, const Vec3 & _rot);
	void SetMimic(const Vec3 & _pos, const Vec3 & _rot);

	void DamageStack(UINT ID, const Vec3& _dir, float _damage ,UINT _state = 0);
	std::list<tDamageStack>& GetDamageStack() { return m_tDamageStack; }


	void SetHpUI(UINT u);

	void setPlayer(CGameObject * p) { pPlayer = p; }
	

	void setBool(bool b) { boN = b; };
	bool getBool() { return boN; }

	//	지민 추
	void LowHp();
	const bool& GetLowHp() { return m_bLowHp; }

	bool GetPlayerDeath() { return m_bPlayerDeath; }
	void SetPlayerDeath(bool _bDeath) { m_bPlayerDeath = _bDeath; }
	
	void setIsPlayer(bool b) { m_bIsPlayer = b; }

	void OnHpUies();
	void OffHpUies();

	void UpdateCoolTime(float _toc);
};

