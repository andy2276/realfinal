#pragma once

class CComponent;
class CTransform;
class CAnimator3D;
class CStatusComponent;
class CGameObject;
class CSkillPlatform;

namespace BT
{
	namespace Tool
	{
		class CBlackBoard;
		class CAIProperty;
	}
}

namespace BT
{
	//	다시 처음부터 평가를 할건데, 중요한건 한번 돌았을때가 중요한것이다.
	//	처음에 매번 확인해야하는 녀석들을 넣어놓고, 리턴만 새로운걸로 리턴을 하고, 상태는 여전히 평가를 하게 한다면
	//	다시 평가를 할때 그곳부터 확인을 할것이다.
	//	처음에 무조건 평가 함수를 실행시켜놓고 혹은 상태를 확인하고 들어가는 최적화도 나쁘지 않을것 같다.
	
	//	<완전 개별정보 (statusComponent)>
	//	최대체력,이동속도,회전속도,스킬현재123,스킬맥스123,TARGET_POS,MINRANGE
	//	<BT내 개별정보>
	//	가지고있는스킬,스킬사용가능여부,스킬사거리123,페이즈가능여부123,페이즈체력123,현재페이즈,
	//	기본공격가능여부,기본공격사거리
	namespace ObjInfos
	{
		struct tSkillInfo
		{
			float _fCoolMax;
			float _fAttackAngle;
			float _fAttackRange;
			float _fAttackDamage;
			float _fShootTime;	//	공격 가능시간을 뜻하기도함
		};
		struct tInit
		{
			float						a_fHPMax;
			float						b_fMoveSpeed;
			float						c_fRotateSpeed;
			float						da_fLookAngle;
			float						db_fLookDist;
			
			bool						ea_bHaveSkills[4];	//	[ 0 : skill 1][ 1 : skill 2][ 2 : skill3][3 : bagicAttack]
			BT::ObjInfos::tSkillInfo	eb_tSkillInfo[4];
		};
	}
	//	실시간으로 업데이트 해야하는것
	//	공용
	//	플레이어 위치
	//	개인
	//	자신의 체력,스킬 사용유무
	enum class Return : short
	{
		Evaluation = -1
		,Failure = 0
		,Succeess = 1
		,Running = 2
	};
	//	
	//	
	//	
	namespace Virtual
	{
		class CNode
		{
		private:
			static	UINT	g_nID;
			UINT			m_nID;
			wstring			m_wstrName;
		protected:
			CGameObject*	m_pObj;
			BT::Tool::CBlackBoard*			m_pBlackBoard;
			BT::Tool::CAIProperty*			m_pProperty;
			BT::Virtual::CNode*				m_pParent;
			BT::Return						m_eState;		//	현시간 확인값
			bool							m_bRecordType;	//	true : default : once , false : Infinity 
		public:
			CNode()
				:m_wstrName()
				, m_pObj(nullptr)
				, m_pBlackBoard(nullptr)
				, m_pParent(nullptr)
				, m_eState(BT::Return::Evaluation)
				, m_bRecordType(true)
			{
				m_nID = g_nID++;
			};
			virtual ~CNode() {};
		//	==========================================================
		//	Bagic Interface

			//	Setting

			const wstring& GetName() { return m_wstrName; }
			CGameObject* GetObj() { return m_pObj; }
			BT::Virtual::CNode* GetParent() { return m_pParent; }
			virtual BT::Virtual::CNode* GetChild(int _idx) { return nullptr; }
			BT::Tool::CBlackBoard* GetBlackBoard(BT::Tool::CBlackBoard* _black) { return m_pBlackBoard; }
			const UINT& GetID() { return m_nID; }
			BT::Tool::CAIProperty* GetProperty() { return m_pProperty; }

			virtual void SetData(int _offset, void* pData) {};
			void SetBlackBoard(BT::Tool::CBlackBoard* _black) { m_pBlackBoard = _black; }
			void SetName(const wstring& _name) { m_wstrName = _name; }
			void SetObj(CGameObject* _pObj) { m_pObj = _pObj; }
			void SetParent(BT::Virtual::CNode* _p) { m_pParent = _p; }
			void SetProperty(BT::Tool::CAIProperty* _prop) { m_pProperty = _prop; }
			virtual void AddChild(BT::Virtual::CNode*) {};

			void SetRecordOnce() { m_bRecordType = true; }
			void SetRecordInfinity() { m_bRecordType = false; }

			void SetState(BT::Return _state) { m_eState = _state; }
			BT::Return GetState() { return m_eState; }

			CSkillPlatform* Skills();
			float Toc();

			virtual void StateReset() {};
			virtual void SoftReset() {};
			virtual void HardReset() {};

		//	==========================================================
		//	GetObjectInfo

			CTransform* Transform();
			CAnimator3D* Animator3D();
			CStatusComponent* Status();

			virtual BT::Return Evaluate() = 0;
		};

		class CComposite : public CNode
		{
		protected:
			vector<BT::Virtual::CNode*>		m_pChild;
			bool							m_bRetry;
		public:
			CComposite()
				:m_bRetry(false)
			{};
			virtual ~CComposite() {};

			void AddChild(BT::Virtual::CNode* _child) { _child->SetParent(this); m_pChild.push_back(_child); }
			BT::Virtual::CNode* GetChild(int _idx) { return m_pChild[_idx]; }
			vector<BT::Virtual::CNode*>& GetChildVector() { return m_pChild; }
			
			virtual void StateReset();
			virtual void SoftReset();
			virtual void HardReset();

			virtual BT::Return Evaluate() = 0;
		};
		class CCondition :public BT::Virtual::CNode
		{
		public:
			CCondition() {};
			virtual ~CCondition() {};

			virtual BT::Return Evaluate() = 0;
		};
		class CAction :public BT::Virtual::CNode
		{
		public:
			CAction() {};
			virtual ~CAction() {};

			virtual BT::Return Evaluate() = 0;
		};
	}
	namespace Collect
	{
		class CSelection :public BT::Virtual::CComposite
		{
		public:
			CSelection() {};
			virtual ~CSelection() {};

			virtual void StateReset();
			virtual void SoftReset();
			virtual void HardReset();

			virtual BT::Return Evaluate();
		};
		class CSequence :public BT::Virtual::CComposite
		{
		public:
			CSequence() {};
			virtual ~CSequence() {};

			virtual void StateReset();
			virtual void SoftReset();
			virtual void HardReset();

			virtual BT::Return Evaluate();
		};
		//	이거는 무조건 실행시키는 녀석 기본은 셀렉션
		class CReDecorate : public BT::Virtual::CComposite
		{
		private:
			int			m_nType;
		public:
			CReDecorate():m_nType(0){};
			virtual ~CReDecorate() {};

			virtual void StateReset();
			virtual void SoftReset();
			virtual void HardReset();

			//	offset : 0, pData : int type(sel == 0, seq == 1)
			virtual void SetData(int _offset, void* pData);

			virtual BT::Return Evaluate();
		};

		//	아직 만드는중
		class CCollaborate : public BT::Virtual::CComposite
		{
		public:
			struct tCollabo
			{
				//	
				short _type = 0;
				BT::Return reValue = BT::Return::Evaluation;
				//	0 : 끝나면 그대로 끝남 1 :  0의 녀석들이 모두 끝나면 관계없이 종료하지만 먼저끝나면 끝남
				//	2 : 무한정 반복을 하며 1과같지만 무한정 반복을함. 0의 녀석들이 끝나야 끝남
			};
		private:
			vector<tCollabo> m_vecCollect;

		public:
			CCollaborate()  {};
			virtual ~CCollaborate() {};

			virtual void StateReset();
			virtual void SoftReset();
			virtual void HardReset();

			//	offset : -1, pData : init;
			//	offset : x(0 < = x < 16384), pData : tCollabo;
			virtual void SetData(int _offset, void* pData);

			virtual BT::Return Evaluate();
		};
	}
	namespace Tool
	{
		class CBlackBoard
		{
		private:
			Vec3			m_v3PlayerPos;
			Vec3			m_v3PlayerLook;
			Vec3			m_v3PlayerRight;

			Vec3			m_v3NoYPlayerPos;
			
			Vec3			m_v3MainCamPos;

			bool			m_bIsLook;
		public:
			CGameObject*	pPlayer;
			int				arrMappingAnim[(UINT)EC_MAPPING_ANIM::End];

			CBlackBoard();
			~CBlackBoard();

			void SetMappingAnimIdx(EC_MAPPING_ANIM _bossAct, int _destAnim = -1)
			{
				arrMappingAnim[(UINT)_bossAct] = _destAnim;
			};
			bool IsPossibleAnim(int _idx)
			{
				if (arrMappingAnim[_idx] < 0)return false;
				else return true;
			}
			int GetMappingAnimIdx(int _idx)
			{
				return arrMappingAnim[_idx];
			}

			//	playerDist
			void UpdateBlackBoard();
			
			//	플레이어와 나의 거리(nonZ)
			const float& PlayerToDist(const Vec3& _mePos);
			//	플레이어를 바라보는 방향
			const Vec3& PlayerToLook(const Vec3& _mePos);

			//	PlayerToLook 이것후에 불러야 함
			const bool& IsLookPlayer();


			//	플레이어와 나의 각도 및 앵글
			const float& PlayerToAngle(const Vec3& _mePos,const Vec3& _meLook);

		};
		class CAIProperty
		{
		private:
			std::map<wstring, BT::Virtual::CNode*>	m_mapNodes; //	검색용
			vector< BT::Virtual::CNode*>			m_vecNodes;	//	전체적용용
			float									m_fToc;
			CSkillPlatform*							m_pSkillPlatform;

			float									m_fDamaged;
			float									m_fBagicAttackDmg;
		public:
			std::list<tDamageStack>*			StackDmg;

			CAIProperty();
			~CAIProperty();

			std::map<wstring, BT::Virtual::CNode*>& GetNodeMap() { return m_mapNodes; }
			vector< BT::Virtual::CNode*>& GetNodeVector() { return m_vecNodes; }
			BT::Virtual::CNode* Search(const wstring& _name);

			void StatusReset();


			void SetToc(float _toc) { m_fToc = _toc; }
			float GetToc() { return m_fToc; }

			void SetSkillPlatform(CSkillPlatform*	_pSkillPlatform) { m_pSkillPlatform = _pSkillPlatform; }
			CSkillPlatform* GetSkillPlatform() { return m_pSkillPlatform; }
			CSkillPlatform* Skills(){ return m_pSkillPlatform; }

			float GetDamaged() { return m_fDamaged; }
			void SetDamaged(float _dmg) { m_fDamaged = _dmg; }

			float GetBagicAttackDmg() { return m_fBagicAttackDmg; }
			void SetBagicAttackDmg(float _dmg) { m_fBagicAttackDmg = _dmg; }
		};
		class CBTMaker
		{
		public:
			CGameObject*							pObj;
			CBlackBoard*							pBlackBoard;
			CAIProperty*							pProperty;

			BT::Virtual::CNode*						pRoot;
			BT::Virtual::CNode*						pPreParent;
			BT::Virtual::CNode*						pCurParent;
			BT::Virtual::CNode*						pPreNode;
			BT::Virtual::CNode*						pCurNode;
			BT::Virtual::CNode*						pPreChild;
			BT::Virtual::CNode*						pCurChild;
			BT::Virtual::CNode*						pFindNode;


			vector< BT::Virtual::CNode*>*			vecNodes;
			std::map<wstring, BT::Virtual::CNode*>*	pMapNodes;

		public:
			

			CBTMaker();
			~CBTMaker();

			void Init(CGameObject* _pObj, BT::Tool::CBlackBoard* _black);
			void Init(CGameObject* _pObj, BT::Tool::CBlackBoard* _black, CSkillPlatform* _plat);

			template<typename T>
			CBTMaker* NeoNode(const wstring& _name);

			template<typename T>
			CBTMaker* NeoRoot(const wstring& _name);

			template<typename T>
			CBTMaker* NeoParent(const wstring& _name);
			template<typename T>
			CBTMaker* NeoChild(const wstring& _name);

			BT::Tool::CBTMaker* Search(const wstring& _name);

			//	SetData
			CBTMaker* SetData(int _offset, void* _pData);

			//	커서변경
			CBTMaker* Parent(const wstring& _name);
			//	Tap
			CBTMaker* a____();

			void Final(){}


			//	직접 접근
			//	parent
			BT::Virtual::CNode* DirectParent();

			BT::Virtual::CNode* GetRoot() { return pRoot; }
			BT::Tool::CAIProperty* GetProperty() { return pProperty; }

		};
		

		template<typename T>
		inline CBTMaker * CBTMaker::NeoNode(const wstring & _name)
		{
			pPreNode = pCurNode;
			pCurNode = new T;
			pCurNode->SetName(_name);
			pCurNode->SetObj(pObj);
			pCurNode->SetBlackBoard(pBlackBoard);
			pMapNodes->emplace(make_pair(pCurNode->GetName(), pCurNode));
			vecNodes->push_back(pCurNode);
			return this;
		}

		template<typename T>
		inline CBTMaker * CBTMaker::NeoRoot(const wstring & _name)
		{
			pRoot = new T;
			pPreNode = nullptr;
			pCurNode = pRoot;
			pPreParent = nullptr;
			pCurParent = pRoot;
			pRoot->SetName(_name);
			pRoot->SetObj(pObj);
			pRoot->SetBlackBoard(pBlackBoard);
			pRoot->SetProperty(pProperty);
			pMapNodes->emplace(make_pair(pRoot->GetName(), pRoot));
			vecNodes->push_back(pRoot);
			return this;
		}

		template<typename T>
		inline CBTMaker * CBTMaker::NeoParent(const wstring & _name)
		{
			pPreParent = pCurParent;
			pCurParent = new T;
			pCurParent->SetName(_name);
			pCurParent->SetObj(pObj);
			pCurParent->SetBlackBoard(pBlackBoard);
			pCurParent->SetProperty(pProperty);
			pPreNode = pCurNode;
			pCurNode = pCurParent;
			pMapNodes->emplace(make_pair(pCurParent->GetName(), pCurParent));
			vecNodes->push_back(pCurParent);
			return this;
		}
		template<typename T>
		inline CBTMaker * CBTMaker::NeoChild(const wstring & _name)
		{
			pPreChild = pCurChild;
			pCurChild = new T;
			pCurChild->SetName(_name);
			pCurChild->SetObj(pObj);
			pCurChild->SetBlackBoard(pBlackBoard);
			pCurChild->SetParent(pCurParent);
			pCurChild->SetProperty(pProperty);

			pCurParent->AddChild(pCurChild);
			pPreNode = pCurNode;
			pCurNode = pCurChild;
			pMapNodes->emplace(make_pair(pCurChild->GetName(), pCurChild));
			vecNodes->push_back(pCurChild);
			return this;
		}

	}
	
}