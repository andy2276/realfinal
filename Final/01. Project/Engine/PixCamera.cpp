#include "stdafx.h"
#include "PixCamera.h"
#include "Camera.h"
#include "Transform.h"
#include "MapMgr.h"

CPixCamera::CPixCamera()
	:CScript((UINT)SCRIPT_TYPE::CAM)
	, m_nShudderPatternCur(-1)
	, m_bShuuderStop(false)
	,m_bDeathEvent(false)
{
	tShudder right = {};
	{
		right.nFrameCnt = 2;
		right.vFramePos = Vec3::Zero;
		right.vFrameRot = Vec3(0.f, XM_ANGLE, 0.0f);
	}
	tShudder left = {};
	{
		left.nFrameCnt = 2;
		left.vFramePos = Vec3::Zero;
		left.vFrameRot = Vec3(0.f, -XM_ANGLE, 0.0f);
	}
	tShudder up = {};
	{
		up.nFrameCnt = 2;
		up.vFramePos = Vec3::Zero;
		up.vFrameRot = Vec3(XM_ANGLE, 0.0f, 0.0f);
	}
	tShudder down = {};
	{
		down.nFrameCnt = 2;
		down.vFramePos = Vec3::Zero;
		down.vFrameRot = Vec3(-XM_ANGLE, 0.f, 0.0f);
	}
	tShudder rightUp = {};
	{
		rightUp.nFrameCnt = 2;
		rightUp.vFramePos = Vec3::Zero;
		rightUp.vFrameRot = Vec3(XM_ANGLE, XM_ANGLE, 0.0f);
	}
	tShudder leftDown = {};
	{
		leftDown.nFrameCnt = 2;
		leftDown.vFramePos = Vec3::Zero;
		leftDown.vFrameRot = Vec3(-XM_ANGLE, -XM_ANGLE, 0.0f);
	}
	tShudder leftUp = {};
	{
		leftUp.nFrameCnt = 2;
		leftUp.vFramePos = Vec3::Zero;
		leftUp.vFrameRot = Vec3(XM_ANGLE, -XM_ANGLE, 0.0f);
	}
	tShudder SkillBack = {};
	{
		SkillBack.nFrameCnt = 2;
		SkillBack.vFramePos = Vec3(0.f,0.f,-4.0f);
		SkillBack.vFrameRot = Vec3(0.f, 0.f, 0.0f);
	}
	tShudder Skillfront = {};
	{
		Skillfront.nFrameCnt = 4;
		Skillfront.vFramePos = Vec3(0.f, 0.f, 2.f);
		Skillfront.vFrameRot = Vec3(0.f, 0.f, 0.0f);
	}


	m_tSkillShudder;
	{
		tShudderPattern shudder = {};
		shudder.wstrName = L"DamagedShudderRRUD";
		right.nFrameCnt = 2;
		shudder.vecShudder.push_back(right);
		leftUp.nFrameCnt = 6;
		shudder.vecShudder.push_back(leftUp);
		down.nFrameCnt = 8;
		shudder.vecShudder.push_back(down);

		m_vecShdderPAttern.push_back(shudder);
	}
	{
		tShudderPattern shudder = {};
		shudder.wstrName = L"DamagedShudderLDRU";
		leftDown.nFrameCnt = 2;
		shudder.vecShudder.push_back(leftDown);
		rightUp.nFrameCnt = 6;
		shudder.vecShudder.push_back(rightUp);

		m_vecShdderPAttern.push_back(shudder);
	}
	{
		tShudderPattern shudder = {};
		shudder.wstrName = L"Skill";
		SkillBack.nFrameCnt = 2;
		shudder.vecShudder.push_back(SkillBack);
		Skillfront.nFrameCnt = 6;
		shudder.vecShudder.push_back(Skillfront);

		m_tSkillShudder = shudder; 
	}
	m_pCurShudder = &m_vecShdderPAttern[0];

	int nFrame = 0;
	tShudder deatFront = {};
	{
		deatFront.nFrameCnt = 41;
		deatFront.vFramePos = Vec3(0.f, 0.5f, 0.73170731f);
		deatFront.vFrameRot = Vec3(XM_ANGLE * 2.f, 0.f, 0.0f);
	}
	tShudder deathTurnUp = {};
	{
		deathTurnUp.nFrameCnt = 400;
		deathTurnUp.vFramePos = Vec3(0.f, 0.5f, 0.f);
		deathTurnUp.vFrameRot = Vec3(0.f, XM_ANGLE, 0.0f);
	}
	{
		tShudderPattern shudder = {};
		shudder.wstrName = L"DeathShudder";
		deatFront.nFrameCnt = 41;
		shudder.vecShudder.push_back(deatFront);
		deathTurnUp.nFrameCnt = 441;
		shudder.vecShudder.push_back(deathTurnUp);

		m_tDeathShudder = shudder;
	}
}


CPixCamera::~CPixCamera()
{
	
}

void CPixCamera::update()
{
	if (m_bShuuderStop)
	{
		if (!m_bDeathEvent)
		{
			m_bShuuderStop = false;
		}
		else
		{
			return;
		}
	}
	
	if (m_bSudderOn)
	{
		if (!m_bShudderStart)
		{
			if (m_nShudderCurOrder < m_pCurShudder->vecShudder.size())
			{
				if (m_nShudderCurFrame < m_pCurShudder->vecShudder[m_nShudderCurOrder].nFrameCnt)
				{
					Vec3 camPos = Transform()->GetLocalPos();
					Vec3 camRot = Transform()->GetLocalRot();

					camPos += m_pCurShudder->vecShudder[m_nShudderCurOrder].vFramePos;
					camRot += m_pCurShudder->vecShudder[m_nShudderCurOrder].vFrameRot;

					++m_nShudderCurFrame;

					Transform()->SetLocalPos(camPos);
					Transform()->SetLocalRot(camRot);
				}
				else
				{
					++m_nShudderCurOrder;
				}
			}
			else
			{
				if (!m_bDeathEvent)
				{
					m_bSudderOn = false;
					m_bShudderStart = true;
					m_nShudderCurOrder = 0;
					m_nShudderCurFrame = 0;

					//	기존으로 돌아가기
					Transform()->SetLocalPos(m_vChangePos);
					Transform()->SetLocalRot(m_vChangeRot);
				}
				else
				{
					m_bShuuderStop = true;
				}
				
			}
		}
		else
		{
			m_nShudderCurOrder = 0;
			m_nShudderCurFrame = 0;
			m_bShudderStart = false;

			m_vChangePos = CMapMgr::GetInst()->GetMainCam()->Transform()->GetLocalPos();
			m_vChangeRot = CMapMgr::GetInst()->GetMainCam()->Transform()->GetLocalRot();
		}
	}
}

void CPixCamera::RandomShudder()
{
	m_bSudderOn = true;
	++m_nShudderPatternCur;
	m_nShudderPatternCur = m_nShudderPatternCur % m_vecShdderPAttern.size();
	m_pCurShudder = &m_vecShdderPAttern[m_nShudderPatternCur];
}

void CPixCamera::Skill1Shudder()
{
	m_pCurShudder = &m_tSkillShudder;
	m_bSudderOn = true;
}

void CPixCamera::DeathShudder()
{
	m_pCurShudder = &m_tDeathShudder;
	m_bSudderOn = true;
}
