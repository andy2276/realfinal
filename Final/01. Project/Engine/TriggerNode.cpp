#include "stdafx.h"
#include "TriggerNode.h"
#include "SponSystem.h"
#include "GameObject.h"

UINT Trigger::Base::CBasis::g_nId = 0;

Trigger::Base::CBasis::CBasis() 
	: m_pStack(nullptr)
	, m_pShareHouse(nullptr)
	, m_pGameObj(nullptr)
	, m_pSponSystem(nullptr)
{
	m_nId = g_nId++;
	m_wstrName  = __FUNCTIONW__;
	m_wstrName += std::to_wstring(m_nId);
}

Trigger::Base::CBasis::~CBasis()
{
	m_pStack = nullptr;
	m_pGameObj->GetScript<CSponSystem>();
}

EC_RESPON_TYPE Trigger::Base::CBasis::GetResponType(UINT _idx)
{
	 return m_pSponSystem->GetResponType(_idx); 
}

EC_RESPON_TYPE Trigger::Base::CBasis::GetResponType()
{
	 return m_pSponSystem->GetCurResponType();
}

Trigger::Base::CControler::CControler()
{
}

Trigger::Base::CControler::~CControler()
{
	for (int i = 0; i < m_vecChild.size(); ++i) {
		if (m_vecChild[i] != nullptr)delete m_vecChild[i];
		m_vecChild[i] = nullptr;
	}
}

Trigger::Fork::CProcedurer::CProcedurer()
{
}

Trigger::Fork::CProcedurer::~CProcedurer()
{
}

Trigger::Result Trigger::Fork::CProcedurer::Update(Trigger::Result _return)
{
	Trigger::Result result;
	vector<Trigger::Base::CBasis*>& vec = *GetChildren();
	//	최초 평가시에 사용
	if (Trigger::Result::Evaluate == _return) {
		PushStack(this);//	자기 자신을 일단 쌓아놓는다
		for (int i = 0; i < vec.size(); ++i) {
			result = vec[i]->Update(Trigger::Result::Evaluate);	//	자식을 일단 먼저 실행시킨다
			switch (result){
			case Trigger::Result::Wait:
				//	지금 기다리라는 명령이 내려진 자식을 쌓아놓는다.
				PushStack(vec[i]); 
				SetCurIdx(i);	//	현재 인덱스를 저장한다. 이게 현재 실행되고있는 녀석일거임.
				return Trigger::Result::Wait;
			case Trigger::Result::Success:
				//	자식이 성공하면 계속 진행한다 그러면 자식은 계속 전체적으로 실행될거임
				break;
			case Trigger::Result::Fail:
			case Trigger::Result::Evaluate:
				ResetIdx();
				PopStack();	//	자기자신을 빼버린다.
				return Trigger::Result::Fail;
			}
		}
		//	자기자신의 모든 자식이 성공했기에 자기자신을 이제 평가할이유가 없으니 뺀다
		ResetIdx();
		PopStack();
		return Trigger::Result::Success;	//	그리고 성공을 리턴
	}
	else {
		//	자식이나 이전의 것이 리턴으로 결과 값을 준다.
		//	외부는 지금 스텍에서 탑해서 가장 최근스택을 얻고
		//	그 녀석을 실행 후(최종 끝에 있는것은 무조건 말단 노드이므로) 결과를 저장하므로 문제가없음 여기만 잘처리해주면됨
		//	여기서 처리하는것은 성공과 실패뿐 그 나머지는 여기서 처리하지 않는다. 
		switch (_return)
		{
		case Trigger::Result::Wait:
			//	이게 들어올리도 없지만	이거에 대한 처리는 외부에서한다. 나중에 다시만들때는 하나에서 처리할수 있게하자.
			return Trigger::Result::Wait;
		case Trigger::Result::Success:
			//	자기자식이 성공시 다음 인덱스를 실행시켜준다.
			for (int i = GetCurIdx() + 1; i < vec.size(); ++i) {
				result = vec[i]->Update(Trigger::Result::Evaluate);
				switch (result) {
				case Trigger::Result::Wait:
					//	지금 기다리라는 명령이 내려진 자식을 쌓아놓는다.
					PushStack(vec[i]);
					SetCurIdx(i);	//	현재 인덱스를 저장한다. 이게 현재 실행되고있는 녀석일거임.
					return Trigger::Result::Wait;
				case Trigger::Result::Success:
					//	자식이 성공하면 계속 진행한다 그러면 자식은 계속 전체적으로 실행될거임
					break;
				case Trigger::Result::Fail:
				case Trigger::Result::Evaluate:
					ResetIdx();
					PopStack();	//	자기자신을 빼버린다.
					return Trigger::Result::Fail;
				}
			}
			ResetIdx();
			PopStack();
			return Trigger::Result::Success;
		case Trigger::Result::Fail:
		case Trigger::Result::Evaluate:
			ResetIdx();		//	자기 자식들까지 모두 초기화 해준다
			PopStack();		//	그리고 자기자신을 다시 팝시켜준다.
			return Trigger::Result::Fail;
		}
	}
	return Trigger::Result::Fail;
}

Trigger::Fork::CChooser::CChooser()
{
}

Trigger::Fork::CChooser::~CChooser()
{
}

Trigger::Result Trigger::Fork::CChooser::Update(Trigger::Result _return)
{
	Trigger::Result result;
	vector<Trigger::Base::CBasis*>& vec = *GetChildren();
	
	if (Trigger::Result::Evaluate == _return) {
		PushStack(this);
		for (int i = 0; i < vec.size(); ++i) {
			result = vec[i]->Update(Trigger::Result::Evaluate);	
			switch (result) {
			case Trigger::Result::Wait:
				PushStack(vec[i]);
				SetCurIdx(i);	
				return Trigger::Result::Wait;
			case Trigger::Result::Fail:
				break;
			case Trigger::Result::Success:
			case Trigger::Result::Evaluate:
				ResetIdx();
				PopStack();	
				return Trigger::Result::Success;
			}
		}
		ResetIdx();
		PopStack();
		return Trigger::Result::Fail;
	}
	else {
		switch (_return)
		{
		case Trigger::Result::Wait:
			return Trigger::Result::Wait;
		case Trigger::Result::Fail:
		{
			for (int i = GetCurIdx() + 1; i < vec.size(); ++i) 
			{
				result = vec[i]->Update(Trigger::Result::Evaluate);
				switch (result) 
				{
				case Trigger::Result::Wait:
					PushStack(vec[i]);
					SetCurIdx(i);	
					return Trigger::Result::Wait;
				case Trigger::Result::Fail:
					break;
				case Trigger::Result::Success:
				case Trigger::Result::Evaluate:
					ResetIdx();
					PopStack();	
					return Trigger::Result::Success;
				}
			}
			ResetIdx();
			PopStack();
			return Trigger::Result::Fail;
		}
		case Trigger::Result::Success:
		case Trigger::Result::Evaluate:
			ResetIdx();	
			PopStack();	
			return Trigger::Result::Success;
		}
	}
	return Trigger::Result::Success;
}

Trigger::Base::CQuery::CQuery()
{
}

Trigger::Base::CQuery::~CQuery()
{
}

Trigger::Result Trigger::Base::CQuery::Update(Trigger::Result _return)
{
	return Result::Fail;
}

Trigger::Base::CWork::CWork()
{
}

Trigger::Base::CWork::~CWork()
{
}

Trigger::Result Trigger::Base::CWork::Update(Trigger::Result _return)
{
	return Result::Fail;
}
