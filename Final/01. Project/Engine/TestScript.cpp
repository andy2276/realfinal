#include "stdafx.h"
#include "TestScript.h"
#include "MapMgr.h"
#include "Transform.h"
#include "Camera.h"
#include "Animator3D.h"

CTestScript::CTestScript()
	: CScript((UINT)SCRIPT_TYPE::TESTSCRIPT)
	, m_fMaxSpeed(100.f)
	, m_fSpeed(100.0f)
{
}

CTestScript::~CTestScript()
{
}

void CTestScript::SetTargetPos(const Vec3 & _targetPos)
{
	m_vTargetPos = _targetPos;

	m_nDir = (m_vTargetPos - m_vInitPos).Normalize();
}

void CTestScript::update()
{
	
	Vec3 camPos = CMapMgr::GetInst()->GetMainCam()->Transform()->GetWorldPos();

	Vec3 mePos = Transform()->GetLocalPos();
	Vec3 meDir = Transform()->GetLocalDir(DIR_TYPE::FRONT);



	Vec3 toPos = mePos - camPos;
	float angle = atan2(toPos.x, toPos.z) ;



	Vec3 rot = Transform()->GetLocalRot();
	rot.y = angle;

	
	Transform()->SetLocalRot(rot);
	
}

void CTestScript::OnCollisionEnter(CCollider2D * _pOther)
{
	
}

void CTestScript::OnCollisionExit(CCollider2D * _pOther)
{
	
}
