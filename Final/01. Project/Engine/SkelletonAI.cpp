#include "stdafx.h"
#include "SkelletonAI.h"
#include "Animator3D.h"
#include "StatusComponent.h"
#include "TimeMgr.h"
#include "MapMgr.h"



CSkelletonAI::CSkelletonAI()
	: CScript((UINT)SCRIPT_TYPE::AI)
	, m_pPlayer(nullptr)
	, m_pInitPos()
	, m_nHealCnt(2)
	, m_state(EC_SKELL_STATE::Evaluate)
	, m_fMaxDeathTime(0.03333f * 40.0f)
	, m_fCurDeathTime(0.0f)
	, m_fMaxSkillTime(0.03333f* 110.f)
	, m_fCurSkillTime(0.0f)
	, m_bBagicAttackOn(false)
	, m_fBagicAttackTiming(ANIMFRAME(40.f))
	, m_fBagicAttackMaxTime(ANIMFRAME(40.f))
	, m_fBagicAttackHitTime(ANIMFRAME(5.f))
	, m_fBagicAttackRange(14.f)
	, m_fBagicAttackDamage(13.f)
	, m_fBagicAttackAngle(90.f * XM_ANGLE)
	, m_fPatrolDistNow(15.f)
	, m_fPatrolDistMax(15.f)
	, m_fTic(0.f)
	, m_fToc(0.f)
	, m_fTicMax(0.016f)
	, m_fDamagedCur(ANIMFRAME(10.f))
	, m_fDamagedMax(ANIMFRAME(10.f))
	, m_bFastRunBuff(false)
	, m_fFastRunTime(ANIMFRAME(3.f))
	, m_fFastRunMax(ANIMFRAME(3.f))
	, m_fFastRunSpeed(1.8f)
	, m_fRunOper(1.f)

{
}


CSkelletonAI::~CSkelletonAI()
{
}

void CSkelletonAI::update()
{
	if (m_fTicMax > m_fTic)
	{
		m_fTic += DT;
		return;
	}
	else
	{
		m_fToc = m_fTic;
		m_fTic = 0.0f;
	}

	CStatusComponent* state = GetObj()->StatusComponent();
	CTransform* trans = GetObj()->Transform();
	CAnimator3D* animate = GetObj()->Animator3D();

	static Vec3 mePos, meLook, playerPos, lookPlayer, turnOper, meRot, destPos, initDir;
	static float dist, angle, oper, idleDist, idleDest,height;
	//	------------------------------------------------------------------
	//	Getter
	mePos = trans->GetLocalPos();
	height = CMapMgr::GetInst()->GetHeight(state->GetMapNum(),mePos);
	meLook = trans->GetWorldDir(DIR_TYPE::FRONT);
	playerPos = m_pPlayer->Transform()->GetLocalPos();
	meRot = trans->GetLocalRot();

	//	------------------------------------------------------------------
	//	계산
	mePos.y = 0.f;
	playerPos.y = 0.f;
	meLook.y = 0.f;

	lookPlayer = (playerPos - mePos).Normalize();
	turnOper = meLook.Cross(lookPlayer);

	dist = Vec3::Distance(playerPos, mePos);
	angle = meLook.Dot(lookPlayer);
	oper = (turnOper.y < 0.f) ? -1.0f : 1.0f;
	idleDist = 0.0f;
	idleDest = 10.f;

	if (state->GetLowHp())
	{
		m_state = EC_SKELL_STATE::Death;
	}


	//	매번 확인해야하는 부분
	if (m_bFastRunBuff)
	{
		if (m_fFastRunTime > 0.0f)
		{
			m_fFastRunTime += m_fToc;
		}
		else
		{
			m_fFastRunTime = m_fFastRunMax;
			m_fRunOper = 1.f;
			m_bFastRunBuff = false;
		}
	}
	float fDamaged = 0.f;
	std::list<tDamageStack>& stack = state->GetDamageStack();
	while (!stack.empty())
	{
		fDamaged += stack.back().fAttackDamage;
		stack.pop_back();
	}
	if (fDamaged > 20.f)	//	순간 데미지
	{
		m_fDamaged = true;
	}
	if(!FloatCmp(fDamaged,0.0f,0.000001f))state->setHpDown(fDamaged);
	switch (m_state)
	{
	case EC_SKELL_STATE::Evaluate:
		if (m_fDamaged)
		{
			m_state = EC_SKELL_STATE::Damaged;
		}
		else
		{
			animate->ChangeAnimation((UINT)EC_SKELL_ANIM::Idle,true);
		}
		break;
	case EC_SKELL_STATE::BackHeal:
		break;
	case EC_SKELL_STATE::Move:
		if (m_bFastRunBuff)
		{
			GetObj()->Animator3D()->ChangeAnimation((UINT)EC_SKELL_ANIM::Fast_run, true);
		}
		else
		{
			GetObj()->Animator3D()->ChangeAnimation((UINT)EC_SKELL_ANIM::Run, true);
		}
		mePos.y = height;
		trans->SetLocalPos(mePos + (lookPlayer * m_fToc * (state->GetFloat(STATUS_FLOAT::MOVESPEED)+ m_fRunOper - m_fToc)));
		m_state = EC_SKELL_STATE::Evaluate;
		break;
	case EC_SKELL_STATE::Death:
		if (animate->IsAnimEnd()) {
			state->OffHpUies();
			CMapMgr::GetInst()->DropItem(mePos);
			GetObj()->SetActive(false);
			m_state = EC_SKELL_STATE::Evaluate;
		}

		break;
	case EC_SKELL_STATE::Escape:
		break;
	case EC_SKELL_STATE::SKill1:
		m_bFastRunBuff = true;
		m_fRunOper = m_fFastRunSpeed;
		m_state = EC_SKELL_STATE::Evaluate;
		break;
	case EC_SKELL_STATE::FastRun:
		m_bFastRunBuff = true;
		m_fRunOper = m_fFastRunSpeed;
		state->SetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW, 0.f);
		m_state = EC_SKELL_STATE::Evaluate;
		break;
	case EC_SKELL_STATE::BagicAttack:
		if (animate->IsAnimIdx((UINT)EC_SKELL_ANIM::Attack))
		{
			if (animate->IsAnimEnd())
			{
				if (m_bBagicAttackOn)
				{
					m_pPlayer->StatusComponent()->DamageStack(GetObj()->GetID(),meLook, m_fBagicAttackDamage);
					m_bBagicAttackOn = false;
				}
				state->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW, 0.f);
				m_state = EC_SKELL_STATE::Evaluate;
			}
			else
			{
				if (animate->GetCurAnimLeftTime() < 1.f)
				{
					if (dist < m_fBagicAttackRange && angle < m_fBagicAttackAngle)
					{
						if (!m_bBagicAttackOn)m_bBagicAttackOn = true;
					}
				}
			}
		}
		else
		{
			animate->ChangeAnimation((UINT)EC_SKELL_ANIM::Attack);
		}
		break;
	case EC_SKELL_STATE::Turn:
		if (Vector3Cmp(lookPlayer, meLook, 0.1f))
		{
			m_state = EC_SKELL_STATE::Evaluate;
		}
		else
		{
			animate->ChangeAnimation((UINT)EC_SKELL_ANIM::Run, true);
			meRot.y += state->GetFloat(STATUS_FLOAT::ROTSPEED)*m_fToc * oper;
			trans->SetLocalRot(meRot);
		}
		break;
	case EC_SKELL_STATE::Patrol:
		break;
	case EC_SKELL_STATE::DestGo:
		if (m_bFastRunBuff)
		{
			GetObj()->Animator3D()->ChangeAnimation((UINT)EC_SKELL_ANIM::Fast_run, true);
		}
		else
		{
			GetObj()->Animator3D()->ChangeAnimation((UINT)EC_SKELL_ANIM::Run, true);
		}
		mePos.y = height;
		trans->SetLocalPos(mePos + (lookPlayer * m_fToc * (state->GetFloat(STATUS_FLOAT::MOVESPEED) + m_fRunOper - m_fToc)));
		m_state = EC_SKELL_STATE::Evaluate;
		break;
	case EC_SKELL_STATE::MoveBack:
		if (m_bFastRunBuff)
		{
			GetObj()->Animator3D()->ChangeAnimation((UINT)EC_SKELL_ANIM::Fast_run, true);
		}
		else
		{
			GetObj()->Animator3D()->ChangeAnimation((UINT)EC_SKELL_ANIM::Run, true);
		}
		mePos.y = height;
		trans->SetLocalPos(mePos + (lookPlayer * m_fToc *  -1.f *(state->GetFloat(STATUS_FLOAT::MOVESPEED) + m_fRunOper - m_fToc)));
		m_state = EC_SKELL_STATE::Evaluate;
		break;
	case EC_SKELL_STATE::Damaged:
	{
		if (animate->IsAnimIdx((UINT)EC_SKELL_ANIM::Damage))
		{
			if (animate->IsAnimEnd())
			{
				m_state = EC_SKELL_STATE::Evaluate;
				m_fDamaged = false;
			}
			
		}
		else 
		{
			GetObj()->Animator3D()->ChangeAnimation((UINT)EC_SKELL_ANIM::Damage);
		}
	}
		break;
	case EC_SKELL_STATE::Targeting:
		break;
	}

	if (m_state != EC_SKELL_STATE::Evaluate)return;

	//----------------------------------------------------------------------------------
	//	명령부

	if (dist < state->GetFloat(STATUS_FLOAT::MINRANGE))
	{
		m_state = EC_SKELL_STATE::MoveBack;
	}
	else if (dist < state->GetFloat(STATUS_FLOAT::ATTACKRANGE))
	{
		if (state->GetFloat(STATUS_FLOAT::SKILL_1_COOL_MAX) < state->GetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW))
		{
			m_state = EC_SKELL_STATE::FastRun;
			return;
		}
		else
		{
			state->SetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW, state->GetFloat(STATUS_FLOAT::SKILL_1_COOL_NOW) + m_fToc);
		}

		if (state->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX) < state->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW))
		{
			if(FloatCmp(angle, 1.0f, 0.01f))
		//	if (FloatCmp(angle, m_fBagicAttackAngle, 0.001f))
			{
				
				m_state = EC_SKELL_STATE::BagicAttack;
				return;
			}
			else
			{
				m_state = EC_SKELL_STATE::Turn;
				return;
			}

		}
		else
		{
			state->SetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW, state->GetFloat(STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW) + m_fToc);
		}

	}
	else if (dist < state->GetFloat(STATUS_FLOAT::SEARCHRANGE))
	{
		if (FloatCmp(angle,1.0f,0.01f))
		{
			m_state = EC_SKELL_STATE::Move;
		}
		else
		{
			m_state = EC_SKELL_STATE::Turn;
		}
	}
	else
	{
		//	여기는 나중에
		//idleDist = Vec3::Distance(m_pInitPos, mePos);
		//
		//
		//if (idleDist < 20.0f)
		//{
		//	meRot.y += 30.f * XM_ANGLE;
		//	m_state = EC_SKELL_STATE::Patrol;
		//}
		//else
		//{
		//	initDir = (m_pInitPos - mePos).Normalize();
		//
		//
		//}
	}


}

void CSkelletonAI::Reset(const Vec3 & _pos, const Vec3 & _rot)
{
	m_pInitPos = _pos;
	m_pInitPos.y = 0.0f;
	GetObj()->Transform()->SetLocalPos(_pos);
	GetObj()->Transform()->SetLocalRot(_rot);

	m_fCurDeathTime = 0.0f;
	m_fCurSkillTime = m_fMaxSkillTime;	//	바로사용가능
	m_fBagicAttackTiming = m_fBagicAttackMaxTime;
	m_fPatrolDistNow = m_fPatrolDistMax;
	m_fDamagedCur = m_fDamagedMax;

	CStatusComponent* s = GetObj()->StatusComponent();
	s->SetSkelleton(_pos, _rot);


}
void CSkelletonAI::OnCollision(CCollider2D * _pOther)
{
	Vec3 otherPos = _pOther->Transform()->GetLocalPos();
	Vec3 mePos = GetObj()->Transform()->GetLocalPos();

	Vec3 dir = (otherPos - mePos).Normalize();
	dir.y = 0.f;
	GetObj()->Transform()->SetLocalPos(mePos + (dir * -3.f * DT));
}