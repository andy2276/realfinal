#pragma once
#include "Script.h"

struct tBossReset
{
	float fMax = 500.f;

	float bagicDist = 20.f;
	float bagicCool = 5.f;
	float bagicAngle = 45.f * XM_ANGLE;
	float bagicAttackTime = ANIMFRAME(5);

	float skill1Dist = 40.f;
	float skill1ShootTime = 1.f;
	float skill1Cool = 15.f;


	float skill2Dist = 60.f;
	float skill2ShootTime = 1.f;
	float skill2Cool = 15.f;

	float skill3Dist = 100.f;
	float skill3ShootTime = 1.f;
	float skill3Cool = 15.f;

	float minRange = 15.f;
	float attackRange = 60.f;
	float SearchRange = 300.f;

	float phase1hp = fMax;
	float phase2hp = 0.f;
	float phase3hp = 0.f;

	float moveSpeed = 65.f;
	float rotSpeed = 120.0f * XM_ANGLE;
};
namespace nBT
{
	namespace TSK
	{
		namespace CDT
		{
			class bFloatScrCmpDest;
		}
	}
}


class CSkillPlatform;
class CBossAi :
	public CScript
{
private:

	EC_BOSS_STATE	m_eState;					//	강제로 평가
	std::list< UINT> m_eStatStack;	
	UINT			m_nInitCheckList;			//	확인해야하는것을 여기다가 저장
	UINT			m_nAlwaysCheckList;			//	언제나 확인해야하는 가장 필수적인정보
	UINT			m_nCurCheckList;			//	스킬과 같은걸 확인하는곳 지금 가능한 패턴을 설정해준다
	UINT			m_nCurAction;				//	지금 당장 사용해야하는 혹은 지금의 정보

	UINT			m_nEvalueation;

	//	페이즈 2 아림쓰 참조
	UINT			m_nPhaseNum;
	bool			m_OnAngryStart;
	bool			m_OnAngry;

	bool			m_OnSearch;
	bool			m_OnTargeting;
	bool			m_OnActOrder;
	bool			m_OnActOrderStart;

	float			m_fCurNearDist;


	float			m_fMoveDelayCur;
	float			m_fMoveDelayMax;

	int				m_nBossType;
	std::list<Vec3> m_listDestPos;

	float					m_fTic;
	float					m_fToc;
	float					m_fTicMax;


	float					m_fShootSkillTime[3];
	float					m_fShootSkillDist[3];
	bool					m_bShootSkillOn[3];

	float					m_bBagicAttackDamage;

	float					m_fBagicAttackTime;
	bool					m_bBagicAttackOn;

	float					m_bBaggicAttackDist;
	float					m_bBaggicAttackMinAngle;
	float					m_bBaggicAttackMaxAngle;

	//	pattern
	
	float					m_fPhaseHp[3];	

	float					m_fPhase1Pattern[5];
	float					m_fPhase2Pattern[5];
	float					m_fPhase3Pattern[5];

	tBossReset				m_tResetInfo;

	CGameObject*			m_pPlayer;
	CGameObject*			m_pParent;
	int			m_arrBossAnim[(UINT)EC_MAPPING_ANIM::End];
	CSkillPlatform*			m_arrSkillPlatform[3];

public:
	CBossAi();
	virtual ~CBossAi();

	CLONE(CBossAi);

	//player
	void SetPlayer(CGameObject* _pPlayer) { m_pPlayer = _pPlayer; }
	//	스킬의 종류설정
	void SetSkillCheck(UINT _state) { m_nInitCheckList = _state; }
	//	스킬 발사 시간설정
	void SetSkillShootTime(int _idx, float _fShootTime) { m_fShootSkillTime[_idx] = _fShootTime; }
	//	스킬 사용가능 거리설정
	void SetSkillShootDist(int _idx, float _fShootDist) { m_fShootSkillDist[_idx] = _fShootDist; }
	//	기본공격 데미지 설정
	void SetBagicAttackDamage(float _fDamage) { m_bBagicAttackDamage = _fDamage; }
	//	피격 가능 공격 시간
	void SetBagicAttackTime(float _fAttack) { m_fBagicAttackTime = _fAttack; }
	//	기본공격 거리
	void SetBagicAttackDist(float _fRange) { m_bBaggicAttackDist = _fRange; }
	//	기본공격 피격각도
	void SetBagicAttackAngle(float _min, float _max) { m_bBaggicAttackMinAngle = _min; m_bBaggicAttackMaxAngle = _max; }
	//	페이지에 들어가는 체력 최소치
	void SetPhaseHp(int _idx, float _hp) { m_fPhaseHp[_idx] = _hp; }
	//	보스의 타입을 설정
	void SetBossType(int _type) { m_nBossType = _type; }
	//	스킬을 설정한다.
	void SetBossSkill(int _idx,int _type);

	//	애니메이션 매핑
	void SetMappingAnimIdx(EC_MAPPING_ANIM _bossAct, int _destAnim = -1)
	{
		m_arrBossAnim[(UINT)_bossAct] = _destAnim;
	};

	void SetResetInfo(const tBossReset& _scr) { memcpy(&m_tResetInfo, &_scr, sizeof(tBossReset)); };
	const tBossReset& GetResetInfo() { return m_tResetInfo; }
	int GetBossType() { return m_nBossType; }

	CStatusComponent* GetStatus();

	void Reset(int _mapNum, const Vec3& _pos, const Vec3& _rot);

	void PushStack(UINT _nValue);

	virtual void update();

	bool IsPossibleAnim(int _idx)
	{
		if (m_arrBossAnim[_idx] < 0)return false;
		else return true;
	}
	int GetMappingAnimIdx(int _idx)
	{
		return m_arrBossAnim[_idx];
	}
	void ChangeMappingAnim(int _idx,bool _bTrue = true);
	bool IsMappingAnimEnd(int _idx);
	

	

	//	새로운
	//	언제나 체크해야하는것들
	UINT GetAlwaysCheckList() {return m_nAlwaysCheckList;}

	void AddCurCheckList(UINT _state) { m_nCurCheckList |= _state; }
	void SubCurCheckList(UINT _state) { m_nCurCheckList &= ~_state; }
	UINT GetCurCheckList() { return m_nCurCheckList; }




	friend class CBossAiEvaluation;
	friend class nBT::TSK::CDT::bFloatScrCmpDest;
};
