#pragma once
#include "Script.h"

class CInventoryStatus :
	public CScript
{
private:
	int m_nSkillType;
public:
	CInventoryStatus();
	virtual ~CInventoryStatus();

	CLONE(CInventoryStatus);

	void SetSkillType(int _type) { m_nSkillType = _type; }
	int GetSkillType() { return m_nSkillType; }

	virtual void update();
};

