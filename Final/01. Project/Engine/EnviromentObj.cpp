#include "stdafx.h"
#include "EnviromentObj.h"
#include "MapMgr.h"
#include "StatusComponent.h"


CEnviromentObj::CEnviromentObj()
	:CScript((UINT)SCRIPT_TYPE::ENVIROMENT)
{
}


CEnviromentObj::~CEnviromentObj()
{
}

void CEnviromentObj::update()
{
}

void CEnviromentObj::OnCollisionEnter(CCollider2D * _pOther)
{
}

void CEnviromentObj::OnCollision(CCollider2D * _pOther)
{
	static Vec3 mePos, otherPos,backDir;

	CEnviromentObj* pSame = nullptr;
	pSame = _pOther->GetObj()->GetScript< CEnviromentObj>();
	if (pSame == nullptr)
	{
		otherPos = _pOther->Transform()->GetLocalPos();
		otherPos.y = 0.0f;
		mePos = Transform()->GetLocalPos();
		mePos.y = 0.0f;
		backDir = (otherPos - mePos).Normalize();
		otherPos += backDir;
		otherPos.y = CMapMgr::GetInst()->GetHeight(otherPos);
		_pOther->Transform()->SetLocalPos(otherPos);
	}
	else
	{
		return;
	}
	
}

void CEnviromentObj::OnCollisionExit(CCollider2D * _pOther)
{
}
