#pragma once
#include "Script.h"
//	struct tRoundInfo {
//		//	Respon
//		int nMaxMonsterCnt;		//	max
//		int nCurMonsterCnt;		//	cur
//		int nClearMonsterCnt;	//	min
//	
//		int nResponType;
//	
//		std::vector< tMonsterInfo> monsterInfo;
//		tRoundEvent* tStartEvent;
//		tRoundEvent* tIngEvent;
//		tRoundEvent* tEndEvent;
//	};

namespace Trigger {
	namespace Base {
		class CBasis;
	}
};

class CMapRapper;

class CSponSystem :
	public CScript
{
private:

	vector< tRoundInfo>	m_vecRoundInfo;
	vector< tResponer> m_vecResponer;
	CMapRapper*			m_pMapRapper;
	//	중요 ! 이거는 인덱스임!!
	UINT				m_nCurRound;
	//	중요 ! 이거는 인덱스임!!
	UINT				m_nMaxRound;
	tRoundEvent* m_tStartEvent;
	tRoundEvent* m_tIngEven;
	tRoundEvent* m_tEndEvent;

	UINT m_nMapIdx;
	//	Trigger
	Trigger::Base::CBasis* m_pRoot;
	std::list< Trigger::Base::CBasis*> m_lStack;
	std::vector< Trigger::Base::CBasis*> m_vecTriggers;
	bool	m_bUpdate;

	EC_RESPON_TYPE GetInResponType(UINT _idx) {
		switch (_idx)
		{
		case (UINT)EC_RESPON_TYPE::SQUENCE:
			return EC_RESPON_TYPE::SQUENCE;
		case (UINT)EC_RESPON_TYPE::ATONCE:
			return EC_RESPON_TYPE::ATONCE;
		case (UINT)EC_RESPON_TYPE::RANDOM:
			return EC_RESPON_TYPE::RANDOM;
		}
		return  EC_RESPON_TYPE::SQUENCE;
	}
	UINT GetInResponType(EC_RESPON_TYPE _idx) {
		switch (_idx)
		{
		case EC_RESPON_TYPE::SQUENCE:
			return (UINT)EC_RESPON_TYPE::SQUENCE;
		case EC_RESPON_TYPE::ATONCE:
			return (UINT)EC_RESPON_TYPE::ATONCE;
		case EC_RESPON_TYPE::RANDOM:
			return (UINT)EC_RESPON_TYPE::RANDOM;
		}
		return (UINT)EC_RESPON_TYPE::SQUENCE;
	}
public:
	static int g_nPossibleInTower;
	CSponSystem();
	virtual ~CSponSystem();

public:
	virtual void update();	//	업데이트시에는 지금의 라운드의 정보에서 몬스터수를 감시한다. 몬스터들중 지금 
	//	리사이즈로 추가한다.
	void SetRoundCnt(int _roundCnt) { m_nMaxRound = _roundCnt; m_vecRoundInfo.resize(_roundCnt); }
	//	라운드 정보를 설정
	void AddRoundInfo(int _roundNum,int _responType,int _clearCnt,int _initMonsterCnt,int _maxCnt
	,int _nowMax,int _nowCur,float _sponDelay
	);
	//	몬스터 리스폰에 대한걸 기록한다
	void RegistMonster(int _roundNum,tMonsterInfo _tMonsterInfo);
	//	리스폰 위치를 설정
	void SetResponPos(const Vec3& _pos);	
	//	업데이트시에 지금 맵에다가 몬스터를 기록해야하니깐 그럼.
	void SetMapIndex(int _idx);		

	void SetMapRapper(int _idx);

	void SetCurRound(UINT _idx) { m_nCurRound = _idx; }
	UINT GetCurRound() { return m_nCurRound; }

	void SetMaxRound(UINT _idx) { m_nMaxRound = _idx; }
	UINT GetMaxRound() { return m_nMaxRound; }

	int GetClearObjCnt(UINT _idx) { return m_vecRoundInfo[_idx].nClearMonsterCnt; }
	int GetCurClearObjCnt(){ return m_vecRoundInfo[m_nCurRound].nClearMonsterCnt; }

	int GetMaxObjCnt(UINT _idx) { return m_vecRoundInfo[_idx].nMaxMonsterCnt; }
	int GetCurMaxObjCnt() { return m_vecRoundInfo[m_nCurRound].nMaxMonsterCnt; }

	int GetCurObjCnt(UINT _idx) { return m_vecRoundInfo[_idx].nCurMonsterCnt; }
	int GetCurCurObjCnt() { return m_vecRoundInfo[m_nCurRound].nCurMonsterCnt; }


	int GetCurRoundSponType() { return m_vecRoundInfo[m_nCurRound].nResponType; }

	tRoundInfo& GetRoundInfo(UINT _idx) { return m_vecRoundInfo[_idx]; }
	tRoundInfo& GetCurRoundInfo() { return m_vecRoundInfo[m_nCurRound]; }

	//	이거는 잘못됨
	//tResponer& GetResponer(UINT _idx) { return m_vecResponer[_idx]; }
	//tResponer& GetCurResponer() { return m_vecResponer[m_nCurRound]; }
	vector< tResponer>& GetResponer() { return m_vecResponer; }
	const Vec3& GetResponPos(int _idx) { return m_vecResponer[_idx].vPos; }

	EC_RESPON_TYPE GetResponType(UINT _idx) {
		return GetInResponType(m_vecRoundInfo[_idx].nResponType);
	}
	EC_RESPON_TYPE GetCurResponType() {
		return GetInResponType(m_vecRoundInfo[m_nCurRound].nResponType);
	}
	void SponMonster(UINT _type,const Vec3& _sponPos);



	//	레디 오브젝트를 계속 들고와야함.




	CLONE(CSponSystem);
};

