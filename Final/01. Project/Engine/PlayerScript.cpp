#include "stdafx.h"
#include "PlayerScript.h"

#include "BulletScript.h"
#include "TestScript.h"
#include "Animator3D.h"

#include "MapMgr.h"	//	0730 지민추
#include "Transform.h"
#include "GameObject.h"

#include"Device.h" //0726아림추
#include"BulletBossSkillScript.h"
#include"StatusComponent.h"
#include"CMETEOR.h"
#include"CPlayerSkillScript.h"
#include "MapScript.h"
#include "RenderMgr.h"
#include"CItemScript.h"
#include "Camera.h"
#include "PixCamera.h"
#include "SceneMgr.h"
#include"CInventoryScript.h"
#include"CGoldSkillScript.h"

CPlayerScript::CPlayerScript()
	: CScript((UINT)SCRIPT_TYPE::PLAYERSCRIPT)
	, m_pOriginMtrl(nullptr)
	, m_pCloneMtrl(nullptr)
	, m_bIdle(true)
	, m_bStaticMouse(false)
	, m_bSkillOn(false)
	, m_bDamaged(false)
	, m_fSkillMax(0.033333f * 60.f)
	, m_fSkillCur(0.0f)
	, m_bRun(false)
	, m_fDamagedMax(0.033333f * 28.f)
	, m_fDamagedCur(0.033333f * 28.f)
	, m_fMouseDir(0.0f)
	, m_fSkill2CoolMax(3.f)
	, m_fSkill2CoolCur(0.0f)
	, m_bSkill2On(true)
	, m_bDeath(false)
	, m_bInventoryOn(true)
	, m_zControl(true)
	, m_GoldSkillOn(false)
	, m_bDeathEvent(false)
	, m_bIsOnTitle(true)

{
	m_ItemUI = new CGameObject;
	m_ItemUI->SetName(L"Z UI");
	m_ItemUI->FrustumCheck(false);

	// Transform 설정
	m_ItemUI->AddComponent(new CTransform);
	m_ItemUI->Transform()->setPhysics(false); //true값이 움직일수 있게 하는것. 
	Vec3 vPos(50, 50, 1.f);
	m_ItemUI->Transform()->SetLocalPos(vPos);

	Vec3 vScale_monster(100.f, 50.f, 1.f);
	m_ItemUI->Transform()->SetLocalScale(vScale_monster);

	m_ItemUI->AddComponent(new CMeshRender);
	m_ItemUI->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	Ptr<CMaterial> pZUIMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
	m_ItemUI->MeshRender()->SetMaterial(pZUIMtrl->Clone());
	Ptr<CTexture> pZtexture = CResMgr::GetInst()->Load<CTexture>(L"Z01", L"Texture\\Z01.png");
	m_ItemUI->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pZtexture.GetPointer());
	//	m_ItemUI->disable();

	CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_ItemUI);


	//----------------------------------------------------------------------------------------
	//	스킬 맥스 선택
	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_1];
	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_1].bSlotOn = true;
	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_1].bSkillOn = true;
	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_1].fSkillMax = 10.0f;
	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_1].nSkillNum = (UINT)PLAYER_SKILL_TYPE::SKILL_FIREBALL;


	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_2].bSlotOn = true;
	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_2].bSkillOn = true;

	//	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_2].fSkillMax = 2.0f;
	//	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_2].nSkillNum = (UINT)PLAYER_SKILL_TYPE::SKILL_ICEARROW;


	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_2].fSkillMax = 1.0f;
	//	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_2].nSkillNum = (UINT)PLAYER_SKILL_TYPE::SKILL_FIREBALL;


	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_2].nSkillNum = (UINT)PLAYER_SKILL_TYPE::SKILL_ICEARROW;


	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_3];
	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_4];
	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_5];
	m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_6];


	for (int i = 0; i < (UINT)EC_PLAYER_SKILL::End; ++i)
	{
		m_arrSkillOn[i] = true;
		m_arrSkillCoolNow[i] = m_arrSkillCoolMax[i];
	}
	//ClientToScreen(CRenderMgr::GetInst()->GetHwnd(), &m_pPt);
//	ScreenToClient(CRenderMgr::GetInst()->GetHwnd(), &m_pPt);

	GetClientRect(CRenderMgr::GetInst()->GetHwnd(), &m_rect);
	m_rectHalf.x = m_rect.right / 2; m_rectHalf.y = m_rect.bottom / 2;

	//	공격 최대사거리

	pSound = CResMgr::GetInst()->Load<CSound>(L"03_coins", L"Sound\\03_coins.wav");
	pTitleSound = CResMgr::GetInst()->Load<CSound>(L"land05", L"Sound\\land05.mp3");
	pMainSound = CResMgr::GetInst()->Load<CSound>(L"land02", L"Sound\\land02.mp3");

	//============================================================================================================
	// 타이틀
	//============================================================================================================

	m_TitleUI = new CGameObject;
	m_TitleUI->SetName(L"title");
	m_TitleUI->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	m_TitleUI->AddComponent(new CTransform);
	m_TitleUI->AddComponent(new CMeshRender);

	tResolution res = CRenderMgr::GetInst()->GetResolution();
	m_TitleUI->Transform()->setPhysics(false);
	m_TitleUI->Transform()->SetLocalScale(Vec3(res.fWidth, res.fHeight, 1.f));
	m_TitleUI->Transform()->SetLocalPos(Vec3(0, 0, 1.0000000000001f));

	m_TitleUI->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	Ptr<CMaterial>pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
	m_TitleUI->MeshRender()->SetMaterial(pMtrl->Clone());

	Ptr<CTexture> pBorder = CResMgr::GetInst()->Load<CTexture>(L"title", L"Texture\\title03.png");
	m_TitleUI->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pBorder.GetPointer());

}

void CPlayerScript::OnCollisionEnter(CCollider2D * _pOther)
{

	if (_pOther->GetObj()->GetName() == L"Item")
	{
		m_ItemUI->SetActive(true);



	}
	if (!m_GoldSkillOn)
	{
		if (_pOther->GetObj()->GetName() == L"Monster")
		{
			//몬스터랑충돌하면 hp 10만큼씩 감소 


//			CStatusComponent().setHpDown(10);

			GetObj()->StatusComponent()->setHpDown(10);

		}
	}

}

void CPlayerScript::OnCollisionExit(CCollider2D * _pOther)
{

	if (_pOther->GetObj()->GetName() == L"Item")
	{
		//	isUIon = true;

	//	std::wcout << "안그려" << std::endl;
		m_ItemUI->SetActive(false);

	}

}

void CPlayerScript::OnCollision(CCollider2D * _pOther)
{
	if (_pOther->GetObj()->GetName() == L"Item")
	{
		//	std::wcout << "충돌해서 그림" << std::endl;
			//m_ItemUI->SetActive(true);
		m_ItemUI->SetActive(true);
		if (m_bPressZ)//충돌도했고, z키가 입력된 상황이라면 아이템이 사라지면서 스킬 획득이 된다.
		{
			//이때 플레이어가 가지고있게될 스킬의 타입을 넘겨주면, 
			//스테이터스 컴포넌트에서 플레이어가 가지고 있는 스킬의 true, false를 해주면 된다. 

			m_inventroyScript->setSkillOwn();
			pSound->Play(1, 1, true);
			m_bPressZ = false;
			m_ItemUI->SetActive(false);
			_pOther->GetObj()->SetActive(false);
			//DeleteObject(_pOther->GetObj());
		}


	}

}
void CPlayerScript::init()
{
}
void CPlayerScript::setGoldMaterial()
{
	Ptr<CMaterial> TexMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
	GetObj()->MeshRender()->SetMaterial(TexMtrl->Clone(), 0);
	Ptr<CTexture> ptexture = CResMgr::GetInst()->Load<CTexture>(L"gold2", L"Texture\\gold2.jpg");
	GetObj()->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, ptexture.GetPointer());

}
CPlayerScript::~CPlayerScript()
{
}

void CPlayerScript::awake()
{
	m_pOriginMtrl = MeshRender()->GetSharedMaterial();
	m_pCloneMtrl = m_pOriginMtrl->Clone();

	int a = 1;
	m_pCloneMtrl->SetData(SHADER_PARAM::INT_0, &a);
	m_tRosol = CRenderMgr::GetInst()->GetResolution();
	m_tRosol.fWidth /= 2.f;
	m_tRosol.fHeight /= 2.f;
	m_pPt = { int(m_tRosol.fWidth),int(m_tRosol.fHeight) };

	m_vAttackDir = Transform()->GetLocalDir(DIR_TYPE::FRONT);
	m_vAttackAngle = Transform()->GetLocalRot();
	m_vOriginCameraAngle = m_vAttackAngle;

	m_ItemUI->SetActive(false);
	//	std::cout << "init()" << std::endl;

	m_inventroyScript = GetObj()->GetScript<CInventoryScript>();

	m_playerSkillScript = GetObj()->GetScript<CPlayerSkillScript>();

	//0.052359876
	m_fCamAngleMin = XM_ANGLE * 3.0f;
	//0.244346088
	m_fCamAngleMax = XM_ANGLE * 14.f;

	//0.191986212

	m_fAttackDistMax = 210.f;
	m_fAttackDist = 800.f;
	m_fAttackDistReal = m_fAttackDistMax - (m_fAttackDist *m_fCamAngleMin);
	//	도당 길이	XM_Angle 당 5.f

	m_fAttackDistCur = 0.0f;

}

void CPlayerScript::update()
{
	m_bPressZ = false;

	//--------------init대신에 update에서 한번만 호출하게 해줘야하는것들 
	if (m_zControl)
	{
		CreateObject(m_TitleUI, L"UI");

		m_ItemUI->SetActive(false);
		m_zControl = false;

		Transform()->setPhysicsValue(0.7, -9.8, 10);
		pTitleSound->Play(1, 0.7, true);

		m_GoldSkillOn = false;

	}


	if (m_bIsOnTitle)
	{
		if (KEY_TAB(KEY_TYPE::KEY_ENTER))
		{
			m_TitleUI->SetActive(false);
			m_bIsOnTitle = false;
			pTitleSound->Stop();
			pMainSound->Play(1, 0.4, true);

		}
	}
	else 
	{

		m_GoldSkillOn = m_playerSkillScript->getGoldOn();


		const static int nLeft = 1;
		const static int nRight = 2;
		const static int nUp = 3;
		const static int nDown = 4;


		Vec3 vPos = GetObj()->Transform()->GetLocalPos();
		Vec3 vRot = GetObj()->Transform()->GetLocalRot();
		Vec3 vGo = GetObj()->Transform()->GetWorldDir(DIR_TYPE::FRONT);
		Vec3 vRight = GetObj()->Transform()->GetWorldDir(DIR_TYPE::RIGHT);
		Vec3 vMoveValue;
		CAnimator3D* animation = GetObj()->Animator3D();
		CStatusComponent* state = GetObj()->StatusComponent();
		float fSpeed = state->GetFloat(STATUS_FLOAT::MOVESPEED);

		float deltaTime = DT;
		float deltaSpeed = deltaTime * fSpeed;
		int nIdx = CMapMgr::GetInst()->GetCalculMapIdx();


		if (m_bDeathEvent)
		{

			m_pPixCamera->DeathShudder();
			m_pPixCamera->OnDeathShudder();
			return;
		}


		//	-------------------------------------------------------------------------------------
		//	스킬 쿨타임 계산
		if (state->GetFloat(STATUS_FLOAT::HP) < 0.001f)
		{
			m_bDeath = true;
		}

		//for (int i = 0; i < (UINT)EC_PLAYER_SKILL::End; ++i)
		//{
		//	if (m_arrSkillSlot[i].bSlotOn)
		//	{
		//		if (!m_arrSkillSlot[i].bSkillOn)
		//		{
		//			if (m_arrSkillSlot[i].fSkillNow < m_arrSkillSlot[i].fSkillMax)
		//			{
		//				m_arrSkillSlot[i].fSkillNow += deltaTime;
		//			}
		//			else
		//			{
		//				m_arrSkillSlot[i].bSkillOn = true;
		//				m_arrSkillSlot[i].fSkillNow = 0.f;
		//			}
		//		}
		//	}
		//}
		//	-------------------------------------------------------------------------------------
		//	키입력 계산
		//	움직임
		if (!m_GoldSkillOn)
		{
			if (KEY_HOLD(KEY_TYPE::KEY_W)) {
				vMoveValue += vGo * deltaSpeed;
				m_bPressMove = true;
			}
			else if (KEY_HOLD(KEY_TYPE::KEY_S)) {
				vMoveValue += (vGo * -1.f)* deltaSpeed;
				m_bPressMove = true;

			}
			if (KEY_HOLD(KEY_TYPE::KEY_A)) {
				vMoveValue += (vRight * -1.f)* deltaSpeed;
				m_bPressMove = true;
			}
			else if (KEY_HOLD(KEY_TYPE::KEY_D)) {
				vMoveValue += (vRight)* deltaSpeed;
				m_bPressMove = true;
			}

			if (KEY_TAB(KEY_TYPE::KEY_Z)) {
				m_bPressZ = true;
			}
		}
		//	인벤토리 켜기 -> 이게 켜지면 스테틱 마우스가 꺼짐
		if (KEY_TAB(KEY_TYPE::KEY_I)) {
			if (m_bInventoryOn)
			{
				m_bInventoryOn = false;
				m_bStaticMouse = true;
				ShowCursor(false);
			}
			else
			{
				m_bInventoryOn = true;
				m_bStaticMouse = false;
				ShowCursor(true);
			}
			GetObj()->GetScript<CInventoryScript>()->setInventoryOn(m_bInventoryOn);
			//Ptr<CSound> pSound = CResMgr::GetInst()->Load<CSound>(L"land04", L"Sound\\land04.mp3");

		//	Ptr<CSound> pSound = CResMgr::GetInst()->Load<CSound>(L"29_arrowDamage", L"Sound\\29_arrowDamage.wav");
			//pSound->Play(1, true);
		}
		if (KEY_TAB(KEY_TYPE::KEY_LSHIFT))
		{
			//		Ptr<CSound> pSound = CResMgr::GetInst()->FindRes<CSound>(L"ok");
			//		pSound->Play(2, false);

					//g_sound->Play(0, 0.3, true);

		}



		if (KEY_HOLD(KEY_TYPE::KEY_7))
		{

			m_playerSkillScript->shoot((UINT)PLAYER_SKILL_TYPE::SKILL_GOLD);
		}

		if (KEY_TAB(KEY_TYPE::KEY_0))
		{
			state->SetFloat(STATUS_FLOAT::HP, 100.f);
			state->setHpDown(0.0f);
		}




		if (KEY_TAB(KEY_TYPE::KEY_1))
		{
			if (!m_bSkillOn)
			{


				//스크롤에 올라온 스킬쓰려면 이 주석 풀어주기 
				int type = m_inventroyScript->getSkillType(1 - 1);
				GetObj()->GetScript<CPlayerSkillScript>()->shoot((UINT)type);

				m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_1].bSkillOn = false;
				m_bSkillOn = true;

			}
		}
		if (KEY_TAB(KEY_TYPE::KEY_2))
		{
			if (!m_bSkillOn)
			{

				m_pPixCamera->Skill1Shudder();
				int type = m_inventroyScript->getSkillType(2 - 1);
				GetObj()->GetScript<CPlayerSkillScript>()->shoot(type);

				m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_2].bSkillOn = false;
				m_bSkillOn = true;


			}
		}
		if (KEY_TAB(KEY_TYPE::KEY_3))
		{
			int type = m_inventroyScript->getSkillType(3 - 1);
			GetObj()->GetScript<CPlayerSkillScript>()->shoot(type);

			m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_3].bSkillOn = false;
			m_bSkillOn = true;
			//	GetObj()->Animator3D()->ChangeAnimation((UINT)EC_PLAYER_ANIM::Skill);
		}
		if (KEY_TAB(KEY_TYPE::KEY_4))
		{
			int type = m_inventroyScript->getSkillType(4 - 1);
			GetObj()->GetScript<CPlayerSkillScript>()->shoot(type);

			m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_4].bSkillOn = false;
			m_bSkillOn = true;
			//	GetObj()->Animator3D()->ChangeAnimation((UINT)EC_PLAYER_ANIM::Skill);
		}
		if (KEY_TAB(KEY_TYPE::KEY_5))
		{
			int type = m_inventroyScript->getSkillType(5 - 1);
			GetObj()->GetScript<CPlayerSkillScript>()->shoot(type);

			m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::_5].bSkillOn = false;
			m_bSkillOn = true;

			//	GetObj()->Animator3D()->ChangeAnimation((UINT)EC_PLAYER_ANIM::Skill);
		}



		if (KEY_TAB(KEY_TYPE::KEY_8))
		{
			if (m_bStaticMouse)
			{
				m_bStaticMouse = false;
				m_fMouseDir = 0.0f;
				ShowCursor(true);
			}
			else
			{
				m_bStaticMouse = true;

			}

		}
		CGameObject* cam = CMapMgr::GetInst()->GetMainCam();
		Vec3 camRot = cam->Transform()->GetLocalRot();
		if (m_bStaticMouse)
		{
			float yRot = 0.0f;
			float xAngle = 0.0f;
			GetClientRect(CRenderMgr::GetInst()->GetHwnd(), &m_rect);
			m_rectHalf.x = m_rect.right / 2; m_rectHalf.y = m_rect.bottom / 2;
			if (!m_bMouseMoveOn)
			{
				m_bMouseMoveOn = true;
			}
			else
			{
				m_pPreMousePos = m_pMousePos;
				m_pMousePos = KEY_MOUSEPOS;

				int ox = m_pMousePos.x - m_pPreMousePos.x;
				int oy = m_pMousePos.y - m_pPreMousePos.y;

				if (ox != 0 || oy != 0)
				{
					if (-23 < oy && oy < 23)
					{
						if (m_fCamAngleMin < camRot.x && camRot.x < m_fCamAngleMax)
						{
							xAngle = float(oy) * XM_ANGLE * 0.1f;
							camRot.x += xAngle;
							if (m_fCamAngleMin < camRot.x && camRot.x < m_fCamAngleMax)
							{
								cam->Transform()->SetLocalRot(camRot);
							}
							else
							{
								xAngle = 0.0f;
							}

						}
					}
					yRot = float(ox)*XM_ANGLE * 0.4f;
					vRot.y += yRot;
					if (vRot.y > 360.0f)vRot.y -= 360.f;
					else if (vRot.y < -360.f)vRot.y += 360.f;
				}
				else
				{
					if (m_fMouseStopTime < 1.5f)
					{
						m_fMouseStopTime += DT;
					}
					else
					{
						m_pMousePos = m_pPreMousePos = m_rectHalf;
						SetCursorPos(m_rectHalf.x, m_rectHalf.y);
						m_fMouseStopTime = 0.0f;
						m_bMouseMoveOn = false;

					}
				}

			}
			//	계산부! 이거는 공격 방향 결정자! 이거 테스트 봐야하는데 일단 아링이꺼 풀!
			m_vAttackAngle.x = camRot.x;
			m_fAttackDistCur = m_fAttackDistMax - (camRot.x * m_fAttackDist);

			Vec3 nonYPlayerPos = vPos;
			nonYPlayerPos.y = 0.0f;

			m_vAttackPos = (vGo * m_fAttackDistCur) + nonYPlayerPos;
			m_vAttackPos.y = CMapMgr::GetInst()->GetHeight(m_vAttackPos);

			m_vAttackAngle.y += yRot;
			Matrix matRot = XMMatrixRotationX(m_vAttackAngle.x);
			matRot *= XMMatrixRotationY(m_vAttackAngle.y);
			matRot *= XMMatrixRotationZ(m_vAttackAngle.z);

			m_vAttackDir = XMVector3TransformNormal(Vec3::Front, matRot);

			Transform()->SetLocalRot(vRot);
		}
		if (nIdx != 9)
		{
			tMapRect t = CMapMgr::GetInst()->GetMapRect(nIdx);

			if (t.v4Rect.x + 10.f > vPos.x || vPos.x > t.v4Rect.y - 10.f || t.v4Rect.z + 10.f > vPos.z || vPos.z > t.v4Rect.w - 10.f)
			{
				Vec3 back = (t.v3Pos - vPos).Normalize();
				vPos += back * 10.f;
			}
		}
		else
		{
			Vec3 towerPos = CMapMgr::GetInst()->GetMap(9)->Transform()->GetLocalPos();
			float towerDist = Vec3::Distance(vPos, towerPos);
			if (towerDist > 195.f)
			{
				Vec3 back = (towerPos - vPos).Normalize();
				vPos += back * 10.f;
			}

		}

		//if (KEY_TAB(KEY_TYPE::KEY_LBTN))
		//{
		//
		//	CGameObject* pTemp = nullptr;
		//	
		//	vPos.y += 10.f;
		//	pTemp = CMapMgr::GetInst()->GetStoneData(0)->Instantiate();
		//	pTemp->Transform()->SetLocalPos(vPos + (m_vAttackDir*20.f));
		//	pTemp->Transform()->SetLocalScale(Vec3(3.f,3.f,3.f));
		//	pTemp->AddComponent(new CCollider2D);
		//	pTemp->Collider2D()->SetOffsetScale(Vec3(2.0f, 2.0f, 2.0f));
		//
		//	CSceneMgr::GetInst()->GetCurScene()->AddGameObject(L"Enviroment", pTemp, false);
		//}

		CMapMgr::GetInst()->SetPlayerClickedPos(m_vAttackPos);
		CMapMgr::GetInst()->SetPlayerAttackDir(m_vAttackDir);
		CMapMgr::GetInst()->SetStaticMouse(m_bStaticMouse);
		//	데미지 테스트
		std::list<tDamageStack>& damage = GetObj()->StatusComponent()->GetDamageStack();
		float fDamage = 0.f;
		Vec3 vDamageDir;
		while (!damage.empty())
		{
			fDamage += damage.back().fAttackDamage;
			vDamageDir = damage.back().vAttackDir;
			if (fDamage > 1.f)
			{
				//	순간데미지가 20.f 이상이면 데미지
				m_bDamaged = true;
			}
			damage.pop_back();
		}

		if (!m_GoldSkillOn)
		{
			if (!FloatCmp(fDamage, 0.0f, 0.0000001f))GetObj()->StatusComponent()->setHpDown(fDamage);
		}

		if (GetObj()->StatusComponent()->GetFloat(STATUS_FLOAT::HP) < 0.00001f)
		{
			m_bDeath = true;
		}
		//	행동부
		if (!m_bDeath)
		{
			if (m_bDamaged)
			{
				if (animation->IsAnimIdx((UINT)EC_PLAYER_ANIM::Damage))
				{
					if (animation->IsAnimEnd())
					{
						m_bDamaged = false;
					}
				}
				else
				{
					animation->ChangeAnimation((UINT)EC_PLAYER_ANIM::Damage, true);
					m_pPixCamera->RandomShudder();
				}
			}
			else
			{
				if (m_bSkillOn)
				{

					if (animation->IsAnimIdx((UINT)EC_PLAYER_ANIM::Attack))
					{
						if (animation->IsAnimEnd())
						{
							m_bSkillOn = false;
						}
					}
					else
					{
						animation->ChangeAnimation((UINT)EC_PLAYER_ANIM::Attack, true);
					}
				}
				else
				{
					if (m_bPressMove)
					{
						if (animation->IsAnimIdx((UINT)EC_PLAYER_ANIM::Run))
						{
							vPos += vMoveValue;
						}
						else
						{
							animation->ChangeAnimation((UINT)EC_PLAYER_ANIM::Run, true);
						}
					}
					else
					{
						if (animation->IsAnimIdx((UINT)EC_PLAYER_ANIM::Idle))
						{
							//	여기는 지금 쉬고있는중이라는거임
						}
						else
						{
							animation->ChangeAnimation((UINT)EC_PLAYER_ANIM::Idle, true);
						}
					}
				}
			}
		}
		else
		{
			if (animation->IsAnimIdx((UINT)EC_PLAYER_ANIM::Death))
			{
				//	죽는 거 하기
				if (animation->IsAnimEnd())
				{
					//	죽는 엔딩이나 다시 재시작
					animation->AnimStop();
					state->SetPlayerDeath(true);
					m_bDeathEvent = true;
				}
			}
			else
			{
				animation->ChangeAnimation((UINT)EC_PLAYER_ANIM::Death, true);
			}
		}

		vPos.y = CMapMgr::GetInst()->GetHeight(nIdx, vPos);
		Transform()->SetLocalPos(vPos);
		m_bPressMove = false;

		Vec3 camerLook = CMapMgr::GetInst()->GetMainCam()->Transform()->GetWorldDir(DIR_TYPE::FRONT);
		camerLook;
	}
}
