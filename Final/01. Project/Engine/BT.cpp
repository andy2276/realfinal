#include "stdafx.h"
#include "BT.h"

#include "MeshData.h"
#include "Transform.h"
#include "Animator3D.h"
#include "StatusComponent.h"

#include "MapMgr.h"

#include "GameObject.h"

UINT BT::Virtual::CNode::g_nID = 0;

CSkillPlatform * BT::Virtual::CNode::Skills()
{
	 return m_pProperty->Skills();
}

float BT::Virtual::CNode::Toc()
{
	return m_pProperty->GetToc();
}

CTransform * BT::Virtual::CNode::Transform()
{
	return m_pObj->Transform();
}

CAnimator3D * BT::Virtual::CNode::Animator3D()
{
	return m_pObj->Animator3D();
}

CStatusComponent * BT::Virtual::CNode::Status()
{
	return m_pObj->StatusComponent();
}

void BT::Collect::CSelection::StateReset()
{
	m_eState = BT::Return::Evaluation;
	for (int i = 0; i < m_pChild.size(); ++i)
	{
		m_pChild[i]->SetState(BT::Return::Evaluation);
	}
}

void BT::Collect::CSelection::SoftReset()
{
	for (int i = 0; i < m_pChild.size(); ++i)
	{
		m_pChild[i]->SoftReset();
	}
}

void BT::Collect::CSelection::HardReset()
{
}

BT::Return BT::Collect::CSelection::Evaluate()
{
	//	만약 성공이면 리턴 성공
	//	실패면 반복
	//	반복끝이면 실패

	//	만약 평가를 안한다면 이미 확인을 한것이기 때문에 리턴한다.
	if (BT::Return::Evaluation != m_eState)return m_eState;

	BT::Return re = BT::Return::Evaluation;
	for (int i = 0; i < m_pChild.size(); ++i)
	{
		re = m_pChild[i]->Evaluate();
		switch (re)
		{
			case BT::Return::Evaluation:
			{
				m_bRecordType = false;
				break;
			}
			case BT::Return::Failure:
			{
				break;
			}
			case BT::Return::Succeess:
			{
				if (m_bRecordType)m_eState = BT::Return::Succeess;
				else m_eState = BT::Return::Evaluation;
				return m_eState;
			}
			case BT::Return::Running:
			{
				return BT::Return::Running;
			}
		}
	}

	if (m_bRecordType)m_eState = BT::Return::Failure;
	else m_eState = BT::Return::Evaluation;

	return m_eState;
}

void BT::Virtual::CComposite::StateReset()
{
	m_eState = BT::Return::Evaluation;
	for (int i = 0; i < m_pChild.size(); ++i)
	{
		m_pChild[i]->SetState(BT::Return::Evaluation);
	}
}

void BT::Virtual::CComposite::SoftReset()
{
	for (int i = 0; i < m_pChild.size(); ++i)
	{
		m_pChild[i]->SoftReset();
	}
}

void BT::Virtual::CComposite::HardReset()
{
}

void BT::Collect::CSequence::StateReset()
{
	m_eState = BT::Return::Evaluation;
	for (int i = 0; i < m_pChild.size(); ++i)
	{
		m_pChild[i]->SetState(BT::Return::Evaluation);
	}
}

void BT::Collect::CSequence::SoftReset()
{
	for (int i = 0; i < m_pChild.size(); ++i)
	{
		m_pChild[i]->SoftReset();
	}
}

void BT::Collect::CSequence::HardReset()
{
}

BT::Return BT::Collect::CSequence::Evaluate()
{
	//	만약 성공이면 반복
	//	실패면 리턴 실패
	//	반복끝이면 성공
	if (BT::Return::Evaluation != m_eState)return m_eState;

	BT::Return re = BT::Return::Evaluation;
	for (int i = 0; i < m_pChild.size(); ++i)
	{
		re = m_pChild[i]->Evaluate();
		switch (re)
		{
			case BT::Return::Evaluation:
			{
				m_bRecordType = false;
				break;
			}
			case BT::Return::Failure:
			{
				if (m_bRecordType)m_eState = BT::Return::Failure;
				else m_eState = BT::Return::Evaluation;

				return m_eState;
			}
			case BT::Return::Succeess:
			{
				break;
			}
			case BT::Return::Running:
			{
				return BT::Return::Running;
			}
		}
	}
	if (m_bRecordType)m_eState = BT::Return::Succeess;
	else m_eState = BT::Return::Evaluation;

	return m_eState;
}
void BT::Collect::CReDecorate::StateReset()
{
	m_eState = BT::Return::Evaluation;
	for (int i = 0; i < m_pChild.size(); ++i)
	{
		m_pChild[i]->SetState(BT::Return::Evaluation);
	}
}
void BT::Collect::CReDecorate::SoftReset()
{
	for (int i = 0; i < m_pChild.size(); ++i)
	{
		m_pChild[i]->SoftReset();
	}
}
void BT::Collect::CReDecorate::HardReset()
{
}
void BT::Collect::CReDecorate::SetData(int _offset, void * pData)
{
	m_nType = *static_cast<int*>(pData);

}
BT::Return BT::Collect::CReDecorate::Evaluate()
{
	BT::Return re = BT::Return::Evaluation;
	if (m_nType)//	seq
	{
		for (int i = 0; i < m_pChild.size(); ++i)
		{
			re = m_pChild[i]->Evaluate();
			switch (re)
			{
			case BT::Return::Evaluation:
				break;
			case BT::Return::Failure:
				StateReset();
				return BT::Return::Failure;
			case BT::Return::Succeess:
				break;
			case BT::Return::Running:
				return BT::Return::Running;
			}
		}
		StateReset();
		return BT::Return::Succeess;
	}
	else
	{
		//	이걸 러닝일때 처음부터 할지 저장할지 생각해보자.
		for (int i = 0; i < m_pChild.size(); ++i)
		{
			re = m_pChild[i]->Evaluate();
			switch (re)
			{
			case BT::Return::Evaluation:
				break;
			case BT::Return::Failure:
				break;
			case BT::Return::Succeess:
				StateReset();
				return BT::Return::Succeess;
			case BT::Return::Running:
				return BT::Return::Running;
			}
		}
		StateReset();
		return BT::Return::Failure;
	}
	return BT::Return::Failure;
}

BT::Tool::CBlackBoard::CBlackBoard()
	:pPlayer(nullptr)
	, m_bIsLook(false)
{
	for (int& i : arrMappingAnim)
	{
		i = -1;
	}
}

BT::Tool::CBlackBoard::~CBlackBoard()
{
}
//	블랙보드 업데이트 매번해줘야함!
void BT::Tool::CBlackBoard::UpdateBlackBoard()
{
	m_v3PlayerPos = pPlayer->Transform()->GetLocalPos();
	m_v3PlayerLook = pPlayer->Transform()->GetLocalDir(DIR_TYPE::FRONT);
	m_v3PlayerRight = pPlayer->Transform()->GetLocalDir(DIR_TYPE::RIGHT);
	m_v3MainCamPos = CMapMgr::GetInst()->GetMainCam()->Transform()->GetLocalPos();

	m_v3NoYPlayerPos = m_v3PlayerPos;
	m_v3NoYPlayerPos.y = 0.f;

}

const Vec3 & BT::Tool::CBlackBoard::PlayerToLook(const Vec3 & _mePos)
{
	static Vec3 fPlayerLook,mePos;
	mePos = _mePos;
	mePos.y = 0.f;
	fPlayerLook = (m_v3NoYPlayerPos - mePos).Normalize();
	return fPlayerLook;
}

const bool & BT::Tool::CBlackBoard::IsLookPlayer()
{
	return m_bIsLook;
}

const float& BT::Tool::CBlackBoard::PlayerToDist(const Vec3 & _mePos)
{
	static float fDist = 0.f;
	static Vec3 mePos;

	mePos = _mePos;
	mePos.y = 0.f;

	fDist = Vec3::Distance(m_v3NoYPlayerPos, mePos);
	return fDist;
}

const float & BT::Tool::CBlackBoard::PlayerToAngle(const Vec3 & _mePos, const Vec3& _meLook)
{
	static float fAngle = 0.f;
	static Vec3 vLook,vDir,mePos;

	mePos = _mePos;
	mePos.y = 0.f;
	vLook = PlayerToLook(mePos);

	fAngle = vLook.Dot(_meLook);
	
	vDir = _meLook.Cross(vLook);
	if (vDir.y < 0.f)m_bIsLook = false;
	else m_bIsLook = true;

	return fAngle;
}

BT::Tool::CBTMaker::CBTMaker()
	: pObj(nullptr)
	, pBlackBoard(nullptr)
	, pProperty(nullptr)
	, pRoot(nullptr)
	, pPreParent(nullptr)
	, pCurParent(nullptr)
	, pPreNode(nullptr)
	, pCurNode(nullptr)
	, pPreChild(nullptr)
	, pCurChild(nullptr)
	, pFindNode(nullptr)
{
	
}

BT::Tool::CBTMaker::~CBTMaker()
{
}

void BT::Tool::CBTMaker::Init(CGameObject * _pObj, BT::Tool::CBlackBoard * _black)
{
	pObj = _pObj;
	pBlackBoard = _black;
	pProperty = new BT::Tool::CAIProperty;
	pMapNodes = &pProperty->GetNodeMap();
	vecNodes = &pProperty->GetNodeVector();
}

void BT::Tool::CBTMaker::Init(CGameObject * _pObj, BT::Tool::CBlackBoard * _black, CSkillPlatform * _plat)
{
	pObj = _pObj;
	pBlackBoard = _black;
	pProperty = new BT::Tool::CAIProperty;
	pMapNodes = &pProperty->GetNodeMap();
	pProperty->SetSkillPlatform(_plat);
}

BT::Tool::CBTMaker * BT::Tool::CBTMaker::Search(const wstring & _name)
{
	std::map<wstring, BT::Virtual::CNode*>::iterator itor = pMapNodes->find(_name);
	if (pMapNodes->end() != itor)
	{
		pFindNode = (*itor).second;
		pPreNode = pCurNode;
		pCurNode = pFindNode;
	}
	else
	{
		//	찾지 못함
		assert(nullptr);
	}
	return this;
}

BT::Tool::CBTMaker * BT::Tool::CBTMaker::SetData(int _offset, void * _pData)
{
	pCurNode->SetData(_offset, _pData);
	
	return this;
}

BT::Tool::CBTMaker * BT::Tool::CBTMaker::Parent(const wstring & _name)
{
	//	반복을 줄이기 위해서 만듦
	if (pCurParent != nullptr && pCurParent->GetName() == _name)return this;
	std::map<wstring, BT::Virtual::CNode*>::iterator itor = pMapNodes->find(_name);
	if (pMapNodes->end() != itor)
	{
		pFindNode = (*itor).second;
		pPreNode = pCurNode;
		pCurNode = pFindNode;
	}
	else
	{
		//	찾지 못함
		assert(nullptr);
	}
	pPreParent = pCurParent;
	pCurParent = pFindNode;
	return this;
}


BT::Tool::CBTMaker * BT::Tool::CBTMaker::a____()
{
	return this;
}

BT::Virtual::CNode * BT::Tool::CBTMaker::DirectParent()
{
	return pCurParent;
}

BT::Tool::CAIProperty::CAIProperty()
	:StackDmg(nullptr)
	, m_fToc(0.f)
	, m_pSkillPlatform(nullptr)
{
}

BT::Tool::CAIProperty::~CAIProperty()
{
}

BT::Virtual::CNode * BT::Tool::CAIProperty::Search(const wstring & _name)
{
	std::map<wstring, BT::Virtual::CNode*>::iterator itor = m_mapNodes.find(_name);
	if (m_mapNodes.end() != itor)
	{
		return (*itor).second;
	}
	else
	{
		return nullptr;
	}

}

void BT::Tool::CAIProperty::StatusReset()
{
	for (int i = 0; i < m_vecNodes.size(); ++i)
	{
		m_vecNodes[i]->SetState(BT::Return::Evaluation);
	}
}

void BT::Collect::CCollaborate::StateReset()
{
}

void BT::Collect::CCollaborate::SoftReset()
{
}

void BT::Collect::CCollaborate::HardReset()
{
}

void BT::Collect::CCollaborate::SetData(int _offset, void * pData)
{
	if (_offset < 0)
	{
		int size = GetChildVector().size();
		if (m_vecCollect.size() < size)
		{
			m_vecCollect.resize(size);
		}
	}
	else
	{
		BT::Collect::CCollaborate::tCollabo collabo = *static_cast<BT::Collect::CCollaborate::tCollabo*>(pData);
		m_vecCollect[_offset] = collabo;
	}
}

BT::Return BT::Collect::CCollaborate::Evaluate()
{
	for (int i = 0; i < m_pChild.size(); ++i)
	{
		switch (m_vecCollect[i].reValue)
		{
		case BT::Return::Evaluation:
			m_vecCollect[i].reValue = m_pChild[i]->Evaluate();
			break;
		case BT::Return::Failure:
			break;
		case BT::Return::Succeess:
			break;
		case BT::Return::Running:
			break;
		default:
			break;
		}
	}

	return BT::Return();
}
