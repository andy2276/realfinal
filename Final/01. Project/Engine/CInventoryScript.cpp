#include "stdafx.h"
#include "CInventoryScript.h"

#include"RenderMgr.h"
#include"SceneMgr.h"
#include"Collider2D.h"
#include"StatusComponent.h"
void CInventoryScript::awake()
{




}

void CInventoryScript::update()
{
	if (m_bOnce)
	{
		m_inventoryBorder->SetActive(false);
		for (int i = 1; i < 5; ++i)
		{
			m_arrScroll_Icon[i]->SetActive(false);
		}
		for (int i = 0; i < m_iconTotalCount; ++i)
		{
			m_IconArr[i]->SetActive(false);
		}
		for (int i = 0; i < 5; ++i)
		{
			m_arrPassive_Icon[i]->SetActive(false);
		}
		m_bOnce = false;
	}
	


	if (isOnInventory)
	{

	//	m_res = CRenderMgr::GetInst()->GetResolution();


		POINT VPOS = CKeyMgr::GetInst()->GetMousePos();
		Vec3 VLPOS = GetObj()->Transform()->GetLocalPos();

		Vec3 MousViewPos; //마우스 포지션 화면 포지션으로 변환한것 
		MousViewPos.x = VPOS.x - m_res.fWidth / 2.f;
		MousViewPos.y = -(float)VPOS.y + m_res.fHeight / 2.f;
		MousViewPos.z = 1.0000000001;
		//-------------------------------------------------------------


		for (int i = 0; i < m_iconTotalCount; ++i)
		{
			//현재 가지고 있는 스킬의 개수만큼 까지만 setActive 상태이다. 
			if (i < 20)
			{			
				//해당 인덱스에 어떤 스킬이 있는지를 받아와서
				int j = m_strInventoryIcon[i].skillType;
				if (j != -1)
				{
					m_IconArr[i]->MeshRender()->SetMaterial(m_arrSkill_iconMtrl[j]);
				//	m_IconArr[i]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, m_arrSkill_iconTex[j].GetPointer());
					m_IconArr[i]->SetActive(true);
				}
				else {
					//m_IconArr[i]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, nullptr);
					m_IconArr[i]->SetActive(false);
				}
				// 해당 이미지를 연결해준다. 
			}
			else {
				//비어있는 인덱스의 경우 렌더를 하지 않는다  
				
			}//-> 추후에 이 포문은 아이템을 습득했을경우에 한번만 업데이트 해주는 함수로 변경해줘야함(시간있을때)	

			
			if (i < (UINT)PLAYER_SKILL_TYPE::END)
			{
				if (m_strScrollIcon[i].skillType != -1)
				{
					int j = m_strScrollIcon[i].skillType;
					m_arrScroll_Icon[i]->MeshRender()->SetMaterial(m_arrSkill_iconMtrl[j]);
					//m_arrScroll_Icon[i]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, m_arrSkill_iconTex[j].GetPointer());
					m_arrScroll_Icon[i]->SetActive(true);

				}
				else {
				//	m_arrScroll_Icon[i]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, nullptr);
					m_arrScroll_Icon[i]->SetActive(false);

				}
			}
		}

	


		if (m_isInvenClick) //올바르게 클릭을 한 이후부터는 아이콘이 마우스를 따라다닌다 
		{
			m_IconArr[ClickIconNum]->Transform()->SetLocalPos(MousViewPos);
		}
		else if (m_isScrollClick)
		{
			m_arrScroll_Icon[ClickIconNum]->Transform()->SetLocalPos(MousViewPos);
		}


		if (KEY_TAB(KEY_TYPE::KEY_0))
		{
			for (int i = 0; i < 5; i++)
			{
				m_strInventoryIcon[i].skillType = i;
			}
		}

		if (KEY_TAB(KEY_TYPE::KEY_LBTN))
		{

			//클릭이 이미되어있을때 vs 처음 클릭할때 
			if (m_isInvenClick)
			{	//텍스쳐가 마우스를 따라다니고있을때 , 
				//제자리를 클릭했을 경우 - 그 위치로 아이콘의 위치가 저장된다.  vs 이상한곳을 클릭했을 경우 -> 원래 있던 위치로 되돌아간다. 

				for (int i = 0; i < m_ScrollCount; ++i)
				{

					bool b = true;
					if (MousViewPos.x <= m_strScrollIcon[i].vPos.x - m_Scroll_IonScale.x / 2)
						b = false;
					if (MousViewPos.x >= m_strScrollIcon[i].vPos.x + m_Scroll_IonScale.x / 2)
						b = false;
					if (MousViewPos.y <= m_strScrollIcon[i].vPos.y - m_Scroll_IonScale.y / 2)
						b = false;
					if (MousViewPos.y >= m_strScrollIcon[i].vPos.y + m_Scroll_IonScale.y / 2)
						b = false;

					//b가 트루라면 충돌했다는것이므로 On 상태로 만든다. 
					if (b)
					{
						int newType = m_strInventoryIcon[i].skillType;
						int nowType = m_strInventoryIcon[ClickIconNum].skillType;

						//이때 이 아이콘의 위치는 원래대로 되돌려 주는대신에
						m_IconArr[ClickIconNum]->Transform()->SetLocalPos(m_strInventoryIcon[ClickIconNum].vPos);
						

						//스크롤은 새로운 타입을 가지게 되고 
						m_strScrollIcon[i].skillType = nowType;
						//그 타입에 따라 이미지를 그린다. 
						m_arrScroll_Icon[i]->MeshRender()->SetMaterial(m_arrSkill_iconMtrl[nowType]);
					//	m_arrScroll_Icon[i]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, m_arrSkill_iconTex[nowType].GetPointer());


						m_isInvenClick = false; //클릭은 false로 되돌아 간다. 
						

						//원래 있던 아이콘의 스킬은 비어있게 되는것이므로, 스킬타입을 지운다 
						m_strInventoryIcon[ClickIconNum].skillType = -1;
						//그리고 현재 가지고 있는 스킬의 개수를 차감한다 
						m_IconArr[ClickIconNum]->SetActive(false);

						m_OwnSkillCount--;
						ClickIconNum = -1;

						break;
					}
					if (i == m_ScrollCount - 1)
					{	//잘못된곳을 클릭하였다면, 원래있던 자리로 되돌아간다. 
						if (!b)
						{
							m_IconArr[ClickIconNum]->Transform()->SetLocalPos(m_strInventoryIcon[ClickIconNum].vPos);
							m_isInvenClick = false;
							ClickIconNum = -1;
						}
					}

				}
			}
			else if (m_isScrollClick)
			{
				//스크롤 위치를 클릭했을때 
				for (int i = 0; i < 6; ++i)
				{
					
					bool b = true;
					if (MousViewPos.x <= m_strScrollIcon[i].vPos.x - m_Scroll_IonScale.x / 2)
						b = false;
					if (MousViewPos.x >= m_strScrollIcon[i].vPos.x + m_Scroll_IonScale.x / 2)
						b = false;
					if (MousViewPos.y <= m_strScrollIcon[i].vPos.y - m_Scroll_IonScale.y / 2)
						b = false;
					if (MousViewPos.y >= m_strScrollIcon[i].vPos.y + m_Scroll_IonScale.y / 2)
						b = false;

					if (b)
					{
						//새로 옮겨진 자리에 아이콘이 없을경우 
						if (m_strScrollIcon[i].skillType == -1)
						{
							int newType = m_strScrollIcon[i].skillType;
							int nowType = m_strScrollIcon[ClickIconNum].skillType;

							// 새로 선택된 자리에 따라다니던 이미지를 놓아주고,  
							m_arrScroll_Icon[i]->MeshRender()->SetMaterial(m_arrSkill_iconMtrl[nowType]);
						//	m_arrScroll_Icon[i]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, m_arrSkill_iconTex[nowType].GetPointer());
							m_strScrollIcon[i].skillType = nowType;
							//원래의 자리엔 이미지를 제거한다. 
							m_strScrollIcon[ClickIconNum].skillType = -1;
							//m_arrScroll_Icon[ClickIconNum]->SetActive(false);

							//마우스를 따라다녔기때문에 위치도 변경해준다. 
							m_arrScroll_Icon[ClickIconNum]->Transform()->SetLocalPos(m_strScrollIcon[ClickIconNum].vPos);



							ClickIconNum = -1;
							m_isScrollClick = false;
							break;
						}
						else {//새로 옮겨진 자리에 아이콘이 있을경우
							int newType = m_strScrollIcon[i].skillType;
							int nowType = m_strScrollIcon[ClickIconNum].skillType;

							// 새로 선택된 자리에 따라다니던 이미지를 놓아주고,  
							m_arrScroll_Icon[i]->MeshRender()->SetMaterial(m_arrSkill_iconMtrl[nowType]);
						//	m_arrScroll_Icon[i]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, m_arrSkill_iconTex[nowType].GetPointer());
							// 원래의 자리엔 새로선택된자리에 있던 이미지를 놓아준다.  
							m_arrScroll_Icon[ClickIconNum]->MeshRender()->SetMaterial(m_arrSkill_iconMtrl[newType]);
						//	m_arrScroll_Icon[ClickIconNum]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, m_arrSkill_iconTex[newType].GetPointer());
							//마우스를 따라다녔기때문에 위치도 변경해준다. 
							m_arrScroll_Icon[ClickIconNum]->Transform()->SetLocalPos(m_strScrollIcon[ClickIconNum].vPos);


							//새로 선택된 자리는 원래의 이미지를 가지게 된다. 
							m_strScrollIcon[i].skillType = nowType;
							m_strScrollIcon[ClickIconNum].skillType = newType;


							ClickIconNum = -1;
							m_isScrollClick = false;
							break;
						}
		
					}


				}
			}
			else { //처음 클릭이 들어왔을때 


				bool temp = false; //else를 더 빨리빠져나가게 해주려고 
				//마우스의 위치와 rect충돌을 일으킨다			
				for (int i = 0; i < 20; ++i)
				{
					//스킬을 소지한 경우만 탐색한다 
					if(m_strInventoryIcon[i].skillType != -1)
					{
						bool b = true;
						if (MousViewPos.x <= m_strInventoryIcon[i].vPos.x - m_iconWidth / 2)
							b = false;
						if (MousViewPos.x >= m_strInventoryIcon[i].vPos.x + m_iconWidth / 2)
							b = false;
						if (MousViewPos.y <= m_strInventoryIcon[i].vPos.y - m_iconheight / 2)
							b = false;
						if (MousViewPos.y >= m_strInventoryIcon[i].vPos.y + m_iconheight / 2)
							b = false;

						//b가 트루라면 충돌했다는것이므로 On 상태로 만든다. 
						if (b)
						{
							ClickIconNum = i;//현재 이아이콘의 넘버를 알려주고, 실시간으로 매번 set을 해준다. 
							m_isInvenClick = true; //클릭 온 
							break;
							temp = true;
						}
					}

				}
				if (!temp) // 인벤토리와 충돌하지 않았을때 
				{
					//스크롤을 클릭하여 스크롤과 자리를 맞바꿀때 , 
					//단, 이미 스킬이 있는것일경우에만 위치를 맞바꾸는것이므로 
					for (int i = 0; i < 6; ++i)
					{
						if (m_strScrollIcon[i].skillType != -1)
						{
							bool b = true;
							if (MousViewPos.x <= m_strScrollIcon[i].vPos.x - m_Scroll_IonScale.x / 2)
								b = false;
							if (MousViewPos.x >= m_strScrollIcon[i].vPos.x + m_Scroll_IonScale.x / 2)
								b = false;
							if (MousViewPos.y <= m_strScrollIcon[i].vPos.y - m_Scroll_IonScale.y / 2)
								b = false;
							if (MousViewPos.y >= m_strScrollIcon[i].vPos.y + m_Scroll_IonScale.y / 2)
								b = false;

							if (b)
							{
								ClickIconNum = i;//현재 이아이콘의 넘버를 알려주고, 실시간으로 매번 set을 해준다. 
								m_isScrollClick = true; //클릭 온 

								//원래그림이 있던 자리에는 그림이 사라진다. 
							//	m_arrScroll_Icon[ClickIconNum]->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, nullptr);

								break;
							}
						}

					}

				}
			}
		}
	}
		
	
}

CInventoryScript::CInventoryScript() : CScript((UINT)SCRIPT_TYPE::INVENTORYSCRIPT),m_res(CRenderMgr::GetInst()->GetResolution())
{
	
	//=======================================================================================
	// 멤버변수 초기화 
	//=======================================================================================

	m_borderThickness = 20; //인벤토리 테두리 두께 

	m_width = 280.f; //인벤토리 창 가로 크기 
	m_height = 370.f ;

	m_iconColCount = 4;// 아이콘 행 개수
	m_iconRowCount = 5; // 아이콘 열 개수
	m_iconTotalCount = m_iconColCount * m_iconRowCount;
	m_iconWidth = (m_width - m_borderThickness ) / (m_iconColCount); //아이콘 한개의 가로 크기
	m_iconheight = (m_height- m_borderThickness ) / (m_iconRowCount ); //아이콘 한개의 가로 크기

	m_PosCenter = Vec3(-300, -20, 1.1f); // 인벤토리 중심의 위치 

	m_iconColliderHeightSize = m_iconheight / 3;
	m_iconColliderWidthSize = m_iconWidth / 3;


	m_isSelection = false;

	colliderIcon = new CGameObject;
	Ptr< CShader> shader = CResMgr::GetInst()->FindRes<CShader>(L"TexShader");
	//Collider2D();

	for (int i = 0; i < m_iconRowCount; ++i)
	{
		for (int j = 0; j < m_iconColCount; ++j)
		{
			// 아이콘의 x좌표는 좌상단에서부터 아이콘 크기만큼씩 증가한다 
			// { 센터좌표 - (인벤토리창의가로크기 /2) } + ( i * 아이콘가로크기)  - 테두리 두께 
			float x = m_PosCenter.x - (m_width / 2.f) + ((j) * (m_iconWidth))  + (m_iconWidth / 2.f)  + m_borderThickness/2.f;
			float y = m_PosCenter.y + (m_height / 2.f) - ((i) * (m_iconheight))  - (m_iconheight /2.f )  - m_borderThickness/2.f;

			m_strInventoryIcon[(int)(i *  m_iconColCount) + j].vPos.x = x;
			m_strInventoryIcon[(int)(i *  m_iconColCount) + j].vPos.y = y;
			m_strInventoryIcon[(int)(i *  m_iconColCount) + j].vPos.z = 1.000001f;
		}
	} 
	isOnInventory_Icon = new bool[m_iconColCount * m_iconRowCount];
	


	//====================================================================================================================
	// 인벤토리 창 테두리 UI
	//====================================================================================================================

	m_inventoryBorder = new CGameObject;
	m_inventoryBorder->SetName(L"UI Border");
	m_inventoryBorder->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	m_inventoryBorder->AddComponent(new CTransform);
	m_inventoryBorder->AddComponent(new CMeshRender);

	Vec3 vScale2 = Vec3(m_width - m_borderThickness+18, m_height + m_borderThickness, 1.f);
	m_inventoryBorder->Transform()->SetLocalPos(Vec3(m_PosCenter.x  ,m_PosCenter.y+18  , 1.2f));

	m_inventoryBorder->Transform()->setPhysics(false);

	m_inventoryBorder->Transform()->SetLocalScale(vScale2);

	m_inventoryBorder->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

	Ptr<CTexture> pInvenTex =  CResMgr::GetInst()->Load<CTexture>(L"inventoryui05", L"Texture\\inventoryui05.png");
	Ptr<CMaterial> pInvenMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"InvenBorderMtrl");
	if(pInvenMtrl.GetPointer() == nullptr)
	{
		pInvenMtrl = new CMaterial;
		pInvenMtrl->DisableFileSave();
		pInvenMtrl->SetShader(shader);
		CResMgr::GetInst()->AddRes(L"InvenBorderMtrl", pInvenMtrl);
		pInvenMtrl->SetData(SHADER_PARAM::TEX_0, pInvenTex.GetPointer());
	}
	m_inventoryBorder->MeshRender()->SetMaterial(pInvenMtrl);

	CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_inventoryBorder);

	//====================================================================================================================
	// 아이콘 UI - 인벤토리에 위치한 아이콘 각각의 한개씩을 말함 . 
	//====================================================================================================================

	const int nCopyTex = (UINT)PLAYER_SKILL_TYPE::END;

	pEmptyIcon = CResMgr::GetInst()->Load<CTexture>(L"UIborder", L"Texture\\UIborder.png");
	Ptr<CTexture> pTemp;
	Ptr<CTexture> copytexture[nCopyTex] =
	{
		CResMgr::GetInst()->Load<CTexture>(L"iconfire01",L"Texture\\icon_fire01.png")
		, CResMgr::GetInst()->Load<CTexture>(L"iconfire03", L"Texture\\icon_fire03.png")
		, CResMgr::GetInst()->Load<CTexture>(L"iconice01", L"Texture\\icon_ice01.png")
		, CResMgr::GetInst()->Load<CTexture>(L"iconice02", L"Texture\\icon_ice02.png")
		, CResMgr::GetInst()->Load<CTexture>(L"iconGold", L"Texture\\icon_Gold.png")
	//	, CResMgr::GetInst()->Load<CTexture>(L"iconbook", L"Texture\\icon_book.png")
	//	,
	};
	for (int i = 0; i < (UINT)PLAYER_SKILL_TYPE::END; ++i)
	{
		m_arrSkill_iconTex[i] = copytexture[i];
	}
	wstring icName[(UINT)PLAYER_SKILL_TYPE::END] =
	{
		L"iconfire01Mtrl"
		,L"iconfire03Mtrl"
		,L"iconice01Mtrl"
		,L"iconice02Mtrl"
		,L"iconGoldMtrl"
	};


	for (int i = 0; i < (UINT)PLAYER_SKILL_TYPE::END; ++i)
	{
		m_arrSkill_iconMtrl[i] = new CMaterial;
		m_arrSkill_iconMtrl[i]->DisableFileSave();
		m_arrSkill_iconMtrl[i]->SetShader(shader);
		CResMgr::GetInst()->AddRes(icName[i], m_arrSkill_iconMtrl[i]);
		m_arrSkill_iconMtrl[i]->SetData(SHADER_PARAM::TEX_0, m_arrSkill_iconTex[i].GetPointer());
	}

	//-------------------------------------------------------------------------
	Ptr<CMaterial> pUIborderMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"EmptyUIborderMtrl");
	

	static int a = 0;
	if (pUIborderMtrl.GetPointer() == nullptr)
	{
		++a;
		if (shader == nullptr)
		{
			a = 100;
		}
		pUIborderMtrl = new CMaterial;
		pUIborderMtrl->DisableFileSave();
		pUIborderMtrl->SetShader(shader);
		CResMgr::GetInst()->AddRes(L"EmptyUIborderMtrl", pUIborderMtrl);
		pUIborderMtrl->SetData(SHADER_PARAM::TEX_0, pEmptyIcon.GetPointer());

	}
	int k = pUIborderMtrl->GetID();
	k;
	a;
	m_IconArr.assign(m_iconColCount * m_iconRowCount, nullptr);
	for (int i = 0; i < m_iconColCount * m_iconRowCount; ++i)
	{
		m_IconArr[i] = new CGameObject;
		m_IconArr[i]->SetName(L"UI icon");
		m_IconArr[i]->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
		m_IconArr[i]->AddComponent(new CTransform);
		m_IconArr[i]->AddComponent(new CMeshRender);
		m_IconArr[i]->Transform()->setPhysics(false);

		//위치설정
		m_IconArr[i]->Transform()->SetLocalPos(m_strInventoryIcon[i].vPos);
		//크기 설정 
		Vec3 vScale2 = Vec3(m_iconWidth, m_iconheight, 1.f);
		m_IconArr[i]->Transform()->SetLocalScale(vScale2);

		m_IconArr[i]->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		m_IconArr[i]->MeshRender()->SetMaterial(pUIborderMtrl);

		CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_IconArr[i]);
	}
	

	//	Transform()->SetLocalPos(m_PosCenter);
//	Transform()->SetLocalScale(Vec3(m_width + m_borderThickness, m_height + m_borderThickness, 1.f));

	tResolution res = CRenderMgr::GetInst()->GetResolution();
	//-----------------------------------------------------------------------------
	// 스크롤 아이콘 위치 설정 - 하단 중앙에 위치한 아이콘들
	//-----------------------------------------------------------------------------

	//Ptr<CTexture> pIconBorder = CResMgr::GetInst()->Load<CTexture>(L"iconBorderTex", L"Texture\\iconBorder.png");



	m_ScrollCount = (UINT)PLAYER_SKILL_TYPE::END;
	isOnScroll_Icon = new bool[m_ScrollCount]; // 스크롤 아이콘이 눌렸는가 

	m_arrScroll_Icon.assign(m_ScrollCount, nullptr);

	for (int i = 0; i < m_ScrollCount; ++i)
	{
		m_arrScroll_Icon[i] = new CGameObject;
		m_arrScroll_Icon[i]->SetName(L"SCROLL ICON");
		m_arrScroll_Icon[i]->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
		m_arrScroll_Icon[i]->AddComponent(new CTransform);
		m_arrScroll_Icon[i]->AddComponent(new CMeshRender);

		//m_Scroll_IonScale = Vec3(80, 80.f, 1.f);
		m_Scroll_IonScale = Vec3(80, 80.f, 1.f);

		m_arrScroll_Icon[i]->Transform()->setPhysics(false);
		m_arrScroll_Icon[i]->Transform()->SetLocalScale(m_Scroll_IonScale);

		float x = -260 + 50 + (m_Scroll_IonScale.x) * i + (i * 30);
		float y = -(res.fHeight / 2.f) + 10 + (m_Scroll_IonScale.y / 2.f);

		m_strScrollIcon[i].vPos = Vec3(x, y, 1.1f);
		m_strScrollIcon[i].skillType = -1;
		m_arrScroll_Icon[i]->Transform()->SetLocalPos(Vec3(x, y, 1.1f));

		m_arrScroll_Icon[i]->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

		Ptr<CTexture> pIconBorderTex;
		Ptr<CMaterial> pIcoBordernMtrl;
		{
			pIconBorderTex = CResMgr::GetInst()->FindRes<CTexture>(L"iconBorderTex");
			if (pIconBorderTex.GetPointer() == nullptr)
			{
				pIconBorderTex = CResMgr::GetInst()->Load<CTexture>(L"empty", L"Texture\\empty.png");
			}

			pIcoBordernMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"iconBorderMtrl");
			if (pIcoBordernMtrl.GetPointer() == nullptr)
			{
				pIcoBordernMtrl = new CMaterial;
				pIcoBordernMtrl->DisableFileSave();
				pIcoBordernMtrl->SetShader(CResMgr::GetInst()->FindRes<CShader>(L"TexShader"));
				CResMgr::GetInst()->AddRes(L"iconBorderMtrl", pIcoBordernMtrl);
				pIcoBordernMtrl->SetData(SHADER_PARAM::TEX_0, pIconBorderTex.GetPointer());
			}
		}

		m_arrScroll_Icon[i]->MeshRender()->SetMaterial(pIcoBordernMtrl);
		CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_arrScroll_Icon[i]);
	}
	m_strScrollIcon[0].skillType = (int)PLAYER_SKILL_TYPE::SKILL_ICEARROW;
	m_arrScroll_Icon[0]->MeshRender()->SetMaterial(m_arrSkill_iconMtrl[(int)PLAYER_SKILL_TYPE::SKILL_ICEARROW]);
	//============================================================================================================
	// 패시브 아이콘 - 상단 위에 뜨는것
	//============================================================================================================

	const int iconPassiveCount = 5;
	Ptr<CTexture> pPassiveTex[iconPassiveCount] = {
		CResMgr::GetInst()->Load<CTexture>(L"iconbook", L"Texture\\icon_book.png")
		, CResMgr::GetInst()->Load<CTexture>(L"iconshield", L"Texture\\icon_shield.png")
		, CResMgr::GetInst()->Load<CTexture>(L"iconbullet", L"Texture\\icon_bullet.png")
		, CResMgr::GetInst()->Load<CTexture>(L"iconPower", L"Texture\\icon_power.png")
		, CResMgr::GetInst()->Load<CTexture>(L"iconEnergyShield", L"Texture\\icon_energyShield.png")

	};
	wstring wstrPassive[iconPassiveCount];
	wstrPassive[0] = L"iconbookMtrl";
	wstrPassive[1] = L"iconshieldMtrl";
	wstrPassive[2] = L"iconbulletMtrl";
	wstrPassive[3] = L"iconPowerMtrl";
	wstrPassive[4] = L"iconEnergyShieldMtrl";

	Ptr<CMaterial> pPassiveMtrl[iconPassiveCount] = {
			CResMgr::GetInst()->FindRes<CMaterial>(L"iconbookMtrl")
		, CResMgr::GetInst()->FindRes<CMaterial>(L"iconshieldMtrl")
		, CResMgr::GetInst()->FindRes<CMaterial>(L"iconbulletMtrl")
		, CResMgr::GetInst()->FindRes<CMaterial>(L"iconPowerMtrl")
		, CResMgr::GetInst()->FindRes<CMaterial>(L"iconEnergyShieldMtrl")
	};
	for (int i = 0; i < iconPassiveCount; ++i)
	{
		if (pPassiveMtrl[i].GetPointer() == nullptr)
		{
			pPassiveMtrl[i] = new CMaterial;
			pPassiveMtrl[i]->DisableFileSave();
			pPassiveMtrl[i]->SetShader(CResMgr::GetInst()->FindRes<CShader>(L"TexShader"));
			CResMgr::GetInst()->AddRes(wstrPassive[i], pPassiveMtrl[i]);
			pPassiveMtrl[i]->SetData(SHADER_PARAM::TEX_0, pPassiveTex[i].GetPointer());
		}
	}

	m_arrPassive_Icon.assign(iconPassiveCount, nullptr);

	for (int i = 0; i < iconPassiveCount; ++i)
	{
		m_arrPassive_Icon[i] = new CGameObject;
		m_arrPassive_Icon[i]->SetName(L"PASSIVE ICON");
		m_arrPassive_Icon[i]->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
		m_arrPassive_Icon[i]->AddComponent(new CTransform);
		m_arrPassive_Icon[i]->AddComponent(new CMeshRender);


		m_Passive_IonScale = Vec3(35, 35.f, 1.f);

		m_arrPassive_Icon[i]->Transform()->setPhysics(false);
		m_arrPassive_Icon[i]->Transform()->SetLocalScale(m_Passive_IonScale);

		m_arrPassive_Icon[i]->Transform()->SetLocalPos(Vec3((-res.fWidth / 2.f) + (m_Passive_IonScale.x + 10)*(i + 1) + 140, +(res.fHeight / 2.f) - 125, 1.00001f));

		m_arrPassive_Icon[i]->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		//pPassiveMtrl[i]
		m_arrPassive_Icon[i]->MeshRender()->SetMaterial(pPassiveMtrl[i]);

		CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_arrPassive_Icon[i]);

	}

	for (int i = 0; i < 20; i++)
	{

		// m_ArrOwnSkill 배열의 값이 -1일 경우 스킬을 가지고 있지 않음을 말한다. 
		// m_ArrOwnSkill의 인덱스는 몇번째 인벤토리창의 칸에서 생성될지의 역할을 담당한다 
		// m_ArrOwnSkill이 배열이 가지고 있는 int 값은 어떤 스킬인지를 말한다.  
		// 이 배열의 int 값을 받아와서 이미지를 띄워주고, 이 배열의 int 값을 받아와서 스킬 사용이 가능하도록 해주면 된다. 
		m_strInventoryIcon[i].skillType = -1;
	}


	//변수 초기화
	m_isInvenClick = false;
	ClickIconNum = NULL;
	IventoryNum = 0;

	isOnInventory = false;
	
	m_bOnce = true;

}


CInventoryScript::~CInventoryScript()
{
}

void CInventoryScript::OnCollisionEnter(CCollider2D * _pOther)
{

	if (L"UI icon" == _pOther->GetObj()->GetName())
		{

			m_isSelection = true;
			m_changePos = _pOther->GetObj()->Transform()->GetLocalPos();

			
		//	//	DeleteObject(GetObj());
		//	std::wcout << "충돌" << std::endl;
		//
		//	//목표 : 충돌했을경우 아이콘의 위치와 바꾸기
		//	Vec3 vPos = GetObj()->Transform()->GetLocalPos();
		//	Vec3 vIconPos = _pOther->GetObj()->Transform()->GetLocalPos();
		//	//			Vec3 vSwap= vPos;
		//
		//	_pOther->GetObj()->Transform()->SetLocalPos(vPos);
		//	GetObj()->Transform()->SetLocalPos(vIconPos);
			
			colliderIcon = _pOther->GetObj();

	}
	else {

	}
	
}

void CInventoryScript::setSkillOwn()
{
	//if (m_OwnSkillCount < 20)
	//{

	//	int random = rand() % 6;
	//	m_strInventoryIcon[m_OwnSkillCount].skillType = random;

	//	m_OwnSkillCount++;
	//}

	for (int i = 0; i < 20; i++)
	{
		if (m_strInventoryIcon[i].skillType == -1)
		{
			int random = rand() % 6;
			m_strInventoryIcon[i].skillType = random;

			m_OwnSkillCount++;
			break;
		}
	}
	//	지민추
	//for (int i = 0; i < 20; i++)
	//{
	//	if (m_strInventoryIcon[i].skillType == -1)
	//	{
	//		int random = rand() % 6;
	//		m_strInventoryIcon[i].skillType = random;
	//		if (m_strInventoryIcon[i].pObj == nullptr)
	//		{
	//			for (int i = 0; i < m_vecItemObjs.size(); ++i)
	//			{
	//				if(m_)
	//
	//			}
	//		}

	//		m_OwnSkillCount++;
	//		break;
	//	}
	//}

}

void CInventoryScript::setInventoryOn(bool b)
{
	isOnInventory = b;
	if (isOnInventory)
	{

		m_inventoryBorder->SetActive(true);
		for (int i = 0; i < m_iconTotalCount; ++i)
		{
			m_IconArr[i]->SetActive(true);
		}

	}
	else {

		m_inventoryBorder->SetActive(false);
		for (int i = 0; i < m_iconTotalCount; ++i)
		{
			m_IconArr[i]->SetActive(false);
		}

	}
}

