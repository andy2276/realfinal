#include "stdafx.h"
#include "FloorScript.h"
#include"MapMgr.h"
#include"BulletScript.h"
void FloorScript::update()
{
	if (m_bCanShoot)
	{
		if (!m_bClick)
		{
			Vec3 pos = CMapMgr::GetInst()->GetPlayerClickedPos();
			if (m_skillType == (UINT)PLAYER_SKILL_TYPE::SKILL_METEOR)
			{
				pos.y += 50;
			}
			else {
				pos.y += 3;
			}
			Transform()->SetLocalPos(pos);

			if (KEY_TAB(KEY_TYPE::KEY_RBTN))
			{
				m_bClick = true;
				m_pos = pos;
				m_bCanShoot = false;
				GetObj()->GetScript<CBulletScript>()->setCanShoot(true);
				pSound->Play(1,1, true);
			}
		}
		else {

		}
	}
	
}

FloorScript::FloorScript(UINT etype) : CScript((UINT)SCRIPT_TYPE::FLOORSCRIPT)

,m_skillType(etype)
{
	if (m_skillType == (UINT)PLAYER_SKILL_TYPE::SKILL_METEOR)
	{
		pSound = CResMgr::GetInst()->Load<CSound>(L"11_meteor", L"Sound\\11_meteor.wav");
		
	}
	else if (m_skillType == (UINT)PLAYER_SKILL_TYPE::SKILL_SNOWSTORM) {
		pSound = CResMgr::GetInst()->Load<CSound>(L"35_snowstorm", L"Sound\\35_snowstorm.flac");

	}
	m_bClick = false;
	m_bCanShoot = false;
}

FloorScript::~FloorScript()
{
}
