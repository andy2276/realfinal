#pragma once
class CGameObject;


enum class EC_MAP_OBJ_TYPE {
	Monster
	,Item
	,End
};
//	이거 더빠르게 하고싶으면 bool로 계산된거면 계산안하게하자
class CMapRapper{
private:
	Vec2		m_pMapStart;
	Vec2		m_pMapEnd;
	Vec2		m_vMapPos;
	CGameObject* m_pMap;
	map<UINT, CGameObject*> m_mapObjReady[(UINT)EC_MAP_OBJ_TYPE::End];


//	map<UINT, CGameObject*> m_mapObjStop[(UINT)EC_MAP_OBJ_TYPE::End];
	UINT m_nCurMopCnt;
	UINT m_nCurMopStopCnt[(UINT)EC_MOSTER_TYPE::End];
	UINT m_nCurItemStopCnt;
	vector<CGameObject*> m_vecMonsterObjStop[(UINT)EC_MOSTER_TYPE::End];//	몬스터 인포에서 해당몬스터 개수를 저장해서 그거로 리사이즈 가장큰 몬스터 수를 리사이즈 해줘야함.
	vector<CGameObject*> m_vecItemObjStop;

	float*		m_pHeightMap;
	BYTE*		m_pColliderMap;
	
	BYTE*		m_pOriginColliderMap;

	float			m_fColMapScale;
	//	여기의 인덱스는 순서나 개수가 아니라 만약 내가 1번맵이면 8번과 2번에 데이터가 들어가 있을 것이다. 
	tPortal		m_arrPortal_Out[10];
	int			m_nNextMap;


	XMINT2		m_vHeightSize;
	XMINT2		m_vColSize;


	UINT		m_nFullSize;

	bool		m_bMovePassible;

	BYTE		m_bColID;
	vector<UINT>			m_vecAwakeIdx;
public:
	CMapRapper();
	~CMapRapper();

	void SetMapObj(UINT _type, CGameObject* _pObj);
	void SetMapObj(const EC_MAP_OBJ_TYPE& _type, CGameObject* _pObj);

	CGameObject* GetMapObj(UINT _type, UINT _id);
	CGameObject* GetMapObj(const EC_MAP_OBJ_TYPE& _type, UINT _id);

	//	----------------------------------------------------------------------------------------------
	//	Map Culling

	//	삭제를 시키는것이 아니라 해제만 시킨다
	void ReleaseAnyObj(UINT _type, UINT _id);

	//	삭제를 시키는것이 아니라 해제만 시킨다
	void ReleaseAnyObj(const EC_MAP_OBJ_TYPE& _type, UINT _id);	

	//	시점은 dead보다 앞서야함. 
	void ArrangeToDead();		

	//	if(Ready->IsActive(false))go to Stop 레디에 있는것들중 멈춘것들을 멈춤으로 이동
	void ArrangeToStop();		

	//	if(Stop->IsActive(true))go to Ready,	거의 일어나지 않을것임
	void ArrangeToReady();		

	//	맵의 레디 오브젝트들을 정지시킬 때
	void PauseReadyObj();
	//	맵의 레디 오브젝트들을 실행시킨다
	void PlayReadyObj();
	//	맵을 끌때
	void PauseMap();			
	//	맵을 띄울때
	void PlayMap();
	//	모든 오브젝트들을 해제한다
	void ReleaseAllObjs();
	//	Stop에 있는 것들을 모두 Ready로 옮긴다
	void ResetAllObjs();			

	//	----------------------------------------------------------------------------------------------
	//	MapHeight
	void SetHeightMap(wstring _filePath,int wid,int hei);
	void SetColliderMap(wstring _filePath, int wid, int hei,float _scale = 1.0f);

	const float GetCurHeight(float _z,float _X);
	float* GetHeightMap() {return m_pHeightMap;};
	
	
	BYTE* GetColliderMap() { return m_pColliderMap; }
	int GetNextMap() { return m_nNextMap; }

	//	----------------------------------------------------------------------------------------------
	//	MapCollider
	EC_COL_TYPE GetColType(int _z, int _x) {
		int value = m_pColliderMap[(_z * m_vColSize.x) + _x];
		if(value > 2)return EC_COL_TYPE::Path;
		else {
			if(value > 0 )return EC_COL_TYPE::Player;
			return EC_COL_TYPE::Blocked;
		}
	}
	Vec2 SearchAround(CGameObject* _pObj,int _z,int _x);
	//	포지션을 여기서 만들어서 줘야하나?! 그리고 오더에 따라서 다시 카운트 할수 있게 해야하나?!
	const Vec3& UnitCollider(CGameObject* _pObj, int _z, int _x,bool& _stop);
	void StempingColliderMap(BYTE _colId,int _z, int _x);
	void UpdateHeight();
	void ClearColliderMap(){ memcpy(m_pColliderMap, m_pOriginColliderMap, m_nFullSize); }
	const bool ColliderEnableStemping(BYTE _self, BYTE _type) {
		if ((BYTE)EC_COL_TYPE::Blocked == _type || _self == _type) {
			return false;
		}
		else {
			return true;
		}
	}
	//	----------------------------------------------------------------------------------------------
	//	Map Event
	//	내가 나가야하는 맵의 번호
	void SetPortal(POINT _lu, POINT _rd, const UINT& _outMapIdx,const Vec3& _startPos,const Vec3& _startRot);

	//	이걸로ㅓ 다음 인덱스를 얻어온다
	const Vec3& GetMoveMapPos(BYTE _curMapIdx) { return m_arrPortal_Out[_curMapIdx].StartPos; }
	const tPortal& GetPortal(int _idx) { return m_arrPortal_Out[_idx]; }

	//	----------------------------------------------------------------------------------------------
	//	Map Utility
	bool IsOver(const XMINT2 & size,int _z,int _x){
		return ((size.y < _z )||( _z < 0 )||( size.x < _x )||( _x < 0));
	}
	const Vec3& GetMapPos();
	const Vec2& GetMapPos2();


	void UpdateCollider();
	void Update();
	void SetMap(CGameObject* _pMap);


	void SetStartPos(float _x, float _z) {
		m_pMapStart = { _x,_z };
		//	추<		여기 매직넘버라 바꿔야함
		m_pMapEnd = { m_pMapStart.x +400.f,m_pMapStart.y + 400.f };
	}
	const Vec2& GetStartPos() { return m_pMapStart; }
	void SetAwakeIdx(int* _idxArr, int cnt);
	vector<UINT>& GetAwakeIdx() { return m_vecAwakeIdx; }

	UINT GetReadyObjCnt() { return m_mapObjReady->size(); }

	map<UINT, CGameObject*>& GetReadyMapObjs(UINT _type) { return m_mapObjReady[_type]; }

	//	트리거에서 이거를 켜주면 나중에 
	void MapMoveOn() { m_bMovePassible = true; }
	bool IsMapMovePossible() { return m_bMovePassible; }

	//	맵 포탈에 들어와잇는가?
	bool IsInPortar(const Vec2& _playerPos);

	void ResizeMonster(UINT _type,UINT _cnt) {
		m_vecMonsterObjStop[_type].resize(_cnt, nullptr);
	}
	float GetColMapScale() { return m_fColMapScale; }
	const XMINT2& GetColMapSize() { return m_vColSize; }

	//	----------------------------------------------------------------------------------------------
	//	Map Respon
	void CheckCurMonster();
	UINT GetCurMonsterCnt() { return m_nCurMopCnt; }
	CGameObject* OnMonster(UINT _type);
	
	void Level0();
	void Level1();
	void Level2();
	void Level3();
	void Level4();
	void Level5();
	void Level6();
	void Level7();
	void Level8();
	void LevelTower();

	//	이거를 마지막에 해줘야 몬스터가 정확히 들어감

};

