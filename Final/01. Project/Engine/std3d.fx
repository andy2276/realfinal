#include "value.fx"
#include "func.fx"

#ifndef _STD3D
#define _STD3D

// ==========================
// Std3D Shader
// Deferred = true
//
// g_tex_0 : Diffuse Texture
// g_tex_1 : Normalmap Texture
// BlendState : false
// ==========================
struct VS_STD3D_INPUT
{
    float3 vPos : POSITION;
    float2 vUV : TEXCOORD;
    
    float3 vTangent : TANGENT;
    float3 vNormal : NORMAL;
    float3 vBinormal : BINORMAL;
    
    float4 vWeight : BLENDWEIGHT;
    float4 vIndices : BLENDINDICES;
    
    // Instancing
    row_major matrix matWorld : WORLD;
    row_major matrix matWV : WV;
    row_major matrix matWVP : WVP;
    int iRowIdx : ROWINDEX; // Animaion 행렬 행
 // 추<   uint1 objId : OBJID;
    uint nObjId : OBJID;
    uint iInstanceID : SV_InstanceID;
};

struct VS_STD3D_OUTPUT
{
    float4 vPosition : SV_Position;
    float3 vViewPos : POSITION;
    
    nointerpolation float3 vViewTangent : TANGENT;
    nointerpolation float3 vViewNormal : NORMAL;
//    float3 vViewNormal : NORMAL;
    nointerpolation float3 vViewBinormal : BINORMAL;
    //  추< nointerpolation uint1 objId : OBJID;
    float2 vUV : TEXCOORD;
    uint nObjID : OBJID;
};

VS_STD3D_OUTPUT VS_Std3D(VS_STD3D_INPUT _in)
{
    VS_STD3D_OUTPUT output = (VS_STD3D_OUTPUT) 0.f;
        
    if (g_int_0)
    {
        Skinning(_in.vPos, _in.vTangent
                , _in.vBinormal, _in.vNormal
                , _in.vWeight, _in.vIndices, 0);        
    }
    
    output.vPosition = mul(float4(_in.vPos, 1.f), g_matWVP);
   
    output.vViewPos = mul(float4(_in.vPos, 1.f), g_matWV).xyz;
    output.vViewTangent = normalize(mul(float4(_in.vTangent, 0.f), g_matWV).xyz);
    output.vViewNormal = normalize(mul(float4(_in.vNormal, 0.f), g_matWV).xyz);
    output.vViewBinormal = normalize(mul(float4(_in.vBinormal, 0.f), g_matWV).xyz);
    output.vUV = _in.vUV;
    output.nObjID = g_objId;
    return output;
}


VS_STD3D_OUTPUT VS_Std3D_Inst(VS_STD3D_INPUT _in)
{
    VS_STD3D_OUTPUT output = (VS_STD3D_OUTPUT) 0.f;
        
    if (g_int_0)
    {
        Skinning(_in.vPos, _in.vTangent
        , _in.vBinormal, _in.vNormal
        , _in.vWeight, _in.vIndices, _in.iRowIdx);
    }
    
    output.vPosition = mul(float4(_in.vPos, 1.f), _in.matWVP);
   
    output.vViewPos = mul(float4(_in.vPos, 1.f), _in.matWV).xyz;
    output.vViewTangent = normalize(mul(float4(_in.vTangent, 0.f), _in.matWV).xyz);
    output.vViewNormal = normalize(mul(float4(_in.vNormal, 0.f), _in.matWV).xyz);
    output.vViewBinormal = normalize(mul(float4(_in.vBinormal, 0.f), _in.matWV).xyz);
    output.vUV = _in.vUV;
    output.nObjID = _in.nObjId;
    return output;
}
struct GS_STD3D_OUTPUT
{
    float4 vPosition : SV_Position;
    
    float3 vViewPos : POSITION;
    
    nointerpolation float3 vViewTangent : TANGENT;
    nointerpolation float3 vViewNormal : NORMAL;
//    float3 vViewNormal : NORMAL;
    nointerpolation float3 vViewBinormal : BINORMAL;
    //  추< nointerpolation uint1 objId : OBJID;
    float2 vUV : TEXCOORD;
    uint nObjID : OBJID;
};
[maxvertexcount(3)]
void GS_STD3D(triangle VS_STD3D_OUTPUT _in[3], inout TriangleStream<GS_STD3D_OUTPUT> OutputStream)
{
    GS_STD3D_OUTPUT output[3] =
    {
        (GS_STD3D_OUTPUT) 0.f, (GS_STD3D_OUTPUT) 0.f, (GS_STD3D_OUTPUT) 0.f
    };
  //  float3 n = 0.3333333f * (_in[0].vViewNormal + _in[1].vViewNormal + _in[2].vViewNormal);
    float3 n = cross(normalize(_in[1].vPosition.xyz - _in[0].vPosition.xyz), normalize(_in[2].vPosition.xyz - _in[0].vPosition.xyz));
    
    output[0].vPosition = _in[0].vPosition;
    output[0].vViewPos = _in[0].vViewPos;
    output[0].vViewTangent = _in[0].vViewTangent;
    output[0].vViewNormal = n;
    output[0].vViewBinormal = _in[0].vViewBinormal;
    output[0].vUV = _in[0].vUV;
    output[0].nObjID = _in[0].nObjID;
    
    output[1].vPosition = _in[1].vPosition;
    output[1].vViewPos = _in[1].vViewPos;
    output[1].vViewTangent = _in[1].vViewTangent;
    output[1].vViewNormal = n;
    output[1].vViewBinormal = _in[1].vViewBinormal;
    output[1].vUV = _in[1].vUV;
    output[1].nObjID = _in[1].nObjID;
    
    output[2].vPosition = _in[2].vPosition;
    output[2].vViewPos = _in[2].vViewPos;
    output[2].vViewTangent = _in[2].vViewTangent;
    output[2].vViewNormal = n;
    output[2].vViewBinormal = _in[2].vViewBinormal;
    output[2].vUV = _in[2].vUV;
    output[2].nObjID = _in[2].nObjID;
    
    OutputStream.Append(output[0]);
    OutputStream.Append(output[1]);
    OutputStream.Append(output[2]);
    OutputStream.RestartStrip();
}


struct PS_STD3D_OUTPUT
{ 
    float4 vTarget0 : SV_Target0; // Diffuse
    float4 vTarget1 : SV_Target1; // Normal
    float4 vTarget2 : SV_Target2; // Position
    
    uint nTarget3 : SV_Target3;
};

PS_STD3D_OUTPUT PS_Std3D(GS_STD3D_OUTPUT _in)
{
    PS_STD3D_OUTPUT output = (PS_STD3D_OUTPUT) 0.f;
  
    if (tex_0)
        output.vTarget0 = g_tex_0.Sample(g_sam_1, _in.vUV);
    else
        output.vTarget0 = float4(1.f, 0.f, 1.f, 1.f);
        
    if (g_nLButtonDown)
    {
        float2 screenUV = float2(_in.vPosition.x / g_vResolution.x, _in.vPosition.y / g_vResolution.y);
        if (FloatEqual(screenUV.x, g_vMousePos.x, 0.001f) && FloatEqual(screenUV.y, g_vMousePos.y, 0.001f))
        {
            g_RWObjId[0].x = _in.nObjID;
        }
    }
    float3 vViewNormal = _in.vViewNormal;
    // 노말맵이 있는경우
    if (tex_1)
    {
        float3 vTSNormal = g_tex_1.Sample(g_sam_0, _in.vUV).xyz;
        vTSNormal.xyz = (vTSNormal.xyz - 0.5f ) * 2.f;
        float3x3 matTBN = { _in.vViewTangent, _in.vViewBinormal, _in.vViewNormal };
        vViewNormal = normalize(mul(vTSNormal, matTBN));
    }
    
    output.vTarget1.xyz = vViewNormal;
    output.vTarget2.xyz = _in.vViewPos;
    output.nTarget3.x = _in.nObjID.x;
    
    return output;
}


// =============
// Skybox Shader
// mesh         : sphere
// rasterizer   : CULL_FRONT
// DepthStencilState : Less_Equal
// g_tex_0 : Output Texture
// =============
struct VS_SKY_IN
{
    float3 vPos : POSITION;
    float2 vUV : TEXCOORD;
};

struct VS_SKY_OUT
{
    float4 vPosition : SV_POSITION;
    float2 vUV : TEXCOORD;
};

VS_SKY_OUT VS_Skybox(VS_SKY_IN _in)
{
    VS_SKY_OUT output = (VS_SKY_OUT) 0.f; 
    
    float4 vViewPos = mul(float4(_in.vPos, 0.f), g_matView);
    float4 vProjPos = mul(vViewPos, g_matProj);
    
    // w 값으로 z 값을 나눌것이기 때문에 미리 w 값을 셋팅해두면
    // 어떤 상황에서도 깊이값이 1.f 로 판정된다.
    vProjPos.z = vProjPos.w;
    
    output.vPosition = vProjPos;
    output.vUV = _in.vUV;
    
    return output;
}

float4 PS_Skybox(VS_SKY_OUT _in) : SV_Target
{
    float4 vOutColor = g_tex_0.Sample(g_sam_0, _in.vUV);  
    return vOutColor;
}

//-----------------------------------------
// Monster Hp Shaer 

// g_float_0 : Start Scale
// g_float_1 : End Scale

// g_tex_0 : Particle Texture
//------------------------------------------

struct VTX_HP_IN
{
    float3 vPos : POSITION;
    
   
    float2 vUV : TEXCOORD;
    
};

struct VTX_HP_OUT
{
    float4 vViewPos : POSITION;
    float2 vUV : TEXCOORD;
};



VTX_HP_OUT VS_MonsterHp(VTX_HP_IN _in)
{
    VTX_HP_OUT output = (VTX_HP_OUT) 0.f;
    
    float3 vWorldPos = mul(float4(_in.vPos, 1.f), g_matWorld).xyz;
  //  vWorldPos += tData[_in.iID].vWorldPos;
    
    output.vViewPos = mul(float4(vWorldPos, 1.f), g_matView);
    output.vUV = _in.vUV;
    
    return output;
}

struct GS_HP_OUT
{
    float4 vPosition : SV_Position;
    float2 vUV : TEXCOORD;
//    uint iInstID : SV_InstanceID;
};


[maxvertexcount(6)]
void GS_MonsterHp(point VTX_HP_OUT _in[1], inout TriangleStream<GS_HP_OUT> OutputStream)
{
    GS_HP_OUT output[4] =
    {
        (GS_HP_OUT) 0.f, (GS_HP_OUT) 0.f, (GS_HP_OUT) 0.f, (GS_HP_OUT) 0.f
    };
    
    //uint iInstID = (uint) _in[0].iInstID;
    
    
    //if (0 == tData[iInstID].iAlive)
    //    return;
    
   // float fRatio = tData[iInstID].m_fCurTime / tData[iInstID].m_fLifeTime;
   // float fCurScale = ((g_float_1 - g_float_0) * fRatio + g_float_0) / 2.f;
    
    float fCurScale = 1;
    
    output[0].vPosition = _in[0].vViewPos + float4(-fCurScale, fCurScale, 0.f, 0.f);
    output[1].vPosition = _in[0].vViewPos + float4(fCurScale, fCurScale, 0.f, 0.f);
    output[2].vPosition = _in[0].vViewPos + float4(fCurScale, -fCurScale, 0.f, 0.f);
    output[3].vPosition = _in[0].vViewPos + float4(-fCurScale, -fCurScale, 0.f, 0.f);
   
    output[0].vPosition = mul(output[0].vPosition, g_matProj);
    output[1].vPosition = mul(output[1].vPosition, g_matProj);
    output[2].vPosition = mul(output[2].vPosition, g_matProj);
    output[3].vPosition = mul(output[3].vPosition, g_matProj);
    
    output[0].vUV = float2(0.f, 0.f);
    output[1].vUV = float2(1.f, 0.f);
    output[2].vUV = float2(1.f, 1.f);
    output[3].vUV = float2(0.f, 1.f);
        
    
    OutputStream.Append(output[0]);
    OutputStream.Append(output[1]);
    OutputStream.Append(output[2]);
    OutputStream.RestartStrip();
    
    OutputStream.Append(output[0]);
    OutputStream.Append(output[2]);
    OutputStream.Append(output[3]);
    OutputStream.RestartStrip();
}

float4 PS_MonsterHp(GS_HP_OUT _in) : SV_Target
{
  //  float fRatio = tData[_in.iInstID].m_fCurTime / tData[_in.iInstID].m_fLifeTime;
  //  float4 vCurColor = (g_vec4_1 - g_vec4_0) * fRatio + g_vec4_0;
   // return vCurColor * g_tex_0.Sample(g_sam_0, _in.vUV);
    
    
    float4 vColor = (float4) 0.f;

    if (tex_0)
        vColor = g_tex_0.Sample(g_sam_0, _in.vUV);
    else
        vColor = float4(1.f, 0.f, 1.f, 0.f);

    return vColor;
}




#endif