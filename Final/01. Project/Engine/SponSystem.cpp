#include "stdafx.h"
#include "SponSystem.h"
#include "MapMgr.h"
#include "MapRapper.h"
#include "TriggerNode.h"
#include "TriggerSpecial.h"
#include "TriggerMaker.h"
#include "Transform.h"
int CSponSystem::g_nPossibleInTower = 8;
CSponSystem::CSponSystem()
	: CScript((UINT)SCRIPT_TYPE::RESPON)
{
}


CSponSystem::~CSponSystem()
{
}

void CSponSystem::update()
{
	//m_vecRoundInfo[m_nCurRound].nCurMonsterCnt = m_pMapRapper->GetReadyObjCnt();
	if (!m_lStack.empty() && !m_bUpdate) {
		Trigger::Base::CBasis* pTemp = nullptr;
		Trigger::Result reValue = Trigger::Result::Fail;
		while (!m_lStack.empty()) {
			pTemp = m_lStack.back();
		//	m_lStack.pop_back();
			reValue = pTemp->Update(reValue);
			switch (reValue){
			case Trigger::Result::Wait:return;
			case Trigger::Result::Fail:
			case Trigger::Result::Success:
			case Trigger::Result::Evaluate:
				break;
			}
		}
	}
	else {
		m_pRoot->Update(Trigger::Result::Evaluate);
	}

	//	지금 레벨이 엔드인가?
	//	
	//	
	//	
	//	
	//	
	//	
	//	
	//	
	//	
	//	
	//	
	//if (m_nCurRound < m_nMaxRound) {
	//	tRoundInfo& r = m_vecRoundInfo[m_nCurRound];
	//	//	라운드가 종료카운트가 맞는가?
	//	if (r.nClearMonsterCnt < r.nCurMonsterCnt) {
	//		if (r.bIsIng) {
	//			//	진행중
	//			if (r.tIngEvent != nullptr) {
	//				//	ex )  사운드나 뭐 정보?
	//			}
	//
	//			
	//		}
	//		else {
	//			//	시작
	//			if (r.tStartEvent != nullptr) {
	//				//	라운드 시작 이벤트
	//			}
	//
	//		}
	//	}
	//	else {
	//		//	라운드 종료
	//		if (r.tEndEvent != nullptr) {
	//			//	라운드 종료 이벤트
	//		}
	//		++m_nCurRound;
	//	}
	//
	//}
	//else {
	//	//	레벨 종료
	//	
	//
	//}
}

void CSponSystem::AddRoundInfo(int _roundNum, int _responType, int _clearCnt, int _initMonsterCnt, int _maxCnt
	, int _nowMax, int _nowCur, float _sponDelay
)
{
	//int nMaxMonsterCnt = 0;		//	max		
	//int nCurMonsterCnt = 0;		//	cur				이거는 전체수에서 빼는거임
	////	개수임 (0 이면 아무것도 없다 최소 1마리!)
	//int nClearMonsterCnt = 0;	//	min
	//
	//int nResponType = 0;
	//int	nNowMaxMonster		 = 0;
	//int	nNowCurMonster		 = 0;
	//float fSponDelay			= 0.f;
	//float fCurSponDelay		= 0.f;
	//
	//std::vector< tMonsterInfo> monsterInfo;
	//std::vector< tMonsterInfo>::iterator itorMonster;
	//tRoundEvent* tStartEvent = nullptr;
	//tRoundEvent* tIngEvent = nullptr;
	//tRoundEvent* tEndEvent = nullptr;
	//bool bIsIng = false;

	m_vecRoundInfo[_roundNum].nMaxMonsterCnt = _maxCnt;
	m_vecRoundInfo[_roundNum].nCurMonsterCnt = _initMonsterCnt;
	m_vecRoundInfo[_roundNum].nClearMonsterCnt = _clearCnt;
	m_vecRoundInfo[_roundNum].nResponType = _responType;

	m_vecRoundInfo[_roundNum].nNowMaxMonster = _nowMax;
	m_vecRoundInfo[_roundNum].nNowCurMonster = _nowCur;
	m_vecRoundInfo[_roundNum].fSponDelay = _sponDelay;
	m_vecRoundInfo[_roundNum].fCurSponDelay = 0.0f;
}

void CSponSystem::RegistMonster(int _roundNum, tMonsterInfo _tMonsterInfo)
{
	m_vecRoundInfo[_roundNum].monsterInfo.push_back(_tMonsterInfo);
}

void CSponSystem::SetResponPos(const Vec3 & _pos)
{
	tResponer t = {};
	t.vPos = _pos;
	t.nCurIdx = 0;
	m_vecResponer.push_back(t);
}

void CSponSystem::SetMapIndex(int _idx)
{
	m_nMapIdx = _idx;
}

void CSponSystem::SetMapRapper(int _idx)
{
	m_pMapRapper = CMapMgr::GetInst()->GetMapRapper(_idx);
}

void CSponSystem::SponMonster(UINT _type,const Vec3 & _sponPos)
{
	CGameObject* pObj = m_pMapRapper->OnMonster(_type);
	pObj->SetActive(true);
	pObj->Transform()->SetLocalPos(_sponPos);
	
}
