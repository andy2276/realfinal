#include "stdafx.h"
#include "BossAiEvaluationInit.h"
#include "Transform.h"
#include "StatusComponent.h"
#include "Animator3D.h"
#include "MapMgr.h"
#include "BossAi.h"

nBT::TSK::CDT::bFloatScrCmpDest::bFloatScrCmpDest()
	:m_pDestValue(nullptr)
	, m_fScrValue(0.f)
{
}

nBT::TSK::CDT::bFloatScrCmpDest::~bFloatScrCmpDest()
{
}

nBT::bResult nBT::TSK::CDT::bFloatScrCmpDest::Evaluate(bResult _result)
{
	if (m_eState != nBT::bResult::Evaluation)return m_eState;
	if (m_pDestValue == nullptr) 
	{
		m_eState = nBT::bResult::Fail;
		return nBT::bResult::Fail;
	} 
	switch (m_eCmp)
	{
	case nBT::TSK::EnumValue::Cmp::Less:
		if (m_fScrValue < (*m_pDestValue))
		{
			m_eState = nBT::bResult::Success;
			return nBT::bResult::Success;
		}
		break;
	case nBT::TSK::EnumValue::Cmp::below:
		if (m_fScrValue <= (*m_pDestValue))
		{
			m_eState = nBT::bResult::Success; 
			return nBT::bResult::Success;
		}
		break;
	case nBT::TSK::EnumValue::Cmp::more:
		if (m_fScrValue > (*m_pDestValue))
		{ 
			m_eState = nBT::bResult::Success;
			return nBT::bResult::Success;
		
		}
		break;
	case nBT::TSK::EnumValue::Cmp::above:
		if (m_fScrValue >= (*m_pDestValue))
		{
			m_eState = nBT::bResult::Success;
			return nBT::bResult::Success;

		}
		break;
	case nBT::TSK::EnumValue::Cmp::same:
		if (FloatCmp(m_fScrValue, (*m_pDestValue), 0.01f))
		{
			m_eState = nBT::bResult::Success; 
			return nBT::bResult::Success;
		}
		break;
	}
	m_eState = nBT::bResult::Fail;
	return nBT::bResult::Fail;
}

nBT::TSK::ACT::bMovePos::bMovePos()
	:m_fNear(10.f)
{
}

nBT::TSK::ACT::bMovePos::~bMovePos()
{
}

nBT::bResult nBT::TSK::ACT::bMovePos::Evaluate(bResult _result)
{
	if (m_eState != nBT::bResult::Evaluation)return m_eState;
	CBossAi* boss = GetBossScript();
	if (boss == nullptr) 
	{
		m_eState = nBT::bResult::Fail;
		return nBT::bResult::Fail; 
	}
	if (boss->IsPossibleAnim((UINT)EC_MAPPING_ANIM::Run))
	{
		int mappingIdx = boss->GetMappingAnimIdx((UINT)EC_MAPPING_ANIM::Run);
		boss->ChangeMappingAnim((UINT)EC_MAPPING_ANIM::Run);
	}
	Vec3 mePos = boss->Transform()->GetLocalPos();
	Vec3 toDest = m_vDestPos - mePos;
	float dist = toDest.Length();

	if (m_fNear < dist)
	{
		Vec3 dir = toDest.Normalize();
		mePos += dir * GetBossScript()->GetStatus()->GetFloat(STATUS_FLOAT::MOVESPEED) * DT;
		mePos.y = CMapMgr::GetInst()->GetHeight(mePos);
		boss->Transform()->SetLocalPos(mePos);
		m_eState = nBT::bResult::Evaluation;
		return nBT::bResult::Running;
	}
	else
	{
		m_eState = nBT::bResult::Success;
		return nBT::bResult::Success;
	}
	return nBT::bResult();
}

nBT::TSK::ACT::bTurnAngle::bTurnAngle()
	:m_fOper(1.0f)
	,m_fCurAngle(0.0f)
	,m_fOrderAngle(0.0f)
	,m_fAngle(0.0f)
{
}

nBT::TSK::ACT::bTurnAngle::~bTurnAngle()
{
}

nBT::bResult nBT::TSK::ACT::bTurnAngle::Evaluate(bResult _result)
{
	if (m_eState != nBT::bResult::Evaluation)return m_eState;
	CBossAi* boss = GetBossScript();
	if (boss == nullptr)
	{
		m_eState = nBT::bResult::Fail;
		return nBT::bResult::Fail;
	}
	if (boss->IsPossibleAnim((UINT)EC_MAPPING_ANIM::Run))
	{
		int mappingIdx = boss->GetMappingAnimIdx((UINT)EC_MAPPING_ANIM::Run);
		boss->ChangeMappingAnim((UINT)EC_MAPPING_ANIM::Run);
	}
	Vec3 rot = boss->Transform()->GetLocalRot();
	
	float od = m_fAngle - m_fOrderAngle;
	if (m_fOrderAngle < m_fAngle)
	{
		m_fCurAngle += m_fOper * boss->GetStatus()->GetFloat(STATUS_FLOAT::ROTSPEED)*od * DT;
		m_fOrderAngle = abs(m_fCurAngle);
		rot.y += m_fCurAngle;
		boss->Transform()->SetLocalRot(rot);
		m_eState = nBT::bResult::Evaluation;
		return nBT::bResult::Running;
	}
	else
	{
		m_fOrderAngle = 0.0f;
		m_fCurAngle = 0.0f;
		m_eState = nBT::bResult::Success;
		return nBT::bResult::Success;
	}

}

nBT::TSK::ACT::bTurnDest::bTurnDest()
{
}

nBT::TSK::ACT::bTurnDest::~bTurnDest()
{
}

nBT::bResult nBT::TSK::ACT::bTurnDest::Evaluate(bResult _result)
{
	if (m_eState != nBT::bResult::Evaluation)return m_eState;
	CBossAi* boss = GetBossScript();
	if (boss == nullptr)
	{
		m_eState = nBT::bResult::Fail;
		return nBT::bResult::Fail;
	}

	Vec3 rot = boss->Transform()->GetLocalRot();
	Vec3 mePos = boss->Transform()->GetLocalPos();
	Vec3 meLook = boss->Transform()->GetLocalDir(DIR_TYPE::FRONT);
	Vec3 destLook = (m_vDestPos - mePos).Normalize();

	if (boss->IsPossibleAnim((UINT)EC_MAPPING_ANIM::Run))
	{
		int mappingIdx = boss->GetMappingAnimIdx((UINT)EC_MAPPING_ANIM::Run);
		boss->ChangeMappingAnim((UINT)EC_MAPPING_ANIM::Run);
	}

	float angle = meLook.Dot(destLook);
	if (abs(angle) <= 0.00001f)
	{
		m_eState = nBT::bResult::Success;
		return nBT::bResult::Success;
	}
	else
	{
		rot.y += angle * boss->GetStatus()->GetFloat(STATUS_FLOAT::ROTSPEED) * DT;
		boss->Transform()->SetLocalRot(rot);
		m_eState = nBT::bResult::Evaluation;
		return nBT::bResult::Running;
	}
}
