#pragma once
#include "Script.h"
enum class EC_SKELL_STATE {
	  Evaluate = 0
	, BackHeal = 1
	, Move = 2
	, Death = 3
	, Escape = 4
	, SKill1 = 5
	, FastRun = 6
	, BagicAttack = 7
	, Turn = 8
	, Patrol = 9
	, DestGo = 10
	, MoveBack = 11
	, Damaged = 12
	, Targeting = 13
};

class CSkelletonAI :
	public CScript
{
private:
	CGameObject*			m_pPlayer;
	Vec3					m_pInitPos;
	Vec3					m_vTargetPos;

	int						m_nHealCnt;					//	리셋시 필요
	EC_SKELL_STATE			m_state;					//	리셋시 필요

	//	Death
	float					m_fMaxDeathTime;
	float					m_fCurDeathTime;			//	리셋시 0,0

	//	Skill
	float					m_fMaxSkillTime;
	float					m_fCurSkillTime;			//	리셋시 0.0f
	bool					m_fDamaged;

	float					m_fDamagedCur;
	float					m_fDamagedMax;


	//	BagicAttack
	bool					m_bBagicAttackOn;
	float					m_fBagicAttackTiming;		//	공격시간(애니메이션 시간이랑 동일)
	float					m_fBagicAttackMaxTime;
	float					m_fBagicAttackHitTime;		//	피격가능한 시간
	float					m_fBagicAttackRange;		//	공격 사거리	
	float					m_fBagicAttackDamage;		//	공격 데미지
	float					m_fBagicAttackAngle;		//	공격 사이각

	//	Patrol
	float					m_fPatrolDistNow;
	float					m_fPatrolDistMax;


	bool					m_bFastRunBuff;
	float					m_fFastRunTime;
	float					m_fFastRunMax;
	float					m_fFastRunSpeed;
	float					m_fRunOper;

	float					m_fTic;
	float					m_fToc;
	float					m_fTicMax;

	string					m_strEvalue;
public:
	CSkelletonAI();
	virtual ~CSkelletonAI();

	CLONE(CSkelletonAI);

	virtual void update();

	void SetPlayer(CGameObject* _pPlayer) { m_pPlayer = _pPlayer; }
	void Reset(const Vec3& _pos, const Vec3& _rot);

	const string& GetState() 
	{
		switch (m_state)
		{
		case EC_SKELL_STATE::Evaluate:m_strEvalue = "Evaluate";
			break;
		case EC_SKELL_STATE::BackHeal:m_strEvalue = "BackHeal";
			break;
		case EC_SKELL_STATE::Move:m_strEvalue = "Move";
			break;
		case EC_SKELL_STATE::Death:m_strEvalue = "Death";
			break;
		case EC_SKELL_STATE::Escape:m_strEvalue = "Escape";
			break;
		case EC_SKELL_STATE::SKill1:m_strEvalue = "SKill1";
			break;
		case EC_SKELL_STATE::FastRun:m_strEvalue = "FastRun";
			break;
		case EC_SKELL_STATE::BagicAttack:m_strEvalue = "BagicAttack";
			break;
		case EC_SKELL_STATE::Turn:m_strEvalue = "Turn";
			break;
		case EC_SKELL_STATE::Patrol:m_strEvalue = "Patrol";
			break;
		case EC_SKELL_STATE::DestGo:m_strEvalue = "DestGo";
			break;
		case EC_SKELL_STATE::MoveBack:m_strEvalue = "MoveBack";
			break;
		case EC_SKELL_STATE::Damaged:m_strEvalue = "Damaged";
			break;
		case EC_SKELL_STATE::Targeting:m_strEvalue = "Targeting";
			break;
		default:
			break;
		}
		return m_strEvalue;
	}
	virtual void OnCollision(CCollider2D * _pOther);
};

