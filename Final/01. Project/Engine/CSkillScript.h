#pragma once
#include "Script.h"
class CSkillScript :
	public CScript
{
private: 
	float m_;


	int m_attackRadius;//메테오 범위 
	float m_attackStart;//메테오 출발 지점 
	Vec3 m_ArrivePos;//메테오 도착 지점 

	float m_attackHypotenuseLenght;//빗변 길이 


	float m_curTime;
	float m_coolTime;
	float m_remainCoolTime;
	bool isCanShoot;

public:
	virtual void update();

	CSkillScript();
	virtual ~CSkillScript();
	CLONE(CSkillScript);

	void init(float cooltime=0);
	
};

