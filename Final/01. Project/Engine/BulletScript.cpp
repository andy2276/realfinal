#include "stdafx.h"
#include "BulletScript.h"

#include"StatusComponent.h"
#include"ParticleSystem.h"
#include"SceneMgr.h"

#include"CSkillScript.h"
#include "MapMgr.h"
#include "EffectScript.h"
CBulletScript::CBulletScript()	
	: CScript((UINT)SCRIPT_TYPE::BULLETSCRIPT)
	, m_curTime(0)
	, m_coolTime(5)
	, m_remainCoolTime(0)
	, m_isCanShoot(false)
	, m_aliveCount(0)
	
{

}

CBulletScript::CBulletScript(UINT etype) : CScript((UINT)SCRIPT_TYPE::BULLETSCRIPT)
	, m_skillType(etype)
	, m_firebulletUI(nullptr)
	, m_bHasFloor(false)
	, m_LifeTime(0)
	, m_curTime(0)
	, m_coolTime(5)
	, m_remainCoolTime(0)
	, m_isCanShoot(false)
	, m_aliveCount(0)
	, m_startPos(0,0,0)
{
	switch (etype)
	{
	case (UINT)PLAYER_SKILL_TYPE::SKILL_FIREBALL:
	{	
		m_flameEffect = new CGameObject;
		m_flameEffect->SetName(L"FIRE BALL POWER UI");
		m_flameEffect->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
		m_flameEffect->AddComponent(new CTransform);
		m_flameEffect->AddComponent(new CMeshRender);
		m_flameEffect->AddComponent(new CEffectScript);

	
		m_firebulletUI = new CGameObject;

	m_firebulletUI->SetName(L"FIRE BALL POWER UI");
	m_firebulletUI->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	m_firebulletUI->AddComponent(new CTransform);
	m_firebulletUI->AddComponent(new CMeshRender);
	m_firebulletUI->Transform()->setPhysics(false);


	m_fireBulletUI_scale = Vec3(10, 0, 1.f);
	m_fireBulletUI_pos = Vec3(-20, 0, 1.f);

	m_firebulletUI->Transform()->SetLocalPos(m_fireBulletUI_pos);
	m_firebulletUI->Transform()->SetLocalScale(m_fireBulletUI_scale);


	m_firebulletUI->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	Ptr<CMaterial> pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
	float i = 0;
//	pMtrl->SetData(SHADER_PARAM::FLOAT_0, &i);
	m_firebulletUI->MeshRender()->SetMaterial(pMtrl->Clone());

	CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_firebulletUI);
	}
		break;
	case (UINT)PLAYER_SKILL_TYPE::SKILL_METEOR:
	{
		m_boomEffect = new CGameObject;
		m_boomEffect->SetName(L"boomeffect");
		m_boomEffect->AddComponent(new CTransform);
		m_boomEffect->AddComponent(new CMeshRender);
		m_boomEffect->Transform()->SetLocalScale(Vec3(2.f, 2.f, 2.f));
		m_boomEffect->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
		//	pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		m_boomEffect->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));
		//	m_effect->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"FlameEffectEz4Mtrl"));

		Ptr<CTexture> explosion = CResMgr::GetInst()->Load<CTexture>(L"explosion", L"Texture\\explosion.jpg");
		m_boomEffect->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, explosion.GetPointer());


	}
	break;
	default:
		break;
	}

	switch (m_skillType)
	{
	case (UINT)PLAYER_SKILL_TYPE::SKILL_FIREBALL:
		m_damage = 10;

		pSound_Shoot = CResMgr::GetInst()->Load<CSound>(L"26_jumping", L"Sound\\26_jumping.wav");
		break;
	case (UINT)PLAYER_SKILL_TYPE::SKILL_METEOR:
		m_damage = 30;
		m_LifeTime = 10; 

		break;
	case (UINT)PLAYER_SKILL_TYPE::SKILL_ICEARROW:
		m_damage = 10;
		m_LifeTime = 5;

		pSound_Shoot = CResMgr::GetInst()->Load<CSound>(L"33_whip", L"Sound\\33_whip.wav");
		break;
	case (UINT)PLAYER_SKILL_TYPE::SKILL_SNOWSTORM:
		m_damage = 100;
		m_LifeTime = 10;
		break;
	case (UINT)PLAYER_SKILL_TYPE::SKILL_GOLD:
		m_damage = 0;
		m_LifeTime = 10;
	default:
		break;
	}


	pSound_Collider = CResMgr::GetInst()->Load<CSound>(L"34_punch", L"Sound\\34_punch.mp3");
}

CBulletScript::~CBulletScript() 
{
}

void CBulletScript::shoot()
{
	// 발사하라는 함수가 들어오면 시작 위치와
	// 도착 위치를 결정해준다. 
	// 생존시간도 결정해준다. 


	switch (m_skillType)
	{
	case (UINT)PLAYER_SKILL_TYPE::SKILL_FIREBALL:
		m_flameEffect = CEffectScript::CreateFireBurn(Vec3(0,0,0), 5.f, 6.f, 20.f, 15.f, 15.f);
		m_flameEffect->SetActive(false);
			break;
	case (UINT)PLAYER_SKILL_TYPE::SKILL_METEOR:
	{
		m_boomEffect->SetActive(false);
		int random = rand() % 25;
//		if(m_idx)

		m_startPos.x = m_Pos.x +30*cosf(m_idx * 10);
		m_startPos.y = m_Pos.y-25 + random;
		m_startPos.z = m_Pos.z + 30*sinf(m_idx * 10);

	

//	m_startPos = m_Pos;
//	m_startPos.x = m_Pos.x + random;
//	m_startPos.y = 50+ random;
//	m_startPos.z = m_Pos.z;

//	m_arrivePos.x = m_Pos.x + random;
//	m_arrivePos.y = 0;
//	m_arrivePos.z = m_Pos.z;
//
	Transform()->SetLocalPos(m_startPos);
	m_isCanShoot = true;
	}
			break;
	case (UINT)PLAYER_SKILL_TYPE::SKILL_ICEARROW:
	{

		pSound_Shoot->Play(1,1, true);
		m_Dir =  CMapMgr::GetInst()->GetPlayerObj()->Transform()->GetLocalDir(DIR_TYPE::FRONT);

		m_isCanShoot = true;
	}
			break;
	case (UINT)PLAYER_SKILL_TYPE::SKILL_SNOWSTORM:
	{
		m_Dir = -1 * CMapMgr::GetInst()->GetPlayerObj()->Transform()->GetLocalDir(DIR_TYPE::RIGHT);
		m_DirUP = -1 * CMapMgr::GetInst()->GetPlayerObj()->Transform()->GetLocalDir(DIR_TYPE::UP);
		m_DirLook = CMapMgr::GetInst()->GetPlayerObj()->Transform()->GetLocalDir(DIR_TYPE::FRONT);

		Vec3 rot = CMapMgr::GetInst()->GetPlayerObj()->Transform()->GetLocalRot();
		rot.y += XM_ANGLE * 135.f;
		rot.x += XM_ANGLE * 140.f;

		GetObj()->Transform()->SetLocalOffset(rot, Vec3((0.f, 0.f, 0.f)));
//		GetObj()->Transform()->SetLocalOffset(Vec3((-1 * m_Dir) + (m_Dir.x + XM_ANGLE * 130.f, 0.f, 0.f)), Vec3((0.f, 0.f, 0.f)));



		m_Radius = 50;
		float random; //랜덤 시작 지점 x좌표 결정하기 
		//시작지점의 최대는 에임지점 + 2* 반지름 
		// ' 2* 반지름 ' 이므로 랜덤값은 반지름보다 크지 않으면 된다. 
		random = rand() % ((int)m_Radius * 2) + 1;
	
		if (m_idx >= 0 && m_idx < 6)
		{
			m_startPos.x = m_Pos.x + 10 * m_idx * cosf(m_idx * 10);//+ m_Dir.x *m_Radius*2;
			m_startPos.y = m_Pos.y + random;
			m_startPos.z = m_Pos.z + 10 * m_idx * sinf(m_idx * 10);
		}
		if (m_idx >= 6 && m_idx < 12)
		{
			m_startPos.x = m_Pos.x + 30 * m_idx * cosf(m_idx * 10);//+ m_Dir.x *m_Radius*2;
			m_startPos.y = m_Pos.y + random;
			m_startPos.z = m_Pos.z + 30 * m_idx * sinf(m_idx * 10);
		}		if (m_idx >= 12 && m_idx < 19)
		{
			m_startPos.x = m_Pos.x + 60 * m_idx * cosf(m_idx * 10);//+ m_Dir.x *m_Radius*2;
			m_startPos.y = m_Pos.y + random;
			m_startPos.z = m_Pos.z + 60 * m_idx * sinf(m_idx * 10);
		}
		m_startPos -= m_Dir *m_Radius;
	//	m_startPos.y = random + 50;
	//	m_startPos.x = m_Pos.x + random; // 시작지점을 (최대값이 존재하는)랜덤으로 설정한뒤
	////	m_startPos.y = m_Pos.y;
	//	m_startPos.z = m_Pos.z;


	//	m_arrivePos.x = m_startPos.x - m_Radius;// 도착지점의 값은 시작지점 - 반지름 
	//	m_arrivePos.y = 0;
	//	m_arrivePos.z = m_Pos.z;
	//	
	//	m_Pos.x = m_startPos.x;
		

		Transform()->SetLocalPos(m_startPos);
		m_isCanShoot = true;
	}


			break;

	case (UINT)PLAYER_SKILL_TYPE::SKILL_GOLD:

		m_isCanShoot = true;
		
			break;
	case (UINT)PLAYER_SKILL_TYPE::END:
			break;
	default:
		break;
	}
}

void CBulletScript::init(UINT etype)
{
	m_skillType = etype;
	switch (m_skillType)
	{
	case (UINT)PLAYER_SKILL_TYPE::SKILL_FIREBALL:
		

		break;

	default:
		break;
	}


	// 눈보라 스킬 
	// 필요 오브젝트 : 파티클 +얼음화살 / 장판 

	
}



void CBulletScript::update()
{
	// 발사 규칙
	// 왼쪽버튼이 눌렸을때의 시작 시간을 기록한다 
	// 왼쪽버튼이 놓아질때까지 계속해서 게이지의 크기와 중력의 크기를 증가시켜준다. 
	// 왼쪽버튼이 놓아짐과 동시에 발사된다 

	if (m_isCanShoot)
	{
		if (m_skillType == (UINT)PLAYER_SKILL_TYPE::SKILL_SNOWSTORM 
			|| m_skillType == (UINT)PLAYER_SKILL_TYPE::SKILL_METEOR 
			|| m_skillType == (UINT)PLAYER_SKILL_TYPE::SKILL_GOLD 
			|| m_skillType == (UINT)PLAYER_SKILL_TYPE::SKILL_ICEARROW)
		{
			m_curTime += DT;
			if (m_LifeTime < m_curTime)
			{
				GetObj()->SetActive(false);
				m_curTime = 0;
				m_isCanShoot = false;
			}
		}
	}


	float famount = 200;
	
	switch (m_skillType)
	{
	case (UINT)PLAYER_SKILL_TYPE::SKILL_FIREBALL:
	{
		if (m_firebulletUI != nullptr)
		{
			//아직 발사되지 않았을경우에만 유아이를 띄운다. 
			//발사가 되고 나서는 다시 오른쪽마우스버튼 입력을 받지 않는다. 
			if (!m_isCanShoot)
			{
				if (KEY_HOLD(KEY_TYPE::KEY_RBTN))
				{
					m_power += 2.f;
					if (m_power >= 50)
						m_power = 0; //최대치 넘으면 다시 0 으로온다

					m_fireBulletUI_scale.y = m_power;
					m_fireBulletUI_pos.y = -20 + m_fireBulletUI_scale.y / 2;

					m_firebulletUI->Transform()->SetLocalPos(m_fireBulletUI_pos);
					m_firebulletUI->Transform()->SetLocalScale(m_fireBulletUI_scale);
					//우클릭 동안 따라다니도록
					m_Pos = CMapMgr::GetInst()->GetPlayerObj()->Transform()->GetLocalPos();
					m_Pos.y += 10;
					Vec3 right = CMapMgr::GetInst()->GetPlayerObj()->Transform()->GetLocalDir(DIR_TYPE::RIGHT);
					m_Pos += right*4;

					Transform()->SetLocalPos(m_Pos);
				}
				else if (KEY_AWAY((KEY_TYPE::KEY_RBTN))) { //오른쪽마우스에서 손을 떼면 
		
				//	m_power *= 3;
					Transform()->setVelocity(0, m_power, 0);
					//m_Pos = Transform()->GetLocalPos(); //1. 플레이어의 현재 포지션을 받아서 넣어줘야한다. 
					Transform()->setSkillPhysics(true); //true값이 움직일수 있게 하는것. 

					m_power = 0;
					m_fireBulletUI_scale.y = 0;
					m_fireBulletUI_pos.y = -20;
					m_firebulletUI->Transform()->SetLocalPos(m_fireBulletUI_pos);
					m_firebulletUI->Transform()->SetLocalScale(m_fireBulletUI_scale);
					m_isCanShoot = true;
					m_firebulletUI->SetActive(false);

					Vec3 rot = CMapMgr::GetInst()->GetPlayerObj()->Transform()->GetLocalRot();
					m_Pos = CMapMgr::GetInst()->GetPlayerObj()->Transform()->GetLocalPos();
					m_Pos.y += 10; 
				//	Transform()->SetLocalRot(rot);
					Transform()->SetLocalPos(m_Pos);
					m_Dir = CMapMgr::GetInst()->GetPlayerObj()->Transform()->GetLocalDir(DIR_TYPE::FRONT);

					pSound_Shoot->Play(1,1, true);

					m_flameEffect->SetParent(GetObj());
					m_flameEffect->GetScript<CEffectScript>()->SetScales(5.f, 100.f, 20.f, 0.f, 15.f);
					m_flameEffect->Transform()->SetLocalPos(m_Pos);
					m_flameEffect->SetActive(true);

				}

			}
		}
		if(m_isCanShoot)
		{
			Vec3 pos = Transform()->GetLocalPos();
		
			pos += m_Dir * DT * 200;
			Transform()->SetLocalPos(pos);

		}

		if (Transform()->GetLocalPos().y <= 0)
		{
			GetObj()->SetActive(false);
			m_flameEffect->SetActive(false);
		}

	}

	break;
	case (UINT)PLAYER_SKILL_TYPE::SKILL_ICEARROW:
	{
		if (m_isCanShoot)
		{//처음 출발 위치보다 z값이 length보다 멀어졌을 경우에 삭제된다 	
			if (GetObj()->IsActive()) //살아있을경우 실시간으로 위치를 증가시켜준다. 
			{
				//m_Dir = Transform()->GetLocalDir(DIR_TYPE::FRONT);
				m_Pos = Transform()->GetLocalPos(); //실시간 현재 위치를 받아오고 
				Vec3 vSetPos = m_Pos + (m_Dir * DT* famount);
				Transform()->SetLocalPos(vSetPos);

				if (vSetPos.y <= 0)
					GetObj()->SetActive(false);
			}
		}

	}
	break;

	case (UINT)PLAYER_SKILL_TYPE::SKILL_SNOWSTORM:
	{
		if (!m_bHasFloor)//장판일때는 위치이동을 시켜줄 필요가 없으므로 제외시키는것임. 
		{//눈보라 매커니즘
		// 1. 1번을 누르면 마우스위치로 장판이 따라다닌다. 
		// 2. 마우스 왼쪽버튼을 누르면 그 위치로 장판이 고정되고, 장판의 위치를 넘겨준다.
		// 3. 장판의 위치로 생성되는 얼음화살들의 위치를 정한다. 
		// 4. 얼음화살의 위치를 변화시켜준다. 
		// 5. 쿨타임이 지나면 사라진다. 
			if (m_isCanShoot)//총알발사가 가능하다는 명령이 들어오면 ,
			{

				Vec3 tempPos = Transform()->GetLocalPos();
				
				tempPos += m_Dir * DT * 50;
				tempPos.y -= 5;
				//tempPos += m_DirUP * DT * 50;

			//	tempPos.x -= m_Dir * DT * 50;
			//	tempPos.y -= DT * 100;
			

				Transform()->SetLocalPos(tempPos);
				if (tempPos.y <= 0)
				{
					Transform()->SetLocalPos(m_startPos);
				}
			}
		}
		
	}break;
	case (UINT)PLAYER_SKILL_TYPE::SKILL_METEOR:
	{
		if (!m_bHasFloor)
		{
			if (m_isCanShoot)//총알발사가 가능하다는 명령이 들어오면 ,
			{
				Vec3 tempPos = Transform()->GetLocalPos();
				tempPos.y -= DT * 100;
				Transform()->SetLocalPos(tempPos);
				if (tempPos.y <= 0)
				{
					Transform()->SetLocalPos(m_startPos);

					CreateObject(m_boomEffect, L"notInfluenceAnything");
					m_boomEffect->SetActive(true);
					m_boomEffect->Transform()->SetLocalPos(tempPos);
				}

			}
			if (m_boomEffect->IsActive())
			{
					Vec3 m_effectScale = m_boomEffect->Transform()-> GetLocalScale();
					m_effectScale.x += 1.0;
					m_effectScale.y += 1.0;
					m_effectScale.z += 1.0;

					m_boomEffect->Transform()->SetLocalScale(m_effectScale);
					if (m_effectScale.x >=10)
					{
						m_boomEffect->SetActive(false);
						m_boomEffect->Transform()->SetLocalScale(Vec3(2,2,2));
					}
			}
		}
	}
		break;
	default:
		break;
	}



	//생존시간일 경우에 변화될값들을 설정해준다. 위치 or 뭐 등등 

	
	//얼음화살이 disable



		//업데이트가 될때마다 현재 시간을 누적해주고, 
		//m_curTime += DT;
		////ex) 쿨타임 10초가 지나면 스킬을 쓸수 있다. 
		//if (m_coolTime - m_curTime < 0)
		//{
		//	isCanShoot = true;
		//	m_curTime = 0;
		//}


		//업데이트가 될때마다 현재 시간을 누적해주고, 
//		m_curTime += DT;
//		//ex) 쿨타임 N초가 지나면 유성이 다시 살아나서 떨어지게 한다.  
//		if (m_coolTime - m_curTime < 0)
//		{
//			//if (!m_Bullets[m_aliveCount]->IsActive())
//		
//				m_Bullets[m_aliveCount]->enable();
//				m_Particles[m_aliveCount]->enable();
//
//				
//				m_curTime = 0;
//
//			//	std::wcout << "현재 살아난 순서" << m_aliveCount << std::endl;
//
//
//		
//			m_aliveCount++;
//			if (m_aliveCount >= bulletCount)
//			{
//				m_aliveCount = 0;
//			}
//		}
//	//	std::wcout << "현재 카운트 수 "  << m_aliveCount << std::endl;
////		std::wcout << "============================== " <<  std::endl;
////
//		isCanShoot = true;
//
//
//		if (isCanShoot)
//		{	//도착지점까지 x축 이동해주기 
//		//////////////////////////////
//			for (int i = 0; i < bulletCount; ++i)
//			{
//		//		std::wcout << "nope" << std::endl;
//
//				if (m_Bullets[i]->IsActive())
//				{
//					Vec3 vPos = m_Bullets[i]->Transform()->GetLocalPos();//현재 위치 받아와서 
//
//					float speed = 200.f;
//					if (vPos.x > m_ArrivePos[i].x)
//					{
//						vPos.x -= speed * DT;
//						vPos.y -= speed * 2 * DT;
//					}
//					else {
//						//vPos.x = m_ArrivePos.x;
//					}
//
//
//					//vPos.y -= 10;
//
//					//y값이 0이 되면 사라지게 
//					if (vPos.y <= 0)
//					{
//					//	std::wcout << "액티브" << std::endl;
//
//						isCanShoot = false;
//						m_Bullets[i]->disable();
//						m_Particles[i]->disable();
//						vPos.x = m_attackStart[i];
//						vPos.y = m_ArrivePos[i].y;// m_attackRadius * 2;
//					}
//
//					m_Bullets[i]->Transform()->SetLocalPos(vPos);
//					vPos.x += 10;
//					vPos.y += 10;
//					m_Particles[i]->Transform()->SetLocalPos(vPos);
//				//	std::cout << i <<"_____"<< vPos.x << " , " << vPos.z << " / " << m_ArrivePos[i].x << std::endl;
//				}
//			}
//		}
//

}

void CBulletScript::OnCollisionEnter(CCollider2D * _pOther)
{

//	_pOther->GetObj()->StatusComponent()->DamageStack(_pOther->GetObj()->Transform()->GetLocalDir(DIR_TYPE::FRONT), 10.f);
//	GetObj()->SetActive(false);

	//if (L"Monster" == _pOther->GetObj()->GetName())
	//{
	//	//_pOther->GetObj()->StatusComponent()->setHpDown(10);
	//
	//	
	//}

	if (m_skillType == (UINT)PLAYER_SKILL_TYPE::SKILL_ICEARROW || m_skillType == (UINT)PLAYER_SKILL_TYPE::SKILL_FIREBALL)
	{
		pSound_Collider->Play(1,1, true);
		GetObj()->SetActive(false);
	}
	_pOther->GetObj()->StatusComponent()->DamageStack(GetObj()->GetID(),Transform()->GetLocalDir(DIR_TYPE::FRONT), 50.f);
	
}

void CBulletScript::awake()
{


}
