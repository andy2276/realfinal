#pragma once
#include "Script.h"
enum class EC_N_MAP_STATE
{
	Evaluate		=	0
	,Sponning		=	1
	,RoundClear		=	2
	,LevelClear		=	3
	,MapMove		=	4
	,Start			=	5
	,Stop			=	6
	





};
struct tNeoRoundInfo
{
	//	라운드 이름
	//	라운드 몬스터 수
	int	nAllMonster;	//	==	클리어 몬스터수

	//	라운드 클리어 몬스터수
	//	라운드 보스몬스터
	//	
	//	
	//	
	//	
	//	
	//	
	//	
	//	
	//	
	//	
	//	
	//	
};
enum class EC_N_OBJ_TYPE
{



};


class CGameObject;

class CNeoMapScript :
	public CScript
{
private:
	EC_N_MAP_STATE m_eState;

	Vec3			m_vMapStartPos;



	CGameObject* m_pPlayer;

	vector<tNeoRoundInfo>	m_vecRoundInfo;

	vector<CGameObject*> m_vecAllGameObj;//	
	vector<CGameObject*> m_vecMonsterObj;
	vector<CGameObject*> m_vecEnvirObj;
	

public:
	CNeoMapScript();
	virtual ~CNeoMapScript();

public:
	CLONE(CNeoMapScript);
	//	밖으로 나가면 안되고
	//	스폰 위치가 필요하고
	//	맵이동 포탈이 필요함
	//	
	//	
	//	
	//	
	//	
	


	void SetRoundInfo(int _mimic,int _Skell,int _Stone,bool _IsBoss = false,int _bossType = -1);
	void SetMapPortal(const Vec3& _pos);
	void SetSponnerPoint();




	virtual void update();
};

