#include "stdafx.h"
#include "SkillPlatform.h"

#include "MapMgr.h"
#include "ResMgr.h"
#include "SceneMgr.h"

#include "SkillElementDir.h"
#include "SkillEvent.h"

CSkillPlatform::CSkillPlatform()
	:CScript(UINT(SCRIPT_TYPE::MONSTERSKILL))
{
}


CSkillPlatform::~CSkillPlatform()
{
}

void CSkillPlatform::InitSkillBullet(const tSkillArch & _arch)
{
	m_tSkillInfo.A_nBulletObjCnt = _arch.A_nBulletObjCnt;
	m_tSkillInfo.B_nShootBulletCnt = _arch.B_nShootBulletCnt;
	m_tSkillInfo.Ca_wstrMeshPath = _arch.Ca_wstrMeshPath;
	m_tSkillInfo.C_bBulletMesh = _arch.C_bBulletMesh;
	m_tSkillInfo.Da_nMapMgrMeshIdx = _arch.Da_nMapMgrMeshIdx;
	m_tSkillInfo.D_bMapMgrMesh = _arch.D_bMapMgrMesh;
	m_tSkillInfo.E_tSkillElementValue = _arch.E_tSkillElementValue;
	m_tSkillInfo.F_nTypes = _arch.F_nTypes;
	m_tSkillInfo.G_vOffsetScale = _arch.G_vOffsetScale;
	m_tSkillInfo.H_wstrLayer = _arch.H_wstrLayer;

}

void CSkillPlatform::InitSkillBullet(
	int A_nBulletObjCnt, int B_nShootBulletCnt, 
	bool C_bBulletMesh, wstring Ca_wstrMeshPath, 
	bool D_bMapMgrMesh, int Da_nMapMgrMeshIdx, 
	tSkillElementValue::tPosValues		_posValue, 
	tSkillElementValue::tEndtypes		_endType, 
	tSkillElementValue::tMoveValues		 _movValue,
	tSkillElementValue::tSinCosAngles	_sinCos,
	tSkillElementValue::tEventTimes		_eventTime,
	tSkillElementValue::tRepeateValues	_respon, 
	tSkillElementValue::tOthers			_other, 
	UINT F_nTypes, Vec3 G_vOffsetScale, wstring H_wstrLayer
)
{
	m_tSkillInfo.A_nBulletObjCnt = A_nBulletObjCnt;
	
	m_tSkillInfo.B_nShootBulletCnt =B_nShootBulletCnt;
	m_tSkillInfo.C_bBulletMesh =C_bBulletMesh;
	m_tSkillInfo.Ca_wstrMeshPath =Ca_wstrMeshPath;
	m_tSkillInfo.D_bMapMgrMesh =D_bMapMgrMesh;
	m_tSkillInfo.Da_nMapMgrMeshIdx =Da_nMapMgrMeshIdx;

	m_tSkillInfo.E_tSkillElementValue.PosValues = _posValue;
	m_tSkillInfo.E_tSkillElementValue.Endtypes = _endType;
	m_tSkillInfo.E_tSkillElementValue.MoveValues = _movValue;
	m_tSkillInfo.E_tSkillElementValue.SinCosAngles = _sinCos;
	m_tSkillInfo.E_tSkillElementValue.EventTimes = _eventTime;
	m_tSkillInfo.E_tSkillElementValue.RepeateValues = _respon;
	m_tSkillInfo.E_tSkillElementValue.Others = _other;

	m_tSkillInfo.F_nTypes = F_nTypes;
	m_tSkillInfo.G_vOffsetScale = G_vOffsetScale;
	m_tSkillInfo.H_wstrLayer = H_wstrLayer;
}

void CSkillPlatform::InitSkillEvent(int _idx, tSkillEventValue & _event)
{
	m_tSkillEventInfo[_idx] = _event;
}

void CSkillPlatform::InitSkillEvent(
	int					_idx, 
	const Vec3 &		a_vStartPos, 
	const Vec3 &		a_vStratRot, 
	const Vec3 &		b_vScaleMax,
	const Vec3 &		b_vScaleCur,
	const Vec3 &		b_vScaleMin, 
	const Vec3 &		b_vScaleInit, 
	float				b_fScaleAcc, 
	CGameObject *		c_pTarget, 
	const float			c_fDelayDist, 
	const Vec3 &		d_vRotSpeed, 
	int					eA_nTexIdx, 
	UINT				eB_nType,
	const Vec3 &		eC_vInitPos, 
	const Vec3 &		eD_vInitRot, 
	const Vec3 &		eE_vInitRotSpeed
)
{
	m_tSkillEventInfo[_idx].a_vStartPos = a_vStartPos;
	m_tSkillEventInfo[_idx].a_vStratRot = a_vStratRot;
	m_tSkillEventInfo[_idx].b_vScaleMax = b_vScaleMax;
	m_tSkillEventInfo[_idx].b_vScaleCur = b_vScaleCur;
	m_tSkillEventInfo[_idx].b_vScaleMin = b_vScaleMin;
	m_tSkillEventInfo[_idx].b_vScaleInit = b_vScaleInit;
	m_tSkillEventInfo[_idx].b_fScaleAcc = b_fScaleAcc;
	m_tSkillEventInfo[_idx].c_pTarget = c_pTarget;
	m_tSkillEventInfo[_idx].c_fDelayDist = c_fDelayDist;
	m_tSkillEventInfo[_idx].d_vRotSpeed = d_vRotSpeed;
	m_tSkillEventInfo[_idx].eA_nTexIdx = eA_nTexIdx;
	m_tSkillEventInfo[_idx].eB_nType = eB_nType;
	m_tSkillEventInfo[_idx].eC_vInitPos = eC_vInitPos;
	m_tSkillEventInfo[_idx].eD_vInitRot = eD_vInitRot;
	m_tSkillEventInfo[_idx].eE_vInitRotSpeed = eE_vInitRotSpeed;
}

void CSkillPlatform::CreateSkillBullet(int _cnt)
{
	if (_cnt < 0)return;
	Ptr<CMeshData> ptr;
	if (m_tSkillInfo.C_bBulletMesh)
	{
		ptr = CResMgr::GetInst()->LoadFBX(m_tSkillInfo.Ca_wstrMeshPath);
		CMapMgr::GetInst()->setBulletData(ptr);
	}
	else if (m_tSkillInfo.D_bMapMgrMesh)
	{
		if (m_tSkillInfo.D_bMapMgrFireBall)
		{

		}
		else
		{
			ptr = CMapMgr::GetInst()->GetBulletData(m_tSkillInfo.Da_nMapMgrMeshIdx);
			
		}
	}
	else
	{
		return;
	}
	CGameObject* pObj = nullptr;
	CSkillElementDir* element = nullptr;
	//				한번에 발사되는 개수
	if (ptr.GetPointer() == nullptr)return;
	for (int i = 0; i < _cnt;++i)
	{
		pObj = ptr->Instantiate();
		if ((UINT)EC_MONSTER_BULLET::DarkBall == m_tSkillInfo.Da_nMapMgrMeshIdx)
		{
			pObj->Transform()->SetLocalScale(Vec3(0.25f, 0.25f, 0.25f));
		}
		pObj->AddComponent(new CCollider2D);
		pObj->Collider2D()->SetOffsetScale(m_tSkillInfo.G_vOffsetScale);
		element = new CSkillElementDir;

		pObj->AddComponent(element);
		element->SetTypes(m_tSkillInfo.F_nTypes);
		element->SetValues(m_tSkillInfo.E_tSkillElementValue);

		m_vecSkillObj.push_back(pObj);
		m_vecBulletScript.push_back(element);

		CSceneMgr::GetInst()->GetCurScene()->AddGameObject(m_tSkillInfo.H_wstrLayer, pObj, false);
	}
}

void CSkillPlatform::CreateSkillEvent(int _timing, tSkillEventValue & _tValue)
{
}




void CSkillPlatform::Shoot(const Vec3& _pos, const Vec3 _rot, const Vec3& _dir,int _option)
{
	static Vec3 goPos;
	goPos = _pos;
	CGameObject* pEvent = nullptr;
	int onceCnt = m_tSkillInfo.B_nShootBulletCnt;
	for (int i = 0; i < m_vecSkillObj.size(); ++i)
	{
		if (onceCnt <= 0)return;
		if (m_vecSkillObj[i] != nullptr && !m_vecSkillObj[i]->IsActive())
		{
			if (m_tSkillInfo.E_tSkillElementValue.MoveValues.bTargetPlayer)
			{
				goPos = CMapMgr::GetInst()->GetPlayerObj()->Transform()->GetLocalPos();
			}
			m_vecBulletScript[i]->InitShoot(goPos, _rot);
			if (m_tSkillInfo.E_tSkillElementValue.MoveValues.bDonInitMove)
			{
				m_vecBulletScript[i]->SetMoveDir(Transform()->GetLocalDir(m_tSkillInfo.E_tSkillElementValue.MoveValues.eDirType));
			}
			
			m_vecBulletScript[i]->SetAlive(true);
			m_vecSkillObj[i]->SetActive(true);

			if ((UINT)EC_SKILL_ELEMENT::EventType_StartEffect & m_vecBulletScript[i]->GetTypes())
			{
				m_tSkillEventInfo[0].a_vStartPos = goPos + m_tSkillEventInfo[0].eC_vInitPos;
				m_tSkillEventInfo[0].a_vStratRot = m_tSkillEventInfo[0].eD_vInitRot;
				m_tSkillEventInfo[0].d_vRotSpeed = m_tSkillEventInfo[0].eE_vInitRotSpeed;
				m_tSkillEventInfo[0].c_pTarget = m_vecSkillObj[i];

				if (m_vecBulletScript[i]->HaveStartEffect())
				{
					m_vecBulletScript[i]->ResetStartEffect(m_tSkillEventInfo[0]);
					m_vecBulletScript[i]->OnStartEffect();
				}
				else
				{
					m_vecBulletScript[i]->Option_StartEvent(2.f);
					pEvent = m_vecBulletScript[i]->CreateEffect
					(
						0
						, m_tSkillEventInfo[0].eA_nTexIdx
						, m_tSkillEventInfo[0].eB_nType
						, m_tSkillEventInfo[0]
					);
					if(pEvent != nullptr)CSceneMgr::GetInst()->GetCurScene()->AddGameObject(L"Particle", pEvent, false);
				}
			}
			--onceCnt;
		}
	}
	CreateSkillBullet(onceCnt);
}

void CSkillPlatform::update()
{
}
