#pragma once
#include "Entity.h"
class CReadBackBuffer :
	public CEntity
{
private:
	ComPtr<ID3D12Resource>			m_pWriteBuffer;	//	In GPU, 
	ComPtr<ID3D12DescriptorHeap>	m_pWriteUAV;
	D3D12_CPU_DESCRIPTOR_HANDLE		m_hWriteUAV;

	ComPtr<ID3D12Resource>			m_pReadBuffer;	//	In CPU

	UAV_REGISTER					m_eRegisterNum;
	tReadBackInfo					m_ReadBackData;
	UINT							m_nRWBufferSize;
	D3D12_RESOURCE_STATES			m_eState;
	bool							m_bBeUpdate;
public:
	CReadBackBuffer();
	virtual ~CReadBackBuffer();

	void Create(UINT _iBufferSize, UINT _iMaxCount, UAV_REGISTER _eRegisterNum);
	ComPtr<ID3D12DescriptorHeap> GetWriteBufferView() { return m_pWriteUAV; }
	ComPtr<ID3D12Resource> GetWriteBuffer() { return m_pWriteBuffer; }
	ComPtr<ID3D12Resource> GetReadBuffer() { return m_pReadBuffer; }
	const UINT& GetRWBufferSize() { return m_nRWBufferSize; }

	void** GetDataForRead() { return reinterpret_cast<void**>(&m_ReadBackData); }

	D3D12_CPU_DESCRIPTOR_HANDLE GetWriteHandle() { return m_hWriteUAV; }
	const UAV_REGISTER& GetRegisterNum() { return m_eRegisterNum; }
	D3D12_RESOURCE_STATES GetResState() { return m_eState; }
	void SetResState(D3D12_RESOURCE_STATES _eState) { m_eState = _eState; }
	void BeUpdate() { if (m_bBeUpdate)m_bBeUpdate = false; else m_bBeUpdate = true; }
	const bool& IsUpdate() { return m_bBeUpdate; }
	bool test();
	tReadBackInfo* GetData() { if (m_bBeUpdate)return &m_ReadBackData; else return nullptr; }

	void SetPickId(UINT _id) { m_ReadBackData.objId = _id; }
	const UINT GetPickID() { return m_ReadBackData.objId; }
};

