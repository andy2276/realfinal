#include "stdafx.h"
#include "SkillEvent.h"
#include "SkillElementDir.h"
#include "MapMgr.h"

CSkillEvent::CSkillEvent()
	:CScript((UINT)SCRIPT_TYPE::MONSTERSKILL)
{
}


CSkillEvent::~CSkillEvent()
{
}

void CSkillEvent::SetTypes(UINT _type)
{
	m_Type = _type;
	m_vStartPos = m_vPrePos = m_vCurPos = Vec3::Zero;
	if ((UINT)EC_SKILL_EVENT::ActionType_Spread & m_Type)
	{
		m_vScaleMax = m_vScaleCur = m_vScaleMin = Vec3::Zero;
		m_fScaleAcc = 0.0f;
	}
	if ((UINT)EC_SKILL_EVENT::ActionType_Flow& m_Type)
	{
		m_pTarget = nullptr;
		m_fDelayDist = 0.0f;
	}
	if ((UINT)EC_SKILL_EVENT::ActionType_Rotate& m_Type)
	{
		m_vRotSpeed = Vec3::Zero;
	}
}

void CSkillEvent::SetValues(const tSkillEventValue& _value)
{
	m_vStartPos		=	_value.a_vStartPos;
	m_vPrePos		=	_value.a_vStartPos;
	m_vCurPos		=	_value.a_vStartPos;
	m_vStartRot = _value.a_vStratRot;
	
	if ((UINT)EC_SKILL_EVENT::ActionType_Spread & m_Type)
	{
		m_vScaleMax		=	_value.b_vScaleMax;
		m_vScaleCur		=	_value.b_vScaleCur;
		m_vScaleMin		=	_value.b_vScaleMin;
		m_fScaleAcc		=	_value.b_fScaleAcc;
		m_vScaleInit = _value.b_vScaleInit;
	}
	if ((UINT)EC_SKILL_EVENT::ActionType_Flow& m_Type)
	{
		m_pTarget		=	_value.c_pTarget;
		m_fDelayDist	=	_value.c_fDelayDist;
	}
	if ((UINT)EC_SKILL_EVENT::ActionType_Rotate& m_Type)
	{
		m_vRotSpeed		=	_value.d_vRotSpeed;
	}

}

void CSkillEvent::update()
{
	if (m_bAlive)
	{
		float fDeltaTime = DT;
		if (!m_bStart)
		{
			if (!m_bEnd)//	ing
			{
				m_vPrePos = m_vCurPos;
				m_vCurPos = Transform()->GetLocalPos();
				Vec3 vScale = Transform()->GetLocalScale();
				if ((UINT)EC_SKILL_EVENT::ActionType_Spread & m_Type)
				{
					//	이거는 나중에
					if (m_vScaleMin.x <= m_vScaleCur.x && m_vScaleCur.x <= m_vScaleMax.x)
					{
						m_vScaleCur.x += m_fScaleAcc;
						m_vScaleCur.y += m_fScaleAcc;
						m_vScaleCur.z += m_fScaleAcc;
					}
					else
					{
						if ((UINT)EC_SKILL_EVENT::ActionType_Option_TrunRepeat & m_Type)
						{
							
							if (m_fScaleAcc < 0.f)
							{
								m_vScaleCur = m_vScaleMin;
							}
							else
							{
								m_vScaleCur = m_vScaleMax;
							}
							m_fScaleAcc *= -1.f;
						}
						else
						{
							if (m_fScaleAcc < 0.f)
							{
								m_vScaleCur = m_vScaleMax;
							}
							else
							{
								m_vScaleCur = m_vScaleMin;
							}
						}
					}
					Transform()->SetLocalScale(m_vScaleCur);
				}
				if ((UINT)EC_SKILL_EVENT::ActionType_Flow& m_Type)
				{
					
				}
				if ((UINT)EC_SKILL_EVENT::ActionType_Rotate& m_Type)
				{
					Vec3	rot = Transform()->GetLocalRot();

					rot += m_vRotSpeed * fDeltaTime;
					Transform()->SetLocalRot(rot);
				}
				if ((UINT)EC_SKILL_EVENT::ActionType_Option_BillBoard& m_Type)
				{
					Vec3 camPos = CMapMgr::GetInst()->GetMainCam()->Transform()->GetWorldPos();

					Vec3 rot = Transform()->GetLocalRot();
					Vec3 lookPos = m_vCurPos - camPos;
					float fAngle = atan2(lookPos.x, lookPos.z);
					rot.y = fAngle;
					Transform()->SetLocalRot(rot);
				}

			}
			else//	EndTiming
			{
				//	초기화
				m_vScaleCur = m_vScaleInit;
				Transform()->SetLocalPos(m_vScaleCur);
				m_bAlive = false;
				m_bStart = true;
				m_bEnd = false;
				GetObj()->SetActive(false);
			}
		}
		else//	startTiming
		{
			m_vScaleCur = m_vScaleInit;
			Transform()->SetLocalPos(m_vScaleCur);
			Transform()->SetLocalPos(m_vStartPos);
			Transform()->SetLocalRot(m_vStartRot);
			m_bStart = false;
		}
	}
}
