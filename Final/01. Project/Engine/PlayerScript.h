#pragma once
#include "Script.h"
class CInventoryScript;
class CPlayerSkillScript;
enum class EC_PLAYER_SKILL {
	  _1
	, _2
	, _3
	, _4
	, _5
	, _6
	, End
};
struct tSkillSlot
{
	bool bSlotOn = false;
	bool bSkillOn = false;
	int nSkillNum = 0;
	float fSkillMax = 0.0f;
	float fSkillNow = 0.0f;
};
class CPixCamera;
class CPlayerScript :
	public CScript
{
private:
	Ptr<CMaterial>		m_pOriginMtrl;
	Ptr<CMaterial>		m_pCloneMtrl;

	CPixCamera*			m_pPixCamera;

	bool				m_bIdle;
	bool				m_bRun;


	float				m_fSkillCur;
	float				m_fSkillMax;

	float				m_fIdleMax;
	float				m_fIdleCur;

	bool				m_bStaticMouse;

	bool				m_bDamaged;
	float				m_fDamagedMax;
	float				m_fDamagedCur;

	tResolution			m_tRosol;
	RECT				m_rect;
	POINT				m_rectHalf;
	POINT				m_pMousePos;
	POINT				m_pPreMousePos;
	POINT				m_pPt;
	int					m_nDist;
	float				m_fMouseDir;
	CGameObject * m_ItemUI;

	//	지민추
	bool				m_bPressMove;
	bool				m_bSkillOn;
	bool				m_bInventoryOn;
	bool				m_bDeath;
	bool				m_bDeathEvent;

	Vec3				m_vAttackDir;
	Vec3				m_vAttackAngle;
	Vec3				m_vOriginCameraAngle;
	float				m_fAttackDist;
	float				m_fAttackDistMax;
	float				m_fAttackDistReal;
	float				m_fAttackDistCur; 

	Vec3				m_vAttackPos;

	float				m_fCamAngleMin;
	float				m_fCamAngleMax;


	tSkillSlot			m_arrSkillSlot[(UINT)EC_PLAYER_SKILL::End];

	float				m_fSkill2CoolMax;
	float				m_fSkill2CoolCur;
	bool				m_bSkill2On;

	bool				m_arrSkillOn[(UINT)EC_PLAYER_SKILL::End];
	float				m_arrSkillCoolMax[(UINT)EC_PLAYER_SKILL::End];
	float				m_arrSkillCoolNow[(UINT)EC_PLAYER_SKILL::End];

	//	지민추
	//	좌우는 무한회전임 

	int					m_nPreDirX;	//	이거삭제
	
	int					m_nLeftCnt = 0;//이거도삭제
	bool				m_bLeftOn;

	int					m_nRightCnt = 0;
	
	
	float				m_fRotSpeed;
	float				m_fRotCurAngle;

	bool				m_bPlayerRotOn;
	bool				m_bMouseMoveOn = false;
	bool				m_bMouseSet = true;

	float				m_fMouseStopTime = 0.0f;
	int					m_nYMax = 0;
	float				m_fYCur = 0.0f;

	Vec2				m_v2MouseAcc;


	bool				m_bPressZ; //z눌렸는지 
	CInventoryScript *	m_inventroyScript;
	bool				m_zControl;

	Ptr<CMaterial>		m_PlayerMtrl;
	CPlayerSkillScript * m_playerSkillScript;
	bool				m_GoldSkillOn;
	bool				m_bIsOnTitle;
	
	Ptr<CSound> pSound;
	CGameObject *	    m_TitleUI;
	Ptr<CSound> pTitleSound;
	Ptr<CSound> pMainSound;

public:
	virtual void awake();	
	virtual void update();

public:
	CLONE(CPlayerScript);

public:
	CPlayerScript();
	virtual ~CPlayerScript();
	virtual void OnCollisionEnter(CCollider2D* _pOther); // 충돌체가 처음 충돌
	virtual void OnCollisionExit(CCollider2D* _pOther);  // 충돌체가 서로 떨어질 때
	virtual void OnCollision(CCollider2D* _pOther);      // 충돌 중

	void SetPixCamera(CPixCamera* _pPix) { m_pPixCamera = _pPix; }
	bool isOn;

	void init();

	void setGoldMaterial();
};

