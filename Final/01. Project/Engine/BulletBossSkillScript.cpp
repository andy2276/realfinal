#include "stdafx.h"
#include "BulletBossSkillScript.h"



CBulletBossSkillScript::CBulletBossSkillScript():CScript((UINT)SCRIPT_TYPE::BULLETBOSSSCRIPT)

{
	m_Bullets.assign(100, nullptr);

	for (int i = 0; i < m_Bullets.size(); ++i)
	{
		//m_Bullets[i] = new CGameObject;


		Ptr<CMeshData> pMeshData10 = CResMgr::GetInst()->LoadFBX(L"FBX\\stone_s.FBX");

		pMeshData10->Save(pMeshData10->GetPath());

		m_Bullets[i] = pMeshData10->Instantiate();
		m_Bullets[i]->SetName(L"Bullet Object");
		m_Bullets[i]->AddComponent(new CTransform);
		m_Bullets[i]->Transform()->setPhysics(true);
		m_Bullets[i]->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));

	}

}


CBulletBossSkillScript::~CBulletBossSkillScript()
{
}

void CBulletBossSkillScript::awake()
{
}

void CBulletBossSkillScript::update()
{

}

void CBulletBossSkillScript::shoot(UINT eType)
{
	int bulletCount = 10;
	int bulletAngle = 360 / bulletCount;

	int layer = 0;

	switch (eType)
	{
	case (UINT)BULLET_TYPE::STRAIGHT:

		//플레이어의 방향으로 일직선으로 커다란 돌을 던진다. 
		//1. 플레이어의 방향 계산하기
		//2. 전조시간 보여주기 
		//3. 돌바닥에 충돌함과 동시에 파티클 3초정도 생성후 사라지기 . 
		//4. 


		//***** 이스킬 fbx 큰돌로 바꾸기 .
		bulletCount = 1;

			for (int i = 0; i < bulletCount; ++i)
			{
				Vec3 playerPos;
				Vec3 myPos = GetObj()->Transform()->GetLocalPos();

				Vec3 vdir = (playerPos - myPos).Normalize();

				float famount = 200;
				vdir.z *= famount;
				vdir.x *= famount;
				vdir.y *= famount;

				m_Bullets[i]->Transform()->setVelocity(vdir.x, vdir.y, vdir.z);
			}
		

		//기존에 사용했던 코드 -==============================================================================================================
		//총알이 앞으로 3개가 나아간다
		layer = 5;
		bulletCount = 5;
		for (int j = 0; j < layer; ++j)
		{
			for (int i = 0; i < bulletCount; ++i)
			{


				Vec3 vPos = GetObj()->Transform()->GetLocalPos();
				vPos.x += 10 * i;
				vPos.z += 20 * j;
				vPos.y += 50;
				m_Bullets[(j * bulletCount) + i]->Transform()->SetLocalPos(vPos);
							  
			//	m_Bullets[(j * bulletCount) + i]->AddComponent(new CMeshRender);
			//	m_Bullets[(j * bulletCount) + i]->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
			//	m_Bullets[(j * bulletCount) + i]->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"Std2DMtrl"));
				/*
						m_Bullets[i]->AddComponent(new CCollider2D);
						m_Bullets[i]->Collider2D()->SetOffsetScale(Vec3(5.f, 50.f, 50.f));
						m_Bullets[i]->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::OBB);*/

				Vec3 vdir = GetObj()->Transform()->GetWorldDir(DIR_TYPE::FRONT);
				float famount = 200;
				vdir.z *= famount;
				vdir.x *= famount;
				m_Bullets[(j * bulletCount) + i]->Transform()->setVelocity(0, 0, vdir.z);

				CreateObject(m_Bullets[(j * bulletCount) + i], L"Bullet");

			}
		}
		//=======================================================================================================================================
		break;
	case (UINT)BULLET_TYPE::LAYERCIRCLE:
		//원형으로 총알이 퍼지며 날아간다. 
		layer = 6;

		for (int j = 0; j < layer; ++j)
		{
			for (int i = 0; i < bulletCount; ++i)
			{

			 	Vec3 vPos = GetObj()->Transform()->GetLocalPos();
				Vec3 newPos;

				newPos.x = vPos.x + ((20*(j+1)) * sin(XM_ANGLE *bulletAngle *i)) ;
				newPos.z = vPos.z + ((20*(j+1)) * cos(XM_ANGLE *bulletAngle *i));
				newPos.y += 50;

				m_Bullets[(j * bulletCount) + i]->Transform()->SetLocalPos(newPos);
			
			//	m_Bullets[(j * bulletCount) + i]->AddComponent(new CMeshRender);
			//	m_Bullets[(j * bulletCount) + i]->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
			//	m_Bullets[(j * bulletCount) + i]->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"Std2DMtrl"));
				
				/*
						m_Bullets[i]->AddComponent(new CCollider2D);
						m_Bullets[i]->Collider2D()->SetOffsetScale(Vec3(5.f, 50.f, 50.f));
						m_Bullets[i]->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::OBB);*/

						//			float vdirZ = m_Bullets[i].tr GetObj()->Transform()->GetWorldDir(DIR_TYPE::FRONT).z;
						//			float vdirX = GetObj()->Transform()->GetWorldDir(DIR_TYPE::RIGHT).x;

				float vdirZ = m_Bullets[(j * bulletCount) + i]->Transform()->GetWorldDir(DIR_TYPE::FRONT).z;
				float vdirX = m_Bullets[(j * bulletCount) + i]->Transform()->GetWorldDir(DIR_TYPE::RIGHT).x;

				Vec3 vDir = newPos - vPos;
				float size = vDir.Length();

				m_Bullets[(j * bulletCount) + i]->Transform()->setVelocity((vDir.x / size) * 100, 0, (vDir.z / size) * 100);


				CreateObject(m_Bullets[(j * bulletCount) + i], L"Bullet");

				
			}
		}
		break;

	case (UINT)BULLET_TYPE::CIRCLE:

		bulletCount = 35;
		bulletAngle = 360 / bulletCount;
		for (int i = 0; i < bulletCount; ++i)
			{

				Vec3 vPos = GetObj()->Transform()->GetLocalPos();
				Vec3 newPos;

				newPos.x = vPos.x + ((20 ) * sin(XM_ANGLE *bulletAngle *i));
				newPos.z = vPos.z + ((20 ) * cos(XM_ANGLE *bulletAngle *i));
				newPos.y += 50;

				m_Bullets[i]->Transform()->SetLocalPos(newPos);
			
				/*
						m_Bullets[i]->AddComponent(new CCollider2D);
						m_Bullets[i]->Collider2D()->SetOffsetScale(Vec3(5.f, 50.f, 50.f));
						m_Bullets[i]->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::OBB);*/

						//			float vdirZ = m_Bullets[i].tr GetObj()->Transform()->GetWorldDir(DIR_TYPE::FRONT).z;
						//			float vdirX = GetObj()->Transform()->GetWorldDir(DIR_TYPE::RIGHT).x;

				float vdirZ = m_Bullets[i]->Transform()->GetWorldDir(DIR_TYPE::FRONT).z;
				float vdirX = m_Bullets[i]->Transform()->GetWorldDir(DIR_TYPE::RIGHT).x;

				Vec3 vDir = newPos - vPos;
				float size = vDir.Length();

				m_Bullets[i]->Transform()->setVelocity((vDir.x / size) * 100, 0, (vDir.z / size) * 100);


				CreateObject(m_Bullets[i], L"Bullet");

				
			}
		

		break;
	case (UINT)BULLET_TYPE::RANDOM:

		bulletCount = 35;
		bulletAngle = 360 / bulletCount;
		for (int i = 0; i < bulletCount; ++i)
		{

			Vec3 vPos = GetObj()->Transform()->GetLocalPos();
			Vec3 newPos;

			newPos.x = vPos.x + ((20) * sin(XM_ANGLE *bulletAngle *i));
			newPos.z = vPos.z + ((20) * cos(XM_ANGLE *bulletAngle *i));
			newPos.y += 50;

			m_Bullets[i]->Transform()->SetLocalPos(newPos);

		//	m_Bullets[i]->AddComponent(new CMeshRender);
		//	m_Bullets[i]->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
		//	m_Bullets[i]->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"Std2DMtrl"));
		//
			/*
					m_Bullets[i]->AddComponent(new CCollider2D);
					m_Bullets[i]->Collider2D()->SetOffsetScale(Vec3(5.f, 50.f, 50.f));
					m_Bullets[i]->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::OBB);*/

					//			float vdirZ = m_Bullets[i].tr GetObj()->Transform()->GetWorldDir(DIR_TYPE::FRONT).z;
					//			float vdirX = GetObj()->Transform()->GetWorldDir(DIR_TYPE::RIGHT).x;

			float vdirZ = m_Bullets[i]->Transform()->GetWorldDir(DIR_TYPE::FRONT).z;
			float vdirX = m_Bullets[i]->Transform()->GetWorldDir(DIR_TYPE::RIGHT).x;

			Vec3 vDir = newPos - vPos;
			float size = vDir.Length();
			
			float random1 = (rand() % 50)-25;
			float random2 = (rand() % 50)-25;

			m_Bullets[i]->Transform()->setVelocity(random1 * 10, 0, random2 * 10);


			CreateObject(m_Bullets[i], L"Bullet");

			
		}


		break;
	default:
		break;
	}
	
}
