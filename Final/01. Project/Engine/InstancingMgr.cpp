#include "stdafx.h"
#include "InstancingMgr.h"

CInstancingMgr::CInstancingMgr()
	: m_iInstIdx(0)
{
}

CInstancingMgr::~CInstancingMgr()
{
	for (size_t i = 0; i < m_vecInstingBuffer.size(); ++i)
	{
		SAFE_DELETE(m_vecInstingBuffer[i]);
	}
}

void CInstancingMgr::init()
{
	CInstancingBuffer* pInstBuffer = new CInstancingBuffer;
	pInstBuffer->init();
	m_vecInstingBuffer.push_back(pInstBuffer);
}

void CInstancingMgr::clear()
{
	m_iInstIdx = 0;
	for (size_t i = 0; i < m_vecInstingBuffer.size(); ++i)
	{
		m_vecInstingBuffer[i]->Clear();
	}
}

void CInstancingMgr::AddInstancingData(tInstancingData & _tData, bool _bAnim)
{
	m_vecInstingBuffer[m_iInstIdx]->AddInstancingData(_tData, _bAnim);
}

CInstancingBuffer * CInstancingMgr::GetInstancingBuffer(long long _iBufferID)
{
	for (auto& pBuffer : m_vecInstingBuffer)
	{
		if (pBuffer->GetBufferID() == _iBufferID)
			return pBuffer;
	}
	return nullptr;
}

CInstancingBuffer* CInstancingMgr::AllocBuffer(long long _bufferid)
{
	//	넣은녀석이 기존 버퍼보다 크면
	if (m_iInstIdx >= (UINT)m_vecInstingBuffer.size())
	{
		CInstancingBuffer* pInstBuffer = new CInstancingBuffer;
		pInstBuffer->init();
		pInstBuffer->SetBufferIdx(m_iInstIdx);
		m_vecInstingBuffer.push_back(pInstBuffer);
	}

	m_vecInstingBuffer[m_iInstIdx]->SetBufferID(_bufferid);

	return m_vecInstingBuffer[m_iInstIdx];
}

CInstancingBuffer * CInstancingMgr::AllocBuffer(const wstring & _name, long long _bufferid)
{
	CInstancingBuffer* pInstBuffer = new CInstancingBuffer;
	pInstBuffer->init();
	pInstBuffer->SetBufferIdx(m_vecInstingBuffer.size());
	pInstBuffer->SetName(_name);
	pInstBuffer->SetBufferID(_bufferid);
	m_vecInstingBuffer.push_back(pInstBuffer);

	return pInstBuffer;

}

void CInstancingMgr::SetData()
{
	m_vecInstingBuffer[m_iInstIdx]->SetData();	
	++m_iInstIdx;
}
