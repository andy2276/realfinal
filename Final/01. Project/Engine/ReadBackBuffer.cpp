#include "stdafx.h"
#include "ReadBackBuffer.h"
#include "Device.h"


CReadBackBuffer::CReadBackBuffer()
{
}


CReadBackBuffer::~CReadBackBuffer()
{
}

void CReadBackBuffer::Create(UINT _iBufferSize, UINT _iMaxCount, UAV_REGISTER _eRegisterNum)
{
	m_bBeUpdate = false;
	HRESULT hr = S_OK;
	m_nRWBufferSize = _iBufferSize;
	m_eRegisterNum = _eRegisterNum;
	D3D12_HEAP_PROPERTIES writeHeapProp{ CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT) };
	D3D12_RESOURCE_DESC writeBufferDesc{ CD3DX12_RESOURCE_DESC::Buffer(_iBufferSize, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS) };

	hr = DEVICE->CreateCommittedResource(
		&writeHeapProp
		, D3D12_HEAP_FLAGS::D3D12_HEAP_FLAG_NONE
		, &writeBufferDesc
		, D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_COPY_SOURCE
		, nullptr
		, IID_PPV_ARGS(m_pWriteBuffer.GetAddressOf())
	);
	m_ReadBackData.objId = 0;
	D3D12_HEAP_PROPERTIES readHeapProp{ CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_READBACK) };
	D3D12_RESOURCE_DESC readBufferDesc{ CD3DX12_RESOURCE_DESC::Buffer(_iBufferSize) };

	hr = DEVICE->CreateCommittedResource(
		&readHeapProp
		, D3D12_HEAP_FLAGS::D3D12_HEAP_FLAG_NONE
		, &readBufferDesc
		, D3D12_RESOURCE_STATES::D3D12_RESOURCE_STATE_COPY_DEST
		, nullptr
		, IID_PPV_ARGS(m_pReadBuffer.GetAddressOf())
	);
	D3D12_DESCRIPTOR_HEAP_DESC uavHeapDesc = {};
	uavHeapDesc.NumDescriptors = 1;
	uavHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	uavHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	DEVICE->CreateDescriptorHeap(&uavHeapDesc, IID_PPV_ARGS(&m_pWriteUAV));

	D3D12_CPU_DESCRIPTOR_HANDLE handle = m_pWriteUAV->GetCPUDescriptorHandleForHeapStart();

	D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
	uavDesc.Format = DXGI_FORMAT_UNKNOWN;
	uavDesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
	uavDesc.Buffer.FirstElement = 0;
	uavDesc.Buffer.NumElements = _iMaxCount;
	uavDesc.Buffer.StructureByteStride = _iBufferSize;
	uavDesc.Buffer.CounterOffsetInBytes = 0;
	uavDesc.Buffer.Flags = D3D12_BUFFER_UAV_FLAG_NONE;

	DEVICE->CreateUnorderedAccessView(m_pWriteBuffer.Get(), nullptr, &uavDesc, handle);
}

bool CReadBackBuffer::test()
{
	if (m_ReadBackData.objId == 6) {
		int a = 1;
		a;
		return true;
	}
	else
		return false;
}