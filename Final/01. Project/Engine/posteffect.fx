#ifndef _POSTEFFECT
#define _POSTEFFECT

#include "value.fx"
#include "func.fx"


struct VS_INPUT
{
    float3 vPos : POSITION;
    float2 vUV : TEXCOORD;
};

struct VS_OUTPUT
{
    float4 vOutPos : SV_Position;  
    float2 vOutUV : TEXCOORD;
};

//========================
// Distortion Shader
// AlphaBlend = false
// PostEffect = true
// g_Tex_0 : Posteffect Target
//========================

VS_OUTPUT VS_Distortion(VS_INPUT _input)
{
    // input 으로 들어온 위치정보를 투영좌표계 까지 변환한다.
    VS_OUTPUT output = (VS_OUTPUT) 0.f;
   
    output.vOutPos = mul(float4(_input.vPos, 1.f), g_matWVP);
    output.vOutUV = _input.vUV;

    return output;
}

struct PS_STD3D_OUT
{
    float4 vDiffuse : SV_Target0;
    float4 vNormal : SV_Target1;
    float4 vPosition : SV_Target2;
};

float4 PS_Distortion(VS_OUTPUT _input) : SV_Target
{
    float2 vScreenUV = float2(_input.vOutPos.x / g_vResolution.x, _input.vOutPos.y / g_vResolution.y);
    //return g_tex_0.Sample(g_sam_0, vScreenUV);
    
    
    float2 vDir = normalize(float2(0.5f, 0.5f) - _input.vOutUV);
    
    float fDist = distance(float2(0.5f, 0.5f), _input.vOutUV);

    // 왜곡 강도에 영향을 주는 중심으로부터 떨어진 비율( 중심에 가까울 수록 0에 가깝다.)
    float fRatio = (fDist / 0.5f);
    fRatio = fRatio * (0.2f + (fRatio) * 0.4f);

    vScreenUV += vDir * sin(abs((fRatio - g_fAccTime * 0.06f) * 80.f)) * 0.03f;
    
    PS_STD3D_OUT output = (PS_STD3D_OUT) 0.f;
    output.vDiffuse = g_tex_0.Sample(g_sam_0, vScreenUV);
    
    return g_tex_0.Sample(g_sam_0, vScreenUV);
}

//========================
// Distortion Character
// AlphaBlend = false
// PostEffect = true
// g_Tex_0 : Posteffect Texture
//========================
struct VS_DS_CHAR_INPUT
{
    float3 vPos : POSITION;
    float2 vUV : TEXCOORD;
    
    float4 vWeight : BLENDWEIGHT;
    float4 vIndices : BLENDINDICES;
};

struct VS_DS_CHAR_OUTPUT
{
    float4 vOutPos : SV_Position;    
    float2 vOutUV : TEXCOORD;
};

VS_DS_CHAR_OUTPUT VS_DistortionCharacter(VS_DS_CHAR_INPUT _input)
{
    // input 으로 들어온 위치정보를 투영좌표계 까지 변환한다.
    VS_DS_CHAR_OUTPUT output = (VS_DS_CHAR_OUTPUT) 0.f;
   
    if (g_int_0)
    {
        Skinning(_input.vPos, _input.vWeight, _input.vIndices, 0);
    }
    
    output.vOutPos = mul(float4(_input.vPos, 1.f), g_matWVP);  
    output.vOutUV = _input.vUV;
    
    return output;
}

float4 PS_DistortionCharacter(VS_DS_CHAR_OUTPUT _input) : SV_Target
{
    float2 vScreenUV = float2(_input.vOutPos.x / g_vResolution.x, _input.vOutPos.y / g_vResolution.y);           
    
    
    float2 vDir = normalize(float2(0.5f, 0.5f) - vScreenUV);
    float fDist = distance(float2(0.5f, 0.5f), vScreenUV);

    // 왜곡 강도에 영향을 주는 중심으로부터 떨어진 비율( 중심에 가까울 수록 0에 가깝다.)
    float fRatio = (fDist / 0.5f);
    fRatio = fRatio * (0.2f + (fRatio) * 0.4f);

    //                                                 속도     진폭                                         
    vScreenUV += vDir * sin(abs((fRatio - g_fAccTime * 0.06f) * 40.f)) * 0.03f;
    
    PS_STD3D_OUT output = (PS_STD3D_OUT) 0.f;
    output.vDiffuse = g_tex_0.Sample(g_sam_0, vScreenUV);
    
    return g_tex_0.Sample(g_sam_0, vScreenUV);
}
//========================
// Forge
// AlphaBlend = false
// PostEffect = true
// g_Tex_0 : Posteffect Texture
// g_Tex_1 : Position Texture
//========================

VS_OUTPUT VS_FORGE_EFFECT(VS_INPUT _in)
{
    VS_OUTPUT output = (VS_OUTPUT) 0.f;
    
    output.vOutPos = float4(_in.vPos.xyz, 0.0f);
    output.vOutUV = _in.vUV;
    return output;
}
float4 PS_FORGE_EFFECT(VS_OUTPUT _in) : SV_Target
{
    float2 vScreenUV = float2(_in.vOutPos.x / g_vResolution.x, _in.vOutPos.y / g_vResolution.y);
  
    float4 vColor = g_tex_0.Sample(g_sam_0, vScreenUV);
    float4 vPos = g_tex_1.Sample(g_sam_0, vScreenUV);
    
    PS_STD3D_OUT output = (PS_STD3D_OUT) 0.f;
   
    float fDist = distance(float2(0.5f, 0.5f), vScreenUV);
    
    if (vPos.z > 88.0f)
    {
        float fCul = (float) 0.f;
        fCul = (vPos.z - 100.f) * 0.0005f;
        vColor.xyz += fCul;
        output.vDiffuse = vColor;
        return vColor;
    }
    clip(-1);
    //vColor.w = 0.0f;
    return vColor;
}
VS_OUTPUT VS_Blur_EFFECT(VS_INPUT _in)
{
    VS_OUTPUT output = (VS_OUTPUT) 0.f;
    
    output.vOutPos = float4(_in.vPos.xyz, 0.0f);
    output.vOutUV = _in.vUV;
    return output;
}
float4 PS_Blur_EFFECT(VS_OUTPUT _in) : SV_Target
{
    float2 vScreenUV = float2(_in.vOutPos.x / g_vResolution.x, _in.vOutPos.y / g_vResolution.y);
  
    float4 vColor = g_tex_0.Sample(g_sam_0, vScreenUV);
    float4 vPos = g_tex_1.Sample(g_sam_0, vScreenUV);
    
    float2 vPixUV = float2(vScreenUV.x - 0.5f, vScreenUV.y);
    
    float dist = length(vPixUV);
    

    if (dist > 0.9f && vPos.z < 100.f && (vScreenUV.x < 0.23f || vScreenUV.x > 0.77f ))
    {
        float sinBlur = (0.55f + 0.45f * sin(5.0f * vScreenUV.x + g_fDT));
        float blurDist = 20.f - (100.f * (vPos.z / g_vResolution.y));
       // float blurDist = 15.f;
        float srand = Hash12n(vScreenUV + frac(g_fDT)) - 0.5f;
      
        float3 sumcol0 = (float3) 0.0f;
        {
            float2 blurDir = float2(1.0f, 0.577350269f);
            float2 blurVec = normalize(blurDir) / g_vResolution.xy;
            float2 p0 = vScreenUV - 0.5f * blurDist * blurVec;
            float2 p1 = vScreenUV + 0.5f * blurDist * blurVec;
            float2 stepVec = (p1 - p0) / 16.f;
            float2 p = p0;
            p += srand * stepVec;
          
            for (int i = 0; i < 16; ++i)
            {
                sumcol0 += srgb2Lin(g_tex_0.Sample(g_sam_0, p).rgb);
                p += stepVec;

            }
            sumcol0 /= 16.f;
        }
      
        float3 sumcol1 = (float3) 0.0f;
        {
            float2 blurDir2 = float2(-1.0f, 0.577350269f);
            float2 blurVec2 = normalize(blurDir2) / g_vResolution.xy;
            float2 p02 = vScreenUV - 0.5f * blurDist * blurVec2;
            float2 p12 = vScreenUV + 0.5f * blurDist * blurVec2;
            float2 stepVec = (p12 - p02) / 16.f;
            float2 p2 = p02;
            p2 += srand * stepVec;
          
            for (int i = 0; i < 16; ++i)
            {
                sumcol1 += srgb2Lin(g_tex_0.Sample(g_sam_0, p2).rgb);
                p2 += stepVec;

            }
            sumcol1 /= 16.f;
        }
      
        float3 sumcol = min(sumcol0, sumcol1);
        vColor = float4(lin2Srgb(sumcol), 1.0f);
        return vColor;

    }
    clip(-1);
    //if(vPos.z > 88.0f)
    //{
    //    float fCul = (float) 0.f;
    //    fCul = (vPos.z - 100.f) * 0.0005f;
    //    vColor.xyz += fCul;
    //    return vColor;
    //
    //}
   // vColor.w = 0.0f;
     return vColor;
}
//  Fire
//========================
// Forge
// AlphaBlend = false
// PostEffect = true
// g_Tex_0 : Noise Texture
// g_Tex_1 : FireTexture(no MipLevel) < - no
//========================
struct VS_FLAM_INPUT
{
    float3 vPos : POSITION;
    float2 vUV : TEXCOORD;
};



VS_OUTPUT VS_Flame_EFFECT_EZ(VS_INPUT _in)
{
    VS_OUTPUT output = (VS_OUTPUT) 0.f;
    
    output.vOutPos = mul(float4(_in.vPos, 1.f), g_matWV);
    
    output.vOutUV = _in.vUV;
    return output;
}
//  이거는 나중에
VS_OUTPUT VS_Flame_EFFECT_Inst(VS_INPUT _in)
{
    VS_OUTPUT output = (VS_OUTPUT) 0.f;
    output.vOutPos = mul(float4(_in.vPos, 1.f), g_matWV);
    output.vOutUV = _in.vUV;
    return output;
}

float4 PS_Flame_EFFECT_EZ(VS_OUTPUT _in) : SV_Target
{
    float2 uv = float2(_in.vOutPos.x / g_vResolution.x, _in.vOutPos.y / g_vResolution.y);
    
    float a = max(0.f, 1.f - distance(_in.vOutPos.x, g_vResolution.xy / 2.f) / 60.f);
    
    float2 texCoord = float2(_in.vOutUV.x, _in.vOutUV.y - g_fDT * 70.f);
    float4 texColor = g_tex_0.Sample(g_sam_0, texCoord);
    
    float i = texColor.x;
    
    a *= a;
    
    a /= i / 10.f;
    
    float3 outColor = float3(0.168f, 0.811f, 0.941f) * a;
    
   // PS_STD3D_OUT output = (PS_STD3D_OUT) 0.f;
   // output.vDiffuse = float4(outColor, 1.f);
    
    return float4(outColor, 1.f);
}
//  MdFire-cartoon
//========================
// Forge
// AlphaBlend = false
// PostEffect = true
// g_Tex_0 : Noise Texture
// g_Tex_1 : FireTexture(no MipLevel) < - no
//========================

//  FireMidFunc
float3 Mod289(float3 _x)
{
    return _x - floor(_x * (1.f / 289.f)) * 289.f;
}
float4 Mod289(float4 _x)
{
    return _x - floor(_x * (1.f / 289.f)) * 289.f;

}
float4 Permute(float4 _x)
{
    return Mod289(((_x * 34.f) + 1.f) * _x);

}
float4 TaylorInvSqrt(float4 _r)
{
    //  1.79284291400159f - 0.85373472095314f * _r;
    return 1.7928429140f - 0.8537347210f * _r;
}
float SNoise(float3 _v)
{
    const float2 C = float2(1.f / 6.f, 1.f / 3.f);
    const float4 D = float4(0.0f, 0.5f, 1.0f, 2.0f);

    float3 i = floor(_v + dot(_v, C.yyy));
    float3 x0 = _v - i + dot(i, C.xxx);
    
    float3 g = step(x0.yzx, x0.xyz);
    float3 l = 1.0f - g;
    float3 i1 = min(g.xyz, l.zxy);
    float3 i2 = max(g.xyz, l.zxy);
    
    float3 x1 = x0 - i1 + C.xxx;
    float3 x2 = x0 - i2 + C.yyy;
    float3 x3 = x0 - D.yyy;
    
    i = Mod289(i);
    
    float4 p = Permute(Permute(Permute(i.z + float4(0.0f, i1.z, i2.z, 1.0f)) + i.y + float4(0.0f, i1.y, i2.y, 1.0f))
           + i.x + float4(0.0f, i1.x, i2.x, 1.0f));
    
    //  0.142857142857f;
    float n_ = 0.1428571429f; // 1.0/7.0
    
    float3 ns = n_ * D.wyz - D.xzx;
    
    float4 j = p - 49.0f * floor(p * ns.z * ns.z); //  mod(p,7*7)
    
    float4 x_ = floor(j * ns.z);
    float4 y_ = floor(j - 7.0f * x_); // mod(j,N)
    
    float4 x = x_ * ns.x + ns.yyyy;
    float4 y = y_ * ns.x + ns.yyyy;
    float4 h = 1.0f - abs(x) - abs(y);
    
    float4 b0 = float4(x.xy, y.xy);
    float4 b1 = float4(x.zw, y.zw);
    
    float4 s0 = (floor(b0) * 2.0f) + 1.0f;
    float4 s1 = (floor(b1) * 2.0f) + 1.0f;
    float4 sh = -step(h, (float4)0.0f);
 
    float4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
    float4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
    
    float3 p0 = float3(a0.xy, h.x);
    float3 p1 = float3(a0.zw, h.y);
    float3 p2 = float3(a1.xy, h.z);
    float3 p3 = float3(a1.zw, h.w);
    
    float4 norm = TaylorInvSqrt(float4(dot(p0, p0), dot(p1, p1), dot(p2, p2), dot(p3, p3)));
    p0 *= norm.x;
    p1 *= norm.y;
    p2 *= norm.z;
    p3 *= norm.w;
    
    float4 m = max(0.6f - float4(dot(x0, x0), dot(x1, x1), dot(x2, x2), dot(x3, x3)), 0.0f);
    
    m = m * m;
    
    return 42.0f * dot(m * m, float4(dot(p0, x0), dot(p1, x1),dot(p2, x2), dot(p3, x3)));
}
float3 HSV2rgb(float3 c)
{
    float4 K = float4(1.0f, 2.0f / 3.0f, 1.0f / 3.0f, 3.0f);
    float3 p = abs(frac(c.xxx + K.xyz) * 6.0f - K.www);
    return c.z * lerp(K.xxx, clamp(p - K.xxx, 0.0f, 1.0f), c.y);
}
float GetNoise(float2 uv, float t)
{
    float TRAVEL_SPEED = 1.5f;
    
    float SCALE = 2.0f;
    float noise = SNoise(float3(uv.x * SCALE, uv.y * SCALE - t * TRAVEL_SPEED, 0.f));
    
    SCALE = 6.0f;
    noise += SNoise(float3(uv.x * SCALE + t, uv.y * SCALE, 0.f)) * 0.2f;
    
    noise = (noise / 2.f + 0.5f);
    return noise;
};
float GetDepth(float n)
{
    float d = (n - 0.15f) / (1. - 0.15f);
    d = floor(d * 4.f) / 4.f;
    return d;
};


//========================
// FireMd
// AlphaBlend = false
// PostEffect = true

//========================

VS_OUTPUT VS_Flame_EFFECT_MD(VS_INPUT _in)
{
    VS_OUTPUT output = (VS_OUTPUT) 0.f;
    
    output.vOutPos = mul(float4(_in.vPos, 1.f), g_matWVP);
    
    output.vOutUV = _in.vUV;
    return output;
};
float4 PS_Flame_EFFECT_MD(VS_OUTPUT _in) : SV_Target
{
    float CUTOFF = 0.15f;
    float2 uv = _in.vOutUV / g_vResolution.xy;
    uv *= uv.y;
    float t = g_fDT * 3.f;
    float3 col = (float3) 0.f;
   
    float noise = GetNoise(uv, t);
    
    CUTOFF = uv.y;
    CUTOFF += pow(abs(uv.x * 0.5f - 1.f), 1.f);
    
    if(noise < CUTOFF)
    {
        col = (float3)0.f;
    }
    else
    {
        float d = pow(GetDepth(noise), 0.7f);
        float hsv = float3(d * 0.17f, 0.8f - d / 4.f, d + 0.8f);
        col = HSV2rgb(hsv);
    }
    PS_STD3D_OUT output = (PS_STD3D_OUT) 0.f;
    output.vDiffuse = float4(col,1.f);
    
    return float4(col, 1.f);
}
//========================
// FireMd2
// AlphaBlend = false
// PostEffect = true
// g_tex_0 : postEffect
//========================

float Turbulence(float3 position, float minFreq, float maxFreq, float qWidth)
{
    float value = 0.0;
    float cutoff = clamp(0.5 / qWidth, 0.0, maxFreq);
    float fade;
    float fOut = minFreq;

    //  noiseStep
    for (int i = 4; i >= 0; i--)
    {
        if (fOut >= 0.5f * cutoff)
            break;

        fOut *= 2.0f;
        value += abs(SNoise(position * fOut)) / fOut;
    }

    fade = clamp(2.0f * (cutoff - fOut) / cutoff, 0.0f, 1.0f);
    value += fade * abs(SNoise(position * fOut)) / fOut;

    return 1.0f - value;
}
#define Color1 float4(1.0, 1.0, 1.0, 1.0)
#define Color2 float4(1.0, 0.8, 0.2, 1.0)
#define Color3 float4(1.0, 0.03, 0.0, 1.0)
#define Color4 float4(0.05, 0.02, 0.02, 1.0)

float SphereDist(float3 position)
{
    float Radius = 2.0f;
    return length(position - (float3) 0.f) - Radius;
}
float4 Shade(float distance)
{
    float c1 = saturate(distance * 5.0f + 0.5f);
    float c2 = saturate(distance * 5.0f);
    float c3 = saturate(distance * 3.4f - 0.5f);
	
    float4 a = lerp(Color1, Color2, c1);
    float4 b = lerp(a, Color3, c2);
    return lerp(b, Color4, c3);
}

float RenderScene(float3 position, out float distance)
{
    //  Animation float3(0.0, -3.0, 0.5)
    //  NoiseFrequency 2.2
    //  NoiseAmplitude 0.1
    float NoiseFrequency = 2.2f;
    float NoiseAmplitude = 0.1f;
    
    float Animation = float3(0.0f, -3.f, 0.5f);
    float noise = Turbulence(position * NoiseFrequency + Animation * g_fDT, 0.1, 1.5, 0.03) * NoiseAmplitude;
    noise = saturate(abs(noise));
    distance = SphereDist(position) - noise;
		
    return noise;
}
float4 March(float3 rayOrigin, float3 rayStep,float4 bg)
{
    float3 position = rayOrigin;
	
    float distance;
    float displacement;
    int MarchStemps = 6;
    float4 Background = bg;
	
    for (int step = MarchStemps; step >= 0; --step)
    {
        displacement = RenderScene(position, distance);
	
        if (distance < 0.05f)
            break;
		
        position += rayStep * distance;
    }
    return lerp(Shade(displacement), Background, float(distance >= 0.5f));
}
bool IntersectSphere(float3 ro, float3 rd, float3 pos, float radius, out float3 intersectPoint)
{
    float3 relDistance = (ro - pos);
	
    float b = dot(relDistance, rd);
    float c = dot(relDistance, relDistance) - radius * radius;
    float d = b * b - c;
	
    intersectPoint = ro + rd * (-b - sqrt(d));
	
    return d >= 0.0;
}



VS_OUTPUT VS_Flame_EFFECT_MD2(VS_INPUT _in)
{
    VS_OUTPUT output = (VS_OUTPUT) 0.f;
    
    output.vOutPos = mul(float4(_in.vPos, 1.f), g_matWVP);
    output.vOutUV = _in.vUV;
    return output;
};

float4 PS_Flame_EFFECT_MD2(VS_OUTPUT _in) : SV_Target
{
    float Radius = 2.0f;
    float NoiseAmplitude = 0.1f;
    
    float2 vScreenUV = float2(_in.vOutPos.x / g_vResolution.x, _in.vOutPos.y / g_vResolution.y);
    
    
    float4 Background = g_tex_0.Sample(g_sam_0, vScreenUV);
    
    float2 p = float2(_in.vOutPos.x / g_vResolution.x, _in.vOutPos.y / g_vResolution.y);
    p.x *= g_vResolution.x / g_vResolution.y;  //   이거는 프로젝션과 다를바가 없음
    
    float rotx = 4.f * g_fDT;
    float roty = -1.f * g_fDT;
    float zoom = 5.0f * g_fDT;
    
    // camera
    float3 ro = zoom * normalize(float3(cos(roty), cos(rotx), sin(roty)));
    float3 ww = normalize(float3(0.0, 0.0, 0.0) - ro);
    float3 uu = normalize(cross(float3(0.0, 1.0, 0.0), ww));
    float3 vv = normalize(cross(ww, uu));
    float3 rd = normalize(p.x * uu + p.y * vv + 1.5 * ww);
    
    
    float4 col = Background;//이거는 아마 그 뭐냐 포스트이펙트 텍스쳐로 해야할듯
    
    float3 origin;
    
    if (IntersectSphere(ro, rd, (float3) 0.f, Radius + NoiseAmplitude * 6.0f, origin))
    {
        col = March(origin, rd, Background);
    }
    return col;
}
//========================
// FireEz2
// AlphaBlend = false
// PostEffect = true

//========================
float EzNoise(float3 _p)
{
    float3 i = floor(_p);
    float4 a = dot(i, float3(1.f, 57.f, 21.f)) + float4(0.f, 57.f, 21.f, 78.f);
    float3 f = cos((_p - i) * acos(-1.f)) * (-5.f) + 0.5f;
    
    a = lerp(sin(cos(a) * a), sin(cos(1.f + a) * (1.f + a)), f.x);
    a.xy = lerp(a.xz, a.yw, f.y);
    return lerp(a.x, a.y, f.z);
}
float Sphere(float3 p, float4 spr)
{
    return length(spr.xyz - p) - spr.w;
}
float Flame(float3 p)
{
    float d = Sphere(p * float3(1.f, 0.5f, 1.f), float4(0.0f, -1.f, 0.0f, 1.f));
    return d + (EzNoise(p + float3(0.0f, g_fDT* 2.f, 0.0f)) + EzNoise(p * 3.f) * 0.5f) * 0.25f * (p.y);
}
float scene(float3 p)
{
    return min(100.f - length(p), abs(Flame(p)));
}
float4 raymarch(float3 org, float3 dir)
{
    float d = 0.0f, glow = 0.0f, eps = 0.02f;
    float3 p = org;
    bool glowed = false;
	
    for (int i = 0; i < 64; i++)
    {
        d = scene(p) + eps;
        p += d * dir;
        if (d > eps)
        {
            if (Flame(p) < 0.0f)
                glowed = true;
            if (glowed)
                glow = float(i) / 64.f;
        }
    }
    return float4(p, glow);
}

VS_OUTPUT VS_Flame_EFFECT_EZ2(VS_INPUT _in)
{
    VS_OUTPUT output = (VS_OUTPUT) 0.f;
    
    output.vOutPos = mul(float4(_in.vPos, 1.f), g_matWVP);
    output.vOutUV = _in.vUV;
    return output;
};

float4 PS_Flame_EFFECT_EZ2(VS_OUTPUT _in) : SV_Target
{
    float2 vScreenUV = float2(_in.vOutPos.x / g_vResolution.x, _in.vOutPos.y / g_vResolution.y);
    
    float4 Background = g_tex_0.Sample(g_sam_0, vScreenUV);
    
    float2 v = -1.0f + 2.0f * _in.vOutPos.xy / g_vResolution.xy;
    v.x *= g_vResolution.x / g_vResolution.y;
	
    float3 org = float3(0.0f, -2.f, 4.f);
    float3 dir = normalize(float3(v.x * 1.6f, -v.y, -1.5f));
	
    float4 p = raymarch(org, dir);
    float glow = p.w;
	
    float4 col = lerp(float4(1.f, 0.5f, 0.1f, 1.f), float4(0.1f, 0.5f, 1.f, 1.f), p.y * 0.02f + 0.4f);
    
    float4 color = lerp(Background, col, pow(glow * 2.f, 4.f));
	//fragColor = mix(vec4(1.), mix(vec4(1.,.5,.1,1.),vec4(0.1,.5,1.,1.),p.y*.02+.4), pow(glow*2.,4.));
    PS_STD3D_OUT output = (PS_STD3D_OUT) 0.f;
    output.vDiffuse = color;
    return color;
    
}
float mod(float x, float y)
{
    return x - y * floor(x / y);
}
float mod(float3 x,float y)
{
    return x - y * floor(x / y);
}
//========================
// FireEz3
// AlphaBlend = false
// PostEffect = true

//========================
float Ez3Snoise(float3 uv,float res)
{
    const float3 s = float3(1e0, 1e2, 1e3);
	
    uv *= res;
	
    float3 uv0 = floor(mod(uv, res)) * s;
    float3 uv1 = floor(mod(uv + (float3)1.f, res)) * s;
	
    float3 f = frac(uv);
    f = f * f * (3.0f - 2.0f * f);

    float4 v = float4(uv0.x + uv0.y + uv0.z, uv1.x + uv0.y + uv0.z,
		      	  uv0.x + uv1.y + uv0.z, uv1.x + uv1.y + uv0.z);

    float4 r = frac(sin(v * 1e-1) * 1e3);
    float r0 = lerp(lerp(r.x, r.y, f.x), lerp(r.z, r.w, f.x), f.y);
	
    r = frac(sin((v + uv1.z - uv0.z) * 1e-1) * 1e3);
    float r1 = lerp(lerp(r.x, r.y, f.x), lerp(r.z, r.w, f.x), f.y);
	
    return lerp(r0, r1, f.z) * 2. - 1.;
    
    
    
}
VS_OUTPUT VS_Flame_EFFECT_EZ3(VS_INPUT _in)
{
    VS_OUTPUT output = (VS_OUTPUT) 0.f;
    
    output.vOutPos = mul(float4(_in.vPos, 1.f), g_matWVP);
    output.vOutUV = _in.vUV;
    return output;
};

float4 PS_Flame_EFFECT_EZ3(VS_OUTPUT _in) : SV_Target
{
    float2 vScreenUV = float2(_in.vOutPos.x / g_vResolution.x, _in.vOutPos.y / g_vResolution.y);
    
    float2 p = -0.5f + _in.vOutUV.xy / g_vResolution.xy;
    p.x *= g_vResolution.x / g_vResolution.y;
	
    float color = 3.0f - (3.f * length(2.f * p));
	
    float3 coord = float3(atan2(p.x,p.y) / 6.2832f + 0.5f, length(p) * 0.4f, 0.5f);
    
    for (int i = 1; i <= 7; i++)
    {
        float power = pow(2.0f, float(i));
        color += (1.5f / power) * Ez3Snoise(coord + float3(0.f, -g_fAccTime * 0.05f, g_fAccTime * 0.01f), power * 16.);
    }
    float4 fragColor = float4(color, pow(max(color, 0.f), 2.f) * 0.4f, pow(max(color, 0.f), 3.f) * 0.15f, 1.0f);
    
	//fragColor = mix(vec4(1.), mix(vec4(1.,.5,.1,1.),vec4(0.1,.5,1.,1.),p.y*.02+.4), pow(glow*2.,4.));
    PS_STD3D_OUT output = (PS_STD3D_OUT) 0.f;
    output.vDiffuse = fragColor;
    return fragColor;
}
//========================
// FireEz3
// AlphaBlend = false
// PostEffect = true
// g_tex_0 : posteffect
// g_tex_1 : noise
//========================
struct VS_FLAME_INPUT
{
    float3 vPos : POSITION;
    float3 vNormal : NORMAL;
    float2 vUV : TEXCOORD;
};

struct VS_FLAME_OUTPUT
{
    float4 vOutPos : SV_Position;
    float3 vNormal : NORMAL;
    float4 vWorld : ORIGIN;
    float2 vOutUV : TEXCOORD;
};


float2x2 Makeem2(float theta)
{
    float c = cos(theta);
    float s = sin(theta);
    return float2x2(c, -s, s, c);
}
float Ez4Noise(float2 x)
{
    return g_tex_1.Sample(g_sam_0, x * 0.1f).x;
}
float Fbm(float2 p)
{
    float z = 6.f;
    float rz = 0.f;
    float2 bp = p;
    for (float i = 1.; i < 6.; i++)
    {
        rz += abs((Ez4Noise(p) - 0.5f) * 2.f) / z;
        z = z * 2.;
        p = p * 2.;
    }
    return rz;
}
float DualFbm(float2 p)
{
    float2 p2 = p * .7;
    float time = (g_fAccTime * 0.15f);
    float2 basis = float2(Fbm(p2 - (time * 0.15f) * 1.6), Fbm(p2 + (time * 0.15f) * 1.7f));
    basis = (basis - 0.5f) * 0.76f;
    p += basis;
	
	//coloring
    float2x2 mat = Makeem2(time * 0.72f);
    float2 t = float2(p.x * mat._11 + p.y + mat._21, p.x * mat._12 + p.y + mat._22);
    
    return Fbm(t);
}
float circ(float2 p)
{
    float r = length(p);
    r = log(sqrt(r));
    return abs(mod(r * 5.1f, 6.2831853f) - 3.14f) * 3.f + 0.2f;
}



VS_FLAME_OUTPUT VS_Flame_EFFECT_EZ4(VS_FLAME_INPUT _in)
{
    VS_FLAME_OUTPUT output = (VS_FLAME_OUTPUT) 0.f;
    
    output.vOutPos = mul(float4(_in.vPos, 1.f), g_matWVP);
    output.vNormal = normalize(mul(float4(_in.vNormal, 0.0), g_matWV));
    output.vWorld = mul(float4(_in.vPos, 1.0f), g_matWorld);
    output.vOutUV = _in.vUV;
    return output;
};

//float4 PS_Flame_EFFECT_EZ4(VS_OUTPUT _in) : SV_Target
//{
//    float2 vScreenUV = float2(_in.vOutPos.x / g_vResolution.x, _in.vOutPos.y / g_vResolution.y);
//    
//    float4 vColor = g_tex_0.Sample(g_sam_0, vScreenUV);
//    
//    float2 vDir = normalize(float2(cos(g_fDT), 0.5f) - _in.vOutUV);
//    float fDist = distance(float2(sin(g_fAccTime), cos(g_fAccTime)), _in.vOutUV);
//    
//    float3 coord = float3(fDist / 6.2832f + 0.5f, 0.f, 0.f);
//    float c = 0.f;
//    for (int i = 1; i <= 7; i++)
//    {
//        float power = pow(2.0f, float(i));
//        c += (1.5f / power) * Ez3Snoise(coord , power * 16.f);
//    }
//  //  float co = 3.0f - (3.f * length( _in.vOutUV));
//   
//    float4 color = float4(c, 0.f, 0.f, 1.0f);
// //   vColor *= 0.5f;
//  //  vColor += color;
//    vColor.x += color.x;
//    PS_STD3D_OUT output = (PS_STD3D_OUT) 0.f;
//    output.vDiffuse = vColor;
//    return vColor;
//}
//  new TT

float Ez5Hash(float p)
{
    uint x = uint(p + 16777041.);
    x = 1103515245U * ((x >> 1U) ^ (x));
    uint h32 = 1103515245U * ((x) ^ (x >> 3U));
    uint n = h32 ^ (h32 >> 16);
    return float(n) * (1.0 / float(0xffffffffU));
}
float Ez5Noise(float3 x)
{
    float3 p = floor(x);
    float3 f = frac(x);
    f = f * f * (3.0 - 2.0 * f);
    float n = p.x + p.y * 57.0 + 113.0 * p.z;
    return lerp(lerp(lerp(Ez5Hash(n + 0.0f), Ez5Hash(n + 1.0f), f.x), lerp(Ez5Hash(n + 57.0f), Ez5Hash(n + 58.0f), f.x), f.y),
           lerp(lerp(Ez5Hash(n + 113.0f), Ez5Hash(n + 114.0f), f.x), lerp(Ez5Hash(n + 170.0f), Ez5Hash(n + 171.0f), f.x), f.y), f.z);
}
float4 Ez5Map(float3 p)
{
    float d = 0.2 - p.y;
    float3 q = p - float3(0.0f, 1.0f, 0.0f) * g_fAccTime;
    float f = 0.50000 * Ez5Noise(q);
    q = q * 2.02 - float3(0.0f, 1.0f, 0.0f) * g_fAccTime;
    f += 0.25000f * Ez5Noise(q);
    q = q * 2.03f - float3(0.0f, 1.0f, 0.0f) * g_fAccTime;
    f += 0.12500f * Ez5Noise(q);
    q = q * 2.01f - float3(0.0f, 1.0f, 0.0f) * g_fAccTime;
    f += 0.06250f * Ez5Noise(q);
    q = q * 2.02f - float3(0.0f, 1.0f, 0.0f) * g_fAccTime;
    f += 0.03125f * Ez5Noise(q);
    d = clamp(d + 4.5f * f, 0.0f, 1.0f);
    float3 col = lerp(float3(1.0f, 0.9f, 0.8f), float3(0.4f, 0.1f, 0.1f), d) + 0.05f * sin(p);
    return float4(col, d);
}
float3 Ez5RayMarch(float3 ro, float3 rd)
{
    float4 s = float4(0.f, 0.f, 0.f, 0.f);
    float t = 0.f;
    for (int i = 0; i < 128; i++)
    {
        if (s.a > 0.99f)
            break;
        float3 p = ro + t * rd;
        float4 k = Ez5Map(p);
        k.rgb *= lerp(float3(3.0f, 1.5f, 0.15f), float3(0.5f, 0.5f, 0.5f), clamp((p.y - 0.2f) / 2.0f, 0.0f, 1.0f));
        k.a *= 0.5f;
        k.rgb *= k.a;
        s = s + k * (1.0f - s.a);
        t += 0.05f;
    }
    return clamp(s.xyz, 0.0f, 1.0f);
}
//  other
float2 N3Rotate2D(float2 p, float a)
{
    float si = sin(a);
    float co = cos(a);
    float2x2 mat2 = float2x2(si, co, -co, si);
    float2 result = float2(p.x * si + p.y * -co,p.x * -co + p.y * si);
    return result * p;
}
float N3Noise(float3 x)
{
    float3 p = floor(x);
    float3 f = frac(x);
    f = f * f * (3.0f - 2.0f * f);
    float2 uv = (p.xy + float2(37.0f, 17.0f) * p.z) + f.xy;
    float2 rg = g_tex_1.Sample(g_sam_0,uv);
    return lerp(rg.x, rg.y, f.z);
}

float N3Voronoi(float3 p)
{
    float d = 1.0e10;
    for (int zo = -1; zo <= 1; zo++)
    {
        for (int xo = -1; xo <= 1; xo++)
        {
            for (int yo = -1; yo <= 1; yo++)
            {
                float3 tp = floor(p) + float3(xo, yo, zo);
                d = min(d, length(p - tp - N3Noise(p)));
            }
        }
    }
    return 0.72f - d * d * d;
}
float3 FlameColour(float f)
{
    if (f > .999999f)
        return (float3) 1.0f;
    return min(float3(f * 5.0f, f * f, f * f * 0.05f), 1.0f);
}
float4 Map(in float3 p)
{
    float3 col = (float3)0.0;
    float di = length(p) - 8.0f;

    if (di > 0.0)
    {
        di -= N3Noise(p * 1.5f + float3(0.0f, 0.0f, g_fAccTime * 1.5f)) * 8.0f + cos(g_fAccTime * 0.5f) * 4.0f + 6.0f;
		
        float3 loops = p + float3(0.0f, 0.0f, g_fAccTime * 5.0f);
        loops.xy = N3Rotate2D(loops.xy, di * 0.15f - g_fAccTime * 0.15f);
		
        float h = N3Voronoi(loops * 0.2f);
        di = di + pow(h, 12.0) * 500.0;
        col = FlameColour(clamp(-di * .13, 0.0, 1.0));
    }
    else
    {
        col = (float3)1.0f;
        di *= 20.0f;
    }
    return float4(col, -di * 0.006f);
}

float2 N6hash(float2 p)
{
    p = float2(dot(p, float2(127.1f, 311.7f)),dot(p, float2(269.5f, 183.3f)));
    return -1.0f + 2.0f * frac(sin(p) * 43758.5453123f);
}
float N6Noise(float2 p)
{
    const float K1 = 0.366025404f; // (sqrt(3)-1)/2;
    const float K2 = 0.211324865f; // (3-sqrt(3))/6;

    float2 i = floor(p + (p.x + p.y) * K1);

    float2 a = p - i + (i.x + i.y) * K2;
    float2 o = step(a.yx, a.xy);
    float2 b = a - o + K2;
    float2 c = a - 1.0 + 2.0 * K2;

    float3 h = max(0.5f - float3(dot(a, a), dot(b, b), dot(c, c)), 0.0f);

    float3 n = h * h * h * h * float3(dot(a, N6hash(i + 0.0f)), dot(b, N6hash(i + o)), dot(c, N6hash(i + 1.0f)));

    return dot(n, float3(70.0f,70.f,70.f));
}
float N6Fbm(in float2 p)
{
    float f = 0.0;
    float2x2 m = float2x2(1.6f, 1.2f, -1.2f, 1.6f);
    f = 0.5000f * N6Noise(p);
    p = float2(p.x * m._11 + p.y * m._21, p.x * m._12 + p.y * m._22);
    
    f += 0.2500f * N6Noise(p);
    p = float2(p.x * m._11 + p.y * m._21, p.x * m._12 + p.y * m._22);
    
    f += 0.1250f * N6Noise(p);
    p = float2(p.x * m._11 + p.y * m._21, p.x * m._12 + p.y * m._22);
    
    f += 0.0625f * N6Noise(p);
    p = float2(p.x * m._11 + p.y * m._21, p.x * m._12 + p.y * m._22);
    
    f = 0.5f + 0.5f * f;
    return f;
}
float3 N6BumpMap(float2 uv)
{
    float2 s = 1.f / g_vResolution.xy;
    float p = N6Fbm(uv);
    float h1 = N6Fbm(uv + s * float2(1., 0));
    float v1 = N6Fbm(uv + s * float2(0, 1.));

    float2 xy = (p - float2(h1, v1)) * 40.f;
    return float3(xy + 0.5f, 1.0f);
}
float4 PS_Flame_EFFECT_EZ4(VS_FLAME_OUTPUT _in) : SV_Target
{
    float2 vScreenUV = float2(_in.vOutPos.x / g_vResolution.x, _in.vOutPos.y / g_vResolution.y);
    float2 vColorUV = vScreenUV;
    float2 vUV = _in.vOutUV;
    float4 vColor = g_tex_0.Sample(g_sam_0, vScreenUV);
    float2 vDistortionMove = float2(-0.01f, -0.3f);
    float vFireMovement = float2(-0.01f, -0.3f);
    
    float3 normal = N6BumpMap(vUV * float2(1.f, 0.3f) + vDistortionMove + g_fAccTime);
    float2 displace = clamp((normal.xy - 0.5f) * 40.f, -1.f, 1.f);
    vUV += displace;
    
    float2 uvT = (vUV * float2(1.f, 0.5f)) + vFireMovement * g_fAccTime;
    float n = pow(N6Fbm(8.f * uvT), 1.f);
    
    float gradient = pow(1.f - vUV.y, 2.f) * 5.f;
    float finalNoise = n * gradient;
    
    float3 color = finalNoise * float3(2.f * n, 2.f * n * n * n, n * n * n * n);
    
    //float2 vDir = normalize(float2(5.f, 0.5f) - _in.vOutUV);
    //float fDist = distance(float2(5.f, 0.5f), _in.vOutUV);
    //
    //// 왜곡 강도에 영향을 주는 중심으로부터 떨어진 비율( 중심에 가까울 수록 0에 가깝다.)
    //float fRatio = (fDist / 0.5f);
    //float2 neoColor = float2(cos(_in.vOutUV.x * g_fAccTime), sin(_in.vOutUV.y * g_fAccTime));
    vColor *= 0.3f;
    vColor += float4(color, 1.0f) * 0.7f;
   
  //  vColor.g += neoColor.y * 0.5f;
    
    return vColor;
}


//float4 PS_Flame_EFFECT_EZ4(VS_OUTPUT _in) : SV_Target
//{
//    float2 vScreenUV = float2(_in.vOutPos.x / g_vResolution.x, _in.vOutPos.y / g_vResolution.y);
//    float4 vColor = g_tex_0.Sample(g_sam_0, vScreenUV);
//    
//    float3x3 rotationMatrix = float3x3(1.0f, 0.0f, 0.0f, 0.0f, 0.47f, -0.88f, 0.0f, 0.88f, 0.47f);
//    float3 ro = float3(0.0, 4.9, -40.);
//    float3 m = normalize(float3((2.0 * _in.vOutUV.xy - g_vResolution.xy) / g_vResolution.y, 2.0));
//    float3 rd = float3(m.x * rotationMatrix._11 + m.y * rotationMatrix._21 + m.x * rotationMatrix._31,m.x * rotationMatrix._12 + m.y * rotationMatrix._22 + m.x * rotationMatrix._32, m.x * rotationMatrix._13 + m.y * rotationMatrix._23 + m.x * rotationMatrix._33);
//    
//    float3 volume = raymarch(ro, rd);
//    volume = volume * 0.5 + 0.5 * volume * volume * (3.0f - 2.0f * volume);
//    vColor += float4(volume.xyz,1.0f) * 0.011f;
//    
//    return vColor;
//}
#endif