#include "stdafx.h"
#include "PathResource.h"

CPathResource::CPathResource()
	:m_pData(nullptr)
	,m_nZCnt(0)
	,m_nXCnt(0)
	,m_nReference(0)
{}


CPathResource::~CPathResource(){
	if (m_pData != nullptr)delete[] m_pData;
}

void CPathResource::Init(const wstring & _path, const int& _zCnt, const int& _xCnt, const bool& _bInverse){
	m_nZCnt = _zCnt;
	m_nXCnt = _xCnt;
	if (m_pData == nullptr) {
		m_pData = new tPath[m_nZCnt*m_nXCnt];
		for (UINT i =0; i < m_nZCnt; ++i) {
			for (int j = 0; j < m_nXCnt; ++j) {
				m_pData[(i * m_nZCnt) + j].z = i;
				m_pData[(i * m_nZCnt) + j].x = j;
			}
		}

	}
	DWORD realSize = 0;
	BYTE* pScrData = new BYTE[m_nZCnt*m_nXCnt];
	HANDLE hFile = CreateFile(
		_path.c_str()
		, GENERIC_READ
		, FILE_SHARE_READ
		, NULL
		, OPEN_EXISTING
		, FILE_ATTRIBUTE_NORMAL
		, nullptr
	);
	ReadFile(
		hFile
		, pScrData
		, m_nZCnt*m_nXCnt
		, &realSize
		, nullptr
	);
	if (_bInverse) {
		int nDst = 0;
		for (int i = m_nZCnt - 1; i > 0; --i,++nDst) {
			for (int j = 0; j < m_nXCnt; ++j) {
				m_pData[(nDst * m_nZCnt) + j].type = pScrData[(i * m_nZCnt) + j];
			}
		}
	}
	else {
		for (int i = 0; i < m_nZCnt; ++i) {
			for (int j = 0; j < m_nXCnt; ++j) {
				m_pData[(i * m_nZCnt) + j].type = pScrData[(i * m_nZCnt) + j];
			}
		}
	}
	CloseHandle(hFile);
	delete[] pScrData;
}
