#include "stdafx.h"
#include "ParticleSystem.h"

#include "StructuredBuffer.h"
#include "ResMgr.h"
#include "KeyMgr.h"
#include "TimeMgr.h"

#include "Transform.h"

CParticleSystem::CParticleSystem()
	: CComponent(COMPONENT_TYPE::PARTICLESYSTEM)
	, m_pParticleBuffer(nullptr)
	, m_pSharedBuffer(nullptr)
	, m_iMaxParticle(500)//파티클 최대개수 1000->5개로 변경
	, m_fFrequency(0.002f)
	, m_fAccTime(0.f)
	, m_fMinLifeTime(0.1f)
	, m_fMaxLifeTime(0.25f)
	, m_fMinSpeed(100)
	, m_fMaxSpeed(50.f)
	, m_fStartScale(10.f)
	, m_fEndScale(0.5f)
	, m_vStartColor(Vec4(0.8f, 0.f, 0.f, 1.f))
	, m_vEndColor(Vec4(0.9f, 0.9f, 0.0f, 1.0f))
	, m_bInit(true)
	, m_dir(0,0,0,0)
	, m_bOnce(true)

{
	//처음 기초크기
//	m_fStartScale(30.f)
//		, m_fEndScale(10.f)


		//	, m_vStartColor(Vec4(0.2f, 0.2f, 0.8f, 1.4f))
		//	, m_vEndColor(Vec4(0.6f, 0.6f, 0.8f, 1.0f)) // 물방울색

	//	m_vStartColor(Vec4(0.8f, 0.2f, 0.2f, 1.4f)) //불 색 
	//	 m_vEndColor(Vec4(0.8f, 0.6f, 0.6f, 1.0f))
	
	//Ptr<CTexture> pParticle;




	// 구조화 버퍼 생성
	//m_pParticleBuffer = new CStructuredBuffer;	
	//m_pParticleBuffer->Create(sizeof(tParticle), m_iMaxParticle, nullptr);

	//m_pSharedBuffer = new CStructuredBuffer;	
	//m_pSharedBuffer->Create(sizeof(tParticleShared), 1, nullptr);
	//   	 
	//// 사각형 Mesh
	//m_pMesh = CResMgr::GetInst()->FindRes<CMesh>(L"PointMesh");

	//// Material
	//m_pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"ParticleMtrl");

	//기존 물방울 
	//Ptr<CTexture> pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\CartoonSmoke.png", L"Texture\\Particle\\Bubbles50px.png");

	
	

	//

	//m_pMtrl->SetData(SHADER_PARAM::TEX_0, pParticle.GetPointer());

	//// ParticleUpdate
	//m_pUpdateMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"ParticleUpdateMtrl");	
		// 사각형 Mesh
	m_pMesh = CResMgr::GetInst()->FindRes<CMesh>(L"PointMesh");

	// Material
	m_pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"ParticleMtrl");
	m_pMtrl =	m_pMtrl->Clone();



	// 구조화 버퍼 생성
	m_pParticleBuffer = new CStructuredBuffer;
	m_pParticleBuffer->Create(sizeof(tParticle), m_iMaxParticle, nullptr);

	m_pSharedBuffer = new CStructuredBuffer;
	m_pSharedBuffer->Create(sizeof(tParticleShared), 1, nullptr);




	m_iMaxParticle = 1000; //파티클 최대개수 1000->5개로 변경
	//	m_fFrequency(0.002f)
	//	m_fAccTime(0.f)
	m_fMinLifeTime = 0.5f;
	m_fMaxLifeTime = 1.f;
	m_fMinSpeed = 100;
	m_fMaxSpeed = 50.f;
	m_fStartScale = 50.f;
	m_fEndScale = 10.f;
	m_vStartColor = Vec4(0.22f, 0.10f, 0.45f, 1.4f);
	m_vEndColor = Vec4(0.4f, 0.2f, 0.8f, 1.0f);

	Ptr<CTexture>  pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\CartoonSmoke.png", L"Texture\\Particle\\CartoonSmoke.png");
	m_pMtrl->SetData(SHADER_PARAM::TEX_0, pParticle.GetPointer());


//	Ptr<CTexture>  pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\CartoonSmoke.png", L"Texture\\Particle\\electric.png");
//	m_pMtrl->SetData(SHADER_PARAM::TEX_0, pParticle.GetPointer());

	// ParticleUpdate
	m_pUpdateMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"ParticleUpdateMtrl");
	m_pUpdateMtrl = m_pUpdateMtrl->Clone();

}

CParticleSystem::CParticleSystem(UINT eType) : CComponent(COMPONENT_TYPE::PARTICLESYSTEM)
, m_pParticleBuffer(nullptr)
, m_pSharedBuffer(nullptr)
, m_iMaxParticle(1000)//파티클 최대개수 1000->5개로 변경
, m_fFrequency(0.002f)//m_fFrequency(0.002f)
, m_fAccTime(0.f)
, m_fMinLifeTime(0.5f)
, m_fMaxLifeTime(1.f)
, m_fMinSpeed(100)
, m_fMaxSpeed(50.f)
, m_fStartScale(10.f)
, m_fEndScale(0.5f)
, m_vStartColor(Vec4(0.8f, 0.f, 0.f, 1.f))
, m_vEndColor(Vec4(0.9f, 0.9f, 0.0f, 1.0f))
, m_bInit(true)
, m_dir(0, 0,0,0)
, m_bOnce(true)
{
	m_pMesh = CResMgr::GetInst()->FindRes<CMesh>(L"PointMesh");
	m_pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"ParticleMtrl");
	m_pMtrl = m_pMtrl->Clone();


	// 구조화 버퍼 생성
	m_pParticleBuffer = new CStructuredBuffer;
	m_pParticleBuffer->Create(sizeof(tParticle), m_iMaxParticle, nullptr);

	m_pSharedBuffer = new CStructuredBuffer;
	m_pSharedBuffer->Create(sizeof(tParticleShared), 1, nullptr);

	m_iMaxParticle = 500; //파티클 최대개수 1000->5개로 변경
	//	m_fFrequency(0.002f)
	//	m_fAccTime(0.f)
	m_fMinLifeTime = 0.5f;
	m_fMaxLifeTime = 1.f;
	m_fMinSpeed = 100;
	m_fMaxSpeed = 50.f;
	m_fStartScale = 50.f;
	m_fEndScale = 40.f;
	m_vStartColor = Vec4(0.22f, 0.10f, 0.45f, 1.4f);
	m_vEndColor = Vec4(0.4f, 0.2f, 0.8f, 1.0f);

//	Ptr<CTexture>  pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\CartoonSmoke.png", L"Texture\\Particle\\snowflower2.png");
//	m_pMtrl->SetData(SHADER_PARAM::TEX_0, pParticle.GetPointer());

	m_pUpdateMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"ParticleUpdateMtrl");
	m_pUpdateMtrl = m_pUpdateMtrl->Clone();

	m_eType = eType;

	UINT u = m_eType;
	switch (u)
	{
	case (UINT)PARTICLE_TYPE::SNOW:
	{
		m_iMaxParticle = 3; //파티클 최대개수 1000->5개로 변경
		m_fFrequency = 0.002f;
		m_fAccTime = 0.f;
		m_fMinLifeTime = 0.5f;//m_fMinLifeTime = 0.5f;
		m_fMaxLifeTime = 1.f;
		m_fMinSpeed = 100;
		m_fMaxSpeed = 50.f;
		m_fStartScale = 10.f;
		m_fEndScale = 0.f;
		m_vStartColor = Vec4(0.1f, 0.1f, 0.6f, 1.4f);
		m_vEndColor = Vec4(0.9, 0.9f, 0.9f, 1.0f);

	//	Ptr<CTexture>  pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\CartoonSmoke.png", L"Texture\\Particle\\snowflower2.png");
	//	m_pMtrl->SetData(SHADER_PARAM::TEX_0, pParticle.GetPointer());
		//m_pUpdateMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"ParticleUpdateMtrl");

		m_dir = Vec4(1, 1, 0,0);
	}
	break;
	case (UINT)PARTICLE_TYPE::FIRE:
	{
	//	Ptr<CTexture> pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\CartoonSmoke.png", L"Texture\\Particle\\fire.png");
	//	m_pMtrl->SetData(SHADER_PARAM::TEX_0, pParticle.GetPointer());

	
		m_iMaxParticle = 10; //파티클 최대개수 1000->5개로 변경
		m_fFrequency = 0.002f;//	m_fFrequency(0.002f)
		//	m_fAccTime(0.f)
		m_fMinLifeTime = 0.5f;
		m_fMaxLifeTime = 2.f;
		m_fMinSpeed = 100;
		m_fMaxSpeed = 50.f;
		m_fStartScale = 10.f;
		m_fEndScale = 5.f;
		m_vStartColor = Vec4(0.9f, 0.2f, 0.0f, 1.4f);
		m_vEndColor = Vec4(0.9f, 0.9, 0.0f, 1.0f);
		m_dir = Vec4(0, 1, 0,0);
	
	}
	break;
	case (UINT)PARTICLE_TYPE::BLOOD:
	{
		m_iMaxParticle = 100; //파티클 최대개수 1000->5개로 변경
		m_fFrequency = 0.002f;//	m_fFrequency(0.002f)
		//	m_fAccTime(0.f)
		m_fMinLifeTime = 2.f;
		m_fMaxLifeTime = 2.5f;
		m_fMinSpeed = 200;
		m_fMaxSpeed = 200.f;

		m_fStartScale = 10.f;
		m_fEndScale = 0.f;
		m_vStartColor = Vec4(0.6f, 0.01f, 0.0f, 1.4f);
		m_vEndColor = Vec4(0.6f, 0.6, 0.0f, 1.0f);
		m_dir = Vec4(0, 1, 0, 0);
		
	}

	break;
	case (UINT)PARTICLE_TYPE::BLUR:
	{
		m_iMaxParticle = 100; //파티클 최대개수 1000->5개로 변경
		m_fFrequency = 0.002f;//	m_fFrequency(0.002f)
		//	m_fAccTime(0.f)
		m_fMinLifeTime = 10.f;
		m_fMaxLifeTime = 10.f;
		m_fMinSpeed = 200;
		m_fMaxSpeed = 200.f;
		m_fStartScale = 15.f;
		m_fEndScale = 0.f;
		m_vStartColor = Vec4(0.6f, 0.6f, 0.9f, 1.4f);
		m_vEndColor = Vec4(0.6f, 0.6f, 0.6f, 1.4f);
		m_dir = Vec4(0, 1, 0, 0);
		Ptr<CTexture> pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\blur7.png", L"Texture\\Particle\\blur7.png");
		m_pMtrl->SetData(SHADER_PARAM::TEX_0, pParticle.GetPointer());
	}
	break;
	default:
		break;
	}
	if (eType == (UINT)PARTICLE_TYPE::FIRE)
	{


		Ptr<CTexture> pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\flameEffect.png", L"Texture\\Particle\\flameEffect.png");
		m_pMtrl->SetData(SHADER_PARAM::TEX_0, pParticle.GetPointer());
	}
	else if(eType == (UINT)PARTICLE_TYPE::SNOW){

		Ptr<CTexture> pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\snowflower2.png", L"Texture\\Particle\\snowflower2.png");
		m_pMtrl->SetData(SHADER_PARAM::TEX_0, pParticle.GetPointer());
	}
	else if (eType == (UINT)PARTICLE_TYPE::BLOOD)
	{
		Ptr<CTexture> pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\hit2.png", L"Texture\\Particle\\hit2.png");
		m_pMtrl->SetData(SHADER_PARAM::TEX_0, pParticle.GetPointer());
	}

}

CParticleSystem::CParticleSystem(bool _noCtor)
	: CComponent(COMPONENT_TYPE::PARTICLESYSTEM)
{
	m_pMesh = CResMgr::GetInst()->FindRes<CMesh>(L"PointMesh");
	m_pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"ParticleMtrl");
	m_bOnce = true;
}

CParticleSystem::~CParticleSystem()
{
	SAFE_DELETE(m_pParticleBuffer);
	SAFE_DELETE(m_pSharedBuffer);
}

void CParticleSystem::init(UINT u)
{

	//switch (u)
	//{
	//case (UINT)PARTICLE_TYPE::SNOW:
	//{
	//	m_iMaxParticle = 5; //파티클 최대개수 1000->5개로 변경
	//	m_fFrequency = 0.002f;
	//	m_fAccTime = 0.f;
	//	m_fMinLifeTime = 0.5f;
	//	m_fMaxLifeTime = 1.f;
	//	m_fMinSpeed = 100;
	//	m_fMaxSpeed = 50.f;
	//	m_fStartScale = 10.f;
	//	m_fEndScale = 5.f;
	//	m_vStartColor = Vec4(0.1f, 0.1f, 0.3f, 1.4f);
	//	m_vEndColor = Vec4(0.9, 0.9f, 0.9f, 1.0f);

	//	Ptr<CTexture>  pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\CartoonSmoke.png", L"Texture\\Particle\\snowflower2.png");
	//	m_pMtrl->SetData(SHADER_PARAM::TEX_0, pParticle.GetPointer());
	//	//m_pUpdateMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"ParticleUpdateMtrl");
	
	//}
	//break;
	//case (UINT)PARTICLE_TYPE::FIRE :
	//{
	//	Ptr<CTexture> pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\CartoonSmoke.png", L"Texture\\Particle\\fire.png");
	//	m_pMtrl->SetData(SHADER_PARAM::TEX_0, pParticle.GetPointer());

	

	//	m_iMaxParticle = 2; //파티클 최대개수 1000->5개로 변경
	//	//	m_fFrequency(0.002f)
	//	//	m_fAccTime(0.f)
	//	m_fMinLifeTime = 0.5f;
	//	m_fMaxLifeTime = 1.f;
	//	m_fMinSpeed = 100;
	//	m_fMaxSpeed = 50.f;
	//	m_fStartScale = 100.f;
	//	m_fEndScale = 100.f;
	//	m_vStartColor = Vec4(0.6f, 0.01f, 0.0f, 1.4f);
	//	m_vEndColor = Vec4(0.9f, 0.6, 0.0f, 1.0f);


	//}
	//break;
	//default:
	//	break;
	//}



	//Ptr<CTexture>  pParticle;









	//switch (m_eType)
	//{
	//case (UINT)PARTICLE_TYPE::SNOW :

	//	pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\CartoonSmoke.png", L"Texture\\Particle\\snowflower2.png");



	//		m_iMaxParticle = 2; //파티클 최대개수 1000->5개로 변경
	//	//	m_fFrequency(0.002f)
	//	//	m_fAccTime(0.f)
	//		m_fMinLifeTime = 0.5f;
	//		m_fMaxLifeTime = 1.f;
	//		m_fMinSpeed = 100;
	//		m_fMaxSpeed = 50.f;
	//		m_fStartScale = 10.f;
	//		m_fEndScale =5.f;
	//		m_vStartColor = Vec4(0.1f, 0.1f, 0.3f, 1.4f);
	//		m_vEndColor = Vec4(0.9, 0.9f, 0.9f, 1.0f);


	//		

	//	break;
	//case (UINT)PARTICLE_TYPE::FIRE:

	//	pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\CartoonSmoke.png", L"Texture\\Particle\\fire.png");



	//	m_iMaxParticle = 2; //파티클 최대개수 1000->5개로 변경
	//	//	m_fFrequency(0.002f)
	//	//	m_fAccTime(0.f)
	//	m_fMinLifeTime = 0.5f;
	//	m_fMaxLifeTime = 1.f;
	//	m_fMinSpeed = 100;
	//	m_fMaxSpeed = 50.f;
	//	m_fStartScale = 100.f;
	//	m_fEndScale = 100.f;
	//	m_vStartColor = Vec4(0.6f, 0.01f, 0.0f, 1.4f);
	//	m_vEndColor = Vec4(0.9f, 0.6, 0.0f, 1.0f);




	//	break;
	//case (UINT)PARTICLE_TYPE::DARK:
	//	
	//	pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\CartoonSmoke.png", L"Texture\\Particle\\fire.png");




	//	m_iMaxParticle = 5; //파티클 최대개수 1000->5개로 변경
	//	//	m_fFrequency(0.002f)
	//	//	m_fAccTime(0.f)
	//	m_fMinLifeTime = 0.5f;
	//	m_fMaxLifeTime = 1.f;
	//	m_fMinSpeed = 100;
	//	m_fMaxSpeed = 50.f;
	//	m_fStartScale = 100.f;
	//	m_fEndScale = 100.f;
	//	m_vStartColor = Vec4(0.22f, 0.10f, 0.45f, 1.4f);
	//	m_vEndColor = Vec4(0.4f, 0.2f, 0.80f, 1.0f);




	//	break;

	//	

	//default:
	//	break;
	//}



}

void CParticleSystem::Init
(
	int _nMaxParticle, float _fFrequency, float _fAccTime
	,float _fMinLifeTime, float _fMaxLifeTime
	, float, float _fMinSpeed, float _fMaxSpeed
	, float _fStartScale, float _fEndScale
	, const Vec4 & _v4StartColor, const Vec4 & _v4EndColor
	, const wstring& _particleTexture
)
{


}

void CParticleSystem::finalupdate()
{	
	if (!m_bInit)return;
	if (m_bOnce)
	{
		m_bOnce = false;
		if (m_eType == (UINT)PARTICLE_TYPE::FIRE)
		{

			Ptr<CTexture> pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\fire.png", L"Texture\\Particle\\fire.png");

			//	안바뀌는 이유가 이름에 있지않을까?!

			m_pMtrl->SetData(SHADER_PARAM::TEX_0, pParticle.GetPointer());
		}
		else if (m_eType == (UINT)PARTICLE_TYPE::SNOW)
		{
			

		//	Ptr<CTexture> pParticle = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Particle\\snowflower2.png", L"Texture\\Particle\\snowflower2.png");
		//	m_pMtrl->SetData(SHADER_PARAM::TEX_0, pParticle.GetPointer());
		}

	//	m_pMtrl->SetData(SHADER_PARAM::VEC4_2, &m_dir);

	}


	//20200721 아림추
		if (m_parent != nullptr)
		{
			if (m_parent->IsActive())
			{

			//	GetObj()->SetActive(true);
				Vec3 vpos = m_parent->Transform()->GetLocalPos();
				vpos.y += 10;
				//vpos.z += 20;
				Transform()->SetLocalPos(vpos);
			}
			else {
		//		GetObj()->SetActive(false);
			}
			
		
		}




	// 추가될 파티클 개수 계산
	m_fAccTime += DT;

	int iAdd = 0;
	if (m_fFrequency < m_fAccTime)
	{		
		m_fAccTime = m_fAccTime - m_fFrequency;
		iAdd = 1;
	}
	   	
	m_pParticleBuffer->UpdateRWData(UAV_REGISTER::u0);
	m_pSharedBuffer->UpdateRWData(UAV_REGISTER::u1);
	
	m_pUpdateMtrl->SetData(SHADER_PARAM::INT_0, &m_iMaxParticle);
	m_pUpdateMtrl->SetData(SHADER_PARAM::INT_1, &iAdd);
	m_pUpdateMtrl->SetData(SHADER_PARAM::INT_2, &m_eType); //

	//int_2 :  true = 
	m_pUpdateMtrl->SetData(SHADER_PARAM::FLOAT_0, &m_fMinLifeTime);
	m_pUpdateMtrl->SetData(SHADER_PARAM::FLOAT_1, &m_fMaxLifeTime);

	m_pUpdateMtrl->SetData(SHADER_PARAM::FLOAT_2, &m_fMinSpeed);
	m_pUpdateMtrl->SetData(SHADER_PARAM::FLOAT_3, &m_fMaxSpeed);
	
	m_pUpdateMtrl->Dispatch(1, 1, 1);
}

void CParticleSystem::render()
{
//	if (!m_bInit)return;
	Transform()->UpdateData();

	m_pParticleBuffer->UpdateData(TEXTURE_REGISTER::t10);

	m_pMtrl->SetData(SHADER_PARAM::VEC4_0, &m_vStartColor);
	m_pMtrl->SetData(SHADER_PARAM::VEC4_1, &m_vEndColor);
	m_pMtrl->SetData(SHADER_PARAM::FLOAT_0, &m_fStartScale);
	m_pMtrl->SetData(SHADER_PARAM::FLOAT_1, &m_fEndScale);

	m_pMtrl->UpdateData();		
	m_pMesh->render_particle(m_iMaxParticle);	
}

void CParticleSystem::awake()
{


}

void CParticleSystem::SaveToScene(FILE * _pFile)
{
}

void CParticleSystem::LoadFromScene(FILE * _pFile)
{
}
