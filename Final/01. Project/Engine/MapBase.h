#pragma once
class CMapBase{
private:
	UINT m_nWid, m_nHei;
public:
	CMapBase();
	virtual ~CMapBase();

	const UINT& GetWid() { return m_nWid; }
	const UINT& GetHei() { return m_nHei; }
	virtual void CreateMapData(const wstring* _wstrPath) {};
};

