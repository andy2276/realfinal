#pragma once
#include "Script.h"
struct tShudder
{
	int					nFrameCnt;
	Vec3				vFrameRot;
	Vec3				vFramePos;
};
struct tShudderPattern
{
	wstring				wstrName;
	vector<tShudder>	vecShudder;
};

class CPixCamera :
	public CScript
{
private:
	Vec3		m_vOriginPos;
	Vec3		m_vOriginRot;

	//	��
	Vec3		m_vChangePos;
	Vec3		m_vChangeRot;


	bool		m_bSudderOn;
	bool		m_bShudderStart;

	bool		m_bDeathEvent;

	bool		m_bShuuderStop;

	int			m_nShudderPatternMax;
	int			m_nShudderPatternCur;
	int			m_nShudderCurOrder;
	int			m_nShudderCurFrame;

	vector< tShudderPattern>	m_vecShdderPAttern;
	tShudderPattern				m_tSkillShudder;
	tShudderPattern				m_tDeathShudder;
	tShudderPattern*			m_pCurShudder;

public:
	CPixCamera();
	virtual ~CPixCamera();
public:
	CLONE(CPixCamera);

public:
	virtual void update();

	void RandomShudder();
	void Skill1Shudder();
	void DeathShudder();
	void SetShudder(int _idx) { m_bSudderOn = true; m_nShudderPatternCur = _idx; }

	void OnDeathShudder() { m_bDeathEvent = true; };
	void OffDeathShudder() { m_bDeathEvent = false; };

	void SetOriginPos(const Vec3& _pos){ m_vOriginPos = m_vChangePos = _pos; };
	void SetOriginRot(const Vec3& _rot) { m_vOriginRot = m_vChangeRot= _rot; };

};

