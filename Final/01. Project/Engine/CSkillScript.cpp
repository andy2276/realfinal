#include "stdafx.h"
#include "CSkillScript.h"


void CSkillScript::update()
{
	//
	//업데이트가 될때마다 현재 시간을 누적해주고, 
	m_curTime += DT;
	//ex) 쿨타임 N초가 지나면 유성이 다시 살아나서 떨어지게 한다.  
	if (m_coolTime - m_curTime < 0)
	{
		isCanShoot = true;
		m_curTime = 0;
		GetObj()->enable();


	}

	//순서대로 살아나도록 하기 
	m_curTime += DT;

	if (m_coolTime - m_curTime < 0)
	{

		isCanShoot = true;
		m_curTime = 0;
		GetObj()->enable();


	}

	if (isCanShoot)
	{	//도착지점까지 x축 이동해주기 
	//////////////////////////////

		Vec3 vPos = GetObj()->Transform()->GetLocalPos();//현재 위치 받아와서 

		//도착 지점보다 멀리있으면 x값을 더해준다 
		//if (!FloatCmp(vPos.x, m_ArrivePos.x, 0.0001f))
		//{
		//	//vPos.x -= 1 / m_attackHypotenuseLenght;
		//	vPos.x -= (m_attackHypotenuseLenght/ m_attackHypotenuseLenght);

		//}
		//else {
		//	vPos.x = m_ArrivePos.x;
		//}
		float speed = 200.f;
		if (vPos.x > m_ArrivePos.x)
		{
			vPos.x -= speed * DT;
			vPos.y -= speed * 2 * DT;
		}
		else {
			//vPos.x = m_ArrivePos.x;
		}

		//y값이 0이 되면 사라지게 
		if (vPos.y <= 0)
		{
			isCanShoot = false;
			GetObj()->disable();
			vPos.x = m_attackStart;
			vPos.y = m_attackRadius * 2 ;
		}

		GetObj()->Transform()->SetLocalPos(vPos);

	}



}

CSkillScript::CSkillScript() : CScript((UINT)SCRIPT_TYPE::SKILLSCRIPT)
	, m_curTime(0)
	,m_coolTime(3)
	,m_remainCoolTime(0)
	,isCanShoot(false)
	
{
}


CSkillScript::~CSkillScript()
{
}

void CSkillScript::init(float cooltime)
{
	m_coolTime = 3;

	Vec3 vPos = GetObj()->Transform()->GetLocalPos();
//	vPos.y += 200; //하늘에서 떨어지게 하기 위해서 

//	, m_attackArrive()

	m_attackRadius = 50; 


	float random; //랜덤 시작 지점 x좌표 결정하기 
	//시작지점의 최대는 에임지점 + 2* 반지름 
	// ' 2* 반지름 ' 이므로 랜덤값은 반지름보다 크지 않으면 된다. 
	random = rand() % (m_attackRadius * 2) + 1;

	if (random / 2 != 0)
	{
		vPos.y += m_attackRadius *2;
	}
	else {
		vPos.y += m_attackRadius *2 + 50;
	}





	m_attackStart = vPos.x + random; // 시작지점을 (최대값이 존재하는)랜덤으로 설정한뒤

	m_ArrivePos.x = m_attackStart - m_attackRadius;// 도착지점의 값은 시작지점 - 반지름 
	m_ArrivePos.y = vPos.y;
	m_ArrivePos.z = vPos.z;




	vPos.x = m_attackStart;
	


	Vec3 vDir;
	vDir = (m_ArrivePos - vPos);
	vDir/=vDir.Length();
	//방향벡터 구하기 
	m_attackHypotenuseLenght = vDir.x;


	m_attackHypotenuseLenght = sqrt(m_ArrivePos.x*m_ArrivePos.x +vPos.y * vPos.y);
	GetObj()->Transform()->SetLocalPos(vPos);
}
