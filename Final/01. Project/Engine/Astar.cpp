#include "stdafx.h"
#include "Astar.h"
#include "PathResource.h"



const USHORT CAstar::GetHuristic(const tPathIdx& _dest, const int & _z, const int & _x) {
	USHORT pz = _dest.z - _z;
	USHORT px = _dest.x - _x;

	pz = (pz < 0) ? pz * -1 : pz;
	px = (px < 0) ? px * -1 : px;
	return (pz + px);
}

const USHORT CAstar::GetIncreaGraph(const E_ASTAR_DIR & _eDir) {
	UINT reValue = 0;
	switch (_eDir) {
	case E_ASTAR_DIR::LeftTop:
	case E_ASTAR_DIR::RightTop:
	case E_ASTAR_DIR::LeftBot:
	case E_ASTAR_DIR::RightBot:
		reValue = 14;
		break;
	case E_ASTAR_DIR::MidTop:
	case E_ASTAR_DIR::LeftMid:
	case E_ASTAR_DIR::RightMid:
	case E_ASTAR_DIR::MidBot:
		reValue = 10;
		break;
	case E_ASTAR_DIR::MidMid:
	case E_ASTAR_DIR::End:
		reValue = 0;
		break;
	}
	return reValue;
}

const E_ASTAR_DIR CAstar::GetDir(const float & _z, const float & _x) {
	if (_z == 0 && _x == 0)return E_ASTAR_DIR::LeftTop;
	else if (_z == 0 && _x == 1)return E_ASTAR_DIR::MidTop;
	else if (_z == 0 && _x == 2)return E_ASTAR_DIR::RightTop;
	else if (_z == 1 && _x == 0)return E_ASTAR_DIR::LeftMid;
	else if (_z == 1 && _x == 1)return E_ASTAR_DIR::MidMid;
	else if (_z == 1 && _x == 2)return E_ASTAR_DIR::RightMid;
	else if (_z == 2 && _x == 0)return E_ASTAR_DIR::LeftBot;
	else if (_z == 2 && _x == 1)return E_ASTAR_DIR::MidBot;
	else if (_z == 2 && _x == 2)return E_ASTAR_DIR::RightBot;
	else return E_ASTAR_DIR::End;
}

const USHORT CAstar::GetDirToGraph(const float & _z, const float & _x) {
	return GetIncreaGraph(GetDir(_z,_x));
}

CAstar::CAstar() {
}


CAstar::~CAstar() {
	CPathResource::ReleasePathRes(m_pColliderMap);
}

void CAstar::CreateColliderMap(const wstring & _wstrPath,
	const int & _zCnt, const int & _xCnt, const bool & _bInverse) {
	if (m_pColliderMap != nullptr)return;
	m_pColliderMap = CPathResource::CreatePathRes();
	m_pColliderMap->Init(_wstrPath, _zCnt, _xCnt, _bInverse);
}
void CAstar::SetColliderMap(CPathResource * _pRes) {
	if (m_pColliderMap == nullptr) {
		m_pColliderMap = _pRes->GetPathResource();
	}
	else {
		CPathResource::ReleasePathRes(m_pColliderMap);
		m_pColliderMap = _pRes->GetPathResource();
	}
}

tPathCoord * CAstar::GetPath(const tPathCoord& _start, const tPathCoord& _dest) {
	static int up = -1, down = 1, left = -1, right = 1;
	static const tPathIdx arrDir[3][3] = {
		 { {up,left}	,{ up, 0},	{up,right}}
		,{ {0,left}		,{  0, 0},	{ 0, right}}
		,{ {down,left}	,{down,0},	{down, right}}
	};
	CPathResource& p = *m_pColliderMap;
	m_listOpen.clear();
	m_listClose.clear();
	tPathIdx startCoord = { _start.z - m_tMapStart.z,_start.x - m_tMapStart.x };
	tPathIdx destCoord = { _dest.z - m_tMapStart.z,_dest.x - m_tMapStart.x };
	tPath*	 destPath = p.GetCoord(destCoord.z, destCoord.x);
	m_listOpen.push_back(p.GetCoord(startCoord.z, startCoord.x));

	tPath*	tileA = p.GetCoord(startCoord.z, startCoord.x);
	tPath*	tileB = nullptr;
	tPath*	temp = nullptr;

	bool bIsPathSearch = false;
	while (true) {
		for (auto i = m_listOpen.begin(); i != m_listOpen.end(); ++i) {
			if (temp != nullptr) {
				if (((*i)->g + (*i)->h) <= (temp->g + temp->h)){
					temp = *i;
				}
			}
			else {
				temp = *m_listOpen.begin();
			}
		}
		tileA = temp;
		temp = nullptr;
		m_listClose.push_back(tileA);
		if (m_listOpen.size() == 0)break;
		m_listOpen.remove(tileA);
		if ((tileA->z == destCoord.z) && (tileA->x == destCoord.x)) { bIsPathSearch = true; break; }
		int pz = 0, px = 0;
		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 3; ++j) {
				pz = tileA->z + arrDir[i][j].z;
				px = tileA->x + arrDir[i][j].x;
				tileB = p.GetCoord(pz, px);
				if (tileB == nullptr)continue;
				if (tileB->type == (BYTE)EC_PATH_TYPE::Block)continue;
				if (m_listClose.end() != find(m_listClose.begin(), m_listClose.end(), tileB))continue;
				if (m_listOpen.end() != find(m_listOpen.begin(), m_listOpen.end(), tileB))continue;
				tileB->pParnent = tileA;
				tileB->g = GetDirToGraph(i, j) + tileA->g;
				tileB->h = GetHuristic(destCoord, pz, px);
				tileB->parentToDir = (BYTE)GetDir(i, j);
				m_listOpen.push_back(tileB);
			}
		}
	}
	if (bIsPathSearch) {
		tPath* cur = *m_listClose.rbegin();
		int idx = 0;
		for (int i = 0; i < m_listClose.size(); ++i, ++idx) {
			m_vecPath.push_back({ (cur)->z - m_tMapStart.z,(cur)->x - m_tMapStart.x });
			cur = cur->pParnent;
			if (cur == nullptr)break;
		}
		tPathCoord* returnArray = new tPathCoord[m_vecPath.size()];
		memcpy(returnArray, m_vecPath.data(), sizeof(tPathCoord)*m_vecPath.size());
		m_vecPath.clear();

		return returnArray;
	}
	else {
		return nullptr;
	}
}

void CAstar::ResetPath(){


}
