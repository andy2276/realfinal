#include "stdafx.h"
#include "RoadingMgr.h"
#include "MeshData.h"
#include "Mesh.h"
#include "Transform.h"

#include "TriggerMaker.h"
#include "StatusComponent.h"
#include "FBXLoader.h"

#include "GameObject.h"

CRoadingMgr::CRoadingMgr()
{
}


CRoadingMgr::~CRoadingMgr()
{
	m_mapOrignObj.clear();
}

void CRoadingMgr::AddLoadingObj( CGameObject * _pObj)
{
	m_mapOrignObj.insert(make_pair(_pObj->GetName(), _pObj));
}

CGameObject * CRoadingMgr::GetOriginObj(const wstring & _name)
{
	CGameObject* pTarget = nullptr;
	map<wstring, CGameObject*>::iterator i = m_mapOrignObj.find(_name);
	pTarget = (*i).second;
	if (pTarget == nullptr) {
	}
	return pTarget;
}

void CRoadingMgr::AllDeActive()
{
	map<wstring, CGameObject*>::iterator itor = m_mapOrignObj.begin();
	for (; itor != m_mapOrignObj.end(); ++itor) {
		itor->second->SetActive(false);
	}
}

void CRoadingMgr::SetMonsterMeshData(EC_MOSTER_TYPE _type, CMeshData * _data)
{
	m_pMonsterMeshData[(UINT)_type] = _data;
}

void CRoadingMgr::SetBlackBoard(EC_MOSTER_TYPE _type, BT::CBlackBoard * _pBlack)
{
	m_pBlackBoard[(UINT)_type] = _pBlack;
}

CGameObject * CRoadingMgr::MonsterMaker(EC_MOSTER_TYPE _type, const Vec3 & _initPos)
{
	

	return nullptr;
}
