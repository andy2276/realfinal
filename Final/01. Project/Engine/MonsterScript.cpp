#include "stdafx.h"
#include "MonsterScript.h"

#include "StatusComponent.h"
#include "ParticleSystem.h"
CMonsterScript::CMonsterScript()
	: CScript((UINT)SCRIPT_TYPE::MONSTERSCRIPT)
	, m_iDir(1)
{
	
		m_effect = new CGameObject;
		m_effect->SetName(L"testDistortion");
		m_effect->AddComponent(new CTransform);
		m_effect->AddComponent(new CMeshRender);
		m_effect->Transform()->SetLocalScale(Vec3(5.f, 5.f, 5.f));
		m_effect->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
		//	pObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		m_effect->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));
	//	m_effect->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"FlameEffectEz4Mtrl"));

		Ptr<CTexture> explosion = CResMgr::GetInst()->Load<CTexture>(L"explosion", L"Texture\\explosion.jpg");
		m_effect->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, explosion.GetPointer());



		m_bOnce = true;
		m_bEffectOn = false;

		m_curTime = 0;
		m_coolTime = 0.8;
		m_effectScale = Vec3(5, 5,5);

		//hit-------------------------------------------------------------------------------
		m_hiteffect = new CGameObject;
		m_hiteffect->SetName(L"Particle");
		m_hiteffect->AddComponent(new CTransform);
		m_hiteffect->AddComponent(new CParticleSystem((UINT)PARTICLE_TYPE::BLOOD
		));
	
		m_hiteffect->FrustumCheck(false);
		//	m_meteor_particle->Transform()->SetLocalPos(Vec3(0.f, 50.f, 0.f));
	//	m_hiteffect->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
	//	m_hiteffect->Particlesystem()->setParent(GetObj());

		m_bhitOn = false;
		m_OnEffect = false;
}

CMonsterScript::~CMonsterScript()
{
}


void CMonsterScript::update()
{
	if (m_bhitOn)
	{
		

			m_curTime += DT;
		if (m_curTime >= m_coolTime)
		{
			m_curTime = 0;
			m_hiteffect->SetActive(false);
			m_bhitOn = false;
			m_OnEffect = true;
			DeleteObject(m_hiteffect);
		}
	}


		if (m_OnEffect)
		{
			//Vec3 objScale = Transform()->GetLocalScale();
			//Vec3 vpos = Transform()->GetLocalPos();
			//vpos.y += objScale.y / 2;
			//m_effect->Transform()->SetLocalPos(vpos);


			//Vec3 m_effectScale = m_effect->Transform()-> GetLocalScale();
			//m_effectScale.x += 0.5;
			//m_effectScale.y += 0.5;
			//m_effectScale.z += 0.5;

			//m_effect->Transform()->SetLocalScale(m_effectScale);


		
			//m_curTime += DT;
			//if (m_curTime >= m_coolTime)
			//{
			//	m_curTime = 0;
			//	m_OnEffect = false;
			//	m_effectScale = Vec3(5, 5, 5);
			//	m_effect->Transform()->SetLocalScale(m_effectScale);
			//	m_effect->SetActive(false);
			//	m_bEffectOn = false;

			//}
		}
	

	if (m_bOnce)
	{
		CreateObject(m_effect, L"Monster");
		m_effect->SetActive(false);
		m_bOnce = false;

		m_hiteffect->SetActive(false);
		CreateObject(m_hiteffect, L"Monster");
	}
}

void CMonsterScript::OnCollisionEnter(CCollider2D * _pOther)
{

		// 충돌이 발생하고, 상대 물체가 총일이면 스스로를 삭제
	if (L"Bullet" == _pOther->GetObj()->GetName())
	{
		if (!m_OnEffect) {

		
		if (!m_bhitOn)
		{
			m_bhitOn = true;
			m_hiteffect->SetActive(true);
			Vec3 vpos = Transform()->GetLocalPos();
			Vec3 objScale = Transform()->GetLocalScale();
			//	vpos.y += objScale.y*;
		//		vpos.y += 10;
			Vec3 bulletpos = _pOther->GetObj()->Transform()->GetLocalPos();
			vpos.y = bulletpos.y;
			m_hiteffect->Transform()->SetLocalPos(bulletpos);
		}
		}
	}
	

}

void CMonsterScript::OnCollisionExit(CCollider2D * _pOther)
{	
}
