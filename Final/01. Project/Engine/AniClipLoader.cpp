#include "stdafx.h"
#include "AniClipLoader.h"


CAniClipLoader::CAniClipLoader()
{
}


CAniClipLoader::~CAniClipLoader()
{
}

void CAniClipLoader::SetAniClip(const EC_ANI_CLIP & clip, CMesh * _pMesh, tMTAnimClip * data)
{
	if (m_arrpAniClips[(UINT)clip] != nullptr)delete m_arrpAniClips[(UINT)clip];
	m_arrpAniClips[(UINT)clip] = data;
}


