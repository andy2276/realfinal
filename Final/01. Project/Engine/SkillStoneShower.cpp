#include "stdafx.h"
#include "SkillStoneShower.h"
#include "SkillElementDir.h"
#include "Collider2D.h"
#include "MapMgr.h"
#include "SkillEvent.h"
#include "SceneMgr.h"


CSkillStoneShower::CSkillStoneShower()
	:CScript((UINT)SCRIPT_TYPE::SKILLSCRIPT)
	, m_nCurBullet(0)
	, m_bInit(false)
{
}


CSkillStoneShower::~CSkillStoneShower()
{
}

void CSkillStoneShower::Init(CScene* _pScene, const wstring& _layer)
{
	m_nBulletCnt = 3;

	m_pCurScene = _pScene;
	m_wstrLayer = _layer;

	m_vecBullet.reserve(m_nBulletCnt);
	m_vecBulletScript.reserve(m_nBulletCnt);

	Ptr<CMeshData> stone = CMapMgr::GetInst()->GetBulletData((UINT)EC_MONSTER_BULLET::Stone);
	CGameObject* pObj = nullptr;
	CSkillElementDir* element = nullptr;
	tSkillElementValue v = {};
	v.Endtypes.fEndDist = 100.f;
	v.PosValues.vOffsetPos = Vec3(0.0f, 0.f, 0.0f);
	v.Others.fDamage = 15.f;
	v.MoveValues.fMoveSpeed = 100.f;
	UINT type = 
		(
			  (UINT)EC_SKILL_ELEMENT::EndType_Dist
			| (UINT)EC_SKILL_ELEMENT::EndType_Col
			| (UINT)EC_SKILL_ELEMENT::MoveType_Straight
			| (UINT)EC_SKILL_ELEMENT::ColType_Damage
			| (UINT)EC_SKILL_ELEMENT::EventType_StartEffect
		);
	//MonsterBullet
	for (int i = 0; i < m_nBulletCnt; ++i)
	{
		pObj = stone->Instantiate();
		pObj->AddComponent(new CCollider2D);
		pObj->Collider2D()->SetOffsetScale(Vec3(2.0f, 2.9f, 2.0f));
		element = new CSkillElementDir;
		
		pObj->AddComponent(element);
		element->SetTypes(type);
		element->SetValues(v);
		m_vecBullet.push_back(pObj);
		m_vecBulletScript.push_back(element);
		m_pCurScene->AddGameObject(m_wstrLayer, pObj, false);
	}
	m_bInit = true;
}

void CSkillStoneShower::Shoot()
{
	for (int i = 0; i < m_nBulletCnt; ++i)
	{
		if (!m_vecBullet[i]->IsActive())
		{
			m_vecBulletScript[i]->SetStartPos(Transform()->GetLocalPos());
			m_vecBulletScript[i]->SetMoveDir(Transform()->GetLocalDir(DIR_TYPE::FRONT));
			m_vecBullet[i]->SetActive(true);
			break;
		}
	}

}

void CSkillStoneShower::Shoot(const Vec3 & _pos, const Vec3 _rot, const Vec3 & _dir)
{
	CGameObject* pObj = nullptr;
	for (int i = 0; i < m_nBulletCnt; ++i)
	{
		if (m_vecBullet[i] != nullptr)
		{
			if (!m_vecBullet[i]->IsActive())
			{
				m_vecBulletScript[i]->InitShoot(_pos, _rot);
				m_vecBulletScript[i]->SetMoveDir(_dir);
				m_vecBulletScript[i]->SetAlive(true);
				m_vecBullet[i]->SetActive(true);

				tSkillEventValue v = {};
				v.a_vStartPos = _pos;
				v.a_vStartPos.y += 4.f;
				v.a_vStratRot = Vec3(-90.f * XM_ANGLE, 0.0f, 0.0f);
				//	시작 셋팅을 설정
				v.d_vRotSpeed = Vec3(0.0f, 10.f* XM_ANGLE, 0.0f);
				v.c_pTarget = m_vecBullet[i];

				if ((UINT)EC_SKILL_ELEMENT::EventType_StartEffect & m_vecBulletScript[i]->GetTypes())
				{
					if (m_vecBulletScript[i]->HaveStartEffect())
					{
						m_vecBulletScript[i]->ResetStartEffect(v);
						m_vecBulletScript[i]->OnStartEffect();
					}
					else
					{
						m_vecBulletScript[i]->Option_StartEvent(3.f);
						pObj = m_vecBulletScript[i]->CreateEffect
						(
							0
							, 0
							, (UINT)EC_SKILL_EVENT::MeshType_Rect
							| (UINT)EC_SKILL_EVENT::ActionType_Rotate
							, v

						);
						if (pObj != nullptr)CSceneMgr::GetInst()->GetCurScene()->AddGameObject(L"Particle", pObj, false);
					}
				}

				break;
			}
		}
		else
		{

		}
	}
}

void CSkillStoneShower::update()
{
	if (m_bInit)
	{
		for (int i = 0; i < m_nBulletCnt; ++i)
		{
			m_vecBullet[i]->SetActive(false);
		}
		m_bInit = false;
	}
}

void CSkillStoneShower::CreateStartEffect()
{



}
