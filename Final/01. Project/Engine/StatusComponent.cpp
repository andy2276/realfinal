#include "stdafx.h"
#include "StatusComponent.h"

#include "TimeMgr.h"

#include "Transform.h"
#include "MeshRender.h"
#include "ResMgr.h"
#include "SceneMgr.h"
#include"Scene.h"
#include"Layer.h"
#include"RenderMgr.h"
#include"KeyMgr.h"
#include "MapMgr.h"
#include "Camera.h"
CStatusComponent::CStatusComponent()
	:CComponent(COMPONENT_TYPE::STATUS)
{
	
	temp = true;
	boN = false;
	m_bAIUpdate = true;
	m_bPlayerDeath = false;
	m_fValue[(UINT)STATUS_FLOAT::HP] = 100.0f;
	m_fValue[(UINT)STATUS_FLOAT::MP] = 100.0f;
	m_fValue[(UINT)STATUS_FLOAT::MASS] = 10.0f;
	m_fValue[(UINT)STATUS_FLOAT::SEARCHRANGE] = 100.0f;		//	여기수정
	m_fValue[(UINT)STATUS_FLOAT::ATTACKRANGE] = 8.0f;
	m_fValue[(UINT)STATUS_FLOAT::ROTSPEED] = 50.0f * XM_ANGLE;
	m_fValue[(UINT)STATUS_FLOAT::MOVESPEED] = 50.0f;
	m_fValue[(UINT)STATUS_FLOAT::MINRANGE] = 4.0f;

	m_fValue[(UINT)STATUS_FLOAT::SKILL_1_COOL_MAX] = 4.0f;
	m_fValue[(UINT)STATUS_FLOAT::SKILL_1_COOL_NOW] = 0.0f;

	m_fValue[(UINT)STATUS_FLOAT::SKILL_2_COOL_MAX] = 10.0f;
	m_fValue[(UINT)STATUS_FLOAT::SKILL_2_COOL_NOW] = 0.0f;

	m_fValue[(UINT)STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX] = 2.0f;
	m_fValue[(UINT)STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW] = 0.0f;
	m_fValue[(UINT)STATUS_FLOAT::TIME_ACCEL] = 1.0f;

	m_vec3Value[(UINT)STATUS_VEC3::VELOCITY] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::ACCELERATION] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::FRICTION] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::LOCAL_SCALE] = Vec3(100.f, 100.f, 100.f); //Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::LOCAL_ROT] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS_OFFSET] = Vec3(0.f, 0.f, 0.f);


	m_characterUI = new CGameObject;
	m_characterUI->SetName(L"HP UI");
	m_characterUI->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	m_characterUI->AddComponent(new CTransform);
	m_characterUI->AddComponent(new CMeshRender);

	m_HpUI = new CGameObject;
	m_HpUI->SetName(L"HP UI BORDER");
	m_HpUI->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	m_HpUI->AddComponent(new CTransform);
	m_HpUI->AddComponent(new CMeshRender);
	

	m_BlackHpUI = new CGameObject;
	m_BlackHpUI->SetName(L"HP BLACK UI");
	m_BlackHpUI->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
	m_BlackHpUI->AddComponent(new CTransform);
	m_BlackHpUI->AddComponent(new CMeshRender);

	for (int i = 0; i < (UINT)PLAYER_SKILL_TYPE::END; ++i)
	{
		isOwnSkill[i] = false;
	}

	m_curOwnSkillCount = 0;
	for (int i = 0; i < m_totalSkillCount; ++i)
	{
		m_ArrOwnSkill[i] = -1;
		// -1이면 스킬을 가지고 있지 않다는 뜻이며, 
		// m_curOwnSkillCount 가 현재 가지고있는 스킬의 총 개수이다 
		// 이 배열이 가지고 있는 int 값은 enum class의 타입중 하나이다.
	}
	m_bLowHp = false;

	//-----------game over ui -----------------------------
	m_gameoverUI = new CGameObject;
	m_gameoverUI->SetName(L"Z UI_State");
	m_gameoverUI->FrustumCheck(false);

	// Transform 설정
	m_gameoverUI->AddComponent(new CTransform);
	m_gameoverUI->Transform()->setPhysics(false); //true값이 움직일수 있게 하는것. 
	Vec3 vPos(0, 0, 1.f);
	m_gameoverUI->Transform()->SetLocalPos(vPos);

	Vec3 vScale_monster(600.f, 300.f, 1.f);
	m_gameoverUI->Transform()->SetLocalScale(vScale_monster);

	m_gameoverUI->AddComponent(new CMeshRender);
	m_gameoverUI->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

	//	material

	Ptr<CTexture> pZtexture = CResMgr::GetInst()->FindRes< CTexture>(L"gameover");
	if (pZtexture.GetPointer() == nullptr)
	{
		pZtexture = CResMgr::GetInst()->Load<CTexture>(L"gameover", L"Texture\\gameover.png");
	}

	Ptr<CMaterial> pZUIMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"zMtrl");
	if (pZUIMtrl.GetPointer() == nullptr)
	{
		
		pZUIMtrl = new CMaterial;
		pZUIMtrl->DisableFileSave();
		pZUIMtrl->SetShader(CResMgr::GetInst()->FindRes<CShader>(L"TexShader"));
		CResMgr::GetInst()->AddRes(L"zMtrl", pZUIMtrl);
		pZUIMtrl->SetData(SHADER_PARAM::TEX_0,pZtexture.GetPointer());
	}

	m_gameoverUI->MeshRender()->SetMaterial(pZUIMtrl);
	
	//	m_ItemUI->disable();

	CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_gameoverUI);
	m_bGOVERcontrol = true;
	//if (GetObj()->GetName() == L"character")
	//{
	//	m_bIsPlayer = true;
	//}
	//else {
	//	m_bIsPlayer = false;
	//}
	m_bIsPlayer = false;
	pSound = nullptr;
	m_bInit = true;
}


CStatusComponent::~CStatusComponent()
{
}

void CStatusComponent::update()
{
	if (m_bGOVERcontrol)
	{
		m_gameoverUI->SetActive(false);
		m_bGOVERcontrol = false;
	}
	if (m_bIsPlayer)
	{
		if (m_fValue[(UINT)STATUS_FLOAT::HP] <= 0)
		{
			m_gameoverUI->SetActive(true);
		}
	}


	//	HP,
	//	MP,
	//	MASS,
	//	SEARCHRANGE,
	//	ATTACKRANGE,
	//	ROTSPEED,
	//	SKILL_1_COOL_MAX,
	//	SKILL_1_COOL_NOW,
	//	SKILL_2_COOL_MAX,
	//	SKILL_2_COOL_NOW,
	//	SKILL_HEAL_COOL_MAX,
	//	SKILL_HEAL_COOL_NOW,
	//	ATTACK_BAGIC_COOL_NOW,
	//	ATTACK_BAGIC_COOL_MAX,
	//	||-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_||
	//		|가	|나	|다	|라	|	|	|	|	|	|	|	|	|	|	|	|	|	|	|	|	|	|	|	|	|	|	|	|	|	||		
	//	||-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_||
	//							
	//if ((UINT)STATUS_FLOAT::HP < 0.0f) {
	//	GetObj()->SetActive(false);
	//}
	//if (m_fValue[(UINT)STATUS_FLOAT::SKILL_1_COOL_NOW] < m_fValue[(UINT)STATUS_FLOAT::SKILL_1_COOL_MAX]) {
	//	m_fValue[(UINT)STATUS_FLOAT::SKILL_1_COOL_NOW] += DT;
	//}
	//if (m_fValue[(UINT)STATUS_FLOAT::SKILL_2_COOL_NOW] < m_fValue[(UINT)STATUS_FLOAT::SKILL_2_COOL_MAX]) {
	//	m_fValue[(UINT)STATUS_FLOAT::SKILL_2_COOL_NOW] += DT;
	//}
	//if (m_fValue[(UINT)STATUS_FLOAT::SKILL_HEAL_COOL_NOW] < m_fValue[(UINT)STATUS_FLOAT::SKILL_HEAL_COOL_MAX]) {
	//	m_fValue[(UINT)STATUS_FLOAT::SKILL_HEAL_COOL_NOW] += DT;
	//}
	//if (m_fValue[(UINT)STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW] < m_fValue[(UINT)STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX]) {
	//	m_fValue[(UINT)STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW] += DT;
	//}


	


	if (m_eMonsterType == MONSTER_TYPE::Mimic)
	{
		//setHpDown(10);
		//몬스터의 위치를 받아와야함 
		Vec3 vPos = Transform()->GetLocalPos();
		Vec3 vPos2 = Transform()->GetLocalPos();

		vPos.y += 15;
		Vec3 vMonsterScale = Transform()->GetLocalScale();


		 vPos.x = vPos.x - (vMonsterScale.x*2.5) + (m_vec3Value[(UINT)STATUS_VEC3::UI_HP_SCALE].x / 2);
		
		m_HpUI->Transform()->SetLocalScale(m_vec3Value[(UINT)STATUS_VEC3::UI_HP_SCALE]);
		m_HpUI->Transform()->SetLocalPos(vPos);
		
		vPos2.x = vPos2.x - (vMonsterScale.x*2.5) + (m_vec3Value[(UINT)STATUS_VEC3::UI_BLACK_HP_SCALE].x / 2) + (m_vec3Value[(UINT)STATUS_VEC3::UI_HP_SCALE].x );
		vPos2.y += 15;

	/*	m_BlackHpUI->Transform()->SetLocalScale(m_vec3Value[(UINT)STATUS_VEC3::UI_BLACK_HP_SCALE]);
		m_BlackHpUI->Transform()->SetLocalPos(vPos2);

		*/


		//	빌보드
		Vec3 camPos = CMapMgr::GetInst()->GetMainCam()->Transform()->GetWorldPos();
		Vec3 hpPos = m_HpUI->Transform()->GetLocalPos();


		Vec3 lookPos = hpPos - camPos;
		float fAngle = atan2(lookPos.x, lookPos.z);


		Vec3 hpRot = m_HpUI->Transform()->GetLocalRot();
		hpRot.y = fAngle;

		m_HpUI->Transform()->SetLocalRot(hpRot);

		//빌보드 2 
		// camPos = CMapMgr::GetInst()->GetMainCam()->Transform()->GetWorldPos();
		// Vec3 hpPos2 = m_BlackHpUI->Transform()->GetLocalPos();


		// lookPos = hpPos2 - camPos;
		// fAngle = atan2(lookPos.x, lookPos.z);


		// Vec3 hpRot2 = m_BlackHpUI->Transform()->GetLocalRot();
		//hpRot2.y = fAngle;

		//m_BlackHpUI->Transform()->SetLocalRot(hpRot2);

	}


}

void CStatusComponent::Collision(int _colType, CGameObject * _pCollisionObj, const Vec3 & _vCollisionDir, float _pDamage)
{
}

void CStatusComponent::AddForce(float x, float y, float z, float time)
{
	//들어온 힘의 양으로 가속도를 계산하고, 그 가속도를 이용해 속도를 계산한다. 

	float accX = x / m_fValue[(UINT)STATUS_FLOAT::MASS];
	float accY = y / m_fValue[(UINT)STATUS_FLOAT::MASS];
	float accZ = z / m_fValue[(UINT)STATUS_FLOAT::MASS];

	

	
	m_vec3Value[(UINT)STATUS_VEC3::VELOCITY].x = m_vec3Value[(UINT)STATUS_VEC3::VELOCITY].x + accX * DT;
	m_vec3Value[(UINT)STATUS_VEC3::VELOCITY].y = m_vec3Value[(UINT)STATUS_VEC3::VELOCITY].y + accY * DT;
	m_vec3Value[(UINT)STATUS_VEC3::VELOCITY].z = m_vec3Value[(UINT)STATUS_VEC3::VELOCITY].z + accZ * DT;

	//가속도  = 힘/ 질량

}

void CStatusComponent::AddForce(const Vec3 & _dir, const float & _time)
{
	Vec3 acc = _dir / m_vec3Value[(UINT)STATUS_FLOAT::MASS];

	m_vec3Value[(UINT)STATUS_VEC3::VELOCITY] = m_vec3Value[(UINT)STATUS_VEC3::VELOCITY] * (acc * DT);

}

void CStatusComponent::setVelocity(float x, float y, float z)
{
	m_vec3Value[(UINT)STATUS_VEC3::VELOCITY].x = x;
	m_vec3Value[(UINT)STATUS_VEC3::VELOCITY].y = y;
	m_vec3Value[(UINT)STATUS_VEC3::VELOCITY].z = z;

}

void CStatusComponent::setHpDown(float x,bool reset)
{

	if (reset) {
		m_fValue[(UINT)STATUS_FLOAT::HP] = m_fValue[(UINT)STATUS_FLOAT::MAX_HP];
	}
	else {
		m_fValue[(UINT)STATUS_FLOAT::HP] -= x;

	}
	switch (m_eMonsterType)
	{
	case MONSTER_TYPE::Player:
	{	
	//hpdown 함수가 호출되면 하는일은 다음과 같다. 
	// 1. 인자로 넘어온 값 만큼 값을 감소시킨다. 
	// 2. UI의 크기와 위치가  줄어들게 해준다 
	//		크기는 줄어든 값만큼 줄어들고, 위치는 줄어든값의 1/2만큼 감소한다. 
		int hp = (int)((185 * m_fValue[(UINT)STATUS_FLOAT::HP]) / m_fValue[(UINT)STATUS_FLOAT::MAX_HP]);


	//	hp = 100;
		m_vec3Value[(UINT)STATUS_VEC3::UI_HP_SCALE].x = hp;

		
		m_HpUI->Transform()->SetLocalScale(m_vec3Value[(UINT)STATUS_VEC3::UI_HP_SCALE]);


		m_vec3Value[(UINT)STATUS_VEC3::BORDER_POS];
	//	m_HpUI->Transform()->SetLocalPos(Vec3(0, 0, 0));
		m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS].x = m_vec3Value[(UINT)STATUS_VEC3::BORDER_POS].x + hp / 2;
		m_HpUI->Transform()->SetLocalPos(m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS]);
	

		if (m_fValue[(UINT)STATUS_FLOAT::HP] == 0)
			m_vec3Value[(UINT)STATUS_VEC3::UI_HP_SCALE].x = 0;
		if(pSound!=nullptr)
		pSound->Play(1, 1, true);
	}
		break;
	case MONSTER_TYPE::Stone:
		break;
	case MONSTER_TYPE::Skell:
		break;
	case MONSTER_TYPE::Mimic:
	{		
		//정수에서 비율로 줄어들게 만든다. ;;.
			
		// 100 : 80 = 6 : x  <- 비례식 이용 
		// 6이 원래 x길이 
		int hp = (int)((6 * m_fValue[(UINT)STATUS_FLOAT::HP]) / m_fValue[(UINT)STATUS_FLOAT::MAX_HP]);
		
		m_vec3Value[(UINT)STATUS_VEC3::UI_HP_SCALE].x = hp;
		
		if (m_fValue[(UINT)STATUS_FLOAT::HP] == 0)
			m_vec3Value[(UINT)STATUS_VEC3::UI_HP_SCALE].x = 0;

		int hp2 = (int)((6 * (m_fValue[(UINT)STATUS_FLOAT::MAX_HP] - m_fValue[(UINT)STATUS_FLOAT::HP])) / m_fValue[(UINT)STATUS_FLOAT::MAX_HP]);

		m_vec3Value[(UINT)STATUS_VEC3::UI_BLACK_HP_SCALE].x = hp2;
	}
		break;
	default:
		break;
	}
	
}

void CStatusComponent::SetStone(const Vec3 & _pos, const Vec3 & _rot)
{
	m_bAIUpdate = true;
	m_bLowHp = false;
	m_fValue[(UINT)STATUS_FLOAT::HP] = 100.0f;
	m_fValue[(UINT)STATUS_FLOAT::MAX_HP] = 100.0f;
	m_fValue[(UINT)STATUS_FLOAT::MP] = 100.0f;
	m_fValue[(UINT)STATUS_FLOAT::MASS] = 10.0f;
	m_fValue[(UINT)STATUS_FLOAT::SEARCHRANGE] = 300.0f;		//	여기수정
	m_fValue[(UINT)STATUS_FLOAT::ATTACKRANGE] = 15.0f;
	m_fValue[(UINT)STATUS_FLOAT::ROTSPEED] = 90.0f * XM_ANGLE;
	m_fValue[(UINT)STATUS_FLOAT::MOVESPEED] = 50.0f;
	m_fValue[(UINT)STATUS_FLOAT::MINRANGE] = 3.f;

	m_fValue[(UINT)STATUS_FLOAT::SKILL_1_COOL_MAX] = 4.0f;
	m_fValue[(UINT)STATUS_FLOAT::SKILL_1_COOL_NOW] = 0.0f;

	m_fValue[(UINT)STATUS_FLOAT::SKILL_2_COOL_MAX] = 10.0f;
	m_fValue[(UINT)STATUS_FLOAT::SKILL_2_COOL_NOW] = 0.0f;

	m_fValue[(UINT)STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX] = ANIMFRAME(40.f);
	m_fValue[(UINT)STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW] = ANIMFRAME(40.f);


	m_vec3Value[(UINT)STATUS_VEC3::VELOCITY] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::ACCELERATION] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::FRICTION] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS] = _pos;
	m_vec3Value[(UINT)STATUS_VEC3::LOCAL_ROT] = _rot;
	m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS_OFFSET] = Vec3(0.f, 0.f, 0.f);

	OnHpUies();
}

void CStatusComponent::SetSkelleton(const Vec3 & _pos, const Vec3 & _rot)
{
	m_bAIUpdate = true;
	m_bLowHp = false;
	m_fValue[(UINT)STATUS_FLOAT::HP] = 150.0f;
	m_fValue[(UINT)STATUS_FLOAT::MAX_HP] = 150.0f;
	m_fValue[(UINT)STATUS_FLOAT::MP] = 100.0f;
	m_fValue[(UINT)STATUS_FLOAT::MASS] = 10.0f;
	m_fValue[(UINT)STATUS_FLOAT::SEARCHRANGE] = 300.0f;		//	여기수정
	m_fValue[(UINT)STATUS_FLOAT::ATTACKRANGE] = 15.0f;
	m_fValue[(UINT)STATUS_FLOAT::ROTSPEED] = 90.0f * XM_ANGLE;
	m_fValue[(UINT)STATUS_FLOAT::MOVESPEED] = 30.0f;
	m_fValue[(UINT)STATUS_FLOAT::MINRANGE] = 3.0f;

	m_fValue[(UINT)STATUS_FLOAT::SKILL_1_COOL_MAX] = 15.0f;
	m_fValue[(UINT)STATUS_FLOAT::SKILL_1_COOL_NOW] = 0.0f;

	m_fValue[(UINT)STATUS_FLOAT::SKILL_2_COOL_MAX] = 30.0f;
	m_fValue[(UINT)STATUS_FLOAT::SKILL_2_COOL_NOW] = 0.0f;

	m_fValue[(UINT)STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX] = 2.0f;
	m_fValue[(UINT)STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW] = 2.0f;


	m_vec3Value[(UINT)STATUS_VEC3::VELOCITY] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::ACCELERATION] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::FRICTION] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS] = _pos;
	m_vec3Value[(UINT)STATUS_VEC3::LOCAL_ROT] = _rot;
	m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS_OFFSET] = Vec3(0.f, 0.f, 0.f);

	OnHpUies();
}

void CStatusComponent::SetMimic(const Vec3 & _pos, const Vec3 & _rot)
{
	m_bLowHp = false;
	m_bAIUpdate = true;
	m_fValue[(UINT)STATUS_FLOAT::HP] = 100.0f;
	m_fValue[(UINT)STATUS_FLOAT::MAX_HP] = 100.0f;
	m_fValue[(UINT)STATUS_FLOAT::MP] = 100.0f;
	m_fValue[(UINT)STATUS_FLOAT::MASS] = 10.0f;
	m_fValue[(UINT)STATUS_FLOAT::SEARCHRANGE] = 300.0f;		//	여기수정
	m_fValue[(UINT)STATUS_FLOAT::ATTACKRANGE] = 15.0f;
	m_fValue[(UINT)STATUS_FLOAT::ROTSPEED] = 90.0f * XM_ANGLE;
	m_fValue[(UINT)STATUS_FLOAT::MOVESPEED] = 50.0f;
	m_fValue[(UINT)STATUS_FLOAT::MINRANGE] = 4.0f;

	m_fValue[(UINT)STATUS_FLOAT::SKILL_1_COOL_MAX] = 4.0f;
	m_fValue[(UINT)STATUS_FLOAT::SKILL_1_COOL_NOW] = 0.0f;

	m_fValue[(UINT)STATUS_FLOAT::SKILL_2_COOL_MAX] = 10.0f;
	m_fValue[(UINT)STATUS_FLOAT::SKILL_2_COOL_NOW] = 0.0f;

	m_fValue[(UINT)STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX] = 2.0f;
	m_fValue[(UINT)STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW] = 0.0f;


	m_vec3Value[(UINT)STATUS_VEC3::VELOCITY] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::ACCELERATION] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::FRICTION] = Vec3(0.f, 0.f, 0.f);
	m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS] = _pos;
	
	m_vec3Value[(UINT)STATUS_VEC3::LOCAL_ROT] = _rot;
	m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS_OFFSET] = Vec3(0.f, 0.f, 0.f);

	OnHpUies();
}

void CStatusComponent::DamageStack(UINT ID, const Vec3 & _dir, float _damage , UINT _state)
{
	//	중복을 제거하게 했는데도 중복되는거는 오브젝트가 2개가 동시에 날라간다는거임.
	std::list<tDamageStack>::reverse_iterator rItor = m_tDamageStack.rbegin();
	std::list<tDamageStack>::reverse_iterator rEnd = m_tDamageStack.rend();
	while (rItor != rEnd)
	{
		if (ID == (*rItor).nBulletID)return;
		++rItor;
	}
	m_tDamageStack.push_back(tDamageStack{ ID, _dir, _damage,_state });
}

void CStatusComponent::SetHpUI(UINT u)
{

//	m_fValue[(UINT)STATUS_FLOAT::MAX_HP] = 100.0f;

	// hp bar에 필요한것은 두가지 이다. 
	// 1. bar border (테두리)
	// 2. border의 사이즈가 hp에따라서 실시간으로 변경되어야 한다. 

	


	tResolution	res = CRenderMgr::GetInst()->GetResolution();
	Vec3 vScale_character(150, 150.f, 1.f);
	Ptr<CMaterial> pMtrl;
	Ptr<CMaterial> pMtrl2;

	Ptr<CTexture> pTex;
	Ptr<CTexture> pTex2;

	Ptr<CTexture> hpTex;
	Ptr<CMaterial> hpMtrl;


	switch (u)
	{
	case (UINT)MONSTER_TYPE::Player:
	{
		m_fValue[(UINT)STATUS_FLOAT::MAX_HP] = 100.0f;
		pSound = CResMgr::GetInst()->Load<CSound>(L"Hit", L"Sound\\Hit.mp3");
		m_eMonsterType = MONSTER_TYPE::Player;
		m_characterUI->Transform()->setPhysics(false);

		Vec3 vScale_character(350, 180.f, 1.01f);
		m_characterUI->Transform()->SetLocalScale(vScale_character);
		m_characterUI->Transform()->SetLocalPos(Vec3((-res.fWidth / 2.f) + 20 + (vScale_character.x / 2.f), +(res.fHeight / 2.f) - (vScale_character.y / 2.f), 1.01f));

		//	m_HpUI->Transform()->SetLocalRot(Vec3(XM_ANGLE * 90, 0, 0)); //x축 90도 회전 

		m_characterUI->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

		pTex = CResMgr::GetInst()->FindRes< CTexture>(L"characteruiTex");
		if(pTex.GetPointer() == nullptr)
		{
			pTex = CResMgr::GetInst()->Load<CTexture>(L"characteruiTex", L"Texture\\character06.png");
		}
		pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"characteruiMtrl"); 
		if (pMtrl.GetPointer() == nullptr)
		{
			pMtrl = new CMaterial;
			pMtrl->DisableFileSave();
			pMtrl->SetShader(CResMgr::GetInst()->FindRes<CShader>(L"TexShader"));
			CResMgr::GetInst()->AddRes(L"characteruiMtrl", pMtrl);
			pMtrl->SetData(SHADER_PARAM::TEX_0, pTex.GetPointer());

		}
		m_characterUI->MeshRender()->SetMaterial(pMtrl);


		CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_characterUI);

		//------------------------------------------------------------------------------------
		// hp border / 캐릭터 체력 연동된 실제 hp bar 유아이 
		//------------------------------------------------------------------------------------

		m_HpUI->SetName(L"State HP UI BORDER");
		m_HpUI->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
		m_HpUI->AddComponent(new CTransform);
		m_HpUI->AddComponent(new CMeshRender);
		m_HpUI->Transform()->setPhysics(false);

		Vec3 vScale_hpbar(350.f / 2.f + 10.f, 150.f / 4.f - 4.f, 1.1f);
		Vec3 vPos((-res.fWidth / 2.f) + (vScale_character.x / 2.f) + 85, +24 + (res.fHeight / 2.f) - (vScale_character.y / 2.f), 1.001f);

		m_vec3Value[(UINT)STATUS_VEC3::BORDER_POS] = vPos;
		m_vec3Value[(UINT)STATUS_VEC3::BORDER_POS].x -= vScale_hpbar.x / 2;

		m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS] = vPos;
		m_HpUI->Transform()->SetLocalPos(vPos);
		m_vec3Value[(UINT)STATUS_VEC3::UI_HP_SCALE] = vScale_hpbar;
		m_HpUI->Transform()->SetLocalScale(vScale_hpbar);
		
		
		
			m_bInit = false;
			m_HpUI->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

			hpTex = CResMgr::GetInst()->FindRes< CTexture>(L"hpUITex");
			if (hpTex.GetPointer() == nullptr)
			{
				hpTex = CResMgr::GetInst()->Load<CTexture>(L"hpUITex", L"Texture\\hp.png");
			}
			hpMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"hpUIMtrl");
			if (hpMtrl.GetPointer() == nullptr)
			{
				hpMtrl = new CMaterial;
				hpMtrl->DisableFileSave();
				hpMtrl->SetShader(CResMgr::GetInst()->FindRes<CShader>(L"TexShader"));
				CResMgr::GetInst()->AddRes(L"hpUIMtrl", hpMtrl);
				hpMtrl->SetData(SHADER_PARAM::TEX_0, hpTex.GetPointer());
			}
			m_HpUI->MeshRender()->SetMaterial(hpMtrl);
			CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_HpUI);
		
		//------------------------------------------------------------------------------------


	}
	break;

	case (UINT)MONSTER_TYPE::Mimic:
	{
		m_eMonsterType = MONSTER_TYPE::Mimic;

		//   m_characterUI->SetName(L"Monster Hp");
		//   m_characterUI->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
		//   m_characterUI->AddComponent(new CTransform);
		//   m_characterUI->AddComponent(new CMeshRender);
		//   m_characterUI->Transform()->setPhysics(false);
		//   
		//   
		//   Vec3 vPos = Transform()->GetLocalPos();
		//   // vPos.y += 15;
		//   // vPos.z= 1.001;
		//   
		//   m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS] = vPos;
		//   m_characterUI->Transform()->SetLocalPos(m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS]);
		//   
		//   
		//   //Vec3 vScale_Monster(100/10/2, 10.f/10/2, 1.0001f);
		//   Vec3 vScale_Monster(25, 5.f, 1.0001f);
		//   
		//   m_vec3Value[(UINT)STATUS_VEC3::LOCAL_SCALE] = vScale_Monster;
		//   m_characterUI->Transform()->SetLocalScale(m_vec3Value[(UINT)STATUS_VEC3::LOCAL_SCALE]);
		//   
		//   
		//   m_characterUI->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		//   Ptr<CMaterial> pMonsterHpMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"); //빌보드 
		//   CMaterial* p = pMonsterHpMtrl->Clone();
		//   m_characterUI->MeshRender()->SetMaterial(p);
		//   
		//   //	m_HpUI->MeshRender()->SetMaterial(pMonsterHpMtrl->Clone());
		//   //	pMonsterHpMtrl->SetData()
		//   Ptr<CTexture> ptemp = CResMgr::GetInst()->Load<CTexture>(L"Border", L"Texture\\Border.png");
		//   //		Ptr<CTexture> pHpBorder = CResMgr::GetInst()->Load<CTexture>(L"hpborder", L"Texture\\hpborder.png");
		//   
		//   m_characterUI->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, ptemp.GetPointer());
		//   
		//   CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"Monster")->AddGameObject(m_characterUI);

		//------------------------------------------------------------------------------------
		// hp ui / 몬스터 체력바 
		//------------------------------------------------------------------------------------

		m_HpUI->SetName(L"State Monster Hp UI");
		m_HpUI->FrustumCheck(false);	// 절두체 컬링 사용하지 않음
		m_HpUI->AddComponent(new CTransform);
		m_HpUI->AddComponent(new CMeshRender);
		m_HpUI->Transform()->setPhysics(false);


		//위치 설정
		Vec3 vPos_hpUi = GetObj()->Transform()->GetLocalPos();
		vPos_hpUi.y += 15;
		m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS] = vPos_hpUi;
		m_HpUI->Transform()->SetLocalPos(vPos_hpUi);

		//크기 설정
		Vec3 vScale_MonsterHPUI(6, 0.8, 1.1f);
		m_vec3Value[(UINT)STATUS_VEC3::UI_HP_SCALE] = vScale_MonsterHPUI;
		m_HpUI->Transform()->SetLocalScale(vScale_MonsterHPUI);


		//	메테리얼
		
		
			m_HpUI->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

			hpTex = CResMgr::GetInst()->FindRes< CTexture>(L"hpUITex");
			if (hpTex.GetPointer() == nullptr)
			{
				hpTex = CResMgr::GetInst()->Load<CTexture>(L"hpUITex", L"Texture\\hp.png");
			}
			hpMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"hpUIMtrl");
			if (hpMtrl.GetPointer() == nullptr)
			{
				hpMtrl = new CMaterial;
				hpMtrl->DisableFileSave();
				hpMtrl->SetShader(CResMgr::GetInst()->FindRes<CShader>(L"TexShader"));
				CResMgr::GetInst()->AddRes(L"hpUIMtrl", hpMtrl);
				hpMtrl->SetData(SHADER_PARAM::TEX_0, hpTex.GetPointer());
			}

			m_HpUI->MeshRender()->SetMaterial(hpMtrl);
			CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"Monster")->AddGameObject(m_HpUI);
		

		
		//------------------------------------------------------------------------------------
		

		//------------------------------------------------------------------------------------
		// hp black ui / 몬스터 체력 닳은 정도를 보여주기 위해서  
		//------------------------------------------------------------------------------------
		m_BlackHpUI->Transform()->setPhysics(false);
		m_BlackHpUI->SetName(L"State Monster BlackHpUI Hp UI");
		//위치 설정
		Vec3 vPos_BlackHpUi = GetObj()->Transform()->GetLocalPos();
		vPos_BlackHpUi.y += 15;
		m_vec3Value[(UINT)STATUS_VEC3::LOCAL_POS] = vPos_BlackHpUi;
		m_BlackHpUI->Transform()->SetLocalPos(vPos_hpUi);

		//크기 설정
		Vec3 vScale_MonsterblackHPUI(0, 0.8, 1.1f);
		m_vec3Value[(UINT)STATUS_VEC3::UI_BLACK_HP_SCALE] = vScale_MonsterblackHPUI;
		m_BlackHpUI->Transform()->SetLocalScale(vScale_MonsterblackHPUI);
		Ptr<CTexture> pBlackTex;
		Ptr<CMaterial> pBlackMtrl;
		
		
			m_bInit = false;
			m_BlackHpUI->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

			pBlackTex = CResMgr::GetInst()->FindRes< CTexture>(L"BLACKhpTex");
			if (pBlackTex.GetPointer() == nullptr)
			{
				pBlackTex = CResMgr::GetInst()->Load<CTexture>(L"BLACKhpTex", L"Texture\\BLACKhp.png");
			}
			pBlackMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"BLACKhpMtrl");
			if (pBlackMtrl.GetPointer() == nullptr)
			{
				pBlackMtrl = new CMaterial;
				pBlackMtrl->DisableFileSave();
				pBlackMtrl->SetShader(CResMgr::GetInst()->FindRes<CShader>(L"TexShader"));
				CResMgr::GetInst()->AddRes(L"BLACKhpMtrl", pBlackMtrl);
				pBlackMtrl->SetData(SHADER_PARAM::TEX_0, pBlackTex.GetPointer());
			}

			m_BlackHpUI->MeshRender()->SetMaterial(pBlackMtrl);
			CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"Monster")->AddGameObject(m_BlackHpUI);
		

		
	}
	break;
	default:
		break;
	}
}
void CStatusComponent::LowHp()
{
	if (m_fValue[(UINT)STATUS_FLOAT::HP] < 0.000001f)
	{
		m_bLowHp = true;
	}
	else
	{
		m_bLowHp = false;
	}
}

void CStatusComponent::OnHpUies()
{
	if (!m_HpUI->IsActive())
	{
		m_HpUI->SetActive(true);
	}
	if (!m_BlackHpUI->IsActive())
	{
		m_BlackHpUI->SetActive(false);
	}

}

void CStatusComponent::OffHpUies()
{
	if (m_HpUI->IsActive())
	{
		m_HpUI->SetActive(false);
	}
	if (m_BlackHpUI->IsActive())
	{
		m_BlackHpUI->SetActive(false);
	}
}

void CStatusComponent::UpdateCoolTime(float _toc)
{
	if (m_fValue[(UINT)STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW] < m_fValue[(UINT)STATUS_FLOAT::ATTACK_BAGIC_COOL_MAX])
	{
		m_fValue[(UINT)STATUS_FLOAT::ATTACK_BAGIC_COOL_NOW] += _toc;
	}
	if (m_fValue[(UINT)STATUS_FLOAT::SKILL_1_COOL_NOW] < m_fValue[(UINT)STATUS_FLOAT::SKILL_1_COOL_MAX])
	{
		m_fValue[(UINT)STATUS_FLOAT::SKILL_1_COOL_NOW] += _toc;
	}
	if (m_fValue[(UINT)STATUS_FLOAT::SKILL_2_COOL_NOW] < m_fValue[(UINT)STATUS_FLOAT::SKILL_2_COOL_MAX])
	{
		m_fValue[(UINT)STATUS_FLOAT::SKILL_2_COOL_NOW] += _toc;
	}
	if (m_fValue[(UINT)STATUS_FLOAT::SKILL_HEAL_COOL_NOW] < m_fValue[(UINT)STATUS_FLOAT::SKILL_HEAL_COOL_MAX])
	{
		m_fValue[(UINT)STATUS_FLOAT::SKILL_HEAL_COOL_NOW] += _toc;
	}
}

