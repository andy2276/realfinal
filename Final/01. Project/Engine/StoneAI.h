#pragma once
#include "Script.h"

enum class EC_STONE_STATE {
	 Evaluate		=0
	,BackHeal		=1
	,Move			=2
	,Death			=3
	,Escape			=4
	,SKill1			=5
	,Skill2			=6
	,BagicAttack	=7
	,Turn			=8
	,Patrol			=9
	,DestGo			=10
	,MoveBack		=11
	,Damaged		=12

};
class CBulletBossSkillScript;
class CSkillStoneShower;

class CStoneAI :
	public CScript
{
private:
	CGameObject*			m_pPlayer;
	CBulletBossSkillScript* m_pSkill1;
	CSkillStoneShower*		m_pSkill;
	int						m_nMapIdx;

	int						m_nHealCnt;
	EC_STONE_STATE			m_state;
	bool					m_bSkillShootOn = false;

	
	//	Death
	float					m_fMaxDeathTime;
	float					m_fCurDeathTime;

	//	Skill
	float					m_fMaxSkillTime;
	float					m_fCurSkillTime;

	//	BagicAttack
	float					m_fBagicAttackTiming;		//	공격시간(애니메이션 시간이랑 동일)

	Vec3					m_pInitPos;					//	리셋시 필요
													//	리셋시 필요
	//	BagicAttack
	
	float					m_fBagicAttackMaxTime;		//	피격가능한 시간

	float					m_fBagicAttackHitTime;		//	피격가능한 시간 * 0.6f

	float					m_fBagicAttackRange;		//	공격 사거리	
	float					m_fBagicAttackDamage;		//	공격 데미지
	float					m_fBagicAttackAngle;		//	공격 사이각
	bool					m_bBagicAttackOn;

	//	skill 1
	float					m_fSkill1TimeCur;
	float					m_fSkill1TimeShoot;
	float					m_fSkill1TimeMax;

	//	Patrol

	float					m_fPatrolDistNow;			
	float					m_fPatrolDistMax;

	float					m_fDamagedNow;				//	리셋시 max로
	float					m_fDamagedMax;

	float					m_fTic;
	float					m_fToc;
	float					m_fTicMax;
				
	bool					m_bBeUpdate;				//	이걸로 모든 업데이트 계산
	bool					m_bDamaged;

	UINT					m_nBulletType;
	//	forAstar
	float					m_fFindDestTimeMax;
	float					m_fFindDestTimeCur;
	
	CGameObject*			m_pEffect;


public:
	CStoneAI();
	virtual ~CStoneAI();

	CLONE(CStoneAI);
public:
	virtual void update();

	void SetPlayer(CGameObject* _pPlayer) { m_pPlayer = _pPlayer; }
	void SetInitPos(const Vec3& _init) { m_pInitPos = _init; }

	void Reset(const Vec3& _pos, const Vec3& _rot);
	void SetSkill1(CBulletBossSkillScript* _skill) { m_pSkill1 = _skill; }
	void SetSkill(CSkillStoneShower* _pSkill) { m_pSkill = _pSkill; }
	void SetBulletType(UINT _type) { m_nBulletType = _type; }

	void Collision(int _type,CGameObject* _pObj);

	virtual void OnCollisionEnter(CCollider2D* _pOther);
	virtual void OnCollision(CCollider2D* _pOther);
	virtual void OnCollisionExit(CCollider2D* _pOther) {}
};

