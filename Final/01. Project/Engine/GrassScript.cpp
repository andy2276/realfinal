#include "stdafx.h"
#include "GrassScript.h"
#include "MapMgr.h"


CGrassScript::CGrassScript()
	:CScript((UINT)SCRIPT_TYPE::OTHERS)
{
}


CGrassScript::~CGrassScript()
{
	m_vGrassScale = Vec4(-0.5f, 6.f, 0.5f, 0.f);
}

void CGrassScript::update()
{
	static CMapMgr* mgr = CMapMgr::GetInst();
	static Vec4 size = Vec4(-0.45f, 6.f, 0.45f, 0.f);
	static Vec4 camPos;
	
	
	static Vec2 widHei = Vec2(1.f, 5.f);
	float curScale = 5.5f;
	float tessMin = 0.f;
	float tessMax = 400.f;
	float windAngle = 20.f;
	float windPower = sin(windAngle) *2.f;


	static Vec2 vWindDir = Vec2(cos(windAngle), sin(windAngle));

	camPos = mgr->GetMainCam()->Transform()->GetWorldPos();

	Ptr<CMaterial> pMtrl = MeshRender()->GetSharedMaterial();
	
	//	vec4
	pMtrl->SetData(SHADER_PARAM::VEC4_0, &camPos);
	pMtrl->SetData(SHADER_PARAM::VEC4_1, &size);
	//	vec2
	pMtrl->SetData(SHADER_PARAM::VEC2_0, &widHei);
	pMtrl->SetData(SHADER_PARAM::VEC2_1, &vWindDir);	//	windDir
	//	float
	pMtrl->SetData(SHADER_PARAM::FLOAT_0, &curScale);	//	curScale
	pMtrl->SetData(SHADER_PARAM::FLOAT_1, &tessMin);	//	tessMin
	pMtrl->SetData(SHADER_PARAM::FLOAT_2, &tessMax);	//	tessMax
	pMtrl->SetData(SHADER_PARAM::FLOAT_3, &windPower);

}

Vec2 CGrassScript::GrassHash(Vec2 & _vec)
{
	Vec2 x = _vec;
	Vec2 y = _vec;
	_vec.x = x.Dot(Vec2(127.1f, 311.7f));
	_vec.y = y.Dot(Vec2(269.5f, 183.3f));
	float a = 0.f;
	Vec2 reValue = Vec2(-1.f + 2.f * modf(sin(_vec.x) * 43758.545312f, &a), -1.f + 2.f * modf(sin(_vec.y) * 43758.545312f, &a));
	return reValue;
}
