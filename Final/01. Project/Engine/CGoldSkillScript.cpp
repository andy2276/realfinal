#include "stdafx.h"
#include "CGoldSkillScript.h"

void CGoldSkillScript::update()
{
	if (isCanShoot)
	{
		m_curTime += DT;
		if (m_coolTime - m_curTime < 0)
		{
			isCanShoot = false;
			m_curTime = 0;
		}
	}
}

CGoldSkillScript::CGoldSkillScript() : CScript((UINT)SCRIPT_TYPE::GOLDSKILLSCRIPT)
, m_curTime(0)
,m_coolTime(5)
,m_remainCoolTime(0)
,isCanShoot(false)
{

}

CGoldSkillScript::~CGoldSkillScript()
{
}
