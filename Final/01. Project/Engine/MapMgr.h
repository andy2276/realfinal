#pragma once
//	
//	
//	'7'		'8'		'9'
//	'4'		'5'		'6'
//	'1'		'2'		'3'
//	 		'0'		  
//
#include "Ptr.h"
class CGameObject;
class CMapRapper;
class CTransform;
class CMapScript;
class CMeshData;
struct tSkillArch;
enum class EC_COL_TYPE :BYTE;
enum class EC_BOSS_TYPE
{
	STONE
	,MIRROR
	,End
};


enum class EC_MONSTER_BULLET
{
	Stone
	,DarkBall
	,Ice
	,Meteo
	,End
};
enum class EC_MONSTER_EFFECT
{
	YellowBlack
	,End

};
enum class EC_SKILL_ARCH
{
	RealStoneShower
	,End
};

struct tMapRect
{
	Vec4	v4Rect;
	Vec3	v3Pos;

};
class CScene;
class CTexture;
class CBossAi;
class CMapMgr{
	SINGLE(CMapMgr);
private:
	//	이제이거 사용안함
	vector<CMapRapper*> m_vecMaplappers;
	UINT				m_nCurMapIdx;
	bool				m_bMoveMap;	//	외부에서 실행해주는 녀석
	bool				m_bMissionClear;

	Vec2				m_vMapSize;
	UINT				m_nNextMapIdx;

	Vec3				m_vMouseWorld;
	//	새롭게 사용할것
	CGameObject*		m_pPlayer;
	CGameObject*		m_pCamera;
	Ptr<CMeshData>		m_pMeshData[(UINT)EC_MOSTER_TYPE::End];
	Ptr<CMeshData>		m_pMeshTree[7];
	Ptr<CMeshData>		m_pMeshStone[7];
	vector< Ptr<CMeshData>>	m_vecMeshBullet;
	vector<CGameObject*>	m_vecEnviroment;

	vector<Ptr<CTexture>>	m_vecEffectTexture;
	//	item
	vector< Ptr<CMeshData>>	m_vecScrolls;
	vector<CGameObject*>	m_vecItems;

	Vec3				m_vPlayerClickedPos;
	Vec3				m_vPlayerAttackDir;

	CScene*				m_pScene;
	//	스톤
	int					m_nCurMap;
	int					m_arrMapIdx[3][3];
	int					m_nForcedActive;
	//	사용할것

	bool				m_bStaticMouse;

	vector< CGameObject*> m_vecAllMonster[(UINT)EC_MOSTER_TYPE::End];

	vector<Ptr<CTexture>> m_vecFloorTex;

	CGameObject* m_arrMaps[10];
	CMapScript* m_arrMapScripts[10];
	//	boss
	vector<CGameObject*> m_vecBossMonster;
	vector< CBossAi*> m_vecBossAiScript;
	vector< Ptr<CMeshData>> m_vecBossData;

	vector< tSkillArch> m_vecSkillArchInfo;

	CGameObject*	m_pOutTowerObj;

	const int CalculMapIdxByPlayerPos();
	const float Linear(float v0, float v1, float t) {return (v0 * (1.0f - t) + v1 + t);}
public:
	void Init();
	void Resize(UINT _cnt);
	void RegistMap(UINT _mapNum,CGameObject* _pMap);
	void RegistHeightMap(UINT _mapNum, const wstring& _path,float wid,float hei);
	void RegistColliderMap(UINT _mapNum, const wstring& _path, float wid, float hei,float _scale = 1.0f);
	void SettingAwakeMap(UINT _mapIdx,int* _mapArr, int _mapCnt);
	void SetPortal(UINT _Mapidx, POINT _lu, POINT _rd, UINT _outMapIdx, const Vec3& _startPos, const Vec3& _startRot);
	void Update();
	void ClearCurColliderMap();
	//	엉
	//	리셋이 필요함 처음부터 0번으로 오는게 필요함

	void SetMoster(UINT _mapNum,CGameObject* _pMonster);
	void SetItem(UINT _mapNum,CGameObject* _pItem);

	void CalculHeight(CGameObject* _obj);
	void CalculCollider(CGameObject* _obj);
	void UpdatePlayer();

	CMapRapper* GetMapRapper(int _idx) { return m_vecMaplappers[_idx]; }

	//	외부에서 실행해주는 녀석
	void MoveMap() { m_bMoveMap = true; };

	UINT GetReadyObj(UINT _idx);
	UINT GetCurReadyObj();
	//	-------------------------------------------------------------------------------
	//	사용할것
	void SetScene(CScene* _pScene) { m_pScene = _pScene; }
	CScene* GetScene() { return m_pScene; }
	void SetPlayerObj(CGameObject* _pPlayer);
	CGameObject* GetPlayerObj() { return m_pPlayer; }

	void SetMainCam(CGameObject* _pCamera) { m_pCamera = _pCamera; }
	CGameObject* GetMainCam() { return m_pCamera; }


	int GetCurMapIdx();

	int GetMapIdx(const Vec3& _pos);

	void SetMap(int _mapNum, CGameObject* _pMap);
	
	//	이거도 최초에 해줘야한다.
	void SetMonsterMeshData(EC_MOSTER_TYPE _type, Ptr<CMeshData> _pMeshData) {
		m_pMeshData[(UINT)_type] = _pMeshData;
	};
	Ptr<CMeshData> GetMonsterMeshData(EC_MOSTER_TYPE _type) {
		return m_pMeshData[(UINT)_type];
	}
	void ReUpdate();

	int GetCalculMapIdx();

	//	주어진 인덱스의 맵 오브젝트를 들고온다
	CGameObject* GetMap(int _mapNum) { return m_arrMaps[_mapNum]; }

	//	지금 인덱스의 맵 오브젝트를 들고온다
	CGameObject* GetMap() { return m_arrMaps[m_nCurMap]; }

	//	주어진 인덱스의 맵 스크립트를 들고온다
	CMapScript* GetMapScript(int _mapNum) { return m_arrMapScripts[_mapNum]; }

	//	지금 인덱스의 맵 스크립트를 들고온다
	CMapScript* GetMapScript() { return m_arrMapScripts[m_nCurMap]; }

	//	주어진 인덱스의 주어진 종류의 몬스터를 들고온다
	vector<CGameObject*>& GetMonster(int _mapNum,int _mopType);

	//	지금 인덱스의 주어진 종류의 몬스터를 들고온다
	vector<CGameObject*>& GetMonster(int _mopType);

	//	지금 인덱스의 몬스터들을 들고온다(리스트)
	list< CGameObject*>& GetMonster();

	void PushObj(EC_MOSTER_TYPE _type,CGameObject* _pObj);

	void PushStone(CGameObject*);
	void PushMimic(CGameObject*);
	void PushSkell(CGameObject*);

	vector< CGameObject*>& GetStone() { return m_vecAllMonster[(UINT)EC_MOSTER_TYPE::Stone]; }
	vector< CGameObject*>& GetMimic() { return m_vecAllMonster[(UINT)EC_MOSTER_TYPE::Mimic]; }
	vector< CGameObject*>& GetSkell() { return m_vecAllMonster[(UINT)EC_MOSTER_TYPE::Skelleton]; }

	vector< CGameObject*>& GetTransferMonster(EC_MOSTER_TYPE _type){return m_vecAllMonster[(UINT)_type];}
	vector< CGameObject*>* GetTransferMonster(int _type) { return &m_vecAllMonster[_type]; }

	const Vec3& GetMouseWorld(float _range = 100.0f);

	const Vec3& GetInTowerPos() { static Vec3 pos(0.0f, 0.0f, 650.f); return pos; }
	const Vec3& GetInTowerRot() { return Vec3::Zero; }

	void StopFiledMap();
	void PlayFiledMap();

	void StopTowerMap();
	void PlayTowerMap();

	float GetHeight(int _mapNum,const Vec3& _pos);
	float GetHeight(const Vec3& _pos);

	void SetTreeData(int _idx, Ptr<CMeshData> _pMeshData) { m_pMeshTree[_idx] = _pMeshData; };
	Ptr<CMeshData> GetTreeData(int _idx) { return m_pMeshTree[_idx]; }

	void SetStoneData(int _idx, Ptr<CMeshData> _pMeshData) { m_pMeshStone[_idx] = _pMeshData; }
	Ptr<CMeshData> GetStoneData(int _idx) { return m_pMeshStone[_idx]; }


	void PushEnviroment(CGameObject* _pObj) { m_vecEnviroment.push_back(_pObj); }

	void setBulletData(Ptr<CMeshData> _pMeshData) { m_vecMeshBullet.push_back(_pMeshData); }
	Ptr<CMeshData> GetBulletData(int _idx) { return m_vecMeshBullet[_idx]; }
	
	void PushMonsterSkillEffectTexture(Ptr<CTexture> _pTex) { m_vecEffectTexture.push_back(_pTex); }
	Ptr<CTexture> GetMonsterSkillEffectTexture(int _idx) { return m_vecEffectTexture[_idx]; }

	const tMapRect& GetMapRect(int _idx);
	//	아이템
	void PushItemScrollData(Ptr<CMeshData> _pData) { m_vecScrolls.push_back(_pData); }
	void DropItem(const Vec3& _pos,int _offset = 0);

	void SetPlayerClickedPos(const Vec3& _pos) { m_vPlayerClickedPos = _pos; }
	const Vec3& GetPlayerClickedPos() { return m_vPlayerClickedPos; }
	
	void SetPlayerAttackDir(const Vec3& _dir) { m_vPlayerAttackDir = _dir; }
	const Vec3& GetPlayerAttackDir();

	//	boss
	CGameObject* CreateBossMonster(int _bossType, int _offset);
	CGameObject* ResponBoss(int _mapNum,int _bossType,const Vec3& _pos,const Vec3& _rot,int _offset = 0);

	void PushBossMeshData(Ptr<CMeshData> _pMeshData) { m_vecBossData.push_back(_pMeshData); };
	Ptr<CMeshData> GetBossMeshData(int _idx) { return m_vecBossData[_idx]; }

	bool IsStaticMouse() { return m_bStaticMouse; }
	void OffStaticMouse() { m_bStaticMouse = false; }
	void OnStaticMouse() { m_bStaticMouse = true; }
	void SetStaticMouse(bool _bTrue) { m_bStaticMouse = _bTrue; }

	void InitSkillArch();
	const tSkillArch& GetSkillArch(int _idx) { return m_vecSkillArchInfo[_idx]; }

	void SetOutTowerObj(CGameObject* _outTower) { m_pOutTowerObj = _outTower; };
	void OnOutTowerObj();
	void OffOutTowerObj();

	void InitFloorTex();
	Ptr<CTexture> GetFloorTex(int _idx) { return m_vecFloorTex[_idx]; };

};

