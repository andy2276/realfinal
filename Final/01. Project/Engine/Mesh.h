#pragma once
#include "Resource.h"

#include "FBXLoader.h"
#include "Ptr.h"
#include "StructuredBuffer.h"

class CInstancingBuffer;

struct tIndexInfo
{
	ComPtr<ID3D12Resource>		pIB;
	D3D12_INDEX_BUFFER_VIEW		tIdxView;
	DXGI_FORMAT					eIdxFormat;
	UINT						iIdxCount;
	void*						pIdxSysMem;
};

class CMesh :
	public CResource
{
private:
	ComPtr<ID3D12Resource>		m_pVB;
	D3D12_VERTEX_BUFFER_VIEW	m_tVtxView; // 버텍스 버퍼 정보 구조체(stride, total size, 가상 GPU 주소)
	
	UINT						m_iVtxSize;
	UINT						m_iVtxCount;
	void*						m_pVtxSysMem;

	vector<tIndexInfo>			m_vecIdxInfo;

	// Animation3D 정보
	vector<tMTAnimClip>			m_vecAnimClip;
	vector<tMTBone>				m_vecBones;	

	CStructuredBuffer*			m_pBoneFrameData; // 전체 본 프레임 정보
	CStructuredBuffer*			m_pBoneOffset;	   // 각 뼈의 offset 행렬

	int							m_nIdxStack;
	FbxTime::EMode				m_eMode;

	const double CalculTime(const int& _frame) { return (double(_frame) * 0.3); }

	int							m_nInstIdx;
public:
	void Create(UINT _iVtxSize, UINT _iVtxCount, BYTE* _pVtxSysMem
		, DXGI_FORMAT _iIdxFormat, UINT _iIdxCount, BYTE* _pIdxSysMem);

	static CMesh * CreateFromContainer(CFBXLoader & _loader);

	void render(UINT _iSubset = 0);
	void render_particle(UINT _iInstancCount, UINT _iSubset = 0);
	void render_instancing(UINT _iSubset, CInstancingBuffer* _pInstBuffer);
	//	해결점 1
	void render_instancing(UINT _iSubset, CInstancingBuffer* _pInstBuffer, int _nCurCnt);

	void render_instancing_shadow(UINT _iSubset, CInstancingBuffer* _pInstBuffer);

	void SetInstBufferIdx(int _idx) { m_nInstIdx = _idx; }
	int GetInstBufferIdx() { return m_nInstIdx; }
public:
	UINT GetSubsetCount() { return (UINT)m_vecIdxInfo.size(); }
	const vector<tMTBone>* GetBones() { return &m_vecBones; }	
	vector<tMTAnimClip>* GetAnimClip() { return &m_vecAnimClip; }	
	CStructuredBuffer*	GetBoneFrameDataBuffer() { return m_pBoneFrameData; } // 전체 본 프레임 정보
	CStructuredBuffer*	GetBoneOffsetBuffer() { return  m_pBoneOffset; }	   // 각 뼈의 offset 행렬	
	UINT GetBoneCount() { return (UINT)m_vecBones.size(); }
	bool IsAnimMesh() { return !m_vecAnimClip.empty(); }

	void ClipedInfo();
	void ClipedAnimation(tMTAnimClip* _pClip, const UINT& _cnt);
	void ClipedAnimation(const wstring& _name, UINT _idx, int _start, int _end);
	void ClipedAnimationReset() { if (m_vecAnimClip.size() > 0) { m_eMode = m_vecAnimClip[0].eMode; } m_vecAnimClip.clear(); }
	void ClipedAnimationResize(const int& _cnt) { ClipedAnimationReset(); m_vecAnimClip.resize(_cnt); }
	void ClipedAnimationReserve(const int& _cnt) { ClipedAnimationReset(); m_vecAnimClip.reserve(_cnt); }
	//	if(-1){push_back} else{m_vecAnimClip[_inIdx]}
	void ClipedAnimationEasy(const tMTAnimClipEasy& _easy,const int& _inIdx = -1);
	void ClipedAnimationEasy(const wstring& _name, const int& _endFrame) { ClipedAnimationEasy(tMTAnimClipEasy{ _name,_endFrame }); }
	void ClipedAnimationEasy(const wstring& _name, const int& _endFrame,const int& _inIdx) { ClipedAnimationEasy(tMTAnimClipEasy{ _name,_endFrame }, _inIdx); }
	void ClipedAnimationFinal() {}
public:	
	virtual void Load(const wstring& _strFullPath);
	virtual void Save(const wstring& _strPath/*상대 경로*/);

public:
	CMesh();
	virtual ~CMesh();
};

