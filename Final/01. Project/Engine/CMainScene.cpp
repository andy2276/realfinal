#include "stdafx.h"
#include "CMainScene.h"

#include "CollisionMgr.h"
#include "EventMgr.h"
#include "RenderMgr.h"
#include "Device.h"
#include "Core.h"

#include "ResMgr.h"
#include "Shader.h"
#include "Mesh.h"
#include "Texture.h"
#include "Transform.h"
#include "MeshRender.h"
#include "Animator2D.h"
#include "Animation2D.h"
#include "Collider2D.h"
#include "Light2D.h"
#include "Light3D.h"
#include "ParticleSystem.h"
#include "StatusComponent.h"
#include"UIscript.h"

#include "Layer.h"
#include "TimeMgr.h"
#include "KeyMgr.h"
#include "Camera.h"

#include "PlayerScript.h"
#include "MonsterScript.h"
#include "ToolCamScript.h"
#include "GridScript.h"


#include "Animator3D.h"

#include "meshdata.h"

CMainScene::CMainScene()
{

}
CMainScene::~CMainScene()
{

}
void CMainScene::ResInit()
{

}
void CMainScene::init()
{
	ResInit();

	setUseLayer();

	CGameObject* pObject = nullptr;

	CGameObject* pMainCam = new CGameObject;
	pMainCam->SetName(L"MainCam2");
	pMainCam->AddComponent(new CTransform);
	pMainCam->AddComponent(new CCamera);
	pMainCam->AddComponent(new CToolCamScript);

	pMainCam->Camera()->SetProjType(PROJ_TYPE::PERSPECTIVE);
	pMainCam->Camera()->SetFar(100000.f);
	pMainCam->Camera()->SetLayerAllCheck();
	pMainCam->Camera()->SetLayerCheck(30, false);
	pMainCam->Transform()->SetLocalPos(Vec3(0.0f, 65.0f, -110.0f));
	pMainCam->Transform()->SetLocalRot(Vec3((XM_PI / 180.0f)*10.0f, 0.0f, 0.0f));

	FindLayer(L"Default")->AddGameObject(pMainCam);

	CGameObject* pUICam = new CGameObject;
	pUICam->SetName(L"MainCam3");
	pUICam->AddComponent(new CTransform);
	pUICam->AddComponent(new CCamera);

	pUICam->Camera()->SetProjType(PROJ_TYPE::ORTHOGRAPHIC);
	pUICam->Camera()->SetFar(100.f);
	pUICam->Camera()->SetLayerCheck(30, true);

	FindLayer(L"Default")->AddGameObject(pUICam);

	pObject = new CGameObject;
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CLight3D);

	pObject->Light3D()->SetLightPos(Vec3(0.f, 80.f, 0.f));
	pObject->Light3D()->SetLightType(LIGHT_TYPE::DIR);
	pObject->Light3D()->SetDiffuseColor(Vec3(1.f, 1.f, 1.f));
	pObject->Light3D()->SetSpecular(Vec3(0.3f, 0.3f, 0.3f));
	pObject->Light3D()->SetAmbient(Vec3(0.1f, 0.1f, 0.1f));
	pObject->Light3D()->SetLightDir(Vec3(1.f, -1.f, 1.f));
	pObject->Light3D()->SetLightRange(1000.f);

	FindLayer(L"Default")->AddGameObject(pObject);

	pObject = new CGameObject;
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CLight3D);

	pObject->Light3D()->SetLightPos(Vec3(0.f, 80, 0.f));
	pObject->Light3D()->SetLightType(LIGHT_TYPE::POINT);
	pObject->Light3D()->SetDiffuseColor(Vec3(1.f, 1.f, 1.f));
	pObject->Light3D()->SetSpecular(Vec3(0.3f, 0.3f, 0.3f));
	pObject->Light3D()->SetAmbient(Vec3(0.1f, 0.1f, 0.1f));
	pObject->Light3D()->SetLightDir(Vec3(1.f, -1.f, 1.f));
	pObject->Light3D()->SetLightRange(1000.f);

	FindLayer(L"Default")->AddGameObject(pObject);

	CGameObject* pPlayer = new CGameObject;

//	Ptr<CMeshData> pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\boxsize.FBX");
	//pMeshData->Save(pMeshData->GetPath());

	//Ptr<CMeshData> pMeshData01 = CResMgr::GetInst()->LoadFBX(L"FBX\\boxsize.FBX");


	Ptr<CMeshData> pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\monster.mdat", L"MeshData\\monster.mdat");


	pPlayer = pMeshData->Instantiate();
//	CAniClipMgr::GetInst()->ClipedAnimation(pPlayer->MeshRender()->GetMesh().GetPointer(), L"Player00", EC_ANI_CLIP::Idle, 0.0f, 1.1f, 0, 32);
	//CAniClipMgr::GetInst()->ClipedAnimation(pPlayer->MeshRender()->GetMesh().GetPointer(), L"Player00", EC_ANI_CLIP::BackWalk, 1.2f, 2.2f, 33, 64);
	pPlayer->SetName(L"Player");

	pPlayer->AddComponent(new CTransform);
	pPlayer->FrustumCheck(false);
	pPlayer->Transform()->SetLocalPos(Vec3(50.f, 0.f, 0.f));
	//pPlayer->Transform()->SetLocalScale(Vec3(5.f, 5.f, 5.f));
	pPlayer->Transform()->SetLocalScale(Vec3(10.f, 10.f, 10.f));

	pPlayer->Transform()->SetLocalRot(Vec3(0.f, 0.f, 0.f));

	pPlayer->AddComponent(new CCollider2D);
	pPlayer->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::OBB);
	//pPlayer->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::SPHERE);

	//pPlayer->AddComponent(new CStatusComponent);
	//pPlayer->Collider2D()->SetOffsetPos(Vec3(0.f, 85.f, 0.f));
//	vFinalPos.y += (m_vOffsetScale.y / 2);
	pPlayer->Collider2D()->SetOffsetScale(Vec3(100.f, 100.f, 100.f));
	pPlayer->AddComponent(new CPlayerScript); 

;
	//pMainCam->SetParent(pPlayer);
//	pMainCam->Camera()->setPlayer(pPlayer);
	//pMainCam->Camera()->setPlayer(pPlayer);
	//pPlayer->Animator3D()->SetAniClilpLoader(CAniClipMgr::GetInst()->GetClipLoader(L"Player00"));
	FindLayer(L"Player")->AddGameObject(pPlayer); // AddGameObject


	//=================================================================================================================================
	//=================================================================================================================================




	Ptr<CMeshData> pMeshData01 = CResMgr::GetInst()->LoadFBX(L"FBX\\002.FBX");
	pMeshData01->Save(pMeshData01->GetPath());


//	Ptr<CMeshData> pMeshData01 = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\stone_wing.mdat", L"MeshData\\stone_wing.mdat");

	CGameObject* pBoss = new CGameObject;

	pBoss = pMeshData01->Instantiate();
	pBoss->SetName(L"Boss");
	pBoss->FrustumCheck(false);
	pBoss->Transform()->SetLocalPos(Vec3(50.f, 50.f, 0.f));
//	pBoss->Transform()->SetLocalScale(Vec3(3.f, 3.f, 3.f));
	pBoss->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));

	pBoss->Transform()->SetLocalRot(Vec3(0, 0.f, 0.f));

//	pBoss->AddComponent(new CMonsterScript);
//	pBoss->GetScript<CMonsterScript>()->setPlayer(pPlayer);
//	pBoss->AddComponent(new CStatusComponent);
	pBoss->AddComponent(new CCollider2D);
	pBoss->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::OBB);
	//pBoss->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::SPHERE);
	pBoss->Collider2D()->SetOffsetScale(Vec3(10.f, 10.f, 10.f));

	
	//pBoss->Collider2D()->SetOffsetScale(Vec3(30.f, 30.f, 30.f));

	AddGameObject(L"Monster", pBoss, false);


	//=================================================================================================================================
	//=================================================================================================================================

//	Ptr<CMeshData>pMeshData2;

	Ptr<CMeshData>pMeshData2 = CResMgr::GetInst()->LoadFBX(L"FBX\\bossmap05.fbx");
	//Ptr<CMeshData>pMeshData2 = CResMgr::GetInst()->LoadFBX(L"FBX\\0615.fbx");
	//Ptr<CMeshData>pMeshData2 = CResMgr::GetInst()->LoadFBX(L"FBX\\1232.fbx");


	pMeshData2->Save(pMeshData2->GetPath());


	pObject = pMeshData2->Instantiate();
	pObject->SetName(L"bossmap");

	pObject->AddComponent(new CTransform);

	pObject->FrustumCheck(false);
	pObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 0.f));
	pObject->Transform()->SetLocalScale(Vec3(100.f,100.f, 100.f));
	pObject->Transform()->SetLocalRot(Vec3(0.f, 0.f, 0.f));

	FindLayer(L"Player")->AddGameObject(pObject);


	/////////////////////////////////////////
	Ptr<CTexture> pSky01 = CResMgr::GetInst()->Load<CTexture>(L"Sky01", L"Texture\\Skybox\\Sky01.png");
	Ptr<CMaterial> pPM = CResMgr::GetInst()->FindRes<CMaterial>(L"MergeLightMtrl");
	pPM->SetData(SHADER_PARAM::TEX_3, pSky01.GetPointer());

	pObject = new CGameObject;
	pObject->SetName(L"SkyBox");
	pObject->FrustumCheck(false);
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);

	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"SkyboxMtrl"));
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pSky01.GetPointer());

	FindLayer(L"Default")->AddGameObject(pObject);




	//pObject = new CGameObject;
	//pObject->SetName(L"Grid");
	//pObject->FrustumCheck(false);
	//pObject->AddComponent(new CTransform);
	//pObject->AddComponent(new CMeshRender);
	//pObject->AddComponent(new CGridScript);


	//pObject->Transform()->SetLocalScale(Vec3(100000.f, 100000.f, 1.f));
	//pObject->Transform()->SetLocalRot(Vec3(XM_PI / 2.f, 0.f, 0.f));


	//pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	//pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"GridMtrl"));

	//pObject->GetScript<CGridScript>()->SetToolCamera(pMainCam);
	//pObject->GetScript<CGridScript>()->SetGridColor(Vec3(0.8f, 0.2f, 0.2f));


	//FindLayer(L"Tool")->AddGameObject(pObject);




	pMeshData2 = CResMgr::GetInst()->LoadFBX(L"FBX\\tower01.fbx");
	pMeshData2->Save(pMeshData2->GetPath());

//	pMeshData2 = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\tower01.mdat", L"MeshData\\tower01.mdat");

	pObject = pMeshData2->Instantiate();
	pObject->SetName(L"tower");

	pObject->AddComponent(new CTransform);
	pObject->FrustumCheck(false);
	pObject->Transform()->SetLocalPos(Vec3(500.f, 0.f, 0.f));
	pObject->Transform()->SetLocalScale(Vec3(100.f, 100.f, 100.f));
	pObject->Transform()->SetLocalRot(Vec3(0.f, 0.f, 0.f));

	FindLayer(L"Player")->AddGameObject(pObject);


	Ptr<CTexture> singleborder = CResMgr::GetInst()->Load<CTexture>(L"singleborder", L"Texture\\border.png");
	Ptr<CTexture> pSky02 = CResMgr::GetInst()->Load<CTexture>(L"Sky02", L"Texture\\Skybox\\Sky02.jpg");
	//Ptr<CTexture> pInventory = CResMgr::GetInst()->Load<CTexture>(L"border", L"Texture\\borderBound.png");
	Ptr<CTexture> pInventory = CResMgr::GetInst()->Load<CTexture>(L"border", L"Texture\\borderBound_firstSkill.png");

	//===========================================================

	//pObject = new CGameObject;
	//pObject->SetName(L"tower1");

	//pObject->AddComponent(new CTransform);
	//pObject->AddComponent(new CMeshRender);
	//pObject->FrustumCheck(false);
	//pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
	//pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));
	//pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pSky02.GetPointer());

	//pObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 0.f));
	//pObject->Transform()->SetLocalScale(Vec3(10.f, 10.f, 10.f));
	//pObject->Transform()->SetLocalRot(Vec3(0.f, 0.f, 0.f));

	//FindLayer(L"Player")->AddGameObject(pObject);







	//===========================================================
	

	//pObject = new CGameObject;
	//pObject->SetName(L"Particle");
	//pObject->AddComponent(new CTransform);
	//pObject->AddComponent(new CParticleSystem);

	//pObject->FrustumCheck(false);
	//pObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 0.f));

	//FindLayer(L"Default")->AddGameObject(pObject);





	

	//boss ui border - tex
	///////////////////////////////////////////////////////////////////////////////////////////
	Vec3 vBARScale(230.f, 15.f, 1.f);

	Vec3 vScale(200.f, 10.f, 1.f);
	tResolution res = CRenderMgr::GetInst()->GetResolution();


	//pObject = new CGameObject;
	//pObject->SetName(L"Boss UI BAR");
	//pObject->FrustumCheck(false);
	//pObject->AddComponent(new CTransform);
	//pObject->AddComponent(new CMeshRender);

	res = CRenderMgr::GetInst()->GetResolution();
	////	 1280, 768

	//pObject->Transform()->SetLocalPos(Vec3(10.f, 300.f, 1.f));
	////vScale.x += 10;
	////vScale.y += 10;
	//pObject->Transform()->SetLocalScale(vBARScale);

	//pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

	//pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));
	////pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, phpborder.GetPointer());
	//pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, singleborder.GetPointer());



	//pObject->AddComponent(new UIscript);
	//pObject->GetScript<UIscript>()->setPlayer(pBoss);

	//FindLayer(L"UI")->AddGameObject(pObject);



	// PLAYER UI border - tex
	///////////////////////////////////////////////////////////////////////////////////////////

	pObject = new CGameObject;
	pObject->SetName(L"Boss UI BAR");
	pObject->FrustumCheck(false);
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);

	res = CRenderMgr::GetInst()->GetResolution();
	//	 1280, 768

	//pObject->Transform()->SetLocalPos(Vec3(400.f, -300.f, 1.f));
	//pObject->Transform()->SetLocalScale(Vec3(270.f,60.f,0.f));

	pObject->Transform()->SetLocalPos(Vec3(-(res.fWidth / 2.f) + (vScale.x / 2.f) + (vScale.x) - 100.f + 10.f
		, (res.fHeight / 2.f) - (vScale.y / 2.f) - 700.f
		, 1.f));
	//pObject->Transform()->SetLocalPos(Vec3(10.f, 300.f, 1.f));
	//vScale.x += 10;
	//vScale.y += 10;
	pObject->Transform()->SetLocalScale(vBARScale);



	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));
	//pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, phpborder.GetPointer());
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, singleborder.GetPointer());

	FindLayer(L"UI")->AddGameObject(pObject);


	


	//BOSS UI border BAR
	///////////////////////////////////////////////////////////////////////////////////////////

	pObject = new CGameObject;
	pObject->SetName(L"Boss UI BAR");
	pObject->FrustumCheck(false);
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);

	res = CRenderMgr::GetInst()->GetResolution();
	//	 1280, 768


	pObject->Transform()->SetLocalPos(Vec3(-(res.fWidth / 2.f) + (vScale.x / 2.f) + (vScale.x) - 100.f + 10.f
		, (res.fHeight / 2.f) - (vScale.y / 2.f) - 650.f
		, 1.f));

	//pObject->Transform()->SetLocalPos(Vec3(0.f, 300.f, 1.f));
	//vScale.x += 10;
	//vScale.y += 10;
	pObject->Transform()->SetLocalScale(vBARScale);



	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));
	//pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, phpborder.GetPointer());
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, singleborder.GetPointer());

	FindLayer(L"UI")->AddGameObject(pObject);





	// player hp status 
	///////////////////////////////////////////////////////////////////////////////////////////
	{

	pObject = new CGameObject;
	pObject->SetName(L"Player Hp UI");
	pObject->FrustumCheck(false);
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);


	pObject->Transform()->SetLocalPos(Vec3(-(res.fWidth / 2.f) + (vScale.x / 2.f) + (vScale.x) - 100.f
		, (res.fHeight / 2.f) - (vScale.y / 2.f) - 650.f
		, 1.f));
	//vScale.x -= 10;
	//vScale.y -= 10;

	pObject->Transform()->SetLocalScale(vScale);


	Ptr<CTexture> pTex = CResMgr::GetInst()->Load<CTexture>(L"TestTex", L"Texture\\Health.png");

	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"UIHPMtrl"));

	pObject->AddComponent(new UIscript);
	pObject->GetScript<UIscript>()->setPlayer(pPlayer);
	pObject->GetScript<UIscript>()->init();
	pObject->GetScript<UIscript>()->setUItype((UINT)UI_TYPE::HP);
	FindLayer(L"UI")->AddGameObject(pObject);

}


	

	{
		pObject = new CGameObject;
		pObject->SetName(L"Player Mp UI");
		pObject->FrustumCheck(false);
		pObject->AddComponent(new CTransform);
		pObject->AddComponent(new CMeshRender);

		res = CRenderMgr::GetInst()->GetResolution();

		pObject->Transform()->SetLocalPos(Vec3(-(res.fWidth / 2.f) + (vScale.x / 2.f) + (vScale.x) - 100.f
			, (res.fHeight / 2.f) - (vScale.y / 2.f) - 700.f
			, 1.f));

		pObject->Transform()->SetLocalScale(vScale);


		//Ptr<CTexture> pTex = CResMgr::GetInst()->Load<CTexture>(L"TestTex", L"Texture\\Health.png");

		pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

		pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"UIMPMtrl"));

		pObject->AddComponent(new UIscript);
		pObject->GetScript<UIscript>()->setPlayer(pPlayer);
		pObject->GetScript<UIscript>()->init();
		pObject->GetScript<UIscript>()->setUItype((UINT)UI_TYPE::MP);
	
		FindLayer(L"UI")->AddGameObject(pObject);

	}

	
	{
		//pObject = new CGameObject;
		//pObject->SetName(L"Boss Hp UI");
		//pObject->FrustumCheck(false);
		//pObject->AddComponent(new CTransform);
		//pObject->AddComponent(new CMeshRender);

		//res = CRenderMgr::GetInst()->GetResolution();
		////	 1280, 768

		//pObject->Transform()->SetLocalPos(Vec3(0.f, 300.f, 1.f));
		//pObject->Transform()->SetLocalScale(vScale);



		//pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

		//pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"UIHPMtrl"));

		//pObject->AddComponent(new UIscript);
		//pObject->GetScript<UIscript>()->setPlayer(pBoss);

		//FindLayer(L"UI")->AddGameObject(pObject);
	}


	
	//border
	pObject = new CGameObject;
	pObject->SetName(L"Border UI");
	pObject->FrustumCheck(false);
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);

	res = CRenderMgr::GetInst()->GetResolution();
	//	 1280, 768

	pObject->Transform()->SetLocalPos(Vec3(400.f, -290.f, 1.f));
	pObject->Transform()->SetLocalScale(Vec3(270.f,60.f,0.f));

	int i = 1;

	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"UIinventoryMtrl"));

	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pInventory.GetPointer());

	FindLayer(L"UI")->AddGameObject(pObject);


	//
	////hp,mp word
	//Ptr<CTexture> pStatusWord = CResMgr::GetInst()->Load<CTexture>(L"statusword", L"Texture\\status.png");

	//pObject = new CGameObject;
	//pObject->SetName(L"Border UI");
	//pObject->FrustumCheck(false);
	//pObject->AddComponent(new CTransform);
	//pObject->AddComponent(new CMeshRender);

	//res = CRenderMgr::GetInst()->GetResolution();
	////	 1280, 768

	//pObject->Transform()->SetLocalPos(Vec3(0.f, -300.f, 1.f));
	//pObject->Transform()->SetLocalScale(Vec3(150.f, 60.f, 0.f));



	//pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));

	////pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));

	//pObject->AddComponent(new UIscript);
	//pObject->GetScript<UIscript>()->setPlayer(pBoss);

	//FindLayer(L"UI")->AddGameObject(pObject);

	//Ptr<CTexture> pGrass02Tex = CResMgr::GetInst()->Load<CTexture>(L"TerrainTex", L"Texture\\Grass02.png");
	//Ptr<CTexture> pGrass02TexNormal = CResMgr::GetInst()->Load<CTexture>(L"TerrainTex_nomal", L"Texture\\Map100_100_Nomal.png");
	//
	//wstring myData = CPathMgr::GetResPath();
	//myData += L"Texture\\Grass100_100.raw";
	//CMapMgr::GetInst()->CreateMapLoader(EC_MAP_REGION::StartArea, myData.c_str(), 100, 100, 10, 10, true);
	////	CMapMgr::GetInst()->CreateMapLoaderEx(EC_MAP_REGION::StartArea, myData.c_str(), 100, 100, 16, 16);
	//CMapAndGravity* mapScript = new CMapAndGravity;
	//mapScript->SetRegion(EC_MAP_REGION::StartArea);
	//CResMgr::GetInst()->CreateTerrain(L"Terrain",1000, 1000, 100, 100, CMapMgr::GetInst()->GetMapLoader(EC_MAP_REGION::StartArea));
	//
	//pObject = new CGameObject;
	//pObject->SetName(L"TestMap2");
	//pObject->AddComponent(new CTransform);
	//pObject->AddComponent(new CMeshRender);
	//pObject->FrustumCheck(false);
	//
	//pObject->Transform()->SetLocalPos(Vec3(0.0f, 0.0f, 0.0f));
	//pObject->Transform()->SetLocalScale(Vec3(1.0f, 1.0f, 1.0f));
	//
	//pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"Terrain"));
	//pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"Std3DMtrl"));
	//pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pGrass02Tex.GetPointer());
	//pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pGrass02TexNormal.GetPointer());



		//
		//pObject = new CGameObject;
		//pObject->SetName(L"item UI");
		//pObject->FrustumCheck(false);
		//pObject->AddComponent(new CTransform);
		//pObject->AddComponent(new CMeshRender);

		//pObject->Transform()->SetLocalPos(Vec3( 0 , -5.f, 0.f));
		//pObject->Transform()->SetLocalScale(Vec3(100000.f,1000000.f,10000.f));
		//pObject->Transform()->SetLocalRot(Vec3(XM_PI/2.f, 0.f,0.f));
		//

		//Ptr<CTexture> pTex = CResMgr::GetInst()->Load<CTexture>(L"Grass02", L"Texture\\Grass02.png");

		//pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		//Ptr<CMaterial> pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
		//pObject->MeshRender()->SetMaterial(pMtrl->Clone());
		//pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTex.GetPointer());



		//FindLayer(L"Default")->AddGameObject(pObject);
		//==============================================================================================


	CCollisionMgr::GetInst()->CheckCollisionLayer(L"Player", L"Monster");
	CCollisionMgr::GetInst()->CheckCollisionLayer(L"Bullet", L"Monster");
	CCollisionMgr::GetInst()->CheckCollisionLayer(L"Player", L"Item");
	CCollisionMgr::GetInst()->CheckCollisionLayer(L"Bullet", L"Item");


}

void CMainScene::setUseLayer()
{

	GetLayer(0)->SetName(L"Default");
	GetLayer(1)->SetName(L"Player");
	GetLayer(2)->SetName(L"Monster");
	GetLayer(3)->SetName(L"Bullet");
	GetLayer(4)->SetName(L"Item");


	GetLayer(30)->SetName(L"UI");
	GetLayer(31)->SetName(L"Tool");

}