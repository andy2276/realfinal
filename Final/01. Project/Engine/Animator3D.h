#pragma once
#include "Component.h"

#include "Ptr.h"
#include "Texture.h"
#include "Material.h"
#include "Mesh.h"

class CStructuredBuffer;

class CAniClipLoader;
class CAnimator3D : public CComponent
{
private:
	const vector<tMTBone>*		m_pVecBones;
	const vector<tMTAnimClip>*	m_pVecClip;

	vector<float>				m_vecClipUpdateTime;
	vector<Matrix>				m_vecFinalBoneMat; // 텍스쳐에 전달할 최종 행렬정보
	int							m_iFrameCount; // (초당 프레임)
	double						m_dCurTime;
	int							m_iCurClip; // 클립 인덱스	
	int							m_iFrameIdx; // 클립의 현재 프레임
	int							m_iNextFrameIdx; // 다음 프레임 인덱스
	float						m_fRatio;	// 프레임 사이 비율

	Ptr<CMaterial>				m_pBoneMtrl; // BoneData Update Compute Shader
	CStructuredBuffer*			m_pBoneFinalMat;  // 특정 프레임의 최종 행렬
	bool						m_bFinalMatUpdate; // 최종행렬 연산 수행여부

	bool						m_bAnimStop;
	
	EC_ANI_CLIP					m_ecCurState;
	
	bool						m_bAnimEnd;
	//	지민추
	float						m_fCurLeftTime;

public:
	void SetBones(const vector<tMTBone>* _vecBones) { m_pVecBones = _vecBones; m_vecFinalBoneMat.resize(m_pVecBones->size()); }
	void SetAnimClip(vector<tMTAnimClip>* _vecAnimClip);
	void UpdateData();
	void UpdateData_Inst(CStructuredBuffer* _pBoneBuffer, UINT _iRow);
	void SetClipTime(int _iClipIdx, float _fTime) { m_vecClipUpdateTime[_iClipIdx] = _fTime; }

	void ChangeAnimation() { ++m_iCurClip; if (m_iCurClip > m_pVecClip->size())m_iCurClip = 0; }
	void ChangeAnimation(const int& _idx,bool _seq = false) 
	{
		if (_seq)
		{
			if (m_iCurClip == _idx)return;
		}
		m_bAnimEnd = false; 
		m_vecClipUpdateTime[m_iCurClip] = 0.0f;
		m_iCurClip = _idx; 
	}
	void ResizeAnimationUpdate() { m_vecClipUpdateTime.clear(); m_vecClipUpdateTime.resize(m_pVecClip->size()); }
	CStructuredBuffer* GetFinalBoneMat() { return m_pBoneFinalMat; }
	UINT GetBoneCount() { return (UINT)m_pVecBones->size(); }
private:
	void check_mesh(Ptr<CMesh> _pMesh);
public:
	virtual void awake() {};
	virtual void start() {};
	virtual void update();
	virtual void lateupdate();
	virtual void finalupdate();

public:
	virtual void SaveToScene(FILE* _pFile);
	virtual void LoadFromScene(FILE* _pFile);
	CAnimator3D* Clone() { return new CAnimator3D(*this); }

	const bool& IsAnimEnd() { return m_bAnimEnd; }
	bool IsAnimIdx(int _idx)
	{
		return (_idx == m_iCurClip);
	}
	void SetAnimEnd(const bool& _bool) { m_bAnimEnd = _bool; }

	float GetCurAnimLeftTime() { return m_fCurLeftTime; }

	void AnimEnd() { m_bAnimEnd = true; }
	void AnimStart() { m_bAnimEnd = false; }
	void AnimClipeReset() { m_pVecClip = nullptr; }

	void AnimStop() { m_bAnimStop = true; }
	void AnimPlay() { m_bAnimStop = false; }

public:
	CAnimator3D();
	virtual ~CAnimator3D();
};

