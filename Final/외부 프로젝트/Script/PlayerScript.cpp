#include "stdafx.h"
#include "PlayerScript.h"

#include "BulletScript.h"
//#include "TestScript.h"

CPlayerScript::CPlayerScript()
	: CScript((UINT)SCRIPT_TYPE::PLAYERSCRIPT)
	, m_pOriginMtrl(nullptr)
	, m_pCloneMtrl(nullptr)
{	
}

CPlayerScript::~CPlayerScript()
{
}

void CPlayerScript::awake()
{
	m_pOriginMtrl = MeshRender()->GetSharedMaterial();
	m_pCloneMtrl = m_pOriginMtrl->Clone();

	// child object 추가
	CGameObject* pChildObj = new CGameObject;

	pChildObj->SetName(L"Child");
	pChildObj->AddComponent(new CTransform);
	pChildObj->Transform()->SetLocalPos(Vec3(1.5f, 0.f, 100.f));
	pChildObj->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
	pChildObj->Transform()->SetLocalRot(Vec3(0.f, 0.f, 0.f));

	pChildObj->AddComponent(new CMeshRender);
	pChildObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	pChildObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl")->Clone());
	Ptr<CTexture> pTexture2 = CResMgr::GetInst()->FindRes<CTexture>(L"Texture\\ironman.png");
	pChildObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, &pTexture2);

	pChildObj->AddComponent(new CCollider2D);
	pChildObj->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::RECT);
	pChildObj->Collider2D()->SetOffsetPos(Vec3(0.f, 0.f, 0.f));
	pChildObj->Collider2D()->SetOffsetScale(Vec3(0.8f, 0.8f, 1.f));

	//pChildObj->AddComponent(new CTestScript);

	AddChild(pChildObj);
}

void CPlayerScript::update()
{
	Vec3 vPos = Transform()->GetLocalPos();
	Vec3 vRot = Transform()->GetLocalRot();

	if (KEY_HOLD(KEY_TYPE::KEY_W))
	{
		vPos.y += DT * 100.f;
	}

	if (KEY_HOLD(KEY_TYPE::KEY_S))
	{
		vPos.y -= DT * 100.f;
	}

	if (KEY_HOLD(KEY_TYPE::KEY_A))
	{	
		vPos.x -= DT * 100.f;
	}

	if (KEY_HOLD(KEY_TYPE::KEY_D))
	{
		vPos.x += DT * 100.f;
	}

	if (KEY_HOLD(KEY_TYPE::KEY_Z))
	{
		vRot.z += DT * XM_PI;
							
		MeshRender()->SetMaterial(m_pCloneMtrl);		
		 
		int a = 1;
		m_pCloneMtrl->SetData(SHADER_PARAM::INT_0, &a);
	}
	else if (KEY_AWAY(KEY_TYPE::KEY_Z))
	{
		MeshRender()->SetMaterial(m_pOriginMtrl);		
	}

	Transform()->SetLocalPos(vPos);
	Transform()->SetLocalRot(vRot);

	if (KEY_TAB(KEY_TYPE::KEY_SPACE))
	{
		// 미사일 쏘기
		CGameObject* pBullet = new CGameObject;

		vPos.y += 50;
		pBullet->AddComponent(new CTransform());
		pBullet->Transform()->SetLocalPos(vPos);
		pBullet->Transform()->SetLocalScale(Vec3(50.f, 50.f, 1.f));

		pBullet->AddComponent(new CMeshRender);
		pBullet->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"CircleMesh"));
		pBullet->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"Std2DMtrl"));

		pBullet->AddComponent(new CCollider2D);
		pBullet->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::RECT);


		pBullet->AddComponent(new CBulletScript);
			   
		CreateObject(pBullet, L"Bullet");
	}
}
