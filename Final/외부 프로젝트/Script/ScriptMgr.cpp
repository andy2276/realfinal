#include "stdafx.h"
#include "ScriptMgr.h"

#include "BulletScript.h"
#include "MonsterScript.h"
#include "PlayerScript.h"
#include "SomeScript.h"
#include "TestScript.h"

void CScriptMgr::GetScriptInfo(vector<wstring>& _vec)
{
	_vec.push_back(L"BulletScript");
	_vec.push_back(L"MonsterScript");
	_vec.push_back(L"PlayerScript");
	_vec.push_back(L"SomeScript");
	_vec.push_back(L"TestScript");
}

CScript * CScriptMgr::GetScript(const wstring& _strScriptName)
{
	if (L"BulletScript" == _strScriptName)
		return new CBulletScript;
	if (L"MonsterScript" == _strScriptName)
		return new CMonsterScript;
	if (L"PlayerScript" == _strScriptName)
		return new CPlayerScript;
	if (L"SomeScript" == _strScriptName)
		return new CSomeScript;
	if (L"TestScript" == _strScriptName)
		return new CTestScript;
	return nullptr;
}

const wchar_t * CScriptMgr::GetScriptName(CScript * _pScript)
{
	switch ((SCRIPT_TYPE)_pScript->GetScriptType())
	{
	case SCRIPT_TYPE::BULLETSCRIPT:
		return L"BulletScript";
		break;

	case SCRIPT_TYPE::MONSTERSCRIPT:
		return L"MonsterScript";
		break;

	case SCRIPT_TYPE::PLAYERSCRIPT:
		return L"PlayerScript";
		break;

	case SCRIPT_TYPE::SOMESCRIPT:
		return L"SomeScript";
		break;

	case SCRIPT_TYPE::TESTSCRIPT:
		return L"TestScript";
		break;

	}
	return nullptr;
}